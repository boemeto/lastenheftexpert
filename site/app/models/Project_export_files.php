<?php

class Project_export_files extends Eloquent
{
    public static $rules = [
        'file_name' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'file_id';
    protected $table = 'project_export_files';
    protected $guarded = ['file_id'];
    protected $fillable = ['file_name', 'file_type', 'project_id', 'exported_by', 'date'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Project_export_files::whereFile_id($id)->first();
        return $list;
    }

    public static function getLastByProjectId($id)
    {
        $list = Project_export_files::whereProject_id($id)->orderBy('file_id', 'desc')->first();
        return $list;
    }

    public static function getLastBySoftwareId($id)
    {
        $list = Project_export_files::whereSoftware_id($id)->orderBy('file_id', 'desc')->first();
        return $list;
    }

    public static function getLastByIndustryId($id)
    {
        $list = Project_export_files::whereIndustry_id($id)->orderBy('file_id', 'desc')->first();
        return $list;
    }
}
