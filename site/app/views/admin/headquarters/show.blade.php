@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <a href="{{URL::route('headquarters.edit',[ $headquarter->id_headquarter])}}"
               class="btn ls-light-blue-btn btn-flat" title="Edit Headquarter">
                <i class="fa fa-cube"></i>
            </a>
            <h3 class="box-title">Headquarter {{ $headquarter->name_headquarter }} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <ul>
                <li> Name: {{ $headquarter->name_headquarter }}  </li>
                <li> Email: {{ $headquarter->email_headquarter }}  </li>
                <li> Telephone: {{ $headquarter->telephone_headquarter }}  </li>
                <li> Address: {{ $headquarter->street}} {{$headquarter->street_nr }},
                    {{ $headquarter->city}}, {{$headquarter->country }}
                </li>
            </ul>
            <h3><i class="fa fa-user"></i> Users</h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Groups</th>
                    <th></th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->first_name.' '.$user->last_name}} </td>
                        <td>{{$user->username }} </td>
                        <td>{{$user->email }} </td>
                        <td>@foreach($groups as $id_group=> $group)
                                @if($user->fk_group == $id_group)
                                    {{$group}}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <a href="{{URL::route('members.edit',[$user->id])}}"
                               class="btn ls-light-blue-btn btn-flat" title="Edit user">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
            <br>
            {{ link_to_route('members.create',"Add user",['id'=>$headquarter->id_headquarter],['class'=>"btn ls-light-blue-btn btn-flat "]) }}
        </div>
        <!-- /.box-body -->
    </div>
@stop