<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmAttachementsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_attachements', function ($table) {
            $table->increments('id_attachement')->unsigned();
            $table->string('name_file', 200)->nullable();
            $table->integer('fk_task')->nullable();
            $table->string('file_type', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_attachements');
    }
}