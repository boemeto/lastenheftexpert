<div class="col-xs-3">
    <div class="user_details_dashboard">
        <div class="title_dashboard">
            {{trans('messages.label_welcome')}} {{$user->first_name}} {{$user->last_name}}
        </div>
        <?php $image = User::getLogoUser($user->id);    ?>
        <div class=" user_image_centerd">
            <img src="{{ URL::asset($image)}}" alt="image">

            <div class="description_dashboard">
                {{$company->name_company}}
            </div>
        </div>

    </div>
</div>


