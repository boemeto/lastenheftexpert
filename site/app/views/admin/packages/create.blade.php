<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title">{{trans('messages.act_add')}} {{trans('messages.nav_package')}}</h3>
    <div class="clear"></div>
</div>
{{ Form::open(['route'=>'packs.store'])}}

<div class="modal-body">
    <div class="row pd-10">
        <div class="col-sm-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_package', '', [
                  'class'=>"form-control",
                  'placeholder'=>trans('messages.l_name'),
                  'required',
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">DE</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description', '', ['class'=>"summernote form-control",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
        <div class="col-sm-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_package_en', '', [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">EN</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_en', '', ['class'=>"form-control summernote",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
        <div class="col-sm-6 pd-5">
            {{ Form::text('price', '', ['class'=>"form-control",'placeholder'=>trans('messages.label_price')]) }}
        </div>
        <div class="col-sm-6 pd-5">
            {{ Form::text('color', '', ['class'=>"form-control",'placeholder'=>trans('messages.label_color')]) }}
        </div>
        <div class="col-sm-6 pd-5">
            {{ Form::number('period', '', ['class'=>"form-control",'placeholder'=>trans('messages.label_period')]) }}
        </div>
        <div class="col-sm-6 pd-5">
            {{ Form::select("period_type",$period_type,0,["class"=>"form-control"]) }}
        </div>
        <div class="col-xs-6 pd-5">
            {{ Form::select("fk_type_project",$project_type,0,["class"=>"form-control"]) }}
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h3 class="box-title">{{trans('messages.set_subpackages')}}</h3>
        </div>
        @foreach($package_types as $package_type)
            <div class="col-sm-12 pd-5 subpackage_item">
              <div class="col-xs-4">
                  <div class="col-sm-6 pd-5 subpackage_details subpackage_details_responsive">
                      {{trans('messages.subpackage_name')}}:
                  </div>
                  <div class="col-sm-6 pd-5">
                      {{ Form::text('subpackage_name[' . $package_type->id_package_type.']', '', [
                          'class'=>"form-control",
                          'placeholder'=> $package_type->package_type_name,
                          'required',
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                        ]) }}
                  </div>
              </div>
              <div class="col-xs-4">
                <div class="col-sm-5 pd-5 subpackage_details">
                    {{trans('messages.subpackage_period')}}:
                </div>
                <div class="col-sm-7 pd-5">
                    {{ Form::number('subpackage_period[' . $package_type->id_package_type.']', '', [
                        'class'=>"form-control",
                        'placeholder'=>trans('messages.label_period_for') . ' ' . $package_type->package_type_name,
                        'required',
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                </div>
              </div>
              <div class="col-xs-4">
                <div class="col-sm-5 pd-5 subpackage_details">
                    {{trans('messages.subpackage_price')}}:
                </div>
                <div class="col-sm-7 pd-5">
                    {{ Form::number('subpackage_price[' . $package_type->id_package_type.']', '', [
                        'class'=>"form-control",
                        'placeholder'=>$package_type->package_type_name,
                        'required',
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                </div>
              </div>
            </div>
        @endforeach
    </div>
    <br>
</div>
<div class="modal-footer">
    <button type="submit" class='btn ls-light-blue-btn'>
        {{trans('messages.act_save_close')}}
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}
