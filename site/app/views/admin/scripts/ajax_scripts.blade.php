<script type="text/javascript">

    // call ajax
    function getXMLHTTP() {
        var xmlhttp = false;
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e1) {
                    xmlhttp = false;
                }
            }
        }

        return xmlhttp;
    }

    // Synonyme/Tags, Address-Code fields - call meta tags
    function input_tags_call() {
        $('.input-tags').selectize({
            plugins: ['remove_button'],
            delimiter: ' ',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            },
            render: {
                item: function (data, escape) {
                    return '<div>"' + escape(data.text) + '"</div>';
                }
            },
            onDelete: function (values) {
                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
            }
        });
    }


    // edit industry details
    function modal_dialog_edit_industry(id) {

        var strURL = "{{URL::to('/')}}/industry/" + id + "/edit";
        var req = getXMLHTTP();

        if (req) {

            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {

                        jQuery('#task_page_details').html(req.responseText);
                        $('.summernote').summernote({});
                        show_modal_box();

                    } else {
                        jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                    }
                }
            };
            req.open("GET", strURL, true);
            req.send(null);
        }
    }

    // edit software details
    function modal_dialog_edit_software(id) {

        var strURL = "{{URL::to('/')}}/software/" + id + "/edit";
        var req = getXMLHTTP();

        if (req) {

            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {

                        jQuery('#task_page_details').html(req.responseText);
                        $('.summernote').summernote({});
                        show_modal_box();

                    } else {
                        jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                    }
                }
            };
            req.open("GET", strURL, true);
            req.send(null);
        }
    }

    // edit project details
    function modal_dialog_edit_projects(id) {

        var strURL = "{{URL::to('/')}}/projects/" + id + "/edit";
        var req = getXMLHTTP();

        if (req) {

            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {

                        jQuery('#task_page_details').html(req.responseText);
                        $('.summernote').summernote({});
                        show_modal_box();

                    } else {
                        jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                    }
                }
            };
            req.open("GET", strURL, true);
            req.send(null);
        }
    }

    // edit an existing module/ submodule
    function modal_dialog_edit_module(id, is_project = 0) {
        if (id == 0) {
            document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
        }
        else {
            if(is_project == 0) {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_module/" + id;
            } else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_project_module/" + id;
            }
            var req = getXMLHTTP();
            if (req) {
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {

                            jQuery('#task_page_details').html(req.responseText);
                            input_tags_call();
                            jQuery(".switchCheckBox").bootstrapSwitch();
                            $('.summernote').summernote({});
                            $('input.icheck-green').iCheck({
                                checkboxClass: 'icheckbox_minimal-green',
                                radioClass: 'iradio_minimal-green',
                                increaseArea: '10%' // optional
                            });
                            show_modal_box();
                        } else {
                            jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }

    // edit task - add task to an industry (on check with ajax)
//    $(document).on('ifChecked', ".task_check", function (event) {
//
//        var id = jQuery(this).attr('id');
//        var id_nr = id.replace('task_check_', "");
//        var arr = id_nr.split('_');
//        var data = {
//            'id_industry': arr[0],
//            'id_ms_task': arr[1],
//            'is_checked': 1
//        };
//        // save to database
//        jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
//                .done(function (msg) {
//                    $.amaran({
//                        'theme': 'colorful',
//                        'content': {
//                            message: msg,
//                            bgcolor: '#324e59',
//                            color: '#fff'
//                        }
//                    });
//                })
//                .fail(function (xhr, textStatus, errorThrown) {
//                    alert(xhr.responseText);
//                });
//    });
//
//    // edit task - remove task from an industry (on check with ajax)
//    $(document).on("ifUnchecked", ".task_check", function () {
//        var id = jQuery(this).attr('id');
//        var id_nr = id.replace('task_check_', "");
//        var arr = id_nr.split('_');
//        var data = {
//            'id_industry': arr[0],
//            'id_ms_task': arr[1],
//            'is_checked': 0
//        };
//        // save to database
//        jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
//                .done(function (msg) {
//                    $.amaran({
//                        'theme': 'colorful',
//                        'content': {
//                            message: msg,
//                            bgcolor: '#324e59',
//                            color: '#fff'
//                        }
//                    });
//                })
//                .fail(function (xhr, textStatus, errorThrown) {
//                    alert(xhr.responseText);
//                });
//    });

    // edit module/submodule - add all child tasks / submodules to the checked industry
    $(document).on('click', '.add_all_industry', function () {
        var thisClick = $(this);
        var id = jQuery(this).attr('id');
        var id_nr = id.replace('add_all_industry_', "");
        var arr = id_nr.split('_');
        var idItem = $(this).data('id');
        if ($(this).hasClass('module-class')) {
            var type = 'module';
        } else {
            var type = 'submodule';
        }
        var data = {
            'id_industry': arr[1],
            'id_module_structure': arr[0],
            'type': type,
            'id_item': idItem
        };

        jQuery.post('{{  route("ajax.add_all_to_industry") }}', data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.parent().parent().find('input[type="checkbox"]').iCheck('check');
                    thisClick.children().removeClass('fa-plus').addClass('fa-check');
                    thisClick.parent().parent().find('div.icheckbox_minimal-green').addClass('checked');
                    thisClick.css('width', '20px');
                    thisClick.attr('disabled', 'disabled');
                    thisClick.parent().parent().find('.remove_all_industry').removeAttr('disabled');

                    // thisClick.parents('.article_content').find('.check-all-module').hide();
                    // thisClick.parents('.article_content').find('.check-all-module-inactive').show();
                    thisClick.parents('.article_content').find('.uncheck-all-module').show();
                    thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();

                    var industries_containers = thisClick.parents('.tab-pane').find('.industry-container').length;
                    var industries_checked = 0;
                    thisClick.parents('.tab-pane').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                            industries_checked++;
                        }
                    });

                    if(industries_containers == industries_checked) {
                        thisClick.parents('.tab-pane').find('.check-all-module-type').hide();
                        thisClick.parents('.tab-pane').find('.check-all-module-type-inactive').show();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type-inactive').hide();
                    }

                    if(industries_checked > 0 && industries_containers > industries_checked) {
                        thisClick.parents('.tab-pane').find('.check-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.check-all-module-type-inactive').hide();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type-inactive').hide();
                    }

                    var total_industries_containers = thisClick.parents('.tab-content').find('.industry-container').length;
                    var total_industries_checked = 0;
                    thisClick.parents('.tab-content').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                            total_industries_checked++;
                        }
                    });

                    if(total_industries_containers == total_industries_checked) {
                        thisClick.parents('.article_content').find('.check-all-module').hide();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();
                    }
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                });
    });

    $(document).on('click', '.check-all-module', function() {
        var title = "Confirm";
        var msg = $(this).attr('data-message');
        // var categoryId = $(this).attr('data-category-id');
        var url = $(this).attr('data-url');
        var id_submodule = $(this).attr('data-submodule-id');
        var type = $(this).attr('data-type');

        $('#modalConfirmModule').find('#frm_body').html(msg).end()
                                .find('#frm_title').html(title).end()
                                .modal('show');

        $("#submit_module").addClass('check-all-module-ok');
        $("#submit_module").append('<input class="modal-input" type="hidden"/>');
        $('.modal-input').attr('data-url', url)
                         .attr('data-submodule-id', id_submodule)
                         .attr('data-type', type);
    });

    $(document).on('click', '.modal-cancel-btn', function() {
        $("#submit_module").removeClass('check-all-module-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    $(document).on('click', '.close', function() {
        $("#submit_module").removeClass('check-all-module-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    // add module/submodule for all industries
    $(document).on('click', '.check-all-module-ok', function () {
        var input = $(this).find('.modal-input');
        var id_submodule = $(this).find('.modal-input').attr('data-submodule-id');
        var url = $(this).find('.modal-input').attr('data-url');
        var type = $(this).find('.modal-input').attr('data-type');
        var thisClick = $('.check-all-module');

        var data = {
            'id_module_structure': id_submodule,
            'type': type
        };

        $(this).find('.modal-input').remove();
        $(this).removeClass('check-all-module-ok');
        $('#modalConfirmModule').modal('hide');

        jQuery.post(url, data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.hide();
                    thisClick.siblings('.check-all-module-inactive').show();
                    thisClick.siblings('.uncheck-all-module').show();
                    thisClick.siblings('.uncheck-all-module-inactive').hide();

                    thisClick.parents('.article_content').find('.check-all-type-module').each(function() {
                        $(this).find('.check-all-module-type').hide();
                        $(this).find('.check-all-module-type-inactive').show();
                        $(this).find('.uncheck-all-module-type').show();
                        $(this).find('.uncheck-all-module-type-inactive').hide();
                    });

                    thisClick.parents('.article_content').find('a.add_all_industry').each(function() {
                        $(this).find('i').removeClass('fa-plus').addClass('fa-check');
                        $(this).attr('disabled', 'disabled');
                        $(this).css('width', '20px');
                        $(this).parent().siblings().find('a.remove_all_industry').removeAttr('disabled');
                        $(this).parent().parent().find('input[type="checkbox"]').iCheck('check');
                        $(this).parent().parent().find('div.icheckbox_minimal-green').addClass('checked');
                    });
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    // alert(xhr.responseText);
                });
    });

    $(document).on('click', '.check-all-module-type', function() {
        var title = "Confirm";
        var msg = $(this).attr('data-message');
        var categoryId = $(this).attr('data-category-id');
        var url = $(this).attr('data-url');
        var id_submodule = $(this).attr('data-submodule-id');
        var type = $(this).attr('data-type');

        $('#modalConfirmModule').find('#frm_body').html(msg).end()
                                .find('#frm_title').html(title).end()
                                .modal('show');

        $("#submit_module").addClass('check-all-module-type-ok');
        $("#submit_module").append('<input class="modal-input" type="hidden"/>');
        $('.modal-input').attr('data-category-id', categoryId)
                         .attr('data-url', url)
                         .attr('data-submodule-id', id_submodule)
                         .attr('data-type', type);
    });

    $(document).on('click', '.modal-cancel-btn', function() {
        $("#submit_module").removeClass('check-all-module-type-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    $(document).on('click', '.close', function() {
        $("#submit_module").removeClass('check-all-module-type-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    // edit module/submodule - add all child tasks / submodules to the checked industry category
    $(document).on('click', '.check-all-module-type-ok', function () {
        var input = $(this).find('.modal-input');
        var id_industry_category = $(this).find('.modal-input').attr('data-category-id');
        var id_submodule = $(this).find('.modal-input').attr('data-submodule-id');
        var url = $(this).find('.modal-input').attr('data-url');
        var type = $(this).find('.modal-input').attr('data-type');
        var thisClick = $('#ind'+id_industry_category).find('.check-all-module-type');

        var data = {
            'id_industry_category': id_industry_category,
            'id_module_structure': id_submodule,
            'type': type
        };

        $(this).find('.modal-input').remove();
        $(this).removeClass('check-all-module-type-ok');
        $('#modalConfirmModule').modal('hide');

        jQuery.post(url, data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.hide();
                    thisClick.siblings('.check-all-module-type-inactive').show();
                    thisClick.siblings('.uncheck-all-module-type').show();
                    thisClick.siblings('.uncheck-all-module-type-inactive').hide();
                    // console.log(thisClick.parents('.check-all-type-module'));
                    thisClick.parents('.tab-pane').find('a.add_all_industry').each(function() {
                        $(this).find('i').removeClass('fa-plus').addClass('fa-check');
                        $(this).attr('disabled', 'disabled');
                        $(this).css('width', '20px');
                        $(this).parent().siblings().find('a.remove_all_industry').removeAttr('disabled');
                        $(this).parent().parent().find('input[type="checkbox"]').iCheck('check');
                        $(this).parent().parent().find('div.icheckbox_minimal-green').addClass('checked');
                    });

                    var total_industries_containers = thisClick.parents('.tab-content').find('.industry-container').length;
                    var total_industries_checked = 0;
                    thisClick.parents('.tab-content').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                            total_industries_checked++;
                        }
                    });

                    if(total_industries_containers == total_industries_checked) {
                        thisClick.parents('.article_content').find('.check-all-module').hide();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();
                    }

                    if(total_industries_checked > 0 && total_industries_containers > total_industries_checked) {
                        thisClick.parents('.article_content').find('.check-all-module').show();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();
                    }
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    // alert(xhr.responseText);
                });
    });

    // edit module/submodule - remove all child tasks / submodules from the checked industry
    $(document).on("click", ".remove_all_industry", function () {
        var thisClick = $(this);
        var id = jQuery(this).attr('id');
        var id_nr = id.replace('remove_all_industry_', "");
        var arr = id_nr.split('_');
        var idItem = $(this).data('id');
        if ($(this).hasClass('module-class')) {
            var type = 'module';
        } else {
            var type = 'submodule';
        }
        var data = {
            'id_industry': arr[1],
            'id_module_structure': arr[0],
            'type': type,
            'id_item': idItem
        };
        jQuery.post('{{  route("ajax.remove_all_from_industry") }}', data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.parent().parent().find('input[type="checkbox"]').iCheck('uncheck');
                    thisClick.attr('disabled', 'disabled');
                    thisClick.parent().parent().find('div.icheckbox_minimal-green').removeClass('checked');
                    thisClick.parent().parent().find('.add_all_industry').removeAttr('disabled');
                    thisClick.parent().parent().find('.add_all_industry').children().removeClass('fa-check').addClass('fa-plus');
                    thisClick.parent().parent().find('.add_all_industry').css('width', '');

                    thisClick.parents('.article_content').find('.check-all-module').show();
                    thisClick.parents('.article_content').find('.check-all-module-inactive').hide();
                    // thisClick.parents('.article_content').find('.uncheck-all-module').show();
                    // thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();

                    var industries_containers = thisClick.parents('.tab-pane').find('.industry-container').length;
                    var industries_checked = 0;
                    thisClick.parents('.tab-pane').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') !== "disabled" ){
                            industries_checked++;
                        }
                    });

                    if(industries_containers == industries_checked) {
                        thisClick.parents('.tab-pane').find('.check-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.check-all-module-type-inactive').hide();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type').hide();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type-inactive').show();
                    }

                    if(industries_checked > 0 && industries_containers > industries_checked) {
                        thisClick.parents('.tab-pane').find('.check-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.check-all-module-type-inactive').hide();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type').show();
                        thisClick.parents('.tab-pane').find('.uncheck-all-module-type-inactive').hide();
                    }

                    var total_industries_containers = thisClick.parents('.tab-content').find('.industry-container').length;
                    var total_industries_checked = 0;
                    thisClick.parents('.tab-content').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') !== "disabled" ){
                            total_industries_checked++;
                        }
                    });

                    if(total_industries_containers == total_industries_checked) {
                        thisClick.parents('.article_content').find('.check-all-module').show();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').show();
                    }
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                });
    });

    $(document).on('click', '.uncheck-all-module', function() {
        var title = "Confirm";
        var msg = $(this).attr('data-message');
        var url = $(this).attr('data-url');
        var id_submodule = $(this).attr('data-submodule-id');
        var type = $(this).attr('data-type');

        $('#modalConfirmModule').find('#frm_body').html(msg).end()
                                .find('#frm_title').html(title).end()
                                .modal('show');

        $("#submit_module").addClass('uncheck-all-module-ok');
        $("#submit_module").append('<input class="modal-input" type="hidden"/>');
        $('.modal-input').attr('data-url', url)
                         .attr('data-submodule-id', id_submodule)
                         .attr('data-type', type);
    });

    $(document).on('click', '.modal-cancel-btn', function() {
        $("#submit_module").removeClass('uncheck-all-module-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    $(document).on('click', '.close', function() {
        $("#submit_module").removeClass('uncheck-all-module-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    // remove module/submodule from all industries
    $(document).on("click", ".uncheck-all-module-ok", function () {
        var input = $(this).find('.modal-input');
        var id_submodule = $(this).find('.modal-input').attr('data-submodule-id');
        var url = $(this).find('.modal-input').attr('data-url');
        var type = $(this).find('.modal-input').attr('data-type');
        var thisClick = $('.uncheck-all-module');

        var data = {
            'id_module_structure': id_submodule,
            'type': type
        };

        $(this).find('.modal-input').remove();
        $(this).removeClass('uncheck-all-module-ok');
        $('#modalConfirmModule').modal('hide');

        jQuery.post(url, data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.hide();
                    thisClick.siblings('.check-all-module').show();
                    thisClick.siblings('.check-all-module-inactive').hide();
                    thisClick.siblings('.uncheck-all-module-inactive').show();

                    thisClick.parents('.article_content').find('.check-all-type-module').each(function() {
                        $(this).find('.check-all-module-type').show();
                        $(this).find('.check-all-module-type-inactive').hide();
                        $(this).find('.uncheck-all-module-type').hide();
                        $(this).find('.uncheck-all-module-type-inactive').show();
                    });

                    thisClick.parents('.article_content').find('a.remove_all_industry').each(function() {
                        $(this).attr('disabled', 'disabled');
                        $(this).parent().parent().find('input[type="checkbox"]').iCheck('uncheck');
                        $(this).parent().parent().find('div.icheckbox_minimal-green').removeClass('checked');
                        $(this).parent().parent().find('.add_all_industry').removeAttr('disabled');
                        $(this).parent().parent().find('.add_all_industry').children().removeClass('fa-check').addClass('fa-plus');
                        $(this).parent().parent().find('.add_all_industry').css('width', '');
                    });
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    // alert(xhr.responseText);
                });
    });

    $(document).on('click', '.uncheck-all-module-type', function() {
        var title = "Confirm";
        var msg = $(this).attr('data-message');
        var categoryId = $(this).attr('data-category-id');
        var url = $(this).attr('data-url');
        var id_submodule = $(this).attr('data-submodule-id');
        var type = $(this).attr('data-type');

        $('#modalConfirmModule').find('#frm_body').html(msg).end()
                                .find('#frm_title').html(title).end()
                                .modal('show');

        $("#submit_module").addClass('uncheck-all-module-type-ok');
        $("#submit_module").append('<input class="modal-input" type="hidden"/>');
        $('.modal-input').attr('data-category-id', categoryId)
                         .attr('data-url', url)
                         .attr('data-submodule-id', id_submodule)
                         .attr('data-type', type);
    });

    $(document).on('click', '.modal-cancel-btn', function() {
        $("#submit_module").removeClass('uncheck-all-module-type-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    $(document).on('click', '.close', function() {
        $("#submit_module").removeClass('uncheck-all-module-type-ok');
        $("#submit_module").find('.modal-input').remove();
    });

    // edit module/submodule - remove all child tasks / submodules from the checked industry category
    $(document).on("click", ".uncheck-all-module-type-ok", function () {
        var input = $(this).find('.modal-input');
        var id_industry_category = $(this).find('.modal-input').attr('data-category-id');
        var id_submodule = $(this).find('.modal-input').attr('data-submodule-id');
        var url = $(this).find('.modal-input').attr('data-url');
        var type = $(this).find('.modal-input').attr('data-type');
        var thisClick = $('#ind'+id_industry_category).find('.uncheck-all-module-type');

        var data = {
            'id_industry_category': id_industry_category,
            'id_module_structure': id_submodule,
            'type': type
        };

        $(this).find('.modal-input').remove();
        $(this).removeClass('uncheck-all-module-type-ok');
        $('#modalConfirmModule').modal('hide');

        $('#modalConfirmModule').find('.modal-cancel-btn').on('click', function() {
            $("#submit_module").removeClass('uncheck-all-module-type-ok');
            $("#submit_module").find('.modal-input').remove();
        });

        $('#modalConfirmModule').find('.close').on('click', function() {
            $("#submit_module").removeClass('uncheck-all-module-type-ok');
            $("#submit_module").find('.modal-input').remove();
        });

        jQuery.post(url, data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                    thisClick.hide();
                    thisClick.siblings('.uncheck-all-module-type-inactive').show();
                    thisClick.siblings('.check-all-module-type').show();
                    thisClick.siblings('.check-all-module-type-inactive').hide();
                    thisClick.parents('.tab-pane').find('a.remove_all_industry').each(function() {
                        $(this).parent().parent().find('.add_all_industry').children().removeClass('fa-check').addClass('fa-plus');
                        $(this).attr('disabled', 'disabled');
                        $(this).css('width', '20px');
                        $(this).parent().siblings().find('a.add_all_industry').removeAttr('disabled');
                        $(this).parent().parent().find('input[type="checkbox"]').iCheck('uncheck');
                        $(this).parent().parent().find('div.icheckbox_minimal-green').removeClass('checked');
                        // thisClick.parent().parent().find('input[type="checkbox"]').iCheck('check');
                        // thisClick.children().removeClass('fa-plus').addClass('fa-check');
                    });

                    var total_industries_containers = thisClick.parents('.tab-content').find('.industry-container').length;
                    var total_industries_checked = 0;
                    thisClick.parents('.tab-content').find('.industry-container').each(function() {
                        if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                            total_industries_checked++;
                        }
                    });

                    if(total_industries_checked == 0) {
                        thisClick.parents('.article_content').find('.check-all-module').show();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').show();
                    }

                    if(total_industries_checked > 0 && total_industries_containers > total_industries_checked) {
                        thisClick.parents('.article_content').find('.check-all-module').show();
                        thisClick.parents('.article_content').find('.check-all-module-inactive').hide();
                        thisClick.parents('.article_content').find('.uncheck-all-module').show();
                        thisClick.parents('.article_content').find('.uncheck-all-module-inactive').hide();
                    }

                    // alert(total_industries_containers+'----'+total_industries_checked);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    // alert(xhr.responseText);
                });
    });

    // on page loaded
    jQuery(document).ready(function () {






        // fade loader when page is loaded
        $('.loader_back').fadeOut(1000);

        // description character counter
        var allWordCounter = 0;
        $('[name="description_counter"]').each(function() {
            allWordCounter += parseInt($(this).val());
        });
        $('.description_word_counter').text(allWordCounter);

        // hide tasks div
        jQuery('.close_tasks').click(function () {
            var li = $(this).parent().parent();
            if ($(this).hasClass('glyphicon-minus-sign')) {
                li.find('.border_row').hide("slow");
                $(this).addClass('glyphicon-plus-sign')
                        .removeClass('glyphicon-minus-sign');
            } else {
                $(this).addClass('glyphicon-minus-sign')
                        .removeClass('glyphicon-plus-sign');
                li.find('.border_row').show("slow");
            }
        });

        // show all functions closed
        function closeEasyTree() {
            var parent = $(".easy-tree").find('li').not('#project_name');
            var children = parent.find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                parent.children('span').find(
                        ' > span.glyphicon').addClass(
                        'glyphicon-plus-sign').removeClass(
                        'glyphicon-minus-sign');
            }
            $('.border_row').hide();

        }

//        closeEasyTree();
    });
    // effect on the right iframe box to appear
    function show_modal_box() {

        $('#task_page_details_parent').show("slide", {direction: "right"}, 500);

        $('a .hide_controller ').attr('onclick', 'hide_modal_box()');
        $('.iframe-overlay').fadeIn();
    }

    // effect on the right iframe box to hide
    function hide_modal_box() {

        $('a .hide_controller ').attr('onclick', 'show_modal_box()');
        $('#task_page_details_parent').hide("slide", {direction: "right"}, 500);
        $('.iframe-overlay').fadeOut();
    }

    // nested filter  - search function jquery
    $(function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[":"].contains = jQuery.expr.createPseudo(function (arg) {
            return function (elem) {
                return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        // this function calls itself, to search in every level
        function hasChild(list) {
            // the searched value
            var filter = $('#filterinput').val();
            // for every children, do function:
            list.children('li').each(function () {
                // see if the list contains the filte
                if ($(this).find("span > a:contains(" + filter + ")").length > 0) {
                    // see if it has nested levels
                    if ($(this).find('ul :first').length > 0) {
                        $(this).show();
                        // recall function, searching the ul tag
                        hasChild($(this).find('ul :first').parent());
                    } else {
                        $(this).show();
                    }
                } else {
                    $(this).hide();
                }
            });
        }

        $('#filterinput').keyup(function () {
            // the id of the filtered list
            var list = '#list';
            // the searched value
            var filter = $('#filterinput').val();
            if (filter) {
                // call previous function
                hasChild($(list));
            } else {
                // show all
                $(list).find("li").show();
            }
            hide_empty_modules();
            return false;
        });
    });

    $(document).on('change', '#task_pair', function () {

        var selected = $(this).find('option:selected');
        var color = selected.data('color');
        if (color == null) {
            $(this).css('background', '#ffffff');
        } else {
            $(this).css('background', color);
        }

    });
</script>
