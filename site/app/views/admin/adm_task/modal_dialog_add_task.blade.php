<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
{{ Form::open(['route'=>'adm_task.store', 'enctype'=>"multipart/form-data"])}}
<div class="modal-header task-label-white">

    Neue Task

</div>

<div class="modal-body show_textarea">
    <div class="row">
        <div class="col-md-12">
            {{-- `id_task`, `name_task`, `notes_task`, `fk_creator`, `fk_developer`, `fk_sprint`, `fk_status`, `fk_priority`,--}}
            <div class="form-group">

                {{Form::text('name_task','',['class'=>'form-control','placeholder'=>trans('messages.label_name_task')])}}
            </div>
            <div class="form-group">

                {{Form::textarea('notes_task','',['class'=>'form-control summernote_task'])}}
                {{Form::hidden('fk_sprint',$fk_sprint)}}
                {{Form::hidden('fk_status',1)}}
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-exclamation"></i> </span>
                        {{Form::select('fk_priority',$priorities,'0',['class'=>'form-control', 'required'])}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
                        <input name="due_date" type="text" id="sandbox-container" value="{{date('d.m.Y')}}" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-eur"></i> </span>
                        {{Form::text('cost_task','',['class'=>'form-control','placeholder'=>'Kosten für die Aufgabe'  ])}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-check-square-o"></i> </span>
                        {{Form::select('fk_status',$status,1,['class'=>'form-control','disabled'])}}
                    </div>
                </div>
            </div>
            <br>

            <div class="adm_task_attachments">

                <div class="input-group">
                <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
              {{trans('messages.msg_attach_files')}}
                    <input type="file" name="task_attachment[]" multiple>
                </span>

                </span>
                    <input type="text" class="form-control" readonly style="width: 200px">
                </div>

            </div>

            <br>

            <div class=" row">
                @foreach($users as $user)
                    <?php    $image_user = User::getLogoUser($user->id); ?>
                    <div class="col-md-3">
                        <table>
                            <tr>
                                <td><img class="img-circle" src="{{ URL::asset($image_user)}}" title="{{$user->first_name.' '.$user->last_name}}">
                                </td>
                            </tr>
                            <tr>
                                <td><br>
                                    {{ Form::checkbox('fk_responsible[]',$user->id,false,['class'=>'switchCheckBox','data-size'=>"mini"]) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                @endforeach
            </div>
            <br>

        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn ls-light-blue-btn btn-block">

        {{trans('messages.act_save')}}
    </button>


    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}


 