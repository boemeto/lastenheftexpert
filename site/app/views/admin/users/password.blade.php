@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-user"></i>
            <h3 class="box-title"> Change Password for User {{$user->first_name}} {{$user->last_name}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <br>
            {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id]])}}
            <div class="form-group has-feedback">
                {{ Form::password("password_old",[ "placeholder"=>"Old Password", "class"=>"form-control",'id'=>'password_old', "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("password_old",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{ Form::password("password",[ "placeholder"=>"New Password", "class"=>"form-control",'id'=>'password', "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("password",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{ Form::password("password_confirm",[ "placeholder"=>"Password confirm", "class"=>"form-control",'id'=>'password_confirm', "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("password_confirm",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <input type="hidden" name="redirect_link" value="password_admin">
            {{ Form::submit("Change Password",["class" => "btn ls-light-blue-btn  btn-flat" ]) }}
            {{ Form::close() }}
            <br><br>
        </div>
        <!-- /.box-body -->
    </div>
@stop
