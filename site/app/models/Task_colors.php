<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 04-Dec-15
 * Time: 09:55
 */
class Task_colors extends Eloquent
{


    public static $messages = [];
    public $primaryKey = 'id_task_color';
    protected $table = 'pr_tasks_colors';
    protected $guarded = ['id_task_color'];
    protected $fillable = ['name_color'];

    public static function getFirstById($id)
    {
        $list = self::where('id_task_color', '=', $id)->first();
        return $list;
    }

    public static function getFirstByName($id)
    {
        $list = self::where('name_color', '=', $id)->first();
        return $list;
    }


    public static function getAllColorsList()
    {
        $array = array();
        $lists = self::select('*')->get();

        foreach ($lists as $list) {
            if (Session::get('lang') == 'de') {
                $array[$list->id_task_color] = $list->name_color;
            } else {
                $array[$list->id_task_color] = $list->name_color_en;
            }

        }
        return $array;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_tasks_colors', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_tasks_colors', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }
}