@extends('front.layouts.login')
        <!-- uses the front/layouts/login template -->
@section('content')
    <div class="login-content">
        <div class="login-user-icon">
            <i class="glyphicon glyphicon-user turquoise"></i>
        </div>
        <h3>Identify Yourself</h3>
    </div>
    {{-- the login form --}}
    <div class="login-form">
        {{ Form::open(array('action' => 'SessionsController@store', 'class'=>"form-horizontal ls_form", 'role'=>"login"))}}
        {{-- email validatin message--}}
        {{ $errors->first('email_','<div class="text-red">:message</div>') }}
        {{-- email input--}}
        <div class="input-group ls-group-input">
            {{ Form::email('email_','',[ 'placeholder'=>"E-mail", 'class'=>"form-control", 'required']) }}
            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
        </div>
        {{-- password validatin message--}}
        {{ $errors->first('password_','<div class="text-red">:message</div>') }}
        {{-- password input --}}
        <div class="input-group ls-group-input">
            {{ Form::password('password_',['placeholder'=>"Password", 'class'=>"form-control", 'required']) }}
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
        </div>
        {{-- confirmation message --}}
        {{ $errors->first('confirm_','<div class="text-green">:message</div>') }}

        {{-- submit button --}}
        <div class="input-group ls-group-input login-btn-box">
            <button class="btn ls-light-green-btn ladda-button" data-style="slide-down">
                <span class="ladda-label"><i class="fa fa-key"></i></span>
                <span class="ladda-spinner"></span></button>
            {{-- forgot password link--}}
            <a class="forgot-password" href="{{ URL::to('sessions/create') }}">Forgot password</a>
            {{-- register link --}}
            <a class="register-now" href="{{ URL::to('sessions/create') }}">Register new user</a>
        </div>
    </div>
    {{ Form::close() }}
@stop
