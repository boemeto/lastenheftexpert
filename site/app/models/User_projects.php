<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 8/3/2015
 * Time: 2:17 PM
 */
class User_projects extends Eloquent
{


    public static $rules = [
        'fk_user' => 'required',
        'fk_project' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_user_project';
    protected $table = 'pr_users_projects';
    protected $guarded = ['id_user_project'];
    protected $fillable = ['fk_user', 'fk_project', 'is_owner'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = User_projects::whereId_user_project($id)->first();
        return $list;
    }

    public static function getSelectableUsersByProject($id_project)
    {

        $projects = Project::getFirstById($id_project);
        $fk_company = $projects->fk_company;
        $arr_proj = array();
        $user_projects = User_projects::whereFk_project($id_project)->get();
        foreach ($user_projects as $user_project) {
            $arr_proj[] = $user_project->fk_user;
        }

        $users = User::whereNotIn('id', $arr_proj)
            ->whereFk_company($fk_company)
            ->get();
        if (count($users) == 0) {
            $users = array();
        }

        return $users;

    }

    public static function getSelectedUsersByProject($id_project)
    {

        $projects = Project::getFirstById($id_project);
        $fk_company = $projects->fk_company;

        $users = DB::table('users')
            ->join('pr_users_projects', 'pr_users_projects.fk_user', '=', 'users.id')
            ->whereFk_project($id_project)
            ->whereFk_company($fk_company)
            ->get();

        return $users;

    }

}