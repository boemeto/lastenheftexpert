-- phpMyAdmin SQL Dump
-- version 4.2.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 08, 2015 at 09:17 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `erp3`
--
CREATE DATABASE IF NOT EXISTS `erp3` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `erp3`;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE IF NOT EXISTS `actions` (
`id_action` int(11) unsigned NOT NULL,
  `name_action` varchar(100) COLLATE utf8_bin NOT NULL,
  `details_action` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id_action`, `name_action`, `details_action`, `created_at`, `updated_at`) VALUES
(1, 'Login', 'user login', '2015-06-02 09:26:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `affiliation`
--

CREATE TABLE IF NOT EXISTS `affiliation` (
`id_affiliation` bigint(20) unsigned NOT NULL,
  `fk_company` bigint(20) unsigned NOT NULL,
  `email_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pending` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `affiliation_company`
--

CREATE TABLE IF NOT EXISTS `affiliation_company` (
`id_affiliation_company` bigint(20) unsigned NOT NULL,
  `fk_company` bigint(20) unsigned DEFAULT NULL,
  `fk_affiliation` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
`id_company` bigint(20) unsigned NOT NULL,
  `name_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cui_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_company` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `users_company` int(11) DEFAULT NULL,
  `size_company` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id_company`, `name_company`, `cui_company`, `website_company`, `email_company`, `users_company`, `size_company`, `created_at`, `updated_at`) VALUES
(9, 'First Company S.R.L', '2313123', 'www.test.com', 'mycompany@name.com', NULL, NULL, '2015-06-02 04:34:01', '2015-06-02 04:34:01'),
(10, 'Second Test', '1223132', '', 'laura.diosoft@yahoo.com', NULL, NULL, '2015-06-02 04:37:30', '2015-06-04 08:39:42'),
(11, 'Test 3', '', '', '', NULL, NULL, '2015-06-02 11:10:39', '2015-06-02 11:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `company_headquarters`
--

CREATE TABLE IF NOT EXISTS `company_headquarters` (
`id_headquarter` bigint(20) unsigned NOT NULL,
  `name_headquarter` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_headquarter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone_headquarter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_main` tinyint(4) NOT NULL DEFAULT '0',
  `fk_company` bigint(20) unsigned DEFAULT NULL,
  `fk_location` bigint(20) unsigned DEFAULT NULL,
  `is_blocked` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `company_headquarters`
--

INSERT INTO `company_headquarters` (`id_headquarter`, `name_headquarter`, `email_headquarter`, `telephone_headquarter`, `is_main`, `fk_company`, `fk_location`, `is_blocked`, `created_at`, `updated_at`) VALUES
(11, 'Loc1', 'test@rer.com', '075555444', 1, 9, 11, 1, '2015-06-02 04:34:01', '2015-06-02 04:34:01'),
(12, 'Loc2', 'laura.diosoft@yahoo.com', '', 1, 10, 12, 1, '2015-06-02 04:37:31', '2015-06-04 04:39:43'),
(13, 'Loc3', '', '', 0, 10, 13, 1, '2015-06-02 08:21:07', '2015-06-02 08:21:07'),
(14, 'Test name', '', '', 1, 11, 14, 1, '2015-06-02 11:10:39', '2015-06-02 11:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `company_industries`
--

CREATE TABLE IF NOT EXISTS `company_industries` (
`id_company_industry` bigint(20) unsigned NOT NULL,
  `fk_headquarter` bigint(20) unsigned DEFAULT NULL,
  `fk_industry` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dictionary`
--

CREATE TABLE IF NOT EXISTS `dictionary` (
`id_dictionary` int(11) NOT NULL,
  `name_dictionary` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dictionary_positions`
--

CREATE TABLE IF NOT EXISTS `dictionary_positions` (
  `id_position` int(11) NOT NULL,
  `name_position` varchar(100) COLLATE utf8_bin NOT NULL,
  `value_position` varchar(100) COLLATE utf8_bin NOT NULL,
  `fk_dictionary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id_group` int(11) NOT NULL,
  `name_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_selectable_user` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id_group`, `name_group`, `permissions`, `is_selectable_user`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'access backend, access front', 0, '2015-04-30 21:00:00', '2015-04-30 21:00:57'),
(2, 'Company Admin', 'acces front, special functions/ company', 1, '2015-04-30 21:00:00', '2015-04-30 21:00:00'),
(3, 'Company user ', 'Frontend - few permissions', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `industry_type`
--

CREATE TABLE IF NOT EXISTS `industry_type` (
`id_industry_type` int(11) unsigned NOT NULL,
  `name_industry_type` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
`id_location` bigint(20) unsigned NOT NULL,
  `street_nr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id_location`, `street_nr`, `street`, `city`, `state`, `country`, `postal_code`, `latitude`, `longitude`, `address_string`, `created_at`, `updated_at`) VALUES
(11, '', 'Strada Nufărului', 'Oradea', 'Județul Bihor', 'Romania', '34-223', '47.029298', '21.955173999999943', 'Nufărului, Strada Nufărului, Oradea, Bihor County, Romania', '2015-06-02 04:34:01', '2015-06-02 04:34:01'),
(12, '10', 'D.S.Abdelaziz', 'Marrakesh', 'Marrakesh-Tensift-El Haouz', 'Morocco', 'CA14 2RE', '31.6317948', '-7.9886748000000125', 'D.S.Abdelaziz, Marrakesh, Marrakesh-Tensift-El Haouz, Morocco', '2015-06-02 04:37:30', '2015-06-04 09:10:30'),
(13, '', 'Bulevardul Decebal', 'Oradea', 'Județul Bihor', 'Romania', '', '47.0607735', '21.918826200000012', 'Decebal Boulevard, Oradea, Bihor County, Romania', '2015-06-02 08:21:07', '2015-06-02 08:21:07'),
(14, '70', 'Bulevardul Decebal', 'Oradea', 'Județul Bihor', 'Romania', '', '47.055799', '21.917715999999928', 'Decebal Boulevard 70, Oradea, Bihor County, Romania', '2015-06-02 11:10:39', '2015-06-02 11:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
`id_migration` int(10) NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id_migration`, `migration`, `batch`) VALUES
(1, '2015_05_13_073600_create_users_table', 1),
(2, '2015_05_14_121743_create_company_table', 1),
(3, '2015_05_14_121816_create_company_headquarters_table', 1),
(4, '2015_05_14_122639_create_company_industries_table', 1),
(5, '2015_05_14_122700_create_groups_table', 1),
(6, '2015_05_14_122716_create_users_groups_table', 1),
(7, '2015_05_14_122800_create_affiliation_company_table', 2),
(8, '2015_05_14_131606_create_affiliation_table', 2),
(9, '2015_05_15_083458_create_users_actions_table', 2),
(10, '2015_05_18_115543_create_location_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
`id_throttle` int(10) unsigned NOT NULL,
  `fk_user` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(4) NOT NULL DEFAULT '0',
  `banned` tinyint(4) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_actions`
--

CREATE TABLE IF NOT EXISTS `user_actions` (
`id_user_action` bigint(20) unsigned NOT NULL,
  `fk_user` bigint(20) unsigned DEFAULT NULL,
  `fk_action` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `user_actions`
--

INSERT INTO `user_actions` (`id_user_action`, `fk_user`, `fk_action`, `created_at`, `updated_at`) VALUES
(2, 13, 1, '2015-06-02 06:26:45', '2015-06-02 06:26:45'),
(3, 13, 1, '2015-06-02 11:11:35', '2015-06-02 11:11:35'),
(4, 13, 1, '2015-06-04 03:59:10', '2015-06-04 03:59:10'),
(5, 13, 1, '2015-06-04 05:20:34', '2015-06-04 05:20:34'),
(6, 15, 1, '2015-06-04 05:29:27', '2015-06-04 05:29:27'),
(7, 15, 1, '2015-06-04 05:30:56', '2015-06-04 05:30:56'),
(8, 15, 1, '2015-06-04 07:46:58', '2015-06-04 07:46:58'),
(9, 13, 1, '2015-06-04 09:36:25', '2015-06-04 09:36:25'),
(10, 12, 1, '2015-06-04 09:38:02', '2015-06-04 09:38:02'),
(11, 15, 1, '2015-06-04 09:38:24', '2015-06-04 09:38:24'),
(12, 13, 1, '2015-06-04 09:46:51', '2015-06-04 09:46:51'),
(13, 12, 1, '2015-06-04 10:37:41', '2015-06-04 10:37:41'),
(14, 15, 1, '2015-06-04 10:59:37', '2015-06-04 10:59:37'),
(15, 12, 1, '2015-06-04 10:59:47', '2015-06-04 10:59:47'),
(16, 15, 1, '2015-06-04 11:02:01', '2015-06-04 11:02:01'),
(17, 12, 1, '2015-06-04 11:02:12', '2015-06-04 11:02:12'),
(18, 12, 1, '2015-06-04 11:12:56', '2015-06-04 11:12:56'),
(19, 13, 1, '2015-06-04 11:17:33', '2015-06-04 11:17:33'),
(20, 12, 1, '2015-06-04 11:31:05', '2015-06-04 11:31:05'),
(21, 13, 1, '2015-06-04 11:39:06', '2015-06-04 11:39:06'),
(22, 13, 1, '2015-06-05 03:37:15', '2015-06-05 03:37:15'),
(23, 13, 1, '2015-06-05 05:06:38', '2015-06-05 05:06:38'),
(24, 13, 1, '2015-06-05 07:04:18', '2015-06-05 07:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id_user` bigint(20) unsigned NOT NULL,
  `fk_title` int(11) DEFAULT NULL,
  `fk_personal_title` int(11) DEFAULT NULL,
  `first_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `activated` tinyint(4) DEFAULT '0',
  `activation_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `fk_company` bigint(20) unsigned DEFAULT NULL,
  `fk_headquarter` bigint(20) unsigned DEFAULT NULL,
  `fk_group` int(11) DEFAULT '3',
  `is_blocked` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `fk_title`, `fk_personal_title`, `first_name`, `last_name`, `username`, `email`, `password`, `activated`, `activation_code`, `activated_at`, `last_login`, `language`, `fk_company`, `fk_headquarter`, `fk_group`, `is_blocked`, `created_at`, `updated_at`, `remember_token`) VALUES
(12, 1, 1, 'Laura', 'Bot', 'laura', 'laura.bot2489@gmail.com', '$2y$10$upZQIMw00uSWczs6QJu/IeZG3YuZJ2RlQQSpbgwW3Q62Y2AbpgZBu', 1, '702eb07928ed7b84626b', NULL, '2015-06-04 14:31:05', 'en', 9, 11, 2, 1, '2015-06-02 04:34:01', '2015-06-04 11:31:12', 'JARSuBUhsH02mm1b8b7OTYvVMOWIuJAWHC5ZgDGCeLW8k72pmjNBQvryquoI'),
(13, 1, 1, 'Test', 'Last', 'test', 'laura.diosoft@yahoo.com', '$2y$10$IcahUFtZgURzB8JHANXxJeo7CLmZKfu/DfiourviV93lHcu41rJz6', 1, '86d081884c7d659a2fea', '2015-06-02 07:54:19', '2015-06-05 10:04:18', 'en', 10, 13, 2, 1, '2015-06-02 04:37:31', '2015-06-05 07:04:18', 'u5HNKjgW9BD3GnoCG4dfawZqbSPnmh5e430bFXdu4k73ukeuHUAYUgbtM7qy'),
(14, 2, NULL, 'Test', 'Test', 'test1', 'test1234@yahoo.com', '$2y$10$JLGspncpoOGRtNkSU9ynzOeOuih/wswwqxiePJo1O872cSRcI8yri', 1, '4f0e9851971998e73207', NULL, NULL, 'en', 11, 14, 2, 1, '2015-06-02 11:10:39', '2015-06-02 11:11:44', 'NfkXtCZJnZPtUQfWUKIzLJv1z6WUVhNulMEeqMmKxL1LyTZz3qSvdXdKGy0Z'),
(15, 0, 1, 'Laura', 'Bot', 'laurabot', 'laura_2489@yahoo.com', '$2y$10$8fi83arO9i0cupVUzD/Zf.CcIDmHCjLBxt7yO6cmkqafKlc3mPOFW', 1, '77536b34cce5317ff86e', '2015-06-04 08:29:21', '2015-06-04 14:02:01', 'en', 10, 12, 3, 1, '2015-06-04 05:29:03', '2015-06-04 11:02:08', 'muxxyAmNpCTPDgrUPUD4fqMkVNh7vcLAkL59EQgUJPPYBRkxjzDYEX5bbgg4'),
(16, 2, 1, 'Vasile', 'Pop', 'vasile', 'email@email.ro', '$2y$10$Rnqyl3ODobkUQyXG6/3lKOV4Tc7W0AMJkIC6JIKrtc6Zc/pPtmUNO', 1, NULL, NULL, NULL, 'en', 10, 12, 3, 1, '2015-06-04 07:02:56', '2015-06-04 07:15:21', NULL),
(17, 0, 0, 'Test', 'Pop', 'nr_user', 'dsasdsad@fgdgdsg.cy', '$2y$10$1G1tBUVoMkZedx1U7ezQBulasWsQ.rkGP.KQjiJDWYNFqI4gj5UR2', 0, NULL, NULL, NULL, 'en', 10, 12, 3, 1, '2015-06-04 10:10:17', '2015-06-04 10:10:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_personaltitle`
--

CREATE TABLE IF NOT EXISTS `users_personaltitle` (
`id_personaltitle` int(11) NOT NULL,
  `name_personaltitle` varchar(100) COLLATE utf8_bin NOT NULL,
  `description_personaltitle` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users_personaltitle`
--

INSERT INTO `users_personaltitle` (`id_personaltitle`, `name_personaltitle`, `description_personaltitle`, `created_at`, `updated_at`) VALUES
(0, 'Select', NULL, NULL, NULL),
(1, 'Miss', 'an unmarried female', NULL, NULL),
(2, 'Mrs', 'a married female', NULL, NULL),
(3, 'Mr', 'a man', NULL, NULL),
(4, 'Ms', 'a woman who perhaps you don''t kow if she is married, or some woman prefer to be called this anyway', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_title`
--

CREATE TABLE IF NOT EXISTS `users_title` (
`id_user_title` int(11) NOT NULL,
  `name_title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description_title` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users_title`
--

INSERT INTO `users_title` (`id_user_title`, `name_title`, `description_title`, `created_at`, `updated_at`) VALUES
(0, 'Select', NULL, NULL, NULL),
(1, 'Prof', NULL, NULL, NULL),
(2, 'Dr', NULL, NULL, NULL),
(3, 'Ing', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
 ADD PRIMARY KEY (`id_action`);

--
-- Indexes for table `affiliation`
--
ALTER TABLE `affiliation`
 ADD PRIMARY KEY (`id_affiliation`), ADD KEY `fk_company` (`fk_company`);

--
-- Indexes for table `affiliation_company`
--
ALTER TABLE `affiliation_company`
 ADD PRIMARY KEY (`id_affiliation_company`), ADD KEY `fk_company` (`fk_company`), ADD KEY `fk_affiliation` (`fk_affiliation`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
 ADD PRIMARY KEY (`id_company`);

--
-- Indexes for table `company_headquarters`
--
ALTER TABLE `company_headquarters`
 ADD PRIMARY KEY (`id_headquarter`), ADD KEY `fk_company` (`fk_company`), ADD KEY `fk_location` (`fk_location`);

--
-- Indexes for table `company_industries`
--
ALTER TABLE `company_industries`
 ADD PRIMARY KEY (`id_company_industry`), ADD KEY `fk_headquarter` (`fk_headquarter`), ADD KEY `fk_industry` (`fk_industry`);

--
-- Indexes for table `dictionary`
--
ALTER TABLE `dictionary`
 ADD PRIMARY KEY (`id_dictionary`);

--
-- Indexes for table `dictionary_positions`
--
ALTER TABLE `dictionary_positions`
 ADD PRIMARY KEY (`id_position`), ADD KEY `fk_dictionary` (`fk_dictionary`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `industry_type`
--
ALTER TABLE `industry_type`
 ADD PRIMARY KEY (`id_industry_type`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
 ADD PRIMARY KEY (`id_location`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
 ADD PRIMARY KEY (`id_migration`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
 ADD PRIMARY KEY (`id_throttle`), ADD KEY `fk_user_id` (`fk_user`);

--
-- Indexes for table `user_actions`
--
ALTER TABLE `user_actions`
 ADD PRIMARY KEY (`id_user_action`), ADD KEY `fk_user` (`fk_user`), ADD KEY `fk_action` (`fk_action`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id_user`), ADD UNIQUE KEY `users_username_unique` (`username`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `fk_company` (`fk_company`), ADD KEY `fk_headquarter` (`fk_headquarter`), ADD KEY `fk_group` (`fk_group`), ADD KEY `personal_title` (`fk_personal_title`), ADD KEY `title` (`fk_title`);

--
-- Indexes for table `users_personaltitle`
--
ALTER TABLE `users_personaltitle`
 ADD PRIMARY KEY (`id_personaltitle`);

--
-- Indexes for table `users_title`
--
ALTER TABLE `users_title`
 ADD PRIMARY KEY (`id_user_title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
MODIFY `id_action` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `affiliation`
--
ALTER TABLE `affiliation`
MODIFY `id_affiliation` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `affiliation_company`
--
ALTER TABLE `affiliation_company`
MODIFY `id_affiliation_company` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
MODIFY `id_company` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `company_headquarters`
--
ALTER TABLE `company_headquarters`
MODIFY `id_headquarter` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `company_industries`
--
ALTER TABLE `company_industries`
MODIFY `id_company_industry` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dictionary`
--
ALTER TABLE `dictionary`
MODIFY `id_dictionary` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `industry_type`
--
ALTER TABLE `industry_type`
MODIFY `id_industry_type` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
MODIFY `id_location` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
MODIFY `id_migration` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
MODIFY `id_throttle` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_actions`
--
ALTER TABLE `user_actions`
MODIFY `id_user_action` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id_user` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users_personaltitle`
--
ALTER TABLE `users_personaltitle`
MODIFY `id_personaltitle` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_title`
--
ALTER TABLE `users_title`
MODIFY `id_user_title` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `affiliation`
--
ALTER TABLE `affiliation`
ADD CONSTRAINT `affiliation_ibfk_1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `affiliation_company`
--
ALTER TABLE `affiliation_company`
ADD CONSTRAINT `affiliation_company_ibfk_1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `affiliation_company_ibfk_2` FOREIGN KEY (`fk_affiliation`) REFERENCES `affiliation` (`id_affiliation`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `company_headquarters`
--
ALTER TABLE `company_headquarters`
ADD CONSTRAINT `company_headquarters_ibfk_1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id_company`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `company_headquarters_ibfk_2` FOREIGN KEY (`fk_location`) REFERENCES `locations` (`id_location`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `dictionary_positions`
--
ALTER TABLE `dictionary_positions`
ADD CONSTRAINT `dictionary_positions_ibfk_1` FOREIGN KEY (`fk_dictionary`) REFERENCES `dictionary` (`id_dictionary`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_actions`
--
ALTER TABLE `user_actions`
ADD CONSTRAINT `user_actions_ibfk_1` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `user_actions_ibfk_2` FOREIGN KEY (`fk_action`) REFERENCES `actions` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`fk_company`) REFERENCES `companies` (`id_company`),
ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`fk_headquarter`) REFERENCES `company_headquarters` (`id_headquarter`),
ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`fk_title`) REFERENCES `users_title` (`id_user_title`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`fk_group`) REFERENCES `groups` (`id_group`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`fk_personal_title`) REFERENCES `users_personaltitle` (`id_personaltitle`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
