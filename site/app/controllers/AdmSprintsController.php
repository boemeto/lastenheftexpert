<?php

class AdmSprintsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //        'name_sprint', 'fk_release', 'due_date', 'comment', 'is_closed'
        $sprint = new Adm_sprint();
        $sprint->name_sprint = Input::get('name_sprint');
        $sprint->fk_release = Input::get('fk_release');
     
        $sprint->comment = Input::get('comment');
        $sprint->save();

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $sprint = Adm_sprint::getFirstById($id);
        $sprint->name_sprint = Input::get('name_sprint');
   
        $sprint->comment = Input::get('comment');
        $sprint->is_closed = Input::get('is_closed');
        $sprint->save();
        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function adm_tasks_dialog_add_sprint($id_release)
    {
        return View::make('admin.adm_task.modal_dialog_add_sprint', compact('id_release'));
    }

    public function adm_tasks_dialog_edit_sprint($id_sprint)
    {
        $sprint = Adm_sprint::getFirstById($id_sprint);
        return View::make('admin.adm_task.modal_dialog_edit_sprint', compact('sprint'));
    }


}
