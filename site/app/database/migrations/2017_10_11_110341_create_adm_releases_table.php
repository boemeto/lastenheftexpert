<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmReleasesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_releases', function ($table) {
            $table->increments('id_release')->unsigned();
            $table->string('name_release', 250);
            $table->tinyInteger('is_completed');
            $table->tinyInteger('is_current');
            $table->date('start_date');
            $table->float('budget');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_releases');
    }
}