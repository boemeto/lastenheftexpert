<?php
    $headquarters[0] = 'Standort auswählen';
    ksort($headquarters);
?>
{{ $errors->first("id_headquarter[1]","<div class='text-red'>:message</div>") }}
<div class="form-group">
    {{ Form::select("id_headquarter[1]",$headquarters,0,[  "id"=>'id_headquarter', "class"=>"form-control" ,"onChange"=>" viewHeadquarterDetails(this.value)"]) }}
    {{ $errors->first("id_headquarter","<div class='text-red'>:message</div>") }}

    <div class='text-red' id='errorid_headquarter'></div>
</div>
<div id="divViewHeadquarterDetails">
</div>
