<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title">{{trans('messages.act_edit')}} {{trans('messages.nav_posts_pages')}}</h3>

    <div class="clear"></div>
</div>
{{ Form::model($post,['route'=>['packs_row.update',$list->id_pack_row], 'method'=>'PATCH'])}}

<div class="modal-body">
    <div class="row pd-10">
        <div class="col-xs-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_field',  $list->getAttribute('name_field','de'), [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">DE</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_pack', $list->getAttribute('description_pack','de'), ['class'=>"summernote form-control",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
        <div class="col-xs-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_field_en', $list->getAttribute('name_field','en'), [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">EN</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_pack_en', $list->getAttribute('description_pack','en'), ['class'=>"form-control summernote",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
    </div>
    <div class=" row pd-10">
        <div class="col-xs-6 pd-5">
            <label>{{trans('messages.label_font_icon')}}</label>
            {{ Form::text('fa_icon',$list->fa_icon, ['class'=>"form-control",'placeholder'=>trans('messages.label_font_icon')]) }}
        </div>
    </div>
    <br>
</div>
<div class="modal-footer">
    <button type="submit" class='btn ls-light-blue-btn'>
        {{trans('messages.act_save_close')}}
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}
