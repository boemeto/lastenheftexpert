<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 27.05.2015
 * Time: 12:43
 */
class Users_title extends Eloquent
{


    public static $rules = [
        'name_title' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_user_title';
    protected $table = 'users_title';
    protected $guarded = ['id_user_title'];
    protected $fillable = ['name_title', 'description_title'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getTitle()
    {
        $array = array();
        $lists = Users_title::select('*')->get();

        foreach ($lists as $list) {
            $array[$list->id_user_title] = $list->name_title;
        }
        return $array;
    }

    public static function getUserTitle($id_user)
    {
        $fk_title = User::getFirstById($id_user)->fk_title;
        $title = self::getFirstById($fk_title);
        if (count($title) > 0 && $fk_title > 0) {
            $name_title = $title->name_title;
        } else {
            $name_title = '';
        }

        return $name_title;
    }

    public static function getFirstById($id)
    {
        $list = self::where('id_user_title', '=', $id)->first();
        return $list;
    }


}