<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTableSeeder
 *
 * @author Raul Daniel Popa
 */
class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert(
            array(
                'email' => 'admin@admin.com',
                'password' => Hash::make('1q2w3e4r'),
                'activated' => '1',
                'fk_company' => '1',
                'fk_headquarter' => '1',
                'fk_personal_title' => '1',
                'fk_title' => '1',
            )
        );
    }
}
