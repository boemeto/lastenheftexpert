<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Messages Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the template library to build
      | the buttons and other messages. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */
    // title
    'title_long' => 'Lastenheft Expert',
    'title_short' => 'Lastenheft',

    // actions
    'act_permanently_delete' => 'Delete permanently',
    'act_add' => 'Add',
    'act_delete' => 'Delete',
    'act_undelete' => 'Show',
    'act_link' => 'Link',
    'act_edit' => 'Modify',
    'act_modify' => 'Modify',
    'act_details' => 'Details',
    'Yes' => 'Ja',
    'No' => 'Nein',
    'language' => 'Language',

    // navigation
    'nav_dashboard' => 'Dashboard',
    'nav_software' => 'Software',
    'nav_industry' => 'Industrie',
    'nav_industries' => 'Industries',
    'nav_settings' => 'Settings',
    'nav_module' => 'Module',
    'nav_submodule' => 'Submodule',
    'nav_task' => 'Task',
    'nav_new_project' => 'Add new project',
    'nav_companies' => 'Companies',
    'nav_company' => 'Company',
    'nav_users' => 'Users',
    'nav_logout' => 'Logout',
    'nav_task_manager' => 'Task Manager',
    'nav_edit_profile' => 'Edit Profile',
    'nav_change_password' => 'Change password',
    'nav_profile' => 'Profile',
    'nav_lists' => 'Lists',


    // table headers
    'th_id' => 'Identifier',
    'th_name' => 'Name',
    'th_description' => 'Description',
    'th_type' => 'Type',
    'th_is_deleted' => 'Is deactivated',

    // form labels
    'l_industry_type' => 'Industry type',
    'l_name' => 'Name',
    'l_description' => 'Description',
    'l_german' => '(in german language)',

);
?>