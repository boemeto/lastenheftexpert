<?php

class InvoiceAdmController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $invoices = Invoice::getInvoicesAdmin();
        $invoice_status = Invoice_status::all();
        return View::make('admin.invoices.index', compact('invoices', 'invoice_status'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();
        $invoice = Invoice::getFirstById($id);
        $company = Company::getFirstById($invoice->fk_company);
        $main_headquarter = Company_headquarters::getMainHeadquarter($invoice->fk_company);
        $project = Project::getFirstById($invoice->fk_project);
        $admin_company = Admin_company::getFirstById(1);
        $user = User::getFirstById($invoice->fk_user);
        $package_id = Projects_package::whereFk_project($project->id_project)->first();
        $package = Package::getFirstById($package_id->fk_package);
        $invoices = Invoice::getInvoicesByProject($project->id_project);
        return View::make('admin.invoices.show', compact('invoice', 'company', 'main_headquarter', 'project', 'admin_company', 'user', 'package', 'invoices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit()
    {
        // dd(Input::all());
        $id = Input::get('invoice_id');
        $status = Input::get('invoice_status');
        $invoice = Invoice::getFirstById($id);
        switch ($status) {
            case 'Open':
                $invoice->fk_status = 1;
                break;
            case 'Offen':
                $invoice->fk_status = 1;
                break;
            case 'Expired':
                $invoice->fk_status = 2;
                break;
            case 'Fällig':
                $invoice->fk_status = 2;
                break;
            case 'Paid':
                $invoice->fk_status = 3;
                break;
            case 'Bezahlt':
                $invoice->fk_status = 3;
                break;
            case 'Storno':
                $invoice->fk_status = 4;
                break;
            case 'Storno':
                $invoice->fk_status = 4;
                break;
        }
        $invoice->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
