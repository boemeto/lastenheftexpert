<?php
use Illuminate\Support\MessageBag;

class TaskController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $task = Ms_task::getNotAprovedTasks();
        $ms_tasks = Ms_task::all();
        $pr_tasks = Task::all();
        $custom_task = Dictionary::getOptions(4, 1);
        $is_default = 0;
        $industries_types = Industry_type::all();
        // $pagination = $task->links();

        return View::make('admin.task.index', compact('task', 'pagination', 'ms_tasks', 'pr_tasks', 'custom_task', 'is_default', 'industries_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Task::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(Task::$messages);
        }
        $name_task = Input::get('name_task');

        $description_task = Input::get('description_task');
        $description_task = str_replace('<p><br></p>', '', $description_task);
        $task = Task::getFirstByName($name_task, $description_task);
        if (count($task) == 0) {
            $task = new Task();
            $task->fill(Input::all());
            $task->save();
        }
        Session::flash('scrollTo', 'task'.$task->id_task);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.add_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $task = Task::getFirstById($id);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();

        return View::make('admin.task.edit', compact('task', 'priorities', 'pairs', 'industries_types', 'atachments'));
    }

    public function modal_dialog_edit_task_admin($id)
    {
        $task = Task::getFirstById($id);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        $id_ms_task = Ms_task::getFirstByfk_task($id);
        $pr_module_task = Ms_task::getOneTaskById_task($id_ms_task->id_ms_task);
        $attachments = Projects_attach::getAttachmentByTask($id_ms_task->id_ms_task, $id_ms_task->fk_project);
        Session::flash('scrollTo', 'software'.$software->id_software);
        return View::make('admin.task.modal_dialog_edit_task_admin', compact('task', 'priorities', 'pairs', 'industries_types', 'attachments', 'pr_module_task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        // echo "<pre>"; dd(Input::all());
        $task = Task::getFirstById($id);
        $id_ms_task = Input::get('id_ms_task');

        $ms_task = Ms_task::getFirstById($id_ms_task);
        if (Input::has('fk_pair')) {
            if (Input::get('fk_pair') != '0') {
                $fk_pair = Input::get('fk_pair');
                $ms_task->fk_pair = $fk_pair;
            } else {
                $ms_task->fk_pair = null;
            }
        }
        if (Input::has('lvl_priority')) {
            if (Input::get('lvl_priority') != '') {
                $lvl_priority = Input::get('lvl_priority');
                $ms_task->lvl_priority = $lvl_priority;
            }
        }

        if (Input::has('is_checked')) {
            $ms_task->is_checked = Input::get('is_checked');
        } else {
            $ms_task->is_checked = 0;
        }

        if (Input::has('is_mandatory')) {
            $ms_task->is_mandatory = Input::get('is_mandatory');
        } else {
            $ms_task->is_mandatory = 0;
        }

        $ms_task->save();
//        ind_description_task
        if (Input::has('fk_industry')) {
            $fk_industries = Input::get('fk_industry');
        } else {
            $fk_industries = 0;
        }

        if (Input::has('edit_task') && Input::get('edit_task') == 2) {
            $ms_tasks = Ms_task::where('fk_task', '=', $id)->get();
            if (count($ms_tasks) > 1) {
                $name_task = Input::get('name_task');
                $description_task = Input::get('description_task');
                $description_task = str_replace('<p><br></p>', '', $description_task);
                $task = Task::getFirstByName($name_task, $description_task);
                if (count($task) == 0) {
                    $task = new Task();
                }
                $task->fill(Input::all());
                $task->save();
                $ms_task = Ms_task::getFirstById($id_ms_task);
                $ms_task->fk_task = $task->id_task;
                $ms_task->save();
            } else {
                $task = Task::getFirstById($id);
                if (count($task) > 0) {
                    $task->fill(Input::all());
                    $task->save();
                }
            }
        } else {
            $task->fill(Input::all());
            $task->save();
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        Session::flash('scrollTo', $id_ms_task);
        return Redirect::back()->withErrors($errors);
//        return Redirect::back()->with('success_message', trans('validation.edit_success'));
    }

    public function duplicate_software_task($task_id) {
        $task = Task::getFirstById($task_id);
        $ms_task = Ms_task::getFirstByfk_task($task_id);
        return $task;
    }

    public function set_task_default()
    {
        $id_ms_task = Input::get('id_ms_task');
        $ms_task = Ms_task::getFirstById($id_ms_task);
        $current_date = date("Y-m-d H:i:s");
        if (Input::get('is_default') == 'true') {
            $ms_task->is_default = 1;
            $ms_task->is_from_industry = 1;
            $ms_task->locked_at = '0000-00-00 00:00:00';
            $ms_task->save();
        } else {
            $ms_task->is_default = 0;
            $ms_task->is_from_industry = 0;
            $ms_task->locked_at = $current_date;
            $ms_task->save();
            // dd($current_date);
            // $ms_task = Ms_task::getFirstById($id_ms_task);
            // $locked_at = strtotime($ms_task->updated_at);
            // $locked_at = date('Y-m-d H:m:s', $locked_at);
            // $ms_task->locked_at = $locked_at;
            // $ms_task->save();
        }

        return $ms_task->is_default;
    }

    public function check_admin_industry_task()
    {
        // dd(Input::all());
        $id_ms_task = Input::get('id_ms_task');
        $is_checked = Input::get('is_checked');
        $id_industry = Input::get('id_industry');

        if ($is_checked==1) {
            $ms_task = Ms_task::getFirstById($id_ms_task);
            $industries_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                ->where('fk_ms_task', '=', $id_ms_task)->first();
            if (count($industries_ms) == 0) {
                Industry_ms::createNewItem($id_industry, $id_ms_task, $ms_task->fk_module_structure);
                $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                if (count($attachments) > 0) {
                    foreach ($attachments as $attachment) {
                        $folder = $attachment->folder_attachment;
                        $directory_soft = 'images/attach_software/' . $folder . '/';
                        $filename = $attachment->name_attachment;
                        $extension = $attachment->extension;
                        Industry_attach::addAttachToSelectedIndustry($id_ms_task, $id_industry, $folder, $directory_soft, $filename, $extension);
                    }
                }
            }
            return trans('validation.industry_checked');
        } else {
            $industries_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                ->where('fk_ms_task', '=', $id_ms_task)->first();
            if (count($industries_ms) > 0) {

                $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                if (count($attachments) > 0) {
                    foreach ($attachments as $attachment) {
                        $folder = $attachment->folder_attachment;
                        $filename = $attachment->name_attachment;
                        Industry_attach::deleteAttachFromSelectedIndustry($id_ms_task, $id_industry, $filename, $folder);
                    }
                }
                $industries_ms->delete();
            }
            return trans('validation.industry_unchecked');
        }
    }

    public function check_admin_all_industry_task()
    {
        $id_ms_task = Input::get('id_ms_task');
        $is_checked = Input::get('is_checked');
        if (Input::has('category_id')) {
            $category_id = Input::get('category_id');
            $id_industries = Industry::whereFk_type($category_id)->lists('id_industry');
        } else {
            $id_industries = Industry::lists('id_industry');
        }
        $ms_task = Ms_task::getFirstById($id_ms_task);
        //  dd($ms_task);
        if ($is_checked==1) {
            foreach($id_industries as $id_industry) {
                $industries_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                    ->where('fk_ms_task', '=', $id_ms_task)->first();
                if (count($industries_ms) == 0) {
                    Industry_ms::createNewItem($id_industry, $id_ms_task, $ms_task->fk_module_structure);
                    $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                    if (count($attachments) > 0) {
                        foreach ($attachments as $attachment) {
                            $folder = $attachment->folder_attachment;
                            $directory_soft = 'images/attach_software/' . $folder . '/';
                            $filename = $attachment->name_attachment;
                            $extension = $attachment->extension;
                            Industry_attach::addAttachToSelectedIndustry($id_ms_task, $id_industry, $folder, $directory_soft, $filename, $extension);
                        }
                    }
                }
            }
            return trans('validation.all_industry_checked');
        } elseif ($is_checked==0) {
            foreach($id_industries as $id_industry) {
                $industries_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                    ->where('fk_ms_task', '=', $id_ms_task)->first();
                if (count($industries_ms) > 0) {
                    $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                    if (count($attachments) > 0) {
                        foreach ($attachments as $attachment) {
                            $folder = $attachment->folder_attachment;
                            $filename = $attachment->name_attachment;
                            Industry_attach::deleteAttachFromSelectedIndustry($id_ms_task, $id_industry, $filename, $folder);
                        }
                    }
                    $industries_ms->delete();
                }
            }
            return trans('validation.all_industry_unchecked');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $task = Task::getFirstById($id);
        $task->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.delete_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    // add new task + structure
    public function add_new_task_admin()
    {
        if (Input::has('fk_module_structure')) {
            $fk_module_structure = Input::get('fk_module_structure');
            $module_structure = Module_structure::getFirstById($fk_module_structure);
            $fk_software = $module_structure->fk_software;
        } else {
            $fk_module_structure = 0;
            $fk_software = Input::get('fk_software');
        }

        $name_task = Input::get('name_task');
        $task_description = Input::get('description_task');
        $task_description = str_replace('<p><br></p>', '', $task_description);

        $task = Task::getFirstByName($name_task, $task_description);

        if (count($task) == 0) {
            $task = new Task();
//            $task->is_default = 1;
            $task->name_task = $name_task;
            $task->name_task_en = Input::get('name_task_en');
            $task->description_task = $task_description;
            $task->description_task_en = Input::get('description_task_en');
            $task->meta_tags = Input::get('meta_tags');
            $task->code_task = Input::get('code_task');
            $task->address_code = Input::get('address_code');
            $task->save();
        }

        $fk_task = $task->id_task;

        if (Input::has('structure')) {
            $structure = Input::get('structure');

            $structure_array = explode('_', $structure);

            unset($structure_array[0], $structure_array[1], $structure_array[2]);
            $length = count($structure_array) + 2;
            $last_structure = 0;
            $level = 1;

            for ($i = 3; $i <= $length; $i += 2) {
                if ($structure_array[$i + 1] != 0) {
                    $last_structure = $structure_array[$i + 1];
                } else {
                    if ($last_structure == 0) {

                        $module = Module::where('name_module', '=', $structure_array[$i])->first();
                        if (count($module) == 0) {
                            $module = new Module();
                            $module->name_module = $structure_array[$i];
                            $module->is_default = 1;
                            $module->save();
                        }

                        $id_module = $module->id_module;

                        $structure_module = Module_structure::hasPath($fk_software, $id_module, 0, $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_module;
                            $structure_module->fk_parent = 0;
                            $structure_module->id_type = 1;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $id_module;
                            $structure_module->is_default = 1;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    } else {
                        $submodule = Submodule::where('name_submodule', '=', $structure_array[$i])->first();
                        if (count($submodule) == 0) {
                            $submodule = new Submodule();
                            $submodule->name_submodule = $structure_array[$i];
                            $submodule->is_default = 1;
                            $submodule->save();

                        }
                        $id_submodule = $submodule->id_submodule;
                        $module_parrent = Module_structure::getFirstById($last_structure);
                        $structure_module = Module_structure::hasPath($fk_software, $id_submodule, $last_structure,
                            $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_submodule;
                            $structure_module->fk_parent = $last_structure;
                            $structure_module->id_type = 2;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $module_parrent->fk_module_parent;
                            $structure_module->is_default = 1;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    }
                }
                $level++;
            }

            $fk_module_structure = $last_structure;
        }

        $ms_task = Ms_task::where('fk_task', '=', $fk_task)
            ->where('fk_module_structure', '=', $fk_module_structure)
            ->first();

        if (count($ms_task) == 0) {
            $ms_task = new Ms_task();
            $ms_task->fk_task = $fk_task;
            $ms_task->fk_module_structure = $fk_module_structure;
        }

        if (Input::has('is_default') && Input::get('is_default') == 1) {
            $ms_task->is_default = 1;
            $ms_task->locked_at = '0000-00-00 00:00:00';
        } else {
            $ms_task->is_default = 0;
            $ms_task->locked_at = date('Y-m-d H:m:s');
        }

        if (Input::has('is_checked') && Input::get('is_checked') == 1) {
            $ms_task->is_checked = 1;
        } else {
            $ms_task->is_checked = 0;
        }

        if (Input::has('is_mandatory') && Input::get('is_mandatory') == 1) {
            $ms_task->is_mandatory = 1;
        } else {
            $ms_task->is_mandatory = 0;
        }

        if (Input::has('fk_pair') && Input::get('fk_pair') > 0) {
            $ms_task->fk_pair = Input::get('fk_pair');
        }

        if (Input::has('lvl_priority')) {
            $ms_task->lvl_priority = Input::get('lvl_priority');
        }
        $ms_task->save();

        // add new task + strcture + link to industry
        if (Input::has('fk_industry')) {
            $fk_industries = Input::get('fk_industry');
            // dd($fk_industries);
            foreach ($fk_industries as $fk_industry) {
                Industry_ms::createNewItem($fk_industry, $ms_task->id_ms_task, $ms_task->fk_module_structure);
            }

            if (count($industries_ms) == 0) {
                Industry_ms::createNewItem($fk_industry, $ms_task->id_ms_task, $ms_task->fk_module_structure);
            }
        }

        $fk_ms_task =$ms_task->id_ms_task;
        $files = Input::get('files');
        // check if is file
        if ($files) {
            $folder = date('m-Y');
            foreach($files as $file) {
                $extension = end(explode('.', $file));
                $directory = 'images/attach_software/' . $folder . '/';
                $software_attachments = new Software_attach();
                $software_attachments->fk_ms_task = $fk_ms_task;
                $software_attachments->folder_attachment = $folder;
                $software_attachments->name_attachment = $file;
                $software_attachments->extension = $extension;
                $software_attachments->save();
            }
        }

        Session::flash('scrollTo', $ms_task->id_ms_task);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.add_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function add_new_task_admin_industry()
    {
        if (Input::has('fk_module_structure')) {
            $fk_module_structure = Input::get('fk_module_structure');
            $module_structure = Module_structure::getFirstById($fk_module_structure);
            $fk_software = $module_structure->fk_software;
        } else {
            $fk_module_structure = 0;
            $fk_software = Input::get('fk_software');
        }

        $name_task = Input::get('name_task');
        $task_description = str_replace('<p><br></p>', '', Input::get('description_task'));

        $task = Task::getFirstByName($name_task, $task_description);
        if (count($task) == 0) {
            $task = new Task();
//            $task->is_default = 1;
            $task->name_task = $name_task;
            $task->name_task_en = Input::get('name_task_en');
            $task->description_task = $task_description;
            $task->description_task_en = Input::get('description_task_en');
            $task->meta_tags = Input::get('meta_tags');
            $task->code_task = Input::get('code_task');
            $task->address_code = Input::get('address_code');
            $task->save();
        }

        $fk_task = $task->id_task;

        if (Input::has('structure')) {
            $structure = Input::get('structure');
            $structure_array = explode('_', $structure);

            if ($fk_software == 0) {
                if (strpos($structure_array[4], 'software') === 0) {
                    $fk_software = str_replace('software', '', $structure_array[4]);
                }
            }
            unset($structure_array[0], $structure_array[1], $structure_array[2], $structure_array[3], $structure_array[4]);
            // length + unseted items length
            $length = count($structure_array) + 4;
            $last_structure = 0;
            $level = 1;

            for ($i = 5; $i <= $length; $i += 2) {
                if ($structure_array[$i + 1] != 0) {
                    $last_structure = $structure_array[$i + 1];
                } else {
                    if ($last_structure == 0) {
                        $module = Module::where('name_module', '=', $structure_array[$i])->first();
                        if (count($module) == 0) {
                            $module = new Module();
                            $module->name_module = $structure_array[$i];
                            $module->is_default = 1;
                            $module->save();
                        }

                        $id_module = $module->id_module;

                        $structure_module = Module_structure::hasPath($fk_software, $id_module, 0, $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_module;
                            $structure_module->fk_parent = 0;
                            $structure_module->id_type = 1;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $id_module;
                            $structure_module->is_default = 1;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    }
                    else {
                        $submodule = Submodule::where('name_submodule', '=', $structure_array[$i])->first();
                        if (count($submodule) == 0) {
                            $submodule = new Submodule();
                            $submodule->name_submodule = $structure_array[$i];
                            $submodule->is_default = 1;
                            $submodule->save();
                        }
                        $id_submodule = $submodule->id_submodule;
                        $module_parrent = Module_structure::getFirstById($last_structure);
                        $structure_module = Module_structure::hasPath($fk_software, $id_submodule, $last_structure,
                            $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_submodule;
                            $structure_module->fk_parent = $last_structure;
                            $structure_module->id_type = 2;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $module_parrent->fk_module_parent;
                            $structure_module->is_default = 1;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    }
                }
                $level++;
            }
            $fk_module_structure = $last_structure;
        }

        $ms_task = Ms_task::where('fk_task', '=', $fk_task)
            ->where('fk_module_structure', '=', $fk_module_structure)
            ->first();

        if (count($ms_task) == 0) {
            $ms_task = new Ms_task();
            $ms_task->is_from_industry = 1;
            $ms_task->fk_task = $fk_task;
            $ms_task->fk_module_structure = $fk_module_structure;
        }
        if (Input::has('is_default') && Input::get('is_default') == 1) {
            $ms_task->is_default = 1;
            $ms_task->locked_at = '0000-00-00 00:00:00';
        } else {
            $ms_task->is_default = 0;
            $ms_task->locked_at = date('Y-m-d H:m:s');
        }
        if (Input::has('is_checked') && Input::get('is_checked') == 1) {
            $ms_task->is_checked = 1;
        } else {
            $ms_task->is_checked = 0;
        }
        if (Input::has('is_mandatory') && Input::get('is_mandatory') == 1) {
            $ms_task->is_mandatory = 1;
        } else {
            $ms_task->is_mandatory = 0;
        }
        if (Input::has('fk_pair') && Input::get('fk_pair') > 0) {
            $ms_task->fk_pair = Input::get('fk_pair');
        } else {
            $ms_task->fk_pair = null;
        }
        if (Input::has('lvl_priority')) {
            $ms_task->lvl_priority = Input::get('lvl_priority');
        }
        $ms_task->save();

        // add new task + strcture + link to industry
        if (Input::has('fk_industries')) {
            $fk_industries = Input::get('fk_industries');
            foreach ($fk_industries as $fk_industry) {
                Industry_ms::createNewItem($fk_industry, $ms_task->id_ms_task, $ms_task->fk_module_structure);
            }
        }

        $files = Input::get('files');
        // check if is file
        if ($files) {
            $folder = date('m-Y');
            $fk_industry_ms = $ms_task->id_ms_task;
            foreach($files as $file) {
                $extension = end(explode('.', $file));
                $directory = 'images/attach_software/' . $folder . '/';
                // $industry_attachment = new Industry_attach();
                // $industry_attachment->fk_industry_ms = $fk_industry_ms;
                // $industry_attachment->folder_attachment = $folder;
                // $industry_attachment->name_attachment = $file;
                // $industry_attachment->extension = $extension;
                // $industry_attachment->save();
                $filename = $file;
                Industry_attach::addAttachToAllIndustry($fk_industry_ms, $folder, $directory, $filename, $extension);
                // Industry_attach::addAttachToSelectedIndustry($fk_industry_ms, $fk_industry, $folder, $directory, $filename, $extension);
            }
        }

        Session::flash('scrollTo', $ms_task->id_ms_task);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.add_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function modal_dialog_edit_task($id_ms_task)
    {
        $pr_module_task = Ms_task::getOneTaskById_task($id_ms_task);
        $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        $task = Task::getFirstById($pr_module_task->id_task);

        return View::make('admin.software.modal_dialog_edit_task', compact('industries_types', 'task', 'priorities', 'pairs', 'pr_module_task', 'id_ms_task', 'attachments'));
    }

    public function modal_dialog_edit_task_industry($id_ms_task, $fk_industry)
    {
        $pr_module_task = Ms_task::getOneTaskById_task($id_ms_task);
        $ind_ms_task = Industry_ms::getFirstByKey($id_ms_task, $fk_industry);
        $attachments = Industry_attach::whereFk_industry_ms($ind_ms_task->id_industry_ms)->get();
        // $attachments = DB::table('pr_industry_ms')
        //         ->join('pr_software_attachments', 'pr_software_attachments.fk_ms_task', '=', 'pr_industry_ms.fk_ms_task')
        //         ->join('pr_industry_attachments', 'pr_industry_attachments.fk_industry_ms', '=', 'pr_industry_ms.id_industry_ms')
        //         ->where('pr_industry_attachments.fk_industry_ms', '=', $ind_ms_task->id_industry_ms)
        //         ->get();
        // dd($attachments);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        $task = Task::getFirstById($pr_module_task->id_task);

        return View::make('admin.industries.modal_dialog_edit_task', compact('industries_types', 'task', 'priorities', 'pairs', 'pr_module_task', 'id_ms_task', 'ind_ms_task', 'fk_industry', 'attachments'));
    }

    public function modal_dialog_edit_project_task_industry($id_ms_task, $fk_industry, $id_project)
    {
        $pr_module_task = Ms_task::getOneTaskById_task($id_ms_task);
        $ind_ms_task = Industry_ms::getFirstByKey($id_ms_task, $fk_industry);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        $task = Task::getFirstById($pr_module_task->id_task);
        $project = Project::getFirstById($id_project);
        $fk_industry = $project->fk_industry;
        $pr_module_task_by_key = Ms_task::getOneTaskByKey($id_ms_task, $id_project);
        $pr_module_task_project = Ms_task::getOneTaskByKey($id_ms_task, $id_project);
        $attachments = Projects_attach::getAttachmentByTask($id_ms_task, $id_project);
        $attachments_industry = Industry_attach::getAttachmentByTask($id_ms_task, $fk_industry);
        return View::make('admin.industries.modal_dialog_edit_project_task', compact('pr_module_task_project', 'pr_module_task_by_key' ,'id_project', 'project',
                          'attachments_industry', 'industries_types', 'task', 'priorities', 'pairs', 'pr_module_task', 'id_ms_task', 'ind_ms_task', 'fk_industry', 'attachments'));
    }

    //software
    public function modal_dialog_add_task($id_module_structure)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);
        $name_module = Module::getNameByMS($pr_module_structure->fk_id, $pr_module_structure->id_type);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();

        return View::make('admin.software.modal_dialog_add_task', compact('pairs', 'priorities', 'name_module', 'pr_module_structure', 'id_module_structure', 'industries_types'));
    }

    //industry
    public function modal_dialog_add_task_industry($id_module_structure, $id_industry)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);
        $name_module = Module::getNameByMS($pr_module_structure->fk_id, $pr_module_structure->id_type);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();

        return View::make('admin.industries.modal_dialog_add_task', compact('pairs', 'priorities', 'name_module', 'pr_module_structure', 'id_module_structure', 'id_industry', 'industries_types'));
    }

    //software
    public function modal_dialog_add_task_new($id_module_structure, $id_software)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        return View::make('admin.software.modal_dialog_add_task_new', compact('pr_module_structure', 'pairs', 'priorities', 'id_module_structure', 'industries_types', 'id_software'));
    }

    //industry
    public function modal_dialog_add_task_new_industry($id_module_structure, $id_software, $id_industry)
    {
        $priorities = Priority::all();
        $pairs = Task_pair::all();
        $industries_types = Industry_type::all();
        return View::make('admin.industries.modal_dialog_add_task_new', compact('pairs', 'priorities', 'id_module_structure', 'id_software', 'id_industry', 'industries_types'));
    }

    public function delete_task_from_structure($id_ms_task)
    {
        $projects_rows = Projects_rows::where('fk_ms_task', '=', $id_ms_task)->first();
        if (count($projects_rows) == 0) {
            Industry_ms::where('fk_ms_task', '=', $id_ms_task)->delete();
            $task = Ms_task::getFirstById($id_ms_task);
            $task->delete();
            $errors = new MessageBag(['confirm_' => [Lang::get('validation.delete_success')]]);
        } else {
            $errors = new MessageBag(['error_' => [Lang::get('validation.task_in_project')]]);
        }
        return Redirect::back()->withErrors($errors);
    }

    public function set_order_adm_industry_task()
    {
        $list_order = Input::get('list_order');
        $fk_industry = Input::get('fk_industry');

        $list = explode(',', $list_order);
        $i = 1;
        foreach ($list as $id) {
            $task = Industry_ms::where('fk_ms_task', '=', $id)
                ->where('fk_industry', '=', $fk_industry)
                ->first();

            if (count($task) > 0) {
                $task->ind_order = $i;
                $task->save();
                $i++;
            }
        }
        return $list_order;
    }

    public function set_order_adm_task()
    {
        $list_order = Input::get('list_order');
        $list = explode(',', $list_order);
        $i = 1;
        foreach ($list as $id) {
            $task = Ms_task::getFirstById($id);
            $task->task_order = $i;
            $task->save();
            $i++;
        }
        return $list_order;
    }

    public function approve_tasks($id)
    {
        $user->email = 'cimpanbogdantest@gmail.com';
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $notAprovedTasks = Ms_task::where('is_default', '=', 0)->get();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        Mail::send('emails.tasks', ['user' => $user, 'data' => $notAprovedTasks], function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Tasks list');
        });
        return Redirect::back()->withErrors($errors);
    }
}
