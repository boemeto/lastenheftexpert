$(document).on('ifChecked', ".task_check", function (event) {
    if($(".select_industry input:checked").length == $(".industry-container").length) {
        $('.check-all-icon').hide();
        $('.check-all-icon-inactive').show();
        $('.uncheck-all-icon').show();
        $('.uncheck-all-icon-inactive').hide();
    }

    if($(".select_industry input:checked").length > 0 && $(".select_industry input:checked").length < $(".industry-container").length) {
        $('.check-all-icon').show();
        $('.check-all-icon-inactive').hide();
        $('.uncheck-all-icon').show();
        $('.uncheck-all-icon-inactive').hide();
    }

    var category_inputs = $(this).parents('.tab-pane').find('.industry-container').length;
    var selected_inputs = $(this).parents('.tab-pane').find('input:checked').length;

    if(category_inputs == selected_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').hide();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }

    if(selected_inputs > 0 && selected_inputs < category_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }

    var id = jQuery(this).attr('id');
    var url = jQuery(this).attr('data-url');
    var id_nr = id.replace('task_check_', "");
    var arr = id_nr.split('_');
    var data = {
        'id_industry': arr[0],
        'id_ms_task': arr[1],
        'is_checked': 1
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
           // alert(xhr.responseText);
        });
});

// edit task - remove task from an industry (on check with ajax)
$(document).on("ifUnchecked", ".task_check", function () {
    if($(".select_industry input:checked").length == 0) {
        $('.uncheck-all-icon').hide();
        $('.uncheck-all-icon-inactive').show();
        $('.check-all-icon').show();
        $('.check-all-icon-inactive').hide();
    }

    if($(".select_industry input:checked").length > 0 && $(".select_industry input:checked").length < $(".industry-container").length) {
        $('.check-all-icon').show();
        $('.check-all-icon-inactive').hide();
        $('.uncheck-all-icon').show();
        $('.uncheck-all-icon-inactive').hide();
    }

    var category_inputs = $(this).parents('.tab-pane').find('.industry-container').length;
    var selected_inputs = $(this).parents('.tab-pane').find('input:checked').length;

    if(selected_inputs == 0) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').show();
    }

    if(selected_inputs > 0 && selected_inputs < category_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }

    var id = jQuery(this).attr('id');
    var url = jQuery(this).attr('data-url');
    var id_nr = id.replace('task_check_', "");
    var arr = id_nr.split('_');
    var data = {
        'id_industry': arr[0],
        'id_ms_task': arr[1],
        'is_checked': 0
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
            // alert(xhr.responseText);
        });
});

$('.button_delete_task').on('click', function(e) {
      e.preventDefault();
      var title = $(this).attr('data-title');
      var msg = $(this).attr('data-message');
      var dataForm = $(this).attr('data-form');
      var url = $(this).attr('href');

    $('#formConfirm').find('#frm_body').html(msg).end()
                     .find('#frm_title').html(title).end()
                     .modal('show');

    $('#formConfirm').find('#frm_submit').attr('href', url);
});

$('.button_delete_file').on('click', function(e) {
     e.preventDefault();
     $('.confirm_button_delete_file').removeClass('disabled');
     var data_id = $(this).attr('id');

      //var dataForm = $(this).attr('data-form');
     var url = $(this).attr('href');
     var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
     var msg = "Delete attachment " + title + "?";

     $('#formConfirm')
      .find('#frm_body').html(msg)
      .end().modal('show');

     $('#frm_submit').addClass('confirm_button_delete_file').attr('data_id', data_id);

     $('.confirm_button_delete_file').click(function (event) {
        var id = jQuery(this).attr('data_id');
        var id_nr = id.replace('button_delete_file', '');
        var data = {
            'id_software_attachment': id_nr
        };
        jQuery.post('/remove_file_software', data)
                .done(function (msg) {
                    if (parseInt(msg) > 0) {
                        var attached_files, new_val, id_ms_task;
                        id_ms_task = parseInt(msg);
                        attached_files = $('#attachment_counter' + id_ms_task).html();
                        new_val = parseInt(attached_files) - 1;
                        $('#attachment_counter' + id_ms_task).html(new_val);
                    }
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                });
       jQuery('#col-attachments' + id_nr).hide();
       $('#formConfirm').modal('hide');
    });
});

$('.attachment-label').on('click', function() {
    if($(this).hasClass('is_closed')) {
        var task_margin_bottom = parseInt($(this).attr('data-task-margin')) + 50;
        $('#task_description').css('margin-bottom', task_margin_bottom+'px');
        $(this).removeClass('is_closed');
    } else {
        var heightAttachments = $('.attachment-label').parent().find('article:first').height();
        $('#task_description').css('margin-bottom','50px');
        $(this).attr('data-task-margin', heightAttachments);
        $(this).addClass('is_closed');
    }
});

$('.description-label').on('click', function() {
    if($(this).hasClass('is_closed')) {
        var task_margin_bottom = parseInt($(this).attr('data-task-margin')) ;
        $('#task_description').css('margin-bottom', task_margin_bottom+'0px');
        $(this).removeClass('is_closed');
        var attachmentHeight = $('.attachment-section').height();
        setTimeout(function() {
             $('.attachment-section').css('top', $('#task_description').outerHeight());
                var attachmentHeight = $('.attachment-section').height();
                $('#task_description').css('margin-bottom', attachmentHeight+'px');
        }, 30);
    } else {
        $('#task_description').css('margin-bottom','0px');
        $(this).addClass('is_closed');
        setTimeout(function() {
            var topHeight = $('#task_description').outerHeight() + 5;
            $('.attachment-section').css('top', topHeight);
            var attachmentHeight = $('.attachment-section').height();
            $('#task_description').css('margin-bottom', attachmentHeight+'px');
        }, 30);
    }
});

$(document).ready(function() {
    if($(".select_industry input:checked").length == $(".industry-container").length) {
        $('.check-all-icon').hide();
        $('.check-all-icon-inactive').show();
    }

    if ($(".select_industry input:checked").length == 0) {
        $('.uncheck-all-icon').hide();
        $('.uncheck-all-icon-inactive').show();
    }
});

$(document).on('click', '.check-all-icon', function(e) {
    e.preventDefault();
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var taskId = $(this).attr('data-task-id');
    var url = $(this).attr('data-url');

    $('#modalConfirm').find('#frm_body').html(msg).end()
                     .find('#frm_title').html(title).end()
                     .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                     .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                     .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                     .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                     .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                     .find('.modal-confirm-btn a').addClass('modal-check-all-icon').end()
                     .modal('show');

    $('.modal-check-all-icon').attr('data-task-id', taskId).attr('data-url', url);
});

$(document).on('click', '.modal-check-all-icon',function(){
    $('.check-all-icon').hide();
    $('.uncheck-all-icon-inactive').hide();
    $('.check-all-icon-inactive').show();
    $('.uncheck-all-icon').show();

    $(".select_industry div.icheckbox_minimal-green").each(function(){
        $(this).addClass('checked');
        $(this).find('input').prop('checked', true);
    });

    $(".select_industry div.tab-pane").each(function(){
        $(this).find('.add-check-all-type').hide();
        $(this).find('.add-uncheck-all-type').show();
        $(this).find('.add-uncheck-all-type-inactive').hide();
        $(this).find('.add-check-all-type-inactive').show();
    });
    var taskId = $(this).attr('data-task-id');
    var url = $(this).attr('data-url');
    var data = {
        'id_ms_task': taskId,
        'is_checked': 1
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
           // alert(xhr.responseText);
        });
  $('#modalConfirm').modal('hide');
});

$(document).on('click', '.uncheck-all-icon', function(e){
    e.preventDefault();
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var url = $(this).attr('data-url');
    var taskId = $(this).attr('data-task-id');

    $('#modalConfirm').find('#frm_body').html(msg).end()
                     .find('#frm_title').html(title).end()
                     .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                     .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                     .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                     .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                     .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                     .find('.modal-confirm-btn a').addClass('modal-uncheck-all-icon').end()
                     .modal('show');

    $('.modal-uncheck-all-icon').attr('data-task-id', taskId)
                                .attr('data-url', url);
});

$(document).on('click', '.modal-uncheck-all-icon',function(){
    $('.uncheck-all-icon').hide();
    $('.check-all-icon-inactive').hide();
    $('.uncheck-all-icon-inactive').show();
    $('.check-all-icon').show();

    $(".select_industry div.icheckbox_minimal-green").each(function(){
        $(this).removeClass('checked');
        $(this).find('input').prop('checked', false);
    });

    $(".select_industry div.tab-pane").each(function(){
        $(this).find('.add-check-all-type').show();
        $(this).find('.add-uncheck-all-type').hide();
        $(this).find('.add-uncheck-all-type-inactive').show();
        $(this).find('.add-check-all-type-inactive').hide();
    });

    var taskId = $(this).attr('data-task-id');
    var url = $(this).attr('data-url');

    var data = {
        'id_ms_task': taskId,
        'is_checked': 0
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
           // alert(xhr.responseText);
        });
    $('#modalConfirm').modal('hide');
});

$(document).on('click', '.add-check-all-type', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var categoryId = $(this).attr('data-category-id');
    var url = $(this).attr('data-url');
    var taskId = $(this).attr('data-task-id');
    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').addClass('modal-check-all-type').end()
                      .modal('show');
    $('.modal-check-all-type').attr('data-category-id', categoryId)
                              .attr('data-url', url)
                              .attr('data-task-id', taskId);
});

$(document).on('click', '.modal-check-all-type', function(){
    $('.uncheck-all-icon').show();
    $('.uncheck-all-icon-inactive').hide();

    var catId = $(this).attr('data-category-id');
    var category_unchecked_inputs = $('#ind' + catId).find('.industry-container input:not(:checked)').length;
    var all_unchecked_inputs = $(".select_industry input:not(:checked)").length;

    if(category_unchecked_inputs == all_unchecked_inputs) {
        $('.check-all-icon').hide();
        $('.check-all-icon-inactive').show();
    }

    $('#ind' + catId).find('.add-check-all-type').hide();
    $('#ind' + catId).find('.add-uncheck-all-type').show();
    $('#ind' + catId).find('.add-uncheck-all-type-inactive').hide();
    $('#ind' + catId).find('.add-check-all-type-inactive').show();
    $('#ind' + catId).find(".industry-container div.icheckbox_minimal-green").each(function(){
        $(this).addClass('checked');
        $(this).find('input').prop('checked', true);
    });
    $('#modalConfirm').find('.modal-confirm-btn a').removeClass('.modal-add-check-all-type');
    $('#modalConfirm').modal('hide');

    var taskId = $(this).attr('data-task-id');
    var categoryId = $(this).attr('data-category-id');
    var url = $(this).attr('data-url');
    var data = {
        'id_ms_task': taskId,
        'category_id': categoryId,
        'is_checked': 1
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
         // alert(xhr.responseText);
        });
});

$(document).on('click', '.add-uncheck-all-type', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var categoryId = $(this).attr('data-category-id');
    var url = $(this).attr('data-url');
    var taskId = $(this).attr('data-task-id');

    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').addClass('modal-uncheck-all-type').end()
                      .modal('show');

   $('.modal-uncheck-all-type').attr('data-category-id', categoryId)
                               .attr('data-url', url)
                               .attr('data-task-id', taskId);
});

$(document).on('click', '.modal-uncheck-all-type', function(){
    $('.add-check-all-icon').show();
    $('.add-check-all-icon-inactive').hide();

    var catId = $(this).attr('data-category-id');
    var total_inputs = $(".industry-container").length;
    var category_inputs = $('#ind' + catId).find('.industry-container').length;
    var category_checked_inputs = $('#ind' + catId).find('.industry-container input:checked').length;
    var all_checked_inputs = $(".select_industry input:checked").length;

    if(all_checked_inputs <= category_inputs) {
        $('.add-uncheck-all-icon').hide();
        $('.add-uncheck-all-icon-inactive').show();
    }

    if(all_checked_inputs <= category_inputs && all_checked_inputs-category_checked_inputs == 0) {
        $('.uncheck-all-icon').hide();
        $('.uncheck-all-icon-inactive').show();
    }

    $('#ind' + catId).find('.add-uncheck-all-type').hide();
    $('#ind' + catId).find('.add-check-all-type').show();
    $('#ind' + catId).find('.add-uncheck-all-type-inactive').show();
    $('#ind' + catId).find('.add-check-all-type-inactive').hide();
    $('#ind' + catId).find(".industry-container div.icheckbox_minimal-green").each(function(){
        $(this).removeClass('checked');
        $(this).find('input').prop('checked', false);
    });

    $('#modalConfirm').find('.modal-confirm-btn a').removeClass('.modal-add-uncheck-all-type');
    $('#modalConfirm').modal('hide');

    var taskId = $(this).attr('data-task-id');
    var categoryId = $(this).attr('data-category-id');
    var url = $(this).attr('data-url');
    var data = {
        'id_ms_task': taskId,
        'category_id': categoryId,
        'is_checked': 0
    };
    // save to database
    jQuery.post(url, data)
        .done(function (msg) {
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: msg,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        })
        .fail(function (xhr, textStatus, errorThrown) {
         // alert(xhr.responseText);
        });
});

$(document).ready(function() {
    var all_checked_inputs = $(".select_industry input:checked").length;
    $('.select_industry .tab-pane').each(function(){
        var category_checked_inputs = $(this).find('input:checked').length;
        var category_input_containers= $(this).find('.industry-container').length;
        if (category_checked_inputs == 0) {
            $(this).find('.add-check-all-type').show();
            $(this).find('.add-check-all-type-inactive').hide();
            $(this).find('.add-uncheck-all-type').hide();
            $(this).find('.add-uncheck-all-type-inactive').show();
        }
        if (category_checked_inputs > 0 && category_checked_inputs < category_input_containers) {
            $(this).find('.add-check-all-type').show();
            $(this).find('.add-check-all-type-inactive').hide();
            $(this).find('.add-uncheck-all-type').show();
            $(this).find('.add-uncheck-all-type-inactive').hide();
        }
        if(category_checked_inputs == category_input_containers) {
            $(this).find('.add-check-all-type').hide();
            $(this).find('.add-check-all-type-inactive').show();
            $(this).find('.add-uncheck-all-type').show();
            $(this).find('.add-uncheck-all-type-inactive').hide();
        }
    });
});
