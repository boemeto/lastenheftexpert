<script type="text/javascript">
    (function ($) {
        $.fn.EasyTree = function (options) {
            var defaults = {
                selectable: true,
                deletable: false,
                editable: false,
                addable: false,
                i18n: {
                    deleteNull: '<?php print trans('messages.label_deleteNull') ?>',
                    deleteConfirmation: '<?php print trans('messages.label_deleteConfirmation') ?>',
                    confirmButtonLabel: '<?php print trans('messages.act_confirm') ?>',
                    editMultiple: '<?php print trans('messages.act_edit') ?>',
                    addMultiple: '<?php print trans('messages.act_add') ?>',
                    collapseTip: '<?php print trans('messages.act_collapse') ?>',
                    expandTip: '<?php print trans('messages.act_expand') ?>',
                    selectTip: '<?php print trans('messages.act_select') ?>',
                    unselectTip: '<?php print trans('messages.act_deselect') ?>',
                    editTip: '<?php print trans('messages.act_edit') ?>',
                    saveTip: '<?php print trans('messages.act_save') ?>',
                    addTip: '<?php print trans('messages.act_add') ?>',
                    deleteTip: '<?php print trans('messages.act_delete') ?>',
                }
            };


            var warningAlert = $('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var dangerAlert = $('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var createInput = $('<div class="input-group">' +
                    '<input type="text" class="tree-control">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm"><?php print trans('messages.act_save') ?></button>' +
                    '<button type="button" class="btn ls-red-btn cancel"><?php print trans('messages.act_cancel') ?></button>' +
                    ' </span>' +
                    ' </div> ');
            var editInput = $('<div class="input-group">' +
                    '<input type="text" class="easy-tree-editor">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm_edit"><?php print trans('messages.act_save') ?></button>' +
                    '<button type="button" class="btn ls-red-btn cancel_edit"><?php print trans('messages.act_cancel') ?></button>' +
                    ' </span>' +
                    ' </div> ');
            var createButton = $('<div class="create"><button class="btn ls-light-blue-btn btn-round disabled"><i ' +
                    'class="fa fa-plus"></i></button></div> ');
            var editButton = $('<div class="edit"><button  class="btn ls-orange-btn btn-round disabled"><i ' +
                    'class="fa fa-edit"></i></button></div>');
            var deleteButton = $('<div class="remove"><button data-form="dataForm" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Module" data-message="{{trans("validation.delete_module")}}" \n\
class="formConfirm btn  ls-red-btn btn-round disabled"><i ' +
                    'class="fa fa-trash"></i></button></div>');
            var exportButton = $('<div class="export"><button class="btn ls-light-green-btn btn-round btn-export-modal" data-toggle="modal" data-target="#modal-export"><i ' +
                    'class="fa fa-download"></i></button></div>');
            var optionBox = '<div class="row">' +
                    '<div class="col-xs-2"></div>' +
                    '<div class="col-xs-10 inside-with-border">' +
                    '<a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onClick="modal_dialog_add_task_new(0)"> ' +
                    '<i class="fa fa-plus"></i> </a>' +
                    '</div>' +
                    '</div>';

            options = $.extend(defaults, options);

            this.each(function () {


                var easyTree = $(this);

                $(easyTree).find('li:has(ul)').addClass('parent_li').find('span.menu_title').attr('title', options.i18n.collapseTip);

                // add easy tree toolbar dom
                if (options.deletable || options.editable || options.addable) {

                    $(easyTree).find('li > span.menu_title').after('<div class="easy-tree-toolbar"  style="display:none;"></div>');
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(deleteButton);
//                    $(easyTree).prepend('<div class="easy-tree-toolbar" style="display:block;"></div> ');

                }


                // addable
                if (options.addable) {

                    $(easyTree).find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('.create > button').click(function () {

                        var createBlock = $(this).parent();
                        if (!$(createBlock).find('input').hasClass('tree-control')) {
                            $(createBlock).after(createInput);
                        }

                        $(createInput).find('input').focus();
                        $(createInput).find('.confirm').click(function () {
                            if ($(createInput).find('input').val() === '')
                                return;
                            var selected = getSelectedItems();
                            $.amaran({
                                'theme': 'colorful',
                                'content': {
                                    message: '{{trans("validation.add_submodule")}}',
                                    bgcolor: '#324e59',
                                    color: '#fff'
                                }
                            });

                            var item = $('<li class="li_not_editable"><span class="menu_title" id="0">' +
                                    '<span class="glyphicon glyphicon-minus-sign close_tasks"></span>' +
                                    '<a href="javascript: void(0);"> ' +
                                    $(createInput).find('input').val() + '</a> ' +
                                    '<i class="fa fa-exclamation" style="font-size: 13px"></i>' +
                                    '</span>' + optionBox + '</li>');
                            $(item).find('a.addSubmoduleTask').attr('data-id', 1);

                            $(item).find(' > span.menu_title > span').attr('title', options.i18n.collapseTip);
                            $(item).find(' > span.menu_title > a').attr('title', options.i18n.selectTip);


                            if (selected.length <= 0) {
                                $(easyTree).find(' > ul').append($(item));
                                $(item).find('.easy-tree-toolbar').append(createButton);
                                $(item).find('.easy-tree-toolbar').append(editButton);
                                $(item).find('.easy-tree-toolbar').append(deleteButton);
                            } else if (selected.length > 1) {
                                $(easyTree).prepend(warningAlert);
                                $(easyTree).find('.alert .alert-content').text(options.i18n.addMultiple);

                            } else {
                                if ($(selected).hasClass('parent_li')) {
                                    $(selected).find(' > ul').append(item);
                                    $(item).find('span.menu_title').after('<div class="easy-tree-toolbar" style="display:none;"></div>');
                                    $(item).find('.easy-tree-toolbar').append(createButton);
                                    $(item).find('.easy-tree-toolbar').append(editButton);
                                    $(item).find('.easy-tree-toolbar').append(deleteButton);
                                } else {
                                    $(selected).find('div.row').remove();
//                                    $(selected).find('div.create').remove();
                                    $(selected).addClass('parent_li').find('  span.menu_title > span').addClass('glyphicon-minus-sign');
                                    $(selected).append($('<ul></ul>')).find(' > ul').append(item);
                                    $(item).find('span.menu_title').after('<div class="easy-tree-toolbar" style="display:none;"></div>');
                                    $(item).find('.easy-tree-toolbar').append(createButton);
                                    $(item).find('.easy-tree-toolbar').append(editButton);
                                    $(item).find('.easy-tree-toolbar').append(deleteButton);

                                }

                            }
//                            var var_id = $(selected).closest('li').find('.menu_title').attr('id');
//                            var id_nr = var_id.replace('submodule_', "");
//                            alert($(item).find('.menu_title').find('a').html());

                            var parents_list = '';
                            var parents = $(item).find('.menu_title').parents("li");
                            var level_par = parents.length;

                            $(item).find('.row').addClass('margin_left_minus' + String(level_par));
                            for (var i = parents.length - 1; i >= 0; i--) {
                                var var_id = $(parents[i]).find('.menu_title').attr('id');
                                var var_name = $(parents[i]).find('.menu_title').find('a').html();
                                var id_nr = var_id.replace('submodule', "");
                                var name = $.trim(var_name.replace('_', " "));
                                var name = $.trim(name.replace('/', ""));
                                parents_list = parents_list + '_' + name + '_' + id_nr;
                            }
//                            $('#easyTreeStructure').val(parents_list);
                            $(item).find('a.addSubmoduleTask').attr('onClick', "modal_dialog_add_task_new('" + parents_list + "')");
                            $(createInput).find('input').val('');
                            if (options.selectable) {
                                $(item).find('li > span.menu_title > a').attr('title', options.i18n.selectTip);
                                $(item).find(' > span > a').click(function (e) {

                                    var li = $(this).parent().parent();
                                    var span = $(this).parent();

                                    $('.easy-tree-toolbar').not($(li).children('.easy-tree-toolbar')).hide();


                                    if (li.hasClass('li_selected')) {
                                        $(span).find('a').attr('title', options.i18n.selectTip);
                                        $(li).removeClass('li_selected');
                                        $('.easy-tree-toolbar').hide();
                                    }
                                    else {
                                        $(easyTree).find('li.li_selected').removeClass('li_selected');
                                        $(span).find('a').attr('title', options.i18n.unselectTip);
                                        $(li).addClass('li_selected');

                                        if (!$(li).hasClass('not_selectable') || $(li).hasClass('li_editable')) {
                                            $(span).next('.easy-tree-toolbar').show();
                                            $(span).next('.easy-tree-toolbar').append(createButton);
                                            $(span).next('.easy-tree-toolbar').append(editButton);
                                            $(span).next('.easy-tree-toolbar').append(deleteButton);

                                        }

                                    }


                                    if (options.deletable || options.editable || options.addable) {
                                        var selected = getAddableItems();
                                        var li_parents = $(this).parents("li").length;
                                        if (options.addable) {
                                            // nested levels
                                            if ((selected.length <= 0 || selected.length > 1) || li_parents > 4) {
                                                $(easyTree).find('.easy-tree-toolbar .create > button').addClass('disabled');
                                            }
                                            else {
                                                $(easyTree).find('.easy-tree-toolbar .create > button').removeClass('disabled');
                                            }
                                        }

                                        var selected = getEditableItems();
                                        if (options.editable) {
                                            if (selected.length <= 0 || selected.length > 1)
                                                $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                            else
                                                $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                                        }

                                        var selected = getDeletableItems();
                                        if (options.deletable) {
                                            if (selected.length <= 0 || selected.length > 1)
                                                $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                            else
                                                $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                                        }

                                    }

                                    e.stopPropagation();
                                });
                            }
                            $(createInput).remove();
                        });
                        $(createInput).find('.cancel').text(options.i18n.cancelButtonLabel);
                        $(createInput).find('.cancel').click(function () {
                            $(createInput).remove();
                        });
                    });
                }

                // editable
                if (options.editable) {
//
                    $(easyTree).find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('.edit > button').attr('title', options.i18n.editTip).click(function () {


//                        $(easyTree).find('input.easy-tree-editor').remove();
                        $(easyTree).find('li > span.menu_title > a:hidden').show();
                        var selected = getEditableItems();
                        if (selected.length <= 0) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editNull);
                        }
                        else if (selected.length > 1) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editMultiple);
                        }
                        else {
                            var value = $.trim($(selected).find(' > span.menu_title > a').text());

                            var id = $(selected).find('.menu_title').attr('id');
                            if (id.indexOf("submodule") >= 0) {
                                var id_nr = id.replace('submodule', "");
//                                                                if($(selected).find('.menu_title').hasClass('is_project')) {
//                                    var is_project = 1;
//                                } else {
//                                    var is_project = 0;
//                                }
//                                console.log(is_project);
//                                modal_dialog_edit_module(id_nr, is_project);
                                modal_dialog_edit_module(id_nr);
                            }
                            if (id.indexOf("software") >= 0) {
                                var id_nr = id.replace('software', "");
                                modal_dialog_edit_software(id_nr);
                            }
                            if (id.indexOf("industry") >= 0) {
                                var id_nr = id.replace('industry', "");
                                modal_dialog_edit_industry(id_nr);

                            }

//                            $(selected).find(' > span.menu_title > a').hide();
//                            $(selected).find(' > span.menu_title > i').hide();
//                            $(selected).find(' > span.menu_title > span').hide();

//                            if (!$(selected).find(' > span.menu_title >input').hasClass('easy-tree-editor')) {
//                                $(selected).find(' > span.menu_title').append(editInput);
//                            }
                            //                   var editor = $(editInput).find(' input.easy-tree-editor');
                            //           $(editor).val(value);
                            //                    $(editor).focus();
                            /*     $(editInput).find('.confirm_edit').click(function () {

                             if ($.trim($(editor).val() !== '')) {
                             var editor_val = $(editor).val();

                             var id = $(selected).find('.menu_title').attr('id');
                             var id_nr = id.replace('submodule', "");


                             var data = {
                             'id_module_structure': id_nr,
                             'editor_val': editor_val
                             };

                             jQuery.post('
                            <?php //  printroute("ajax.edit_module_s_admin") ?>', data)
                             .done(function (msg) {
                             //                                            alert(JSON.stringify(data))
                             //                                            alert(msg)
                             })
                             .fail(function (xhr, textStatus, errorThrown) {
                             alert(JSON.stringify(data))
                             alert(xhr.responseText);
                             });


                             $(selected).find(' > span.menu_title > a').html($(editor).val());
                             $(editInput).remove();
                             $(selected).find(' > span.menu_title > a').show();
                             $(selected).find(' > span.menu_title > i').show();
                             $(selected).find(' > span.menu_title > span').show();
                             }


                             });*/
                            /*    $(editInput).find('.cancel_edit').click(function () {
                             $(editInput).remove();
                             $(selected).find(' > span.menu_title > a').show();
                             $(selected).find(' > span.menu_title > i').show();
                             $(selected).find(' > span.menu_title > span').show();
                             });
                             */

                        }
                    });
                }

                // deletable
                if (options.deletable) {

                    $(easyTree).find('.easy-tree-toolbar').append(deleteButton);
                    $(easyTree).find('.remove > button').click(function (event) {
                        event.preventDefault();
                        var selected = getEditableItems();
                        if (selected.length <= 0) {
//                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.deleteNull);
                        } else {
                            $(document).one('click', '#frm_submit', function() {
                                var id = $(selected).find('.menu_title').attr('id');
                                var id_nr = id.replace('submodule', "");
                                var child_id = '';

                                $(selected).find('.menu_title').each(function () {
                                    var id_ch = $(this).attr('id');
                                    var id_nr_ch = id_ch.replace('submodule', "");
                                    child_id = child_id + '_' + String(id_nr_ch);
                                });

                                var data = {
                                    'id_module_structure': id_nr,
                                    'id_software': '<?php print $software->id_software; ?>',
                                    'child_structure': child_id
                                };

                                jQuery.post('{{  route("ajax.delete_module_structure_software") }}', data)
                                        .done(function (msg) {
                                            var delete_succes = '{{trans('validation.delete_success')}}';
                                            var amaran_bgcolor = '';
                                            if(msg.trim() == delete_succes) {
                                                amaran_bgcolor = '#324e59';
                                                $(selected).find(' ul ').remove();
                                                if ($(selected).parent('ul').find(' > li').length <= 1) {
                                                    $(selected).parents('li').removeClass('parent_li').find(' > span.menu_title > span').removeClass('glyphicon-minus-sign');
                                                    $(selected).parent('ul').remove();
                                                }
                                                $(selected).remove();
                                            } else {
                                                amaran_bgcolor = '#D9534F';
                                            }

                                            $.amaran({
                                                'theme': 'colorful',
                                                'content': {
                                                    message: msg,
                                                    bgcolor: amaran_bgcolor,
                                                    color: '#fff'
                                                }
                                            });

                                            data = undefined;
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            //alert(JSON.stringify(data));
                                            //alert(xhr.responseText);
                                        });
                                $('#formConfirm').modal('hide');
                            });
                        }
                        selected = getEditableItems();
                    });
                }

                // collapse or expand
                $(easyTree).delegate(' li.parent_li > span.menu_title', 'click', function (e) {
                    var div_children = $(this).parent('li.parent_li').find(' div > ul > li');
                    var all_children = $(this).parent('li.parent_li').find('  ul > li');
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(':visible')) {
//                        children.hide('fast');
                        $(this).attr('title', options.i18n.expandTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-plus-sign')
                                .removeClass('glyphicon-minus-sign');

                        all_children.children('span').find(
                                ' > span.glyphicon').addClass(
                                'glyphicon-plus-sign').addClass(
                                'click_item').removeClass(
                                'glyphicon-minus-sign');
                        $(this).parent('li.parent_li').find('.border_row').hide('fast');
                        all_children.hide('fast');

                    } else {

                        if ($(this).find('tasks_title')) {
                            children.show('fast');
                            div_children.show('fast');
                        }
                        $(this).attr('title', options.i18n.collapseTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-minus-sign')
                                .removeClass('glyphicon-plus-sign');
                    }
                    e.stopPropagation();
                });

                // selectable, only single select
                if (options.selectable) {

//                    alert('selectable');

                    $(easyTree).find('li > span > a').attr('title', options.i18n.selectTip);
                    $(easyTree).find('li > span > a').click(function (e) {
                        var li = $(this).parent().parent();

                        $('.easy-tree-toolbar').not($(li).children('.easy-tree-toolbar')).hide();


                        if (li.hasClass('li_selected')) {
                            $(this).attr('title', options.i18n.selectTip);
                            $(li).removeClass('li_selected');
                            $('.easy-tree-toolbar').hide();
                        }
                        else {
                            $(easyTree).find('li.li_selected').removeClass('li_selected');
                            $(this).attr('title', options.i18n.unselectTip);
                            $(li).addClass('li_selected');

                            if (!$(li).hasClass('not_selectable') || $(li).hasClass('li_editable')) {
                                $(this).parent().next('.easy-tree-toolbar').show();
                            }
                        }

                        if (options.deletable || options.editable || options.addable) {
                            var selected = getAddableItems();
                            var li_parents = $(this).parents("li").length;
                            hide_modal_box();
                            if (options.addable) {

                                if ((selected.length <= 0 || selected.length > 1) || li_parents > 4) {
//                                    alert('trues');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').addClass('disabled');
                                }
                                else {
//                                    alert('falses');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').removeClass('disabled');
                                }
                            }
                            var selected = getEditableItems();
                            if (options.editable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                            }
                            var selected = getDeletableItems();
                            if (options.deletable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                            }

                        }

                        e.stopPropagation();

                    });
                }

                /* Export button */
                $(easyTree).find('.easy-tree-toolbar').append(exportButton);

                var getAddableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_selectable');
                };

                // Get selected items
                var getSelectedItems = function () {
                    return $(easyTree).find('li.li_selected');
                };

                var getEditableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable');
                };

                var getDeletableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable').not('li.li_not_deletable');
                };

            });
        };

    })(jQuery);

    $(document).ready(function(){
        $(".glyphicon-sort-by-attributes").on("click", function(e){
            $(this).parent().siblings('ul').children("li").children().children().each(function() {
                e.preventDefault();
                $(this).click();
            });
        });
    });
</script>
