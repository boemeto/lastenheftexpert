<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 27.05.2015
 * Time: 12:41
 */
class Industry_type extends Eloquent
{


    public static $rules = [
        'name_industry_type' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_industry_type';
    protected $table = 'pr_industry_type';
    protected $guarded = ['id_industry_type'];
    protected $fillable = ['name_industry_type', 'box_color', 'font_icon'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Industry_type::whereId_industry_type($id)->first();
        return $list;
    }

    public static function getIndustry_types()
    {
        $array = array();
        $lists = Industry_type::select('*')->get();
        $array[0] = 'Select';
        foreach ($lists as $list) {
            $array[$list->id_industry_type] = $list->name_industry_type;
        }
        return $array;
    }
 

}