@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-plus-square-o"></i>
            <h3 class="box-title">{{trans('messages.add')}} {{trans('messages.task')}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {{ Form::open(['route'=>'task.store'])}}
            <div class="form-group">
                {{ Form::label('name_task','Name: ') }}
                {{ Form::text('name_task', '', ['class'=>"form-control"]) }}
                {{ $errors->first('name_task','<div class="alert alert-danger">:message</div>') }}
            </div>
            <div class="form-group">
                {{ Form::label('description_task','Description: ') }}
                {{ Form::textarea('description_task', '', ['class'=>"form-control"]) }}
                {{ $errors->first('description_task','<div class="alert alert-danger">:message</div>') }}
            </div>
            <div>
                <button type="submit" class="btn ls-light-blue-btn btn-flat ">
                    <i class="fa  fa-plus-square-o"></i> {{trans('messages.add')}}
                </button>
            </div>
            {{ Form::close()}}
        </div>
        <!-- /.box-body -->
    </div>
@stop
