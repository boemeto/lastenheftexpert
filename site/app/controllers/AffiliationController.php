<?php
use Illuminate\Support\MessageBag;

class AffiliationController extends \BaseController
{

    /**
     * Display a listing of the affiliates and the  possibility to add more affiliates
     *
     * @return Response
     */
    public function index()
    {
        // get the group of the authenticated user
        $fk_group = Auth::user()->fk_group;
        // if user is company admin
        if ($fk_group == 2) {
            $id_company = Auth::user()->fk_company;
            // get the company details
            $company = Company::getFirstById($id_company);
            $companies = array();
            $searched_company = '';
            // list of affiliated companies
            $affiliated_companies = Affiliation_company::getAffiliatedCompanies($id_company);
            $affiliated_ids = Affiliation_company::getAffiliatedIds($id_company);

            // return the view with the affiliated companies
            return View::make('front.affiliation.index', compact('companies', 'company', 'searched_company', 'affiliated_companies', 'affiliated_ids'));

        } else {
            // if user is super admin
            if ($fk_group == 1) {
                $affiliated_companies = Affiliation_company::getAffiliatedCompanies();
                $affiliated_ids = Affiliation_company::getAffiliatedIds();
                // return the administrator's view
                return View::make('admin.affiliation.index', compact('affiliated_companies', 'affiliated_ids'));
            }
            // return error
            return Redirect::to('404');
        }
    }


    /**
     * Search affiliates company
     *
     * @return Response
     */
    public function store()
    {

        $id_company = Auth::user()->fk_company;
        $company = Company::whereId_company($id_company)->first();

        // validate search field
        $rules = array(
            'searched_company' => 'required|min:3|alpha_num_spaces',

        );
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::Back()
                ->withErrors($validator)// send back all errors to the login form
                ->withInput();
        }
        $searched_company = '';
        if (Input::has('searched_company')) {
            $searched_company = Input::get('searched_company');
        }
        if ($searched_company != '') {
            $companies = Company::where('name_company', 'like', '%' . $searched_company . '%')
                ->where('id_company', '!=', $id_company)
                ->get();

        }

        $affiliated_companies = Affiliation_company::getAffiliatedCompanies($id_company);
        $affiliated_ids = Affiliation_company::getAffiliatedIds($id_company);
        if (count($companies) > 0) {

            return View::make('front.affiliation.index', compact('companies', 'company', 'searched_company', 'affiliated_companies', 'affiliated_ids'));

        } else {
            $errors = new MessageBag(['searched_company' => [Lang::get('validation.no_company')]]);
            return Redirect::back()->withErrors($errors);
        }
    }


    /**
     * Accept Affiliation to Company
     *
     * @return Response
     */


    public function accept()
    {

        $id = Input::get('id_affiliation');
        $affiliation = Affiliation_company::whereId_affiliation_company($id)->first();
        $affiliation->is_pending = 0;
        $affiliation->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);

    }


    /**
     * Save the affiliation
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $id_affiliate = Input::get('id_company');

        // search if the affiliation was already requested -> by this company
        $affiliation_company = Affiliation_company::whereFk_affiliation($id_affiliate)
            ->whereFk_company($id)->first();
        if (count($affiliation_company) > 0) {
            if ($affiliation_company->is_pending == 1) {
                $errors = new MessageBag(['searched_company' => [Lang::get('validation.message_affiliation_pending')]]);
                return Redirect::back()->withErrors($errors);
            } else {
                $errors = new MessageBag(['searched_company' => [Lang::get('validation.message_affiliation_exists')]]);
                return Redirect::back()->withErrors($errors);
            }

        } else {
            // search if the affiliation was already requested -> by the other company
            $company_affiliation = Affiliation_company::whereFk_affiliation($id)
                ->whereFk_company($id_affiliate)->first();
            if (count($company_affiliation) > 0) {
                if ($company_affiliation->is_pending == 1) {
                    $errors = new MessageBag(['searched_company' => [Lang::get('validation.message_affiliation_pending')]]);
                    return Redirect::back()->withErrors($errors);
                } else {
                    $errors = new MessageBag(['searched_company' => [Lang::get('validation.message_affiliation_exists')]]);
                    return Redirect::back()->withErrors($errors);
                }
            } else {
                // create new affiliation
                $affiliation = new Affiliation_company();
                $affiliation->fk_affiliation = $id_affiliate;
                $affiliation->fk_company = $id;
                $affiliation->is_pending = 1;
                $affiliation->save();
                $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
                return Redirect::back()->withErrors($errors);
            }

        }


    }


    /**
     * Reject / Cancel the affiliation
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $affiliation = Affiliation_company::whereId_affiliation_company($id)->first();
        $affiliation->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }


}
