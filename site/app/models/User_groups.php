<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 24-Nov-15
 * Time: 11:31
 */
class User_groups extends Eloquent
{
    public static $rules = [
        'fk_user' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_user_group';
    protected $table = 'users_groups';
    protected $guarded = ['id_user_group'];
    protected $fillable = ['fk_user', 'fk_group'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = User_groups::where('id_user_group', '=', $id)->first();
        return $list;
    }

    public static function getGroupByUser($id_user)
    {
        $array = array();
        $lists = User_groups::where('fk_user', '=', $id_user)->get();
        foreach ($lists as $list) {
            $array[] = $list->fk_group;
        }
        return $array;
    }

    public static function createNewGroup($id_user, $fk_group)
    {
        $user_group = User_groups::getGroupByKey($id_user, $fk_group);
        if (count($user_group) == 0) {
            $user_group = new User_groups();
            $user_group->fk_user = $id_user;
            $user_group->fk_group = $fk_group;
            $user_group->save();
        }
        return $user_group->id_user_group;
    }

    public static function getGroupByKey($id_user, $id_group)
    {
        $array = array();
        $lists = User_groups::where('fk_user', '=', $id_user)
            ->where('fk_group', '=', $id_group)
            ->first();

        return $lists;
    }

    public static function getGroupByUserAndCompany($id_user, $id_company)
    {
        $group = static::select('users_groups.*')
                    ->join('users', 'users.id','=', 'users_groups.fk_user')
                    ->where('users_groups.fk_user', '=', $id_user)
                    ->where('users.fk_company', '=', $id_company)
                    ->get()
                    ->first();

        return $group;
    }
}
