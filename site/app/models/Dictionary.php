<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 24.03.2015
 * Time: 10:27
 */
class Dictionary extends Eloquent
{

    public $primaryKey = 'id_position';
    protected $table = 'dictionary_positions';
    protected $guarded = ['id_position'];
    protected $fillable = ['fk_dictionary', 'name_en', 'name_de', 'value'];

    public static function getOptions($fk_dictionary = 1, $empty_value = 0)
    {
        $array = array();

        $dictionary = Dictionary::select('value', 'name_en', 'name_de')->whereFk_dictionary($fk_dictionary)
            ->orderBy('value')->get();
        if ($empty_value == 1) {
            $array[0] = trans('messages.input_select');
        }

        foreach ($dictionary as $list) {
            if (Session::get('lang') == 'de') {
                $array[$list->value] = $list->name_de;
            } else {
                $array[$list->value] = $list->name_en;
            }
        }
        return $array;
    }
}
