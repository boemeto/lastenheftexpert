<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 31-Mar-16
 * Time: 11:29
 */

?>
@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('footer_scripts')
    <script type="text/javascript">
        $(document).on("click", ".delete_keyword", function (e) {
                e.preventDefault();
                $('.confirm_delete_keyword').removeClass('disabled');

                var data_id = $(this).attr('id');
                var msg = $(this).attr('data_msg');

                 $('#formConfirm')
                        .find('#frm_body').html(msg)
                        .end().modal('show');

                 $('#frm_submit').addClass('confirm_delete_keyword').attr('data_id', data_id);

                 $('#formConfirm').on('click', '.confirm_delete_keyword', function(ev) {
                        var id = $(this).attr('data_id');
                        var form = $("[id=" + id + "]").parent();
                        $(form).submit();
                });
         });
    </script>
@stop

@section('content')

    <div class="box box-default">
        <div class="box-body">
            <div class="panel widget light-widget panel-bd-top ">
                <div class="panel-heading bordered">
                    <h3 class="box-title">  {{trans('messages.language')}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row paddings-mini">

                        <div class="col-md-12">
                            <table class="table  table-responsive data_table">
                                <thead>
                                <tr>
                                    <th style="display: none">{{trans('messages.l_key')}}</th>
                                    @if($fk_group == 4)
                                        <th style="width: 20%">{{trans('messages.l_key')}}</th>
                                    @endif
                                    <th>
                                        @if($id == 2)
                                            {{trans('messages.label_validations')}}
                                        @else
                                            {{trans('messages.label_terms')}}
                                        @endif
                                        [DE]
                                    </th>
                                    <th>   @if($id == 2)
                                            {{trans('messages.label_validations')}}
                                        @else
                                            {{trans('messages.label_terms')}}
                                        @endif [EN]
                                    </th>
                                    <th>{{trans('messages.label_action')}}  </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($fk_group == 4)
                                    <tr>
                                        {{ Form::open(['route'=>'lang.store'])}}
                                        <td style="display: none"></td>
                                        <td>
                                            <div class="form-group">
                                                {{ Form::text('key', '', ['class'=>"form-control",'placeholder'=>trans('messages.l_key'),'required']) }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                {{ Form::text('name_de',  '', ['class'=>"form-control",'placeholder'=>trans('messages.l_name'),'required']) }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                {{ Form::text('name_en',  '', ['class'=>"form-control",'placeholder'=>trans('messages.l_name'),'required']) }}
                                            </div>
                                        </td>
                                        <td>
                                            {{Form::hidden('fk_type',$id)}}
                                            <button type="submit" class='btn btn-primary btn-round btn-xl  btn-flat'>
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </td>
                                        {{Form::close()}}
                                    </tr>
                                @endif
                                @foreach($list as $row)
                                    <tr>
                                        {{ Form::model($row,['route'=>['lang.update',$row->id_language], 'method'=>'PATCH'])}}
                                        <td style="display: none">{{$row->key}} {{$row->name_de}} {{$row->name_en}}</td>
                                        @if($fk_group == 4)
                                            <td>
                                                <div class="form-group">
                                                    {{ Form::text('key', $row->key, ['class'=>"form-control",'placeholder'=>trans('messages.l_key'),'required']) }}
                                                </div>
                                            </td>
                                        @endif
                                        <td>
                                            <div class="form-group">
                                                {{ Form::text('name_de',  $row->name_de, ['class'=>"form-control",'placeholder'=>trans('messages.l_name'),'required']) }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                {{ Form::text('name_en',  $row->name_en, ['class'=>"form-control",'placeholder'=>trans('messages.l_name'),'required']) }}
                                            </div>
                                        </td>
                                        <td>
                                            {{Form::hidden('fk_type',$id)}}
                                                <button type="submit" class='btn btn-primary btn-round btn-xl   btn-flat'>
                                                    <i class="fa fa-save"></i>
                                                </button>
                                            {{Form::close()}}
                                            {{ Form::open(array('route' => array('lang.destroy', $row->id_language), 'method' => 'delete')) }}
                                                <button id="{{ $row->id_language }}" type="submit" class="btn btn-danger btn-round btn-xl btn-flat delete_keyword" data_msg="{{trans('validation.confirmation_delete')}}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End content-->
    </div>

    <!-- End content info - Services Items -->
    @include('admin.alert_box.delete_confirm')
@stop
