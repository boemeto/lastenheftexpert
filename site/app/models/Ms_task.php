<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 8/4/2015
 * Time: 2:15 PM
 */
class Ms_task extends Eloquent
{


    public static $rules = [
        'fk_task' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_ms_task';
    protected $table = 'pr_ms_tasks';
    protected $guarded = ['id_ms_task'];
    protected $fillable = ['fk_task', 'fk_module_structure', 'fk_pair', 'lvl_priority', 'task_order', 'is_checked', 'is_default', 'fk_company', 'fk_project'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Ms_task::whereId_ms_task($id)->first();
        return $list;
    }

    public static function getFirstByfk_task($id)
    {
        $task = Ms_task::wherefk_task($id)->first();
        return $task;
    }

    public static function getNotAprovedTasks()
    {
        $tasks = DB::table('pr_ms_tasks')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->groupBy('pr_tasks.id_task')
            ->where('is_default', '=', 0)
            ->get();
        return $tasks;
    }

    public static function getOneTaskById_task($id_ms_task)
    {
        $tasks = DB::table('pr_ms_tasks')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->leftJoin('pr_tasks_pair', 'pr_ms_tasks.fk_pair', '=', 'pr_tasks_pair.id_pair');
//        if ($fk_industry > 0) {
//            $tasks = $tasks->join('pr_industry_ms', function ($join) use ($fk_industry) {
//                $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
//                $join->where('pr_industry_ms.fk_industry', '=', $fk_industry);
//            });
//        }
        $tasks = $tasks->where('id_ms_task', '=', $id_ms_task)
                    ->select('*', 'pr_ms_tasks.is_default AS pr_ms_tasks_is_default')
            ->first();
        return $tasks;
    }

    public static function getSoftwareTasks($id_module_structure)
    {
        $tasks = Ms_task::where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)->get();
        return $tasks;
    }

    public static function getTasksPairsByStructure($id_module_structure)
    {
        $array = array();
        $tasks = DB::table('pr_ms_tasks')
            ->selectRaw('fk_pair,  min(id_ms_task) as id_ms_task')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
            ->where('pr_ms_tasks.fk_pair', '>', 0)
            ->groupBy('pr_ms_tasks.fk_pair')
            ->orderBy('task_order')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $array[$task->fk_pair] = $task->id_ms_task;
            }
        }
        return $array;
    }

    public static function getTasksPairsByStructureIndustry($id_module_structure, $id_industry, $min_2 = 0)
    {
        $array = array();
        $tasks = DB::table('pr_ms_tasks')
            ->selectRaw('fk_pair,  min(id_ms_task) as id_ms_task')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->join('pr_industry_ms', function ($join) use ($id_industry) {
                $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
            })
            // ->where('pr_ms_tasks.is_default', '=', '1')
            ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
            ->where('pr_ms_tasks.fk_pair', '>', 0)
            ->groupBy('pr_ms_tasks.fk_pair');

        if ($min_2 == 1) {
            $tasks = $tasks->havingRaw("COUNT(fk_pair) > 1");
        }

        $tasks = $tasks->orderBy('pr_industry_ms.ind_order', 'asc')
            ->orderBy('task_order')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $array[$task->fk_pair] = $task->id_ms_task;
            }
        }

        return $array;
    }

    public static function getTaskPairsByPair($id_module_structure, $fk_pair)
    {
        $array = array();
        $tasks = DB::table('pr_ms_tasks')
            ->selectRaw('fk_pair,  min(id_ms_task) as id_ms_task')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
            ->where('pr_ms_tasks.fk_pair', '=', $fk_pair)
            ->groupBy('pr_ms_tasks.fk_pair')
            ->orderBy('task_order')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $array[$task->fk_pair] = $task->id_ms_task;
            }
        }
        return $array;
    }

    public static function getTaskPairsByPairIndustry($id_module_structure, $id_industry, $fk_pair, $min_2 = 0)
    {
        $array = array();
        $tasks = DB::table('pr_ms_tasks')
            ->selectRaw('fk_pair,  min(id_ms_task) as id_ms_task')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->join('pr_industry_ms', function ($join) use ($id_industry) {
                $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
            })
            // ->where('pr_ms_tasks.is_default', '=', '1')
            ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
            ->where('pr_ms_tasks.fk_pair', '=', $fk_pair)
            ->groupBy('pr_ms_tasks.fk_pair');
        if ($min_2 == 1) {
            $tasks = $tasks->havingRaw("COUNT(fk_pair) > 1 ");
        }
        $tasks = $tasks->orderBy('task_order')
            ->get();

        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $array[$task->fk_pair] = $task->id_ms_task;
            }
        }
        return $array;
    }

    public static function getTasksByStructure($id_module_structure, $id_project = 0, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0)
    {
        if ($id_project == 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->select('pr_ms_tasks.id_ms_task',
                    'pr_ms_tasks.lvl_priority',
                    'pr_tasks.name_task',
                    'pr_tasks.description_task',
                    'pr_ms_tasks.is_default AS ms_tasks_default')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_tasks_pair', 'id_pair', '=', 'pr_ms_tasks.fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);

            if ($is_default == 2) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '0');
            }
            if ($is_default == 1) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
            }
            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                });

            }
            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0');
                });
            }
            // dd($id_module_structure);
            if ($fk_attach == 1) {
                // $attachments = Software_attach::getTasksWithAttachments($id_module_structure);
                $attachments = Industry_attach::getTasksWithAttachments($id_module_structure);
                $tasks = $tasks->whereIn('id_ms_task', $attachments);
            }
            if ($fk_attach == 2) {
                // $attachments = Software_attach::getTasksWithAttachments($id_module_structure);
                $attachments = Industry_attach::getTasksWithAttachments($id_module_structure);
                // dd($attachments);
                if($attachments !== null) {
                    $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
                }
            }

            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }
            if ($fk_pair === null) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', null)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });
            }
            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', '%' . $meta_tags . '%');
            }
            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', '%' . $address_code . '%');
            }
            if ($fk_priority > 0) {
                $tasks = $tasks->where('pr_ms_tasks.lvl_priority', '=', $fk_priority);
            }

            $tasks = $tasks->orderBy('pr_ms_tasks.task_order', 'asc')
                ->get();
        } else {
            $fk_company = Auth::user()->fk_company;
            $tasks = DB::table('pr_ms_tasks')
                ->select('*', 'pr_ms_tasks.is_default AS ms_tasks_default')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);

                })
                ->leftJoin('pr_tasks_pair', 'id_pair', '=', 'fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);


            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                });

            }
            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_projects_rows.is_checked_user', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0');
                });

            }

            if ($fk_attach == 1) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);

                $tasks = $tasks->whereIn('id_ms_task', $attachments);
            }
            if ($fk_attach == 2) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);
                $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
            }

            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }
            if ($fk_pair === null) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', null)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });

            }
            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', '%' . $meta_tags . '%');
            }
            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', '%' . $address_code . '%');
            }
            if (intval($color) > 0) {
                $color_task = Task_colors::getFirstById($color);
                if (count($color_task) > 0) {
                    $tasks = $tasks->where('pr_projects_rows.task_color', 'like', $color_task->code_color);
                }
            }


            if ($is_default == 2) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '0');
            } elseif ($is_default == 1) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
            } elseif ($is_default == 3) {
                $tasks = $tasks->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.fk_company', '=', $fk_company)
                        ->where('pr_ms_tasks.fk_project', '=', $id_project);

                });
            } else {
                $tasks = $tasks->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_default', '=', '0')
                        ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                        ->where('pr_ms_tasks.fk_project', '=', $id_project);

                });
            }
            if ($fk_priority > 0) {
                $tasks = $tasks->where(function ($query1) use ($fk_priority) {
                    $query1->where('pr_ms_tasks.lvl_priority', '=', $fk_priority)
                        ->orWhere('pr_projects_rows.task_priority', '=', $fk_priority);
                });

            }

            $tasks = $tasks->orderBy('pr_projects_rows.task_proj_order', 'asc')
                ->orderBy('pr_ms_tasks.task_order', 'asc')
                ->orderBy('pr_ms_tasks.is_default', 'desc')
                ->get();
        }
        return $tasks;
    }

    public static function getTasksByStructureIndustry($id_module_structure, $id_industry, $id_project = 0, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0)
    {
        if ($id_project == 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->select('*', 'pr_ms_tasks.is_default AS ms_tasks_default')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->join('pr_industry_ms', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_tasks_pair', 'pr_tasks_pair.id_pair', '=', 'pr_ms_tasks.fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);
            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                });
            }
            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0');
                });
            }
            if ($is_default == 2) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '0');
            }
            if ($is_default == 1) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
            }
            if ($fk_attach == 1) {
                $attachments = Industry_attach::getTasksWithAttachments($id_module_structure, $id_industry);
                $tasks = $tasks->whereIn('id_ms_task', $attachments);
            }
            if ($fk_attach == 2) {
                $attachments = Industry_attach::getTasksWithAttachments($id_module_structure, $id_industry);
                if($attachments !== null) {
                    $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
                }
            }
            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }
            if ($fk_pair === null) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', null)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });
            }
            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', "%$meta_tags%");
            }
            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', "%$address_code%");
            }
            if ($fk_priority > 0) {
                $tasks = $tasks->where('pr_ms_tasks.lvl_priority', '=', $fk_priority);
            }
            $tasks = $tasks->orderBy('pr_industry_ms.ind_order', 'asc')
                ->orderBy('pr_ms_tasks.task_order', 'asc')
                ->get();
        } else {
            $fk_company = Auth::user()->fk_company;
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->join('pr_industry_ms', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->leftJoin('pr_tasks_pair', 'pr_tasks_pair.id_pair', '=', 'pr_ms_tasks.fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);

            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                });

            }
            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_projects_rows.is_checked_user', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0');
                });
            }

            if ($fk_attach == 1) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);
                $tasks = $tasks->whereIn('id_ms_task', $attachments);

            }
            if ($fk_attach == 2) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);
                $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
            }
            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }
            if ($fk_pair === null) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', null)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });
            }
            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', '%' . $meta_tags . '%');
            }
            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', '%' . $address_code . '%');
            }
            if ($fk_priority > 0) {
                $tasks = $tasks->where(function ($query1) use ($fk_priority) {
                    $query1->where('pr_ms_tasks.lvl_priority', '=', $fk_priority)
                        ->orWhere('pr_projects_rows.task_priority', '=', $fk_priority);
                });
            }
            if (intval($color) > 0) {
                $color_task = Task_colors::getFirstById($color);
                if (count($color_task) > 0) {
                    $tasks = $tasks->where('pr_projects_rows.task_color', 'like', $color_task->code_color);
                }
            }
            if ($is_default == 2) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '0')
                    ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                    ->where('pr_ms_tasks.fk_project', '=', $id_project);
            } elseif ($is_default == 1) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
            } else {
                $tasks = $tasks->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_default', '=', '1')
                        ->orWhere(function ($query) use ($fk_company, $id_project) {
                            $query->where('pr_ms_tasks.is_default', '=', '0')
                                ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                                ->where('pr_ms_tasks.fk_project', '=', $id_project);
                        });
                });
            }

            $tasks = $tasks->orderBy('pr_projects_rows.task_proj_order', 'asc')
                ->orderBy('pr_industry_ms.ind_order', 'asc')
                ->orderBy('pr_ms_tasks.task_order', 'asc')
                ->groupBy('pr_ms_tasks.id_ms_task')
                ->get();
        }
        return $tasks;
    }

    public static function getTasksByStructureIndustry1($id_module_structure, $id_industry, $id_project = 0, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0)
    {
        if ($id_project == 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->join('pr_industry_ms', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_tasks_pair', 'pr_tasks_pair.id_pair', '=', 'pr_ms_tasks.fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);
            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                });
            }
            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0');
                });
            }
            if ($is_default == 2) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '0');
            }
            if ($is_default == 1) {
                $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
            }
            if ($fk_attach == 1) {
                $attachments = Software_attach::getTasksWithAttachments($id_module_structure);
                $tasks = $tasks->whereIn('id_ms_task', $attachments);
            }
            if ($fk_attach == 2) {
                $attachments = Software_attach::getTasksWithAttachments($id_module_structure);
                $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
            }
            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }
            if ($fk_pair === null) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', null)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });
            }
            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', "%$meta_tags%");
            }
            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', "%$address_code%");
            }
            if ($fk_priority > 0) {
                $tasks = $tasks->where('pr_ms_tasks.lvl_priority', '=', $fk_priority);
            }
            $tasks = $tasks->orderBy('pr_industry_ms.ind_order', 'asc')
                ->orderBy('pr_ms_tasks.task_order', 'asc')
                ->get();
        } else {
            $fk_company = Auth::user()->fk_company;
            $project = Project::getFirstById($id_project);
            $projectCreatedAt = strtotime($project->created_at);
            $module = Module_structure::getFirstById($id_module_structure);
            $moduleLockedAt = strtotime($module->locked_at);

            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->join('pr_industry_ms', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->leftJoin('pr_tasks_pair', 'pr_tasks_pair.id_pair', '=', 'pr_ms_tasks.fk_pair')
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure);

            if ($fk_checked == 1) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1')
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '1');
                });
            }

            if ($fk_checked == 2) {
                $tasks = $tasks->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '0')
                        ->where('pr_ms_tasks.is_mandatory', '=', '0')
                        ->where('pr_projects_rows.is_checked_user', '=', null)
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '0');
                });
            }

            if ($fk_attach == 1) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);
                $tasks = $tasks->whereIn('id_ms_task', $attachments);
            }

            if ($fk_attach == 2) {
                $attachments = Projects_attach::getTasksWithAttachments($id_project);
                $tasks = $tasks->whereNotIn('id_ms_task', $attachments);
            }

            if ($fk_pair > 0) {
                $tasks = $tasks->where('pr_ms_tasks.fk_pair', '=', $fk_pair);
            }

            if ($fk_pair === null || $fk_pair == 0) {
                $tasks = $tasks->where(function ($query) {
                    $query->where('pr_ms_tasks.fk_pair', '=', NULL)
                        ->orWhere('pr_ms_tasks.fk_pair', '=', 0);
                });
            }

            if (strlen($meta_tags) > 1) {
                $tasks = $tasks->where('pr_tasks.meta_tags', 'like', '%' . $meta_tags . '%');
            }

            if (strlen($address_code) > 1) {
                $tasks = $tasks->where('pr_tasks.address_code', 'like', '%' . $address_code . '%');
            }

            if ($fk_priority > 0) {
                $tasks = $tasks->where(function ($query1) use ($fk_priority) {
                    $query1->where('pr_projects_rows.task_priority', '=', $fk_priority);
                        // ->where('pr_ms_tasks.lvl_priority', '=', $fk_priority);
                });
            }

            if (intval($color) > 0) {
                $color_task = Task_colors::getFirstById($color);
                if (count($color_task) > 0) {
                    $tasks = $tasks->where('pr_projects_rows.task_color', 'like', $color_task->code_color);
                }
            }

            if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($moduleLockedAt > $projectCreatedAt)) {
                if ($is_default == 2) {
                    $tasks = $tasks
                        ->where('pr_ms_tasks.is_default', '=', '0')
                        ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                        ->where('pr_ms_tasks.fk_project', '=', $id_project);
                } elseif ($is_default == 1) {
                    $tasks = $tasks
                        ->where('pr_ms_tasks.fk_company', '=', null)
                        ->where('pr_ms_tasks.fk_project', '=', null);
                    // $tasks = $tasks->where('pr_ms_tasks.is_default', '=', '1');
                } else {
                    // $tasks = $tasks->where(function ($query1) use ($fk_company, $id_project) {
                    //     $query1->where('pr_ms_tasks.is_from_industry', '=', '1')
                    //         // ->orWhere('pr_ms_tasks.fk_project', '=', null)
                    //         ->orWhere(function ($query) use ($fk_company, $id_project) {
                    //             $query
                    //                 ->where('pr_projects_rows.fk_project', '=', $id_project);
                    //         });
                    // });
                }
            }

            $tasks = $tasks->orderBy('pr_projects_rows.task_proj_order', 'asc')
                ->orderBy('pr_industry_ms.ind_order', 'asc')
                ->orderBy('pr_ms_tasks.task_order', 'asc')
                ->groupBy('pr_ms_tasks.id_ms_task')
                ->get();
        }
        return $tasks;
    }

    public static function getTasksByStructureChecked($id_module_structure, $id_project = 0)
    {
        if ($id_project == 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query3) {
                    $query3->where('pr_projects_rows.is_checked_user', '=', 1)
                        ->orWhere('pr_ms_tasks.is_checked', '=', 1);
                })
                ->orderBy('pr_ms_tasks.is_default', 'desc')
                ->get();
        } else {
            $fk_company = Auth::user()->fk_company;
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_from_industry', '=', '1')
                        ->orWhere('pr_ms_tasks.fk_project', '=', null)
                        ->orWhere(function ($query) use ($fk_company, $id_project) {
                            $query->where('pr_projects_rows.fk_project', '=', $id_project);
                        });
                })
                ->where(function ($query3) {
                    $query3->where('pr_projects_rows.is_checked_user', '=', 1)
                        ->orWhere('pr_ms_tasks.is_checked', '=', 1)
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', 1);
                })
                ->orderBy('pr_ms_tasks.is_default', 'desc')
                ->get();
        }
        return $tasks;
    }

    public static function getTasksByStructureChecked1($id_module_structure, $id_project = 0, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0)
    {
        if ($id_project == 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query3) {
                    $query3->where('pr_projects_rows.is_checked_user', '=', 1)
                        ->orWhere('pr_ms_tasks.is_checked', '=', 1);
                });
                if ($fk_checked == 1) {
                    $tasks = $tasks->where(function ($query2) {
                        $query2->where('pr_ms_tasks.is_checked', '=', '1')
                            ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                    });
                }
                if ($fk_checked == 2) {
                    $tasks = $tasks->where(function ($query2) {
                        $query2->where('pr_ms_tasks.is_checked', '=', '0')
                            ->where('pr_ms_tasks.is_mandatory', '=', '0');
                    });
                }
                $tasks = $tasks->orderBy('pr_ms_tasks.is_default', 'desc')
                      ->get();
        } else {
            $fk_company = Auth::user()->fk_company;
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_default', '=', '1')
                        ->orWhere(function ($query) use ($fk_company, $id_project) {
                            $query->where('pr_ms_tasks.is_default', '=', '0')
                                ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                                ->where('pr_ms_tasks.fk_project', '=', $id_project);
                        });
                })
                ->where(function ($query3) {
                    $query3->where('pr_projects_rows.is_checked_user', '=', 1)
                        ->orWhere('pr_ms_tasks.is_checked', '=', 1);
                });

                if ($fk_checked == 1) {
                    $tasks = $tasks->where(function ($query2) {
                        $query2->where('pr_ms_tasks.is_checked', '=', '1')
                            ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');;
                    });

                }
                if ($fk_checked == 2) {
                    $tasks = $tasks->where(function ($query2) {
                        $query2->where('pr_ms_tasks.is_checked', '=', '0')
                            ->where('pr_ms_tasks.is_mandatory', '=', '0');
                    });

                }
                $tasks = $tasks->orderBy('pr_ms_tasks.is_default', 'desc')
                                ->get();
        }
        return $tasks;
    }

    public static function getOneTaskByKey($id_ms_task, $id_project)
    {

        $tasks = DB::table('pr_ms_tasks')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                $join->where('pr_projects_rows.fk_project', '=', $id_project);
            })
            ->leftJoin('pr_tasks_pair', 'pr_ms_tasks.fk_pair', '=', 'pr_tasks_pair.id_pair')
            ->where('id_ms_task', '=', $id_ms_task)
            ->first();
        return $tasks;
    }


    public static function getCheckedTasks($id_module_structure, $fk_industry, $id_project)
    {
        $fk_company = Auth::user()->fk_company;
        if ($fk_industry > 0) {
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->join('pr_industry_ms', 'pr_ms_tasks.id_ms_task', '=', 'pr_industry_ms.fk_ms_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('fk_industry', '=', $fk_industry)
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_default', '=', '1')
                        ->orWhere(function ($query) use ($fk_company, $id_project) {
                            $query->where('pr_ms_tasks.is_default', '=', '0')
                                ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                                ->where('pr_ms_tasks.fk_project', '=', $id_project);
                        });
                })
                ->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');
                })
                ->get();
        } else {
            $tasks = DB::table('pr_ms_tasks')
                ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);

                })
                ->where('pr_ms_tasks.fk_module_structure', '=', $id_module_structure)
                ->where(function ($query1) use ($fk_company, $id_project) {
                    $query1->where('pr_ms_tasks.is_default', '=', '0')
                        ->where('pr_ms_tasks.fk_company', '=', $fk_company)
                        ->where('pr_ms_tasks.fk_project', '=', $id_project);
                })
                ->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_checked', '=', '1')
                        ->orWhere('pr_projects_rows.is_checked_user', '=', '1')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');
                })
                ->get();
        }

        return count($tasks);
    }

    public static function isTaskCheckedDefaultProject($id_ms_task, $id_project, $is_empty_task = 0)
    {
        if ($is_empty_task == 1) {
            $tasks = DB::table('pr_ms_tasks')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);
                })
                ->where('id_ms_task', '=', $id_ms_task)
                ->where('pr_ms_tasks.is_mandatory', '=', '1')
                ->first();
        } else {
            $tasks = DB::table('pr_ms_tasks')
                ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                    $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                    $join->where('pr_projects_rows.fk_project', '=', $id_project);

                })
                ->where('id_ms_task', '=', $id_ms_task)
                ->where(function ($query2) {
                    $query2->where('pr_ms_tasks.is_default', '=', '0')
                        ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');
                })
                ->first();
        }

        if (count($tasks) > 0) {
            return 'disabled';
        } else {
            return '';
        }
    }

    public static function isTaskCheckedProject($id_ms_task, $id_project)
    {
        $fk_company = Auth::user()->fk_company;
        $tasks = DB::table('pr_ms_tasks')
            ->join('pr_tasks', 'pr_ms_tasks.fk_task', '=', 'pr_tasks.id_task')
            ->leftJoin('pr_projects_rows', function ($join) use ($id_project) {
                $join->on('pr_projects_rows.fk_ms_task', '=', 'pr_ms_tasks.id_ms_task');
                $join->where('pr_projects_rows.fk_project', '=', $id_project);

            })
            ->where('id_ms_task', '=', $id_ms_task)
            // ->where(function ($query1) use ($fk_company, $id_project) {
            //     $query1->where('pr_ms_tasks.is_default', '=', '1')
            //         ->orWhere(function ($query) use ($fk_company, $id_project) {
            //             $query->where('pr_ms_tasks.is_default', '=', '0')
            //                 ->where('pr_ms_tasks.fk_company', '=', $fk_company)
            //                 ->where('pr_ms_tasks.fk_project', '=', $id_project);
            //         });
            // })
            ->where(function ($query2) {
                $query2->where('pr_ms_tasks.is_checked', '=', '1')
                    ->orWhere('pr_projects_rows.is_checked_user', '=', '1')
                    ->orWhere('pr_ms_tasks.is_mandatory', '=', '1');
            })
            ->first();
        if (count($tasks) > 0) {
            return 'checked';
        } else {
            return '';
        }
    }

    public static function getCheckedTaskByIndustry($fk_industry)
    {
        $tasks = DB::table('pr_ms_tasks')
            ->join('pr_industry_ms', 'id_ms_task', '=', 'pr_industry_ms.fk_ms_task')
            ->where('is_checked', '=', '1')
            ->where('fk_industry', '=', $fk_industry)
            ->get();

        return $tasks;
    }

    public static function getTasksLinks($id_task, $fk_software = null)
    {
        if ($fk_software == null) {
            $ms_task = DB::table('pr_ms_tasks')
                ->select('*')
                ->join('pr_module_structure', 'pr_module_structure.id_module_structure', '=', 'pr_ms_tasks.fk_module_structure')
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_task', '=', $id_task)->get();
        } else {
            $ms_task = DB::table('pr_ms_tasks')
                ->select('*')
                ->join('pr_module_structure', 'pr_module_structure.id_module_structure', '=', 'pr_ms_tasks.fk_module_structure')
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_task', '=', $id_task)->get();
        }

        return $ms_task;


    }

}
