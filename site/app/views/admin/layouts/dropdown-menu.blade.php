<?php
if (Auth::check()) {
    $id_user = Auth::user()->id;
}
?>

<li class="dropdown">
    <!--All task drop down start-->
    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
        <i class="fa fa-retweet"></i> {{trans('messages.nav_choose_soft')}}
    </a>
    <div class="dropdown-menu right top-dropDown-1">
        <h4>{{trans('messages.nav_choose_soft')}}</h4>
        <ul class="goal-item">
            {{--<li><a href="{{URL::to('admin_home')}}">Administration</a></li>--}}
            <li><a href="{{URL::to('/')}}">Applikation</a></li>
        </ul>
    </div>
</li>
<li>
    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
        <i class="fa fa-globe"></i> {{trans('messages.language')}}
    </a>
    <div class="dropdown-menu right top-dropDown-1">
        <h4>{{trans('messages.language')}}</h4>
        <ul class="goal-item">
            <li>  {{link_to_route('language.select', 'English', array('en'))}}</li>
            <li>  {{link_to_route('language.select', 'Deutsch', array('de'))}}</li>
            {{--<li>  {{link_to_route('language.select', 'Romana', array('ro'))}}</li>--}}
        </ul>
    </div>
    <!--All task drop down end-->
</li>
<li class="dropdown">
    <!--All task drop down start-->
    <?php $image = User::getLogoUser($id_user);    ?>
    <a class="dropdown-toggle with_image" data-toggle="dropdown" href="javascript:void(0)">
        <div class="mega-name">
            <img src="{{ URL::asset($image)}}" alt="image" class="dropdown-image">
            {{--{{$name_user}}--}}
            <i class="fa fa-angle-down"></i>
        </div>
    </a>
    <div class="dropdown-menu right top-dropDown-1">
        <h4>{{trans('messages.nav_profile')}}</h4>
        <ul class="goal-item">
            <li><a href="{{URL::to('members/'.$id_user)}}">{{trans('messages.nav_edit_profile')}} </a></li>
            {{--            <li><a href="{{URL::to('password_admin')}}">{{trans('messages.nav_change_password')}}</a></li>--}}
            <li><a href="{{URL::to('logout')}}"><i class="fa fa-sign-out"></i>{{trans('messages.nav_logout')}}</a></li>
        </ul>
    </div>
    <!--All task drop down end-->
</li>
