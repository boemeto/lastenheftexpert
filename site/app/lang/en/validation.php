<?php
return LanguageTrans::getList('en', 2);
return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted"                   => "The :attribute must be accepted.",
    "active_url"                 => "The :attribute is not a valid URL.",
    "after"                      => "The :attribute must be a date after :date.",
    "alpha"                      => "The :attribute may only contain letters.",
    "alpha_dash"                 => "The :attribute may only contain letters, numbers, and dashes.",
    "alpha_num"                  => "The :attribute may only contain letters and numbers.",
    "array"                      => "The :attribute must be an array.",
    "before"                     => "The :attribute must be a date before :date.",
    "between"                    => array(
        "numeric" => "The :attribute must be between :min and :max.",
        "file"    => "The :attribute must be between :min and :max kilobytes.",
        "string"  => "The :attribute must be between :min and :max characters.",
        "array"   => "The :attribute must have between :min and :max items.",
    ),
    "boolean"                    => "The :attribute field must be true or false.",
    "confirmed"                  => "The :attribute confirmation does not match.",
    "date"                       => "The :attribute is not a valid date.",
    "date_format"                => "The :attribute does not match the format :format.",
    "different"                  => "The :attribute and :other must be different.",
    "digits"                     => "The :attribute must be :digits digits.",
    "digits_between"             => "The :attribute must be between :min and :max digits.",
    "email"                      => "The :attribute must be a valid email address.",
    "exists"                     => "The selected :attribute is invalid.",
    "image"                      => "The :attribute must be an image.",
    "in"                         => "The selected :attribute is invalid.",
    "integer"                    => "The :attribute must be an integer.",
    "ip"                         => "The :attribute must be a valid IP address.",
    "login"                      => "Incorrect email or password",
    "max"                        => array(
        "numeric" => "The :attribute may not be greater than :max.",
        "file"    => "The :attribute may not be greater than :max kilobytes.",
        "string"  => "The :attribute may not be greater than :max characters.",
        "array"   => "The :attribute may not have more than :max items.",
    ),
    "mimes"                      => "The :attribute must be a file of type: :values.",
    "min"                        => array(
        "numeric" => "The :attribute must be at least :min.",
        "file"    => "The :attribute must be at least :min kilobytes.",
        "string"  => "The :attribute must be at least :min characters.",
        "array"   => "The :attribute must have at least :min items.",
    ),
    "not_in"                     => "The selected :attribute is invalid.",
    "numeric"                    => "The :attribute must be a number.",
    "regex"                      => "The :attribute format is invalid.",
    "required"                   => "The :attribute field is required.",
    "required_if"                => "The :attribute field is required when :other is :value.",
    "required_with"              => "The :attribute field is required when :values is present.",
    "required_with_all"          => "The :attribute field is required when :values is present.",
    "required_without"           => "The :attribute field is required when :values is not present.",
    "required_without_all"       => "The :attribute field is required when none of :values are present.",
    "same"                       => "The :attribute and :other must match.",
    "size"                       => array(
        "numeric" => "The :attribute must be :size.",
        "file"    => "The :attribute must be :size kilobytes.",
        "string"  => "The :attribute must be :size characters.",
        "array"   => "The :attribute must contain :size items.",
    ),
    "unique"                     => "The :attribute has already been taken.",
    "url"                        => "The :attribute format is invalid.",
    "timezone"                   => "The :attribute must be a valid zone.",
    "confirm_user"               => "User created successfully. Please check your E-mail and activate your account.",
    "confirmation_question"      => "Are you sure you want to delete this item from the database?",
    "delete_success"             => "Deleted successfully",
    "delete_error"               => "This item cannot be deleted",
    "add_success"                => "Added successfully",
    "add_error"                  => "This item cannot be added",
    "edit_success"               => "Modified successfully",
    "edit_error"                 => "This item cannot be modified",
    "link_success"               => "Linked successfully",
    "link_error"                 => "This items cannot be linked",
    "alpha_spaces"               => "The :attribute may only contain letters and spaces.",
    "alpha_num_spaces"           => "The :attribute may only contain letters, numbers and spaces.",
    "no_company"                 => "No results found.",
    "no_value"                   => "Please fill out this field",
    "please_activate"            => "Please activate your account",
    "already_activated"          => "Your account is activated. Please log in",
    "confirm_activation"         => "Your account is activated. Please log in",
    "message_success_add"        => "The data was added successfully",
    "message_success_edit"       => "The data was modified successfully",
    "message_success_delete"     => "The data was deleted successfully",
    "message_success_order"      => "The data was reorderd successfully",
    "admin_activation"           => "Please wait for admin validation",
    "company_exists"             => "Your company already exists. Please select a Headquarter",
    "password_wrong"             => "The password is incorrect",
    "message_affiliation_exists" => "Tis affiliation already exists",
    "industry_unchecked"         => "The task was removed from selected industry",
    "industry_checked"           => "The task was added to selected industry",
    "industry_unchecked_all"     => "All the tasks from this module were removed from the selected industry",
    "industry_checked_all"       => "All the tasks from this module were added to the selected industry",
    "company_blocked"            => "Your company is not yet approved by the software administrator",


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'attribute-name' => array(
            'rule-name' => 'custom-message',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(),

);
