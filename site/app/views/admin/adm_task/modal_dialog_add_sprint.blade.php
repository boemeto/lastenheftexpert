<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    Neuer Sprint
    <div class="clear"></div>
</div>
{{ Form::open(['route'=>'adm_sprint.store'])}}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">

                {{  Form::text('name_sprint','',['class'=>'form-control','placeholder'=>'Sprintname'])  }}
            </div>
            <div class="form-group">
                {{  Form::textarea('comment','',['class'=>'form-control','placeholder'=>'Beschreibung'])  }}
                {{  Form::hidden('fk_release',$id_release)  }}
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn ls-light-blue-btn ">
        {{trans('messages.act_save')}}
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}


 