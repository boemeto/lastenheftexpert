{{ Form::open(['action'=>'MembersController@store','id'=>'','enctype'=>"multipart/form-data"]) }}

<div class="col-md-12">
    <div class="title_line">  {{trans('messages.act_add')}}  {{trans('messages.nav_user')}}  {{trans('messages.l_for')}}   {{$headquarter->name_headquarter}} </div>
</div>
<div class="col-md-12">
    <div class="form-group has-feedback">
        {{ Form::select("fk_personal_title",$personal_titles,"",[ "placeholder"=>"Name",'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }}
        {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::select("fk_title",$titles,"",[ "placeholder"=>"Name", "class"=>"form-control",'id'=>'fk_title', "required"]) }}
        {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::text("first_name","",[ "placeholder"=>"First Name", "class"=>"form-control",'id'=>'first_name', "required"]) }}
        {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::text("last_name","",[ "placeholder"=>"Last Name", "class"=>"form-control",'id'=>'last_name', "required"]) }}
        {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::email("email","",["placeholder"=>"Email", "class"=>"form-control", 'id'=>'email',"required"]) }}
        {{ $errors->first("email",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::password("password",[ "placeholder"=>"Password", "class"=>"form-control",'id'=>'password', "required"]) }}
        {{ $errors->first("password",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::password("password_confirm",[ "placeholder"=>"Password confirm", "class"=>"form-control",'id'=>'password_confirm', "required"]) }}
        {{ $errors->first("password_confirm",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group">
        <input id="file-user" type="file" name="logo_user" multiple="false">
    </div>
    {{ Form::hidden("fk_company",$headquarter->fk_company) }}
    {{ Form::hidden("fk_headquarter",$headquarter->id_headquarter) }}
    {{ Form::hidden("redirect_link",'acompany') }}
    <br>
</div>
<br>
<div class="col-md-6">
    <button type="submit" class='btn ls-light-blue-btn btn-block'>
        {{trans('messages.act_save')}}
    </button>
</div>
<div class="col-md-6">
    <button type="button" class='btn btn-danger btn-block' onclick="hide_modal_box()">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{ Form::close() }}
