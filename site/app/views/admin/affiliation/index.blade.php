@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">


            <h3 class="box-title"> Affiliates </h3>

        </div>

        <div class="box-body">

            @if(count($affiliated_companies) >0)
                <table class="table table-bordered table-responsive">
                    <tr>
                        <th>Company</th>
                        <th>Affiliation Company</th>
                        <th>Is Pending</th>
                    </tr>
                    @foreach($affiliated_companies as $row)
                        <tr>
                            <td>{{$affiliated_ids[$row->fk_company]}}</td>
                            <td>{{$affiliated_ids[$row->fk_affiliation]}}</td>
                            <td>{{ ($row->is_pending == 1)?'Ja':'Nein'}}</td>

                        </tr>
                    @endforeach
                </table>
            @endif


        </div>
        <!-- /.box-header -->


    </div>
    <!-- /.box-body -->
    </div>


@stop