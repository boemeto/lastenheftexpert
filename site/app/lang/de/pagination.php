<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Zurück',
    'next' => 'Weiter &raquo;',
    "emptyTable" => "Keine Daten in der Tabelle vorhanden",
    "info" => "_START_ bis _END_ von _TOTAL_ Einträgen",
    "infoEmpty" => "0 bis 0 von 0 Einträgen",
    "infoFiltered" => "(gefiltert von _MAX_ Einträgen)",
    "infoPostFix" => "",
    "thousands" => ".",
    "lengthMenu" => "_MENU_ Einträge anzeigen",
    "loadingRecords" => "Wird geladen...",
    "processing" => "Bitte warten...",
    "search" => "Suchen",
    "zeroRecords" => "Keine Einträge vorhanden.",
    "page" => "Seite",
    "of" => "von",
    "paginate" => [
        "first" => "Erste",
        "previous" => "Zurück",
        "next" => "Nächste",
        "last" => "Letzte"
    ],
    "aria" => [
        "sortAscending" => ":attribute aktivieren, um Spalte aufsteigend zu sortieren",
        "sortDescending" => ":attribute aktivieren, um Spalte absteigend zu sortieren"
    ]

);
