@extends('front.layouts.default')
@section('dropdown-menu')
@include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
@include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
        <!--Form Wizard CSS Start-->
<link href="{{ URL::asset('assets/css/plugins/custom_smart_wizard.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/jquery.datetimepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/js/chosen/chosen.css') }}">
        <!--Form Wizard CSS End-->
@stop
@section('footer_scripts')
        <!--Chosen JS-->
    <script src="{{ URL::asset('assets/js/chosen/chosen.jquery.js') }}"></script>
        <!--Form Wizard JS Start-->
    <script src="{{ URL::asset('assets/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/formWizard.js') }}"></script>
    <!--Form Wizard JS End-->
    <!-- Date & Time Picker Library Script Start -->
    <script src="{{ URL::asset('assets/js/jquery.datetimepicker.js') }}"></script>
    <!-- Date & Time Picker Library Script End -->
    <script type="text/javascript">
        $(document).on('click', '.payment_options', function() {
            if($(this).hasClass('grey-backg-color')) {
                $(this).removeClass('grey-backg-color');
                $('.create-project-btn').css('display', 'none');
                $(this).find('.check-img').hide();
            } else {
                var form_attr = $(this).attr('data-form-attr');
                $('.payment_options').each(function() {
                    $(this).removeClass('grey-backg-color');
                });
                $(this).addClass('grey-backg-color');
                $('.check-img').each(function() {
                    $(this).hide();
                });
                $(this).find('.check-img').show();
                $('.create-project-btn').css('display', 'inline-block');
                $('.create-project-btn').attr('form-submit', form_attr);
            }
        });

        $(document).on('click', '.panel-package', function() {
            if($(this).hasClass('style_preview_package_active')) {
                $(this).removeClass('style_preview_package_active');
                $('.payment-details').hide();
                $('.project_options').find('.left-side').css('border-right', '0px');
            } else {
                $('.panel-package').each(function() {
                    $(this).removeClass('style_preview_package_active');
                });
                $('.project_options').find('.left-side').css('border-right', '1px solid #e0e0e0');
                $(this).addClass('style_preview_package_active');
                $('.payment-details').show();

                var package_name = $(this).find('.package-name').text();
                var package_price = parseInt(($(this).find('.package-price').attr('data-pack-price')).toLocaleString('de-DE'));
                var package_vat_value = package_price * 0.19;
                var package_total_value = package_price + package_vat_value;
                var stripe_price = $(this).find('.package-price').attr('data-pack-price');
                var package_period = $(this).find('.package-months').text();
                var package_description= $(this).find('.package-description').text();
                var package_tree = $(this).find('.package-tree').text();
                var package_id = $(this).attr('data-package-id');
                $('.selected-package-name').text(package_name);
                $('.selected-package-price').text(package_price.toLocaleString('de-DE') + ' €');
                $('.selected-package-price').attr('data-package-price', stripe_price);
                $('.selected-vat-value').text(package_vat_value.toLocaleString('de-DE'));
                $('.selected-total-value').text(package_total_value.toLocaleString('de-DE'));
                $('.selected-package-period').text(package_period);
                $('.selected-package-description').text(package_description);
                $('.selected-package-tree').text(package_tree);
                $('input[name=id_package]').each(function() {
                    $(this).val(package_id);
                });
                $('.stripe-payment').attr('data-amount', package_price*100000);

                $('.project_options').find('.package-row-preview').each(function() {
                    $(this).remove();
                });

                $(this).find('.package-row').each(function() {
                    var package_row = $(this).find('.package-row-name').text() + ' - ' + $(this).find('.package-row-value').text();
                    $('.project_options').find('.selected-payment-details-end').before(
                        '<div class="package-row-preview"><div class="check-img-details"></div><div class="selected-package-description">' + package_row + '</div></div>'
                    );
                });
            }

        });

        $(document).on('click', '.create-project-btn', function(){
        // $('.create-project-btn').click(function() {
            var fk_industry = $('#FormNewProject').find('input[name=fk_industry]').val();
            var name_project = $('#FormNewProject').find('input[id=project_name]').val();
            var nr_employees = $('#FormNewProject').find('input[name=nr_employees]').val();
            var nr_users = $('#FormNewProject').find('input[name=nr_users]').val();
            var max_budget = $('#FormNewProject').find('select[name=max_budget]').val();
            var start_project = $('#FormNewProject').find('input[name=start_project]').val();
            var implement_time = $('#FormNewProject').find('input[name=implement_time]').val();
            var fk_user = [];
            var user_class = $('#FormNewProject').find('.bootstrap-switch-on');
            for (var i = 0; i < user_class.length; i++)  {
                fk_user[i] = $(user_class[i]).find('.fk_user').val();
            }

            $('#paypal_payment').append('<input type="hidden" name="fk_industry" value="' + fk_industry + '"/>');
            $('#sofort_payment').append('<input type="hidden" name="fk_industry" value="' + fk_industry + '"/>');
            $('#bank_transfer_payment').append('<input type="hidden" name="fk_industry" value="' + fk_industry + '"/>');

            validation = true;
            validation *= checkedFieldCheckbox('terms_and_cond');
            validation *= emptyField('project_name');
            validation *= emptySelectIndustry('selected-industry');

            if (validation == false) {
                // prevent submitting
                event.preventDefault();
                $("html, body").animate({ scrollTop: 400 }, "slow");
                return false;
            }

            if ($(this).attr('form-submit') == 'submit-card') {
                $('.stripe-payment').trigger('click');
                var $button = $('.stripe-payment'),
                    $form = $button.parents('form');

                var opts = $.extend({}, $button.data(), {
                    token: function(result) {
                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                    }
                });
                opts.amount = parseInt($('.selected-package-price').attr('data-package-price'))*119;
                StripeCheckout.open(opts);
            }

            if ($(this).attr('form-submit') == 'submit-paypal') {
                $('#paypal_payment').find('input[name=name_project]').val(name_project);
                $('#paypal_payment').find('input[name=nr_employees]').val(nr_employees);
                $('#paypal_payment').find('input[name=nr_users]').val(nr_users);
                $('#paypal_payment').find('input[name=max_budget]').val(max_budget);
                $('#paypal_payment').find('input[name=start_project]').val(start_project);
                $('#paypal_payment').find('input[name=implement_time]').val(implement_time);
                $('#paypal_payment').find('input[name=fk_user]').val(JSON.stringify(fk_user));
                $('#paypal_payment').submit();
            }

            if ($(this).attr('form-submit') == 'submit-sofort') {
                alert('sofort');
                $('#sofort_payment').find('input[name=name_project]').val(name_project);
                $('#sofort_payment').find('input[name=nr_employees]').val(nr_employees);
                $('#sofort_payment').find('input[name=nr_users]').val(nr_users);
                $('#sofort_payment').find('input[name=max_budget]').val(max_budget);
                $('#sofort_payment').find('input[name=start_project]').val(start_project);
                $('#sofort_payment').find('input[name=implement_time]').val(implement_time);
                $('#sofort_payment').find('input[name=fk_user]').val(JSON.stringify(fk_user));
                $('#sofort_payment').submit();
            }

            if ($(this).attr('form-submit') == 'submit-transfer') {
                $('#bank_transfer_payment').find('input[name=name_project]').val(name_project);
                $('#bank_transfer_payment').find('input[name=nr_employees]').val(nr_employees);
                $('#bank_transfer_payment').find('input[name=nr_users]').val(nr_users);
                $('#bank_transfer_payment').find('input[name=max_budget]').val(max_budget);
                $('#bank_transfer_payment').find('input[name=start_project]').val(start_project);
                $('#bank_transfer_payment').find('input[name=implement_time]').val(implement_time);
                $('#bank_transfer_payment').find('input[name=fk_user]').val(JSON.stringify(fk_user));
                $('#bank_transfer_payment').submit();
            }
        });

        $('.select-project-industry').on('ifChecked', function() {
            $('.choose_industry').css('border-color', '#CDCDCD');
        });

        $('.terms_and_cond').on('ifChecked', function() {
            $('.terms_and_cond_border').css('border-color', '#ffffff');
        });

        $(".switchCheckBox").bootstrapSwitch();
        $(document).ready(function () {
            $('.datePickerOnly').datetimepicker({
                timepicker: false,
                datepicker: true,
                format: 'd/m/Y'
            });

            jQuery('input.fk_headquarter').on('ifChanged', function (event) {
                var id = $(this).attr('id');
                var id_nr = id.replace('fk_headquarter', "");

                if (jQuery(this).prop('checked')) {
                    $('#div_users_list' + id_nr).show();
                } else {
                    $('#div_users_list' + id_nr).hide();
                }
            });

            $('.chosen-industries').chosen({
                width: '70%',
                display_selected_options: false
            });

            $('.chosen-single').find('span').text('<?php echo trans('messages.label_select_industry'); ?>');
            $('.chosen-industries').parent().each(function() {
                $(this).append('<div id="selected-industry" class="selected-industry" style="display: none;"></div>' );
            });

            $('.style_preview_package').each(function(e){
                var bgColor = $(this).attr("data-hex");
                var darkerColor = shadeColor(bgColor, -10);
                var lighterColor = shadeColor(bgColor, 10);
                $(this).find('.package-header').css('border-bottom', '1px solid ' + darkerColor);
                $(this).find('.package-body').css({'border-bottom': '1px solid ' + darkerColor, 'border-top': '1px solid ' + lighterColor});
                $(this).find('.package-footer').css('border-top', '1px solid ' + lighterColor);
            });

            function shadeColor(color, percent) {
                var R = parseInt(color.substring(1,3),16);
                var G = parseInt(color.substring(3,5),16);
                var B = parseInt(color.substring(5,7),16);

                R = parseInt(R * (100 + percent) / 100);
                G = parseInt(G * (100 + percent) / 100);
                B = parseInt(B * (100 + percent) / 100);

                R = (R<255)?R:255;
                G = (G<255)?G:255;
                B = (B<255)?B:255;

                var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
                var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
                var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

                return "#"+RR+GG+BB;
            }
        });

        $(document).on('change', '.chosen-industries', function(e, params){
            $(".selected-industry").each(function() {
                $(this).remove();
            });
            var industry_id = params.selected;
            var selected_industry = $(this).find('option[value=' + industry_id + ']').text();
            $('input[name=fk_industry]').each(function() {
                $(this).remove();
            });
            $('#FormNewProject').append('<input type="hidden" name="fk_industry" value="' + industry_id + '"/>');
            $('.chosen-single').find('span').text('<?php echo trans('messages.label_select_industry'); ?>');
            $(this).parents('.industry-select').append( '<div id="selected-industry" class="selected-industry">' + selected_industry + '</div>' );
        });

    </script>
    <script type="text/javascript">
        $("#modalTerms").on('hidden.bs.modal', function () {
            $("#modalTerms").removeData('bs.modal');
        });
        $("#modalTerms").on('shown.bs.modal', function () {
        });
    </script>

    @include('utils/validations_project')
@stop
@section('content')
    <div class="box new_project">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="panel widget light-widget">
                {{-- <div class="panel-heading ">
                    <h3 class="mgtp-10">
                        {{trans('messages.label_create_new_project')}}
                    </h3>
                </div> --}}
                <div class="panel-body">
                    {{ Form::open(['action'=>'ProjectsController@store','id'=>'FormNewProject']) }}
                    <div class="row packages_background">
                        @foreach ($packages as $package)
                                {{-- {{  "<pre>"; dd($package)}} --}}
                            <div class="select-package">
                                <div class="panel-package style_preview_package" style="background-color: {{$package->color}}" data-hex="{{$package->color}}" data-package-id="{{$package->id_package}}">
                                    <input type='radio' value='1' name='radio' id='radio1'/>
                                    <div class="row package-header">
                                        <div class="package-name">{{$package->name_package}}</div>
                                        <div class="package-details">
                                            <div class="col-xs-7 package-price" data-pack-price="{{$package->price}}">{{formatPackagePrice($package->price)}}</div>
                                            <div class="col-xs-5 package-period">
                                                <div class="row">
                                                    &euro;
                                                </div>
                                                <div class="row">/
                                                    <span class="package-months">{{$package->period}} <span>
                                                    {{trans('messages.months')}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row package-body">
                                        <p class="package-tree">{{$package->fk_type_project == 1?"with tree":'w/o tree'}}</p>
                                        <p class="package-description">{{$package->description}}</p>
                                        <?php
                                            $packages_rows = Package_row::whereFk_package($package->id_package)->get();
                                        ?>
                                        @foreach($packages_rows as $packages_row)
                                            <p class="package-row">
                                                <span class="package-row-name">{{$packages_row->name_field}}</span> -
                                                <span class="package-row-value">{{$packages_row->description_pack}}</span>
                                            </p>
                                        @endforeach
                                    </div>
                                    <div class="row package-footer">
                                        19% VAT
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row project_options">
                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 pd-10 left-side">
                            <div class="title_blue"> {{trans('messages.label_project_title')}}
                            </div>
                            <div class="form-group" style="position: relative">
                                {{Form::text('name_project','',['id'=>'project_name',
                                    'class'=>'form-control',
                                    'placeholder'=>trans('messages.label_project_title'),
                                    'required',
                                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                    "oninput"=>"this.setCustomValidity('')",
                                    "onmouseover"=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')"
                                  ])}}
                            </div>
                            <hr>
                            <div class="title_blue">   {{trans('messages.label_choose_industry')}}</div>
                            {{--Select industry type start--}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="wizard" class="swMainVertical ">
                                        <ul class="anchor">
                                            @foreach($industry_types as $row)
                                                <li>
                                                    <a href="#step-{{$row->id_industry_type}}">
                                                        <div class="stepNumber">
                                                            <i class="fa  {{$row->font_icon}}"></i>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="stepDesc">
                                                            {{$row->name_industry_type}}
                                                        </div>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="">
                                            {{--Industry List Start--}}
                                            @foreach($industry_types as $row)
                                                <div id="step-{{$row->id_industry_type}}" class="content" style="display: block;">
                                                    <?php $industries = Industry::whereFk_type($row->id_industry_type)->get(); ?>
                                                    <div class="row industry-select">
                                                        <select class="chosen-industries select" id="fk_industry">
                                                            <option></option>
                                                            @foreach($industries as $industry)
                            		                            <option value="{{$industry->id_industry}}"> {{$industry->name_industry}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="actionBar">
                                                    </div>
                                                </div>
                                            @endforeach
                                            {{--Industry List Stop--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--Select industry type stop--}}
                            <hr>
                            <div class="title_blue">   {{trans('messages.label_choose_budget')}}</div>
                            {{--Select users / employees / budget start--}}
                            <div class="row project-data">
                                <div class="col-lg-9 col-md-12">
                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5 ">
                                        <div class="form-group">
                                            {{Form::text('nr_employees','',['class'=>'form-control','placeholder'=>trans('messages.label_employees_nr')])}}
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5">
                                        <div class="form-group">
                                            {{Form::text('nr_users','',['class'=>'form-control','placeholder'=>trans('messages.label_users_nr')])}}
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5">
                                        <div class="form-group">
                                            <select class="form-control" name="max_budget">
                                                <option value="" disabled selected>{{trans('messages.label_max_budget')}}</option>
                                                @foreach($budget as $key => $value)
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5">
                                        <div class="form-group">
                                            {{Form::text('start_project','',['class'=>'form-control datePickerOnly','placeholder'=>trans('messages.label_project_start')])}}
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5">
                                        <div class="form-group">
                                            {{Form::text('implement_time','',['class'=>'form-control','placeholder'=>trans('messages.label_implementation_time')])}}
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row title_blue select-hq">  {{trans('messages.label_choose_headquarter')}} </div>
                            {{--Select users / employees / budget stop--}}
                            {{--Select Filiale Start--}}
                            <div class="row headquarters_row">
                                <div class="col-lg-9 col-md-12">
                                    @foreach($headquarters as $headquarter)
                                        <div class="col-lg-4 col-md-4 col-xs-6">
                                            <div class="headquarter_project">
                                                <div class="headquarter_details">
                                                    <span>{{$headquarter->name_headquarter}}</span> <br>
                                                    {{$headquarter->street}} {{$headquarter->street_nr}} <br>
                                                    {{$headquarter->postal_code}}, {{$headquarter->city}} <br>
                                                    {{$headquarter->country}} <br>
                                                </div>
                                                <div class="headquarter_checkbox">
                                                    {{Form::checkbox('fk_headquarter[]',$headquarter->id_headquarter,false,
                                                    ['class'=>'icheck-green fk_headquarter', 'id'=>'fk_headquarter'.$headquarter->id_headquarter])}}
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            {{--Select Filiale Stop--}}
                            <div class="row">
                                <div class="col-lg-10 col-md-12">
                                    @foreach($headquarters as $headquarter)
                                        <div class="div_users_list" id="div_users_list{{$headquarter->id_headquarter}}">
                                            <?php $users = User::getUsersByHeadquarter($headquarter->id_headquarter); ?>
                                            @foreach($users as $user)
                                                @if($user->id != Auth::user()->id)
                                                    <div class="col-lg-4 col-md-4 col-xs-6 pd-5 headquarters_user_details">
                                                        <?php $image_user = User::getLogoUser($user->id);
                                                        ?>
                                                        <div class="image_user">
                                                            <img class="img-circle" alt="friends pic"
                                                                 src="{{ URL::asset($image_user)}}">
                                                        </div>
                                                        <div class="user_details">
                                                            <div style="min-height: 70px">
                                                                <span class="span_headquarter"> {{$headquarter->name_headquarter}}</span>
                                                                <h4>{{$user->first_name}} {{$user->last_name}}</h4>
                                                                {{$user->job_user}}
                                                            </div>
                                                            <div class="user_checkbox">
                                                                {{Form::checkbox('fk_user[]',$user->id,false,
                                                                ['class'=>'switchCheckBox fk_user', 'data-size'=>"mini"])}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <?php
                              $price = $package->price;
                              $tva = 0.19 * $package->price;
                              $total = $price + $tva;
                            ?>
                            {{-- <div class="title_blue">  {{trans('messages.label_choose_more_options')}} </div> --}}
                            {{-- <div class="row">
                                <div class="col-xs-6"> --}}
                                    {{-- {{Form::checkbox('is_licitation','1',false,['class'=>'icheck-green'])}} {{trans('messages.label_is_licitation')}} --}}
                                {{-- </div>
                                <div class="col-xs-6"> --}}
                                    {{-- {{Form::checkbox('has_consultant','1',false,['class'=>'icheck-green'])}} {{trans('messages.label_has_consultant')}} --}}
                                {{-- </div>
                            </div> --}}
                            {{--Submit form start--}}
                            <hr>
                            <div class="row submit_form">
                                <div class="col-xs-1 pd-5 payment_options">
                                    {{Form::hidden('id_package')}}
                                    <input
                                        class="stripe-payment"
                                        type="submit"
                                        value="{{trans('messages.act_create_project')}}"
                                        title="pay with card"
                                        data-key="{{ Config::get('stripe.stripe.public') }}"
                                        data-amount="0"
                                        data-currency="EUR"
                                        data-name="LASTENHEFT EXPERT"
                                        data-description="Create New Project"
                                        style="display: none;"
                                    />
                                    <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                    <script>
                                      $(document).ready(function() {
                                          $(':submit').on('click', function(event) {
                                              validation = true;
                                              validation *= checkedFieldCheckbox('terms_and_cond');
                                              validation *= emptyField('project_name');
                                              validation *= emptySelectIndustry('selected-industry');
                                              if (validation == false) {
                                                  event.preventDefault(); // prevent submitting
                                                  return false;
                                              }
                                              event.preventDefault();
                                          });
                                      });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1 pd-10"></div>
                        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-5 payment-details">
                            <div class="title_blue center payment-summary"> {{trans('messages.payment_summary')}}:</div>
                            <div class="android android-basic no-hover">
                                <div class="selected-package-name"></div>
                                <div class="selected-package-price" data-package-price='0'></div>
                                <div class="selected-package-period"></div>
                                <div class="selected-payment-details">
                                    <div class="package-tree-preview">
                                        <div class="check-img-details"></div>
                                        <div class="selected-package-tree"></div>
                                    </div>
                                    <div class="package-description-preview">
                                        <div class="check-img-details"></div>
                                        <div class="selected-package-description"></div>
                                    </div>
                                    <div class="selected-payment-details-end"></div>
                                </div>
                                <div class="selected-package-vat">
                                    <span>VAT 19%: </span>
                                    <span class="selected-vat-value"></span>
                                    <span> &euro;</span>
                                </div>
                                <div class="selected-package-total">
                                    <span>Total: </span>
                                    <span class="selected-total-value"></span>
                                    <span> &euro;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="select-payment-text">{{trans('messages.label_choose_more_options')}}: </div>
                            <div class="row light-widget select-payment-options" >
                                <div class="row">
                                    <div class="row payment_options" data-form-attr="submit-paypal">
                                        <div class="payment-options-img paypal_pay paypal_pay_image_bw"></div>
                                        <div class="check-img"></div>
                                    </div>
                                    <div class="row payment_options" data-form-attr="submit-card">
                                        <div class="payment-options-img card_pay card_pay_image_bw"></div>
                                        <div class="check-img"></div>
                                    </div>
                                    <div class="row payment_options" data-form-attr="submit-transfer">
                                        <div class="payment-options-img transfer_pay transfer_pay_image_bw"></div>
                                        <div class="check-img"></div>
                                    </div>
                                    <div class="row payment_options" data-form-attr="submit-sofort">
                                        <div class="payment-options-img sofort_pay sofort_pay_image_bw"></div>
                                        <div class="check-img"></div>
                                    </div>
                                </div>
                                <br>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="terms_and_cond_border col-xs-4">
                            {{Form::checkbox('terms_and_cond', 1, false, ['class'=>'icheck-green terms_and_cond', 'id'=>'terms_and_cond', 'data_msg'=>"Terms and Conditions" ])}} {{trans('messages.label_accept_terms')}}
                            <a href="{{ URL::route('posts.show',[$terms->id_post]) }}"
                               class="" data-toggle="modal" data-target="#modalTerms">
                                {{$terms->getAttribute('name_post')}}
                            </a>
                            {{trans('messages.label_accept_terms2')}}

                        </div>
                        <div class="create-btn-container col-xs-3">
                            <div>
                                <button class="btn create-project-btn" form-submit="">
                                    {{strtoupper(trans('messages.label_create_new_project'))}}
                                </button>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <div>
                  {{ Form::open(['action'=>'PaypalPaymentController@store', 'id'=>'paypal_payment']) }}
                      {{Form::hidden('id_package')}}
                      <input type="hidden" name="fk_industry">
                      <input type="hidden" name="name_project">
                      <input type="hidden" name="nr_employees">
                      <input type="hidden" name="nr_users">
                      <input type="hidden" name="max_budget">
                      <input type="hidden" name="start_project">
                      <input type="hidden" name="implement_time">
                      <input type="hidden" name="fk_user">
                      <input class="" type="image" name="submit" alt="">
                  {{ Form::close() }}
                </div>
                <div>
                  {{ Form::open(['action'=>'SofortPaymentController@store', 'id'=>'sofort_payment']) }}
                      {{Form::hidden('id_package')}}
                      <input type="hidden" name="fk_industry">
                      <input type="hidden" name="name_project">
                      <input type="hidden" name="nr_employees">
                      <input type="hidden" name="nr_users">
                      <input type="hidden" name="max_budget">
                      <input type="hidden" name="start_project">
                      <input type="hidden" name="implement_time">
                      <input type="hidden" name="fk_user">
                      <input class="" type="image" name="submit" alt="">
                  {{ Form::close() }}
                </div>
                <div>
                  {{ Form::open(['action'=>'ProjectsController@storeInactiveProject', 'id'=>'bank_transfer_payment']) }}
                      {{Form::hidden('id_package')}}
                      <input type="hidden" name="fk_industry">
                      <input type="hidden" name="name_project">
                      <input type="hidden" name="nr_employees">
                      <input type="hidden" name="nr_users">
                      <input type="hidden" name="max_budget">
                      <input type="hidden" name="start_project">
                      <input type="hidden" name="implement_time">
                      <input type="hidden" name="fk_user">
                      <input class="" type="image" name="submit" alt="">
                  {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalTerms" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--<div class="col-xs-6">--}}
    {{--{{Form::radio('empty_project','1',false,['class'=>'icheck-green'])}} {{trans('messages.label_module_project')}}--}}


    {{--{{Form::radio('empty_project','2',true,['class'=>'icheck-green'])}} {{trans('messages.label_empty_project')}}--}}
    {{--</div>--}}
    @include('admin.alert_box.confirm')
@stop
