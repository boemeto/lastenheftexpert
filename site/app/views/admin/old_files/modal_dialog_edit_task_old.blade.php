<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    {{--<i class="fa fa-times"></i>--}}
    x
</button>
<div class="modal-header task-label-white">
    Edit task
    <div class="clear"></div>
</div>
{{ Form::model($pr_module_task,['route'=>['task.update',$pr_module_task->fk_task], 'method'=>'PATCH'])}}
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form_edit">
                <div class="form-group">
                    {{ Form::text('name_task', $pr_module_task->name_task, ['class'=>"form-control","placeholder"=>'Name Task', "required"]) }}
                </div>
                <div class="form-group">
                   <textarea name="description_task" class="summernote">
                     {{$pr_module_task->description_task}}
                  </textarea>
                </div>
                <div class="form-group">
                    {{Form::radio('edit_task',1,true)}} <label>Edit all tasks</label> |
                    {{Form::radio('edit_task',2,false)}} <label>Edit current task</label>
                </div>
                <div class="form-group">
                    {{Form::checkbox('is_default' ,1, true,['class'=>'switchCheckBox','data-size'=>"mini"])}} <label> Make default task</label><br>
                    {{Form::checkbox('is_checked' ,1, false,['class'=>'switchCheckBox','data-size'=>"mini"])}} <label> Mandatory Task</label>
                    {{Form::hidden('id_ms_task' ,$id_ms_task)}}
                </div>
            </div>
            <br><br>
            <div class="links">
                <h4>Delete task from this structure</h4>
                @foreach($tasks_links as $row)
                    <ul>
                        <li><a href="{{URL::to('delete_task_from_structure/'.$row->id_ms_task)}}" class="btn btn-round btn-danger">
                                <i class="fa fa-remove"></i>
                            </a>
                            <b> {{ Industry::getFirstById($row->fk_industry)->name_industry}}</b>
                            <ul>
                                <?php $parents = Module_structure::getModuleParents($row->id_module_structure); ?>
                                @foreach($parents as $id)
                                    <li>
                                      {{Module_structure::getModuleStructureOne($id)->id_name}}
                                        <ul>
                                            @endforeach
                                            @for($i=0;$i<count($parents);$i++)
                                        </ul>
                                    </li>
                                    @endfor
                            </ul>
                        </li>
                    </ul>
                @endforeach
                <br>
            </div>
            <br><br>
            <div class="pairs">
                <h4>Pair</h4>
                {{ Form::text('fk_pair',$pr_module_task->name_pair,[ 'placeholder'=>"Pair",
                              'class'=>"form-control" ,
                              "id"=>"task_pair"]) }}
            </div>
            <br><br>
        </div>
        <div class="col-md-6">
            {{Form::select('fk_module',$modules,'', ['class'=>'form-control select_module' ])}}
            <br>
            <div id="div_module_structure">
                Please select desired structure.
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    {{Form::submit('Save and Close',['class'=>'btn ls-light-blue-btn'])}}
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        Cancel
    </button>
</div>

{{Form::close()}}
