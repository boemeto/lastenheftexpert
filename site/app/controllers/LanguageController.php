<?php
use Illuminate\Support\MessageBag;

class LanguageController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function select($lang)
    {

        Session::put('lang', $lang);
        if (Auth::check()) {
            $id_user = Auth::user()->id;
            $user = User::getFirstById($id_user);
            $user->language = $lang;
            $user->save();
        }

        return Redirect::back();
    }

    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $key = Input::get('key');
        $language = LanguageTrans::getFirstByKey($key);
        if (count($language) > 0) {
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_edit_duplicate')]]);
            return Redirect::back()->withErrors($errors);
        }


        DB::beginTransaction();
        try {
            $list = new LanguageTrans;
            $list->fill(Input::all());
            $list->save();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_edit')]]);
            return Redirect::back()->withErrors($errors);
        }
        DB::commit();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::Back()->withErrors($errors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();
        $fk_group = 4;
        $list = LanguageTrans::getListDetails($id);
        return View::make('admin.language.show', compact('list', 'id', 'fk_group'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        DB::beginTransaction();
        try {
            $list = LanguageTrans::getFirstById($id);
            $list->fill(Input::all());
            $list->save();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_edit')]]);
            return Redirect::back()->withErrors($errors);
        }
        DB::commit();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::Back()->withErrors($errors);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $list = LanguageTrans::getFirstById($id);
        $list->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::Back()->withErrors($errors);
    }
}
