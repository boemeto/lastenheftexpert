<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 8/4/2015
 * Time: 1:45 PM
 */
class Projects_ms_order extends Eloquent
{


    public static $rules = [
        'fk_project' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_project_ms';
    protected $table = 'pr_projects_ms_order';
    protected $guarded = ['id_project_ms'];
    protected $fillable = ['fk_project', 'fk_module_structure', 'ms_proj_order'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Projects_ms_order::whereId_project_ms($id)->first();
        return $list;
    }

    public static function getFirstByKey($id, $fk_project)
    {
        $list = Projects_ms_order::where('fk_module_structure', '=', $id)
            ->where('fk_project', '=', $fk_project)
            ->first();
        return $list;
    }

}