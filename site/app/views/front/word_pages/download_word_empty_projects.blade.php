<html
    xmlns:o='urn:schemas-microsoft-com:office:office'
    xmlns:w='urn:schemas-microsoft-com:office:word'
    xmlns='http://www.w3.org/TR/REC-html40'>
    <head><title></title>

    <!--[if gte mso 9]><xml>
     <w:WordDocument>
      <w:View>Print</w:View>
      <w:Zoom>90</w:Zoom>
    </w:WordDocument>
    </xml><![endif]-->


    <style>
    p.MsoFooter, li.MsoFooter, div.MsoFooter
    {
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        /*tab-stops:center 3.0in right 4.25in;*/
        text-align: center;
        font-size:12.0pt;
    }

    @page Section1
    {
        size:8.5in 11.0in;
        margin:0in 0in 0in 0in;
        mso-header-margin:.3in;
        mso-footer-margin:.5in;
        mso-title-page:yes;
        mso-header: h1;
        mso-footer: f1;
        mso-first-header: fh1;
        mso-first-footer: ff1;
        mso-paper-source:0;
        text-align:justify;
    }

    div.Section1
    {
        page:Section1;
    }

    .Section1 {
        font-family: Calibri;
    }
    table#hrdftrtbl
    {
        margin:0in 0in 0in 200in;
        width:1px;
        height:1px;
        font-family: Calibri;
        overflow:hidden;
        text-align:justify;
    }

    .company-image {
        position: absolute;
        right: 120px;
        width: 168px;
        height: 168px;
        border: 6px solid #fff;
        background: #fff;
        border-radius: 86px;
        z-index: 500;
        overflow: hidden;
        vertical-align: middle;
        text-align: center;
    }

    .col-grey {
        width: 100%;
        height: 110px;
        background: #dfdfdf;
    }

    .col-grey h1 {
        margin-left: 20px;
    }

    .col-grey2 {
        width: 100%;
        height: 110px;
        background: #dfdfdf;
    }
    
    .description_task > div {
        text-align: justify !important;
    }
    .description_task > div {
        text-align: justify;
    }

    .company-detail {
        position: relative;
        margin-bottom: 50px;
        display: block;
        height: 10px;
        background: #fff;
        width: 100%;
    }

    hr {
        back-ground-color: #e5e5e5;
        color: #e5e5e5;
        height: 1px;
    }
    </style></head>

    <body lang=EN-US style='tab-interval:.5in'>
        <div class="Section1" style="width: 500px;">
            <?php $image = Company::getLogoCompany($company->id_company) ?>
            <div class="company-detail">
                <div class="company-image">
                    <img src="{{ URL::asset($image)}}" width=100px height=120px alt="image">
                </div>
            </div>
            <div class="project-details">
                <h2 style="text-align: center; line-height: 12px">{{utf8_decode($project->name_project)}}</h2>

                <p>&nbsp;</p>

                <p style="text-align: center;line-height: 10px">{{trans('messages.label_category')}}: {{utf8_decode($industry_type->name_industry_type)}}        </p>

                <p style="text-align: center;line-height: 10px">{{trans('messages.label_industry')}}: {{utf8_decode($industry->name_industry)}}</p>

            </div>
            <div style="margin:1.0in 1.0in 1.0in 1.0in;">

                <div class="page" id="cover-page">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p style="line-height: 10px">{{trans('messages.label_date')}}: {{date('d.m.Y')}}</p>

                    <p style="line-height: 10px">{{trans('messages.label_author')}}: {{utf8_decode($user->first_name)}} {{utf8_decode($user->last_name)}}</p>
                </div>
                <br clear="all" style="page-break-before:always" />
                <div class="page" id="manu-page">
                    <div class="right-logo">
                        <div class="logo-col-white">
                            <h3>{{trans('messages.label_toc')}}</h3>
                        </div>
                        <div class=" logo-col-grey"></div>
                    </div>


                    <div>
                        <?php
                        Module_structure::printTreeWordContentsEmptyProject($module_structure, $id_software, $id_industry, 1, $id_project) ?>
                    </div>
                </div>
                <br clear="all" style="page-break-before:always" />
                <div class=" " id="tasks-page">
                    <div class="right-logo">
                        <div class="logo-col-white">
                            <h3></h3>
                        </div>
                        <div class=" logo-col-grey"></div>
                    </div>
                    <?php  Module_structure::printTreeWordDetailsEmptyProject($module_structure, $id_software, $id_industry, 1, $id_project) ?>
                </div>
            <br/>
            <table id='hrdftrtbl' border='0' cellspacing='0' cellpadding='0'>
            <tr><td>

            <div style='mso-element:header' id=h1 >
                <p class=MsoHeader>
                    <h4 style="text-align: right;">
                        {{ utf8_decode($company->name_company) }}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </h4>
                    <hr>
                    <p>&nbsp;</p>
                </p>
            </div>

            </td>
            <td>
            <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>
                    {{trans('pagination.page')}} <span style='mso-field-code: Page '><span style='mso-no-proof:yes'></span></span> {{trans('pagination.of')}} <span style='mso-field-code: NUMPAGES '></span>
            </div>



            <div style='mso-element:header' id=fh1>

            <p class=MsoHeader><span lang=EN-US style='mso-ansi-language:EN-US'>

            <o:p></o:p></span></p>

            </div>

            <div style='mso-element:footer' id=ff1>

            </div>

            </td></tr>
            </table>
            </div>
        </div>
    </body>
</html>