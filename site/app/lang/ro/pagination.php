<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Previous',
    'next' => 'Next &raquo;',
    // "emptyTable" => "No data available in table",
    "emptyTable" => "",
    "info" => "Showing _START_ to _END_ of _TOTAL_ entries",
    "infoEmpty" => "Showing 0 to 0 of 0 entries",
    "infoFiltered" => "(filtered from _MAX_ total entries)",
    "infoPostFix" => "",
    "thousands" => ",",
    "lengthMenu" => "Show _MENU_ entries",
    "loadingRecords" => "Loading...",
    "processing" => "Processing...",
    "search" => "Search=>",
    // "zeroRecords" => "No matching records found",
    "zeroRecords" => "",
    "page" => "Pagina",
    "of" => "din",
    "paginate" => [
        "first" => "First",
        "last" => "Last",
        "next" => "Next",
        "previous" => "Previous"
    ],
    "aria" => [
        "sortAscending" => ":attribute activate to sort column ascending",
        "sortDescending" => ":attribute activate to sort column descending"
    ]

);
