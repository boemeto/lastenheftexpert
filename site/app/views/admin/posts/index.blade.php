<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 29-Feb-16
 * Time: 11:25
 */
?>
@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script type="text/javascript">

        $("#modalAddPost").on('hidden.bs.modal', function () {
            $("#modalAddPost").removeData('bs.modal');
        });
        $("#modalAddPost").on('shown.bs.modal', function () {

            // call sumernote text editor
            $('.summernote').summernote({});
        });
    </script>
@stop


@section('content')
    <div class="panel widget light-widget panel-bd-top ">
        <div class="panel-heading bordered">

            <h3 class="panel-title">{{trans('messages.nav_posts_pages') }}</h3>

            <a href="{{ URL::route('posts.create') }}"
               id="a_form_headquarter_0" data-toggle="modal" data-target="#modalAddPost"
               class="btn static_add_button ls-light-blue-btn a_form_headquarter  btn-round btn-xxl">
                <i class="fa fa-plus"></i>
            </a>
            <div class="title_static_add_button" id="div_a_form_headquarter_0">
                {{trans('messages.nav_posts_pages') }}  {{trans('messages.act_create') }}
            </div>
        </div>

        <!-- /.panel-header -->
        <div class="panel-body ">
            @if(count($posts) > 0)
                <div class="row">
                    @foreach($posts as $row)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="panel widget light-widget panel-bd-top vd_bdt-blue ">
                                <div class="panel-heading bordered ">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <h3 class="panel-title"> {{ $row->name_post  }} </h3>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="{{ URL::route('posts.edit',[$row->id_post]) }}"
                                               class="btn btn-round btn-xl ls-orange-btn" data-toggle="modal" data-target="#modalAddPost">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body panel_software">
                                    <p> {{ substr(strip_tags($row->content_post),0,200)   }}  </p>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            @endif
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Modal Software -->
    <div class="modal fade" id="modalAddPost" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Software End -->

@stop
        