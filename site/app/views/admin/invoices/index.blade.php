@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('footer_scripts')
  <script type="text/javascript">
      $(document).ready(function(){
         $('.select-invoice-status').on('click', function(){
            $('#'+$(this).attr('data-id')).removeClass(function (index, className) {
                      return (className.match (/(^|\s)label-\S+/g) || []).join(' ');
                  }).addClass("label-"+$(this).attr('data-color'));
            $('#'+$(this).attr('data-id')).children('span').text($(this).attr('data-status'));
            var invoice_id = $(this).attr('data-id');
            var invoice_status = $(this).attr('data-status');
            var token = $('input[name="_token"]').val();
            $.ajax({
                url: "invoice/edit",
                type: 'post',
                data: {
                    '_token' : token,
                    'invoice_id': invoice_id,
                    'invoice_status': invoice_status
                },
                success: function (data) {}
           });
         });
      });
  </script>
@stop
@section('content')
  <form>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  </form>
    <div class="panel widget light-widget panel-bd-top">
        <div class="panel-heading  ">
            <h3 class="mgtp-10"> {{trans('messages.nav_invoices')}}</h3>
        </div>
        <div class="panel-body table_invoice">
            <table class="table table-responsive table-condensed data_table">
                <thead>
                    <tr>
                        <th>{{trans('messages.label_nr_crt')}}</th>
                        <th>{{trans('messages.label_project_name')}}</th>
                        <th>{{trans('messages.label_customer')}}</th>
                        <th>{{trans('messages.label_date')}}</th>
                        <th class="preview-sm">{{trans('messages.project_nr')}}</th>
                        <th class="text-center">{{trans('messages.label_payment_term')}}</th>
                        <th class="text-center">{{trans('messages.label_sum')}}</th>
                        <th class="text-center">{{trans('messages.label_status')}}</th>
                        <th class="text-center">{{trans('messages.label_action')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <tr>
                        <td>{{$invoice->inv_serial}}-{{$invoice->inv_number}}</td>
                        <td>{{Invoice::getProjectName($invoice->id_invoice)}}</td>
                        <td>{{Company::getCompanyName($invoice->fk_company)}}</td>
                        <td>{{date('d.m.Y',strtotime($invoice->inv_date))}}</td>
                        <td>
                          {{getProjectId($invoice->fk_project)}}
                        </td>
                        <td class="text-center">{{date('d.m.Y',strtotime($invoice->inv_date. '+ 14 days'))}}</td>
                        <td class="pull-right" style="padding-right: 25%">
                            <b>{{formatToGermanyPrice($invoice->sum_paid)}}<b>
                            <i class="fa fa-euro"></i>
                        </td>
                        <td class="text-center">
                            <div class="dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <span id="{{$invoice->id_invoice}}" class="invoice-status label label-{{Invoice::getInvoiceStatusColor($invoice->id_invoice)}}">
                                        <span>{{Invoice::getInvoiceStatus($invoice->id_invoice)}}</span>
                                        <i class="fa fa-caret-down table-select-arrow" aria-hidden="true"></i>
                                        <ul class="dropdown-menu invoices-status-dropdown">
                                          @foreach($invoice_status as $status)
                                            <li class="select-invoice-status" data-id="{{$invoice->id_invoice}}" data-status="{{$status->name_status}}" data-color="{{$status->color}}">
                                                <div class="select-status-color label label-{{$status->color}}">
                                                    {{$status->name_status}}
                                                </div>
                                            </li>
                                          @endforeach
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td class="menu-action text-center">
                            <a href="{{URL::to('invoices_a/'.$invoice->id_invoice)}}" class="btn btn-round ls-light-blue-btn">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
