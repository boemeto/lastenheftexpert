<div class="tab-pane fade" id="coworkers">
    <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-users mgr-10 profile-icon"></i> {{trans('messages.label_coworkers')}}</h3>
    <div class="row table-coworkers">
        @foreach($all_users as $user_co)
            <?php $image_user = User::getLogoUser($user_co->id);
            ?>
            <div class="content-grid col-lg-4 col-md-4 col-xs-4 col-sm-4 mgbt-xs-15 user-item">
                <a href="{{URL::to('members/'.$user_co->id)}}">
                  <hr>
                  <span class="menu-icon">
                      <img class="img-circle" alt="friends pic" src="{{ URL::asset($image_user)}}">
                   </span>
                </a>
                <div class="menu-text">
                  <div class="div_headquarter_details   mgtp-10">
                      <div class="fa_width_user">
                          <i class=" glyphicon glyphicon-briefcase"></i>
                      </div>
                      {{($user_co->job_user)?$user_co->job_user:'<i>'.trans('messages.unavailable').'</i>'}}
                      <br>
                      <div class="fa_width_user">
                          <i class=" fa fa-mobile"></i>
                      </div>
                      {{($user_co->mobile_user)?$user_co->mobile_user:'<i>'.trans('messages.unavailable').'</i>'}}
                      <br>
                      <div class="fa_width_user">
                          <i class=" fa fa-phone"></i>
                      </div>
                      {{($user_co->telephone_user)?$user_co->telephone_user:'<i>'.trans('messages.unavailable').'</i>'}}
                      <br>
                      <div class="fa_width_user">
                          <i class="fa fa-envelope-o"></i>
                      </div>
                      <a href="mailto:{{$user_co->email}}">{{$user_co->email}}</a>
                   </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
