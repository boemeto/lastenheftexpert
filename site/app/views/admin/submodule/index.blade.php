@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop


@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            {{-- Add a new industry --}}
            <a href="{{ URL::route('submodule.create') }}"
               class="btn ls-light-blue-btn  btn-flat ">
                <i class="fa fa-plus-square-o"></i>

            </a>

            <h3 class="box-title">{{trans('messages.submodule') }}</h3>

        </div>

        <!-- /.box-header -->
        <div class="box-body">

            @if(count($submodule) > 0)
                <table class="table table-responsive table-striped table-hover table-align-buttons">
                    <thead>
                    <tr>
                        <th> Id</th>
                        <th> Name</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($submodule as $row)
                        <tr>
                            <td> {{ $row->id_submodule  }} </td>
                            <td> {{ $row->name_submodule  }} </td>

                            <td>
                                {{-- Link industry with module --}}
                                {{--<a href="{{ URL::route('industry.show',[$row->id_industry]) }}"--}}
                                {{--class="btn btn-app btn-flat ls-light-blue-btn">--}}
                                {{--<i class="fa fa-laptop"></i>   {{trans('messages.departments')}}--}}
                                {{--</a>--}}
                                {{-- Edit a industry --}}
                                <a href="{{ URL::route('submodule.edit',[$row->id_submodule]) }}"
                                   class="btn  ls-orange-btn">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ URL::route('submodule.destroy',[$row->id_submodule]) }}"
                                   class="btn ls-red-btn">
                                    <i class="fa fa-remove"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                {{-- Confirmation Messages--}}
                @if( Session::has('error_message'))
                    <div class="form-group has-error">
                        <label class="control-label" for="inputError">
                            <i class="fa fa-times-circle-o"></i> {{ Session::get('error_message') }}</label>
                    </div>
                @endif
                @if( Session::has('success_message'))
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">
                            <i class="fa fa-check"></i> {{ Session::get('success_message') }}</label>
                    </div>
                @endif
                {{$pagination}}
            @endif
            <br>


        </div>
        <!-- /.box-body -->
    </div>


@stop