<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Feb-16
 * Time: 10:43
 */
class Posts extends Eloquent
{
    public static $rules = [
        'name_post' => 'required',
    ];
    public $primaryKey = 'id_post';
    protected $table = 'posts';
    protected $guarded = ['id_post'];
    protected $fillable = ['name_post', 'name_post_en', 'content_post', 'content_post_en', 'slug_post'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }


    public static function getFirstById($id)
    {
        $list = Posts::where('id_post', '=', $id)->first();
        return $list;
    }

    public static function getFirstBySlug($text)
    {
        $list = Posts::where('slug_post', '=', $text)
            ->orderBY('updated_at', 'desc')
            ->first();
        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('posts', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('posts', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'de':
                return $this->attributes[$field];
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }


}