<?php

use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Session;

class UsersController extends BaseController
{

    /**
     * Create a new company, if the company doesn't exist - COMPANY ADMIN USER
     *
     * @return Response
     */

    public function store()
    {
        // dd(Input::all());
        // user validation rules
        $fk_group = 3; //simple user
        $is_blocked = 1; // user must be approved by admin
        // validation rules
        $rules = [
            'first_name'       => 'required',
            'last_name'        => 'required',
            'password'         => 'required|min:4',
            'password_confirm' => 'required|same:password|min:4',
            'email'            => 'required|email|unique:users|min:4',
            'email_confirm'    => 'required|same:email|min:4',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::except('password'))
                ->withErrors($validator);
        }
        $superAdmins = User::getAdminUserData();
        $newCompany = 0;
        // check if user searched existing company
        if (Input::has('id_company')) {
            $id_company = Input::get('id_company');
        } else {
            $id_company = 0;
        }
        if (Input::has('id_headquarter')) {
            if (reset(Input::get('id_headquarter')) == 0 ) {
                $id_headquarter = 0;
            } else {
                $id_headquarter = Input::get('id_headquarter');
            }
        } else {
            $id_headquarter = 0;
        }
        $newHwadquarter = 0;
        $user_mails = array();
        if ($id_company != 0) {
            $companies = Company::getFirstById($id_company);
            if ($id_headquarter != 0) {
                $headquarter = Company_headquarters::getFirstById(($id_headquarter));
                $id_headquarter = $headquarter->id_headquarter;
            } else {
                $headquarter = Company_headquarters::whereFk_company($id_company)
                    ->whereName_headquarter(Input::get('name_headquarter'))->first();
                if($headquarter) {
                    $id_headquarter = $headquarter->id_headquarter;
                } else {
                    $autocomplete_fields = Input::get('autocomplete');
                    $street_fields = Input::get('street');
                    $street_number_fields = Input::get('street_number');
                    $city_fields = Input::get('city');
                    $state_fields = Input::get('state');
                    $country_fields = Input::get('country');
                    $postal_code_fields = Input::get('postal_code');
                    $latitude_fields = Input::get('latitude');
                    $longitude_fields = Input::get('longitude');
                    $telephone_fields = Input::get('telephone_headquarter');
                    $email_fields = Input::get('email_headquarter');
                    $name_fields = Input::get('name_headquarter');
                    $headquarters = array();

                    foreach ($autocomplete_fields as $key => $autocomplete) {
                        $location = new Location;
                        $location->address_string = $autocomplete;
                        $location->street = $street_fields[$key];
                        $location->street_nr = $street_number_fields[$key];
                        $location->city = $city_fields[$key];
                        $location->country = $country_fields[$key];
                        $location->state = $state_fields[$key];
                        $location->postal_code = $postal_code_fields[$key];
                        $location->latitude = $latitude_fields[$key];
                        $location->longitude = $longitude_fields[$key];
                        $location->save();
                        $headquarter = new Company_headquarters();
                        $headquarter->fk_location = $location->id_location;
                        $headquarter->fk_company = $id_company;
                        $headquarter->telephone_headquarter = $telephone_fields[$key];
                        $headquarter->name_headquarter = $name_fields[$key];
                        $headquarter->email_headquarter = $email_fields[$key];
                        $headquarter->is_main = 0;
                        $headquarter->is_blocked = 1;
                        $headquarter->save();
                        $headquarters[] = $headquarter->id_headquarter;
                        $id_headquarter = $headquarter->id_headquarter;
                        $newHwadquarter = 1;
                    }
                }
            }
            $user_mails = User::getUsersObjectsByGroupCompany(2, $id_company);
        } else {
            //if user inserted a new company
            $fk_group = 2; //  company admin - full rights on his company
            $is_blocked = 0; // user is the admin, so he doesn't need to be approved
            // search if the company is already inserted
            $companies = Company::where('name_company', 'like', Input::get('name_company'))->get();
            if (count($companies) > 0) {
                // if YES -> show user that the company exists
                $companies_list = array();
                foreach ($companies as $company) {
                    $companies_list[] = $company->id_company;
                }
                // search the headquarters
                $headquarters = array();
                if (count($companies_list) > 0) {
                    $headquarters = Company_headquarters::whereIn('fk_company', $companies_list)->get();
                }
                $headquarters_list = array();
                if (count($headquarters) > 0) {
                    foreach ($headquarters as $headquarter) {
                        $headquarters_list[] = $headquarter->fk_company;
                    }
                    $searched_company = Input::get('name_company');
                    $errors = new MessageBag(['searched_company' => [Lang::get('validation.company_exists')]]);
                    return Redirect::action('SessionsController@create')->withErrors($errors);
                }
            } else {
                if (Input::hasFile('logo')) {
                    $image_details = App::make('UsersController')->SaveImage('logo');
                } else {
                    $image_details = null;
                }
                if ($image_details != null || $image_details != '') {
                    $array = explode('//', $image_details);
                    $name_image = $array[0];
                } else {
                    $name_image = null;
                }
                $company = new Company();
                $company->name_company = Input::get('name_company');
                $company->cui_company = Input::get('cui_company');
                $company->email_company = Input::get('email_company');
                $company->website_company = Input::get('website_company');
                $company->telephone_company = Input::get('telephone_company');
                $company->logo_company = $name_image;
                $company->save();
                $newCompany = 1;
            }
// add a new company

            $id_company = $company->id_company;
            $autocomplete_fields = Input::get('autocomplete');
            $street_fields = Input::get('street');
            $street_number_fields = Input::get('street_number');
            $city_fields = Input::get('city');
            $state_fields = Input::get('state');
            $country_fields = Input::get('country');
            $postal_code_fields = Input::get('postal_code');
            $latitude_fields = Input::get('latitude');
            $longitude_fields = Input::get('longitude');
            $telephone_fields = Input::get('telephone_headquarter');
            $email_fields = Input::get('email_headquarter');
            $name_fields = Input::get('name_headquarter');
            $headquarters = array();

            foreach ($autocomplete_fields as $key => $autocomplete) {
                $location = new Location;
                $location->address_string = $autocomplete;
                $location->street = $street_fields[$key];
                $location->street_nr = $street_number_fields[$key];
                $location->city = $city_fields[$key];
                $location->country = $country_fields[$key];
                $location->state = $state_fields[$key];
                $location->postal_code = $postal_code_fields[$key];
                $location->latitude = $latitude_fields[$key];
                $location->longitude = $longitude_fields[$key];
                $location->save();
                $headquarter = new Company_headquarters();
                $headquarter->fk_location = $location->id_location;
                $headquarter->fk_company = $id_company;
                $headquarter->telephone_headquarter = $telephone_fields[$key];
                $headquarter->name_headquarter = $name_fields[$key];
                $headquarter->email_headquarter = $email_fields[$key];
                $headquarter->is_main = 1;
                $headquarter->save();
                $headquarters[] = $headquarter->id_headquarter;
                $id_headquarter = $headquarter->id_headquarter;
            }
        }

        if (Input::hasFile('logo_user')) {
            $image = 1;
        } else {
            $image = 0;
        }
        if ($image == 1) {
            $image_details = App::make('UsersController')->SaveImage('logo_user', "images/image_user/");
        } else {
            $image_details = null;
        }
        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }
        $email = Input::get('email');
        $activation = substr(hash('sha256', $email), 2, 20);

        $fk_personal_title = Input::get('fk_personal_title');
        $fk_title = Input::get('fk_title');
        $job_user = Input::get('job_user');
        $last_name = Input::get('last_name');
        $first_name = Input::get('first_name');
        $email = Input::get('email');
        $mobile_user = Input::get('mobile_user');
        $telephone_user = Input::get('telephone_user');
        $password = Input::get('password');
        $social_linkedin = Input::get('social_linkedin');
        $social_twitter = Input::get('social_twitter');
        $social_xing = Input::get('social_xing');
        $fk_company = $id_company;
        $fk_headquarter = $id_headquarter;
        $activation_code = $activation;
        $blocked = $is_blocked;
        $logo_user = $name_image;
        $is_deleted = 0;
        $activated = 1;

        $user = User::createNewUser($email, $password, $first_name, $last_name, $blocked, $fk_company,
            $fk_headquarter, $fk_personal_title, $fk_title, $job_user, $activation_code,
            $social_linkedin, $social_twitter, $social_xing, $telephone_user, $mobile_user,
            $logo_user, $is_deleted, $fk_group, $activated);

        $activation_code = url() . "/activation?user=" . $user->id . "&hash=" . $activation;
        $user = User::findOrFail($user);
        $user['approve_user_link'] = url() . "/accept-user/" . $user->id;
        $user['decline_user_link'] = url() . "/decline-user/" . $user->id;
        $user['employees_list'] = url() . "/employees-list/" . $user->id;
        if (empty($company)) {
            $company = Company::findOrFail($fk_company);
        }
        $company['view_company'] = url() . "/acompany/" . $company->id_company . "/edit";
        $data = array(
            'detail'  => $activation_code,
            'user'    => $user,
            'company' => $company
        );

        if (count($superAdmins) > 0) {
            foreach ($superAdmins as $superAdmin) {
                Mail::send('emails.new_user', $data, function ($message) use ($superAdmin) {
                    $message->to($superAdmin->email, $superAdmin->first_name . ' ' . $superAdmin->last_name)->subject('New User Registration!');
                });
                if($newCompany) {
                    Mail::send('emails.new_company', $data, function ($message) use ($superAdmin) {
                        $message->to($superAdmin->email, $superAdmin->first_name . ' ' . $superAdmin->last_name)->subject('New User Registration!');
                    });
                }
            }
        }
        Mail::send('emails.activate_user', $data, function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Activation Code!');
        });
        if($newHwadquarter) {
            if (count($user_mails) > 0 && $fk_group == 3) {
                $data['headquarter'] = $headquarter;
                foreach ($user_mails as $user_mail) {
                    Mail::send('emails.approve_user_with_headquarter', $data, function ($message) use ($user_mail) {
                        $message->to($user_mail->email, $user_mail->first_name . ' ' . $user_mail->last_name)->subject('Approve User!');
                    });
                }
            }
        } else {
            if (count($user_mails) > 0 && $fk_group == 3) {
                foreach ($user_mails as $user_mail) {
                    Mail::send('emails.approve_user', $data, function ($message) use ($user_mail) {
                        $message->to($user_mail->email, $user_mail->first_name . ' ' . $user_mail->last_name)->subject('Approve User!');
                    });
                }
            }
        }
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_user')]]);
        // dd($errors->first());
        return Redirect::back()->withErrors($errors);
        // return Redirect::action('SessionsController@confirmRegistration')->withErrors($errors);
        // return View::make('front.sessions.confirmRegistration', compact('errors'));
    }


    /**
     * Used for Register  User - If the company already exists
     *
     * @return Response
     */
    public function edit($id_user)
    {
        $user = User::getFirstById($id_user);


        return View::make('front.users.edit', compact('user'));
    }

    public function update($id_user)
    {
        if (!User::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(User::$messages);
        }

        $user = User::getFirstById($id_user);
        $user->fill(Input::all());
        $user->password = Hash::make(Input::get('password'));
        $user->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_user')]]);
        return Redirect::action('SessionsController@index')->withErrors($errors);
        return Redirect::to('/');
    }

    /**
     * Approve pending members
     *
     * @param  int $id
     * @return Response
     */
    public function approve($id)
    {
        $user = User::getFirstById($id);
        $user->blocked = 0;
        $user->save();
        $headquarter = Company_headquarters::findOrFail($user->fk_headquarter);
        $headquarter->is_blocked = 0;
        $headquarter->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);

        Mail::send('emails.account_accepted', $user, function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
        });
        Session::flash('go-to', URL::to('users/'.$id));
        return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
    }

    public function admin_approve($id)
    {
        $user = User::getFirstById($id);
        $user->activated = 1;
        $user->blocked = 0;
        $user->save();
        $headquarter = Company_headquarters::findOrFail($user->fk_headquarter);
        $headquarter->is_blocked = 0;
        $headquarter->save();

        Mail::send('emails.account_accepted', [$user, $confirm_user=true], function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
        });
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        Session::flash('tab', 'employees');
        Session::flash('go-to', URL::to('members/'.$id));
        return Redirect::action('CompanyAdmController@edit', $headquarter->fk_company)->withErrors($errors);
    }

    // block desired user
    public function block($id)
    {

        $user = User::getFirstById($id);
        $user->is_deleted = 1;
        $user->save();

        Mail::send('emails.account_rejected', $user, function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
        });
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_delete')]]);
        return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
    }

    // block desired user by super admin
    public function admin_block($id)
    {

        $user = User::getFirstById($id);
        $user->is_deleted = 1;
        $user->save();
        Mail::send('emails.account_rejected', $user, function ($message) use ($user) {
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
        });
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_delete')]]);
        Session::flash('tab', 'employees');
        return Redirect::back()->withErrors($errors);
    }

    // change user's group
    public function change_group($id)
    {


        $fk_group = Groups::getGroupsIds($id, 1);

        if (in_array(2, $fk_group)) {

            User_groups::where('fk_user', '=', $id)
                ->update(array('fk_group' => 3));


        } elseif (in_array(3, $fk_group)) {

            User_groups::where('fk_user', '=', $id)
                ->where('fk_group', '=', 3)
                ->update(['fk_group' => 2]);

        }


        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        Session::flash('go-to', URL::to('users/'.$id));
        return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
    }

    // change user's group by admin
    public function change_group_by_admin($id)
    {


        $fk_group = Groups::getGroupsIds($id, 1);

        if (in_array(2, $fk_group)) {

            User_groups::where('fk_user', '=', $id)
                ->update(array('fk_group' => 3));


        } elseif (in_array(3, $fk_group)) {

            User_groups::where('fk_user', '=', $id)
                ->where('fk_group', '=', 3)
                ->update(['fk_group' => 2]);

        }


        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        Session::flash('tab', 'employees');
        Session::flash('go-to', URL::to('members/'.$id));
        return Redirect::back()->withErrors($errors);
    }

    public function profile()
    {
    }

    public function show($id_user)
    {
        $id_user_logged = Auth::user()->id;
        $fk_group = Groups::getGroupsIds($id_user_logged, 1);

        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();


        $user = User::whereId($id_user)->first();
        $fk_group_user = Groups::getGroupsIds($id_user, 1);
        $id_company = $user->fk_company;
        $id_headquarter = $user->fk_headquarter;
        // get user's company details
        $company = Company::whereId_company($id_company)->first();
        // get the headquarters
        $main_headquarter = Company_headquarters::getHeadquartersById($id_headquarter);

        $headquarters = Company_headquarters::getHeadquartersLocations($id_company);
        $headquarters_user = Company_headquarters::getHeadquartersSelect($company->id_company, 1);
        $all_users = User::getAllUsersDetailsByCompany($id_company);


        $users_blocked = User::whereFk_company($id_company)
            ->whereBlocked(1)
            ->count();
        foreach ($headquarters as $headquarter) {

            $users = User::whereFk_headquarter($headquarter->id_headquarter)->get();
            $nr = count($users);
            $nr_users_headquarter[$headquarter->id_headquarter] = $nr;
        }

        if (in_array('2', $fk_group) || $id_user == $id_user_logged) {
            $projects = Project::getUsersProjects($id_user);
        } else {
            $projects = Project::getUsersCommonProjects($id_user, $id_user_logged);
        }

        return View::make('front.users.profile', compact('user', 'company', 'headquarters', 'fk_group', 'fk_group_user',
            'nr_users_headquarter', 'all_users', 'users_blocked', 'titles', 'personal_titles',
            'projects', 'main_headquarter', 'id_headquarter', 'id_user', 'id_user_logged', 'headquarters_user'));
    }

    public function password()
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        return View::make('front.users.password', compact('user'));
    }

    public function profile_admin()
    {
        allowOnlySuperAdmin();
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();
        return View::make('admin.users.profile', compact('user', 'titles', 'personal_titles'));
    }

    public function password_admin()
    {
        allowOnlySuperAdmin();
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        return View::make('admin.users.password', compact('user'));
    }

    function SaveImage($name, $destination_folder = "images/image_company/")
    {

        $current_date = date('dmY');
        $images = Input::file($name)->getFilename();
        $file_name = Input::file($name)->getClientOriginalName();
        //  $file_size = $_FILES[$name]["size"];

        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];


        $name_gen = str_shuffle($pin);
        $file_name = $name_gen . "_" . $current_date;

        $size = GetimageSize(Input::file($name)); // $size[0] = width;

        $h = $size[1];
        $w = $size[0];

        $square = 400;
        if ($w == $h) {
            $iHeight = 400;
            $iWidth = 400;

        } elseif ($w < $h) {
            $iHeight = 400;
            $iWidth = round($iHeight * $w / $h);
            $square = $iWidth;
        } else {
            $iWidth = 400;
            $iHeight = round($iWidth * $h / $w);
            $square = $iHeight;
        }

        if ($w == $h) {
            $tHeight = 80;
            $tWidth = 80;
        } elseif ($w < $h) {
            $tHeight = 80;
            $tWidth = round($tHeight * $w / $h);
        } else {
            $tWidth = 80;
            $tHeight = round($tWidth * $h / $w);
        }

        $type = Input::file($name)->getClientMimeType();
        switch ($type) {
            case 'image/jpeg':
                $file_name .= ".jpg";
                $images_orig = ImageCreateFromJPEG(Input::file($name));

                break;
            case 'image/jpg':
                $images_orig = imagecreatefromjpeg(Input::file($name));
                $file_name .= ".jpg";
                break;
            case 'image/png':
                $images_orig = imagecreatefrompng(Input::file($name));
                $file_name .= ".png";
                break;
            case 'image/gif':
                $images_orig = imagecreatefromgif(Input::file($name));
                $file_name .= ".gif";
                break;
            case 'image/bmp':
                $images_orig = ImageCreateFromBmp(Input::file($name));
                $file_name .= ".bmp";
                break;
            default:
                $images_orig = ImageCreateFromJPEG(Input::file($name));
                $file_name .= ".jpg";
                break;
        }

        $images_copy = ImageCreateTrueColor($iWidth, $iHeight);
        $images_fin = ImageCreateTrueColor($square, $square);
        ImageCopyResampled($images_copy, $images_orig, 0, 0, 0, 0, $iWidth, $iHeight, $w, $h);
        ImageCopyResampled($images_fin, $images_copy, 0, 0, 0, 0, $square, $square, $square, $square);

        // thumbnail
//        $thumb_name = 'thumbnail_' . $file_name;
//        $image_copy = ImageCreateTrueColor($tWidth, $tHeight);
//        $images_thumb = ImageCreateTrueColor(250, 250);
//        ImageCopyResampled($image_copy, $images_orig, 0, 0, 0, 0, $tWidth, $tHeight, $w, $h);
//        ImageCopyResampled($images_thumb, $image_copy, 0, 0, 0, 0, $tWidth, $tHeight, $iWidth, $iHeight);

        switch ($type) {
            case 'image/jpeg':
                ImageJPEG($images_fin, $destination_folder . $file_name);
//                ImageJPEG($images_thumb, $destination_folder . $thumb_name);
                break;
            case 'image/jpg':
                ImageJPEG($images_fin, $destination_folder . $file_name);
//                ImageJPEG($images_thumb, $destination_folder . $thumb_name);
                break;
            case 'image/png':
                imagepng($images_fin, $destination_folder . $file_name);
//                imagepng($images_thumb, $destination_folder . $thumb_name);
                break;
            case 'image/gif':
                imagegif($images_fin, $destination_folder . $file_name);
//                imagegif($images_thumb, $destination_folder . $thumb_name);
                break;
            case 'image/bmp':
                Imagebmp($images_fin, $destination_folder . $file_name);
//                Imagebmp($images_thumb, $destination_folder . $thumb_name);
                break;
            default:
                ImageJPEG($images_fin, $destination_folder . $file_name);
//                ImageJPEG($images_thumb, $destination_folder . $thumb_name);
                break;
        }

        return $file_name . "//" . $iHeight . "//" . $iWidth . "//" . $type;
    }

}
