<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11-Aug-16
 * Time: 10:16
 */
class Invoice extends Eloquent
{
    public static $messages = [];
    public $primaryKey = 'id_invoice';
    protected $table = 'invoices';
    protected $guarded = ['id_invoice'];
    protected $fillable = ['fk_company', 'sum_paid', 'is_paid', 'inv_date'];

    public static function getInvoiceNumber()
    {
        $invoice_nr = Invoice::max('inv_number');
        $nr = intval($invoice_nr) + 1;
        return $nr;
    }

    public static function getInvoicesByCompany($id_company, $id_invoice = null, $limit = null)
    {
        if (limit === null) {
          if ($id_invoice == null) {
              $list = self::where('fk_company', '=', $id_company)
                  ->orderBy('fk_status')
                  ->orderBy('id_invoice', 'desc')
                  ->get();
          } else {
              $list = self::where('fk_company', '=', $id_company)
                  ->orderBy('fk_status')
                  ->orderBy('id_invoice', 'desc')
                  ->where('id_invoice', '!=', $id_invoice)
                  ->get();
          }
        }
        else {
          if ($id_invoice == null) {
              $list = self::where('fk_company', '=', $id_company)
                  ->orderBy('fk_status')
                  ->orderBy('id_invoice', 'desc')
                  ->limit($limit)
                  ->get();
          } else {
              $list = self::where('fk_company', '=', $id_company)
                  ->orderBy('fk_status')
                  ->orderBy('id_invoice', 'desc')
                  ->where('id_invoice', '!=', $id_invoice)
                  ->limit($limit)
                  ->get();
          }
        }

        return $list;
    }

    public static function getInvoicesAdmin()
    {
        $list = self::orderBy('fk_status')->get();
        return $list;
    }

    public static function getProjectName($id)
    {
        $list = self::getFirstById($id);
        $id_project = $list->fk_project;
        $project = Project::getFirstById($id_project);
        return $project->name_project;
    }

    public static function getFirstById($id)
    {
        $list = self::where('id_invoice', '=', $id)->first();
        return $list;
    }

    public static function getInvoiceStatus($id)
    {
        $list = self::getFirstById($id);
        $fk_id = $list->fk_status;
        $status = Invoice_status::getFirstById($fk_id);
        return $status->name_status;
    }

    public static function getInvoiceStatusColor($id)
    {
        $list = self::getFirstById($id);
        $fk_id = $list->fk_status;
        $status = Invoice_status::getFirstById($fk_id);
        return $status->color;
    }

    public static function getInvoicesByProject($id)
    {
        $list = self::where('fk_project', '=', $id)
            ->orderBy('inv_number', 'desc')->get();
        return $list;
    }
}
