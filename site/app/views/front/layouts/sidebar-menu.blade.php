<?php
  if (Auth::check()) {
      $id_user = Auth::user()->id;
      $fk_group = User_groups::getGroupByUser($id_user);
      $fk_company = Auth::user()->fk_company;
  }
?>

<li><a href="{{URL::to('/')}}"> <i class="fa fa-home"></i> <span>{{trans('messages.nav_dashboard')}}</span> </a></li>
<li><a href="{{URL::to('company')}}"> <i class="fa fa-building-o"></i> <span>{{trans('messages.nav_company')}}</span> </a></li>
<li><a href="{{URL::to('projects')}}"> <i class="fa fa-sitemap"></i> <span>{{trans('messages.nav_projects')}}</span> </a></li>
@if (in_array(2, $fk_group))
    <li><a href="{{URL::to('invoice')}}"> <i class="fa fa-file-text-o"></i> <span>{{trans('messages.nav_invoices')}}</span> </a></li>
@endif
<li><a href="{{URL::to('users/'.$id_user)}}"> <i class="fa fa-user"></i> <span>{{trans('messages.nav_user_profile')}}</span> </a></li>
<li><a href="{{ URL::to('logout') }}"> <i class="fa fa-sign-out"></i> <span>{{trans('messages.act_logout')}}</span></a></li>
