<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 27.05.2015
 * Time: 12:41
 */

class PackageType extends Eloquent
{
    public static $rules = [
        'package_type_name' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_package_type';
    protected $table = 'package_type';
    protected $guarded = ['id_package_type'];
    protected $fillable = ['package_type_name'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }
}