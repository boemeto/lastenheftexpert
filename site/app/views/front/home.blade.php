@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <style type="text/css">
        .pie-widget canvas {
            color: #29B9E8;
        }
    </style>
@stop
@section('footer_scripts')
    <script src="{{asset('assets/js/jquery.easypiechart.min.js')}}"></script>
    <script type="text/javascript">
        @foreach($projects as $project)
         $('#chart_{{$project->id_project}}').easyPieChart({
            animate: 2000,
            barColor: 'white',
            scaleColor: '#007C8E',
            trackColor: '#4EC0E6',
            lineWidth: 2,
            size: 90,
            easing: 'easeOutBounce',
            onStep: function (from, to, percent) {

                $(this.el).find('.pie-widget-count-2').text(Math.round(percent));
            }
        });

        $('#chart_{{$project->id_project}}').on("mouseover", function () {
            var value = $('#chart_{{$project->id_project}}').data('percent');

            if (value > 30) {
                $('#chart_{{$project->id_project}}').data('easyPieChart').update(0);
            } else {
                $('#chart_{{$project->id_project}}').data('easyPieChart').update(100);
            }

            $('#chart_{{$project->id_project}}').data('easyPieChart').update(value);
        });
        @endforeach
    </script>
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-home"></i>
            <h3 class="box-title"> {{trans('messages.nav_dashboard')}}   </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @if (in_array('2', $user_groups))
                            <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                                <a href="{{URL::to('/pack')}}">
                                    <div class="dashboard logo-color-lighter-1">
                                        <div class="title_dashboard"> {{trans('messages.label_new_project')}}</div>
                                        <div class=" user_image_centered">
                                            <img alt="" src="{{ URL::asset('images/plus-sign.png')}}">
                                        </div>
                                        <div class="description_dashboard "> {{trans('messages.label_dashboard_new_project')}}</div>
                                    </div>
                                </a>
                            </div>
                        @endif
                        @foreach($projects as $project)
                            <?php
                            $startDate = $project->created_at;
                            $endDate = $project->due_date;
                            $days = daysPercentPassed($startDate, $endDate);
                            ?>
                            <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                                <a href="{{URL::to('/projects/'.$project->id_project)}}">
                                    <div class="dashboard logo-color-1">
                                        <div class="title_dashboard"> {{$project->name_project}}</div>
                                        <div class="pie-widget">
                                            <div class="pie-widget-2 chart-pie-widget" id="chart_{{$project->id_project}}" data-percent="{{$days}}">
                                                <span class="pie-widget-count-2 pie-widget-count">{{$days}}</span>
                                            </div>
                                            <div class="description_dashboard"> {{trans('messages.label_latest_update')}}: {{date('d.m.Y',strtotime($project->due_date))}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    @if(count($projects)>=5)
                      <a href="{{ URL::route('projects.index') }}" class="link-hover white-text">
                        <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                            <button class="btn btn-block logo-color-1 button-hover">
                                {{trans('messages.label_view_all')}}
                            </button>
                        </div>
                      </a>
                    @endif
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @if (in_array('2', $user_groups))
                            <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                                <a href="{{ action('CompanyController@redirectFromDasboardAddNew', ['employees', 'add_employee']) }}">
                                    <div class="dashboard logo-color-lighter-2">
                                        <div class="title_dashboard"> {{trans('messages.label_new_user')}}</div>
                                        <div class=" user_image_centered">
                                            <img alt="" src="{{ URL::asset('images/plus-sign.png')}}">
                                        </div>
                                        <div class="description_dashboard">&nbsp;</div>
                                    </div>
                                </a>
                            </div>
                        @endif
                        {{-- {{dd($users_company)}} --}}
                        @foreach($users_company as $user_c)
                            <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                                <a href="{{URL::to('users/'.$user_c->id)}}">
                                    <div class="dashboard logo-color-2">
                                        <div class="title_dashboard">
                                            {{Users_title::getUserTitle($user_c->id)}}   {{$user_c->first_name}} {{$user_c->last_name}}
                                        </div>
                                        <?php $image = User::getLogoUser($user_c->id);    ?>
                                        <div class=" user_image_centered">
                                            <img src="{{ URL::asset($image)}}" alt="image">
                                        </div>
                                        <div class="description_dashboard"> {{$user_c->name_headquarter}} </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    @if(count($users_company)>=5)
                      <a href="{{ action('CompanyController@redirectFromDasboard', 'employees') }}" class="link-hover white-text">
                        <div class="col-lg-6 col-md-6 col-xs-6 padding-small">
                          <button class="btn btn-block logo-color-2 button-hover">
                            {{trans('messages.label_view_all')}}
                          </button>
                        </div>
                      </a>
                    @endif
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-6">
                          @if (in_array('2', $user_groups))
                              <div class="col-lg-12 col-md-12 col-xs-12 padding-small">
                                  <a href="{{ action('CompanyController@redirectFromDasboardAddNew', ['headquarters', 'add_headquarter']) }}">
                                      <div class="dashboard logo-color-lighter-3">
                                          <div class="title_dashboard"> {{trans('messages.label_new_headquarter')}}</div>
                                          <div class=" user_image_centered">
                                              <img alt="" src="{{ URL::asset('images/plus-sign.png')}}">
                                          </div>
                                          <div class="description_dashboard">&nbsp;</div>
                                      </div>
                                  </a>
                              </div>
                          @endif
                          @foreach($headquarters as $headquarter)
                              <div class="col-lg-12 col-md-12 col-xs-12 padding-small">
                                  <a href="{{URL::to('company')}}">
                                      <div class="dashboard logo-color-3">
                                          <div class="title_dashboard">
                                              {{strtoupper($headquarter->name_headquarter)}}
                                          </div>
                                          <div class=" user_image_centered">
                                              <img alt="" src="{{ URL::asset('images/headquarter-white.png')}}">
                                          </div>
                                          <div class="description_dashboard">
                                              {{$headquarter->city}}, {{$headquarter->country}}
                                          </div>
                                      </div>
                                  </a>
                              </div>
                          @endforeach
                          @if(count($headquarters)>=2)
                            <a href="{{ action('CompanyController@redirectFromDasboard', 'headquarters') }}" class="link-hover white-text">
                              <div class="col-lg-12 col-md-12 col-xs-12 padding-small">
                                <button class="btn btn-block logo-color-3 button-hover">
                                    {{trans('messages.label_view_all')}}
                                </button>
                              </div>
                            </a>
                          @endif
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-6">
                          @foreach($invoices as $invoice)
                              <div class="col-lg-12 col-md-12 col-xs-12 padding-small">
                                  <a href="{{URL::to('invoice/' . $invoice->id_invoice)}}">
                                      <div class="dashboard logo-color-4">
                                          <div class="title_dashboard">
                                              {{$invoice->inv_serial}} - {{$invoice->inv_number}} / {{date('Y', strtotime($invoice->inv_date))}}
                                          </div>
                                          <div class=" user_image_centered">
                                              <?php
                                                  $tva = 0.19 * $invoice->unit_price;
                                                  $total = $invoice->unit_price + $tva;
                                              ?>
                                              <span>
                                                  @if($total > 99)
                                                      <span class="invoice-price-container3">
                                                  @elseif($total > 999)
                                                      <span class="invoice-price-container2">
                                                  @else
                                                      <span class="invoice-price-container">
                                                  @endif
                                                      {{formatToGermanyPrice($total)}}
                                                      <i class="fa fa-eur"></i>
                                                  </span>
                                              </span>
                                              <!--<img alt="" src="{{ URL::asset('images/invoice.png')}}">-->
                                          </div>
                                          <div class="description_dashboard">
                                            <span class="label">
                                              {{-- <span class="label label-{{Invoice::getInvoiceStatusColor($invoice->id_invoice)}}"> --}}
                                              {{Invoice::getInvoiceStatus($invoice->id_invoice)}}
                                            </span>
                                          </div>
                                      </div>
                                  </a>
                              </div>
                            @endforeach
                            @if(count($invoices)>=3)
                              <a href="{{ URL::route('invoice.index') }}" class="link-hover white-text">
                                <div class="col-lg-12 col-md-12 col-xs-12 padding-small">
                                  <button class="btn btn-block logo-color-4  button-hover">
                                      {{trans('messages.label_view_all')}}
                                  </button>
                                </div>
                              </a>
                            @endif
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@stop
