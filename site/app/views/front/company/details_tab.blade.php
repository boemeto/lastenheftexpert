<div class="tab-pane fade active in" id="details">

    <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
        @if(in_array('2',$fk_group))
            <a href="javascript:void(0)"
               id="a_company_show" data-color="#F0AD4E"
               class="btn static_add_button ls-orange-btn a_company_show  btn-round btn-xxl">
                <i class="fa fa-edit append-icon"></i>
            </a>
            <div class="title_static_add_button" id="div_a_company_show">
                {{trans('messages.act_edit')}}
            </div>
        @endif
        <i class="fa fa-building-o mgr-10 profile-icon"></i>
        {{trans('messages.nav_company')}}</h3>


    <div class="row">

        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">

            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.name_company')}}:&nbsp;</th>
                    <td> {{ $company->name_company }}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_phone')}}:&nbsp;</th>
                    <td>  {{$company->telephone_company}}</td>
                </tr>
                @if($company->cui_company)
                    <tr>
                        <th>Identification Code:&nbsp;</th>
                        <td>  {{$company->cui_company}}</td>
                    </tr>
                @endif
                @if($company->mobile_company)
                    <tr>
                        <th>{{trans('messages.label_mobile')}}:&nbsp;</th>
                        <td>  {{$company->mobile_company}}</td>
                    </tr>
                @endif
                <tr>
                    <th>{{trans('messages.label_email')}}:&nbsp;</th>
                    <td><a href="mailto:{{$company->email_company}}">{{$company->email_company}} </a></td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_website')}}:&nbsp;</th>
                    <td><a href="{{$company->website_company}}" target="_blank">{{$company->website_company}} </a></td>
                </tr>

            </table>

        </div>
        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">
            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.name_headquarter')}}:&nbsp;</th>
                    <td>  {{$main_headquarter->name_headquarter}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_street')}}:&nbsp;</th>
                    <td>{{$main_headquarter->street}}  {{$main_headquarter->street_nr}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_zip_code')}} / {{trans('messages.label_city')}}:&nbsp;</th>
                    <td>{{$main_headquarter->postal_code}}, {{$main_headquarter->city}} </td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_country')}}:&nbsp;</th>
                    <td> {{$main_headquarter->country}} </td>
                </tr>
            </table>

        </div>
    </div>
    <div class="row edit_company_hidden">
        {{ Form::model($company, ['method'=>'PATCH', 'route'=>['company.update',$company->id_company],'enctype'=>"multipart/form-data"])}}

        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
            <div class="form-group has-feedback">
                {{ Form::text('name_company',$company->name_company,[ 'placeholder'=>trans('messages.label_company'), 'class'=>"form-control", 'id'=>'name_company', 'required']) }}
                {{ $errors->first('name_company','<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group  has-feedback">

                {{ Form::text('telephone_company',$company->telephone_company,[ 'placeholder'=>trans('messages.label_phone'),'id'=>'telephone_company', 'class'=>"form-control"]) }}
                {{ $errors->first('telephone_company','<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
            </div>
            <div class="form-group  has-feedback">

                {{ Form::text('fax_company',$company->fax_company,[ 'placeholder'=>trans('messages.label_fax'),
                'id'=>'fax_company', 'class'=>"form-control"]) }}
                {{ $errors->first('fax_company','<div class="text-red">:message</div>') }}
                <i class="fa fa-fax form-control-feedback"></i>
            </div>
            <div class="form-group  has-feedback">

                {{ Form::email('email_company',$company->email_company,[ 'placeholder'=>trans('messages.label_email'),'id'=>'email_company', 'class'=>"form-control"]) }}
                {{ $errors->first('email_company','<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group  has-feedback">

                {{ Form::text('website_company',$company->website_company,[ 'placeholder'=>trans('messages.label_website'),'id'=>'website_company', 'class'=>"form-control"]) }}
                {{ $errors->first('website_company','<div class="text-red">:message</div>') }}
                <i class="fa fa-globe form-control-feedback"></i>
            </div>

        </div>
        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
            <div class="form-group has-feedback">

                {{ Form::text('cui_company',$company->cui_company,[ 'placeholder'=>trans('messages.label_identification_code'), 'id'=>'cui_company','class'=>"form-control"]) }}
                {{ $errors->first('cui_company','<div class="text-red">:message</div>') }}
                <i class="fa fa-key form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">

                {{ Form::text('social_xing',$company->social_xing,[ 'placeholder'=>"Xing Account", 
                'id'=>'social_xing','class'=>"form-control"]) }}
                {{ $errors->first('social_xing','<div class="text-red">:message</div>') }}
                <i class="fa fa-xing form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">

                {{ Form::text('social_linkedin',$company->social_linkedin,[ 'placeholder'=>"LinkedIn Account", 
                'id'=>'social_linkedin','class'=>"form-control"]) }}
                {{ $errors->first('social_linkedin','<div class="text-red">:message</div>') }}
                <i class="fa fa-linkedin form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">

                {{ Form::text('social_twitter',$company->social_twitter,[ 'placeholder'=>"Twitter Account", 
                'id'=>'social_twitter','class'=>"form-control"]) }}
                {{ $errors->first('social_twitter','<div class="text-red">:message</div>') }}
                <i class="fa fa-twitter form-control-feedback"></i>
            </div>


            <div class="form-group" id="form-group_file">
                <input id="file-3" type="file" name="logo" multiple="flase">
            </div>
            <input type="hidden" name="is_blocked" value="{{$company->is_blocked}}">


        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                    {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block  ls-light-blue-btn margin-small-bottom "])}}
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                    {{Form::button(trans('messages.act_cancel'),['class'=>"btn btn-block a_company_show  ls-red-btn margin-small-bottom ",'id'=>'a_form_close_'.$user->id])}}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>