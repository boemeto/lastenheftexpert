@extends('admin.layouts.default')
@section('dropdown-menu')
@include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
@include('admin.layouts.sidebar-menu')
@stop

@section('header_scripts')

@stop
@section('footer_scripts')

        <!-- Module menu tree style -->
<script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
@include('admin/industries/tree_script')
<script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>

<!-- Module menu tree style -->
<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery(".switchCheckBox").bootstrapSwitch();
        $('.summernote_new').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    });
    $("#modalEditTask").on('hidden.bs.modal', function () {
        $("#modalEditTask").removeData('bs.modal');
    });
    $("#modalAddTask").on('hidden.bs.modal', function () {
        $("#modalAddTask").removeData('bs.modal');
    });
    $("#modalAddTask").on('shown.bs.modal', function () {
        jQuery(".switchCheckBox").bootstrapSwitch();
        jQuery('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    });
    $("#modalEditTask").on('shown.bs.modal', function () {

        jQuery(".switchCheckBox").bootstrapSwitch();
        $('.select_module').on('change', function () {
            return viewTaskModuleStructure($(this).val());
        });

        function getXMLHTTP() {

            var xmlhttp = false;
            try {
                xmlhttp = new XMLHttpRequest();
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    try {
                        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e1) {
                        xmlhttp = false;
                    }
                }
            }

            return xmlhttp;
        }

        function viewTaskModuleStructure(id) {

            if (id == 0) {

                document.getElementById('div_module_structure').innerHTML = ' Please select desired structure. ';
            }
            else {

                var strURL = "{{URL::to('/')}}/ajax/viewTaskModuleStructure/" + id;

                var req = getXMLHTTP();

                if (req) {
//                    alert(JSON.stringify(req))
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                document.getElementById('div_module_structure').innerHTML = req.responseText;
                            } else {
                                alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }

                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }


        jQuery('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    });


</script>


@stop
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <a href="{{ URL::route('industry.edit',[$industry->id_industry]) }}"
               class="btn ls-orange-btn btn-flat">
                <i class="fa fa-edit"></i>
            </a>

            <h3 class=" box-title">{{ $industry->name_industry }}</h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body industry_module_structure">


            <div class="easy-tree ls-tree-view">
                <ul class="ul-tree">
                    <li>  <span class="menu_title" id="submodule0">
                                      <span class="glyphicon glyphicon-minus-sign"></span>
                                    <a href="javascript:void(0); ">
                                        {{ $industry->name_industry }}
                                    </a>
                          
                           </span>
                        <ul>
                            @foreach($module_structure as $module1)
                                @if(Module_structure::hasChild($module1->id_module_structure) == 1)
                                    <li>
                                               <span class="menu_title" id="submodule{{$module1->id_module_structure}}">
                                       <span class="glyphicon glyphicon-minus-sign"></span>
                                        <a href="javascript:void(0); ">{{$module1->id_name}}</a>
                                                   {{($module1->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>
                                        <ul>
                                            <?php  $module_structure_2 = Module_structure::getModuleByLvl($industry->id_industry, 2, $module1->id_module_structure, 0);                                                    ?>
                                            @foreach($module_structure_2 as $module2)
                                                @if(Module_structure::hasChild($module2->id_module_structure) == 1)
                                                    <li>
                                               <span class="menu_title" id="submodule{{$module2->id_module_structure}}">
                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                        <a href="javascript:void(0); ">{{$module2->id_name}}</a>
                                                   {{($module2->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>
                                                        <ul>
                                                            <?php  $module_structure_3 = Module_structure::getModuleByLvl($industry->id_industry, 3, $module2->id_module_structure, 0);                                                    ?>
                                                            @foreach($module_structure_3 as $module3)
                                                                @if(Module_structure::hasChild($module3->id_module_structure) == 1)
                                                                    <li>
                                               <span class="menu_title" id="submodule{{$module3->id_module_structure}}">
                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                        <a href="javascript:void(0); ">{{$module3->id_name}}</a>
                                                   {{($module3->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>
                                                                        <ul>
                                                                            <?php  $module_structure_4 = Module_structure::getModuleByLvl($industry->id_industry, 4, $module3->id_module_structure, 0);                                                    ?>
                                                                            @foreach($module_structure_4 as $module4)
                                                                                @if(Module_structure::hasChild($module4->id_module_structure) == 1)
                                                                                    <li>
                                               <span class="menu_title" id="submodule{{$module4->id_module_structure}}">
                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                        <a href="javascript:void(0); ">{{$module4->id_name}}</a>
                                                   {{($module4->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>
                                                                                        <ul>
                                                                                            <?php  $module_structure_5 = Module_structure::getModuleByLvl($industry->id_industry, 5, $module4->id_module_structure, 0);                                                    ?>
                                                                                            @foreach($module_structure_5 as $module5)
                                                                                                @if(Module_structure::hasChild($module5->id_module_structure) == 1)
                                                                                                    <li {{($module5->is_default == 0)?'class="li_editable"':''}}>
                                               <span class="menu_title" id="submodule{{$module5->id_module_structure}}">
                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                        <a href="javascript:void(0); ">{{$module5->id_name}}</a>
                                                   {{($module5->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>
                                                                                                    </li>
                                                                                                @else
                                                                                                    <li class="li_not_selectable">
                                               <span class="menu_title" id="submodule{{$module5->id_module_structure}}">
                                                 
                                        <a href="javascript:void(0); ">{{$module5->id_name}}</a>
                                                   {{($module5->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>

                                                                                                        <div class="row margin_left_minus1">
                                                                                                            <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module5->id_module_structure); ?>
                                                                                                            <div class="col-xs-2 ">
                                                                                                            </div>
                                                                                                            <div class="col-xs-10 inside-with-border">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-xs-11">
                                                                                                                        @if(count($pr_module_tasks)>0)
                                                                                                                            @foreach($pr_module_tasks as $pr_module_task)
                                                                                                                                <i class="fa fa-angle-double-right  "></i>
                                                                                                                                <a href="{{URL::to('modal_dialog_edit_task/'.$pr_module_task->id_ms_task)}}"
                                                                                                                                   data-toggle="modal"
                                                                                                                                   data-target="#modalEditTask"
                                                                                                                                   title="">  {{  $pr_module_task->name_task }} </a>
                                                                                                                                {{($pr_module_task->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                                                                <br>
                                                                                                                            @endforeach
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                    <div class="col-xs-1">
                                                                                                                        <a href="{{URL::to('modal_dialog_add_task/'.$module5->id_module_structure)}}" class="btn ls-light-blue-btn addSubmoduleTask" data-toggle="modal" data-target="#modalAddTask"> <i
                                                                                                                                    class="fa fa-plus"></i> </a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </ul>


                                                                                    </li>
                                                                                @else
                                                                                    <li class="li_not_selectable">
                                               <span class="menu_title" id="submodule{{$module4->id_module_structure}}">
                                                   
                                        <a href="javascript:void(0); ">{{$module4->id_name}}</a>
                                                   {{($module4->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>

                                                                                        <div class="row margin_left_minus1">
                                                                                            <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module4->id_module_structure); ?>
                                                                                            <div class="col-xs-2 ">
                                                                                            </div>
                                                                                            <div class="col-xs-10 inside-with-border">
                                                                                                <div class="row">
                                                                                                    <div class="col-xs-11">
                                                                                                        @if(count($pr_module_tasks)>0)
                                                                                                            @foreach($pr_module_tasks as $pr_module_task)
                                                                                                                <i class="fa fa-angle-double-right  "></i>
                                                                                                                <a href="{{URL::to('modal_dialog_edit_task/'.$pr_module_task->id_ms_task)}}"
                                                                                                                   data-toggle="modal"
                                                                                                                   data-target="#modalEditTask"
                                                                                                                   title="">  {{  $pr_module_task->name_task }} </a>
                                                                                                                {{($pr_module_task->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                                                <br>
                                                                                                            @endforeach
                                                                                                        @endif
                                                                                                    </div>
                                                                                                    <div class="col-xs-1">
                                                                                                        <a href="{{URL::to('modal_dialog_add_task/'.$module4->id_module_structure)}}" class="btn ls-light-blue-btn addSubmoduleTask" data-toggle="modal" data-target="#modalAddTask"> <i
                                                                                                                    class="fa fa-plus"></i> </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                @endif
                                                                            @endforeach
                                                                        </ul>

                                                                    </li>
                                                                @else
                                                                    <li class="li_not_selectable">
                                               <span class="menu_title" id="submodule{{$module3->id_module_structure}}">
                                                 
                                        <a href="javascript:void(0); ">{{$module3->id_name}}</a>
                                                   {{($module3->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>

                                                                        <div class="row margin_left_minus1">
                                                                            <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module3->id_module_structure); ?>
                                                                            <div class="col-xs-2 ">
                                                                            </div>
                                                                            <div class="col-xs-10 inside-with-border">
                                                                                <div class="row">
                                                                                    <div class="col-xs-11">
                                                                                        @if(count($pr_module_tasks)>0)
                                                                                            @foreach($pr_module_tasks as $pr_module_task)
                                                                                                <i class="fa fa-angle-double-right  "></i>
                                                                                                <a href="{{URL::to('modal_dialog_edit_task/'.$pr_module_task->id_ms_task)}}"
                                                                                                   data-toggle="modal"
                                                                                                   data-target="#modalEditTask"
                                                                                                   title="">  {{  $pr_module_task->name_task }} </a>
                                                                                                {{($pr_module_task->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                                <br>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="col-xs-1">
                                                                                        <a href="{{URL::to('modal_dialog_add_task/'.$module3->id_module_structure)}}" class="btn ls-light-blue-btn addSubmoduleTask" data-toggle="modal" data-target="#modalAddTask"> <i class="fa fa-plus"></i> </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @else
                                                    <li class="li_not_selectable">
                                               <span class="menu_title" id="submodule{{$module2->id_module_structure}}">
                                                   
                                        <a href="javascript:void(0); ">{{$module2->id_name}}</a>
                                                   {{($module2->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                               </span>

                                                        <div class="row margin_left_minus1">
                                                            <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module2->id_module_structure); ?>
                                                            <div class="col-xs-2 ">
                                                            </div>
                                                            <div class="col-xs-10 inside-with-border">
                                                                <div class="row">
                                                                    <div class="col-xs-11">
                                                                        @if(count($pr_module_tasks)>0)
                                                                            @foreach($pr_module_tasks as $pr_module_task)
                                                                                <i class="fa fa-angle-double-right  "></i>
                                                                                <a href="{{URL::to('modal_dialog_edit_task/'.$pr_module_task->id_ms_task)}}"
                                                                                   data-toggle="modal"
                                                                                   data-target="#modalEditTask"
                                                                                   title="">  {{  $pr_module_task->name_task }} </a>
                                                                                {{($pr_module_task->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                <br>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-xs-1">
                                                                        <a href="{{URL::to('modal_dialog_add_task/'.$module2->id_module_structure)}}" class="btn ls-light-blue-btn addSubmoduleTask" data-toggle="modal" data-target="#modalAddTask"> <i class="fa fa-plus"></i> </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>

                                    </li>
                                @else
                                    <li class="li_not_selectable">
                                                                                     <span class="menu_title" id="submodule{{$module1->id_module_structure}}">
                                                                                        
                                        <a href="javascript:void(0); ">{{$module1->id_name}}</a>
                                                                                         {{($module1->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                     </span>

                                        <div class="row margin_left_minus1">
                                            <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module1->id_module_structure); ?>
                                            <div class="col-xs-2 ">
                                            </div>
                                            <div class="col-xs-10 inside-with-border">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        @if(count($pr_module_tasks)>0)
                                                            @foreach($pr_module_tasks as $pr_module_task)
                                                                <i class="fa fa-angle-double-right  "></i>
                                                                <a href="{{URL::to('modal_dialog_edit_task/'.$pr_module_task->id_ms_task)}}"
                                                                   data-toggle="modal"
                                                                   data-target="#modalEditTask"
                                                                   title="">  {{  $pr_module_task->name_task }} </a>
                                                                {{($pr_module_task->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a href="{{URL::to('modal_dialog_add_task/'.$module1->id_module_structure)}}" class="btn ls-light-blue-btn addSubmoduleTask" data-toggle="modal" data-target="#modalAddTask"> <i class="fa fa-plus"></i> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- Modal Add mew task and structure -->
            <div class="modal fade" id="addNewTask" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content" id="borderInnerTask">
                        <button type="button" class="close close_modal"
                                id="close_modal_new" data-dismiss="modal" aria-hidden="true"> x
                        </button>
                        {{ Form::open(['action'=>'TaskController@add_new_task_admin' ])}}
                        <div class="modal-header task-label-white">
                            Add new task
                            <div class="clear"></div>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form_edit">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Task Name" name="name_task" required>
                                        </div>

                                        <div class="form-group">
                                            <textarea style="width: 100%" name="description_task" class="summernote_new" id="summernote_new"></textarea>
                                        </div>
                                        <div class="form-group">
                                            {{Form::checkbox('is_checked' ,1, false,['class'=>'switchCheckBox','data-size'=>"mini"])}} <label> Mandatory Task</label>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{Form::submit('Save and close', ['class'=>"btn ls-light-blue-btn"])}}
                            {{Form::hidden('structure','',['id'=>'easyTreeStructure'])}}
                            {{Form::hidden('fk_industry',$industry->id_industry)}}
                            <button type="button" class="btn ls-red-btn" data-dismiss="modal">
                                Cancel
                            </button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
            <!-- /.modal -->

            <!-- Modal Edit Task -->
            <div class="modal fade" id="modalEditTask" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- Modal Edit Task End -->

            <!-- Modal Add new task for existing structure -->
            <div class="modal fade" id="modalAddTask" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- Modal Add Task End -->

        </div>
        <!-- /.box-body -->
    </div>
@stop