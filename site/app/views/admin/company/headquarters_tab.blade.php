<div class="tab-pane fade " id="headquarters">
    <div class="row">
        <div class="col-md-12">
            <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
                <i class="fa fa-map-marker mgr-10 profile-icon"></i>
                {{trans('messages.nav_headquarters')}}
                <a href="javascript:void(0)"
                   id="a_form_headquarter_0"
                   class="btn static_add_button ls-light-blue-btn btn-round btn-xxl">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="title_static_add_button" id="div_a_form_headquarter_0">
                    {{trans('messages.add_headquarter')}}
                </div>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="edit_form_hidden_headquarter col-lg-3 col-md-4 col-sm-6 col-xs-12 pd-10 center" id="edit_form_hidden_headquarter0">
            <div class="bg-grey pd-5">
                <div class="headquarter-list">
                    <a href="javascript:void(0);" id="a_details_form_show_0">
                        <img class="img-circle" alt="" src="{{ URL::asset('images/headquarter-white.png')}}">
                    </a>
                </div>
                <div class="ls-friends-about">
                    {{ Form::open(['action'=>'HeadquartersController@store','id'=>'addHeadquarterForm']) }}
                    <div class="form-group has-feedback">
                        {{ Form::text("name_headquarter[1]","",[
                        "placeholder"=>trans('messages.label_name_headquarter'),
                        "class"=>"form-control","required",
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                         ]) }}
                        {{ $errors->first("name_headquarter[1]","<div class='text-red'>:message</div>") }}
                        <i class="fa fa-map-marker form-control-feedback"></i>
                    </div>

                    <div class="form-group has-feedback">
                        {{--<div id="locationField">--}}
                        {{ Form::text("autocomplete[1]","",array( "placeholder"=>trans('messages.label_headquarter_address'),
                        "class"=>"form-control", "id"=>"autocomplete1","onFocus"=>"initialize('1')","required",
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                        )) }}
                        {{ $errors->first("autocomplete[1]","<div class='text-red'>:message</div>") }}
                        <i class="fa fa-map-o form-control-feedback"></i>
                        {{--</div>--}}
                    </div>
                    <div class="form-group">
                        {{ Form::text("street[1]","",[ "placeholder"=>trans('messages.label_street'),
                          "class"=>"form-control","id"=>"route1","required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                          ]) }}
                        {{ $errors->first("street[1]","<div class='text-red'>:message</div>") }}
                    </div>
                    <div class="form-group">
                        {{ Form::text("street_number[1]","",[ "placeholder"=>trans('messages.label_street_nr'),
                          "class"=>"form-control","id"=>"street_number1","required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                           ]) }}
                        {{ $errors->first("street_number[1]","<div class='text-red'>:message</div>") }}
                    </div>
                    <div class="form-group">
                        {{ Form::text("postal_code[1]","",[ "placeholder"=>trans('messages.label_zip_code'),
                          "class"=>"form-control","id"=>"postal_code1", "required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                          ]) }}
                        {{ $errors->first("postal_code[1]","<div class='text-red'>:message</div>") }}
                    </div>
                    <div class="form-group">
                        {{ Form::text("city[1]","",[ "placeholder"=>trans('messages.label_city'),
                          "class"=>"form-control","id"=>"locality1","required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                          ]) }}
                        {{ $errors->first("city[1]","<div class='text-red'>:message</div>") }}
                    </div>

                    <div class="form-group">
                        {{ Form::text("state[1]","",[ "placeholder"=>trans('messages.label_state'), "class"=>"form-control","id"=>"administrative_area_level_11" ]) }}
                        {{ $errors->first("state[1]","<div class='text-red'>:message</div>") }}
                    </div>
                    <div class="form-group">
                        {{ Form::text("country[1]","",[ "placeholder"=>trans('messages.label_country'),
                          "class"=>"form-control","id"=>"country1","required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')"
                          ]) }}
                        {{ $errors->first("country[1]","<div class='text-red'>:message</div>") }}
                        {{ Form::hidden("latitude[1]","",[  "id"=>"latitude1" ]) }}
                        {{ Form::hidden("longitude[1]","",[ "id"=>"longitude1" ]) }}
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::text("telephone_headquarter[1]","",[ "placeholder"=>trans('messages.label_phone'), "class"=>"form-control" ]) }}
                        {{ $errors->first("telephone_headquarter[1]","<div class='text-red'>:message</div>") }}
                        <i class="fa fa-phone form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::text("fax_headquarter[1]","",[
                        "placeholder"=>trans('messages.label_fax'), "class"=>"form-control" ]) }}
                        {{ $errors->first("fax_headquarter[1]","<div class='text-red'>:message</div>") }}
                        <i class="fa fa-fax
                                                            form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::email("email_headquarter[1]","",[ "placeholder"=>trans('messages.label_email'), "class"=>"form-control", "required",
                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                          "oninput"=>"this.setCustomValidity('')", 'data_url_verify'=> action('SessionsController@checkHeadquarterEmail'), 'onblur'=>'validateEmail($(this));'
                           ]) }}
                        {{ $errors->first("email_headquarter[1]","<div class='text-red'>:message</div>") }}
                        <i class="fa fa-envelope form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::text("social_xing",'' ,[
                        "placeholder"=> trans('messages.label_xing')  , "class"=>"form-control",
                        'id'=>'social_xing']) }}
                        {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                        <i class="fa fa-xing form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::text("social_linkedin",'',[
                        "placeholder"=> trans('messages.label_linkedin')  , "class"=>"form-control",
                        'id'=>'social_twitter']) }}
                        {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                        <i class="fa fa-linkedin form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::text("social_twitter",'',[
                        "placeholder"=> trans('messages.label_twitter')  , "class"=>"form-control",
                        'id'=>'social_twitter']) }}
                        {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                        <i class="fa fa-twitter form-control-feedback"></i>
                    </div>
                    {{ $errors->first("confirm_user",'<div class="text-green">:message</div>') }}
                    {{ Form::hidden("id_company",$company->id_company) }}
                    {{Form::hidden('redirect_link','company')}}
                    <div class="row">
                        <div class="col-xs-6" style="padding: 0 ;">
                            {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block add_new_headquarter ls-light-blue-btn"])}}
                        </div>
                        <div class="col-xs-6 " style="padding: 0 0 0 4px;">
                            <button type="button" class="btn btn-danger btn-block a_form_headquarter_close" id="a_form_headquarter_close_0">
                                {{trans('messages.act_close')}}
                            </button>
                        </div>
                    </div>
                    {{ Form::close(); }}
                            <!-- /.col -->
                </div>
            </div>
        </div>
        @foreach($headquarters  as $headquarter)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pd-10  center">
                <div class="bg-grey  pd-15">
                    <div class="headquarter-list">
                        <ul class="social-links-user">
                            <li>
                                @if($headquarter->social_xing)
                                  <a href="{{$headquarter->social_xing}}" target="_blank" class="btn ls-xing-btn btn-round">
                                    <i class="fa fa-xing"></i>
                                  </a>
                                @else
                                  <a href="#" class="btn ls-red-btn btn-round disabled">
                                    <i class="fa fa-xing"></i>
                                  </a>
                                @endif
                            </li>
                            <li>
                                @if($headquarter->social_linkedin)
                                  <a href="{{$headquarter->social_linkedin}}" target="_blank" class="btn ls-linkedin-btn btn-round">
                                    <i class="fa fa-linkedin"></i>
                                  </a>
                                @else
                                  <a href="#" class="btn ls-red-btn btn-round disabled">
                                    <i class="fa fa-linkedin"></i>
                                  </a>
                                @endif
                            </li>
                            <li>
                                @if($headquarter->social_twitter)
                                  <a href="{{$headquarter->social_twitter}}" target="_blank" class="btn ls-twitter-btn btn-round">
                                    <i class="fa fa-twitter"></i>
                                  </a>
                                @else
                                  <a href="#" class="btn ls-red-btn btn-round disabled">
                                    <i class="fa fa-twitter"></i>
                                  </a>
                                @endif
                            </li>
                        </ul>
                        <a href="javascript:void(0);" id="a_details_form_show_{{$headquarter->id_headquarter}}">
                            <img class="img-circle" alt="" src="{{ URL::asset('images/headquarter-white.png')}}">
                        </a>
                        <div class="headquarter_details">
                            <div class="headquarter_header mgbt-md-5">
                                {{$headquarter->name_headquarter}}<br>
                            </div>
                            {{$headquarter->street}} {{$headquarter->street_nr}}<br>
                            {{$headquarter->postal_code}}, {{$headquarter->city}}<br>
                            {{$headquarter->country}}<br>
                            {{--<i class=" fa fa-map-marker"></i>--}}
                            <div class="div_headquarter_details   mgtp-10">
                                <div class="fa_width_user">
                                    <i class=" fa fa-phone"></i>
                                </div>
                                @if($headquarter->telephone_headquarter)
                                    {{$headquarter->telephone_headquarter}}
                                @else
                                    <i>{{trans('messages.unavailable')}}</i>
                                @endif
                                <br>
                                <div class="fa_width_user">
                                    <i class=" fa fa-fax"></i>
                                </div>
                                @if($headquarter->fax_headquarter)
                                    {{$headquarter->fax_headquarter}}
                                @else
                                    <i>{{trans('messages.unavailable')}}</i>
                                @endif
                                <br>
                                <div class="fa_width_user">
                                    <i class=" fa fa-envelope-o"></i>
                                </div>
                                <a href="mailto:{{$headquarter->email_headquarter}}">
                                    {{$headquarter->email_headquarter}}
                                </a>
                            </div>
                        </div>
                        <div class="user-friend-list">
                            <?php
                                $headquarters_users = User::getUsersByHeadquarter($headquarter->id_headquarter);
                            ?>
                            <div class="row center mgtp-10">
                                <button class="col-lg-1 col-md-1 col-sm-1 col-xs-1 prev-members transparent-btn">
                                  @if(count($headquarters_users) > 3)
                                    <i class="fa fa-angle-left" aria-hidden="true" style="font-size: 30px;cursor: not-allowed;opacity: .4"></i>
                                  @endif
                                </button>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    @foreach($headquarters_users as $key => $headquarter_user)
                                        @if($key < 3)
                                            <div class="col-xs-6 col-sm-3 pd-3 col-centered">
                                                <a href="{{URL::to('members/'.$headquarter_user->id)}}" title="{{$headquarter_user->first_name .' '.$headquarter_user->last_name}}">
                                                    <?php $image_user = User::getLogoUser($headquarter_user->id); ?>
                                                    <img class="img-circle" alt="friends pic" src="{{ URL::asset($image_user)}}">
                                                </a>
                                            </div>
                                        @else
                                            <div class="col-xs-6 col-sm-3 pd-3 col-centered" style="display: none">
                                                <a href="{{URL::to('users/'.$headquarter_user->id)}}"
                                                   title="{{$headquarter_user->first_name . ' ' . $headquarter_user->last_name}}">
                                                    <?php $image_user = User::getLogoUser($headquarter_user->id);
                                                    ?>
                                                    <img class="img-circle"
                                                         alt="friends pic"
                                                         src="{{ URL::asset($image_user)}}">
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                    <button class="col-lg-1 col-md-1 col-sm-1 col-xs-1 next-members transparent-btn">
                                        @if(count($headquarters_users) > 3)
                                          <i class="fa fa-angle-right" aria-hidden="true" style="font-size: 30px;cursor: pointer;"></i>
                                        @endif
                                    </button>
                            </div>
                        </div>
                        @if(in_array('2',$fk_group))
                            <div class="edit-details-headquarters">
                                <ul class="menu_user">
                                    @if($headquarter->blocked == 1)
                                        <li>
                                            <a href="{{URL::to('members/approve',[$headquarter->id_headquarter])}}"
                                               class="btn ls-light-blue-btn btn-round">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0)"
                                           id="a_form_headquarter_{{$headquarter->id_headquarter;}}"
                                           class="btn ls-orange-btn a_form_headquarter btn-round">
                                            <i class="fa fa-edit"></i></a>
                                    </li>
                                    @if($headquarter->is_main != 1)
                                        <li>
                                            <a href="{{URL::to('headquarters/admin_make_main',
                                                                        [$headquarter->id_headquarter])}}"
                                               title="Set as main headquarter"
                                               class="btn ls-light-green-btn btn-round">
                                                <i class="fa fa-black-tie"></i>
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{URL::to('headquarters/make_main',
                                                                        [$headquarter->id_headquarter])}}"
                                               class="btn ls-light-green-btn disabled btn-round">
                                                <i class="fa fa-black-tie"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                                <div class="row edit_form_hidden_headquarter"
                                     id="edit_form_hidden_headquarter{{$headquarter->id_headquarter;}}"
                                     style="display: none; margin-top: 15px;">

                                    {{ Form::model($headquarter, ['method'=>'PATCH', 'route'=>['headquarters.update',$headquarter->id_headquarter]])}}
                                    <input type="hidden" name="admin_change_headquarter" value="1" />
                                    <div class="form-group has-feedback">
                                        {{ Form::text("name_headquarter",$headquarter->name_headquarter,[ "placeholder"=>trans('messages.label_name_headquarter'), "class"=>"form-control" ]) }}
                                        {{ $errors->first("name_headquarter","<div class='text-red'>:message</div>") }}
                                        <i class="fa fa-map-marker
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{--<div id="locationField">--}}
                                        {{ Form::text("autocomplete",$headquarter->address_string,array( "placeholder"=>trans('messages.label_headquarter_address'),
                                        "class"=>"form-control", "id"=>"autocomplete".$headquarter->id_headquarter,
                                        "onFocus"=>"initialize('".$headquarter->id_headquarter."')","required",
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                        )) }}
                                        {{ $errors->first("autocomplete","<div class='text-red'>:message</div>") }}
                                        <i class="fa fa-map-o
                                                            form-control-feedback"></i>
                                        {{--</div>--}}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("street",$headquarter->street,[ "placeholder"=>trans('messages.label_street'), "class"=>"form-control","id"=>"route".$headquarter->id_headquarter,"required",
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                          ]) }}
                                        {{ $errors->first("street","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("street_number",$headquarter->street_nr,[ "placeholder"=>trans('messages.label_street_nr'), "class"=>"form-control","id"=>"street_number".$headquarter->id_headquarter ]) }}
                                        {{ $errors->first("street_number","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("postal_code",$headquarter->postal_code,[ "placeholder"=>trans('messages.label_zip_code'), "class"=>"form-control","id"=>"postal_code".$headquarter->id_headquarter]) }}
                                        {{ $errors->first("postal_code","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("city",$headquarter->city,[ "placeholder"=>trans('messages.label_city'), "class"=>"form-control","id"=>"locality".$headquarter->id_headquarter,"required",
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                          ]) }}
                                        {{ $errors->first("city","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("state",$headquarter->state,[ "placeholder"=> trans('messages.label_state') , "class"=>"form-control","id"=>"administrative_area_level_1".$headquarter->id_headquarter ]) }}
                                        {{ $errors->first("state","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text("country",$headquarter->country,[ "placeholder"=>trans('messages.label_country'), "class"=>"form-control","id"=>"country".$headquarter->id_headquarter,"required",
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                          ]) }}
                                        {{ $errors->first("country","<div class='text-red'>:message</div>") }}
                                        {{ Form::hidden("latitude",$headquarter->latitude,[  "id"=>"latitude".$headquarter->id_headquarter ]) }}
                                        {{ Form::hidden("longitude",$headquarter->longitude,[ "id"=>"longitude".$headquarter->id_headquarter ]) }}
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("telephone_headquarter",$headquarter->telephone_headquarter,[ "placeholder"=>trans('messages.label_phone'), "class"=>"form-control" ]) }}
                                        {{ $errors->first("telephone_headquarter","<div class='text-red'>:message</div>") }}
                                        <i class="fa fa-phone
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("fax_headquarter",
                                        $headquarter->fax_headquarter,[ "placeholder"=>trans('messages.label_fax'), "class"=>"form-control" ]) }}
                                        {{ $errors->first("fax_headquarter","<div class='text-red'>:message</div>") }}
                                        <i class="fa fa-fax
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::email("email_headquarter",$headquarter->email_headquarter,[ "placeholder"=>trans('messages.label_email'), "class"=>"form-control", "required",
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                           ]) }}
                                        {{ $errors->first("email_headquarter","<div class='text-red'>:message</div>") }}
                                        <i class="fa fa-envelope
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_xing",$headquarter->social_xing ,[
                                        "placeholder"=>"Xing Account", "class"=>"form-control",
                                        'id'=>'social_xing']) }}
                                        {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-xing
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_linkedin",$headquarter->social_linkedin ,[
                                        "placeholder"=>"Linkedin Account", "class"=>"form-control",
                                        'id'=>'social_twitter']) }}
                                        {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-linkedin
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_twitter",$headquarter->social_twitter ,[
                                        "placeholder"=>"Twitter Account", "class"=>"form-control",
                                        'id'=>'social_twitter']) }}
                                        {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-twitter
                                                            form-control-feedback"></i>
                                    </div>

                                    {{ $errors->first("confirm_user",'<div class="text-green">:message</div>') }}
                                    {{ Form::hidden("id_company",$headquarter->fk_company) }}
                                    {{ Form::hidden("id_location",$headquarter->fk_location) }}
                                    <div class="row">
                                        <div class="col-xs-6" style="padding: 0;">
                                            {{ Form::submit(trans('messages.act_save'),["class" => "btn ls-light-blue-btn  btn-flat btn-block" ]) }}
                                        </div>
                                        <div class="col-xs-6" style="padding: 0 0 0 4px;">
                                            <button type="button" class="btn btn-danger btn-block a_form_headquarter_close" id="a_form_headquarter_close_{{$headquarter->id_headquarter}}">
                                                {{trans('messages.act_close')}}
                                            </button>

                                        </div>
                                    </div>

                                    {{ Form::close() }}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        @endforeach
    </div>
</div>
