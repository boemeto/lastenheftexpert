<?php
use Illuminate\Support\MessageBag;

class ModuleController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        $module = Module::paginate(20);
//        $pagination = $module->appends(
//            array(
//                'sort' => 'name_module',
//                'order' => 'asc'
//            ))->links();
//
//        return View::make('admin.module.index', compact('module', 'pagination'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
//        return View::make('admin.module.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
//        if (!Module::isValid(Input::all())) {
//            return Redirect::back()->withInput()
//                ->withErrors(Module::$messages);
//        }
//        $module = new Module();
//        $module->fill(Input::all());
//        $module->save();
//
//        return Redirect::route('module.index')->with('success_message', trans('validation.add_success'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
//        $module = Module::getFirstById($id);
//
//
//        return View::make('admin.module.edit', compact('module'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        // dd(Input::all());
        $module = Module::getFirstById($id);
        $module->fill(Input::all());
        $current_date = date("Y-m-d H:i:s");
        if (Input::has('is_default') && Input::get('is_default') == 1) {
            $module->is_default = 1;
            $module->locked_at = "0000-00-00 00:00:00";
            $is_default = 1;
            $module->save();
        } else {
            $module->is_default = 0;
            $module->locked_at = $current_date;
            $is_default = 0;
            $module->save();
            $module = Module::getFirstById($id);
            $locked_at = strtotime($ms_task->updated_at);
            $locked_at = date('Y-m-d H:m:s', $locked_at);
            $ms_task->locked_at = $locked_at;
            $ms_task->save();
        }

        $this->selectAllChildren(Input::get('id_module_structure'), $module->is_default);
        $id_module_structure = Input::get('id_module_structure');
        Session::flash('scrollTo', $id_module_structure);

        $mod_str = Module_structure::getFirstById($id_module_structure);
        $mod_str->is_default = $is_default;

        if($is_default == 0) {
            $mod_str->locked_at = $current_date;
        } else {
            $mod_str->locked_at = "0000-00-00 00:00:00";
        }

        $mod_str->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function selectAllChildren($id_module_structure, $is_default)
    {
        $current_date = date("Y-m-d H:i:s");
        if (Module_structure::hasChild($id_module_structure)) {
            $module_children = Module_structure::getModuleChildren($id_module_structure);
            foreach ($module_children as $module_child) {
                $module_structure = Module_structure::getFirstById($module_child->id_module_structure);
                $module_structure->is_default = $is_default;
                if($is_default == 0) {
                    // dd($is_default);
                    $module_structure->locked_at = $current_date;
                } else {

                    $module_structure->locked_at = "0000-00-00 00:00:00";
                }
                $module_structure->save();
                self::selectAllChildren($module_child->id_module_structure, $is_default);
            }
        } else {
            $module_tasks = Ms_task::getSoftwareTasks($id_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $id_ms_task = $module_task->id_ms_task;
                    $ms_task = Ms_task::getFirstById($id_ms_task);
                    $ms_task->is_default = $is_default;
                    if($is_default == 0) {
                        $ms_task->locked_at = $current_date;
                    } else {
                        $ms_task->locked_at = "0000-00-00 00:00:00";
                    }
                    $ms_task->save();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        // dd($id);
        $module = Module::getFirstById($id);
        $module->delete();
        return Redirect::route('module.index')->with('success_message', trans('validation.delete_success'));
    }

    public function set_order_adm_modules()
    {
        $list_order = Input::get('list_order');
        $list = explode(',', $list_order);
        $i = 1;
        foreach ($list as $id) {
            $task = Module_structure::getFirstById($id);
            $task->ms_order = $i;
            $task->save();
            $i++;
        }
        return $list_order;
    }

    public function set_order_adm_industry_modules()
    {
        $list_order = Input::get('list_order');
        $fk_industry = Input::get('fk_industry');
        $list = explode(',', $list_order);
        $i = 1;
        foreach ($list as $id) {

            $task = Industry_ms_order::where('fk_module_structure', '=', $id)
                ->where('fk_industry', '=', $fk_industry)
                ->first();
            if (count($task) > 0) {
                $task->ms_ind_order = $i;
                $task->save();
            } else {
                $task = new Industry_ms_order();
                $task->fk_module_structure = $id;
                $task->fk_industry = $fk_industry;
                $task->ms_ind_order = $i;
                $task->save();
            }
            $i++;
        }
        return $list_order;
    }


}
