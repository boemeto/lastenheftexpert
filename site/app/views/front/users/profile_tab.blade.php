<div class="tab-pane fade active in" id="profile">
    <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
        <i class="fa fa-user mgr-10 profile-icon"></i> {{trans('messages.label_about')}}
        @if (in_array('2', $fk_group) || $id_user == $id_user_logged)
            <a href="javascript:void(0)"
               id="a_form_show_{{$user->id}}" data-color="#F0AD4E"
               class="btn static_add_button ls-orange-btn a_form_show  btn-round btn-xxl">
                <i class="fa fa-edit append-icon"></i>
            </a>
            <div class="title_static_add_button" id="div_a_form_show_{{$user->id}}">
                {{trans('messages.act_edit')}}
            </div>
        @endif
    </h3>
    <div class="row " style="margin-bottom: 20px">
        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 ">
            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.label_first_name')}}:&nbsp;</th>
                    <td> {{$user->first_name}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_last_name')}}:&nbsp;</th>
                    <td> {{$user->last_name}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_job')}}:&nbsp;</th>
                    <td>{{$user->job_user}}</td>
                </tr>
            </table>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.label_phone')}}:&nbsp;</th>
                    <td>{{$user->telephone_user}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_mobile')}}:&nbsp;</th>
                    <td>{{$user->mobile_user}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_email')}}:&nbsp;</th>
                    <td><a href="mailto:{{$user->email}}"> {{$user->email}}</a></td>
                </tr>
            </table>
        </div>
    </div>
    @if(in_array('2', $fk_group) || $id_user == $id_user_logged)
        <div class="row edit_form_hidden" style="margin-bottom: 20px; display:none; " id="edit_form_hidden_{{$user->id;}}">
            <?php $headquarters_user = Company_headquarters::getHeadquartersSelect($user->fk_company); ?>
            {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id],'enctype'=>"multipart/form-data"])}}

            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                <div class="form-group ">
                    {{ Form::select("fk_personal_title",$personal_titles,$user->fk_personal_title,[ 'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }}
                    {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}
                </div>
                <div class="form-group">
                    <input type="hidden" name="fk_headquarter" value="{{$user->fk_headquarter}}">
                    {{ Form::select("fk_title",$titles,$user->fk_title,[  "class"=>"form-control",'id'=>'fk_title', "required"]) }}
                    {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::text("first_name",$user->first_name,[ "placeholder"=>trans('messages.label_first_name'),
                      "class"=>"form-control",'id'=>'first_name', "required",
                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                      "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                    {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::text("last_name",$user->last_name,[ "placeholder"=>trans('messages.label_last_name'),
                      "class"=>"form-control",'id'=>'last_name', "required",
                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                      "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                    {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("job_user",$user->job_user ,[ "placeholder"=>trans('messages.label_job'), "class"=>"form-control",
                    'id'=>'job_user']) }}
                    {{ $errors->first("job_user",'<div class="text-red">:message</div>') }}
                    <i class="glyphicon glyphicon-briefcase form-control-feedback"></i>
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::email("email",$user->email,["placeholder"=>trans('messages.label_email'), "class"=>"form-control", 'id'=>'email',"readonly"]) }}
                    {{ $errors->first("email",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::password("password", [ "placeholder"=>trans('messages.label_password'),
                    "class"=>"form-control",  'id'=>'password']) }}
                    {{ $errors->first("password",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-key form-control-feedback"></i>
                </div>

            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                @if(in_array('2', $fk_group) )
                    <div class="form-group">
                        {{ Form::select("fk_headquarter",$headquarters_user,$user->fk_headquarter,[ 'id'=>'fk_headquarter', "class"=>"form-control", "required"]) }}
                        {{ $errors->first("fk_headquarter","<div class='text-red'>:message</div>") }}
                    </div>
                @endif
                <div class="form-group has-feedback">
                    {{ Form::text("mobile_user",$user->mobile_user ,["placeholder"=>trans('messages.label_mobile'), "class"=>"form-control",   'id'=>'mobile_user']) }}
                    {{ $errors->first("mobile_user",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-mobile form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("telephone_user",$user->telephone_user ,[  "placeholder"=>trans('messages.label_phone'), "class"=>"form-control", 'id'=>'telephone_user']) }}
                    {{ $errors->first("telephone_user",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_xing",$user->social_xing ,[
                    "placeholder"=>"Xing Account", "class"=>"form-control",
                    'id'=>'social_xing']) }}
                    {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-xing form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_linkedin",$user->social_linkedin ,[
                    "placeholder"=>"LinkedIn Account", "class"=>"form-control",
                    'id'=>'social_twitter']) }}
                    {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-linkedin form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_twitter",$user->social_twitter ,[
                    "placeholder"=>"Twitter Account", "class"=>"form-control",
                    'id'=>'social_twitter']) }}
                    {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-twitter form-control-feedback"></i>
                </div>
                <div class="form-group">
                    <input id="file-user" type="file" name="logo_user" multiple="flase">
                </div>

                {{Form::hidden('redirect_link','profile')}}
                        <!-- /.col -->
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                        {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block  ls-light-blue-btn margin-small-bottom "])}}
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                        {{Form::button(trans('messages.act_cancel'),['class'=>"btn btn-block a_form_close  ls-red-btn margin-small-bottom ",'id'=>'a_form_close_'.$user->id])}}
                    </div>
                </div>
            </div>
            {{ Form::close(); }}
        </div>
    @endif
    <hr>
    <div class="row" style="margin-bottom: 20px">
        <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-building-o mgr-10 profile-icon"></i> {{trans('messages.nav_company')}}</h3>
        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">
            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.name_company')}}:&nbsp;</th>
                    <td> {{ $company->name_company }}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_phone')}}:&nbsp;</th>
                    <td>  {{$company->telephone_company}}</td>
                </tr>
                @if($company->mobile_company)
                    <tr>
                        <th>{{trans('messages.label_mobile')}}:&nbsp;</th>
                        <td>  {{$company->mobile_company}}</td>
                    </tr>
                @endif
                <tr>
                    <th>{{trans('messages.label_email')}}:&nbsp;</th>
                    <td><a href="mailto:{{$company->email_company}}">{{$company->email_company}} </a></td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_website')}}:&nbsp;</th>
                    <td><a href="{{$company->website_company}}" target="_blank">{{$company->website_company}} </a></td>
                </tr>
            </table>
        </div>
        <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">
            <table class="table-responsive tab-table">
                <tr>
                    <th>{{trans('messages.name_headquarter')}}:&nbsp;</th>
                    <td>  {{$main_headquarter->name_headquarter}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_street')}}:&nbsp;</th>
                    <td>{{$main_headquarter->street}}  {{$main_headquarter->street_nr}}</td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_zip_code')}} / {{trans('messages.label_city')}}:&nbsp;</th>
                    <td>{{$main_headquarter->postal_code}}, {{$main_headquarter->city}} </td>
                </tr>
                <tr>
                    <th>{{trans('messages.label_country')}}:&nbsp;</th>
                    <td> {{$main_headquarter->country}} </td>
                </tr>
            </table>
        </div>
    </div>
</div>
