<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:29
 */
class Industry_ms_order extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'fk_module_structure' => 'required',
    ];
    public $primaryKey = 'id_industry_ms_order';
    protected $table = 'pr_industry_ms_order';
    protected $guarded = ['id_industry_ms_order'];
    protected $fillable = ['fk_module_structure', 'fk_industry', 'ms_ind_order'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Industry_ms_order::where('id_industry_ms_order', '=', $id)->first();
        return $list;
    }


    public static function getFirstByKey($fk_module_structure, $fk_industry)
    {
        $list = Industry_ms_order::where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_industry', '=', $fk_industry)
            ->first();
        return $list;
    }


}