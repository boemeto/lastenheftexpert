@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-user"></i>

            <h3 class="box-title"> Edit User {{$user->first_name}} {{$user->last_name}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <br>
            {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id]])}}
            <div class="form-group has-feedback">
                {{ Form::select("fk_group",$groups,$user->fk_group,[ "placeholder"=>"Group",'id'=>'fk_group', "class"=>"form-control", "required"]) }}
                {{ $errors->first("fk_group","<div class='text-red'>:message</div>") }}

            </div>
            <div class="form-group has-feedback">
                {{ Form::select("fk_headquarter",$headquarters,$user->fk_headquarter,[ "placeholder"=>"Headquarter",'id'=>'fk_headquarter', "class"=>"form-control", "required"]) }}
                {{ $errors->first("fk_headquarter","<div class='text-red'>:message</div>") }}

            </div>
            <div class="form-group has-feedback">
                {{ Form::select("fk_personal_title",$personal_titles,$user->fk_personal_title,[ "placeholder"=>"Name",'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }}
                {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}

            </div>
            <div class="form-group has-feedback">
                {{ Form::select("fk_title",$titles,$user->fk_title,[ "placeholder"=>"Name", "class"=>"form-control",'id'=>'fk_title', "required"]) }}
                {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}

            </div>
            <div class="form-group has-feedback">
                {{ Form::text("first_name",$user->first_name,[ "placeholder"=>"First Name", "class"=>"form-control",'id'=>'first_name', "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{ Form::text("last_name",$user->last_name,[ "placeholder"=>"Last Name", "class"=>"form-control",'id'=>'last_name', "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{ Form::text("username",$user->username,[ "placeholder"=>"Username", "class"=>"form-control", 'id'=>'username',"readonly"]) }}
                {{ $errors->first("username",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">

                {{ Form::email("email",$user->email,["placeholder"=>"Email", "class"=>"form-control", 'id'=>'email',"readonly"]) }}
                {{ $errors->first("email",'<div class="text-red">:message</div>') }}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>


            {{ Form::submit("Edit User",["class" => "btn ls-light-green-btn  btn-flat" ]) }}

            {{ Form::close() }}
            <br><br>


        </div>
        <!-- /.box-body -->
    </div>


@stop
