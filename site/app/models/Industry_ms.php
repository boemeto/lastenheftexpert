<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 06-Jan-16
 * Time: 16:24
 */
class Industry_ms extends Eloquent
{
    public static $rules = [
        'fk_industry' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_industry_ms';
    protected $table = 'pr_industry_ms';
    protected $guarded = ['id_industry_ms'];
    protected $fillable = ['fk_industry', 'fk_ms_task', 'fk_module_structure', 'ind_description_task', 'ind_description_task_en', 'ind_order'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Industry_ms::whereId_industry_ms($id)->first();
        return $list;
    }

    public static function getFirstByKey($id_ms_task, $id_industry)
    {
        $list = Industry_ms::where('fk_ms_task', '=', $id_ms_task)
            ->where('fk_industry', '=', $id_industry)
            ->first();
        return $list;
    }

    public static function getIndustriesMSByTask($id_task)
    {
        $array = array();
        $lists = Industry_ms::whereFk_ms_task($id_task)->get();

        if (count($lists) > 0) {
            foreach ($lists as $list) {
                $array[] = $list->fk_industry;
            }
        }

        return $array;
    }

    public static function createNewItem($fk_industry, $fk_ms_task, $fk_module_structure = null)
    {
        $pr_industry_ms = new Industry_ms();
        $pr_industry_ms->fk_industry = $fk_industry;
        $pr_industry_ms->fk_ms_task = $fk_ms_task;
        $pr_industry_ms->fk_module_structure = $fk_module_structure;
        $pr_industry_ms->save();
        return $pr_industry_ms;
    }

    public static function isIndustryChecked($fk_industry, $fk_ms_task)
    {
        $responsibility = Industry_ms::where('fk_industry', '=', $fk_industry)
            ->where('fk_ms_task', '=', $fk_ms_task)->first();
        if (count($responsibility) == 1) {
            return true;

        } else {
            return false;
        }
    }

    // if this module / submodule is added to industry
    public static function isIndustryChildChecked($fk_industry, $fk_module_structure)
    {
        $result = false;
        $array_tasks_module = array();
        $array_intersect = array();
        $array_tasks_selected = Industry_ms::getSelectedTasks($fk_industry, $fk_module_structure);

        if (Module_structure::hasChild($fk_module_structure) == 1) {
            $module_children = Module_structure::getModuleChildren($fk_module_structure);
            foreach ($module_children as $module_child) {
                $result = $result || self::isIndustryChildChecked($fk_industry, $module_child->id_module_structure);
            }
        } else {
            $module_tasks = Ms_task::getSoftwareTasks($fk_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $array_tasks_module[] = $module_task->id_ms_task;
                }
            }
            $array_intersect = array_intersect($array_tasks_module, $array_tasks_selected);
            if (count($array_intersect) > 0) {
                $result = true;
            }
        }
        return $result;
    }

    //check if user selected any tasks from project
    public static function isIndustryModuleCheckedTasks($fk_industry, $fk_module_structure, $id_project)
    {
        $result = false;
        $array_tasks_module = array();
        $array_intersect = array();
        $array_tasks_selected = Projects_rows::getSelectedTasks($id_project);
        if (Module_structure::hasChild($fk_module_structure) == 1) {
            $module_children = Module_structure::getModuleChildren($fk_module_structure);
            foreach ($module_children as $module_child) {
                $result = $result || self::isIndustryModuleCheckedTasks($fk_industry, $module_child->id_module_structure, $id_project);
            }
        } else {
            $module_tasks = Ms_task::getSoftwareTasks($fk_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $array_tasks_module[] = $module_task->id_ms_task;
                }
            }
            $array_intersect = array_intersect($array_tasks_module, $array_tasks_selected);
            if (count($array_intersect) > 0) {
                $result = true;
            }
        }
        return $result;
    }

    public static function getSelectedTasks($fk_industry, $fk_module_structure)
    {
        $array_tasks_selected = array();
        $ms_tasks = Industry_ms::where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_industry', '=', $fk_industry)->get();
        if (count($ms_tasks) > 0) {
            foreach ($ms_tasks as $ms_task) {
                $array_tasks_selected[] = $ms_task->fk_ms_task;
            }
        }
        return $array_tasks_selected;
    }

    public static function getProjectSelectedTasks($fk_industry, $fk_module_structure)
    {
        $array_tasks_selected = array();
        $ms_tasks = Industry_ms::where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_industry', '=', $fk_industry)->get();
        if (count($ms_tasks) > 0) {
            foreach ($ms_tasks as $ms_task) {
                $array_tasks_selected[] = $ms_task->fk_ms_task;
            }
        }
        return $array_tasks_selected;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_industry_ms', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_industry_ms', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }
    }
}
