<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyHeadquartersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_headquarters', function ($table) {
            $table->increments('id_headquarter')->unsigned();
            $table->string('street', 100)->nullable();
            $table->integer('fk_company')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('company_headquarters')->delete();
    }

}
