<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 19.06.2015
 * Time: 10:31
 */
class Device extends Eloquent
{


    public static $rules = [
        'code_device' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_device';
    protected $table = 'devices';
    protected $guarded = ['id_device'];
    protected $fillable = ['name_device', 'code_device', 'gcm_id', 'apple_id'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Device::whereId_device($id)->first();
        return $list;
    }

}