<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:30
 */
class Adm_responsibility extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'fk_user' => 'required',
    ];
    public $primaryKey = 'id_responsibility';
    protected $table = 'adm_responsibilities';
    protected $guarded = ['id_responsibility'];
    protected $fillable = ['fk_user', 'fk_task'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_responsibility::where('id_responsibility', '=', $id)->first();
        return $list;
    }

    public static function getResponsibilityByTask($id_task)
    {
        $list = Adm_responsibility::leftJoin('users', 'id', '=', 'fk_user')
            ->where('fk_task', '=', $id_task)
            ->get();
        return $list;
    }

    public static function getResponsibleList($id_task)
    {
        $lists = Adm_responsibility::where('fk_task', '=', $id_task)
            ->get();
        foreach ($lists as $list) {
            $arr_resp[] = $list->fk_user;
        }
        return $arr_resp;
    }

    public static function createNewItem($fk_user, $fk_task)
    {
        $responsibility = new Adm_responsibility();
        $responsibility->fk_user = $fk_user;
        $responsibility->fk_task = $fk_task;
        $responsibility->save();
        return $responsibility->id_responsibility;
    }
}