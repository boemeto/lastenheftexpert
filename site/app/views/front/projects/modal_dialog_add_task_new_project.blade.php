<script src="{{ URL::asset('assets/js/projects/dropzone_add_new.js')}}"></script>
@if($project->is_empty_project == 1)
    {{ Form::open(['action'=>'ProjectsController@add_new_task_empty_project', 'id'=>'dropzoneForm', 'class'=>'dropzone dz-clickable', 'enctype'=>"multipart/form-data"])}}
@else
    {{ Form::open(['action'=>'ProjectsController@add_new_task', 'id'=>'dropzoneForm', 'class'=>'dropzone dz-clickable', 'enctype'=>"multipart/form-data"])}}
@endif
<div class="col-md-12">
    <div class="row">
        <a href="javascript:void(0)" onclick="hide_modal_box();">
            <div class="hide_controller">
                <i class="fa fa-times"></i>
            </div>
        </a>
        <div class="col-md-9 col-xs-8">
            <div class="title_line">  {{trans('messages.label_new_task')}}</div>
        </div>
        <div class="col-md-3 col-xs-4">
            <div class="ls-button-group float-right">
                <div class="colorInnerTask" id="colorInnerTask"></div>
            </div>
            <div class="ls-button-group float-right">
                {{Form::select('set_priority',  $set_priority,  0, ['class'=>'set_priority','id'=>'set_priority'])}}
            </div>
            <div class="ls-button-group float-right" style="padding: 3px;">
                {{Form::checkbox('checkbox', 1,  true,['class'=>"icheck-modal", 'id'=>'icheck-modal', 'disabled'])}}
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_sidebar_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="{{trans('messages.l_name')}}" name="name_task" required
                            oninvalid="this.setCustomValidity('{{trans('validation.validate_empty_field')}}')"
                            oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group">
                            <textarea style="width: 100%" name="task_description" class="summernote" id="summernote"></textarea>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <section class="ac-container add-new-task">
           <div>
              <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
              <label for="ac-2">{{trans('messages.label_sidebar_attachments')}}</label>
              <article class="ac-any">
                  <div class="article_content">
                      <div class="dz-message">
                          <span></span>
                          <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                      </div>
                  </div>
              </article>
           </div>
        </section>
        <section class="ac-container">
            <div>
                {{-- <input id="ac-2" name="accordion-1" type="checkbox" checked="checked"> --}}
                {{-- <label for="ac-2">{{trans('messages.label_switch_buttons')}}</label> --}}
                <article class="ac-any">
                    <div class="article_content">
                        <div class="form-group">
                            <div class="modal-switch-area">
                                <div class="modal-switch-box">
                                    {{-- <input name="is_not_offer" class="switchCheckBox is_not_offer" value="1" id="is_not_offer" type="checkbox" data-size="mini"> --}}
                                    {{-- {{trans('messages.label_is_not_offer')}} --}}
                                </div>
                                <div class="modal-switch-box">
                                    {{-- <input name="is_control" value="1" class="switchCheckBox is_control" id="is_control" type="checkbox" data-size="mini"> --}}
                                    {{-- {{trans('messages.label_is_control')}} --}}
                                    {{Form::hidden('fk_software', 0)}}
                                    {{Form::hidden('fk_project', $id_project)}}
                                    {{Form::hidden('fk_industry', $id_industry)}}
                                    {{Form::hidden('structure', $id_module_structure)}}
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<br>

<button type="submit" class="btn ls-light-blue-btn btn-block">
    {{trans('messages.act_save')}}
</button>
<button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
    {{trans('messages.act_close')}}
</button>

  {{Form::close()}}

<script type="text/javascript">
    $('#dropzoneForm').on('click', function() {
      setTimeout(function() {
         $(document).one('DOMNodeInserted', '.dz-preview', function(e) {
              $(e.target.nodeName).find('.dz-preview').each(function() {
                 $(this).insertAfter('.dz-message');
              });
            });
        });
    });
</script>
