<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 18-Aug-16
 * Time: 13:01
 */
?>
@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')


@stop
@section('footer_scripts')

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.delete_user').click(function (event) {
                var x = confirm("Are you sure you want to delete?");
                if (x) {
                    return true;
                }
                else {
                    event.preventDefault();
                    return false;
                }
            });
            // show edit user form
            /* Headquarter effects start*/
            /* Headquarter edit form*/
            $('.a_form_headquarter').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_headquarter_', "");
                $('.a_headquarter_details_hidden').not('#a_headquarter_details_hidden_' + id_nr).removeClass('active');

                $('#a_headquarter_details_hidden_' + id_nr).toggleClass("active");
//                $('.div_headquarter_details_hidden').hide();
                $('.edit_form_hidden_headquarter').not('#edit_form_hidden_headquarter' + id_nr).hide();
                $('#edit_form_hidden_headquarter' + id_nr).toggle('blind');
                $('.bg-grey').removeClass('active');
                $('#edit_form_hidden_headquarter' + id_nr).find('.bg-grey').addClass('active');
                $('#edit_form_hidden_headquarter' + id_nr).parents('.bg-grey').addClass('active');

                event.preventDefault();
            });
            $('.a_form_headquarter_close').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_headquarter_close_', "");
                $('#edit_form_hidden_headquarter' + id_nr).hide('blind');
                event.preventDefault();
            });
            /* Headquarter effects stop*/
            /* User effects start*/

            $('.a_company_show').click(function (event) {
                $('.edit_company_hidden').toggle('blind');
                event.preventDefault();
            });

            /* Show user edit form */
            $('.a_form_show').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_show_', "");
                $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                //   $('.details_user_form_hidden').hide();
                $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                $('#edit_form_hidden_' + id_nr).toggle('blind');
                $('.bg-grey').removeClass('active');
                $('#edit_form_hidden_' + id_nr).find('.bg-grey').addClass('active');
                $('#edit_form_hidden_' + id_nr).parents('.bg-grey').addClass('active');
                event.preventDefault();
            });
            /* Hide user edit form */
            $('.a_form_close').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_close_', "");
                $('#edit_form_hidden_' + id_nr).hide('blind');
                event.preventDefault();
            });

            /* User effects stop*/


            // image input functions
            animated_text_area();
            file_input_trigger();
        });
        function animated_text_area() {
            'use strict';
            $('.animatedTextArea').autosize({append: "\n"});
        }
        /*** file input Call ****/
        function file_input_trigger() {
            'use strict';
            @foreach($all_users as $user)

                $("#file-user<?php print $user->id ?>").fileinput({
                showCaption: true,
                removeLabel: "",
                removeIcon: '<i class="fa fa-times"></i> ',
                browseLabel: " ",
                initialCaption: 'upload image',
                browseClass: "btn ls-light-blue-btn btn-upload",
                browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                dropZoneEnabled: false,
                previewFileIcon: ' ',
                fileType: "any",
                'showUpload': false
            });
            @endforeach
            $("#file-3").fileinput({
                showCaption: true,
                removeLabel: "",
                removeIcon: '<i class="fa fa-times"></i> ',
                browseLabel: " ",
                initialCaption: 'upload image',
                browseClass: "btn ls-light-blue-btn btn-upload",
                browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                dropZoneEnabled: false,
                previewFileIcon: ' ',
                fileType: "any",
                'showUpload': false
            });
            $("#file-user").fileinput({
                showCaption: true,
                removeLabel: "",
                removeIcon: '<i class="fa fa-times"></i> ',
                browseLabel: " ",
                initialCaption: 'upload image',
                browseClass: "btn ls-light-blue-btn btn-upload",
                browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                dropZoneEnabled: false,
                previewFileIcon: ' ',
                fileType: "any",
                'showUpload': false
            });

        }
    </script>

    @stop

    @section('content')

            <!-- visible content start -->
    <div class="box box-profile-user">
        <!-- /.box-body -->
        <div class="box-body">

            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 user-pic-body">
                    <div class="panel widget light-widget panel-bd-top">
                        <div class="panel-body">
                            <div class="user-profile-details">
                                <ul class="social-links-user">
                                    <li>
                                        <a href="#" class="btn ls-blue-btn btn-round disabled">
                                            <i class="fa fa-xing"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn ls-blue-btn btn-round  disabled">
                                            <i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn ls-blue-btn btn-round disabled">
                                            <i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="text-center vd_info-parent square_main_image">
                                <?php $image = Admin_company::getLogoCompany($company->id_company);  ?>
                                <img src="{{ URL::asset($image)}}" alt="image">
                            </div>
                            <h2 class="font-semibold mgbt-xs-5 center"> {{$company->name_company}} </h2>

                            <div class="mgtp-20">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <td>{{trans('messages.label_member_since')}}:</td>
                                        <td> {{date('d.m.Y' ,strtotime($company->created_at))}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 tabs">
                    <ul class="nav  nav-tabs  nav-left icon-tab profile-navigation-tabs ">
                        <li class="active"><a href="#details" data-toggle="tab">
                                {{trans('messages.label_details')}}
                                <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                            </a></li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content   profile-navigation-content">
                        <!-- details Tab Start -->
                        <div class="tab-pane fade active in" id="details">

                            <h3 class="mgbt-xs-15 mgtp-10 font-semibold">


                                <a href="javascript:void(0)"
                                   id="a_company_show" data-color="#F0AD4E"
                                   class="btn static_add_button ls-orange-btn a_company_show  btn-round btn-xxl">
                                    <i class="fa fa-edit append-icon"></i>
                                </a>
                                <div class="title_static_add_button" id="div_a_company_show">
                                    {{trans('messages.act_edit')}}
                                </div>

                                <i class="fa fa-building-o mgr-10 profile-icon"></i>
                                {{trans('messages.nav_company')}}</h3>


                            <div class="row">

                                <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">

                                    <table class="table-responsive tab-table">
                                        <tr>
                                            <th>{{trans('messages.name_company')}}:&nbsp;</th>
                                            <td> {{ $company->name_company }}</td>
                                        </tr>
                                        <tr>
                                            <th> {{trans('messages.label_company_code')}}:&nbsp;</th>
                                            <td>  {{$company->company_code}}</td>
                                        </tr>
                                        <tr>
                                            <th> {{trans('messages.label_company_number')}}:&nbsp;</th>
                                            <td>  {{$company->company_number}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_phone')}}:&nbsp;</th>
                                            <td>  {{$company->telephone}}</td>
                                        </tr>


                                        @if($company->fax)
                                            <tr>
                                                <th>{{trans('messages.label_fax')}}:&nbsp;</th>
                                                <td>  {{$company->fax}}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th>{{trans('messages.label_email')}}:&nbsp;</th>
                                            <td><a href="mailto:{{$company->email}}">{{$company->email}} </a></td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_website')}}:&nbsp;</th>
                                            <td><a href="{{$company->web}}" target="_blank">{{$company->web}} </a></td>
                                        </tr>

                                    </table>

                                </div>
                                <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12">
                                    <table class="table-responsive tab-table">
                                        <tr>
                                            <th>{{trans('messages.label_company_administrator')}}:&nbsp;</th>
                                            <td>  {{$company->name_user}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_street')}}:&nbsp;</th>
                                            <td>{{$company->street}}  </td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_zip_code')}} / {{trans('messages.label_city')}}:&nbsp;</th>
                                            <td>{{$company->zip_code}}, {{$company->city}} </td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_country')}}:&nbsp;</th>
                                            <td> {{$company->country}} </td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_bank')}}:&nbsp;</th>
                                            <td> {{$company->bank}} </td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_iban')}}:&nbsp;</th>
                                            <td> {{$company->iban}} </td>
                                        </tr>
                                        <tr>
                                            <th>{{trans('messages.label_bic')}}:&nbsp;</th>
                                            <td> {{$company->bic}} </td>
                                        </tr>

                                    </table>

                                </div>
                            </div>
                            <div class="row edit_company_hidden">
                                {{ Form::open( ['method'=>'POST', 'action'=>'CompanyAdmController@update_adm_company','enctype'=>"multipart/form-data"])}}

                                <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                                    <div class="form-group has-feedback">
                                        {{ Form::text('name_company',$company->name_company,[ 'placeholder'=>trans('messages.name_company'),
                                          'class'=>"form-control", 'id'=>'name_company', 'required',
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                          ]) }}
                                        <span class="fa fa-building-o form-control-feedback"></span>
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('company_code',$company->company_code,[ 'placeholder'=>trans('messages.label_company_code'),'id'=>'telephone_company', 'class'=>"form-control"]) }}

                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('company_number',$company->company_number,[ 'placeholder'=>trans('messages.label_company_number'),'id'=>'telephone_company', 'class'=>"form-control"]) }}

                                    </div>
                                    <div class="form-group  has-feedback">
                                        {{ Form::text('telephone',$company->telephone,[ 'placeholder'=>trans('messages.label_phone'),'id'=>'telephone_company', 'class'=>"form-control"]) }}
                                        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                                    </div>
                                    <div class="form-group  has-feedback">

                                        {{ Form::text('fax',$company->fax,[ 'placeholder'=>trans('messages.label_email'), 'id'=>'fax_company', 'class'=>"form-control"]) }}

                                        <i class="fa fa-fax form-control-feedback"></i>
                                    </div>
                                    <div class="form-group  has-feedback">

                                        {{ Form::email('email',$company->email,[ 'placeholder'=>trans('messages.label_email'),'id'=>'email_company', 'class'=>"form-control"]) }}

                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                    <div class="form-group  has-feedback">
                                        {{ Form::text('web',$company->web,[ 'placeholder'=>trans('messages.label_website'),'id'=>'website_company', 'class'=>"form-control"]) }}

                                        <i class="fa fa-globe form-control-feedback"></i>
                                    </div>
                                    <div class="form-group" id="form-group_file">
                                        <input id="file-3" type="file" name="logo" multiple="flase">
                                    </div>


                                </div>
                                <div class=" col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                                    <div class="form-group has-feedback">
                                        {{ Form::text('name_user',$company->name_user,[ 'placeholder'=>trans('messages.label_company_administrator'),
                                          'class'=>"form-control", 'id'=>'name_company', 'required',
                                          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                          "oninput"=>"this.setCustomValidity('')"
                                          ]) }}
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">

                                        {{ Form::text('street',$company->street,[ 'placeholder'=>trans('messages.label_street'), 'id'=>'cui_company','class'=>"form-control"]) }}
                                        <i class="fa fa-map-marker form-control-feedback"></i>
                                    </div>

                                    <div class="form-group ">
                                        {{ Form::text('city',$company->city,[ 'placeholder'=>trans('messages.label_city'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('country',$company->country,[ 'placeholder'=>trans('messages.label_country'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('zip_code',$company->zip_code,[ 'placeholder'=>trans('messages.label_zip_code'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('bank',$company->bank,[ 'placeholder'=>trans('messages.label_bank'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('iban',$company->iban,[ 'placeholder'=>trans('messages.label_iban'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::text('bic',$company->bic,[ 'placeholder'=>trans('messages.label_bic'),  'id'=>'social_xing','class'=>"form-control"]) }}
                                    </div>


                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                                            {{ Form::hidden('id_company',$company->id_company) }}

                                            {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block  ls-light-blue-btn margin-small-bottom "])}}
                                        </div>
                                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 padding-small-right">
                                            {{Form::button(trans('messages.act_cancel'),['class'=>"btn btn-block a_company_show  ls-red-btn margin-small-bottom ",'id'=>'a_form_close_'.$user->id])}}
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <!-- details Tab End -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
