<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Feb-16
 * Time: 10:43
 */
class Consultant extends Eloquent
{

    public $primaryKey = 'id_consultant';
    protected $table = 'consultants';
    protected $guarded = ['id_consultant'];
    protected $fillable = ['name_consultant', 'email_consultant', 'image_consultant', 'tel_consultant', 'social_xing', 'social_linkedin', 'social_twitter', 'blocked'];

    public static function getLogoUser($id)
    {
        $list = Consultant::getFirstById($id);
        if ($list->image_consultant != '') {
            $logo = 'images/image_user/' . $list->image_consultant;
        } else {
            $logo = 'images/image_user/profile_default.png';
        }
        return $logo;
    }

    public static function getFirstById($id)
    {
        $list = Consultant::where('id_consultant', '=', $id)->first();
        return $list;
    }

    public static function getAllConsultants($fk_industry = 0, $all = 0)
    {
        $array = array();
        if ($all == 1) {
            $lists = Consultant::leftJoin('pr_industry_consultant', 'fk_consultant', '=', 'id_consultant');
            $lists = $lists->get();
        } else {
            $lists = Consultant::leftJoin('pr_industry_consultant', 'fk_consultant', '=', 'id_consultant')
                ->where('blocked', '=', 0);
        }
        if ($fk_industry > 0) {
            $lists = $lists->where('fk_industry', '=', $fk_industry);
        }
        $lists = $lists->orderBy('name_consultant')->get();
        foreach ($lists as $list) {
            $array[$list->id_consultant] = $list->name_consultant;
        }
        return $array;
    }

}