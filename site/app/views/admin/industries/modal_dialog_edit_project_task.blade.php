<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-md-12 text-right ">
      <div class="row">
        <div class="col-md-2 pull-right">
          {{Form::checkbox('is_default',1,$pr_module_task->is_default, ['class'=>'switchCheckBox   is_default_task inline','data-size'=>"mini", 'data-ms-task-id'=>$pr_module_task->id_ms_task,'id'=>'is_default_task_'.$pr_module_task->id_ms_task])}}
          <a href="{{URL::to('delete_task_from_structure/'.$id_ms_task)}}"
             class="formConfirm btn inline btn-round btn-l btn-block btn-danger"
             data-form="{{$id_ms_task}}"
             data-toggle="modal" data-target="#confirmDelete"
             data-title="Delete Task"
             data-message="{{trans('validation.confirmation_delete_task')}}">
              <i class="fa fa-trash"></i>
          </a>
        </div>
        <div class="col-md-2 pull-right" style="margin-top: -2px">
          <i class="fa fa-info-circle" title="{{trans('messages.label_default_task_standard')}}"></i>
        </div>
      </div>
    </div>
    <div class="  col-xs-12 mgtp-10">
        <div class="title_line"> {{ $task->name_task }} </div>
    </div>
</div>
{{-- <div class="row"> --}}
    <div class="col-md-12">
        <section class="ac-container attachment-section">
            <div id="task-attachments">
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1" class="attachment-label">{{trans('messages.label_sidebar_attachments')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        {{Form::open(['action'=>'ProjectsController@upload_file', 'class'=>'dropzone dz-clickable','id'=>'dropzoneForm'.$pr_module_task->id_ms_task,         'enctype'=>"multipart/form-data"]) }}
                        {{Form::hidden('id_ms_task', $pr_module_task->id_ms_task, ["id"=>"id_ms_task"])}}
                        {{Form::hidden('id_project', $project->id_project, ["id"=>"id_project"])}}
                        <div class="dz-message">
                            <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                        </div>
                        {{Form::close()}}
                        @if(count($attachments)>0)
                            <div class="modal-attachments">
                                <div class="row">
                                    @foreach($attachments as $attachment)
                                        <?php $icon = Projects_attach::getIconByAttachment($attachment->extension) ?>
                                        <div class="col-xs-3 col-attachments"
                                             id="col-attachments{{$attachment->id_projects_attach}}">
                                            <div style="display: block; width: 100%">
                                                <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                   download class="button_download_file" target="_blank">
                                                    <div class="file_attachments"
                                                         style="background: {{$icon}} "
                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                         title="{{$attachment->name_attachment }}">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment->id_projects_attach}}">
                                                <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                   class="button_download_file" target="_blank">
                                                    <i class="fa fa-search"></i>
                                                </a><br>
                                                <a href="javascript:void(0)"
                                                   id="button_delete_file{{$attachment->id_projects_attach}}"
                                                   class="button_delete_file">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                    @foreach($attachments_industry  as $attachment)
                                        <?php    $icon = Industry_attach::getIconByAttachment($attachment->extension) ?>
                                        <div class="col-xs-3 col-attachments" id="col-attachments1_{{$attachment->id_industry_attachment}}">
                                            <div style="display: block; width: 100%">
                                                <a href="{{ Url::to('/')}}/images/attach_industry/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                   download class="button_download_file" target="_blank">
                                                    <div class="file_attachments"
                                                         style="background: {{$icon}} "
                                                         id="div_file_1_{{$attachment->id_industry_attachment}}"
                                                         title="{{$attachment->name_attachment }}">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="attachments_edit_buttons" id="attachments_edit_buttons1_{{$attachment->id_industry_attachment}}">
                                                <a href="{{ Url::to('/')}}/images/attach_industry/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                   class="button_download_file" target="_blank">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </article>
            </div>
        </section>
    </div>
{{-- </div> --}}
{{ Form::model($pr_module_task,['route'=>['task.update',$pr_module_task->fk_task], 'method'=>'PATCH'])}}
<div class="row">
    <div class="col-md-12">
        <section class="ac-container description-section">
            <div id="task_description">
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2" class="description-label">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="row">
                            <div class="col-md-12 project-body">
                                <section class="ac-container">
                                    <div>
                                        <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                                        <!--<label for="ac-1">{{trans('messages.label_sidebar_details')}}</label>-->
                                        <article class="ac-any">
                                            <div class="article_content">
                                                <div class="modal_description_task">
                                                    {{$pr_module_task_project->description_task}}
                                                </div>
                                                <textarea style="width: 100%" class="summernote" id="summernote{{$pr_module_task_project->id_ms_task}}">
                                                    {{$pr_module_task_project->task_description}}
                                                 </textarea>
                                                <div class="modal_description_task user_comment_task"
                                                     id="user_comment_task{{$pr_module_task_project->id_ms_task}}">
                                                    @if(strlen(strip_tags($pr_module_task_project->task_description))>1)
                                                        <div class="modal_description_task_content">
                                                            {{$pr_module_task_project->task_description}}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="modal_editor_buttons">
                                                    <a href="javascript:void(0)"
                                                       id="button_save_task{{$pr_module_task_project->id_ms_task}}"
                                                       class="btn btn-round btn-l ls-light-blue-btn button_save_task btn-margin-right" style="display: none">
                                                        <i class="fa fa-floppy-o"></i>
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       id="button_add_task{{$pr_module_task_project->id_ms_task}}"
                                                       class="btn btn-round btn-l ls-light-blue-btn button_add_task btn-margin-right
                                      {{(strlen($pr_module_task_project->task_description)>0)?'disabled':''}}">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       id="button_edit_task{{$pr_module_task_project->id_ms_task}}"
                                                       class="btn btn-round btn-l ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task_project->task_description)>0)?'':'disabled'}}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       id="button_delete_task{{$pr_module_task_project->id_ms_task}}"
                                                       class="btn btn-round btn-l btn-danger button_delete_task btn-margin-right {{(strlen($pr_module_task_project->task_description)>0)?'':'disabled'}}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </section>
                            </div>
                        </div>
<!--                        <div class="form-group mgtp-10">
                            {{ Form::text('code_task',  $task->code_task, ['class'=>"form-control","placeholder"=> trans('messages.nav_task_code') ]) }}
                        </div>-->
<!--                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-6">
                                {{Form::checkbox('is_checked' ,1, $pr_module_task->is_checked,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                            </div>
                            <div class="col-xs-6">
                                {{Form::checkbox('is_mandatory' ,1, $pr_module_task->is_mandatory,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="lvl_priority" id="task_pair" class="task-priority-pair">
                                    <option value="">{{trans('messages.label_priority')}}</option>
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->lvl_priority}}" {{($pr_module_task->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                    $selected_pair = Task_pair::getFirstById($pr_module_task->fk_pair);
                                    if (count($selected_pair) == 1) {
                                        $selected_color = $selected_pair->color_pair;
                                    } else {
                                        $selected_color = '#fff';
                                    }
                                ?>
                                <select name="fk_pair" id="task_pair" class="task-priority-pair" style="background: {{$selected_color}}">
                                    <option value="0" style=" background: #fff;">{{trans('messages.label_pair')}} {{trans('messages.nav_task')}}</option>
                                    @foreach($pairs as $pair)
                                        <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($pr_module_task->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="tab-content tab-border select_industry">
                            @foreach($industries_types as $industry_type)
                                <?php
                                $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>

                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">

                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <hr>

                                    @foreach($industries as $industry)

                                        <div style="display: block">
                                            <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$id_ms_task}}"
                                                   {{(Industry_ms::isIndustryChecked($industry->id_industry, $id_ms_task) == true)?'checked':''}}

                                                   name="fk_industry[]" value="{{$industry->id_industry}}" style="display: inline-block"> {{$industry->name_industry}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach

                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$task->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-5" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-5">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$task->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        {{Form::hidden('id_ms_task' ,$id_ms_task)}}
        {{Form::hidden('fk_industry' ,$fk_industry)}}
        <button type="submit" class="btn ls-light-blue-btn btn-block">
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>
<br>
<br>

{{Form::close()}}
<script type="text/javascript">
    jQuery('.button_add_task').click(function (event) {
        var id = jQuery(this).attr('id');
        var id_nr = id.replace('button_add_task', "");
        jQuery('#summernote' + id_nr).next('.note-editor').show();
        jQuery(this).hide();
        jQuery('#button_save_task' + id_nr).show();
        jQuery('#user_comment_task' + id_nr).hide();
        jQuery('#button_edit_task' + id_nr).hide();
    });
    // edit task details
    jQuery('.button_edit_task').click(function (event) {
        var id = jQuery(this).attr('id');
        var id_nr = id.replace('button_edit_task', "");
        jQuery('#summernote' + id_nr).next('.note-editor').show();
        jQuery(this).hide();
        jQuery('#button_save_task' + id_nr).show();
        jQuery('#user_comment_task' + id_nr).hide();
        jQuery('#button_add_task' + id_nr).hide();
    });
    jQuery('.button_save_task').click(function (event) {
        var id = $(this).attr('id');
        var id_nr = id.replace('button_save_task', "");
        var task_description = $.trim(jQuery('#summernote' + id_nr).parent().find('.note-editable').html());
        // if textarea is null
        if (task_description.length > 0 && task_description != '<br>') {

            if (!$('#user_comment_task' + id_nr).children('div').hasClass('modal_description_task_content')) {

                $('#user_comment_task' + id_nr).append('<div class="modal_description_task_content"></div>');
            }
            $('#button_add_task' + id_nr).addClass('disabled');
            $('#button_edit_task' + id_nr).removeClass('disabled');
            $('#button_delete_task' + id_nr).removeClass('disabled');
            $('#user_comment_task' + id_nr).find('.modal_description_task_content').show();
            $('#user_comment_task' + id_nr).find('.modal_description_task_content').html(task_description);
            jQuery('#user_comment_task' + id_nr).show();

            var data = {
                'task_description': task_description,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
        }
        else {
            $('#button_add_task' + id_nr).removeClass('disabled');
            $('#button_edit_task' + id_nr).addClass('disabled');
            $('#button_delete_task' + id_nr).addClass('disabled');
            $('#button_delete_task' + id_nr).trigger('blur');
            $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
            $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
            jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
            var data = {
                'task_description': '~',
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
        }
        // save to database
        jQuery.post('{{  route("ajax.update_project_task") }}', data)
                .done(function (msg) {
                    $.amaran({
                        'theme': 'colorful',
                        'content': {
                            message: msg,
                            bgcolor: '#324e59',
                            color: '#fff'
                        }
                    });
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                });
        $(this).hide();
        jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
        $('#button_add_task' + id_nr).show();
        $('#button_edit_task' + id_nr).show();
        $('#user_comment_task' + id_nr).show();
    });
    // delete task details
    jQuery('.button_delete_task').click(function (event) {
        // confirm empty text
        var confirm_dialog = confirm("Are you sure?");
        if (confirm_dialog == true) {
            var id = $(this).attr('id');
            var id_nr = id.replace('button_delete_task', "");
            jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
            $(this).addClass('disabled');
            var data = {
                'task_description': '~',
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save to database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                                                $.amaran({
//                                                    //   'content': {message: msg}
//                                                });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

            $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
            $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
            jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
            $('#button_add_task' + id_nr).removeClass('disabled');
            $('#button_edit_task' + id_nr).addClass('disabled');
            $('#button_delete_task' + id_nr).addClass('disabled');
            $('#button_delete_task' + id_nr).trigger('blur');
            $('#button_add_task' + id_nr).show();
            $('#button_edit_task' + id_nr).show();
            $('#user_comment_task' + id_nr).show();
            $('#button_save_task' + id_nr).hide();
        }
    });
</script>
<script type="text/javascript">
    $('.formConfirm').on('click', function(e) {
          e.preventDefault();
          var title = $(this).attr('data-title');
          var msg = $(this).attr('data-message');
          var dataForm = $(this).attr('data-form');
          var url = $(this).attr('href');

        $('#formConfirm')
          .find('#frm_body').html(msg)
          .end().find('#frm_title').html(title)
          .end().modal('show');

          $('#formConfirm').find('#frm_submit').attr('href', url);
    });
</script>

<script type="text/javascript">
    $('.button_delete_file').on('click', function(e) {
          e.preventDefault();
          $('.confirm_button_delete_file').removeClass('disabled');
          var data_id = $(this).attr('id');

          //var dataForm = $(this).attr('data-form');
         var url = $(this).attr('href');
         var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
         var msg = "Delete attachment " + title + "?";

        $('#formConfirm')
          .find('#frm_body').html(msg)
          .end().modal('show');

         $('#frm_submit').addClass('confirm_button_delete_file').attr('data_id', data_id);

        $('.confirm_button_delete_file').click(function (event) {
                var id = jQuery(this).attr('data_id');
                var id_nr = id.replace('button_delete_file', '');

                var data = {
                    'id_industry_attachment': id_nr
                };
                jQuery.post('{{  route("ajax.remove_file_industry") }}', data)
                        .done(function (msg) {
                            if (parseInt(msg) > 0) {
                                var attached_files, new_val, id_ms_task;
                                id_ms_task = parseInt(msg);
                                attached_files = $('#attachment_counter' + id_ms_task).html();
                                new_val = parseInt(attached_files) - 1;
                                $('#attachment_counter' + id_ms_task).html(new_val);
                            }
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });
                jQuery('#col-attachments' + id_nr).hide();
               $('#formConfirm').modal('hide');
        });
    });
</script>

<script type="text/javascript">
    $('.attachment-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) + 50;
            $('#task_description').css('margin-bottom', task_margin_bottom+'px');
            $(this).removeClass('is_closed');
        } else {
            var heightAttachments = $('.attachment-label').parent().find('article:first').height();
            $('#task_description').css('margin-bottom','50px');
            $(this).attr('data-task-margin', heightAttachments);
            $(this).addClass('is_closed');
       }
    });

    $('.description-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) ;
            $('#task_description').css('margin-bottom', task_margin_bottom+'0px');
            $(this).removeClass('is_closed');
            var attachmentHeight = $('.attachment-section').height();
            setTimeout(function() {
                 $('.attachment-section').css('top', $('#task_description').outerHeight());
                    var attachmentHeight = $('.attachment-section').height();
                    $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
        } else {
            $('#task_description').css('margin-bottom','0px');
            $(this).addClass('is_closed');
            setTimeout(function() {
                var topHeight = $('#task_description').outerHeight() + 5;
                 $('.attachment-section').css('top', topHeight);
                var attachmentHeight = $('.attachment-section').height();
                $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
       }
    });
</script>
