<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 7/23/2015
 * Time: 9:58 AM
 */

?>
@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')

    {{--// custom scripts--}}
    @include('front.projects.project_scripts')
@stop

@section('content')
    <div class="loader_back">
        <div class="loader_gif"></div>
    </div>
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-sitemap"></i></li>
                        <li class="active"> {{$industry_type->name_industry_type}}</li>
                        <li class="active"> {{$industry->name_industry}} </li>
                        <li class="active"> {{$project->name_project}} </li>
                    </ol>
                    <!--Top breadcrumb stop -->
                    {{ $errors->first("confirm_",'<div class="text-green">:message</div>') }}
                </div>
                <div class="col-xs-12" style="margin-left: -20px">

                    <div class="row">
                        <div class="col-lg-10">

                        </div>
                        <div class="col-lg-2">
                            <a href="{{URL::to('download_project_pdf/'.$project->id_project)}}" target="_blank" class="btn ls-light-blue-btn  btn-block">Download PDF</a>
                        </div>
                    </div>

                    <div class="easy-tree ls-tree-view">
                        <ul class="ul-tree">
                            <li id="project_name"><span class="menu_title" id="submodule0">
                                       <span class="glyphicon glyphicon-minus-sign"></span>
                                    <a href="javascript:void(0); ">{{$project->name_project}}</a></span>
                                <ul>
                                    @foreach($module_structure as $module)
                                        @if(Module_structure::hasChild($module->id_module_structure) == 1)
                                            <li {{($module->is_default == 0)?'class="li_editable"':''}}>
                                                <span class="menu_title"
                                                      id="submodule{{$module->id_module_structure}}">
                                                <span class="glyphicon glyphicon-minus-sign"></span>
                                                    <a href="javascript: void(0);">  {{$module->id_name}}</a>
                                                    {{($module->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                  </span>
                                                <ul>
                                                    <?php  $module_structure_2 = Module_structure::getModuleByLvl
                                                    ($industry->id_industry, 2, $module->id_module_structure,
                                                            $project->id_project);
                                                    ?>
                                                    @foreach($module_structure_2 as $module_2)
                                                        @if(Module_structure::hasChild($module_2->id_module_structure) == 1)
                                                            <li {{($module_2->is_default == 0)?'class="li_editable"':''}}>
                                                                <span class="menu_title"
                                                                      id="submodule{{$module_2->id_module_structure}}">
                                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                                                  <a href="javascript: void(0);">    {{$module_2->id_name}}</a>
                                                                    {{($module_2->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                </span>
                                                                <ul>
                                                                    <?php  $module_structure_3 =
                                                                            Module_structure::getModuleByLvl
                                                                            ($industry->id_industry, 3,
                                                                                    $module_2->id_module_structure, $project->id_project);
                                                                    ?>
                                                                    @foreach($module_structure_3 as $module_3)
                                                                        @if(Module_structure::hasChild($module_3->id_module_structure) == 1)
                                                                            <li {{($module_3->is_default == 0)?'class="li_editable"':''}}>
                                                                                <span class="menu_title"
                                                                                      id="submodule{{$module_3->id_module_structure}}">
                                                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                                                                     <a href="javascript: void(0);"> {{$module_3->id_name}}</a>

                                                                                    {{($module_3->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                </span>
                                                                                <ul>
                                                                                    <?php  $module_structure_4 =
                                                                                            Module_structure::getModuleByLvl
                                                                                            ($industry->id_industry, 4,
                                                                                                    $module_3->id_module_structure, $project->id_project);
                                                                                    ?>
                                                                                    @foreach($module_structure_4 as $module_4)
                                                                                        @if(Module_structure::hasChild($module_4->id_module_structure) == 1)
                                                                                            <li {{($module_4->is_default == 0)?'class="li_editable"':''}}>
                                                                                                <span
                                                                                                        class="menu_title"
                                                                                                        id="submodule{{$module_4->id_module_structure}}">
                                                                                                     <span class="glyphicon glyphicon-minus-sign"></span>
                                                                                                  <a href="javascript: void(0);">  {{$module_4->id_name}}</a>

                                                                                                    {{($module_4->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                                </span>
                                                                                            </li>
                                                                                        @else
                                                                                            <li class="{{($module_4->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                                                        <span class="module-menu-title menu_title"
                                                                                                              id="submodule{{$module_4->id_module_structure}}">
                                                                         <a href="javascript: void(0);"> {{$module_4->id_name}}</a>

                                                                                                            {{($module_4->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                                        </span>

                                                                                                <div class="row 
                                                                                                margin_left_minus4"
                                                                                                        >
                                                                                                    <?php
                                                                                                    $pr_module_tasks = Ms_task::getTasksByStructure($module_4->id_module_structure, $project->id_project); ?>
                                                                                                    <div class="col-xs-2 ">


                                                                                                        <div
                                                                                                                class="circle_tasks_left"
                                                                                                                id="circle_tasks_left{{$module_4->id_module_structure}}">
                                                                                                            {{Ms_task::getCheckedTasks($module_4->id_module_structure, $project->id_project)}}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-xs-10 inside-with-border">
                                                                                                        <div class="row">
                                                                                                            <div class="col-xs-11">
                                                                                                                @if(count($pr_module_tasks)>0)
                                                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                                                        <?php $attachments =
                                                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                                                            <style type="text/css">
                                                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
                                                                                                                                    padding-right: 7px;
                                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     {
                                                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                    right: -30px;
                                                                                                                                }
                                                                                                                            </style>
                                                                                                                        @endif
                                                                                                                        <div class="panel-task-border"
                                                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                                                                <?php
                                                                                                                                $title = '';
                                                                                                                                $task_description = trim(strip_tags($pr_module_task->task_description));
                                                                                                                                $description_task = trim(strip_tags($pr_module_task->description_task));
                                                                                                                                if (strlen($description_task) > 0) {
                                                                                                                                    $title .= $description_task;
                                                                                                                                    if (strlen($task_description) > 0) {
                                                                                                                                        $title .= '<hr>';
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                if (strlen($task_description) > 0) {
                                                                                                                                    $title .= $task_description;
                                                                                                                                }
                                                                                                                                ?>

                                                                                                                                <div class="panel-task-body" title="{{ $title}}">
                                                                                                                                    {{ substr($pr_module_task->name_task,0,250)}}

                                                                                                                                </div>
                                                                                                                                <div class="panel-task-header">
                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                                                             class="colorPicker">
                                                                                                                                            <a class="color"
                                                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                                                <div class="colorInner"
                                                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                                            </a>

                                                                                                                                            <div class="track"
                                                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>


                                                                                                                                            <input type="hidden"
                                                                                                                                                   class="colorInput"
                                                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                                                        </div>
                                                                                                                                    </div>

                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <a href="{{URL::to('modal_dialog_content/'.$pr_module_task->id_ms_task.'/'.$project->id_project)}}"
                                                                                                                                           data-toggle="modal"
                                                                                                                                           data-target="#modalEditTask"
                                                                                                                                           title="">
                                                                                                                                            @if(strlen($description_task) > 0)
                                                                                                                                                <i class="fa fa-info-circle green"></i>
                                                                                                                                            @elseif(strlen($task_description) > 0)
                                                                                                                                                <i class="fa fa-info-circle blue"></i>
                                                                                                                                            @else
                                                                                                                                                <i class="fa fa-info-circle grey"></i>
                                                                                                                                            @endif
                                                                                                                                        </a>

                                                                                                                                    </div>
                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,        ['class'=>'set_priority_task','id'=>'set_priority_task'.$pr_module_task->id_ms_task])}}
                                                                                                                                    </div>
                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                                                            {{ Form::hidden('fk_module_structure', $module_4->id_module_structure) }}
                                                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                                                            <button type="submit"
                                                                                                                                                    style="color: #FF7878;"
                                                                                                                                                    class="btn-icon">
                                                                                                                                                <i class="fa fa-trash"></i>
                                                                                                                                            </button>
                                                                                                                                            {{ Form::close() }}

                                                                                                                                        @endif
                                                                                                                                    </div>
                                                                                                                                    <div class="ls-button-group float-right">
                                                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1, false,['class'=>"icheck-green",    'id'=>'id_circle'.$pr_module_task->fk_module_structure.' icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                                    </div>
                                                                                                                                </div>


                                                                                                                                <div class="clear"></div>
                                                                                                                                {{--// modal fade level 4--}}

                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    @endforeach
                                                                                                                @endif
                                                                                                            </div>
                                                                                                            <div class="col-xs-1">
                                                                                                                <div class="add_new_task">
                                                                                                                    <a href="{{URL::to('modal_dialog_add/'.$module_4->id_module_structure.'/'.$project->id_project)}}"
                                                                                                                       class="btn ls-light-blue-btn" data-toggle="modal" data-target="#modalAddTaskModule">
                                                                                                                        <i class="fa fa-plus"></i>
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </li>
                                                                        @else
                                                                            <li class="{{($module_3->is_default 
                                                                                == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                                      <span class="module-menu-title 
                                                                                      menu_title"
                                                                                            id="submodule{{$module_3->id_module_structure}}">
                                                                       <a href="javascript: void(0);">   {{$module_3->id_name}}</a>
                                                                                          {{($module_3->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                                      </span>

                                                                                <div class="row margin_left_minus3">
                                                                                    <?php      $pr_module_tasks =
                                                                                            Ms_task::getTasksByStructure($module_3->id_module_structure, $project->id_project); ?>
                                                                                    <div class="col-xs-2 ">


                                                                                        <div
                                                                                                class="circle_tasks_left"
                                                                                                id="circle_tasks_left{{$module_3->id_module_structure}}">
                                                                                            {{Ms_task::getCheckedTasks($module_3->id_module_structure, $project->id_project)}}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-10 inside-with-border">
                                                                                        <div class="row ">
                                                                                            <div class="col-xs-11">

                                                                                                @if(count($pr_module_tasks)>0)
                                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                                        <?php $attachments =
                                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                                            <style type="text/css">
                                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
                                                                                                                    padding-right: 7px;
                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     {
                                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                    right: -30px;
                                                                                                                }
                                                                                                            </style>
                                                                                                        @endif
                                                                                                        <div class="panel-task-border"
                                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                                                <?php
                                                                                                                $title = '';
                                                                                                                $task_description = trim(strip_tags($pr_module_task->task_description));
                                                                                                                $description_task = trim(strip_tags($pr_module_task->description_task));
                                                                                                                if (strlen($description_task) > 0) {
                                                                                                                    $title .= $description_task;
                                                                                                                    if (strlen($task_description) > 0) {
                                                                                                                        $title .= '<hr>';
                                                                                                                    }
                                                                                                                }
                                                                                                                if (strlen($task_description) > 0) {
                                                                                                                    $title .= $task_description;
                                                                                                                }
                                                                                                                ?>

                                                                                                                <div class="panel-task-body" title="{{ $title}}">
                                                                                                                    {{ substr($pr_module_task->name_task,0,250)}}
                                                                                                                </div>
                                                                                                                <div class="panel-task-header">
                                                                                                                    <div class="ls-button-group">
                                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                                             class="colorPicker">
                                                                                                                            <a class="color"
                                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                                <div class="colorInner"
                                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                            </a>

                                                                                                                            <div class="track"
                                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>


                                                                                                                            <input type="hidden"
                                                                                                                                   class="colorInput"
                                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="ls-button-group">
                                                                                                                        <a href="{{URL::to('modal_dialog_content/'.$pr_module_task->id_ms_task.'/'.$project->id_project)}}"
                                                                                                                           data-toggle="modal"
                                                                                                                           data-target="#modalEditTask"
                                                                                                                           title="">
                                                                                                                            @if(strlen($description_task) > 0)
                                                                                                                                <i class="fa fa-info-circle green"></i>
                                                                                                                            @elseif(strlen($task_description) > 0)
                                                                                                                                <i class="fa fa-info-circle blue"></i>
                                                                                                                            @else
                                                                                                                                <i class="fa fa-info-circle grey"></i>
                                                                                                                            @endif
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="ls-button-group">
                                                                                                                        {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,        ['class'=>'set_priority_task','id'=>'set_priority_task'.$pr_module_task->id_ms_task])}}
                                                                                                                    </div>
                                                                                                                    <div class="ls-button-group">
                                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                                            {{ Form::hidden('fk_module_structure', $module_3->id_module_structure) }}
                                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                                            <button type="submit"
                                                                                                                                    style="color: #FF7878;"
                                                                                                                                    class="btn-icon">
                                                                                                                                <i class="fa fa-trash"></i>
                                                                                                                            </button>
                                                                                                                            {{ Form::close() }}

                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                    <div class="ls-button-group float-right">
                                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                                        false,['class'=>"icheck-green",    'id'=>'id_circle'.$pr_module_task->fk_module_structure.' icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="clear"></div>
                                                                                                                {{--// modal 
                                                                                                                fade level 3--}}

                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="col-xs-1">
                                                                                                <div class="add_new_task">
                                                                                                    <a href="{{URL::to('modal_dialog_add/'.$module_3->id_module_structure.'/'.$project->id_project)}}"
                                                                                                       class="btn ls-light-blue-btn" data-toggle="modal" data-target="#modalAddTaskModule">
                                                                                                        <i class="fa fa-plus"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </li>
                                                        @else
                                                            <li class="{{($module_2->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                     <span class="module-menu-title menu_title"
                                                                           id="submodule{{$module_2->id_module_structure}}">
                                                                        <a href="javascript: void(0);">  {{$module_2->id_name}}</a>

                                                                         {{($module_2->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                                     
                                                                     </span>

                                                                <div class="row margin_left_minus2">
                                                                    <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module_2->id_module_structure, $project->id_project); ?>
                                                                    <div class="col-xs-2 ">


                                                                        <div
                                                                                class="circle_tasks_left"
                                                                                id="circle_tasks_left{{$module_2->id_module_structure}}">
                                                                            {{Ms_task::getCheckedTasks
                                                                            ($module_2->id_module_structure, 
                                                                            $project->id_project)}}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 inside-with-border">
                                                                        <div class="row">
                                                                            <div class="col-xs-11">
                                                                                @if(count($pr_module_tasks)>0)
                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                        <?php $attachments =
                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                            <style type="text/css">
                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
                                                                                                    padding-right: 7px;
                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     {
                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                    right: -30px;
                                                                                                }
                                                                                            </style>
                                                                                        @endif
                                                                                        <div class="panel-task-border"
                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                            <div class="panel-task {{
                                                                                    ($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">

                                                                                                <?php
                                                                                                $title = '';
                                                                                                $task_description = trim(strip_tags($pr_module_task->task_description));
                                                                                                $description_task = trim(strip_tags($pr_module_task->description_task));
                                                                                                if (strlen($description_task) > 0) {
                                                                                                    $title .= $description_task;
                                                                                                    if (strlen($task_description) > 0) {
                                                                                                        $title .= '<hr>';
                                                                                                    }
                                                                                                }
                                                                                                if (strlen($task_description) > 0) {
                                                                                                    $title .= $task_description;
                                                                                                }
                                                                                                ?>

                                                                                                <div class="panel-task-body" title="{{ $title}}">

                                                                                                    {{ substr($pr_module_task->name_task,0,250)}}

                                                                                                </div>
                                                                                                <div class="panel-task-header">
                                                                                                    <div class="ls-button-group">
                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                             class="colorPicker">
                                                                                                            <a class="color"
                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                <div class="colorInner"
                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                            </a>

                                                                                                            <div class="track"
                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>

                                                                                                            <input type="hidden"
                                                                                                                   class="colorInput"
                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="ls-button-group">
                                                                                                        <a href="{{URL::to('modal_dialog_content/'.$pr_module_task->id_ms_task.'/'.$project->id_project)}}"
                                                                                                           data-toggle="modal"
                                                                                                           data-target="#modalEditTask"
                                                                                                           title="">
                                                                                                            @if(strlen($description_task) > 0)
                                                                                                                <i class="fa fa-info-circle green"></i>
                                                                                                            @elseif(strlen($task_description) > 0)
                                                                                                                <i class="fa fa-info-circle blue"></i>
                                                                                                            @else
                                                                                                                <i class="fa fa-info-circle grey"></i>
                                                                                                            @endif
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="ls-button-group">
                                                                                                        {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,        ['class'=>'set_priority_task','id'=>'set_priority_task'.$pr_module_task->id_ms_task])}}
                                                                                                    </div>
                                                                                                    <div class="ls-button-group">
                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                            {{ Form::hidden('fk_module_structure', $module_2->id_module_structure) }}
                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                            <button type="submit"
                                                                                                                    style="color: #FF7878;"
                                                                                                                    class="btn-icon">
                                                                                                                <i class="fa fa-trash"></i>
                                                                                                            </button>
                                                                                                            {{ Form::close() }}

                                                                                                        @endif
                                                                                                    </div>
                                                                                                    <div class="ls-button-group float-right">
                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                        false,['class'=>"icheck-green",    'id'=>'id_circle'.$pr_module_task->fk_module_structure.' icheck-green'.$pr_module_task->id_ms_task,
                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                    </div>
                                                                                                </div>


                                                                                                <div class="clear"></div>


                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                @endif
                                                                            </div>

                                                                            <div class="col-xs-1">
                                                                                <div class="add_new_task">
                                                                                    <a href="{{URL::to('modal_dialog_add/'.$module_2->id_module_structure.'/'.$project->id_project)}}"
                                                                                       class="btn ls-light-blue-btn" data-toggle="modal" data-target="#modalAddTaskModule">
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                            <li class="{{($module->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                  <span class="module-menu-title menu_title"
                                                        id="submodule{{$module->id_module_structure}}">
                                                                       <a href="javascript: void(0);">   {{$module->id_name}}</a>
                                                      {{($module->is_default == 0)?' <i class="fa fa-pencil " style="font-size: 13px"></i>':''}}
                                                  </span>

                                                <div class="row margin_left_minus1">
                                                    <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $project->id_project); ?>
                                                    <div class="col-xs-2 ">
                                                        <div
                                                                class="circle_tasks_left"
                                                                id="circle_tasks_left{{$module->id_module_structure}}">
                                                            {{Ms_task::getCheckedTasks($module->id_module_structure, $project->id_project)}}
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-10 inside-with-border">
                                                        <div class="row">
                                                            <div class="col-xs-11">
                                                                @if(count($pr_module_tasks)>0)
                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                        <?php $attachments =
                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                        @if($pr_module_task->task_color  != null &&
                                                                            $pr_module_task->task_color != 'transparent')
                                                                            <style type="text/css">
                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
                                                                                    padding-right: 7px;
                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     {
                                                                                    background: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                }

                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                    right: -30px;
                                                                                }
                                                                            </style>
                                                                        @endif
                                                                        <div class="panel-task-border"
                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                <?php
                                                                                $title = '';
                                                                                $task_description = trim(strip_tags($pr_module_task->task_description));
                                                                                $description_task = trim(strip_tags($pr_module_task->description_task));
                                                                                if (strlen($description_task) > 0) {
                                                                                    $title .= $description_task;
                                                                                    if (strlen($task_description) > 0) {
                                                                                        $title .= '<hr>';
                                                                                    }
                                                                                }
                                                                                if (strlen($task_description) > 0) {
                                                                                    $title .= $task_description;
                                                                                }
                                                                                ?>

                                                                                <div class="panel-task-body" title="{{ $title}}">
                                                                                    {{ substr($pr_module_task->name_task,0,250)}}
                                                                                </div>
                                                                                <div class="panel-task-header">
                                                                                    <div class="ls-button-group">
                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                             class="colorPicker">
                                                                                            <a class="color"
                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                <div class="colorInner"
                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                            </a>

                                                                                            <div class="track"
                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>

                                                                                            <input type="hidden"
                                                                                                   class="colorInput"
                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="ls-button-group">
                                                                                        <a href="{{URL::to('modal_dialog_content/'.$pr_module_task->id_ms_task.'/'.$project->id_project)}}"
                                                                                           data-toggle="modal"
                                                                                           data-target="#modalEditTask"
                                                                                           title="">
                                                                                            @if(strlen($description_task) > 0)
                                                                                                <i class="fa fa-info-circle green"></i>
                                                                                            @elseif(strlen($task_description) > 0)
                                                                                                <i class="fa fa-info-circle blue"></i>
                                                                                            @else
                                                                                                <i class="fa fa-info-circle grey"></i>
                                                                                            @endif
                                                                                        </a>

                                                                                    </div>
                                                                                    <div class="ls-button-group">
                                                                                        {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,        ['class'=>'set_priority_task','id'=>'set_priority_task'.$pr_module_task->id_ms_task])}}
                                                                                    </div>
                                                                                    <div class="ls-button-group">
                                                                                        <i class="fa fa-paperclip"></i>
                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                        @if($pr_module_task->is_default == 0)

                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                            {{ Form::hidden('fk_module_structure', $module->id_module_structure) }}
                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                            <button type="submit"
                                                                                                    style="color: #FF7878;"
                                                                                                    class="btn-icon">
                                                                                                <i class="fa fa-trash"></i>
                                                                                            </button>
                                                                                            {{ Form::close() }}

                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="ls-button-group float-right">
                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,  false,['class'=>"icheck-green",   'id'=>'id_circle'.$pr_module_task->fk_module_structure.' icheck-green'.$pr_module_task->id_ms_task,
                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clear"></div>
                                                                                {{--// modal fade level 1--}}

                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>

                                                            <div class="col-xs-1">
                                                                <div class="add_new_task">
                                                                    <a href="{{URL::to('modal_dialog_add/'.$module->id_module_structure.'/'.$project->id_project)}}"
                                                                       class="btn ls-light-blue-btn" data-toggle="modal" data-target="#modalAddTaskModule">
                                                                        <i class="fa fa-plus"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- Modal Add mew task and structure -->
                    <div class="modal fade" id="addNewTask" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content" id="borderInnerTask">
                                <button type="button" class="close close_modal"
                                        id="close_modal_new" data-dismiss="modal" aria-hidden="true"> x
                                </button>
                                {{ Form::open(['action'=>'ProjectsController@add_new_task','id'=>''])}}
                                <div class="modal-header task-label-white">
                                    <div class="ls-button-group float-left">
                                        <div class="colorInnerTask" id="colorInnerTask"></div>
                                    </div>
                                    <div class="ls-button-group float-left">
                                        <title> Add new task</title>
                                    </div>
                                    <div class="ls-button-group float-right" style="padding: 3px;">
                                        {{Form::checkbox('checkbox', 1,  true,['class'=>"icheck-green",    'id'=>'icheck-green', 'disabled'])}}
                                    </div>
                                    <div class="ls-button-group float-right">
                                        {{Form::select('set_priority',  $set_priority,  '', ['class'=>'set_priority_task','id'=>'set_priority_task'])}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="modal-body new-task">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Task Name" name="name_task" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea style="width: 100%" name="task_description" class="summernote_new" id="summernote_new"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="modal-switch-area">
                                            <div class="modal-switch-box">
                                                <input name="is_not_offer" class="switchCheckBox is_not_offer" value="1" id="is_not_offer" type="checkbox" data-size="mini">
                                                Von der Ausschreibung ausgeschlossen
                                            </div>
                                            <div class="modal-switch-box">
                                                <input name="is_control" value="1" class="switchCheckBox is_control" id="is_control" type="checkbox" data-size="mini">
                                                Kontrolfrage
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn ls-light-blue-btn">
                                        Save and Close
                                    </button>
                                    {{Form::hidden('fk_project', $project->id_project)}}
                                    {{Form::hidden('structure','',['id'=>'easyTreeStructure'])}}

                                    <button type="button" class="btn ls-red-btn" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                    <!-- /.modal -->
                    <!-- Modal Edit task -->
                    <div class="modal fade" id="modalEditTask" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- Modal Add new task for existing structure -->
                    <div class="modal fade" id="modalAddTaskModule" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
            </div>
        </div>
    </div>


@stop


 
 
 