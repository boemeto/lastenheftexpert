<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 05-Apr-16
 * Time: 11:59
 */
class LanguageTrans extends Eloquent
{
    public static $rules = [
        'key' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_language';
    protected $table = 'language_trans';
    protected $guarded = ['id_language'];
    protected $fillable = ['key', 'name_de', 'name_en', 'fk_type'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = self::where('id_language', '=', $id)->first();
        return $list;
    }

    public static function getFirstByKey($id)
    {
        $list = self::where('key', '=', $id)->first();
        return $list;
    }

    public static function getList($lang = 'de', $fk_type = 1)
    {
        $lists = self::where('fk_type', '=', $fk_type)
            ->orderBy('key', 'asc')->get();

        $array = array();
        foreach ($lists as $list) {
            if ($lang == 'de') {
                $array[$list->key] = $list->name_de;
            } else {
                $array[$list->key] = $list->name_en;
            }
        }
        return $array;

    }

    public static function getListDetails($id = 1)
    {
        $list = self::where('fk_type', '=', $id)->orderBy('key', 'asc')->get();
        return $list;
    }

}

?>

