<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:37
 */
class Industry_soft extends Eloquent
{


    public static $rules = [
        'fk_industry' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_industry_soft';
    protected $table = 'pr_industry_soft';
    protected $guarded = ['id_industry_soft'];
    protected $fillable = ['fk_industry', 'fk_software'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Industry_soft::whereId_industry_soft($id)->first();
        return $list;
    }

}