<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:29
 */
class Adm_status extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_status' => 'required',
    ];
    public $primaryKey = 'id_status';
    protected $table = 'adm_statuses';
    protected $guarded = ['id_status'];
    protected $fillable = ['name_status', 'position'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_status::where('id_status', '=', $id)->first();
        return $list;
    }

    public static function getAllStatus()
    {
        $array = array();
        $lists = Adm_status::select('*')->get();
        $array[0] = 'Status';
        foreach ($lists as $list) {
            $array[$list->id_status] = $list->name_status;
        }
        return $array;
    }


}