<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 19.06.2015
 * Time: 10:46
 */
class User_device extends Eloquent
{


    public static $rules = [
        'fk_user' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_user_device';
    protected $table = 'user_devices';
    protected $guarded = ['id_user_device'];
    protected $fillable = ['fk_user', 'fk_device'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = User_device::whereId_user_device($id)->first();
        return $list;
    }

}