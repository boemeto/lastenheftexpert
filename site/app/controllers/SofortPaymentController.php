<?php

class SofortPaymentController extends BaseController {

    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    private $_apiContext;

    /**
     * Set the ClientId and the ClientSecret.
     * @param
     *string $_ClientId
     *string $_ClientSecret
     */
    private $_ClientId = 'AQ5ouSQaDsGiEdRu_jKm9rmGYxyg3F6UibX4deOVoFtg4u0-AkXyn_S3BC3vCf7EDpfPTLNLMH13vXhd';
    private $_ClientSecret='EB6k3biCzF85fazZSqSrXJmnNFsC4IH4gcfAD1ze6SDCesCVKEW1S1ol2QBjjJu2JRC4qBZ8eyjDZpWw';

    /*
     *   These construct set the SDK configuration dynamiclly,
     *   If you want to pick your configuration from the sdk_config.ini file
     *   make sure to update you configuration there then grape the credentials using this code :
     *   $this->_cred= Paypalpayment::OAuthTokenCredential();
    */
    public function __construct()
    {

        // ### Api Context
        // Pass in a `ApiContext` object to authenticate
        // the call. You can also send a unique request id
        // (that ensures idempotency). The SDK generates
        // a request id if you do not pass one explicitly.

        // $this->_apiContext = Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);

    }

    /*
    * Display form to process payment using credit card
    */
    public function create()
    {
        return View::make('payment.order');
    }

    public function index()
    {

    }

    public function show($payment_id)
    {
       $payment = Paypalpayment::getById($payment_id,$this->_apiContext);
       dd($payment);
    }

    /*
    * Process payment using credit sofort
    */
    public function store()
    {
        dd('create new project with sofort');
    }

    public function extendProjectWithPaypal()
    {
        dd('extend project');
    }
}
