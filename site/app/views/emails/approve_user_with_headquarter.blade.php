<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>


<div>
    {{$user['first_name']}} {{$user['last_name']}} has just created an account and {{$headquarter['name_headquarter']}} headquarter for {{$company['name_company']}} company.
    <br />
     <a href="{{$user->approve_user_link}}" target="_blank">  Accept </a>
     <a href="{{$user->decline_user_link}}" target="_blank">  Decline </a>
     <a href="{{$user->employees_list}}" target="_blank">  Go to employees </a>
</div>
</body>
</html>
