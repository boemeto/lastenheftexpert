<?php
use Illuminate\Support\MessageBag;

class PackagesRowController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $list = new Package_row();
        $list->fill(Input::all());
        $list->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();


        return View::make('admin.packages.create_row', compact('id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $list = Package_row::getFirstById($id);


        return View::make('admin.packages.edit_row', compact('list'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $list = Package_row::getFirstById($id);
        $list->fill(Input::all());
        $list->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $list = Package_row::getFirstById($id);

        $list->delete();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }


}
