{{ Form::open(['action'=>'CompanyAdmController@store','id'=>'addCompanyAdmForm','enctype'=>"multipart/form-data"]) }}
<div class="col-md-12">
    <div class="title_line">  {{trans('messages.act_add')}}  {{trans('messages.nav_company')}}   </div>
</div>


<div class="col-md-12">
    <div class="form-group">
        {{ Form::label('Name Company') }}
        {{ Form::text('name_company','',[ 'placeholder'=>"Company", 'class'=>"form-control", 'id'=>'name_company', 'required',
          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
          "oninput"=>"this.setCustomValidity('')"
          ]) }}
        {{ $errors->first('name_company','<div class="text-red">:message</div>') }}
    </div>

    <div class="form-group">
        {{ Form::label('Identification Code') }}
        {{ Form::text('cui_company','',[ 'placeholder'=>"Identification Code", 'id'=>'cui_company','class'=>"form-control"]) }}
        {{ $errors->first('cui_company','<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group">
        {{ Form::label('Intracomunitar Number') }}
        {{ Form::text('intracomunitar_nr','',[ 'placeholder'=>"Intracomunitar Code", 'id'=>'intracomunitar_nr','class'=>"form-control"]) }}
        {{ $errors->first('intracomunitar_nr','<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group">
        {{ Form::label('Email Company') }}
        {{ Form::email('email_company','',[ 'placeholder'=>"Email Company",'id'=>'email_company', 'class'=>"form-control"]) }}
        {{ $errors->first('email_company','<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group">
        {{ Form::label('Website Company') }}
        {{ Form::text('website_company','',[ 'placeholder'=>"Website Company",'id'=>'website_company', 'class'=>"form-control"]) }}
        {{ $errors->first('website_company','<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group">
        <input id="file-user" type="file" name="logo" multiple="false">
    </div>
    <br>
</div>
<br>
<div class="col-md-6">
    <button type="submit" class='btn ls-light-blue-btn btn-block'>
        {{trans('messages.act_save')}}
    </button>
</div>
<div class="col-md-6">
    <button type="button" class='btn btn-danger btn-block' onclick="hide_modal_box()">
        {{trans('messages.act_cancel')}}
    </button>
</div>


{{ Form::close() }}
