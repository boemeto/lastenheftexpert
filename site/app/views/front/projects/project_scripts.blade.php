<!-- MiniColors Library Script Start -->
<script src='{{ URL::asset('assets/js/tinycolorpicker.js') }}'></script>
<!-- MiniColors Library Script Start -->
<script src="{{ URL::asset('assets/js/projects/dropzone.js')}}"></script>
<!-- Module menu tree style -->
<script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
{{--<script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js')}}"></script>--}}
{{-- Summernote textarea--}}
<script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>

{{--// ColorPicker & Description --}}
<script type="text/javascript">
    // enable colorpicker
    window.onload = function () {
        var x = document.getElementsByClassName("colorPicker");
        var i;
        for (i = 0; i < x.length; i++) {
            tinycolorpicker(x[i]);
        }
    };
    jQuery(document).ready(function () {

        // colorpicker - change task color
        jQuery(".track").click(function () {
            var palette = [
                "#FE2712", "#A7194B", "#8601AF",
                "#3D01A4", "#0247FE", "#0391CE",
                "#66B032", "#D0EA2B", "#FEFE33",
                "#FABC02", "#FB9902"];
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('track', "");
            var $input = jQuery('#colorInput' + id_nr);
            var hex = $input.val();
            if (jQuery.inArray(hex, palette) == -1) {
                jQuery('#panel-task-border' + id_nr).css("background", 'transparent');
                jQuery('#panel-task-border' + id_nr).css("padding-right", '0');
                jQuery('#colorInnerTask' + id_nr).css("background", 'url("../../assets/images/minicolor/culori-rotund.png")');
                jQuery('#colorInnerTask' + id_nr).css("background-size", '15px 15px');
                jQuery('#borderInnerTask' + id_nr).css("border-right", '0px solid transparent');
                jQuery('#close_modal' + id_nr).css("right", '-24px');
                var hex_color = 'transparent';
            } else {
                jQuery('#panel-task-border' + id_nr).css("background-color", hex);
                jQuery('#panel-task-border' + id_nr).css("padding-right", '7px');
                jQuery('#colorInnerTask' + id_nr).css("background", hex);
                jQuery('#borderInnerTask' + id_nr).css("border-right", '7px solid ' + hex);
                jQuery('#close_modal' + id_nr).css("right", '-30px');
                var hex_color = hex;
            }
            var data = {
                'hex': hex_color,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                        $.amaran({
//                            content: {
//                                //      message: msg,
//                            }
//                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });

        // description on hover
        $('.panel-task-body').hover(function () {
            var tooltip = '<div class="tooltip"></div>';
            // Hover over code
            var title = $.trim($(this).attr('title'));

            if (title.length > 0) {
                $(this).data('tipText', title).removeAttr('title');

//            $(this).parent().append(tooltip);
                $('body').append(tooltip);

                $('.tooltip').html(title);
                $('.tooltip').fadeIn('slow');
            } else {
                $('body').append(tooltip);
            }

        }, function () {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function (e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip').css({top: mousey, left: mousex})
        });
    });


    // drag and drop order tasks/ module
    $(function () {

        $('.task_draggable').sortable({
            group: '.task_draggable',
            itemSelector: '.task_group_item',
            opacity: 0.7,
            exclude: 'style',
            tolerance: 'pointer',
            update: function (event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
//                    alert(list_sortable)
                // change order in the database using Ajax
                var data = {
                    'list_order': list_sortable,
                    'fk_project': '<?php print $project->id_project  ?>'
                };

                $.post('{{  route("ajax.set_order_front_task") }}', data)
                        .done(function (msg) {
                            $.amaran({
                                'theme': 'colorful',
                                'content': {
                                    message: msg,
                                    bgcolor: '#324e59',
                                    color: '#fff'
                                }
                            });
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });
            }
        }); // fin sortable
        // do this for every level
        for (var level = 0; level <= 5; level++) {
            $('.sortable_modules_' + level).sortable({
                axis: 'y',
                opacity: 0.7,
                handle: 'span',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();
                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable,
                        'fk_project': '<?php print $project->id_project  ?>'
                    };

                    $.post('{{  route("ajax.set_order_projects") }}', data)
                            .done(function (msg) {
                                $.amaran({
                                    'theme': 'colorful',
                                    'content': {
                                        message: msg,
                                        bgcolor: '#324e59',
                                        color: '#fff'
                                    }
                                });
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                //  alert(xhr.responseText);
                            });
                }
            }); // fin sortable
        }

    });

</script>

{{--//icheck change , set priority  & open/ close tree --}}
<script type="text/javascript">

    jQuery(document).ready(function () {
        // fade loader when page is loaded
        $('.loader_back').fadeOut(2000);
        // hide tasks div
        jQuery('.close_tasks').click(function () {
            var li = $(this).parent().parent();
            if ($(this).hasClass('glyphicon-minus-sign')) {
                li.find('.border_row').hide("fast");
                $(this).addClass('glyphicon-plus-sign')
                        .removeClass('glyphicon-minus-sign');
            } else {
                $(this).addClass('glyphicon-minus-sign')
                        .removeClass('glyphicon-plus-sign');
                li.find('.border_row').show("fast");
            }
        });
        // show all functions closed
        function closeEasyTree() {
            var parent = $(".easy-tree").find('li').not('#project_name');
            var children = parent.find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                parent.children('span').find(
                        ' > span.glyphicon').addClass(
                        'glyphicon-plus-sign').removeClass(
                        'glyphicon-minus-sign');
                $('.border_row').hide();
            }

        }


//        closeEasyTree();

        // updated
        // front tasks checkbox
        jQuery('input.icheck-project').on('ifChanged', function (event) {
            var id = jQuery(this).attr('id');
            var myArray = id.split(' ');
            var id_nr = myArray[1].replace('icheck-green', "");
            var id_circle = myArray[0].replace('id_circle', "");

            // modify the tasks circle value
            var value_circle = jQuery('#circle_tasks_left' + id_circle).html();
            if ($(this).prop('checked')) {
                var is_checked = 1;
                value_circle = parseInt(value_circle) + 1;
//                jQuery("[id='modal_circle" + id_circle + " icheck-modal" + id_nr + "']").iCheck('uncheck');

            } else {
                var is_checked = 0;
                value_circle = parseInt(value_circle) - 1;
//                jQuery("[id='modal_circle" + id_circle + " icheck-modal" + id_nr + "']").iCheck('uncheck');

            }
            jQuery('#circle_tasks_left' + id_circle).html(value_circle);

            // ajax- stores check value into database
            var data = {
                'is_checked': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
            jQuery.post('<?php print route("ajax.update_project_task") ?>', data)
                    .done(function (msg) {
//                        $.amaran({
//                            //   'content': {message: msg}
//                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
            event.stopPropagation();
        });

        jQuery('input.iradio-project').on('ifClicked', function (event) {
            if ($(this).prop('checked')) {
                $(this).iCheck('uncheck');
            }
        });


        jQuery('input.iradio-project').on('ifChanged', function (event) {


            var id = jQuery(this).attr('id');
            var myArray = id.split(' ');
            var id_nr = myArray[1].replace('iradio-green', "");
            var id_circle = myArray[0].replace('id_circle', "");

            // modify the tasks circle value
            var value_circle = jQuery('#circle_tasks_left' + id_circle).html();


            if ($(this).prop('checked')) {
                var is_checked = 1;
                value_circle = parseInt(value_circle) + 1;
            } else {
                var is_checked = 0;
                value_circle = parseInt(value_circle) - 1;
            }
            jQuery('#circle_tasks_left' + id_circle).html(value_circle);


            // ajax- stores check value into database
            var data = {
                'is_checked': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
            jQuery.post('<?php print route("ajax.update_project_task") ?>', data)
                    .done(function (msg) {
//                        $.amaran({
//                            //     'content': {message: msg}
//                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
            event.stopPropagation();
        });


        // set priority
        jQuery('.set_priority_task').change(function (event) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('set_priority_task', "");
            var priority = jQuery(this).val();
            var data = {
                'task_priority': priority,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // console.log(data);
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
                      //  $.amaran({
                      //        'content': {message: msg}
                      //  });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

        });
    });
</script>


{{--ajax - right iframe functions--}}
<script type="text/javascript">
    // call ajax
    function getXMLHTTP() {
        var xmlhttp = false;
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e1) {
                    xmlhttp = false;
                }
            }
        }
        return xmlhttp;
    }

    // edit an existing module/ submodule
    function modal_dialog_edit_module(id) {

        if (id == 0) {
            document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
        }
        else {
            var strURL = "{{URL::to('/')}}/modal_dialog_edit_module_project/" + id;
            var req = getXMLHTTP();
            if (req) {
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            jQuery('#task_page_details').html(req.responseText);
                            input_tags_call();
                            jQuery(".switchCheckBox").bootstrapSwitch();
                            $('.summernote').summernote({});
                            $('input.icheck-green').iCheck({
                                checkboxClass: 'icheckbox_minimal-green',
                                radioClass: 'iradio_minimal-green',
                                increaseArea: '10%' // optional
                            });
                            show_modal_box();
                        } else {
                            jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }

    var id_project = '<?php print $project->id_project ?>';

    // alert(id_ms_task);
    // edit an existing task
    function modal_dialog_edit_task(id_ms_task, id_project) {
        if (id_ms_task == 0) {
            document.getElementById('task_page_details').innerHTML = '  ';
        }
        else {
            var strURL = "{{URL::to('/')}}/modal_dialog_edit_task_project/" + id_ms_task + "/" + id_project;
            var req = getXMLHTTP();
            if (req) {
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            jQuery('#task_page_details').html(req.responseText);
                            // cal edit buttons on an attachment file
                            $('.col-attachments').hover(function () {
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('col-attachments', "");
                                $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                            });
                            // description on hover
                            $('.file_attachments').hover(function () {
                                var tooltip = '<div class="tooltip"></div>';
                                // Hover over code
                                var title = $.trim($(this).attr('title'));

                                if (title.length > 0) {
                                    $(this).data('tipText', title).removeAttr('title');
                                    $('body').append(tooltip);
                                    $('.tooltip').html(title);
                                    $('.tooltip').fadeIn('slow');
                                } else {
                                    $('body').append(tooltip);
                                }

                            }, function () {
                                // Hover out code
                                $(this).attr('title', $(this).data('tipText'));
                                $('.tooltip').remove();
                            }).mousemove(function (e) {
                                var mousex = e.pageX + 20; //Get X coordinates
                                var mousey = e.pageY + 10; //Get Y coordinates
                                $('.tooltip').css({top: mousey, left: mousex})
                            });
                            jQuery(".switchCheckBox").bootstrapSwitch();
                            $('.summernote').summernote({});
                            $('input.icheck-modal').iCheck({
                                checkboxClass: 'icheckbox_minimal-green',
                                radioClass: 'iradio_minimal-green',
                                increaseArea: '10%' // optional
                            });
                            // save modal checked on click
                            jQuery('input.icheck-modal').on('ifChanged', function (event) {
                                var id = jQuery(this).attr('id');

                                var myArray = id.split(' ');
                                var id_nr = myArray[1].replace('icheck-modal', "");
                                var id_circle = myArray[0].replace('modal_circle', "");
                                var value_circle = jQuery('#circle_tasks_left' + id_circle).html();

                                if (jQuery(this).prop('checked')) {
                                    var is_checked = 1;
                                    value_circle = parseInt(value_circle) + 1;
                                    jQuery("[id='id_circle" + id_circle + " icheck-green" + id_nr + "']").iCheck('check');
                                } else {
                                    var is_checked = 0;
                                    value_circle = parseInt(value_circle) - 1;
                                    jQuery("[id='id_circle" + id_circle + " icheck-green" + id_nr + "']").iCheck('uncheck');
                                }
                                jQuery('#circle_tasks_left' + id_circle).html(value_circle);


                                var data = {
                                    'is_checked': is_checked,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'
                                };
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
//                                            $.amaran({
//                                                //     'content': {message: msg}
//                                            });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                                event.stopPropagation();
                            });
                            // set priority task on change
                            jQuery('.set_priority').change(function (event) {
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('set_priority', "");
                                var priority = jQuery(this).val();
                                var data = {
                                    'task_priority': priority,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'

                                };
                                $("#set_priority_task" + id_nr).val(priority);
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
//                                            $.amaran({
//                                                //        'content': {message: msg}
//                                            });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });

                            });
                            // call dropzone - drag and drop files
                            $(".dropzone").dropzone({uploadMultiple: false});
                            // delete attachment

                            // add task details
                            jQuery('.button_add_task').click(function (event) {
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('button_add_task', "");
                                jQuery('#summernote' + id_nr).next('.note-editor').show();
                                jQuery(this).hide();
                                jQuery('#button_save_task' + id_nr).show();
                                jQuery('#user_comment_task' + id_nr).hide();
                                jQuery('#button_edit_task' + id_nr).hide();
                            });
                            // edit task details
                            jQuery('.button_edit_task').click(function (event) {
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('button_edit_task', "");
                                jQuery('#summernote' + id_nr).next('.note-editor').show();
                                jQuery(this).hide();
                                jQuery('#button_save_task' + id_nr).show();
                                jQuery('#user_comment_task' + id_nr).hide();
                                jQuery('#button_add_task' + id_nr).hide();
                            });
                            // delete task details
//                            jQuery('.confirm_button_delete_task').click(function (event) {
//                                // confirm empty text
//                                alert('xxxxxx');
//                                var confirm_dialog = confirm("Are you sure?");
//                                if (confirm_dialog == true) {
//                                    var id = $(this).attr('id');
//                                    var id_nr = id.replace('button_delete_task', "");
//                                    jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
//                                    $(this).addClass('disabled');
//                                    var data = {
//                                        'task_description': '~',
//                                        'id_ms_task': id_nr,
//                                        'id_project': '<?php print $project->id_project ?>'
//                                    };
//                                    // save to database
//                                    jQuery.post('{{  route("ajax.update_project_task") }}', data)
//                                            .done(function (msg) {
////                                                $.amaran({
////                                                    //   'content': {message: msg}
////                                                });
//                                            })
//                                            .fail(function (xhr, textStatus, errorThrown) {
//                                                alert(xhr.responseText);
//                                            });
//
//                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
//                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
//                                    jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
//                                    $('#button_add_task' + id_nr).removeClass('disabled');
//                                    $('#button_edit_task' + id_nr).addClass('disabled');
//                                    $('#button_delete_task' + id_nr).addClass('disabled');
//                                    $('#button_delete_task' + id_nr).trigger('blur');
//                                    $('#button_add_task' + id_nr).show();
//                                    $('#button_edit_task' + id_nr).show();
//                                    $('#user_comment_task' + id_nr).show();
//                                    $('#button_save_task' + id_nr).hide();
//                                }
//                            });
                            // save task details
                            jQuery('.button_save_task').click(function (event) {
                                var id = $(this).attr('id');
                                var id_nr = id.replace('button_save_task', "");
                                var task_description = $.trim(jQuery('#summernote' + id_nr).parent().find('.note-editable').html());
                                // if textarea is null
                                if (task_description.length > 0 && task_description != '<br>') {
                                    if (!$('#user_comment_task' + id_nr).children('div').hasClass('modal_description_task_content')) {
                                        $('#user_comment_task' + id_nr).append('<div class="modal_description_task_content"></div>');
                                    }
                                    $('#button_add_task' + id_nr).addClass('disabled');
                                    $('#button_edit_task' + id_nr).removeClass('disabled');
                                    $('#button_delete_task' + id_nr).removeClass('disabled');
                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').show();
                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').html(task_description);
                                    jQuery('#user_comment_task' + id_nr).show();
                                    var data = {
                                        'task_description': task_description,
                                        'id_ms_task': id_nr,
                                        'id_project': '<?php print $project->id_project ?>'
                                    };
                                }
                                else {
                                    $('#button_add_task' + id_nr).removeClass('disabled');
                                    $('#button_edit_task' + id_nr).addClass('disabled');
                                    $('#button_delete_task' + id_nr).addClass('disabled');
                                    $('#button_delete_task' + id_nr).trigger('blur');
                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
                                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
                                    jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                                    var data = {
                                        'task_description': '~',
                                        'id_ms_task': id_nr,
                                        'id_project': '<?php print $project->id_project ?>'
                                    };
                                }
                                // save to database
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
                                            $.amaran({
                                                'theme': 'colorful',
                                                'content': {
                                                    message: msg,
                                                    bgcolor: '#324e59',
                                                    color: '#fff'
                                                }
                                            });
                                            $('#panel-task-body'+data.id_ms_task).attr('title', data.task_description);
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                                $(this).hide();
                                jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                                $('#button_add_task' + id_nr).show();
                                $('#button_edit_task' + id_nr).show();
                                $('#user_comment_task' + id_nr).show();
                            });

                            // is_not_offer select box on check
                            $(".is_not_offer").on('switchChange.bootstrapSwitch', function (event, state) {

                                if (state == true) {
                                    var is_checked = 1;
                                }
                                else {
                                    is_checked = 0;
                                }
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('is_not_offer', "");


                                var data = {
                                    'is_not_offer': is_checked,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'

                                };
                                // save tot database
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
//                                            $.amaran({
//                                                //            'content': {message: msg}
//                                            });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                            });
                            // is_control question select box on check
                            $(".is_control").on('switchChange.bootstrapSwitch', function (event, state) {

                                if (state == true) {
                                    var is_checked = 1;
                                }
                                else {
                                    is_checked = 0;
                                }
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('is_control', "");


                                var data = {
                                    'is_control': is_checked,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'

                                };
                                // save tot database
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
//                                            $.amaran({
//                                                //                    'content': {message: msg}
//                                            });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                            });
                            // open frame
                            show_modal_box();

                        } else {
                            jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            show_modal_box();
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }

    // add new task in existing module
    function modal_dialog_add_task(id_module_structure, fk_software, id_project) {
        if (id_module_structure == 0) {
            document.getElementById('task_page_details').innerHTML = '  ';
        }
        else {
            var strURL = "{{URL::to('/')}}/modal_dialog_add_task_project/" + id_module_structure + "/" + fk_software + "/" + id_project;
            var req = getXMLHTTP();
            if (req) {
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            jQuery('#task_page_details').html(req.responseText);
                            jQuery(".switchCheckBox").bootstrapSwitch();
                            $('.summernote').summernote({});
                            $('input.icheck-modal').iCheck({
                                checkboxClass: 'icheckbox_minimal-green',
                                radioClass: 'iradio_minimal-green',
                                increaseArea: '10%' // optional
                            });

                            $(".dropzone").dropzone({uploadMultiple: false});

                            // is_not_offer select box on check
                            $(".is_not_offer").on('switchChange.bootstrapSwitch', function (event, state) {

                                if (state == true) {
                                    var is_checked = 1;
                                }
                                else {
                                    is_checked = 0;
                                }
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('is_not_offer', "");

                                var data = {
                                    'is_not_offer': is_checked,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'

                                };
                                // save tot database
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
                                            // $.amaran({
                                            //     'theme': 'colorful',
                                            //     'content': {
                                            //         message: msg,
                                            //         bgcolor: '#324e59',
                                            //         color: '#fff'
                                            //     }
                                            // });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                            });
                            // is_control question select box on check
                            $(".is_control").on('switchChange.bootstrapSwitch', function (event, state) {

                                if (state == true) {
                                    var is_checked = 1;
                                }
                                else {
                                    is_checked = 0;
                                }
                                var id = jQuery(this).attr('id');
                                var id_nr = id.replace('is_control', "");


                                var data = {
                                    'is_control': is_checked,
                                    'id_ms_task': id_nr,
                                    'id_project': '<?php print $project->id_project ?>'

                                };
                                // save tot database
                                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                                        .done(function (msg) {
//                                            $.amaran({
//                                                'content': {message: msg}
//                                            });
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(xhr.responseText);
                                        });
                            });
                            // open frame
                            show_modal_box();
                        } else {
                            jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            show_modal_box();
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }

    function modal_dialog_add_task_new(id_module_structure) {
        if (id_module_structure == 0) {
            document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
        }
        else {
            var strURL = "{{URL::to('/')}}/modal_dialog_add_task_new_project/" + id_module_structure + "/<?php print $industry->id_industry; ?>" + "/<?php print $project->id_project; ?>";
            var req = getXMLHTTP();
            if (req) {
                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            jQuery('#task_page_details').html(req.responseText);
                            jQuery(".switchCheckBox").bootstrapSwitch();
                            $('.summernote').summernote({});
                            $('input.icheck-modal').iCheck({
                                checkboxClass: 'icheckbox_minimal-green',
                                radioClass: 'iradio_minimal-green',
                                increaseArea: '10%' // optional
                            });
                            // call dropzone - drag and drop files
                            $(".dropzone").dropzone({uploadMultiple: false});
                            show_modal_box();
                        } else {
                            jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }

    // Synonyme/Tags, Address-Code fields - call meta tags
    function input_tags_call() {
        $('.input-tags').selectize({
            plugins: ['remove_button'],
            delimiter: ' ',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            },
            render: {
                item: function (data, escape) {
                    return '<div>"' + escape(data.text) + '"</div>';
                }
            },
            onDelete: function (values) {
                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
            }
        });
    }
    // effect on the right iframe box to appear
    function show_modal_box() {
//        $('.hide_controller ').attr('onclick', 'hide_modal_box()');
        $('#task_page_details_parent').show("slide", {direction: "right"}, 500);
        $('.iframe-overlay').fadeIn();
    }

    // effect on the right iframe box to hide
    function hide_modal_box_confirm() {
        var a = confirm('Are you sure you don\'t want to save?');
        if (a == true) {
//            $('a .hide_controller').attr('onclick', 'show_modal_box()');
            $('#task_page_details_parent').hide("slide", {direction: "right"}, 500);
            $('.iframe-overlay').fadeOut();
        } else {
            return false;
        }
    }
    function hide_modal_box() {
        $('.hide_controller a').attr('onclick', 'show_modal_box()');
        $('#task_page_details_parent').hide("slide", {direction: "right"}, 500);
        $('.iframe-overlay').fadeOut();
        // location.reload();
    }
</script>

<script type="text/javascript">
    $(document).on('change', '#task_color', function () {
        var selected = $(this).find('option:selected');
        var color = selected.data('color');

        if (color == null) {
            $(this).css('background', '#ffffff');
        } else {
            $(this).css('background', color);
        }
    });
    $(document).on('change', '#task_pair', function () {
        var selected = $(this).find('option:selected');
        var color = selected.data('color');
        if (color == null) {
            $(this).css('background', '#ffffff');
        } else {
            $(this).css('background', color);
        }
    });

    // delete existing task
//    $(document).ready(function () {
//        $(".confirm_delete_task").on('click', function (event) {
//            alert('sasa');
//            var x = confirm("Are you sure you want to delete this task?");
//            if (x) {
//                return true;
//            } else {
//                event.preventDefault();
//                return false;
//            }
//        });
//    });
    $(document).ready(function () {
        hide_empty_modules();
    });

    function hide_empty_modules() {
        $(".parent_li").not('#project_name').each(function () {
            if ($(this).find(".panel-task-border").length == 0) {
                $(this).hide();
            }

        });
        $(".li_not_selectable").not('#project_name').each(function () {
            if ($(this).find(".panel-task-border").length == 0) {
                $(this).hide();
            }
        });
    }

    // nested filter  - search function jquery
    $(function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[":"].contains = jQuery.expr.createPseudo(function (arg) {
            return function (elem) {
                return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        // this function calls itself, to search in every level
        function hasChild(list) {
            // the searched value
            var filter = $('#filterinput').val();
            // for every children, do function:
            list.children('li').each(function () {
                li_div = $(this);
                // see if the list contains the filter
                if (li_div.find("span > a:contains(" + filter + ")").length > 0 || li_div.find(".panel-task > .panel-task-body:contains(" + filter + ")").length > 0) {
                    // see if it has nested levels
                    if (li_div.find('ul:first').length > 0) {
                        li_div.show();
                        // recall function, searching the ul tag
                        hasChild(li_div.find('ul :first').parent());
                    } else {
                        li_div.show();
                        // if it has tasks

                        li_div.find('.panel-task-border').each(function () {
                            if ($(this).find("div.panel-task-body:contains(" + filter + ")").length > 0) {
                                $(this).show();
                            }
                            else {
                                $(this).hide();
                            }
                        });

                    }
                } else {
                    $(this).hide();
                }
            });
        }

        $('#filterinput').keyup(function () {
            // the id of the filtered list
            var list = '#list';
            // the searched value
            var filter = $('#filterinput').val();
            if (filter) {
                // call previous function
                hasChild($(list));
            } else {
                // show all
                $(list).find("li").show();
                $(list).find('.panel-task-border').show();

            }
            hide_empty_modules();
            return false;
        });
    });

</script>
