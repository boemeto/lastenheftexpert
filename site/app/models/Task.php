<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11.06.2015
 * Time: 10:13
 */
class Task extends Eloquent
{


    public static $rules = [
        'name_task' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_task';
    protected $table = 'pr_tasks';
    protected $guarded = ['id_task'];
    protected $fillable = ['code_task', 'name_task', 'name_task_en', 'description_task_en', 'meta_tags', 'address_code', 'description_task', 'is_default_task', 'fk_company'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Task::whereId_task($id)->first();
        return $list;
    }

    public static function getFirstByName($name_task, $description_task = null)
    {
        if ($description_task == null) {
            $task = Task::where('name_task', 'like', $name_task)
                ->first();
        } else {
            $task = Task::where('name_task', 'like', $name_task)
                ->where('description_task', 'like', $description_task)
                ->first();
        }

        return $task;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_tasks', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_tasks', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }

}