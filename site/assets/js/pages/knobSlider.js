/*------------------------------------------------------------------
 [ Knob & Ranges  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   knob slider
 -------------------------------------------------------------------*/


jQuery(document).ready(function ($) {
    'use strict';

    /***  Range Slider Call ***/
    rangeSliderLoad();
    /***  Range Slider Call End ***/

    /**** Knob call ****/
    Knob_call();
    /**** Knob call end****/

});
/*$( window ).resize(function() {

 });*/

var ionRangeSlider;
$(window).resize(function () {
    clearTimeout(ionRangeSlider);
    ionRangeSlider = setTimeout(doneResizingIonRangeSlider, 600);

});
function doneResizingIonRangeSlider() {
    $("#rangeSlider_1").ionRangeSlider("update");
    $("#rangeSlider_2").ionRangeSlider("update");
    $("#rangeSlider_3").ionRangeSlider("update");
    $("#rangeSlider_4").ionRangeSlider("update");
}
/***  Range Slider Call ***/
function rangeSliderLoad() {
    $("#rangeSlider_1").ionRangeSlider({
        min: 0,
        max: 5000,
        from: 1000,
        to: 4000,
        type: 'double',
        prefix: "$",
        maxPostfix: "+",
        prettify: false,
        hasGrid: true
    });
    $("#rangeSlider_2").ionRangeSlider({
        min: 0,
        max: 10,
        type: 'single',
        step: 0.1,
        postfix: " carats",
        prettify: false,
        hasGrid: true,
        from: 4
    });
    $("#rangeSlider_3").ionRangeSlider({
        values: [
            "January", "February", "March",
            "April", "May", "June",
            "July", "August", "September",
            "October", "November", "December"
        ],
        type: 'single',
        hasGrid: true

    });
    $("#rangeSlider_4").ionRangeSlider({
        min: 10000,
        max: 100000,
        step: 100,
        postfix: " km",
        from: 55000,
        hideMinMax: true,
        hideFromTo: false
    });
}
/***  Range Slider Call End ***/

function Knob_call() {
    var v, up = 0, down = 0, id = 0, id_nr = 0, i = 0

        , incr = function (id_nr) {
        i = parseInt($("input#ival" + id_nr).val());

        switch (id_nr) {
            case '1':
                break;
            case '2':
                break;
            case '3':
                break;
            case '4':
                break;
            case '5':
                break;

        }
        if (id_nr == 3) {
            i += 100;
        }
        else {
            i++;
        }

        $("div#idir" + id_nr).show().html("+").fadeOut();
        $("input#ival" + id_nr).val(i);

    }
        , decr = function (id_nr) {
        i = parseInt($("input#ival" + id_nr).val());
        if (id_nr == 3) {
            i -= 100;
        }
        else {
            i--;
        }
        if (i >= 0) {
            $("div#idir" + id_nr).show().html("-").fadeOut();
            $("input#ival" + id_nr).val(i);
        }


    };
    $(".infinite").knob({
        min: 0
        , max: 100
        , stopper: false,
        release: function (value) {
            //console.log(this.$.attr('value'));
            console.log("release : " + value);
        },
        cancel: function () {
            console.log("cancel : ", this);
        },
        draw: function () {

            // "tron" case
            if (this.$.data('skin') == 'tron') {

                this.cursorExt = 0.3;

                var a = this.arc(this.cv)  // Arc
                    , pa                   // Previous arc
                    , r = 1;

                this.g.lineWidth = this.lineWidth;

                if (this.o.displayPrevious) {
                    pa = this.arc(this.v);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                    this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }
        , change: function () {

            id_nr = this.$.attr('data-entryid');

            if (v > this.cv) {
                if (up) {
                    decr(id_nr);
                    up = 0;
                } else {
                    up = 1;
                    down = 0;
                }
            } else {
                if (v < this.cv) {
                    if (down) {
                        incr(id_nr);
                        down = 0;
                    } else {
                        down = 1;
                        up = 0;
                    }
                }
            }
            v = this.cv;
        }

    });
    $(".dial").knob({
        'release': function (v) { /*make something*/
        },
        draw: function () {

            // "tron" case
            if (this.$.data('skin') == 'tron') {

                this.cursorExt = 0.3;

                var a = this.arc(this.cv)  // Arc
                    , pa                   // Previous arc
                    , r = 1;

                this.g.lineWidth = this.lineWidth;

                if (this.o.displayPrevious) {
                    pa = this.arc(this.v);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                    this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }
    });

}