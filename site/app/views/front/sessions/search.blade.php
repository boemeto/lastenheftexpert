@extends('front.layouts.login')

@section('login')
    {{ Form::open(array('action' => 'SessionsController@store', 'class'=>"navbar-form navbar-left", 'role'=>"login"))}}
    <div class="form-group">
        <div class="form-group has-feedback" style="float: left">
            {{ Form::text('username_','',[ 'placeholder'=>"Username", 'class'=>"form-control", 'required']) }}
            {{ $errors->first('username_','<div class="text-red">:message</div>') }}
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback" style="float: left">
            {{ Form::password('password_',['placeholder'=>"Password", 'class'=>"form-control", 'required']) }}
            {{ $errors->first('password_','<div class="text-red">:message</div>') }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        {{ Form::submit('Sign In',['class' => "btn ls-light-green-btn btn-flat" , 'style'=>"float: left"]) }}
    </div>
    {{ Form::close() }}
@stop

@section('register')
    <div class="box box-default with-border" style="">
        <div class="box-header">
            <h3 class="box-title">Search Company</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="search_company_form">
                {{ Form::open(array('url' => 'searchCompany', 'method'=>'post')) }}
                    <div class="form-group has-feedback">
                        {{ Form::text('searched_company','',[ 'placeholder'=>"Company", 'class'=>"form-control", 'required', 'id'=>'tags']) }}
                        {{ $errors->first('searched_company','<div class="text-red">:message</div>') }}
                    </div>
                    {{ $errors->first('confirm_user','<div class="text-green">:message</div>') }}
                    {{ Form::submit('Search',['class' => "btn ls-light-green-btn  btn-flat" ]) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('register_it_user')
    <div class="box box-default with-border">
        <div class="box-header">
            <h3 class="box-title">Register IT Form</h3>
        </div>
        <!-- /.box-header -->
    </div>
@stop
