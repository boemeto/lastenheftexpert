@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">


            <h3 class="box-title"> Affiliates </h3>
            
        </div>

        <div class="box-body">

            @if(count($affiliated_companies) >0)
                <table class="table table-bordered table-responsive">
                    <tr>
                        <th>Company</th>
                        <th>Affiliation Company</th>
                        <th>Is Pending</th>
                        <th></th>
                    </tr>
                    @foreach($affiliated_companies as $row)
                        <tr>
                            <td>{{$affiliated_ids[$row->fk_company]}}</td>
                            <td>{{$affiliated_ids[$row->fk_affiliation]}}</td>
                            <td>{{ ($row->is_pending == 1)?'Ja':'Nein'}}</td>
                            <td>
                                @if($row->fk_affiliation == $company->id_company && $row->is_pending == 1)
                                    {{ Form::open(['action' => ['AffiliationController@accept']]) }}
                                    {{Form::hidden('id_affiliation',$row->id_affiliation_company)}}
                                    {{Form::submit('Accept',['class'=>"btn btn-flat ls-light-blue-btn"])}}
                                    {{ Form::close() }}
                                    {{ Form::open(['route' => ['affiliates.destroy', $row->id_affiliation_company], 'method' => 'delete']) }}
                                    {{Form::submit('Reject',['class'=>"btn btn-flat ls-light-blue-btn", 'name'=>'delete'])}}
                                    {{ Form::close() }}
                                @elseif($row->fk_affiliation != $company->id_company && $row->is_pending == 1)
                                    {{ Form::open(['route' => ['affiliates.destroy', $row->id_affiliation_company], 'method' => 'delete']) }}
                                    {{Form::submit('Cancel',['class'=>"btn btn-flat ls-light-blue-btn", 'name'=>'delete'])}}
                                    {{ Form::close() }}

                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif
            <br><br>

            <h3>New Affiliation</h3>

            {{ Form::open(array('action' => 'AffiliationController@store', 'method'=>'post')) }}
            <div class="form-group has-feedback">

                {{ Form::text('searched_company',$searched_company,[ 'placeholder'=>"Company", 'class'=>"form-control", 'required', 'id'=>'tags']) }}
                {{ $errors->first('searched_company','<div class="text-red">:message</div>') }}

            </div>
            {{ Form::submit('Search',['class' => "btn ls-light-blue-btn  " ]) }}

            {{ Form::close() }}
            <br>


            <br><br>
            @if(count($companies) >0)

                {{ Form::model(array(), ['method'=>'PATCH', 'route'=>['affiliates.update',$company->id_company],'id'=>'affiliateCompanyForm'])}}
                <div>
                    @foreach($companies as $row)
                        {{Form::radio('id_company', $row->id_company,false,[ "class"=>"id_company", "id"=>"id_company"]) }}
                        {{$row->name_company}}<br>
                    @endforeach
                </div>
                {{ $errors->first('id_company','<div class="text-red">:message</div>') }}
                {{ Form::submit(' Affiliate to Company',['class' => "btn ls-light-blue-btn" ]) }}


            @endif


        </div>
        <!-- /.box-header -->


    </div>
    <!-- /.box-body -->
    </div>


@stop