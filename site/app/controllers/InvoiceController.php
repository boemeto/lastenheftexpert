<?php

class InvoiceController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        $id_company = $user->fk_company;

        $invoices = Invoice::getInvoicesByCompany($id_company);

        return View::make('front.invoices.index', compact('invoices'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function invoice_list($id_project)
    {
        return View::make('front.invoices.create', compact('id_project', 'id_company'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = Auth::user();
        $id_project = Input::get('id_project');
        $id_package = Input::get('id_package');
        switch ($id_package) {
            case 1:
                $unit_price = 10.99;
                break;
            case 2:
                $unit_price = 30.99;
                break;
            case 3:
                $unit_price = 99.99;
                break;
            default:
                $unit_price = 10.99;
                break;
        }
        $date = date('d-m-Y');
        $id_company = $user->fk_company;

        $invoice = new  Invoice();
        $invoice->inv_serial = 'LE';
        $invoice->inv_number = Invoice::getInvoiceNumber();
        $invoice->inv_date = $date;
        $invoice->fk_company = $id_company;
        $invoice->save();
        $invoice_id = $invoice->id_invoice;

        $invoice_row = new Invoice_row();
        $invoice_row->fk_invoice = $invoice_id;
        $invoice_row->fk_project = $id_project;
        $invoice_row->fk_package = $id_package;
        $invoice_row->qty = 1;
        $invoice_row->unit_price = $unit_price;
        $invoice_row->save();

        return Redirect::to('invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $id_company = $user->fk_company;

        $invoice = Invoice::getFirstById($id);
        if ($invoice->fk_company != $id_company) {
            return App::abort(404);
        }

        $company = Company::getFirstById($invoice->fk_company);
        $main_headquarter = Company_headquarters::getMainHeadquarter($invoice->fk_company);
        $project = Project::getFirstById($invoice->fk_project);
        $admin_company = Admin_company::getFirstById(1);
        $user = User::getFirstById($invoice->fk_user);
        $i = 1;
//        $invoices = Invoice::getInvoicesByCompany($id_company, $id);
        $package_id = Projects_package::whereFk_project($project->id_project)->first();
        $package = Package::getFirstById($package_id->fk_package);
        // echo "<pre>";dd($package->name_package);
        $invoices = Invoice::getInvoicesByProject($project->id_project);
        return View::make('front.invoices.show', compact('invoice', 'invoices', 'i', 'company', 'main_headquarter', 'project', 'admin_company', 'user', 'package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function invoice_pdf($id)
    {
        $invoice = Invoice::getFirstById($id);
        $invoice->last_dld = date("Y.m.d");
        $invoice->save();
        $company = Company::getFirstById($invoice->fk_company);
        $main_headquarter = Company_headquarters::getMainHeadquarter($invoice->fk_company);
        $project = Project::getFirstById($invoice->fk_project);
        $admin_company = Admin_company::getFirstById(1);
        $user = User::getFirstById($invoice->fk_user);
        $package_id = Projects_package::whereFk_project($project->id_project)->first();
        $package = Package::getFirstById($package_id->fk_package);

        // return View::make('front.invoices.invoice_pdf', compact('invoice', 'company', 'main_headquarter', 'project', 'admin_company', 'user'));
        $pdf = PDF::loadView('front.invoices.invoice_pdf',
            compact('invoice', 'company', 'main_headquarter', 'project', 'admin_company', 'user', 'project', 'package'))
            ->setPaper('A4')->setOrientation('portrait');
        return $pdf->stream($invoice->inv_serial . '-' . $invoice->invoice_number . '.pdf');
    }
}
