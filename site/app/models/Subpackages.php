<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Subpackages extends Eloquent
{
    public static $rules = [
        'subpackage_name' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'subpackage_id';
    protected $table = 'subpackages';
    protected $guarded = ['subpackage_id'];
    protected $fillable = ['subpackage_name', 'package_id', 'subpackage_type_id', 'subpackage_price', 'subpackage_period'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstByPackageId($id)
    {
        $subpackage = self::where('subpackage_id', '=', $id)->first();
        return $subpackage;
    }
    
    public static function getLastPackage()
    {
        $subpackage = self::orderBy('id_package', 'desc')->first();
        return $subpackage;
    }

    public static function getSubpackages($id)
    {
        $lists = self::where('package_id', '=', $id)->get();
        return $lists;
    }
}