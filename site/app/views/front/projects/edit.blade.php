<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 7/23/2015
 * Time: 9:58 AM
 */

?>
@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/selectize.bootstrap3.css')}}">
@stop
@section('footer_scripts')
    <script src="{{asset('assets/js/jquery.easypiechart.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('change', 'select.form-control', function() {
            var task_color = $('#task_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.checked-filter').val() == 0 &&
                (task_color.toLowerCase().indexOf("select") >= 0 || task_color.toLowerCase().indexOf("aus­wäh­len") >= 0)) {

                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-color-button', function() {
            var task_color = $('#task_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.checked-filter').val() == 0 &&
                (task_color.toLowerCase().indexOf("select") >= 0 || task_color.toLowerCase().indexOf("aus­wäh­len") >= 0)) {

                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-colors', function() {
            var task_color = $('#task_color').text();
            if ((task_color.toLowerCase().indexOf("select") >= 0 || task_color.toLowerCase().indexOf("aus­wäh­len") >= 0) && $('.priority-filter').val() == 0 &&
                $('.checked-filter').val() == 0 && $('.attach-filter').val() == 0) {

                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).ready(function () {
            var task_color = $('#task_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.checked-filter').val() == 0 &&
                (task_color.toLowerCase().indexOf("select") >= 0 || task_color.toLowerCase().indexOf("aus­wäh­len") >= 0)) {

                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $('.card_pay').click(function() {
          if ($(this).hasClass('card_pay_image_bw')) {
            $(this).removeClass('card_pay_image_bw').addClass('card_pay_image');
            $('.paypal_pay').removeClass('paypal_pay_image').addClass('paypal_pay_image_bw');
            $('.transfer_pay').removeClass('transfer_pay_image').addClass('transfer_pay_image_bw');
            $('.sofort_pay').removeClass('sofort_pay_image').addClass('sofort_pay_image_bw');
            $('.extend-project-btn').css('display', 'inline-block');
            $('.extend-project-btn').attr('form-submit', 'submit-card');
          } else {
            $(this).removeClass('card_pay_image').addClass('card_pay_image_bw');
            $('.extend-project-btn').css('display', 'none');
          }
        });

        $('.paypal_pay').click(function() {
          if ($(this).hasClass('paypal_pay_image_bw')) {
            $(this).removeClass('paypal_pay_image_bw').addClass('paypal_pay_image');
            $('.card_pay').removeClass('card_pay_image').addClass('card_pay_image_bw');
            $('.transfer_pay').removeClass('transfer_pay_image').addClass('transfer_pay_image_bw');
            $('.sofort_pay').removeClass('sofort_pay_image').addClass('sofort_pay_image_bw');
            $('.extend-project-btn').css('display', 'inline-block');
            $('.extend-project-btn').attr('form-submit', 'submit-paypal');
          } else {
            $(this).removeClass('paypal_pay_image').addClass('paypal_pay_image_bw');
            $('.extend-project-btn').css('display', 'none');
          }
        });

        $('.transfer_pay').click(function() {
          if ($(this).hasClass('transfer_pay_image_bw')) {
            $(this).removeClass('transfer_pay_image_bw').addClass('transfer_pay_image');
            $('.paypal_pay').removeClass('paypal_pay_image').addClass('paypal_pay_image_bw');
            $('.card_pay').removeClass('card_pay_image').addClass('card_pay_image_bw');
            $('.sofort_pay').removeClass('sofort_pay_image').addClass('sofort_pay_image_bw');
            $('.extend-project-btn').css('display', 'inline-block');
            $('.extend-project-btn').attr('form-submit', 'submit-transfer');
          } else {
            $(this).removeClass('transfer_pay_image').addClass('transfer_pay_image_bw');
            $('.extend-project-btn').css('display', 'none');
          }
        });

        $('.sofort_pay').click(function() {
          if ($(this).hasClass('sofort_pay_image_bw')) {
            $(this).removeClass('sofort_pay_image_bw').addClass('sofort_pay_image');
            $('.paypal_pay').removeClass('paypal_pay_image').addClass('paypal_pay_image_bw');
            $('.transfer_pay').removeClass('transfer_pay_image').addClass('transfer_pay_image_bw');
            $('.card_pay').removeClass('card_pay_image').addClass('card_pay_image_bw');
            $('.extend-project-btn').css('display', 'inline-block');
            $('.extend-project-btn').attr('form-submit', 'submit-sofort');
          } else {
            $(this).removeClass('sofort_pay_image').addClass('sofort_pay_image_bw');
            $('.extend-project-btn').css('display', 'none');
          }
        });

        $('.extend-project-btn').click(function() {
            var fk_industry = $('#FormNewProject').find('input[name=fk_industry]:checked').val();
            var name_project = $('#FormNewProject').find('input[id=project_name]').val();
            var nr_employees = $('#FormNewProject').find('input[name=nr_employees]').val();
            var nr_users = $('#FormNewProject').find('input[name=nr_users]').val();
            var max_budget = $('#FormNewProject').find('select[name=max_budget]').val();
            var start_project = $('#FormNewProject').find('input[name=start_project]').val();
            var implement_time = $('#FormNewProject').find('input[name=implement_time]').val();
            var fk_user = [];
            var user_class = $('#FormNewProject').find('.bootstrap-switch-on');
            for (var i=0; i<user_class.length; i++)  {
                fk_user[i] = $(user_class[i]).find('.fk_user').val();
            }

            validation = true;
            if (!$('.package_select').is(':checked')) {
                 $('#formConfirmOk')
                      .find('#frm_body').html('Please Select a Package')
                      .end().modal('show');
                validation *= false;
                event.preventDefault();
            }
            validation *= checkedFieldCheckbox('terms_and_cond');
            if (validation == false) {
                event.preventDefault(); // prevent submitting
                return false;
            }
            event.preventDefault();

            if ($(this).attr('form-submit') == 'submit-card') {
                $('.stripe-payment').trigger('click');
                var $button = $('.stripe-payment'),
                    $form = $button.parents('form');
                var opts = $.extend({}, $button.data(), {
                    token: function(result) {
                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                    }
                });
                StripeCheckout.open(opts);
            }
            if ($(this).attr('form-submit') == 'submit-paypal') {
                var subpackage_id = $('.pack_extend').find('input[name=subpackage_id]:checked').val();
                $('#paypal_payment').find('input[name=subpackage_id]').val(subpackage_id);
                $('#paypal_payment').submit();
            }
            if ($(this).attr('form-submit') == 'submit-sofort') {
                alert('sofort');
            }
            if ($(this).attr('form-submit') == 'submit-trtansfer') {
                alert('transfer');
            }
        });
        //
        // $('#paypal_payment').find('input[name=submit]').on('click', function(event){
        //     var subpackage_id = $('.pack_extend').find('input[name=subpackage_id]:checked').val();
        //     $('#paypal_payment').find('input[name=subpackage_id]').val(subpackage_id);
        //     validation = true;
        //     if (!$('.package_select').is(':checked')) {
        //           $('#formConfirmOk')
        //               .find('#frm_body').html('Please Select a Package')
        //               .end().modal('show');
        //         validation *= false;
        //         event.preventDefault();
        //     }
        //     validation *= checkedFieldCheckbox('terms_and_cond');
        //     if (validation == false) {
        //         event.preventDefault(); // prevent submitting
        //         return false;
        //     }
        // });

        function select_color(element) {
            $('[name=color]').val($(element).attr('data-id'));
            $('#task_color').text($(element).attr('data-name'));
            $('.color-preview').css({'background-color': $(element).attr('data-color')});
        };

        $('.menu_title>a').on('click', function(e) {
            e.preventDefault();
            $(this).parent().siblings('.easy-tree-toolbar').find('.remove').find('button').on('click', function(ev) {
                  ev.preventDefault();
                var title = $(this).attr('data-title');
                var msg = $(this).attr('data-message');
                var dataForm = $(this).attr('data-form');
                var url = $(this).attr('href');

              $('#formConfirm')
                .find('#frm_body').html(msg)
                .end().find('#frm_title').html(title)
                .end().modal('show');
            });
        });
    </script>
    <script type="text/javascript">

    $('.delete_task').on('click', function(e) {
        e.preventDefault();
        var title = $(this).attr('data-title');
        var msg = $(this).attr('data-message');
        var id = $(this).attr('data-id');

        $('#formConfirm').find('#frm_body').html(msg)
          .end().find('#frm_title').html(title)
          .end().modal('show');

        $('#frm_submit').addClass('confirm_delete_task').attr('id', id);
      });

      $('#formConfirm').on('click', '.confirm_delete_task', function(e) {
            var id = $(this).attr('id');
            var form = $("[data-id=" + id + "]").parent();
            $(form).submit();
      });

    </script>
    <script type="text/javascript">
        $('#chart_1').easyPieChart({
            animate: 2000,
            barColor: '#324E59',
            scaleColor: 'white',
            trackColor: '#F0AD4E',
            lineWidth: 2,
            size: 90,
            easing: 'easeOutBounce',
            onStep: function (from, to, percent) {
                $(this.el).find('.pie-widget-count-2').text(Math.round(percent));
            }
        });

        $('#chart_1').on("mouseover", function () {
            var value = $('#chart_1').data('percent');
            if (value > 30) {
                $('#chart_1').data('easyPieChart').update(0);
            } else {
                $('#chart_1').data('easyPieChart').update(100);
            }
            $('#chart_1').data('easyPieChart').update(value);
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
           $('input.package_select').on('change', function() {
               $('input.package_select').not(this).prop('checked', false);
           });

           //notification if user wants to download attachments and project doesn't have any tasks with attachments
           var countProjectAttachments = document.getElementsByClassName('attachment_counter');
           var sum = 0;
           for(var i=0; i < countProjectAttachments.length; i++) {
             sum += parseInt(countProjectAttachments[i].innerHTML);
           }
           $('.get-attachments-zip').on('click', function(e){
             if(sum === 0) {
                e.preventDefault();
                var msg = "<?php echo trans('messages.no_attachments'); ?>";
                $('#formConfirmOk')
                  .find('#frm_body').html(msg)
                  .end().find('#frm_title').html('Info')
                  .end().modal('show');
             }
           });

           var countDownDate = document.getElementById("timer").getAttribute('data-timer');
           var x = setInterval(function() {
               var now = new Date().getTime();
               var distance = countDownDate*1000 - now;

               var days = Math.floor(distance / (1000 * 60 * 60 * 24));
               var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
               var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
               var seconds = Math.floor((distance % (1000 * 60)) / 1000);

              document.getElementById("timer").innerHTML =  days + "d : " + hours + "h : " + minutes + "m : " + seconds + "s";
           }, 1000);
        });
    </script>
     <script type="text/javascript">
         jQuery(document).ready(function(){
            jQuery('#hideshow').on('click', function(event) {
                 jQuery('#subpackages').toggle('show');
            });
            jQuery('.close_subpackages').on('click', function(event) {
                 jQuery('#subpackages').toggle('show');
            });
        });
     </script>
     <script type="text/javascript">
         $(".package_select").change(function(){
           if($(this).is(':checked')) {
             var name = $(this).attr('data-name');
             $('#selected_subpackage_name').empty().append(name);

             var period = $(this).attr('data-period');
             $('#selected_subpackage_period').empty().append(period);

             var price = $(this).attr('data-price');
             $('#selected_subpackage_price').empty().append(price);

             var tva = $(this).attr('data-tva');
             $('#selected_subpackage_tva').empty().append(tva);

             var total = parseFloat(price) + parseFloat(tva);
             $('#selected_subpackage_total').empty().append(total);

             $(".table_subpackage").show();
           } else {
             $(".table_subpackage").hide();
           }
         })
    </script>
    {{--// custom scripts--}}
    @include('front.projects.project_scripts')
    @include('front.projects.tree_script')
    @include('utils/validations_project')
@stop

@section('content')
    <div class="loader_back">
        <div class="loader_gif"></div>
    </div>
    <div class="box projects_page">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 tabs">
                    <ul class="nav nav-tabs nav-left icon-tab profile-navigation-tabs">
                        <li class="{{($has_filters)?'':'active'}}"><a data-toggle="tab" href="#home">{{trans('messages.label_details')}}</a></li>
                        <li class="{{($has_filters)?'active':''}}"><a data-toggle="tab" href="#filters">{{trans('messages.label_filters')}}</a></li>
                        <li><a data-toggle="tab" href="#export">{{trans('messages.label_export')}}</a></li>
                        <li><a data-toggle="tab" href="#invoices">{{trans('messages.nav_invoices')}}</a></li>
                        <li><a data-toggle="tab" href="#projects">{{trans('messages.other_projects')}}</a></li>
                    </ul>
                    <div class="tab-content profile-navigation-content">
                        <div id="home" class="tab-pane fade {{($has_filters)?'':' in active'}}">
                            <div class="row">
                                <div class=" col-lg-10 col-xs-9">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12 project-details">
                                            <table class="table-responsive tab-table">
                                                <tr>
                                                    <th>{{trans('messages.label_industry_type')}}:&nbsp;</th>
                                                    <td> {{$industry_type->name_industry_type}}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_industry')}}:&nbsp;</th>
                                                    <td> {{$industry->name_industry}}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_company')}}:&nbsp;</th>
                                                    <td>{{ $company->name_company  }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_admin')}}:&nbsp;</th>
                                                    <td> {{ $admin_user->first_name .' '. $admin_user->last_name }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-5 col-xs-12 project-details">
                                            <table class="table-responsive tab-table">
                                                <tr>
                                                    <th>{{trans('messages.label_project')}}:&nbsp;</th>
                                                    <td> {{$project->name_project}}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_created_at')}}:&nbsp;</th>
                                                    <td>  {{date('d.m.Y',strtotime($project->created_at))}}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_due_date')}}:&nbsp;</th>
                                                    <td>  {{date('d.m.Y',strtotime($project->due_date))}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="project-status-container col-lg-2 col-xs-3">
                                    <?php
                                    $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $project->id_project);
                                    $startDate = $project->created_at;
                                    $endDate = $project->due_date;
                                    $days = daysPercentPassed($startDate, $endDate);
                                    ?>
                                    {{-- <div class="pie-widget pie-project">
                                        <div class="pie-widget-2 chart-pie-widget" id="chart_1" data-percent="{{$days}}">
                                            <span class="pie-widget-count-2 pie-widget-count">{{$days}}</span>
                                        </div>
                                        <div class="clear"></div>

                                        <div id='hideshow' value='hide/show' class="btn btn-primary" style="font-size: 14px"> {{trans('messages.l_extend')}}</div>
                                    </div> --}}
                                    <div class="text-center">
                                      <div class="btn btn-success project-status mw-170"> {{trans('messages.l_active')}}</div>
                                      <div id="timer" class="time-left" data-timer="{{strtotime($endDate)}}"></div>
                                      <div id='hideshow' value='hide/show' class="btn btn-primary mw-170" style="font-size: 14px"> {{trans('messages.l_extend')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="filters" class="tab-pane fade {{($has_filters)?'in active':' '}}">
                            <div class="row filter_row">
                                {{ Form::open(['route'=>['projects.show',$project->id_project], 'method' => 'get', 'class'=>'filterform','id'=>"formSearch"]) }}
                                <div class=" col-xs-12 col-lg-12 pd-10">
                                    <div class=" row">
                                        {{-- filter by checked--}}
                                        <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0  ">
                                            <div class="form-group">
                                                <label class="filter-label">{{trans('messages.label_checked')}}: </label>
                                                {{ Form::select("fk_checked",$set_checked,$fk_checked,[   "class"=>"form-control checked-filter"  ]) }}
                                            </div>
                                        </div>
                                        {{-- filter by priority--}}
                                        <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label class="filter-label">{{trans('messages.label_priority')}}:</label>
                                                {{ Form::select("fk_priority",$priorities,$fk_priority,[   "class"=>"form-control priority-filter"  ]) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        {{-- filter by attachment--}}
                                        <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label class="filter-label">{{trans('messages.label_attachments')}}:</label>
                                                {{ Form::select("fk_attach",$set_attach,$fk_attach,[   "class"=>"form-control attach-filter"  ]) }}
                                            </div>
                                        </div>
                                        {{-- filter by color--}}
                                        <div class="col-xs-12 col-lg-4 col-sm-4  pd-10 pdtp-0">
                                            <?php
                                            $select_color = Task_colors::getFirstById($color);
                                            if (count($select_color) == 1) {
                                                $s_color = $select_color->code_color;
                                            } else {
                                                $s_color = '#fff';
                                            }
                                            ?>
                                            <div class="form-group">
                                                <label class="filter-label">{{trans('messages.label_choose_color')}}: </label>
                                                <div class="dropdown">
                                                    <button class="btn dropdown-toggle form-control select-mark-color-button"
                                                    type="button" data-toggle="dropdown" name="color">
                                                        <span id="task_color" class="">
                                                          {{$select_color->name_color!=null?$select_color->name_color: trans('messages.input_select')}}
                                                        </span>
                                                        <span class="color-preview" style="background: {{$s_color}}"></span>
                                                        <span class="caret pull-right select-arrow-down"></span>
                                                    </button>
                                                    <ul class="dropdown-menu color-dropdown">
                                                        <input type="hidden" name="color">
                                                        {{-- @if ($select_color->name_color!=null) --}}
                                                          <li class="select-colors">
                                                            <a data-id="0" data-name="{{trans('messages.input_select')}}" data-color="" onclick="select_color(this)" href="#" >{{trans('messages.input_select')}}
                                                              </a>
                                                          </li>
                                                        {{-- @endif --}}
                                                        @foreach($set_colors as $set_color)
                                                          <li class="select-colors">
                                                            <a data-id="{{$set_color->id_task_color}}" data-name="{{$set_color->name_color}}" data-color="{{$set_color->code_color}}" onclick="select_color(this)" href="#" >{{$set_color->name_color}}
                                                              <span class="select-color-preview pull-right" style="background:{{$set_color->code_color}}"></span>
                                                              </a>
                                                          </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="filter-buttons col-xs-6 col-lg-2 col-md-2 col-sm-3  pd-10 pdtp-0">
                                        <button type="submit" class="btn ls-light-blue-btn btn-block">
                                            {{trans('messages.act_filter')}}
                                        </button>
                                    </div>
                                    <div class="filter-buttons reset-button col-xs-6 col-lg-2 col-md-2 col-sm-3  pd-10 pdtp-0">
                                        <a href="{{URL::to('projects/'.$project->id_project)}}" class="btn ls-red-btn btn-block">
                                            {{trans('messages.act_reset_filters')}}
                                        </a>
                                    </div>
                                    <div class="filter-buttons-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                        <button type="submit" class="btn btn-block" disabled>
                                            {{trans('messages.act_filter')}}
                                        </button>
                                    </div>
                                    <div class="filter-buttons-inactive reset-button-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                        <button type="submit" class="btn btn-block" disabled>
                                            {{trans('messages.act_reset_filters')}}
                                        </button>
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                        <div id="export" class="tab-pane fade export-panel">
                            <div class="export-links">
                                <a href="{{URL::to('download_project_pdf/'.$project->id_project)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_project_word/'.$project->id_project)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_project_excel/'.$project->id_project)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_project_attachments/'.$project->id_project)}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                            <div class="{{$export_file->file_name!=''?'export-details':''}}">
                                <div class="row">
                                  @if ($export_file->file_name!='')
                                    <div class="col-xs-4">
                                      <strong>{{trans('messages.last_export')}}:</strong>
                                    </div>
                                    <div class="col-xs-8">{{$export_file->file_name}}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->date!='0000-00-00 00:00:00' && $export_file->date!=null)
                                    <div class="col-xs-4">
                                      <strong>{{trans('messages.exported_at')}}:</strong>
                                    </div>
                                    <?php
                                        $date = explode(' ', $export_file->created_at);
                                        $hours = explode(':',$date[1]);
                                        $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                        $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                    ?>
                                    <div class="col-xs-8">{{$date}}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->exported_by!='')
                                    <div class="col-xs-4">
                                      <strong>{{trans('messages.label_author')}}:</strong>
                                    </div>
                                    <div class="col-xs-8">{{$export_file->exported_by}}</div>
                                  @endif
                                </div>
                            </div>
                        </div>
                        <div id="invoices" class="tab-pane fade">
                            @if(count($project_invoices)> 0)
                                <table class="table table-responsive table-bottomless data_table ">
                                    <thead>
                                      <tr>
                                          <th>{{trans('messages.label_nr_crt')}}</th>
                                          <th>{{trans('messages.label_project_name')}}</th>
                                          <th>{{trans('messages.label_date')}}</th>
                                          <th>{{trans('messages.label_payment_term')}}</th>
                                          <th>{{trans('messages.label_sum')}}</th>
                                          <th>{{trans('messages.label_status')}}</th>
                                          <th class="text-center">{{trans('messages.label_action')}}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($project_invoices as $invoice)
                                        <tr>
                                            <td>{{$invoice->inv_serial}}-{{$invoice->inv_number}}</td>
                                            <td>{{Invoice::getProjectName($invoice->id_invoice)}}</td>
                                            <td>{{date('d.m.Y',strtotime($invoice->inv_date))}}</td>
                                            <td>{{date('d.m.Y',strtotime($invoice->inv_date. '+ 14 days'))}}</td>
                                            <td class="pull-right pdr30">
                                              <div class="table-invoice-price">
                                                <b>{{formatToGermanyPrice($invoice->sum_paid)}}</b>
                                                <i class="fa fa-euro"></i>
                                              </div>
                                            </td>
                                            <td>
                                              <span class="user_project label label-{{Invoice::getInvoiceStatusColor($invoice->id_invoice)}}">
                                                {{Invoice::getInvoiceStatus($invoice->id_invoice)}}
                                              </span>
                                            </td>
                                            <td class="menu-action text-center">
                                                <a href="{{URL::to('invoice/'.$invoice->id_invoice)}}" class="btn btn-round ls-light-blue-btn">
                                                    <i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div id="projects" class="tab-pane fade">
                            @if(count($projects)>1)
                                <table class="table table-responsive table-bottomless data_table ">
                                    <thead>
                                         <tr>
                                             <th>{{trans('messages.label_nr_crt')}}</th>
                                             <th>{{trans('messages.label_project_name')}}</th>
                                             <th>{{trans('messages.label_industry_type')}}</th>
                                             <th>{{trans('messages.label_industry')}}</th>
                                             <th>{{trans('messages.label_created_at')}}</th>
                                             <th>{{trans('messages.label_due_date')}}</th>
                                             <th>{{trans('messages.label_creator')}}</th>
                                             <th>{{trans('messages.label_status')}}</th>
                                             <th>{{trans('messages.label_action')}}</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                      <?php $current_project_id = $project->id_project; ?>
                                      @foreach($projects as $proj)
                                          @if ($current_project_id!=$proj->id_project)
                                            <?php
                                              $id_industry = $proj->fk_industry;
                                              $industry = Industry::getFirstById($id_industry);
                                              $industry_type = Industry_type::getFirstById($industry->fk_type);
                                            //   $software = Module_structure::getSoftwareByIndustry($id_industry);
                                              $company = Company::getFirstById($proj->fk_company);
                                              $admin_user = User::getFirstById($proj->fk_user);
                                            ?>
                                            <tr>
                                                <td>{{getProjectId($proj->id_project)}}</td>
                                                <td>{{$proj->name_project}}</td>
                                                <td>{{$industry_type->name_industry_type}}  </td>
                                                <td>{{$industry->name_industry}}</td>
                                                <td>{{date('d.m.Y',strtotime($proj->created_at))}} </td>
                                                <td>{{date('d.m.Y',strtotime($proj->due_date))}}</td>
                                                <td>{{ $admin_user->first_name .' '. $admin_user->last_name }}</td>
                                                <td class="table-project-status">
                                                    {{($proj->status==1)?'<span class="user_project label label-success ">'.trans('messages.l_active').'</span>':' '}}
                                                    {{($proj->status==2)?'<span class="user_project label label-warning">'.trans('messages.label_blocked').'</span>':' '}}
                                                    {{($proj->status==3)?'<span class="user_project label label-danger">'.trans('messages.l_expired').'</span>':' '}}
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{URL::to('projects/'.$proj->id_project)}}" class=" btn btn-round ls-orange-btn"><i class="fa fa-pencil-square-o"></i></a>
                                                </td>
                                            </tr>
                                          @endif
                                      @endforeach
                                    </tbody>
                                </table>
                            @else
                              <h3>{{trans('messages.no_projects_found')}}</h3>
                            @endif
                        </div>
                    </div>
                    <!--End tabs container-->
                </div>
            </div>
            <div class="row" id="subpackages">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                          <div class="col-xs-8 col-sm-8">
                            <h3 class="mgtp-10">
                                {{trans('messages.label_extend_project')}}
                            </h3>
                          </div>
                          <div class="col-xs-4 col-sm-4 pull-right ">
                            <button class="btn ls-red-btn btn-flat close_subpackages">
                                <i class="fa fa-remove"></i>
                            </button>
                          </div>
                        </div>
                        <form class="" action="{{ action('PurchasesController@postCheckout', $project->id_project) }}" method="POST" id="extend_project">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="label_description_extend">
                                        {{trans('messages.label_description_extend')}}
                                    </div>
                                    <br>
                                    <h3 class="mgtp-10 text-center">
                                        Select you package to extend project
                                    </h3>
                                    @foreach($subpackages as $subpackage)
                                        <div class="col-xs-4 pd-5">
                                            <div class="pack pack_extend">
                                                <div class="pack_header">
                                                    <span style="margin-left: 10px;">{{ $subpackage->subpackage_name }}</span>
                                                    <span class="float-right pack_icheck">
                                                        <input type="checkbox" id="package_select" class="package_select" name="subpackage_id"
                                                               data-name="{{ $subpackage->subpackage_name }}"
                                                               data-price="{{ $subpackage->subpackage_price }}"
                                                               data-tva="{{ $subpackage->subpackage_price*0.19 }}"
                                                               data-period="{{ $subpackage->subpackage_period }}"
                                                               price="{{ $subpackage->subpackage_price }}"
                                                               value="{{ $subpackage->subpackage_id }}">
                                                    </span>
                                                    <div class="pack_body">
                                                        <hr class="hr_subpackages">
                                                        <div class="row">
                                                            <span class="subpackage_price">{{ $subpackage->subpackage_price }}</span>
                                                            <i class="fa fa-eur"></i>
                                                        </div>
                                                        <hr class="hr_subpackages">
                                                        <div class="row">
                                                            <i class="fa fa-calendar"></i>
                                                            <span class="subpackage_period">{{ $subpackage->subpackage_period }} Month(s)</span>
                                                        </div>
                                                        <hr class="hr_subpackages">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-lg-3 col-md-3 col-lg-offset-1 col-md-offset-1 table_subpackage">
                                   <div class="row">
                                       <h3 class="text-center selected_package_name">{{$current_package->name_package}}</h3>
                                   </div>
                                   <table class="table table-condensed table_subpackage">
                                      <tr style="background: #F0F0F0; font-size: 14px">
                                          <td class="pd-10"><b>Subpackage</b></td>
                                          <td class="pd-10 pull-right"><span><b id="selected_subpackage_name"></b></span></td>
                                      </tr>
                                      <tr style="font-size: 14px">
                                          <td class="pd-10">Period</td>
                                          <td class="pd-10 pull-right"><b><span id="selected_subpackage_period"></span> Month(s)</b></td>
                                      </tr>
                                      <tr style="background: #F0F0F0; font-size: 14px">
                                          <td class="pd-10"><b>{{trans('messages.label_total_pay')}}</b></td>
                                          <td class="pd-10 pull-right"><span><b id="selected_subpackage_price"></b> <i class="fa fa-eur"></i></span></td>
                                      </tr>
                                      <tr style="font-size: 14px">
                                          <td class="pd-10">{{trans('messages.label_tva_sum')}}</td>
                                          <td class="pd-10 pull-right"><span> <b id="selected_subpackage_tva"></b></span> <i class="fa fa-eur"></i></td>
                                      </tr>
                                      <tr style="background: #F0F0F0;font-size: 16px">
                                          <th class="pd-10"><span>{{trans('messages.label_total_sum')}}</span></th>
                                          <th class="pd-10 pull-right"><span id="selected_subpackage_total"></span> <i class="fa fa-eur"></i></th>
                                      </tr>
                                    </table>
                                <hr>
                                <br>
                                <div class="row">
                                    <div class="">
                                        {{Form::checkbox('terms_and_cond',1,false,    ['class'=>'icheck-green terms_and_cond' , 'id'=>'terms_and_cond', 'data_msg'=>$terms->getAttribute('name_post') ])}}  {{trans('messages.label_accept_terms')}}
                                        <a href="{{ URL::route('posts.show',[$terms->id_post]) }}"
                                           class="" data-toggle="modal" data-target="#modalTerms">
                                            {{$terms->getAttribute('name_post')}}
                                        </a>
                                            {{trans('messages.label_accept_terms2')}}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <input
                                        id="confirmPay"
                                        class="btn btn-block stripe-payment"
                                        data-key="{{ Config::get('stripe.stripe.public') }}"
                                        data-amount="{{$total*100}}"
                                        data-currency="EUR"
                                        data-name="LASTENHEFT EXPERT"
                                        data-description="{{trans('messages.l_extend')}}"/>
                                    <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                    <script>
                                        $(document).ready(function() {
                                            $('#confirmPay').on('click', function(event) {
                                                validation = true;
                                                if (!$('.package_select').is(':checked')) {
                                                      $('#formConfirmOk')
                                                          .find('#frm_body').html('Please Select a Package')
                                                          .end().modal('show');
                                                    validation *= false;
                                                    event.preventDefault();
                                                }
                                                validation *= checkedFieldCheckbox('terms_and_cond');
                                                if (validation == false) {
                                                    event.preventDefault(); // prevent submitting
                                                    return false;
                                                }
                                                event.preventDefault();

                                            });
                                        });
                                    </script>
                                    </div>
                                    <div class="row panel widget light-widget select-payment-options">
                                      {{-- <div class="col-xs-10 col-xs-offset-2"> --}}
                                        <div class="title_blue">  {{trans('messages.label_payment_method')}}: </div>
                                        <div class="row">
                                            <div class="payment_options">
                                                <div class="card_pay card_pay_image_bw"></div>
                                            </div>
                                            <div class="payment_options">
                                                <div class="paypal_pay paypal_pay_image_bw"></div>
                                            </div>
                                            <div class="payment_options">
                                                <div class="transfer_pay transfer_pay_image_bw"></div>
                                            </div>
                                            <div class="payment_options">
                                                <div class="sofort_pay sofort_pay_image_bw"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                          <div class="col-xs-12 text-center submit-payment-method">
                                            <button class="btn btn-info extend-project-btn text-center" form-submit="">
                                                {{trans('messages.l_extend')}}
                                            </button>
                                          </div>
                                        </div>
                                      {{-- </div> --}}
                                    </div>
                                </div>
                            </div>
                            <br>
                        </form>
                        <div class="row pull-right">
                            {{ Form::open(['action'=>'PaypalPaymentController@extendProjectWithPaypal', 'id'=>'paypal_payment']) }}
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="project_id" value="{{$project->id_project}}">
                                <input type="hidden" name="subpackage_id">
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <div class="task_page_tree" id="task_page_tree">
                        <div class="easy-tree ls-tree-view">
                            <ul id="list" class="ul-tree ">
                                <li id="project_name">
                                <span class="menu_title" id="project{{$project->id_project}}">
                                    <span class="glyphicon glyphicon-minus-sign"></span>
                                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                    <a href="javascript:void(0); ">{{$project->name_project}}</a>
                                </span>
                                    <ul class="sortable_modules_1">
                                        <?php
                                        $has_any_task = printTreeEmptyProject($module_structure, 0, $id_software, $project->id_project, $set_priority, 1, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks);
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                            {{($has_any_task == 1)?' ':'Click on project name to add Module, Submodule and Task'}}
                        </div>
                    </div>
                </div>
            </div>
            <div id="task_page_details_parent">
                <div class="task_page_details" id="task_page_details"></div>
            </div>
        </div>
    </div>
    <div class="iframe-overlay">
    </div>
    @include('admin.alert_box.delete_confirm')
    @include('admin.alert_box.confirm')
@stop
