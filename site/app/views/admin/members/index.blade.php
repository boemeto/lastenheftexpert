@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-users"></i>
            <h3 class="box-title">All users </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <h3><i class="fa fa-user"></i> Users</h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Groups</th>
                    <th></th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->first_name.' '.$user->last_name}} </td>
                        <td>{{$user->username }} </td>
                        <td>{{$user->email }} </td>
                        <td>@foreach($groups as $id_group=> $group)
                                @if($user->fk_group == $id_group)
                                    {{$group}}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <a href="{{URL::route('members.edit',[$user->id])}}"
                               class="btn ls-light-blue-btn btn-flat" title="Edit user">
                                <i class="fa fa-edit"></i>
                            </a>
                            @if($user->blocked == 1)
                                <a href="{{URL::to('members/approve',[$user->id])}}"
                                   class="btn ls-light-blue-btn btn-flat" title="Approve">
                                    <i class="fa fa-check-circle-o "></i>
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
            </table>
            <br>
        </div>
        <!-- /.box-body -->
    </div>
@stop
