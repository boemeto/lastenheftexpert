{{ Form::model($headquarter, ['method'=>'PATCH', 'route'=>['headquarters.update',$headquarter->id_headquarter]])}}

<div class="col-md-12">
    <div class="title_line">  {{trans('messages.act_edit')}}     {{ $headquarter->name_headquarter }}  </div>
</div>
<div class="col-md-12">
    <div class="form-group has-feedback">
        {{ Form::text("name_headquarter",$headquarter->name_headquarter,[ "placeholder"=>"Name Headquarter", "class"=>"form-control" ]) }}
        {{ $errors->first("name_headquarter","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        <div id="locationField">
            {{ Form::text("autocomplete",$headquarter->address_string,array( "placeholder"=>"Enter headquarter address",
            "class"=>"form-control", "id"=>"autocomplete1","onFocus"=>"initialize('1')","required")) }}
            {{ $errors->first("autocomplete","<div class='text-red'>:message</div>") }}
        </div>
    </div>
    <table id="address1" class="table table-bordered table-responsive">
        <tr>
            <td>
                {{ Form::text("street",$headquarter->street,[ "placeholder"=>"Street name", "class"=>"form-control","id"=>"route1","required"]) }}
                {{ $errors->first("street","<div class='text-red'>:message</div>") }}
            </td>
            <td>
                {{ Form::text("street_number",$headquarter->street_nr,[ "placeholder"=>"Street number", "class"=>"form-control","id"=>"street_number1" ]) }}
                {{ $errors->first("street_number","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>
            <td>
                {{ Form::text("city",$headquarter->city,[ "placeholder"=>"City", "class"=>"form-control","id"=>"locality1","required"]) }}
                {{ $errors->first("city","<div class='text-red'>:message</div>") }}
            </td>
            <td>
                {{ Form::text("postal_code",$headquarter->postal_code,[ "placeholder"=>"Postal Code", "class"=>"form-control","id"=>"postal_code1"]) }}
                {{ $errors->first("postal_code","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {{ Form::text("state",$headquarter->state,[ "placeholder"=>"State", "class"=>"form-control","id"=>"administrative_area_level_11" ]) }}
                {{ $errors->first("state","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {{ Form::text("country",$headquarter->country,[ "placeholder"=>"Country", "class"=>"form-control","id"=>"country1","required"]) }}
                {{ $errors->first("country","<div class='text-red'>:message</div>") }}
                {{ Form::hidden("latitude",$headquarter->latitude,[  "id"=>"latitude1" ]) }}
                {{ Form::hidden("longitude",$headquarter->longitude,[ "id"=>"longitude1" ]) }}
            </td>
        </tr>
    </table>
    <div class="form-group has-feedback">
        {{ Form::text("telephone_headquarter",$headquarter->telephone_headquarter,[ "placeholder"=>"Telephone Number", "class"=>"form-control" ]) }}
        {{ $errors->first("telephone_headquarter","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::email("email_headquarter",$headquarter->email_headquarter,[ "placeholder"=>"Email", "class"=>"form-control", "required" ]) }}
        {{ $errors->first("email_headquarter","<div class='text-red'>:message</div>") }}
    </div>
    <div class="input_fields_wrap">
    </div>
    {{ $errors->first("confirm_user",'<div class="text-green">:message</div>') }}
    {{ Form::hidden("id_company",$headquarter->fk_company) }}
    {{ Form::hidden("id_location",$headquarter->fk_location) }}
    {{ Form::hidden("redirect_link",'acompany') }}
    <br>
</div>
<br>
<div class="col-md-6">
    <button type="submit" class='btn ls-light-blue-btn btn-block'>
        {{trans('messages.act_save')}}
    </button>
</div>
<div class="col-md-6">
    <button type="button" class='btn btn-danger btn-block' onclick="hide_modal_box()">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{ Form::close() }}
