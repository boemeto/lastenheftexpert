<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function ($table) {
            $table->bigInteger('id')->increments()->unsigned();
            $table->string('email', 100)->unique();
            $table->string('password', 100);
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->tinyInteger('activated')->nullable();
            $table->integer('blocked')->nullable();
            $table->bigInteger('fk_company')->nullable()->unsigned();
            $table->bigInteger('fk_headquarter')->nullable()->unsigned();
            $table->integer('fk_personal_title')->nullable();
            $table->integer('fk_title')->nullable();
            $table->string('job_user', 200);
            $table->string('language', 10)->default('en');
            $table->text('permissions');
            $table->string('activation_code', 255)->nullable();
            $table->dateTime('activated_at')->nullable();
            $table->string('persist_code', 255)->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('reset_password_code', 255)->nullable();
            $table->rememberToken()->nullable();
            $table->string('social_linkedin', 250)->nullable();
            $table->string('social_twitter', 250)->nullable();
            $table->string('social_xing', 250)->nullable();
            $table->string('telephone_user', 250)->nullable();
            $table->string('mobile_user', 200)->nullable();
            $table->string('logo_user', 200)->nullable();
            $table->integer('is_deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->delete();
    }

}
