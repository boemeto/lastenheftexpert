<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title">{{trans('messages.act_add')}} {{trans('messages.nav_industry')}}</h3>

    <div class="clear"></div>
</div>
{{ Form::open(['route'=>'industry.store'])}}

<div class="modal-body">
    <div class="row pd-10">
        <div class="col-md-12 pd-5">
            <div class="form-group">
                {{ Form::select("fk_type",$industry_types,0,[
                  "class"=>"form-control",
                  "required",
                  'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                  "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first("fk_type",'<div class="text-red">:message</div>') }}
            </div>
        </div>
        <div class="col-md-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_industry', '', [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                {{ $errors->first('name_industry','<div class="alert alert-danger">:message</div>') }}
                <span class="input-group-addon">DE</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_industry', '', ['class'=>"form-control",'placeholder'=>trans('messages.l_description')]) }}
                {{ $errors->first('description_industry','<div class="alert alert-danger">:message</div>') }}
            </div>
        </div>
        <div class="col-md-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_industry_en', '', ['class'=>"form-control",'placeholder'=>trans('messages.l_name')]) }}
                {{ $errors->first('name_industry_en','<div class="alert alert-danger">:message</div>') }}
                <span class="input-group-addon">EN</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_industry_en', '', ['class'=>"form-control",'placeholder'=>trans('messages.l_description')]) }}
                {{ $errors->first('description_industry_en','<div class="alert alert-danger">:message</div>') }}
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class='btn ls-light-blue-btn'>
        {{trans('messages.act_save_close')}}
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}
