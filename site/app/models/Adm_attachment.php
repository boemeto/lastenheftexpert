<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 30-Oct-15
 * Time: 15:11
 */
class Adm_attachment extends Eloquent
{
    public static $messages = [];
    public static $rules = [
        'name_file' => 'required',
    ];
    public $primaryKey = 'id_attachment';
    protected $table = 'adm_attachments';
    protected $guarded = ['id_attachment'];
    protected $fillable = ['name_file', 'fk_task', 'file_type'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getTaskAttachments($id_task)
    {
        $list = Adm_attachment::where('fk_task', '=', $id_task)->get();
        return $list;
    }

    public static function getTaskAttachImages($id_attach)
    {
        $list = Adm_attachment::getFirstById($id_attach);
        $file_types = array('jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG');
        if (in_array($list->file_type, $file_types)) {
            $image = URL::to('/') . '/images/attach_adm_task/' . $list->name_file;
        } else {
            $image = URL::to('/') . '/assets/images/file.png';
        }
        return $image;
    }

    public static function getFirstById($id)
    {
        $list = Adm_attachment::where('id_attachment', '=', $id)->first();
        return $list;
    }

    public static function uploadFile($file, $id_task)
    {
        //      $images = $file->getFilename();
        $file_name = $file->getClientOriginalName();
        $extension = File::extension($file_name);
        $directory = 'images/attach_adm_task';
        $filename = $file_name;

        $upload_success = $file->move($directory, $filename);
        $attachment = Adm_attachment::whereName_file($filename)
            ->where('fk_task', '=', $id_task)
            ->first();
        if (count($attachment) == 0) {
            $project_attach = new Adm_attachment();
            $project_attach->fk_task = $id_task;
            $project_attach->name_file = $filename;
            $project_attach->file_type = $extension;
            $project_attach->save();
        }
        return Redirect::back();
    }
}