@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop


@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-pencil-square-o"></i>

            <h3 class="box-title">  {{trans('messages.edit')}}   {{trans('messages.submodule')}} {{$submodule->name_Software}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">

                {{ Form::model($submodule,['route'=>['submodule.update',$submodule->id_submodule], 'method'=>'PATCH'])}}
                <div class="col-lg-12">

                    <div class="form-group">
                        {{ Form::label('name_submodule','Name: ') }}
                        {{ Form::text('name_submodule', $submodule->name_submodule, ['class'=>"form-control"]) }}
                        {{ $errors->first('name_submodule','<div class="alert alert-danger">:message</div>') }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description_submodule','Description: ') }}
                        {{ Form::textarea('description_submodule', $submodule->description_submodule, ['class'=>"form-control"]) }}
                        {{ $errors->first('description_submodule','<div class="alert alert-danger">:message</div>') }}
                    </div>

                </div>
                <div class="col-lg-6">
                    <button type="submit" class="btn ls-light-blue-btn btn-flat ">
                        <i class="fa fa-pencil-square-o"></i> {{trans('messages.edit')}}
                    </button>

                </div>
                {{ Form::close()}}
                <div class="col-lg-6">
                    {{ Form::open(array('route' => array('submodule.destroy', $submodule->id_submodule), 'method' => 'delete')) }}
                    <button type="submit" class="btn ls-light-blue-btn btn-flat "
                            onclick="return confirm('{{trans('validation.confirmation_question')}}')">
                        <i class="fa fa-trash"></i>
                        {{trans('messages.permanently_delete')}}
                    </button>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>


@stop