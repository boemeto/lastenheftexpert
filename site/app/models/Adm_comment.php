<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:30
 */
class Adm_comment extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_comment' => 'required',
    ];
    public $primaryKey = 'id_comment';
    protected $table = 'adm_comments';
    protected $guarded = ['id_comment'];
    protected $fillable = ['comment', 'fk_user', 'fk_task'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_comment::where('id_comment', '=', $id)->first();
        return $list;
    }

    public function getCommentsByTask($id_task)
    {
        $list = Adm_comment::leftJoin('users', 'id', '=', 'fk_user')
            ->where('fk_task', '=', $id_task)
            ->orderBy('adm_comments.created_at', 'desc')
            ->get();
        return $list;
    }
}