@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop



@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-building-o"></i>

            <h3 class="box-title">API </h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body" style="margin: auto; width: 80%;">
            <table class="table table-responsive">
                <tr>
                    <th>View file</th>
                    <th>Api file</th>
                    <th>Input $_POST</th>
                    <th>Output</th>
                </tr>
                <tr>
                    <td colspan="4"><h3>1. Login User </h3></td>
                </tr>
                <tr>
                    <td><a href="views/loginUser">Login</a> <br/></td>
                    <td><a href="{{ URL::to('api/loginUser') }}" target="_blank">Login</a>
                    </td>
                    <td>
                        <ul>
                            <li>name_device</li>
                            <li>code_device - id-ul ala lung al telefonului</li>
                            <li>gcm_id</li>
                            <li>apple_id</li>
                            <li>email</li>
                            <li>password</li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>status: 1 (if ok)</li>
                            <li>error_message: null (string if not ok)</li>
                            <li> login:
                                <ul>
                                    <li>id_user</li>
                                    <li>id_device</li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><h3>2. Get user </h3></td>
                </tr>
                <tr>
                    <td><a href="views/getUser"> Get user</a> <br/></td>
                    <td><a href="{{ URL::to('api/getUser') }}" target="_blank"> Get user</a>
                    </td>
                    <td>
                        <ul>
                            <li>id_user</li>

                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>status: 1 (if ok)</li>
                            <li>error_message: null (string if not ok)</li>
                            <li> user:
                                <ul>

                                    <li>id_user</li>
                                    <li>first_name</li>
                                    <li>last_name</li>
                                    <li>username</li>
                                    <li>email</li>
                                    <li>language</li>
                                    <li>telephone_user</li>
                                    <li>mobile_user</li>
                                    <li>fk_personal_title</li>
                                    <li>fk_title</li>
                                    <li>fk_company</li>
                                    <li>fk_headquarter</li>
                                    <li>fk_group</li>
                                    <li>job_user</li>
                                    <li>logo_user</li>
                                    <li>social_xing</li>
                                    <li>social_linkedin</li>
                                    <li>social_twitter</li>
                                    <li>activated</li>
                                    <li>blocked</li>
                                    <li>is_deleted</li>

                                </ul>
                            </li>

                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><h3>3. Get company </h3></td>
                </tr>
                <tr>
                    <td><a href="views/getCompany"> Get company</a> <br/></td>
                    <td><a href="{{ URL::to('api/getCompany') }}" target="_blank"> Get company</a>
                    </td>
                    <td>
                        <ul>
                            <li>id_user</li>

                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>status: 1 (if ok)</li>
                            <li>error_message: null (string if not ok)</li>
                            <li> company:
                                <ul>
                                    <li>id_company</li>
                                    <li>name_company</li>
                                    <li>cui_company</li>
                                    <li>website_company</li>
                                    <li>email_company</li>
                                </ul>
                            </li>
                            <li> headquarter:
                                <ul>
                                    <li>id_headquarter</li>
                                    <li>name_headquarter</li>
                                    <li>email_headquarter</li>
                                    <li>telephone_headquarter</li>
                                    <li>street_nr</li>
                                    <li>street</li>
                                    <li>city</li>
                                    <li>state</li>
                                    <li>country</li>
                                    <li>postal_code</li>
                                    <li>latitude</li>
                                    <li>longitude</li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>


            </table>
        </div>
    </div>
@stop