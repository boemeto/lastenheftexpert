@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/pages/sampleForm.js')}}"></script>
@stop
@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">


                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="{{Url::to('/')}}"><i class="fa fa-home"></i></a></li>
                        <li class="active">Profile</li>
                    </ol>
                    <!--Top breadcrumb start -->

                </div>
                <div class="col-md-12 " style="padding: 0 0 0 5px; margin: 0">
                    <br>

                    <!-- Details  About the Company -->
                    <!--User Details Start-->
                    <div class="ls-user-details">
                        <div class="row">
                            <!--Company Details start-->
                            <div class="user-detail col-md-2">
                                <div class="ls-user-position">
                                    {{--<a href="javascript:void(0);" data-toggle="modal"--}}
                                    {{--data-target="#myModalFriends">Edit</a>--}}
                                </div>
                                <address>
                                </address>
                                <div class="user-pic">
                                    <a href="javascript:void(0);" data-toggle="modal"
                                       data-target="#modalComapaniesForm" title="Edit Company">
                                        <?php $image = User::getLogoUser($user->id);
                                        ?>
                                        <img src="{{ URL::asset($image)}}"
                                             alt="image">
                                    </a>
                                </div>
                            </div>
                            <div class="ls-user-info  col-md-10">
                                <div class="ls-user-text-border_white">
                                    <div class="ls-user-text-border_grey">
                                        <div class="ls-user-text">
                                            <h1>{{ $user->first_name }} {{ $user->last_name }}</h1>

                                            <p>
                                                <i class=" fa fa-envelope-o"></i> Email: {{$user->email}}<br>
                                                @if($user->telephone_user)
                                                    <i class=" fa fa-phone"></i> {{$user->telephone_user}}<br>
                                                @endif
                                                @if($user->mobile_user)
                                                    <i class=" fa fa-mobile"></i> {{$user->mobile_user}}<br>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="ls-user-text2 ">
                                </div>
                                <div class="ls-user-text3 ">
                                </div>
                            </div>
                            <!--Company Details Finish-->
                        </div>
                    </div>
                    <!--User Details Finish-->
                </div>
                <div class="col-md-12">
                    <div class="user-profile-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  nav-justified icon-tab">
                            <li class="active"><a href="#editprofile" data-toggle="tab">
                                    <i class="fa fa-users"></i> <span>Edit Profile</span></a></li>
                            <li class=""><a href="#editpassword" class="user-google-location" data-toggle="tab">
                                    <i class="fa fa-key"></i> <span>Edit Password</span></a></li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <!-- Profile Tab Start -->
                            <div class="tab-pane fade active in" id="editprofile">

                                <div class="row">
                                    <div class="col-lg-3">


                                        {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id],'enctype'=>"multipart/form-data"])}}
                                        <div class="form-group" style="text-align: center">
                                            @foreach($personal_titles as $key=>$value)
                                                {{Form::radio('fk_personal_title', $key, false,['class'=>"icheck-square-green",'id'=>'fk_personal_title'])}} {{$value}}
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            @endforeach

                                        </div>
                                        <div class="form-group">
                                            <input id="file-user"
                                                   type="file"
                                                   name="logo_user"
                                                   multiple="flase">
                                        </div>

                                        <div class="form-group">
                                            {{ Form::select("fk_title",$titles,$user->fk_title,[ "placeholder"=>"Name", "class"=>"form-control",'id'=>'fk_title', "required"]) }}
                                            {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}

                                        </div>
                                        <div class="form-group  has-feedback">
                                            {{ Form::text("first_name",$user->first_name,[ "placeholder"=>"First Name", "class"=>"form-control",'id'=>'first_name', "required"]) }}
                                            {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        </div>
                                        <div class="form-group  has-feedback">
                                            {{ Form::text("last_name",$user->last_name,[ "placeholder"=>"Last Name", "class"=>"form-control",'id'=>'last_name', "required"]) }}
                                            {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        </div>
                                        <div class="form-group  has-feedback">
                                            {{ Form::email("email",$user->email,["placeholder"=>"Email", "class"=>"form-control", 'id'=>'email',"readonly"]) }}
                                            {{ $errors->first("email",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>

                                        <div class="form-group">
                                            {{ Form::text("job_user",$user->job_user ,[ "placeholder"=>"Occupation", "class"=>"form-control",
                                            'id'=>'job_user']) }}
                                            {{ $errors->first("job_user",'<div class="text-red">:message</div>') }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::text("mobile_user",$user->mobile_user ,[
                                            "placeholder"=>"Mobile phone", "class"=>"form-control",
                                            'id'=>'mobile_user']) }}
                                            {{ $errors->first("mobile_user",'<div class="text-red">:message</div>') }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::text("telephone_user",$user->telephone_user ,[
                                            "placeholder"=>"Telephone", "class"=>"form-control",
                                            'id'=>'telephone_user']) }}
                                            {{ $errors->first("telephone_user",'<div class="text-red">:message</div>') }}
                                        </div>
                                        {{Form::hidden('redirect_link','profile')}}


                                        {{Form::submit('Save',['class'=>"btn ls-light-green-btn ladda-button"])}}


                                        {{ Form::close(); }}
                                                <!-- /.col -->
                                    </div>

                                    <!-- User Edit Form End -->
                                </div>
                            </div>


                            <!-- Profile Tab End -->

                            <!-- Password Tab Start -->
                            <div class="tab-pane fade" id="editpassword">
                                <div class="row">
                                    <div class="col-lg-3">
                                        {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id]])}}
                                        <div class="form-group has-feedback">
                                            {{ Form::password("password_old",[ "placeholder"=>"Old Password", "class"=>"form-control",'id'=>'password_old', "required"]) }}
                                            {{ $errors->first("password_old",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="form-group has-feedback">
                                            {{ Form::password("password",[ "placeholder"=>"New Password", "class"=>"form-control",'id'=>'password', "required"]) }}
                                            {{ $errors->first("password",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="form-group has-feedback">
                                            {{ Form::password("password_confirm",[ "placeholder"=>"Password confirm", "class"=>"form-control",'id'=>'password_confirm', "required"]) }}
                                            {{ $errors->first("password_confirm",'<div class="text-red">:message</div>') }}
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>

                                        <input type="hidden" name="redirect_link" value="profile">
                                        {{ Form::submit("Change Password",["class" => "btn ls-light-green-btn  btn-flat" ]) }}

                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                            <!-- Password Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop