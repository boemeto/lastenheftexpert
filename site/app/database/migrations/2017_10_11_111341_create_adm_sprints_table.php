<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmSprintsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_sprints', function ($table) {
            $table->increments('id_sprint')->unsigned();
            $table->string('name_sprint', 250);
            $table->integer('fk_release');
            $table->text('comment');
            $table->tinyInteger('is_closed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_sprints');
    }
}