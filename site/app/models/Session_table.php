<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 24-Nov-15
 * Time: 14:38
 */
class Session_table extends Eloquent
{


    public static $rules = [
        'user_id' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id';
    protected $table = 'sessions';
    protected $guarded = ['id'];
    protected $fillable = ['payload', 'last_activity', 'user_id'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Session::where('id', '=', $id)->first();
        return $list;
    }

    public static function getFirstByUser($id)
    {
        $list = Session::where('user_id', '=', $id)->first();
        return $list;
    }
}