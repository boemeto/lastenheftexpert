<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title text-center">{{trans('messages.act_edit')}} {{trans('messages.nav_posts_pages')}}</h3>
    <div class="clear"></div>
</div>
{{ Form::model($post,['route'=>['packs.update',$list->id_package], 'method'=>'PATCH'])}}
<div class="modal-body">
    <div class="row pd-10">
        <div class="col-xs-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_package',  $list->getAttribute('name_package','de'), [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">DE</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description', $list->getAttribute('description','de'), ['class'=>"summernote form-control",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
        <div class="col-xs-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_package_en', $list->getAttribute('name_package','en'), [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.l_name'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
                <span class="input-group-addon">EN</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('description_en', $list->getAttribute('description','en'), ['class'=>"form-control summernote",'placeholder'=>trans('messages.l_description')]) }}
            </div>
        </div>
    </div>
    <div class=" pd-10">
        <div class="row">
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_price')}}</label>
                {{ Form::text('price',$list->price, [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.label_price'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
            </div>
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_color')}}</label>
                {{-- {{ Form::text('color', $list->color, ['class'=>"form-control",'placeholder'=>trans('messages.label_color')]) }} --}}
                <input class="form-control color package_color colorPicker" type="color" name="color" value="{{$list->color}}">
            </div>
        </div>
        <div class="row  ">
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_period')}}</label>
                {{ Form::number('period', $list->period, [
                    'class'=>"form-control",
                    'placeholder'=>trans('messages.label_period'),
                    'required',
                    'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                    "oninput"=>"this.setCustomValidity('')"
                  ]) }}
            </div>
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_period_type')}}</label>
                {{ Form::select("period_type",$period_type,$list->period_type,[   "class"=>"form-control"  ]) }}
            </div>
        </div>
        <div class="row  ">
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_project_type')}}</label>
                {{ Form::select("fk_type_project",$project_type,$list->fk_type_project,[   "class"=>"form-control"  ]) }}

            </div>
            <div class="col-xs-6 pd-5">
                <label>{{trans('messages.label_is_active')}}</label>
                {{ Form::select("is_active",$active,$list->is_active,[   "class"=>"form-control"  ]) }}

            </div>
        </div>
    </div>
    <div class="modal-header task-label-white">
        <h3 class="box-title text-center">{{trans('messages.act_edit')}} Subpackages</h3>
        <div class="clear"></div>
    </div>
    <div class="row pd-10">
      @foreach($subpackages as $subpackage)
          <div class="col-sm-12 pd-5 subpackage_item">
              <div class="col-sm-1 pd-5">
                  Subpackage Name:
              </div>
              <div class="col-sm-3 pd-5">
                  {{ Form::text('subpackage_name['. $subpackage->subpackage_type_id . ']', $subpackage->subpackage_name, [
                      'class'=>"form-control",
                      'placeholder'=> $subpackage->subpackage_name,
                      'required',
                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                      "oninput"=>"this.setCustomValidity('')"
                    ]) }}
              </div>
              <div class="col-sm-1 pd-5">
                  Period (In Months):
              </div>
              <div class="col-sm-3 pd-5">
                  {{ Form::number('subpackage_period['. $subpackage->subpackage_type_id . ']', $subpackage->subpackage_period, [
                      'class'=>"form-control",
                      'placeholder'=> $subpackage->subpackage_period,
                      'required',
                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                      "oninput"=>"this.setCustomValidity('')"
                    ]) }}
              </div>
              <div class="col-sm-1 pd-5">
                  Set Price:
              </div>
              <div class="col-sm-3 pd-5">
                  {{ Form::number('subpackage_price[' . $subpackage->subpackage_type_id . ']', $subpackage->subpackage_price, [
                      'class'=>"form-control",
                      'placeholder'=>$subpackage->subpackage_price,
                      'required',
                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                      "oninput"=>"this.setCustomValidity('')"
                    ]) }}
              </div>
          </div>
      @endforeach
    </div>
    @if($subpackages[0] == null)
        @foreach($package_types as $package_type)
            <div class="col-sm-12 pd-5 subpackage_item">
                <div class="col-sm-1 pd-5">
                    Subpackage Name:
                </div>
                <div class="col-sm-3 pd-5">
                    {{ Form::text('subpackage_name[' . $package_type->id_package_type.']', '', [
                        'class'=>"form-control",
                        'placeholder'=> $package_type->package_type_name,
                        'required',
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                </div>
                <div class="col-sm-1 pd-5">
                    Period (In Months):
                </div>
                <div class="col-sm-3 pd-5">
                    {{ Form::number('subpackage_period[' . $package_type->id_package_type.']', '', [
                        'class'=>"form-control",
                        'placeholder'=>trans('messages.label_period') . ' for ' .  $package_type->package_type_name,
                        'required',
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                </div>
                <div class="col-sm-1 pd-5">
                    Set Price:
                </div>
                <div class="col-sm-3 pd-5">
                    {{ Form::number('subpackage_price[' . $package_type->id_package_type.']', '', [
                        'class'=>"form-control",
                        'placeholder'=>$package_type->package_type_name,
                        'required',
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                </div>
            </div>
        @endforeach
    @endif
    <br>
</div>
<div class="modal-footer">
    <button type="submit" class='btn ls-light-blue-btn'>
        {{trans('messages.act_save_close')}}
    </button>
    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}
