@foreach($module_children as $module2)

    <ul>
        <li>{{$module2->id_name}}
            @if(Module_structure::hasChild($module2->id_module_structure) == 1)
                <ul>
                    <?php  $module_structure_3 = Module_structure::getModuleByLvl($fk_software, 3, $module2->id_module_structure, 0);?>
                    @foreach($module_structure_3 as $module3)
                        <li>{{$module3->id_name}}
                            @if(Module_structure::hasChild($module3->id_module_structure) == 1)
                                <ul>
                                    <?php  $module_structure_4 = Module_structure::getModuleByLvl($fk_software, 4, $module3->id_module_structure, 0); ?>
                                    @foreach($module_structure_4 as $module4)
                                        <li>{{$module4->id_name}}
                                            @if(Module_structure::hasChild($module4->id_module_structure) == 1)
                                                <ul>
                                                    <?php  $module_structure_5 = Module_structure::getModuleByLvl($fk_software, 4, $module4->id_module_structure, 0); ?>
                                                    @foreach($module_structure_5 as $module5)
                                                        <li>{{$module5->id_name}}
                                                            {{Form::checkbox('fk_module_structure[]',$module5->id_module_structure)}}   </li>
                                                    @endforeach
                                                </ul>
                                            @else
                                                {{Form::checkbox('fk_module_structure[]',$module4->id_module_structure)}}
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                {{Form::checkbox('fk_module_structure[]',$module3->id_module_structure)}}
                            @endif
                        </li>
                    @endforeach
                </ul>
            @else
                {{Form::checkbox('fk_module_structure[]',$module2->id_module_structure)}}
            @endif
        </li>
    </ul>
@endforeach
 