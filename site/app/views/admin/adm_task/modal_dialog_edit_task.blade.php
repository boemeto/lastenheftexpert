<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    {{--<i class="fa fa-times"></i>--}}
    x
</button>
{{ Form::model($task,['route'=>['adm_task.update',$task->id_task], 'method'=>'PATCH', 'enctype'=>"multipart/form-data"])}}
<div class="modal-header task-label-white">
    Aufgabe bearbeiten
    <div class="clear"></div>
</div>

<div class="modal-body show_textarea">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{Form::text('name_task',$task->name_task,['class'=>'form-control','placeholder'=>'Aufgabenbezeichnung'])}}
            </div>
            <div class="form-group">
                {{Form::textarea('notes_task',$task->notes_task,['class'=>'form-control summernote'])}}
                {{Form::hidden('fk_sprint',$fk_sprint)}}
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-exclamation"></i> </span>
                        {{Form::select('fk_priority',$priorities,$task->fk_priority,['class'=>'form-control', 'required'])}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
                        <input name="due_date" type="text" id="sandbox-container" class="form-control" value="{{date('d.m.Y',strtotime($task->due_date))}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-eur"></i> </span>
                        {{Form::text('cost_task',$task->cost_task,['class'=>'form-control','placeholder'=>'Kosten für die Aufgabe'  ])}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group ls-group-input">
                        <span class="input-group-addon"><i class="fa fa-check-square-o"></i> </span>
                        {{Form::select('fk_status',$status,$task->fk_status,['class'=>'form-control'])}}
                    </div>
                </div>

            </div>
            <br>

            <div class="adm_task_attachments">

                <div class="input-group">
                <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
               Dateien vom Computer anhängen:
                    <input type="file" name="task_attachment[]" multiple>
                </span>

                </span>
                    <input type="text" class="form-control" readonly style="width: 200px">
                </div>

            </div>
            <br>

            <div class=" row">


                @foreach($users as $user)
                    <?php    $image_user = User::getLogoUser($user->id); ?>
                    <div class="col-md-3">
                        <table>
                            <tr>
                                <td><img class="img-circle" src="{{ URL::asset($image_user)}}" title="{{$user->first_name.' '.$user->last_name}}">
                                </td>
                            </tr>
                            <tr>
                                <td><br>

                                    @if (in_array($user->id, $responsible))

                                        {{ Form::checkbox('fk_responsible[]',$user->id,true,['class'=>'switchCheckBox','data-size'=>"mini"]) }}

                                    @else

                                        {{ Form::checkbox('fk_responsible[]',$user->id,false,['class'=>'switchCheckBox','data-size'=>"mini"]) }}

                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                @endforeach
            </div>
            <br>
        </div>
    </div>
</div>
<div class="modal-footer">
    {{Form::submit('Speichern und schließen',['class'=>'btn ls-light-blue-btn'])}}

    <button type="button" class="btn btn-danger" data-dismiss="modal">
        Abbrechen
    </button>
</div>


{{Form::close()}}
