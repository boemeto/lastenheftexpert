<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\"/>
    {{--    <link href="{{ URL::asset('assets/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">--}}
    <style type="text/css">
        body {
            width: 700px;
            margin-left: 50px;
            margin-right: 80px;
            display: block;

        }

        html, body {
            font-family: 'Armata', sans-serif;
            font-size: 12px;
        }

        h2, h3 {
            font-weight: 500;
        }

        h1 {
            font-weight: bold;
            font-size: 26px;
        }

        a {
            text-decoration: none;
            color: #555;
        }

        div.page {
            page-break-after: always;
            page-break-inside: avoid;
        }

        .row {
            display: block;
            width: 700px;
            vertical-align: top;
        }

        .col-xs-4 {
            width: 231px;
            vertical-align: top;
            display: inline-block;
        }

        .company-detail {
            position: relative;
            margin-top: 300px;
            margin-bottom: 300px;
            display: block;
            height: 10px;
            background: #fff;
            width: 100%;
        }

        .col-grey {
            position: absolute;
            display: inline-block;
            width: 60%;
            height: 110px;
            background: #dfdfdf;
            top: 0px;
        }

        .col-grey h1 {
            margin-left: 20px;
        }

        .col-dark-grey {
            position: absolute;

            display: inline-block;
            width: 38%;
            background: #58595B;
            height: 110px;
            top: 0;
            right: 0;
        }

        .company-image {
            position: absolute;
            top: -34px;
            right: 120px;
            width: 168px;
            height: 168px;
            border: 6px solid #fff;
            background: #fff;
            border-radius: 86px;
            z-index: 500;
            overflow: hidden;
            vertical-align: middle;
            text-align: center;
        }

        .company-image img {
            margin-top: 1px;
            margin-left: 1px;
            max-width: 100%;
            width: 160px;
            height: 160px;
            border: 3px solid #DFDFDF;
            border-radius: 80px;
            background-color: transparent;
        }

        .project-details {
            width: 100%;
        }

        .right-logo {
            text-align: left;
            position: relative;
            display: block;
            height: 50px;
            background: #fff;
            width: 100%;
            margin-bottom: 20px;
        }

        .logo-col-white {
            display: block;
            width: 80%;
            background: #fff;
            height: 50px;
            position: absolute;
        }

        .logo-col-grey {
            display: block;
            width: 15%;
            background: #58595B;
            height: 50px;
            position: absolute;
            top: 0;
            right: 0;
        }

        .logo-image {
            position: absolute;
            top: -15px;
            right: 65px;
            width: 76px;
            height: 76px;
            border: 3px solid #fff;
            background: #fff;
            border-radius: 38px;
            z-index: 500;
            overflow: hidden;
            vertical-align: middle;
            text-align: center;
        }

        .logo-image img {

            max-width: 100%;
            width: 70px;
            height: 70px;
            border: 3px solid #DFDFDF;
            border-radius: 35px;
            background-color: transparent;
        }

        .industry-text {
            margin-top: 30px;
            padding-bottom: 5px;
            display: block;
            border-bottom: 1px solid #CDCDCD;
            width: 100%;
            color: #5BC0DE;
        }

        .industry-type {
            margin-top: 20px;
            display: inline-block;
            height: 25px;
            padding: 10px;
            width: 260px;
            line-height: 25px;
            vertical-align: middle;
            background: #FE6C6C;
            border-radius: 10px;
            color: #fff;
            margin-right: 20px;
        }

        .industry-name {
            margin-top: 20px;
            display: inline-block;
            height: 25px;
            padding: 10px;
            line-height: 25px;
            width: 260px;
            border: 1px solid #CDCDCD;
            vertical-align: middle;
            border-radius: 10px;
        }

        .project_employees_box {
            display: inline-block;
            width: 120px;
            margin-top: 20px;
            margin-right: 10px;
            text-align: center;
            position: relative;
            height: 175px;
            margin-bottom: 20px;
        }

        .project_employees_circle {

            display: block;
            background: #ffffff;
            width: 90px;
            height: 90px;
            border: 4px solid #5BC0DE;
            vertical-align: middle;
            border-radius: 47px;
            text-align: center;
            font-size: 23px;
            color: #5BC0DE;
            line-height: 100px;
            font-weight: bold;
            margin-left: 10px;

        }

        .red {
            border-color: #FF7878;
            color: #FF7878;
        }

        .dark_grey {
            border-color: #7A8B83;
            color: #7A8B83;
        }

        img.img-circle {
            margin: 5px;
            height: 70px;
            width: 70px;
            border-radius: 35px;
            border: 1px solid #7A8B83;
        }

        .div_headquarters {
            margin-bottom: 20px;
            margin-top: 20px;
            display: block;
            width: inherit;
        }

        .headquarter_image {
            display: inline-block;
            width: 70px;
            text-align: center;
            /*border: 1px solid red;*/
        }

        .headquarter_image img {
            margin-top: 10px;
        }

        .headquarter_details {
            display: inline-block;
            font-size: 12px;
            color: #7A8B83;
            padding-left: 10px;
            /*width: 20%;*/
            /*border: 1px solid red;*/

        }

        h3.headquarter_head {
            color: #222;
            margin-bottom: 5px;
            margin-top: 10px;
        }

        .headquarter_users {
            display: inline-block;
            font-size: 12px;
            /*border: 1px solid red;*/
            width: 65%;
            padding-top: 10px;
        }

        .headquarter_users_details {

            display: inline-block;
            vertical-align: top;
        }

        .users_image {
            display: inline-block;
            width: 65px;
            /*border: 1px solid red;*/

        }

        .users_image img {
            width: 60px;
            height: 60px;
            border-radius: 30px;
        }

        .users_details {
            display: inline-block;
            padding-top: 10px;
            padding-left: 10px;
            color: #7A8B83;
            vertical-align: top;
            /*border: 1px solid red;*/
        }

        ol {
            font-size: 14px;
            list-style: none;
            -webkit-padding-start: 10px !important;
            margin-left: -30px;
        }

        li {
            display: block;
            margin-left: 0px;
            text-align: justify;
            font-size: 15px;
        }

        .menu_index a {
            text-decoration: none;
            color: #333;
        }

        ol.menu_index {
            font-size: 14px;
            counter-reset: roman;
            color: #333;
            -webkit-padding-start: 10px !important;
        }

        ol.menu_index > li:before {
            counter-increment: roman;
            content: counter(roman) ".";
            padding-right: 5px;
        }

        ol.menu_index li {
            /*position: relative;*/
            font-size: 15px;
            text-align: justify;
        }

        ol.menu_index li ol {
            counter-reset: inner;
        }

        ol.menu_index ol li:before {
            counter-increment: inner;
            content: counter(roman) "." counters(inner, '.');
            padding-right: 5px;
        }

        .circle_task {
            height: 10px;
            width: 10px;
            background-color: transparent;
            display: inline-block;
            border-radius: 5px;
            margin-top: 7px;
            position: relative;
            right: 30px;
        }

        .description_task {

            display: block;
            margin: 10px 0;
            text-align: justify;
        }

        .description_task hr {
            color: #e2e2e2;
        }

        .task-div img {
            margin-right: 10px;
        }

        .task-div {
            display: block;
            width: 100%;
            /*border: 1px solid red;*/
            font-size: 12px;
            position: relative;
            top: 0;
            left: -70px;
            margin-left: 40px;
            margin-bottom: 10px;
            padding: 10px;
            page-break-inside: avoid;
        }

        .task-div h3 {

            display: block;
            font-size: 15px;
            padding: 5px 20px;
            margin: 10px 0;
            position: relative;
            text-align: justify;
        }
        .bullet { width: 15px; height: 15px; border-radius: 50%; background-color: red; display: inline-block; }
    </style>

</head>

<body>
<?php $image = Company::getLogoCompany($company->id_company) ?>
<div class="page" id="cover-page">


    <div class="company-detail">
        <div class="col-grey">

            <h1 style="padding-top: 20px; font-weight: bold">{{ $company->name_company }}</h1>


        </div>
        <div class="company-image">
            <img src="{{ URL::asset($image)}}" alt="image">
            </a>
        </div>
        <div class="col-dark-grey">

        </div>
    </div>
    <div class="project-details">
        <h2 style="text-align: center; line-height: 12px">{{$project->name_project}}</h2>

        <p style="text-align: center;line-height: 10px">{{trans('messages.label_category')}}: {{$industry_type->name_industry_type}}        </p>

        <p style="text-align: center;line-height: 10px">{{trans('messages.label_industry')}}: {{$industry->name_industry}}</p>

    </div>
    <p>&nbsp;</p>
    <p style="line-height: 10px">{{trans('messages.label_date')}}: {{date('d.m.Y')}}</p>

    <p style="line-height: 10px">{{trans('messages.label_author')}}: {{$user->first_name}} {{$user->last_name}}</p>
</div>

<div class="page" id="manu-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3>{{trans('messages.label_toc')}}</h3>
        </div>
        <div class=" logo-col-grey"></div>
    </div>


    <div style="margin-left:100px;">
        <?php
        Module_structure::printTreeWordContentsEmptyProject($module_structure, $id_software, $id_industry, 1, $id_project) ?>
    </div>
</div>
<div class=" " id="tasks-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3></h3>
        </div>
        <div class=" logo-col-grey"></div>
    </div>
    <?php  Module_structure::printTreeWordDetailsEmptyProject($module_structure, $id_software, $id_industry, 1, $id_project) ?>
</div>
</body>
</html>

 
     