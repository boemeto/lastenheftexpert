<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 22-Feb-16
 * Time: 11:10
 */
class Software_attach extends Eloquent
{
    public static $rules = [
        'fk_module_structure' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_software_attachment';
    protected $table = 'pr_software_attachments';
    protected $guarded = ['id_software_attachment'];
    protected $fillable = ['name_attachment', 'ext_attachment', 'folder_attachment', 'fk_ms_task'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Software_attach::where('id_software_attachment', '=', $id)->first();
        return $list;
    }

    public static function getAttachmentByMS_task($fk_ms_task)
    {
        $attachments = Software_attach::join('pr_ms_tasks', 'id_ms_task', '=', 'fk_ms_task')
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->get();
        return $attachments;
    }

    public static function getAttachmentsNamesByMS_task($fk_ms_task)
    {
        $attachments = Software_attach::select('pr_software_attachments.name_attachment')
            ->join('pr_ms_tasks', 'id_ms_task', '=', 'fk_ms_task')
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->get();
        return $attachments;
    }

    public static function getTasksWithAttachments($id)
    {
        $attachments = Software_attach::join('pr_ms_tasks', 'id_ms_task', '=', 'fk_ms_task')
            ->where('fk_module_structure', '=', $id)
            ->get();
        foreach ($attachments as $attachment) {
            $array[] = $attachment->fk_ms_task;
        }
        // dd($array);
        return array_unique($array);
    }

    public static function getIconByAttachment($extension)
    {
        $extension = strtolower($extension);
        $path = base_path() . '/images';
        $url = URL::asset('images');
        $file_path = $path . "/icons/" . $extension . "/" . $extension . "-48_32.png";
        if (file_exists($file_path)) {
            $file = $url . "/icons/" . $extension . "/" . $extension . "-48_32.png";
            $icon = "url('" . $file . "');";
        } else {
            $file = $url . "/icons/text/text-48_32.png";
            $icon = "url('" . $file . "');";
        }
        return $icon;
    }
}
