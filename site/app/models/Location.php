<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 18.05.2015
 * Time: 14:37
 */
class Location extends Eloquent
{


    public static $rules = [
        'country' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_location';
    protected $table = 'locations';
    protected $guarded = ['id_location'];
    protected $fillable = ['street_nr', 'street',
        'city', 'state', 'country',
        'postal_code', 'latitude', 'longitude', 'address_string'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Location::whereId_location($id)->first();
        return $list;
    }

}