<?php
if (Auth::check()) {
    $id_user = Auth::user()->id;
    $fk_group = Auth::user()->fk_group;
}
?>

<li>
    <a href="{{URL::to('admin_home')}}">
        <i class="fa fa-home"></i> <span>{{trans('messages.nav_dashboard')}}</span>
    </a>
</li>

<li>
    <a href="#">
        <i class="fa fa-list"></i> <span>{{trans('messages.nav_checklists_group')}}</span> </a>
    <ul style="display: block;">
        <li>
            <a href="{{URL::to('software')}}"><i class="fa fa-gear "></i> <span>{{trans('messages.nav_software')}}</span></a>
        </li>
        <li>
            <a href="{{URL::to('industry')}}"><i class="fa fa-industry "></i> <span>{{trans('messages.nav_industries')}}</span></a>
        </li>
        <li>
            <a href="{{URL::to('task')}}"><i class="fa fa-tasks "></i> <span>{{trans('messages.nav_tasks')}}</span></a>
        </li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-sitemap"></i> <span>{{trans('messages.nav_projects_group')}}</span> </a>
    <ul style="display: block;">
        <li>
            <a href="{{URL::to('projekte')}}"> <i class="fa fa-sitemap"></i> <span>{{trans('messages.nav_projects')}}</span></a>
        </li>
        <li>
            <a href="{{URL::to('packs')}}"> <i class="fa fa-gift"></i> <span>{{trans('messages.nav_packages')}}</span></a>
        </li>
        <li><a href="{{URL::to('invoices_a')}}"> <i class="fa fa-file-text-o"></i> <span>{{trans('messages.nav_invoices')}}</span> </a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-building"></i> <span>{{trans('messages.nav_company_group')}}</span> </a>
    <ul style="display: block;">

        <li>
            <a href="{{URL::to('acompany')}}"> <i class="fa fa-building-o"></i> <span>{{trans('messages.nav_companies')}}</span></a>
        </li>
        <li>
            <a href="{{URL::to('acompany/1')}}"> <i class="fa fa-building"></i> <span>  {{trans('messages.nav_admin_company')}}</span></a>
        </li>
        <li>
            <a href="{{URL::to('consultant')}}"> <i class="fa fa-user-secret"></i><span> {{trans('messages.nav_consultants')}}</span></a></li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-cog"></i> <span>{{trans('messages.nav_settings')}}</span> </a>
    <ul style="display: block;">
        <li>
            <a href="{{URL::to('posts')}}"> <i class="fa fa-file-text-o"></i><span> {{trans('messages.nav_posts_pages')}}</span></a></li>
        <li>
            <a href="{{URL::to('lang/1')}}"> <i class="fa fa-language"></i><span> {{trans('messages.language')}}</span></a></li>
        <li>
            <a href="{{URL::to('lang/2')}}"> <i class="fa fa-check-circle-o "></i><span> {{trans('messages.label_validations')}}</span></a></li>
        {{--<li>--}}
        {{--            <a href="{{URL::to('adm_task/1')}}"> <i class="fa fa-history"></i><span> {{trans('messages.nav_task_manager')}}</span> </a></li>--}}

    </ul>
</li>

<li><a href="{{ URL::to('logout') }}">
        <i class="fa  fa-sign-out"></i> <span>{{trans('messages.nav_logout')}} </span></a></li>
