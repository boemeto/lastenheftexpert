<?php


/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:38
 */
class Module extends Eloquent
{


    public static $rules = [
        'name_module' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_module';
    protected $table = 'pr_modules';
    protected $guarded = ['id_module'];
    protected $fillable = ['code_module', 'name_module', 'name_module_en', 'description_module', 'description_module_en', 'meta_tags', 'address_code', 'is_default', 'fk_company'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getModuleList($fk_software = null)
    {
        $list = array();
        $list[0] = 'Select';
        if ($fk_software == null) {
            $modules = Module::all();
        } else {
            $modules = DB::table('pr_modules')
                ->whereRaw("id_module in 
                (SELECT DISTINCT fk_id from pr_module_structure where id_type = 1
                and fk_software = " . $fk_software . ")")->get();

        }
        foreach ($modules as $row) {
            $list[$row->id_module] = $row->name_module;
        }
        return $list;
    }

    public static function getNameByMS($fk_id, $id_type)
    {
        if ($id_type == 1) {
            $module = Module::getFirstById($fk_id);
            $name = $module->getAttribute('name_module');

        } else {
            $module = Submodule::getFirstById($fk_id);
            $name = $module->getAttribute('name_submodule');
        }

        return $name;
    }

    public static function getFirstById($id)
    {
        $list = Module::whereId_module($id)->first();
        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_modules', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_modules', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }

}