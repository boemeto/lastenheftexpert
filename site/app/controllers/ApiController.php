<?php

class ApiController extends \BaseController
{
// Function for the main view, where you can access the api helper
    public function index()
    {
        return View::make('api/index');
    }
// View helper
// Includes all forms to test the API Functions
    public function view_help($view_name)
    {

        return View::make('api/view_help', compact('list_types', 'users', 'existing_lists'))->with('view_name', $view_name);
    }

// API for user login
    public function loginUser()
    {
        // input data about the device
        $name_device = Input::get('name_device'); // name of the device
        $code_device = Input::get('code_device'); // device id

        // the key for google push notification
        if (Input::has('gcm_id')) {
            $gcm_id = Input::get('gcm_id');
        } else {
            $gcm_id = '';
        }
        // the key for apple push notifications
        if (Input::has('apple_id')) {
            $apple_id = Input::get('apple_id');
        } else {
            $apple_id = '';
        }
        // user data: username and pass

        $user_data = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        // login attempt
        if (Auth::attempt($user_data)) {
            $user = Auth::user();
        } else {
            $json_string = array('status' => 0, 'error_message' => 'Wrong username or password');
            // return with error
            return Response::json($json_string);
        }
        // get user id
        $id_user = $user->id;
        // find device by the unique code
        $device = Device::where('code_device', '=', $code_device)->first();
        if (count($device) == 0) {
            // if not found, insert
            $device_new = new Device;
            $device_new->name_device = $name_device;
            $device_new->code_device = $code_device;
            $device_new->gcm_id = $gcm_id;
            $device_new->apple_id = $apple_id;
            $device_new->save();
            $id_device = $device_new->id_device;
        } else {
            // if found, update data
            $id_device = $device->id_device;
            $device->name_device = $name_device;
            $device->code_device = $code_device;
            $device->gcm_id = $gcm_id;
            $device->apple_id = $apple_id;
            $device->save();
        }
        // connection between the device and user
        $user_device = User_device::whereFk_user($id_user)
            ->whereFk_device($id_device)->first();
        if (count($user_device) == 0) {
            $user_device = new User_device;
            $user_device->fk_device = $id_device;
            $user_device->fk_user = $id_user;
            $user_device->save();
        }
        $date = array('id_device' => $id_device, 'id_user' => $id_user, 'user' => array($user_device));
        // return if login ok
        $json_string = array('status' => 1, 'error_message' => null, 'login' => array($date));
        return Response::json($json_string);
    }

    public function getUser()
    {
        $id_user = Input::get('id_user');
        // find the user by id
        $user = User::getFirstById($id_user);
        $returns = count($user);
        // return results
        if ($returns == 0) {
            $json_string = array('status' => 0, 'error_message' => 'No results found', 'company' => null);
        } else {
            $json_string = array('status' => 1, 'error_message' => null, 'user' => $user);
        }

        return Response::json($json_string);
    }

    public function getCompany()
    {
        // get the user id
        $id_user = Input::get('id_user');
        // find the user by id
        $user = User::getFirstById($id_user);

        $company_id = $user->fk_company;
        // get the company details
        $company = Company::getFirstById($company_id);
        $headquarter_id = $user->fk_headquarter;

        // get the headquarters details
        $headquarter = Company_headquarters::getHeadquartersById($headquarter_id);
        $returns = count($company);
        // return results
        if ($returns == 0) {
            $json_string = array('status' => 0, 'error_message' => 'No results found', 'company' => null);
        } else {
            $json_string = array('status' => 1, 'error_message' => null, 'company' => $company, 'headquarter' => $headquarter);
        }

        return Response::json($json_string);
    }

    public function getTasks_module()
    {

        $questions = DB::table('tabel_intrebari')->get();
        foreach ($questions as $row) {
            if ($row->module != '') {
                $module = Module::where('name_module', 'like', $row->module)->first();
                if (count($module) == 0) {
                    $module = new Module();
                    $module->name_module = $row->module;
                    $module->save();
                }

            }
            if ($row->submodule != '') {
                $submodule = Submodule::where('name_submodule', 'like', $row->submodule)->first();
                if (count($submodule) == 0) {
                    $submodule = new Submodule();
                    $submodule->name_submodule = $row->submodule;
                    $submodule->save();
                }

            }
            if ($row->submodule2 != '') {
                $submodule = Submodule::where('name_submodule', 'like', $row->submodule2)->first();
                if (count($submodule) == 0) {
                    $submodule = new Submodule();
                    $submodule->name_submodule = $row->submodule2;
                    $submodule->save();
                }

            }
            if ($row->task != '') {
                $task = Task::getFirstByName($row->task, $row->task_desc);
                if (count($task) == 0) {
                    $task = new Task();
                    $task->description_task = $row->task_desc;
                    $task->name_task = $row->task;
                    $task->save();
                }

            }
        }
        return 'finished';

    }

    public function getTasks_module_leg()
    {

        $questions = DB::table('table_legs')->get();
        foreach ($questions as $row) {
            if ($row->id_module != '') {
                $id_module_structure = 0;
                $module = Module::where('name_module', 'like', $row->name_module)->first();
                if (count($module) == 0) {
                    $module = new Module();
                    $module->alter_id = $row->id_module;
                    $module->name_module = $row->name_module;
                    $module->save();
                } else {
                    $module->alter_id = $row->id_module;
                    $module->save();
                }
            }
            if ($row->id_submodule != '') {
                $id_module_structure = 0;
                $submodule = Submodule::where('name_submodule', 'like', $row->name_submodule)->first();
                if (count($submodule) == 0) {
                    $submodule = new Submodule();
                    $submodule->name_submodule = $row->name_submodule;
                    $submodule->alter_id = $row->id_submodule;
                    $submodule->save();
                } else {
                    $submodule->alter_id = $row->id_submodule;
                    $submodule->save();
                }
            }
            if ($row->id_submodule1 != '') {
                $id_module_structure = 0;
                $submodule = Submodule::where('name_submodule', 'like', $row->name_submodule1)->first();
                if (count($submodule) == 0) {
                    $submodule = new Submodule();
                    $submodule->name_submodule = $row->name_submodule1;
                    $submodule->alter_id = $row->id_submodule1;
                    $submodule->save();
                } else {
                    $submodule->alter_id = $row->id_submodule1;
                    $submodule->save();
                }
            }
            if ($row->id_task != '') {
                $task = Task::getFirstByName($row->name_task, $row->description_task);

                if (count($task) == 0) {
                    $task = new Task();
                    $task->description_task = $row->description_task;
                    $task->name_task = $row->name_task;
                    $task->save();
                }

                $ids = explode('.', $row->id_task);
                $count_ids = count($ids);


                if ($id_module_structure == 0) {

                    switch ($count_ids) {
                        case '2':

                            $id_0 = Module::where('alter_id', '=', $ids[0])->first()->id_module;
                            $module_structure = Module_structure::where('fk_id', '=', $id_0)
                                ->where('fk_parent', '=', '0')
                                ->where('level', '=', '1')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_0;
                                $module_structure->fk_parent = 0;
                                $module_structure->id_type = 1;
                                $module_structure->level = 1;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;

                            $ms_tasks = Ms_task::where('fk_task', '=', $task->id_task)
                                ->where('fk_module_structure', '=', $id_module_structure)
                                ->first();

                            if (count($ms_tasks) == 0) {
                                $ms_tasks = new Ms_task();
                                $ms_tasks->fk_task = $task->id_task;
                                $ms_tasks->fk_module_structure = $id_module_structure;
                                $ms_tasks->save();
                            }

                            break;
                        case '3':
                            $id_0 = Module::where('alter_id', '=', $ids[0])->first()->id_module;

                            $module_structure = Module_structure::where('fk_id', '=', $id_0)
                                ->where('fk_parent', '=', '0')
                                ->where('level', '=', '1')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_0;
                                $module_structure->fk_parent = 0;
                                $module_structure->id_type = 1;
                                $module_structure->level = 1;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;
                            $id_1 = Submodule::where('alter_id', '=', $ids[0] . '.' . $ids[1])->first()->id_submodule;
                            $module_structure = Module_structure::where('fk_id', '=', $id_1)
                                ->where('fk_parent', '=', $id_module_structure)
                                ->where('level', '=', '2')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_1;
                                $module_structure->fk_parent = $id_module_structure;
                                $module_structure->id_type = 2;
                                $module_structure->level = 2;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;

                            $ms_tasks = Ms_task::where('fk_task', '=', $task->id_task)
                                ->where('fk_module_structure', '=', $id_module_structure)
                                ->first();

                            if (count($ms_tasks) == 0) {
                                $ms_tasks = new Ms_task();
                                $ms_tasks->fk_task = $task->id_task;
                                $ms_tasks->fk_module_structure = $id_module_structure;
                                $ms_tasks->save();
                            }

                            break;
                        case '4':
                            $id_0 = Module::where('alter_id', '=', $ids[0])->first()->id_module;

                            $module_structure = Module_structure::where('fk_id', '=', $id_0)
                                ->where('fk_parent', '=', '0')
                                ->where('level', '=', '1')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_0;
                                $module_structure->fk_parent = 0;
                                $module_structure->id_type = 1;
                                $module_structure->level = 1;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;
                            $id_1 = Submodule::where('alter_id', '=', $ids[0] . '.' . $ids[1])->first()->id_submodule;
                            $module_structure = Module_structure::where('fk_id', '=', $id_1)
                                ->where('fk_parent', '=', $id_module_structure)
                                ->where('level', '=', '2')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_1;
                                $module_structure->fk_parent = $id_module_structure;
                                $module_structure->id_type = 2;
                                $module_structure->level = 2;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;
                            $id_2 = Submodule::where('alter_id', '=', $ids[0] . '.' . $ids[1] . '.' . $ids[2])->first()->id_submodule;
                            $module_structure = Module_structure::where('fk_id', '=', $id_2)
                                ->where('fk_parent', '=', $id_module_structure)
                                ->where('level', '=', '3')
                                ->first();
                            if (count($module_structure) == 0) {
                                $module_structure = new Module_structure();
                                $module_structure->fk_industry = 1;
                                $module_structure->fk_id = $id_2;
                                $module_structure->fk_parent = $id_module_structure;
                                $module_structure->id_type = 2;
                                $module_structure->level = 3;
                                $module_structure->fk_module_parent = $id_0;
                                $module_structure->save();
                            }
                            $id_module_structure = $module_structure->id_module_structure;

                            $ms_tasks = Ms_task::where('fk_task', '=', $task->id_task)
                                ->where('fk_module_structure', '=', $id_module_structure)
                                ->first();

                            if (count($ms_tasks) == 0) {
                                $ms_tasks = new Ms_task();
                                $ms_tasks->fk_task = $task->id_task;
                                $ms_tasks->fk_module_structure = $id_module_structure;
                                $ms_tasks->save();
                            }

                            break;

                    }
                } else {
                    $ms_tasks = Ms_task::where('fk_task', '=', $task->id_task)
                        ->where('fk_module_structure', '=', $id_module_structure)
                        ->first();

                    if (count($ms_tasks) == 0) {
                        $ms_tasks = new Ms_task();
                        $ms_tasks->fk_task = $task->id_task;
                        $ms_tasks->fk_module_structure = $id_module_structure;
                        $ms_tasks->save();
                    }
                }
            }

        }
        return 'finished';

    }


}
