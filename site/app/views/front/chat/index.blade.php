<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Chat</title>

    <link rel="stylesheet" href="style.css" type="text/css"/>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="app/views/front/chat/chat.js"></script>
    <script type="text/javascript">

        // ask user for name with popup prompt    
        //    var name = prompt("Enter your chat name:", "Guest");

        // default name is 'Guest'

        var name = '<?php print $user->first_name . ' ' . $user->last_name?>';
        var html_text = 'You are: <span>' + name + '</span>';

        // display name on page
        $("#name-area").html(html_text);

        // kick off chat
        var chat = new Chat();
        $(function () {

            chat.getState();

            // watch textarea for key presses
            $("#sendie").keydown(function (event) {

                var key = event.which;

                //all keys including return.  
                if (key >= 33) {

                    var maxLength = $(this).attr("maxlength");
                    var length = this.value.length;

                    // don't allow new content if length is maxed out
                    if (length >= maxLength) {
                        event.preventDefault();
                    }
                }
            });
            // watch textarea for release of key press
            $('#sendie').keyup(function (e) {

                if (e.keyCode == 13) {

                    var text = $(this).val();
                    var maxLength = $(this).attr("maxlength");
                    var length = text.length;

                    // send 
                    if (length <= maxLength + 1) {

                        chat.send(text, name);
                        $(this).val("");

                    } else {

                        $(this).val(text.substring(0, maxLength));

                    }


                }
            });

        });
    </script>
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font: 12px "Lucida Grande", Sans-Serif;
            background: #d0d0d0 url(images/bg.png);
        }

        h2 {
            color: #fa9f00;
            font: 30px Helvetica, Sans-Serif;
            margin: 0 0 10px 0;
        }

        #page-wrap {
            width: 500px;
            margin: 30px auto;
            position: relative;
        }

        #chat-wrap {
            border: 1px solid #eee;
            margin: 0 0 15px 0;
        }

        #chat-area {
            height: 300px;
            overflow: auto;
            border: 1px solid #666;
            padding: 20px;
            background: white;
        }

        #chat-area span {
            color: white;
            background: #333;
            padding: 4px 8px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 8px;
            margin: 0 5px 0 0;
        }

        #chat-area p {
            padding: 8px 0;
            border-bottom: 1px solid #ccc;
        }

        #name-area {
            position: absolute;
            top: 12px;
            right: 0;
            color: white;
            font: bold 12px "Lucida Grande", Sans-Serif;
            text-align: right;
        }

        #name-area span {
            color: #fa9f00;
        }

        #send-message-area p {
            float: left;
            color: white;
            padding-top: 27px;
            font-size: 14px;
        }

        #sendie {
            border: 3px solid #999;
            width: 360px;
            padding: 10px;
            font: 12px "Lucida Grande", Sans-Serif;
            float: right;
        }
    </style>
</head>

<body onload="setInterval('chat.update()', 1000)">

<div id="page-wrap">

    <h2>jQuery/PHP Chat</h2>

    <p id="name-area"></p>

    <div id="chat-wrap">
        <div id="chat-area"></div>
    </div>

    <form id="send-message-area">
        <p>Your message: </p>
        <textarea id="sendie" maxlength='100'></textarea>
        <button type="submit">Send</button>
    </form>

</div>

</body>

</html>