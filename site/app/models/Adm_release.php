<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:28
 */
class Adm_release extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_comment' => 'required',
    ];
    public $primaryKey = 'id_release';
    protected $table = 'adm_releases';
    protected $guarded = ['id_release'];
    protected $fillable = ['name_release', 'start_date', 'budget', 'is_completed', 'is_current'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_release::where('id_release', '=', $id)->first();
        return $list;
    }
}