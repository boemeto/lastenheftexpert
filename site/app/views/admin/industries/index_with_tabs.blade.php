@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('footer_scripts')



    <script type="text/javascript">


        $("#modalAddIndustry").on('hidden.bs.modal', function () {
            $("#modalAddIndustry").removeData('bs.modal');
        });
        $("#modalAddIndustry").on('shown.bs.modal', function () {
        });
    </script>
@stop
@section('content')
    <div class="panel panel-info" style="margin: 10px;">
        <div class="panel-heading">
            <div class="float-left">
                <h3 class="panel-title">{{trans('messages.nav_industries') }}</h3>
            </div>
            <div class="float-right" style="margin-right: 15px">
                {{trans('messages.nav_industries') }}  {{trans('messages.act_create') }}

                <a href="{{ URL::route('industry.create') }}" class="btn ls-light-blue-btn"
                   data-toggle="modal" data-target="#modalAddIndustry">
                    <i class="fa fa-plus"></i> </a>
            </div>
            <div class="clear"></div>
        </div>

        <!-- /.panel-header -->
        <div class="panel-body">
            <br>
            {{-- Confirmation Messages--}}
            @if( Session::has('error_message'))
                <div class="form-group has-error">
                    <label class="control-label" for="inputError">
                        <i class="fa fa-times-circle-o"></i> {{ Session::get('error_message') }}</label>
                </div>
            @endif
            @if( Session::has('success_message'))
                <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess">
                        <i class="fa fa-check"></i> {{ Session::get('success_message') }}</label>
                </div>
            @endif

            <ul class="nav nav-tabs icon-tab">
                @foreach($industry_types as $industry_type)

                    @if($industry_type->id_industry_type == 1)
                        <li class="active">
                            <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                <i class="fa {{$industry_type->font_icon}}"></i> <span class="name_tabs"> {{$industry_type->name_industry_type}}</span></a>
                        </li>
                    @else
                        <li>
                            <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                <i class="fa {{$industry_type->font_icon}}"></i> <span class="name_tabs"> {{$industry_type->name_industry_type}} </span></a>
                        </li>
                    @endif
                @endforeach

            </ul>
            <div class="tab-content tab-border">
                @foreach($industry_types as $industry_type)
                    <?php
                    $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                    ?>
                    <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">

                        @if(count($industries) > 0)

                            <div class="row">
                                @foreach($industries as $row)
                                    <div class="col-md-4">
                                        <div class="panel panel-info">

                                            <div class="panel-body panel_industry">
                                                <div class="row">
                                                    <div class="col-md-8 name_industry">
                                                        {{ $row->name_industry  }}
                                                    </div>

                                                    <div class="col-md-4 col-padding-0">
                                                        <div style="display: inline-block">
                                                            <a href="{{ URL::route('industry.show',[$row->id_industry]) }}"
                                                               class="btn ls-light-green-btn">
                                                                <i class="fa fa-sitemap"></i>
                                                            </a>
                                                        </div>
                                                        <div style="display: inline-block">
                                                            {{ Form::model($row,['route'=>['industry.update',$row->id_industry], 'method'=>'PATCH'])}}
                                                            {{ Form::hidden('deactivate','1') }}
                                                            @if($row->is_deleted == 1)
                                                                <button type="submit" class="btn btn-app btn-flat ls-light-blue-btn">
                                                                    <i class="fa fa-check"></i>
                                                                </button>
                                                            @else
                                                                <button type="submit" class="btn  btn-danger">
                                                                    <i class="fa fa-close"></i>
                                                                </button>
                                                            @endif
                                                            {{ Form::close() }}
                                                        </div>
                                                        @if(strlen(strip_tags($row->description_industry)) > 1)
                                                            <div style="display: inline-block">
                                                                <button class="tooltip_div btn btn-app btn-flat btn-default" title="{{ strip_tags($row->description_industry)  }}"><i class="fa fa-info-circle"></i></button>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        @endif
                    </div>
                @endforeach
            </div>
            <br>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Modal Industry -->
    <div class="modal fade" id="modalAddIndustry" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Industry End -->
@stop