<div class="tab-pane fade table_invoice" id="projects">
    <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
        <i class="fa fa-sitemap mgr-10 profile-icon"></i>
        {{trans('messages.nav_projects')}}
        @if (in_array('2', $fk_group)  )
            <a href="{{URL::to('projects/create')}}" title=" " id="a_form_show_0" class="btn static_add_button a_form_syyhow btn btn-round btn-xxl  ls-light-blue-btn  ">
                <i class="fa fa-plus"></i>
            </a>
            <div class="title_static_add_button" id="div_a_form_syyhow_0">
                {{trans('messages.add_new_project')}}
            </div>
        @endif
    </h3>
    <table class="table table-responsive table-hover data_table">
        <thead>
            <tr>
                <th>Nr.</th>
                <th>{{trans('messages.label_project_name')}}</th>
                <th>{{trans('messages.label_created_at')}}</th>
                <th>{{trans('messages.label_due_date')}}</th>
                <th>{{trans('messages.label_status')}}</th>
                <th>{{trans('messages.label_price')}}</th>
                <th class="text-center">{{trans('messages.label_action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($projects as $project)
                <tr>
                    <td class="text-center">{{++$i}}</td>
                    <td>{{$project->name_project}}</td>
                    <td> {{date('d.m.Y', strtotime($project->created_at))}} </td>
                    <td>{{date('d.m.Y', strtotime($project->due_date))}}</td>
                    <td>
                        <?php $status = is_project_expired($project->id_project); ?>
                        {{($status==1)?'<div class="user_project label label-success ">'.trans('messages.l_active').'</div>':' '}}
                        {{($status==2)?'<div class="user_project label label-warning">'.trans('messages.label_blocked').'</div>':' '}}
                        {{($status==3)?'<div class="user_project label label-danger">'.trans('messages.l_expired').'</div>':' '}}
                    </td>
                    <td class="pd0 pdr10">
                        <?php $totalCost = getProjectTotalCost($project->id_project);
                              $totalCost = formatToGermanyPrice($totalCost); ?>
                        <b class="pull-right">
                          {{$totalCost}} <i class="fa fa-euro"></i>
                        </b>
                    </td>
                    <td class="menu-action rounded-btn text-center">
                        <a class="btn menu-icon ls-orange-btn btn-round" href="{{url('projects/'.$project->id_project)}}" data-placement="top" data-toggle="tooltip" data-original-title="edit">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
