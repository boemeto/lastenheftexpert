<?php
return LanguageTrans::getList('de', 2);
return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted"              => ":attribute muss akzeptiert werden.",
    "active_url"            => ":attribute ist keine gültige Internet-Adresse.",
    "after"                 => ":attribute muss ein Datum nach dem :date sein.",
    "alpha"                 => ":attribute darf nur aus Buchstaben bestehen.",
    "alpha_dash"            => ":attribute darf nur aus Buchstaben, Zahlen, Binde- und Unterstrichen bestehen. Umlaute (ä, ö, ü) und Eszett (ß) sind nicht erlaubt.",
    "alpha_num"             => ":attribute darf nur aus Buchstaben und Zahlen bestehen.",
    "array"                 => ":attribute muss ein Array sein.",
    "before"                => ":attribute muss ein Datum vor dem :date sein.",
    "between"               => [
        "numeric" => ":attribute muss zwischen :min & :max liegen.",
        "file"    => ":attribute muss zwischen :min & :max Kilobytes groß sein.",
        "string"  => ":attribute muss zwischen :min & :max Zeichen lang sein.",
        "array"   => ":attribute muss zwischen :min & :max Elemente haben."
    ],
    "boolean"               => ":attribute muss entweder 'true' oder 'false' sein.",
    "confirmed"             => ":attribute stimmt nicht mit der Bestätigung überein.",
    "date"                  => ":attribute muss ein gültiges Datum sein.",
    "date_format"           => ":attribute entspricht nicht dem gültigen Format für :format.",
    "different"             => ":attribute und :other müssen sich unterscheiden.",
    "digits"                => ":attribute muss :digits Stellen haben.",
    "digits_between"        => ":attribute muss zwischen :min und :max Stellen haben.",
    "email"                 => ":attribute Format ist ungültig.",
    "exists"                => "Der gewählte Wert für :attribute ist ungültig.",
    "filled"                => ":attribute muss ausgefüllt sein.",
    "image"                 => ":attribute muss ein Bild sein.",
    "in"                    => "Der gewählte Wert für :attribute ist ungültig.",
    "integer"               => ":attribute muss eine ganze Zahl sein.",
    "ip"                    => ":attribute muss eine gültige IP-Adresse sein.",
    "login"                 => "Falsche Benutzername oder Passworte",
    "max"                   => [
        "numeric" => ":attribute darf maximal :max sein.",
        "file"    => ":attribute darf maximal :max Kilobytes groß sein.",
        "string"  => ":attribute darf maximal :max Zeichen haben.",
        "array"   => ":attribute darf nicht mehr als :max Elemente haben."
    ],
    "mimes"                 => ":attribute muss den Dateityp :values haben.",
    "min"                   => [
        "numeric" => ":attribute muss mindestens :min sein.",
        "file"    => ":attribute muss mindestens :min Kilobytes groß sein.",
        "string"  => ":attribute muss mindestens :min Zeichen lang sein.",
        "array"   => ":attribute muss mindestens :min Elemente haben."
    ],
    "not_in"                => "Der gewählte Wert für :attribute ist ungültig.",
    "numeric"               => ":attribute muss eine Zahl sein.",
    "regex"                 => ":attribute Format ist ungültig.",
    "required"              => ":attribute muss ausgefüllt sein.",
    "required_if"           => ":attribute muss ausgefüllt sein wenn :other :value ist.",
    "required_with"         => ":attribute muss angegeben werden wenn :values ausgefüllt wurde.",
    "required_with_all"     => ":attribute muss angegeben werden, wenn :values ausgefüllt wurde.",
    "required_without"      => ":attribute muss angegeben werden wenn :values nicht ausgefüllt wurde.",
    "required_without_all"  => ":attribute muss angegeben werden wenn keines der Felder :values ausgefüllt wurde.",
    "same"                  => ":attribute und :other müssen übereinstimmen.",
    "size"                  => [
        "numeric" => ":attribute muss gleich :size sein.",
        "file"    => ":attribute muss :size Kilobyte groß sein.",
        "string"  => ":attribute muss :size Zeichen lang sein.",
        "array"   => ":attribute muss genau :size Elemente haben."
    ],
    "timezone"              => ":attribute muss eine gültige Zeitzone sein.",
    "unique"                => ":attribute ist schon vergeben.",
    "url"                   => "Das Format von :attribute ist ungültig.",
    "confirm_user"          => "Benutzer erfolgreich erstellt. Bitte anmelden.",
    "confirmation_question" => "Sind Sie sicher, Sie wollen diesen Artikel aus der Datenbank löschen?",
    "delete_success"        => "Erfolgreich gelöscht",
    "delete_error"          => "Dieser Artikel kann nicht hinzugefügt werden",
    "add_success"           => "Erfolgreich hinzugefügt",
    "add_error"             => "This item cannot be added",
    "edit_success"          => "Erfolgreich geändert",
    "edit_error"            => "Dieser Artikel kann nicht geändert werden",
    "link_success"          => "Erfolgreich verbunden",
    "link_error"            => "Diese Elemente können nicht verknüpft werden",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'attribute-name' => array(
            'rule-name' => 'custom-message',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(),

);
