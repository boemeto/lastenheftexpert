<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:36
 */
class Industry extends Eloquent
{


    public static $rules = [
        'name_industry' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_industry';
    protected $table = 'pr_industries';
    protected $guarded = ['id_industry'];
    protected $fillable = ['name_industry', 'description_industry', 'name_industry_en', 'description_industry_en', 'fk_type', 'is_deleted'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Industry::whereId_industry($id)->first();
        return $list;
    }

    public static function getIndustryByProject($id)
    {
        $proj = Project::getFirstById($id);

        return $proj->fk_industry;
    }

    public static function getIndustryByType($id)
    {
        $industry = Industry::where('fk_type', '=', $id)->get();
        return $industry;
    }

    public static function getNameById($id)
    {
        $list = Industry::whereId_industry($id)->first();
        return $list->name_industry;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }
        if ($lang == 'de') {
            if (Schema::hasColumn('pr_industries', $field . '_de') && $this->attributes[$field . '_de'] != '') {
                return $this->attributes[$field . '_de'];
            } else {
                return $this->attributes[$field];
            }

        } else {
            return $this->attributes[$field];
        }

    }
}