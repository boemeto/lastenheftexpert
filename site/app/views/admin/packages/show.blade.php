@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('footer_scripts')
    <script>
        $("#modalAddPost").on('hidden.bs.modal', function () {
            $("#modalAddPost").removeData('bs.modal');
        });
        $("#modalAddPost").on('shown.bs.modal', function () {
        });
    </script>
@stop
@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="panel widget light-widget panel-bd-top ">
                    <div class="panel-heading ">
                        <h3 class="mgtp-10">
                            {{$pack->name_package}}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive   data_table">
                            <thead>
                            <tr>
                                <th>{{trans('messages.label_name')}}</th>
                                <th>{{trans('messages.label_details')}}</th>
                                <th>{{trans('messages.label_icon')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach($rows as $row)
                                <tr>
                                  <td>{{$row->name_field}}  </td>
                                  <td>{{$row->description_pack}}</td>
                                  <td>
                                      <i class="fa {{$row->fa_icon}} "></i> {{$row->fa_icon}}
                                  </td>
                                  <td>
                                      <a href="{{ URL::route('packs_row.edit',array($row->id_pack_row)) }}"
                                         data-toggle="modal" data-target="#modalAddPost"
                                         class="btn  ls-orange-btn    btn-round btn-l">
                                          <i class="fa fa-edit"></i>
                                      </a>
                                      {{ Form::open(array('route' => array('packs_row.destroy', $row->id_pack_row), 'method' => 'delete')) }}
                                      <button type="submit" class="btn btn-round btn-l ls-red-btn "
                                              onclick="return confirm('{{trans('validation.confirmation_delete')}}')">
                                          <i class="fa fa-trash"></i>
                                      </button>
                                      {{ Form::close()}}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="{{ URL::route('packs_row.show',array( $pack->id_package)) }}"
       id="a_form_headquarter_0" data-toggle="modal" data-target="#modalAddPost"
       class="btn static_add_button ls-light-blue-btn a_form_headquarter  btn-round btn-xxl">
        <i class="fa fa-plus"></i>
    </a>
    <div class="title_static_add_button" id="div_a_form_headquarter_0">
        {{trans('messages.nav_package') }}  {{trans('messages.act_create') }}
    </div>
    <div class="modal fade" id="modalAddPost" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop
