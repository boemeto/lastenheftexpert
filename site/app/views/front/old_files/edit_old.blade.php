<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 7/23/2015
 * Time: 9:58 AM
 */

?>
@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')
    {{--// custom scripts--}}
    <script type="text/javascript">
        // enable colorpicker
        window.onload = function () {
            var x = document.getElementsByClassName("colorPicker");
            var i;
            for (i = 0; i < x.length; i++) {
                tinycolorpicker(x[i]);
            }
        }
    </script>

    <script type="text/javascript">
        // colorpicker - change task color
        $(".track").click(function () {
            var palette = [
                "#FE2712", "#A7194B", "#8601AF",
                "#3D01A4", "#0247FE", "#0391CE",
                "#66B032", "#D0EA2B", "#FEFE33",
                "#FABC02", "#FB9902"];
            var id = $(this).attr('id');
            var id_nr = id.replace('track', "");
            var $input = $('#colorInput' + id_nr);
            var hex = $input.val();
            if (jQuery.inArray(hex, palette) == -1) {
                $('#panel-task-border' + id_nr).css("background", 'transparent');
                $('#panel-task-border' + id_nr).css("padding-right", '0');
                $('#colorInnerTask' + id_nr).css("background", 'url("../../assets/images/minicolor/culori-rotund.png")');
                $('#colorInnerTask' + id_nr).css("background-size", '15px 15px');
                $('#borderInnerTask' + id_nr).css("border-right", '0px solid transparent');
                $('#close_modal' + id_nr).css("right", '-24px');

                var hex_color = 'transparent';
            } else {
                $('#panel-task-border' + id_nr).css("background-color", hex);
                $('#panel-task-border' + id_nr).css("padding-right", '7px');
                $('#colorInnerTask' + id_nr).css("background", hex);
                $('#borderInnerTask' + id_nr).css("border-right", '7px solid ' + hex);
                $('#close_modal' + id_nr).css("right", '-30px');


                var hex_color = hex;
            }


            var data = {
                'hex': hex_color,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            $.post('{{ route("ajax.update_project_task") }}', data, function (data, textStatus, xhr) {
                /*optional stuff to do after success */
                console.log(data);

            });

        });
  

        // enable iCheck
        $(document).ready(function () {
            $('input.icheck-green').iCheck({
                checkboxClass: 'icheckbox_minimal-green',
                radioClass: 'iradio_minimal-green',
                increaseArea: '0%' // optional
            });

            // save to database - checked, unchecked
            $('input.icheck-green').on('ifChanged', function () {
                var id = $(this).attr('id');
                var id_nr = id.replace('icheck-green', "");
                if ($(this).prop('checked')) {
                    var is_checked = 1;
                } else {
                    var is_checked = 0;
                }

                var data = {
                    'is_checked': is_checked,
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'

                };

                $.post('{{ route("ajax.update_project_task") }}', data, function (data, textStatus, xhr) {
                    /*optional stuff to do after success */
                    console.log(data);

                });
            });

        });

        // module menu - view all functionalities
        $('#module_expnad_all').click(function () {
            if ($('#module_expnad_all').hasClass('collapsed') == true) {
                $('#module_expnad_all').removeClass('collapsed');
                $(".collapsible").not('#module_expnad_all').each(function (index) {
                    if ($(this).hasClass('collapsed') == true) {
                        $(this).removeClass('collapsed');
                        $(this).click();
                    }
                });
                return 1;
            }
            else {
                $('#module_expnad_all').addClass('collapsed');
                $(".collapsible").each(function () {
                    var thisActive = $(this).hasClass('collapsed');
                    if ($(this).hasClass('collapsed') == false) {
                        $(this).addClass('collapsed');
                        $(this).click();
                    }
                });
                return 0;
            }

        });

        $('.set_priority').change(function (event) {
            var id = $(this).attr('id');
            var id_nr = id.replace('set_priority', "");
            var priority = $(this).val();

            var data = {
                'task_priority': priority,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            $.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                        alert(JSON.stringify(data))
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

        });
        $('.button_delete_file').click(function (event) {
            if (confirm("Are you sure?")) {
                // your deletion code

                var id = $(this).attr('id');
                var id_nr = id.replace('button_delete_file', '');

                var data = {
                    'id_project_attach': id_nr
                };
                $.post('{{  route("ajax.remove_file_project") }}', data)
                        .done(function (msg) {
                            // alert('success')
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });
                $('#div_file_' + id_nr).hide();
                return true;
            }
            return false;
        });

        $('.button_edit_task').click(function (event) {
            var id = $(this).attr('id');
            var id_nr = id.replace('button_edit_task', "");
            $(this).prevAll('.note-editor').show();
            $(this).hide();
            $('#button_save_task' + id_nr).show();
            $('#user_comment_task' + id_nr).hide();
        });
        $('.button_save_task').click(function (event) {

            var id = $(this).attr('id');
            var id_nr = id.replace('button_save_task', "");
            var task_description = $(this).prevAll('.note-editor').find('.note-editable').html();
            var data = {
                'task_description': $.trim(task_description),
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save to database

            $.post('{{  route("ajax.update_project_task") }}', data, function (data, textStatus, xhr) {
                /*optional stuff to do after success */
                console.log(data);
            });
            $(this).prevAll('.note-editor').hide();
            $(this).hide();
            $('#button_edit_task' + id_nr).show();
            $('#user_comment_task' + id_nr).show();
            $('#user_comment_task' + id_nr).html($.trim(task_description));
        });

    </script>
@stop

@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="{{Url::to('/')}}"><i class="fa fa-home"></i></a></li>
                        <li class="active">{{$project->name_project}}</li>
                    </ol>
                    <!--Top breadcrumb stop -->
                    {{ $errors->first("confirm_",'<div class="text-green">:message</div>') }}
                </div>
                <div class="col-xs-12" style="margin-left: -26px">
                    <!-- Module Menu -->
                    <div id="ModuleMenu">
                        <div class="list-group panel">
                            <div class="row">
                                <div class="row-height">
                                    <!-- View All row Start -->
                                    <div class="col-xs-2 col-sm-height">
                                        <div class="inside-full-height">
                                            <a href="javascript:void(0)"
                                               class="list-group-item collapsed collapsible list-group-item-module1 "
                                               id="module_expnad_all">
                                                <div class="module-menu-title">View All</div>
                                                <i class="fa fa-plus-square-o "></i>
                                                <i class="fa fa-minus-square-o "></i>
                                                <i class="fa fa-info-circle "></i>

                                                <div style="clear: both"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-height">
                                        <div class="inside-full-height">
                                        </div>
                                    </div>
                                    <!-- View All row Stop -->
                                </div>
                            </div>
                            @foreach($module_structure as $module)

                            @if(Module_structure::hasChild($module->id_module_structure) == 1)
                                    <!--  Module level 1 Start -->
                            <div class="row">
                                <div class="row-height">
                                    <div class="col-xs-2 col-sm-height">
                                        <div class="inside-full-height">
                                            <a href="#module_{{$module->id_module_structure}}"
                                               class="list-group-item collapsed collapsible list-group-item-module1"
                                               data-toggle="collapse"
                                               data-parent="#module_{{$module->id_module_structure}}">
                                                <span class="module-menu-title">{{$module->id_name}}</span>
                                                <i class="fa fa-plus-square-o "></i>
                                                <i class="fa fa-minus-square-o "></i>
                                                <i class="fa fa-info-circle "></i>

                                                <div style="clear: both"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-height">
                                        <div class="inside-full-height">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  Module level 1  stop-->
                            <!--  Collapsable for Module level 1 Start -->
                            <div class="collapse" id="module_{{$module->id_module_structure}}">
                                <!--  get modules lvl 2 -->
                                <?php  $module_structure_2 = Module_structure::getModuleByLvl
                                ($industry->id_industry, 2, $module->id_module_structure);
                                ?>
                                @foreach($module_structure_2 as $module_2)
                                @if(Module_structure::hasChild($module_2->id_module_structure) == 1)
                                        <!--  Module level 2 Start  -->
                                <div class="row">
                                    <div class="row-height">
                                        <div class="col-xs-2 col-sm-height">
                                            <div class="inside-full-height">
                                                <a href="#module_{{$module_2->id_module_structure}}"
                                                   class="list-group-item collapsed collapsible list-group-item-module2"
                                                   data-toggle="collapse"
                                                   data-parent="#module_{{$module->id_module_structure}}">
                                                                    <span class="module-menu-title">
                                                                        {{$module_2->id_name}}</span>
                                                    <i class="fa fa-plus-square-o "></i>
                                                    <i class="fa fa-minus-square-o "></i>
                                                    <i class="fa fa-info-circle "></i>

                                                    <div style="clear: both"></div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xs-10 col-sm-height">
                                            <div class="inside-full-height">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  Module level 2 Stop  -->
                                <div class="collapse list-group-submenu"
                                     id="module_{{$module_2->id_module_structure}}">
                                    <?php  $module_structure_3 = Module_structure::getModuleByLvl
                                    ($industry->id_industry, 3, $module_2->id_module_structure);
                                    ?>
                                    @foreach($module_structure_3 as $module_3)
                                        @if(Module_structure::hasChild($module_3->id_module_structure) == 1)
                                            <div class="row">
                                                <div class="row-height">
                                                    <div class="col-xs-2 col-sm-height">
                                                        <div class="inside-full-height">
                                                            <a href="#module_{{$module_3->id_module_structure}}"
                                                               class="list-group-item collapsed collapsible list-group-item-module2"
                                                               data-toggle="collapse"
                                                               data-parent="#module_{{$module_2->id_module_structure}}">
                                                                                 <span class="module-menu-title">
                                                                                    {{$module_3->id_name}}</span>
                                                                <i class="fa fa-plus-square-o "></i>
                                                                <i class="fa fa-minus-square-o "></i>
                                                                <i class="fa fa-info-circle "></i>

                                                                <div style="clear: both"></div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-10 col-sm-height">
                                                        <div class="inside-full-height">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="collapse list-group-submenu"
                                                 id="module_{{$module_3->id_module_structure}}">
                                                <?php  $module_structure_4 =
                                                        Module_structure::getModuleByLvl
                                                        ($industry->id_industry, 4,
                                                                $module_3->id_module_structure);
                                                ?>
                                                @foreach($module_structure_4 as $module_4)
                                                    @if(Module_structure::hasChild($module_4->id_module_structure) == 1)
                                                        <div class="row">
                                                            <div class="row-height">
                                                                <div class="col-xs-2 col-sm-height">
                                                                    <div class="inside-full-height">
                                                                        <a href="#module_{{$module_4->id_module_structure}}"
                                                                           class="list-group-item collapsed collapsible list-group-item-module2"
                                                                           data-toggle="collapse"
                                                                           data-parent="#module_{{$module_3->id_module_structure}}">
                                                                                             <span class="module-menu-title">
                                                                                             {{$module_4->id_name}}</span>
                                                                            <i class="fa fa-plus-square-o "></i>
                                                                            <i class="fa fa-minus-square-o "></i>
                                                                            <i class="fa fa-info-circle "></i>

                                                                            <div style="clear: both"></div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-10 col-sm-right col-sm-height">
                                                                    <div class="inside-full-height">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="row">
                                                            <div class="row-height">
                                                                <?php
                                                                $pr_module_tasks = Ms_task::getTasksByStructure
                                                                ($module_4->id_module_structure, $project->id_project);

                                                                ?>
                                                                <div class="col-xs-2 col-sm-height">
                                                                    <div class="inside-full-height">
                                                                        <a href="javascript:void(0)"
                                                                           class="list-group-item
                                                                                   list-group-item-module4"
                                                                           data-parent="#module_{{$module_3->id_module_structure}}">
                                                                                             <span class="module-menu-title">
                                                                                                 {{$module_4->id_name}}</span>

                                                                            <div class="circle_tasks_left">
                                                                                {{count($pr_module_tasks)}}
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-10 col-sm-height inside-with-border">
                                                                    <div class="inside-full-height">
                                                                        @foreach($pr_module_tasks as $pr_module_task)
                                                                            <?php $attachments =
                                                                                    Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                            @if($pr_module_task->task_color  != null &&
                                                          $pr_module_task->task_color != 'transparent')
                                                                                <style type="text/css">
                                                                                    #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                       {
                                                                                        padding-right: 7px;
                                                                                        background-color: {{$pr_module_task->task_color}};
                                                                                    }

                                                                                    #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                       {
                                                                                        background-color: {{$pr_module_task->task_color}};
                                                                                    }
                                                                                </style>
                                                                            @endif
                                                                            <div class="panel-task-border"
                                                                                 id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                <div class="panel-task">
                                                                                    <div class="panel-task-header">
                                                                                        <div class="ls-button-group">
                                                                                            {{--<input--}}
                                                                                            {{--class="form-control togglePaletteOnly"--}}
                                                                                            {{--id="togglePaletteOnly{{$pr_module_task->id_ms_task}}"--}}
                                                                                            {{--type="text"/>--}}
                                                                                            <div class="ls-button-group">
                                                                                                <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                     class="colorPicker">
                                                                                                    <a class="color"
                                                                                                       id="color{{$pr_module_task->id_ms_task}}">
                                                                                                        <div class="colorInner"
                                                                                                             id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                    </a>

                                                                                                    <div class="track"
                                                                                                         id="track{{$pr_module_task->id_ms_task}}"></div>
                                                                                                    <ul class="dropdown">
                                                                                                        <li></li>
                                                                                                    </ul>
                                                                                                    <input type="hidden"
                                                                                                           class="colorInput"
                                                                                                           id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="ls-button-group">
                                                                                            <i class="fa fa-info-circle "></i>
                                                                                        </div>
                                                                                        <div class="ls-button-group float-right">
                                                                                            {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, '', false,
                                                                                            ['class'=>"icheck-green",($pr_module_task->is_checked)?'disabled':'',
                                                                                            'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                            ($pr_module_task->is_checked || $pr_module_task->is_checked_user  )?'checked':''])}}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="panel-task-body">
                                                                                        {{ $pr_module_task->name_task}}
                                                                                    </div>
                                                                                    <div class="panel-task-footer">
                                                                                        <ul>
                                                                                            <li>
                                                                                                <i class="fa fa-paperclip"></i>
                                                                                                {{count($attachments)}}
                                                                                            </li>
                                                                                            <li>Edit
                                                                                            </li>

                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="row-height">
                                                    <?php
                                                    $pr_module_tasks = Ms_task::getTasksByStructure
                                                    ($module_3->id_module_structure, $project->id_project);
                                                    ?>
                                                    <div class="col-xs-2 col-sm-height">
                                                        <div class="inside-full-height">
                                                            <a href="javascript:void(0)"
                                                               class="list-group-item
                                                                                   list-group-item-module3"
                                                               data-parent="#module_{{$module_2->id_module_structure}}">
                                                                                 <span class="module-menu-title">
                                                                                      {{$module_3->id_name}}</span>

                                                                <div class="circle_tasks_left">
                                                                    {{count($pr_module_tasks)}}
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-10 col-sm-height inside-with-border">
                                                        <div class="inside-full-height">

                                                            @if(count($pr_module_tasks)>0)

                                                                @foreach($pr_module_tasks as $pr_module_task)
                                                                    <?php $attachments =
                                                                            Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                    @if($pr_module_task->task_color  != null &&
                                                          $pr_module_task->task_color != 'transparent')
                                                                        <style type="text/css">
                                                                            #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                             {
                                                                                padding-right: 7px;
                                                                                background-color: {{$pr_module_task->task_color}};
                                                                            }

                                                                            #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                         {
                                                                                background-color: {{$pr_module_task->task_color}};
                                                                            }
                                                                        </style>
                                                                    @endif
                                                                    <div class="panel-task-border"
                                                                         id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                        <div class="panel-task">
                                                                            <div class="panel-task-header">
                                                                                <div class="ls-button-group">

                                                                                    <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                         class="colorPicker">
                                                                                        <a class="color"
                                                                                           id="color{{$pr_module_task->id_ms_task}}">
                                                                                            <div class="colorInner"
                                                                                                 id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                        </a>

                                                                                        <div class="track"
                                                                                             id="track{{$pr_module_task->id_ms_task}}"></div>
                                                                                        <ul class="dropdown">
                                                                                            <li></li>
                                                                                        </ul>
                                                                                        <input type="hidden"
                                                                                               class="colorInput"
                                                                                               id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="ls-button-group">
                                                                                    <i class="fa fa-info-circle "></i>
                                                                                </div>
                                                                                <div class="ls-button-group float-right">
                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, '', false,
                                                                                   ['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                   ($pr_module_task->is_checked)?'disabled':'',
                                                                                   ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                </div>
                                                                            </div>
                                                                            <div class="panel-task-body">
                                                                                {{ $pr_module_task->name_task}}
                                                                            </div>
                                                                            <div class="panel-task-footer">
                                                                                <ul>
                                                                                    <li><i class="fa fa-paperclip"></i>
                                                                                        {{ count($attachments)  }}
                                                                                    </li>
                                                                                    <li>Edit</li>
                                                                                    <li style="color: #FF7878">
                                                                                        Delete
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                @else
                                        <!--  Module level 2 Start  -->
                                <div class="row">
                                    <div class="row-height">
                                        <?php
                                        $pr_module_tasks = Ms_task::getTasksByStructure
                                        ($module_2->id_module_structure, $project->id_project);

                                        ?>
                                        <div class="col-xs-2 col-sm-height">
                                            <div class="inside-full-height">
                                                <a href="javascript:void(0)" class=" list-group-item
                                                            list-group-item-module2"
                                                   data-parent="#module_{{$module->id_module_structure}}">
                                                                     <span class="module-menu-title">
                                                                        {{$module_2->id_name}}</span>

                                                    <div class="circle_tasks_left">
                                                        {{count($pr_module_tasks)}}
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                        <div class="col-xs-10 col-sm-height inside-with-border">
                                            <div class="inside-full-height ">
                                                @if(count($pr_module_tasks)>0)
                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                        <?php $attachments =
                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                        @if($pr_module_task->task_color  != null &&
                                                            $pr_module_task->task_color != 'transparent')
                                                            <style type="text/css">
                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                             {
                                                                    padding-right: 7px;
                                                                    background-color: {{$pr_module_task->task_color}};
                                                                }

                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                         {
                                                                    background-color: {{$pr_module_task->task_color}};
                                                                }

                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                          {
                                                                    background: {{$pr_module_task->task_color}};
                                                                }

                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                         {
                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                }

                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                         {
                                                                    right: -30px;
                                                                }
                                                            </style>
                                                        @endif
                                                        <div class="panel-task-border"
                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                            <div class="panel-task {{($pr_module_task->is_checked)?
                                                            'panel_background_grey':''}}">
                                                                <div class="panel-task-header">
                                                                    <div class="ls-button-group">
                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                             class="colorPicker">
                                                                            <a class="color"
                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                <div class="colorInner"
                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                            </a>

                                                                            <div class="track"
                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>
                                                                            <ul class="dropdown">
                                                                                <li></li>
                                                                            </ul>
                                                                            <input type="hidden"
                                                                                   class="colorInput"
                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ls-button-group">
                                                                        <a href="javascript:void(0);"
                                                                           data-toggle="modal"
                                                                           data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                           title=""> <i class="fa fa-info-circle
                                                                           "></i></a>
                                                                    </div>
                                                                    <div class="ls-button-group float-right">
                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, '',
                                                                        false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                    </div>
                                                                </div>
                                                                <div class="panel-task-body">
                                                                    {{ $pr_module_task->name_task}}
                                                                </div>
                                                                <div class="panel-task-footer">
                                                                    <ul>
                                                                        <li>
                                                                            <i class="fa fa-paperclip"></i> {{count($attachments)}}
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:void(0);"
                                                                               data-toggle="modal"
                                                                               data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                               title="">
                                                                                Edit
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="modal fade"
                                                                     id="modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                     tabindex="-1" role="dialog"
                                                                     aria-labelledby="myModalLabel"
                                                                     aria-hidden="true"
                                                                     data-keyboard="false" data-backdrop="static"
                                                                     style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content"
                                                                             id="borderInnerTask{{$pr_module_task->id_ms_task}}">
                                                                            <button type="button"
                                                                                    class="close close_modal"
                                                                                    id="close_modal{{$pr_module_task->id_ms_task}}"
                                                                                    data-dismiss="modal"
                                                                                    aria-hidden="true">
                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                x
                                                                            </button>
                                                                            <div class="modal-header task-label-white">
                                                                                <div class="ls-button-group float-left">
                                                                                    <div class="colorInnerTask"
                                                                                         id="colorInnerTask{{$pr_module_task->id_ms_task}}"></div>
                                                                                </div>
                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, '',
                                                                                    false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                    ($pr_module_task->is_checked)?'disabled':'',
                                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                </div>
                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                    {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
                                                                                      ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}

                                                                                </div>


                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                {{Form::open()}}
                                                                                <h4>{{$pr_module_task->name_task}}
                                                                                </h4>

                                                                                <div
                                                                                        class="modal_description_task">
                                                                                    {{$pr_module_task->description_task}}
                                                                                </div>

                                                                                <textarea class="summernote">
                                                                                        {{$pr_module_task->task_description}}
                                                                                </textarea>
                                                                                <a href="javascript:void(0)"
                                                                                   id="button_save_task{{$pr_module_task->id_ms_task}}"
                                                                                   class="btn ls-light-blue-btn
                                                                                       button_save_task"
                                                                                   style="display: none">
                                                                                    Save
                                                                                </a>

                                                                                <div class="modal_description_task
                                                                                user_comment_task"
                                                                                     id="user_comment_task{{$pr_module_task->id_ms_task}}">
                                                                                    @if(strlen($pr_module_task->task_description)>1)

                                                                                        {{$pr_module_task->task_description}}

                                                                                    @endif
                                                                                </div>
                                                                                <a href="javascript:void(0)"
                                                                                   id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                                                                   class="btn ls-light-blue-btn
                                                                                       button_edit_task">
                                                                                    Edit
                                                                                </a>
                                                                                {{Form::close()}}
                                                                                <div class="clear"></div>


                                                                                {{ Form::open(['action'=>'ProjectsController@upload_file',
                                                                                 'class'=>'dropzone dz-clickable',
                                                                                 'id'=>'dropzoneForm'.$pr_module_task->id_ms_task,
                                                                                 'enctype'=>"multipart/form-data"]) }}
                                                                                {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                                                                {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                                                                                <div class="dz-message">
                                                                                    <h4>Drag Photos to Upload</h4>
                                                                                    <span>Or click to browse</span>

                                                                                </div>
                                                                                {{Form::close()}}

                                                                                @if(count($attachments)>0)
                                                                                    <ul class="attachments_list">
                                                                                        @foreach($attachments as $attachment)
                                                                                            <?php    $icon =
                                                                                                    Projects_attach::getIconByAttachment($attachment->extension) ?>

                                                                                            <li id="div_file_{{$attachment->id_projects_attach}}">
                                                                                                <i class="fa {{$icon}}"></i> {{$attachment->name_attachment }}
                                                                                                <a href="javascript:void(0)"
                                                                                                   id="button_delete_file{{$attachment->id_projects_attach}}"
                                                                                                   class="button_delete_file">
                                                                                                    <i class="fa fa-trash-o"></i>
                                                                                                </a>
                                                                                            </li>


                                                                                        @endforeach
                                                                                    </ul>
                                                                                @endif

                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                        class="btn ls-light-blue-btn"
                                                                                        data-dismiss="modal">
                                                                                    Save and Close
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                <div class="add_new_task">
                                                    <i class="fa fa-plus-square-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  Module level 2 Stop  -->
                                @endif
                                @endforeach
                            </div>
                            <!--  Collapsable for Module level 1 Stop -->
                            @else
                                    <!--  Module level 1 Start  -->
                            <div class="row">
                                <div class="row-height">
                                    <?php
                                    // get tasks for module lvl 1
                                    $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $project->id_project);
                                    ?>
                                    <div class="col-xs-2 col-sm-height">
                                        <div class="inside-full-height">
                                            <a href="javascript:void(0)"
                                               class="list-group-item list-group-item-module1"
                                               data-parent="#ModuleMenu1">
                                                        <span class="module-menu-title">
                                                         {{$module->id_name}}</span>

                                                <div class="circle_tasks_left">
                                                    {{count($pr_module_tasks)}}
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- functionalities for tasks module level 1 -->
                                    <div class="col-xs-10 col-sm-height inside-with-border">
                                        <div class="inside-full-height">

                                            @if(count($pr_module_tasks)>0)
                                                @foreach($pr_module_tasks as $pr_module_task)
                                                    <?php $attachments =
                                                            Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                    @if($pr_module_task->task_color  != null &&
                                                        $pr_module_task->task_color != 'transparent')
                                                        <style type="text/css">
                                                            #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                     {
                                                                padding-right: 7px;
                                                                background-color: {{$pr_module_task->task_color}};
                                                            }

                                                            #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                     {
                                                                background-color: {{$pr_module_task->task_color}};
                                                            }
                                                        </style>
                                                    @endif
                                                    <div class="panel-task-border"
                                                         id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                        <div class="panel-task">
                                                            <div class="panel-task-header">
                                                                <div class="ls-button-group">

                                                                    <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                         class="colorPicker">
                                                                        <a class="color"
                                                                           id="color{{$pr_module_task->id_ms_task}}">
                                                                            <div class="colorInner"
                                                                                 id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                        </a>

                                                                        <div class="track"
                                                                             id="track{{$pr_module_task->id_ms_task}}"></div>
                                                                        <ul class="dropdown">
                                                                            <li></li>
                                                                        </ul>
                                                                        <input type="hidden"
                                                                               class="colorInput"
                                                                               id="colorInput{{$pr_module_task->id_ms_task}}"/>

                                                                    </div>
                                                                </div>
                                                                <div class="ls-button-group">
                                                                    <i class="fa fa-info-circle "></i>
                                                                </div>
                                                                <div class="ls-button-group float-right">
                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, '', false,
                                                                    ['class'=>"icheck-green",
                                                                    ($pr_module_task->is_checked == 1)
                                                                    ?'disabled':'',
                                                                    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                </div>
                                                            </div>
                                                            <div class="panel-task-body">
                                                                {{ $pr_module_task->name_task}}
                                                            </div>
                                                            <div class="panel-task-footer">
                                                                <ul>
                                                                    <li>
                                                                        <i class="fa fa-paperclip"></i> {{count($attachments)}}
                                                                    </li>
                                                                    <li>Edit</li>

                                                                </ul>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            @endif
                                            <div class="add_new_task">
                                                <i class="fa fa-plus-square-o"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  Module level 1 Stop -->
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop