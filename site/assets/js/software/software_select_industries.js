$(document).on('click', '.add-check-all-icon', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-uncheck-all-icon modal-add-check-all-icon modal-add-check-all-type modal-add-uncheck-all-type')
                      .addClass('modal-add-check-all-icon').end()
                      .modal('show');
});
//select all industries
$(document).on('click', '.modal-add-check-all-icon', function(){
    $('.add-check-all-icon').hide();
    $('.add-uncheck-all-icon').show();
    $('.add-uncheck-all-icon-inactive').hide();
    $('.add-check-all-icon-inactive').show();
    $(".select_industry div.icheckbox_minimal-green").each(function(){
        $(this).addClass('checked');
        $(this).find('input').prop('checked', true);
    });
    $(".select_industry div.tab-pane").each(function(){
        $(this).find('.add-check-all-type').hide();
        $(this).find('.add-uncheck-all-type').show();
        $(this).find('.add-uncheck-all-type-inactive').hide();
        $(this).find('.add-check-all-type-inactive').show();
    });
    $('#modalConfirm').modal('hide');
});

$(document).on('click', '.add-uncheck-all-icon', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-uncheck-all-icon modal-add-check-all-icon modal-add-check-all-type modal-add-uncheck-all-type')
                      .addClass('modal-add-uncheck-all-icon').end()
                      .modal('show');
});

//uncheck all industries
$(document).on('click', '.modal-add-uncheck-all-icon', function(){
    $('.add-uncheck-all-icon').hide();
    $('.add-check-all-icon').show();
    $('.add-uncheck-all-icon-inactive').show();
    $('.add-check-all-icon-inactive').hide();
    $(".select_industry div.icheckbox_minimal-green").each(function(){
        $(this).removeClass('checked');
        $(this).find('input').prop('checked', false);
    });
    $(".select_industry div.tab-pane").each(function(){
        $(this).find('.add-check-all-type').show();
        $(this).find('.add-uncheck-all-type').hide();
        $(this).find('.add-uncheck-all-type-inactive').show();
        $(this).find('.add-check-all-type-inactive').hide();
    });
    $('#modalConfirm').modal('hide');
});

$(document).on('click', '.add-check-all-type', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var categoryId = $(this).attr('data-category-id');
    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').addClass('modal-add-check-all-type').end()
                      .modal('show');
   $('.modal-add-check-all-type').attr('data-category-id', categoryId);
});

//select all industries from one category
$(document).on('click', '.modal-add-check-all-type', function(){
    $('.add-uncheck-all-icon').show();
    $('.add-uncheck-all-icon-inactive').hide();
    $('.uncheck-all-icon').show();
    $('.uncheck-all-icon-inactive').hide();

    var catId = $(this).attr('data-category-id');
    var all_unchecked_inputs = $(".select_industry input:not(:checked)").length;
    var category_unchecked_inputs = $('#ind' + catId).find('.industry-container input:not(:checked)').length;

    if(category_unchecked_inputs == all_unchecked_inputs) {
        $('.add-check-all-icon').hide();
        $('.add-check-all-icon-inactive').show();
    }

    $('#ind' + catId).find('.add-check-all-type').hide();
    $('#ind' + catId).find('.add-uncheck-all-type').show();
    $('#ind' + catId).find('.add-uncheck-all-type-inactive').hide();
    $('#ind' + catId).find('.add-check-all-type-inactive').show();
    $('#ind' + catId).find(".industry-container div.icheckbox_minimal-green").each(function(){
        $(this).addClass('checked');
        $(this).find('input').prop('checked', true);
    });
    $('#modalConfirm').find('.modal-confirm-btn a').removeClass('.modal-add-check-all-type');
    $('#modalConfirm').modal('hide');
});

$(document).on('click', '.add-uncheck-all-type', function(){
    var title = "Confirm";
    var msg = $(this).attr('data-message');
    var categoryId = $(this).attr('data-category-id');
    $('#modalConfirm').find('#frm_body').html(msg).end()
                      .find('#frm_title').html(title).end()
                      .find('.modal-confirm-btn a').removeAttr('data-category-id data-task-id data-url').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-type modal-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').removeClass('modal-check-all-icon modal-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-icon modal-add-uncheck-all-icon').end()
                      .find('.modal-confirm-btn a').removeClass('modal-add-check-all-type modal-add-uncheck-all-type').end()
                      .find('.modal-confirm-btn a').addClass('modal-add-uncheck-all-type').end()
                      .modal('show');
   $('.modal-add-uncheck-all-type').attr('data-category-id', categoryId);
});

//uncheck all industries from one category
$(document).on('click', '.modal-add-uncheck-all-type', function(){
    $('.add-check-all-icon').show();
    $('.add-check-all-icon-inactive').hide();

    var catId = $(this).attr('data-category-id');
    var total_inputs = $(".industry-container").length;
    var category_inputs = $('#ind' + catId).find('.industry-container').length;
    var checked_inputs = $(".select_industry input:checked").length;

    if(checked_inputs <= category_inputs) {
        $('.add-uncheck-all-icon').hide();
        $('.add-uncheck-all-icon-inactive').show();
        $('.uncheck-all-icon').hide();
        $('.uncheck-all-icon-inactive').show();
    }

    $('#ind' + catId).find('.add-uncheck-all-type').hide();
    $('#ind' + catId).find('.add-check-all-type').show();
    $('#ind' + catId).find('.add-uncheck-all-type-inactive').show();
    $('#ind' + catId).find('.add-check-all-type-inactive').hide();
    $('#ind' + catId).find(".industry-container div.icheckbox_minimal-green").each(function(){
        $(this).removeClass('checked');
        $(this).find('input').prop('checked', false);
    });
    $('#modalConfirm').find('.modal-confirm-btn a').removeClass('.modal-add-uncheck-all-type');
    $('#modalConfirm').modal('hide');
});

$(document).on('ifChecked', ".task_check", function (event) {
    if($(".select_industry input:checked").length == $(".industry-container").length) {
        $('.add-check-all-icon').hide();
        $('.add-check-all-icon-inactive').show();
        $('.add-uncheck-all-icon').show();
        $('.add-uncheck-all-icon-inactive').hide();
    }

    if($(".select_industry input:checked").length > 0 && $(".select_industry input:checked").length < $(".industry-container").length) {
        $('.add-check-all-icon').show();
        $('.add-check-all-icon-inactive').hide();
        $('.add-uncheck-all-icon').show();
        $('.add-uncheck-all-icon-inactive').hide();
    }

    var category_inputs = $(this).parents('.tab-pane').find('.industry-container').length;
    var selected_inputs = $(this).parents('.tab-pane').find('input:checked').length;

    if(category_inputs == selected_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').hide();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }

    if(selected_inputs > 0 && selected_inputs < category_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }
});

$(document).on("ifUnchecked", ".task_check", function () {
    if($(".select_industry input:checked").length == 0) {
        $('.add-uncheck-all-icon').hide();
        $('.add-uncheck-all-icon-inactive').show();
        $('.add-check-all-icon').show();
        $('.add-check-all-icon-inactive').hide();
    }

    if($(".select_industry input:checked").length > 0 && $(".select_industry input:checked").length < $(".industry-container").length) {
        $('.add-check-all-icon').show();
        $('.add-check-all-icon-inactive').hide();
        $('.add-uncheck-all-icon').show();
        $('.add-uncheck-all-icon-inactive').hide();
    }

    var category_inputs = $(this).parents('.tab-pane').find('.industry-container').length;
    var selected_inputs = $(this).parents('.tab-pane').find('input:checked').length;

    if(selected_inputs == 0) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').show();
    }

    if(selected_inputs > 0 && selected_inputs < category_inputs) {
        $(this).parents('.tab-pane').find('.add-check-all-type').show();
        $(this).parents('.tab-pane').find('.add-check-all-type-inactive').hide();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type').show();
        $(this).parents('.tab-pane').find('.add-uncheck-all-type-inactive').hide();
    }
});
