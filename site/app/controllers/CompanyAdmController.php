<?php
use Illuminate\Support\MessageBag;

class CompanyAdmController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {


        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);


        if (in_array(1, $fk_group)) {
            $companies = Company::orderBy('is_blocked', 'desc')
                ->orderBy('created_at', 'desc')
                ->get();

            return View::make('admin.company.index', compact('companies'));
            // make view
        } else {
            return App::abort(404);
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        allowOnlySuperAdmin();
        return View::make('admin.company.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Input::hasFile('logo')) {
            $image_details = App::make('UsersController')->SaveImage('logo');
        } else {
            $image_details = null;
        }

        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }

        $company = new Company();
        $company->fill(Input::except('logo'));
        if ($name_image) {
            $company->logo_company = $name_image;
        }
        $company->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();
        $company = Admin_company::getFirstById($id);
        return View::make('admin.company.admin_company', compact('company'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $id_company = $id;
        $titles = Users_title::getTitle();
        $personal_titles_new = Users_personal_title::getPersonal_Title(1);
        $personal_titles = Users_personal_title::getPersonal_Title();

        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        // if user is company admin

        $user = User::getFirstById($id_user);

        // get user's company details
        $company = Company::getFirstById($id_company);
        // get the headquarters
        $main_headquarter = Company_headquarters::getMainHeadquarter($id_company);

        $headquarters = Company_headquarters::getHeadquartersLocationsNoBlocked($id_company);
        // total number of users
        $all_users = User::getAllUsersDetailsByCompany($id_company);
        $users_blocked = User::whereFk_company($id_company)
            ->whereBlocked(1)
            ->count();
        foreach ($headquarters as $headquarter) {
            $nr = 0;
            $users = User::whereFk_headquarter($headquarter->id_headquarter)->get();
            $nr = count($users);
            $nr_users_headquarter[$headquarter->id_headquarter] = $nr;
        }

        $projects = Project::whereFk_company($id_company)->get();
        // make view


        return View::make('admin.company.edit', compact('user', 'company', 'headquarters', 'fk_group', 'personal_titles_new',
            'nr_users_headquarter', 'all_users', 'users_blocked', 'titles', 'personal_titles', 'projects', 'main_headquarter'));
        // make view


//        $company = Company::whereId_company($id)->first();
//        $id_user = Auth::user()->id;
//        $fk_group = User_groups::getGroupByUser($id_user);
//        // if user is company admin
//        if (in_array(1, $fk_group)) {
//
//            return View::make('admin.company.index', compact('company'));
//        }
    }

    public function update_adm_company()
    {
        $id = Input::get('id_company');
        if (Input::hasFile('logo')) {
            $image_details = App::make('UsersController')->SaveImage('logo');
        } else {
            $image_details = null;
        }

        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }

        $company = Admin_company::getFirstById($id);
        $company->fill(Input::except(array('logo')));
        if ($name_image) {
            $company->logo_company = $name_image;
        }
        $company->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_adm($id_company)
    {
        /* DB::beginTransaction();
     
         try {
             User::where('fk_headquarter', '=', $id_headquarter)->delete();
         } catch (\Exception $e) {
             DB::rollback();
             $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
             return Redirect::back()->withErrors($errors);
         }
     
         try {
             Company_headquarters::where('id_headquarter', '=', $id_headquarter)->delete();
         } catch (\Exception $e) {
             DB::rollback();
             $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
             return Redirect::back()->withErrors($errors);
         }
     
     
         DB::commit();*/
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);


    }

    public function approveCompany($id_company)
    {
        $company = Company::findOrFail($id_company);
        $company->is_blocked = 0;
        $company->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function blockCompany($id_company)
    {
        $company = Company::findOrFail($id_company);
        $company->is_blocked = 1;
        $company->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }
}
