@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('footer_scripts')

    <script src="{{ URL::asset('assets/js/software/dropzone.js')}}"></script>
    <!-- Module menu tree style -->
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
    @include('admin/software/tree_script')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
    {{--<script src="{{ URL::asset('assets/js/jquery.sticky.js')}}"></script>--}}
    @include('admin/scripts/ajax_scripts')
    <script type="text/javascript">


        $(document).ready(function () {
            $(".dropzone").dropzone({uploadMultiple: false});
            if ($('#scrollTo').length != 0) {
                $('html, body').animate({
                    scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50
                }, 'slow');
            }
            hide_empty_modules();
        });
        function hide_empty_modules() {
            $(".parent_li").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }

            });
            $(".li_not_selectable").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }
            });
        }

        $(document).on('ifChecked', ".task_check", function (event) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('task_check_', "");
            var arr = id_nr.split('_');
            var data = {
                'id_industry': arr[0],
                'id_ms_task': arr[1],
                'is_checked': 1
            };
            // save to database
            jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
                    .done(function (msg) {
                        $.amaran({
                            'theme': 'colorful',
                            'content': {
                                message: msg,
                                bgcolor: '#324e59',
                                color: '#fff'
                            }
                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });

        // edit task - remove task from an industry (on check with ajax)
        $(document).on("ifUnchecked", ".task_check", function () {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('task_check_', "");
            var arr = id_nr.split('_');
            var data = {
                'id_industry': arr[0],
                'id_ms_task': arr[1],
                'is_checked': 0
            };
            // save to database
            jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
                    .done(function (msg) {
                        $.amaran({
                            'theme': 'colorful',
                            'content': {
                                message: msg,
                                bgcolor: '#324e59',
                                color: '#fff'
                            }
                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });

        $(document).on('switchChange.bootstrapSwitch', function (event, state) {
            var id = jQuery('.is_default_task').attr('id');
            var id_nr = id.replace('is_default_task_', '');
            var data = {
                'id_ms_task': id_nr,
                'is_default': state
            };
            //console.log(data);
            jQuery.post('{{  route("ajax.set_task_default") }}', data)
            .done(function (msg) {
//                                                alert(msg)
            })
            .fail(function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            });

        });

        // edit an existing task
        function modal_dialog_edit_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_task/" + id;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                // call dropzone - drag and drop files
                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code

                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');


                                        var data = {
                                            'id_software_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
                                    alert('aaaa');
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // open add new task for an existing submodule
        function modal_dialog_add_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task/" + id;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code

                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');


                                        var data = {
                                            'id_software_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // open add new task for a newly created module/submodule
        function modal_dialog_add_task_new(id) {

            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task_new/" + id + "/<?php print $software->id_software; ?>";
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // drag and drop order tasks/ module
        $(function () {

            $('.sortable_task').sortable({
                axis: 'y',
                opacity: 0.7,
                group: '.sortable_task',
                tolerance: 'pointer',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();
//                    alert(list_sortable)
                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable,
                    };

                    $.post('{{  route("ajax.set_order_adm_task") }}', data)
                            .done(function (msg) {
//                            alert('ok')
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                }
            }); // fin sortable
            // do this for every level
            for (var level = 0; level <= 5; level++) {
                $('.sortable_modules_' + level).sortable({
                    axis: 'y',
                    opacity: 0.7,
                    handle: 'span',
                    update: function (event, ui) {
                        var list_sortable = $(this).sortable('toArray').toString();
                        // change order in the database using Ajax
                        var data = {
                            'list_order': list_sortable
                        };

                        $.post('{{  route("ajax.set_order_adm_modules") }}', data)
                                .done(function (msg) {
                                    // success
                                })
                                .fail(function (xhr, textStatus, errorThrown) {
                                    //  alert(xhr.responseText);
                                });
                    }
                }); // fin sortable
            }

        });

    </script>

@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-pencil-square-o"></i>
            <h3 class="box-title">  {{trans('messages.edit')}}   {{trans('messages.task')}} {{$task->name_Software}} </h3>
        </div>
        <!--<div>{{$task}}</div>-->
        <!-- /.box-header -->
        <?php $is_default = DB::table('pr_ms_tasks')
                            ->where('fk_task', $task->id_task)
                            ->first(); ?>
        <div class="box-body">
            <div class="row">
                <a href="javascript:void(0)" onclick="window.location='{{ url("task") }}'">
                    <div class="hide_controller">
                        <i class="fa fa-times"></i>
                    </div>
                </a>
                <div class="col-xs-12 text-right">
                    <i class="fa fa-info-circle   " style="color: #324E59; font-size: 18px; margin-top: 2px " title=" {{trans('messages.label_default_task_standard')}}"></i>
                    {{Form::checkbox('is_default',1,$is_default->is_default, ['class'=>'switchCheckBox inline is_default_task','id'=>'is_default_task_'.$is_default->id_ms_task,'data-size'=>"mini"])}}
                    <a href="{{URL::to('delete_task_from_structure/'.$is_default->id_ms_task)}}" onclick="return confirm('{{trans('validation.confirmation_delete_task')}}')" class="btn inline btn-round btn-l btn-block btn-danger">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
                <div class="col-xs-12 mgtp-10 ">
                    <div class="title_line"> {{($is_default->is_default == 0)?'<i class="fa fa-bell-o" style="color: #D9534F"></i>':''}}  {{ $task->name_task  }} </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <section class="ac-container">
                        <div>
                            <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                            <label for="ac-1">{{trans('messages.label_sidebar_attachments')}}</label>
                            <article class="ac-any">
                                 <div class="article_content">
                                    {{Form::open(['action'=>'SoftwareController@upload_file_software', 'class'=>'dropzone dz-clickable','id'=>'dropzoneForm'.$task->id_task, 'enctype'=>"multipart/form-data"]) }}
                                    {{Form::hidden('id_ms_task', $task->id_task,["id"=>"id_ms_task"])}}
                                        <div class="dz-message">
                                            <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                                        </div>
                                    {{Form::close()}}
                                    <?php $attachments = DB::table('pr_software_attachments')
                                        ->where('fk_ms_task', $task->id_task)
                                        ->get(); ?>
                                    @if(count($attachments)>0)
                                        <div class="modal-attachments">
                                            <div class="row">
                                                @foreach($attachments as $attachment)
                                                    <?php    $icon = Software_attach::getIconByAttachment($attachment->extension) ?>
                                                    <div class="col-xs-3 col-attachments"
                                                         id="col-attachments{{$attachment->id_software_attachment}}">
                                                        <div style="display: block; width: 100%">
                                                            <a href="{{ Url::to('/')}}/images/attach_software/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                               download class="button_download_file" target="_blank">
                                                                <div class="file_attachments" style="background: {{$icon}} "
                                                                     id="div_file_{{$attachment->id_software_attachment}}"
                                                                     title="{{$attachment->name_attachment }}">
                                                                </div>
                                                            </a>

                                                            <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment->id_software_attachment}}">
                                                                <a href="{{ Url::to('/')}}/images/attach_software/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                                   class="button_download_file" target="_blank">
                                                                    <i class="fa fa-search"></i>
                                                                </a><br>
                                                                <a href="javascript:void(0)"
                                                                   id="button_delete_file{{$attachment->id_software_attachment}}"
                                                                   class="button_delete_file">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        {{--<div style="display: block; width: 100%">--}}
                                                        {{--<div class="attachments_name_file" id="attachments_name_file{{$attachment->id_projects_attach}}">--}}
                                                        {{--{{$attachment->name_attachment }}--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                 </div>
                            </article>
                        </div>
                    </section>
                    {{ Form::model($is_default,['route'=>['task.update',$is_default->id_ms_task], 'method'=>'PATCH'])}}
                        <div class="row">
                            <div class="col-md-12">
                                <section class="ac-container">
                                    <div>
                                        <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                                        <label for="ac-2">{{trans('messages.label_details')}}</label>
                                        <article class="ac-any">
                                            <div class="article_content">
                                                <div class="row" style="margin: 10px 0">
                                                    <div class="col-xs-6">
                                                        {{Form::radio('edit_task',2,true,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_current_task')}}
                                                    </div>
                                                    <div class="col-xs-6">
                                                        {{Form::radio('edit_task',1,false,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_all_tasks')}}
                                                    </div>
                                                </div>
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs icon-tab">
                                                    <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                                                    <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content tab-border">
                                                    <div class="tab-pane fade in active" id="deutsch">
                                                        <div class="form-group">
                                                            {{ Form::text('name_task',  $task->getAttribute('name_task','de'), [
                                                                'class'=>"form-control","placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'), "required",
                                                                'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                                                "oninput"=>"this.setCustomValidity('')"
                                                              ]) }}
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="description_task" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ strip_tags($task->getAttribute('description_task','de'))}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="engl">
                                                        <div class="form-group">
                                                            {{ Form::text('name_task_en', $task->getAttribute('name_task','en'), [
                                                                'class'=>"form-control","placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                                                "required",
                                                                'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                                                "oninput"=>"this.setCustomValidity('')"
                                                              ]) }}
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="description_task_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ strip_tags($task->getAttribute('description_task','en'))}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mgtp-10">
                                                    {{ Form::text('code_task',  $task->code_task, ['class'=>"form-control","placeholder"=> trans('messages.nav_task_code') ]) }}
                                                </div>
                                                <div class="row" style="margin: 10px 0">
                                                    <div class="col-xs-6">
                                                        {{Form::checkbox('is_checked' ,1, $is_default->is_checked,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                                                    </div>
                                                    <div class="col-xs-6">
                                                        {{Form::checkbox('is_mandatory' ,1, $is_default->is_mandatory,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        {{trans('messages.label_priority')}}
                                                        <select name="lvl_priority" id="task_pair" style="width: 40px; padding: 3px 0; background: #fff">
                                                            <option value=""> -</option>
                                                            @foreach($priorities as $priority)
                                                                <option value="{{$priority->lvl_priority}}" {{($is_default->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        {{trans('messages.label_pair')}} {{trans('messages.nav_task')}}
                                                        <?php
                                                        $selected_pair = Task_pair::getFirstById($is_default->fk_pair);
                                                        if (count($selected_pair) == 1) {
                                                            $selected_color = $selected_pair->color_pair;
                                                        } else {
                                                            $selected_color = '#fff';
                                                        }
                                                        ?>
                                                        <select name="fk_pair" id="task_pair" style="width: 40px; padding: 3px 0; background: {{$selected_color}}">
                                                            <option value="0" style=" background: #fff"> -</option>
                                                            @foreach($pairs as $pair)
                                                                <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($is_default->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div>
                                        <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                                        <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                                        <article class="ac-any">
                                            <div class="article_content">
                                                <ul class="nav nav-tabs icon-tab">
                                                    @foreach($industries_types as $industry_type)
                                                        @if($industry_type->id_industry_type == 1)
                                                            <li class="active">
                                                                <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                                                    <span><i class="fa {{$industry_type->font_icon}}"></i> </span>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li>
                                                                <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                                                    <span><i class="fa {{$industry_type->font_icon}}"></i> </span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                                <div class="tab-content tab-border select_industry">
                                                    @foreach($industries_types as $industry_type)
                                                        <?php
                                                            $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                                        ?>
                                                        <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">
                                                            <div style="display: block">
                                                                {{$industry_type->name_industry_type}}
                                                            </div>
                                                            <hr>
                                                            @foreach($industries as $industry)
                                                                <div style="display: block">
                                                                    <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$is_default->id_ms_task}}"
                                                                        {{(Industry_ms::isIndustryChecked($industry->id_industry, $is_default->id_ms_task) == true)?'checked':''}}
                                                                        name="fk_industry[]" value="{{$industry->id_industry}}" style="display: inline-block"> {{$industry->name_industry}}
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <div>
                                        <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                                        <label for="ac4">{{trans('messages.label_synonyms')}}</label>
                                        <article class="ac-any">
                                            <div class="article_content">
                                                <input type="text" class="input-tags" name="meta_tags" value="{{$task->getAttribute('meta_tags')}}"/>
                                            </div>
                                        </article>
                                    </div>
                                    <div>
                                        <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                                        <label for="ac-4">{{trans('messages.label_address_code')}}</label>
                                        <article class="ac-any">
                                            <div class="article_content">
                                                <input type="text" class="input-tags" name="address_code" value="{{$task->getAttribute('address_code')}}"/>
                                            </div>
                                        </article>
                                    </div>
                                </section>

                                {{Form::hidden('id_ms_task' ,$is_default->id_ms_task)}}
                                    <button type="submit" class="btn ls-light-blue-btn btn-block">
                                        {{trans('messages.act_save')}}
                                    </button>
                                    <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
                                        {{trans('messages.act_close')}}
                                    </button>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div id="task_page_details_parent">
            <div class="task_page_details" id="task_page_details"></div>
        </div>
        <!-- /.box-body -->
    </div>
@stop
