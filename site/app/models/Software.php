<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:36
 */
class Software extends Eloquent
{
    public static $rules = [
        'name_software' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_software';
    protected $table = 'pr_software';
    protected $guarded = ['id_software'];
    protected $fillable = ['name_software', 'description_software', 'name_software_en', 'description_software_en'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Software::where('id_software', '=', $id)->first();
        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_software', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_software', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }
    }
}