<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:28
 */
class Adm_sprint extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_sprint' => 'required',
    ];
    public $primaryKey = 'id_sprint';
    protected $table = 'adm_sprints';
    protected $guarded = ['id_sprint'];
    protected $fillable = ['name_sprint', 'fk_release', 'comment', 'is_closed'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_sprint::where('id_sprint', '=', $id)->first();
        return $list;
    }
}