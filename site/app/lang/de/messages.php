<?php
/*
  |--------------------------------------------------------------------------
  | Messages Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the template library to build
  | the buttons and other messages. You are free to change them to anything
  | you want to customize your views to better match your application.
  |
 */
return LanguageTrans::getList('de', 1);


return array(
    /*
      |--------------------------------------------------------------------------
      | Messages Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the template library to build
      | the buttons and other messages. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */
    // title
    'title_long'             => 'Lastenheft.Expert',
    'title_short'            => 'Lastenheft',

    // actions
    'act_permanently_delete' => 'Endgültig löschen',
    'act_add'                => 'Fügen Sie',
    'act_delete'             => 'Löschen',
    'act_undelete'           => 'Zeigen',
    'act_link'               => 'Verbinden',
    'act_edit'               => 'Ändern',
    'act_modify'             => 'Ändern',
    'act_details'            => 'Einzelheiten',
    'act_cancel'             => 'Abbrechen',
    'act_save_close'         => 'Speichern und Schließen',
    'act_close'              => 'Schließen',
    'act_save'               => 'Speichern',
    'act_create'             => 'hinzufügen',
    'Yes'                    => 'Ja',
    'No'                     => 'Nein',
    'language'               => 'Sprache',

    // navigation
    'nav_dashboard'          => 'Dashboard',
    'nav_software'           => 'Anwendungen',
    'nav_industry'           => 'Branche',
    'nav_industries'         => 'Branchen',
    'nav_settings'           => 'Einstellungen',
    'nav_module'             => 'Modul',
    'nav_submodule'          => 'Submodul',
    'nav_task'               => 'Aufgabe',
    'nav_tasks'              => 'Aufgaben',
    'nav_new_project'        => 'Neues Projekt erstellen',
    'nav_companies'          => 'Firmen',
    'nav_company'            => 'Firma',
    'nav_users'              => 'Benutzer',
    'nav_user'               => 'Benutzer',
    'nav_logout'             => 'Abmelden',
    'nav_task_manager'       => 'Task Manager',
    'nav_edit_profile'       => 'Profil bearbeiten',
    'nav_change_password'    => 'Kennwort ändern',
    'nav_profile'            => 'Profil',
    'nav_lists'              => 'Listen',
    'nav_choose_soft'        => 'Login auswählen',
    'nav_consultants'        => 'Berater',
    'nav_consultant'         => 'Berater',
    'nav_posts_pages'        => 'Posts Pages',
    'nav_headquarter'        => 'Filiale',

    'label_details'         => 'Allgemeine Daten',
    'label_mandatory'       => 'Erforderliche',
    'label_checked'         => 'Überprüft',
    'label_suggested'       => 'Empfohlene',
    'label_priority'        => 'Gewichtung',
    'label_pair'            => 'Paar',
    'label_select_industry' => 'Branchenzuordnung',
    'label_synonyms'        => 'Synonyme / Tags',
    'label_address_code'    => 'Adress-Code der Software-Anbieter',
    'label_default'         => 'Standard',
    'label_is_not_offer'    => ' Von der Ausschreibung ausgeschlossen',
    'label_is_control'      => 'Kontrolfrage',
    'label_attachments'     => 'Befestigung',
    'label_full_name'       => 'Vollständiger Name',
    'label_first_name'      => 'Vorname',
    'label_last_name'       => 'Nachname',
    'label_telephone'       => 'Telefon',
    'label_email'           => 'Email',
    'label_job'             => 'Arbeitsplatz',
    'label_mobile'          => 'Mobiltelepnon',
    'label_is_licitation'   => 'Participates in a licitation',
    'label_has_consultant'  => 'Project needs consultant',
    'label_slug'            => 'Slug Link',
    'label_module_project'  => 'Project has predefined modules',
    'label_empty_project'   => 'Project is empty',

    // table headers
    'th_id'                 => 'Identifier',
    'th_name'               => 'Name',
    'th_description'        => 'Beschreibung',
    'th_type'               => 'Typ',
    'th_is_deleted'         => 'Ist deaktiviert',

    // form labels
    'l_industry_type'       => 'Industrie-Typ',
    'l_name'                => 'Name',
    'l_new'                 => 'Neue',
    'l_for'                 => 'für',
    'l_edit_all_tasks'      => 'Aufgabe und Verweisen ändern',
    'l_edit_current_task'   => 'Aktuelle Aufgabe ändern',
    'l_description'         => 'Beschreibung',
    'l_german'              => '[DE]',
    'l_rom'                 => '[RO]',
    'l_eng'                 => '[EN]',
    'l_code'                => 'code',
);
?>