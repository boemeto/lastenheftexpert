{{ Form::model($pr_module_structure,['route'=>['module.update',$module->id_module], 'method'=>'PATCH'])}}
<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-xs-12 text-right">
      <div class="row">
          <div class="col-md-2 pull-right pd-5" style="margin-top: 4px; width: 70px">
                {{Form::checkbox('is_default' ,1, $module->is_default,['class'=>'switchCheckBox','data-size'=>"mini", 'style'=>'display:inline-block'])}}
          </div>
          <div class="col-md-2 pull-right pd-5">
              <i class="fa fa-info-circle" title="{{trans('messages.label_default_module_standard')}}"></i>
          </div>
      </div>
    </div>
    <div class="col-xs-12">
        <div class="title_line">  {{ $module->name_module  }} </div>
    </div>
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_module',  $module->getAttribute('name_module','de'), [
                                      'class'=>"form-control",
                                      "placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_name'),
                                      "required",
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_module" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $module->getAttribute('description_module','de')}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_module_en', $module->getAttribute('name_module','en'), [
                                      'class'=>"form-control",
                                      "placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_name'),
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_module_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $module->getAttribute('description_module','en')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mgtp-10">
                            {{ Form::text('code_module',  $module->code_module, ['class'=>"form-control","placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_code')]) }}
                        </div>
                    </div>
                </article>
            </div>
                              <?php?>
                    <i></i>
        </section>
    </div>
    <div class="col-md-12">
        {{Form::hidden('id_module_structure' ,$id_module_structure)}}
        <button type="submit" class="btn ls-light-blue-btn btn-block">
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
        {{--<a href="{{URL::to('delete_task_from_structure/'.$row->id_ms_task)}}" class="btn btn-block btn-danger">--}}
        {{--{{trans('messages.nav_module')}} {{trans('messages.act_delete')}}--}}
        {{--</a>--}}

    </div>
</div>

<br>
<br>
{{Form::close()}}
