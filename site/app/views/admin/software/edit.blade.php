{{ Form::model($software,['route'=>['software.update',$software->id_software], 'method'=>'PATCH'])}}

<div class="row">
    <div class="col-md-12">
        <a href="javascript:void(0)" onclick="hide_modal_box();">
            <div class="hide_controller delay_hide_controller">
                <i class="fa fa-times"></i>
            </div>
        </a>
        <div class="title_line">    {{$software->name_software }} </div>
    </div>
    <div class="col-md-12">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs icon-tab">
            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content tab-border">
            <div class="tab-pane fade in active" id="deutsch">
                <div class="input-group ls-group-input">
                    {{ Form::text('name_software',  $software->getAttribute('name_software','de'), [
                        'class'=>"form-control",
                        "placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_name'),
                        "required",
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                    <span class="input-group-addon">DE</span>
                </div>
                <div class="form-group">
                    <textarea name="description_software" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $software->getAttribute('description_software','de')}}</textarea>
                </div>
            </div>
            <div class="tab-pane fade" id="engl">
                <div class="input-group ls-group-input">
                    {{ Form::text('name_software_en', $software->getAttribute('name_software','en'), [
                        'class'=>"form-control",
                        "placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_name'),
                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                        "oninput"=>"this.setCustomValidity('')"
                      ]) }}
                    <span class="input-group-addon">EN</span>
                </div>
                <div class="form-group">
                    <textarea name="description_software_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $software->getAttribute('description_software','en')}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <br>
        <button type="submit" class='btn ls-light-blue-btn btn-block'>
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>

{{Form::close()}}

<script type="text/javascript">
    $(".delay_hide_controller").parent().hide();
    $(".delay_hide_controller").parent().fadeIn(500);
    $(document).on('click', '.delay_hide_controller', function() {
        $(".delay_hide_controller").parent().hide();
    });
</script>
