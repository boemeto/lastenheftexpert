<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmCommentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_comments', function ($table) {
            $table->increments('id_comment')->unsigned();
            $table->text('comment')->nullable();
            $table->integer('fk_user')->nullable();
            $table->integer('fk_task')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_comments');
    }
}