<?php
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Request;

class SoftwareController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $software = Software::orderBy('name_software')->paginate(12);
        $pagination = $software->appends(
            array(
                'sort'  => 'name_software',
                'order' => 'asc'
            ))->links();
        return View::make('admin.software.index', compact('software', 'pagination'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.software.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Software::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(Software::$messages);
        }
        $software = new Software();
        $software->name_software = ucwords(strtolower(Input::get('name_software')));
        $software->name_software_en = ucwords(strtolower(Input::get('name_software_en')));
        $software->description_software = Input::get('description_software');
        $software->description_software_en = Input::get('description_software_en');
        $software->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        if(!empty(Input::get('tree'))) {
            //to do - get fk_id, fk_ms task
            $structure_module = new Module_structure();
            $structure_module->fk_software = $software->id_software;
            $structure_module->fk_id = 40; //id_module
            $structure_module->fk_parent = 0;
            $structure_module->id_type = 1;
            $structure_module->level = 1;
            $structure_module->fk_module_parent = 40; //id_module
            $structure_module->is_default = 0;
            $structure_module->save();

            $industry_ms = new Industry_ms();
            $industry_ms->fk_industry = Input::get('fk_industry');
            $industry_ms->fk_ms_task = 4377;
            $industry_ms->fk_module_structure = $structure_module->id_module_structure;
            $industry_ms->save();
            Session::flash('scrollTo', 'software'.$software->id_software);
            return Redirect::back()->withErrors($errors);
        } else {
            Session::flash('scrollTo', $software->id_software);
            return Redirect::back()->withErrors($errors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();
        $module_structure = Module_structure::getModuleByLvl($id, 1, 0, 0);
        $yesNo = Dictionary::getOptions();
        $software = Software::getFirstById($id);
        $priorities = Priority::getAllPriorities();
        $pairs = Task_pair::all();
        $custom_task = Dictionary::getOptions(4, 1);
        $set_checked = Dictionary::getOptions(5, 1);
        $set_attach = Dictionary::getOptions(6, 1);
        $has_filters = 0;
        $export_file = Project_export_files::getLastBySoftwareId($id);
        $all_softwares = Software::all();

        if (Input::has('meta_tags')) {
            $meta_tags = Input::get('meta_tags');
        } else {
            $meta_tags = '';
        }
        if (Input::has('address_code')) {
            $address_code = Input::get('address_code');
        } else {
            $address_code = '';
        }
        if (Input::has('fk_pair')) {
            $fk_pair = Input::get('fk_pair');
        } else {
            $fk_pair = 0;
        }
        if (Input::has('fk_priority')) {
            $fk_priority = Input::get('fk_priority');
        } else {
            $fk_priority = 0;
        }
        if (Input::has('fk_checked')) {
            $has_filters = 1;
            $fk_checked = Input::get('fk_checked');
        } else {
            $fk_checked = '';
        }
        if (Input::has('fk_attach')) {
            $has_filters = 1;
            $fk_attach = Input::get('fk_attach');
        } else {
            $fk_attach = '';
        }
        if (Input::has('is_default')) {
            $has_filters = 1;
            $is_default = Input::get('is_default');
        } else {
            $is_default = 0;
        }

        return View::make('admin.software.show', compact('software', 'yesNo', 'module_structure', 'all_softwares',
            'pairs', 'fk_pair', 'address_code', 'meta_tags', 'priorities', 'fk_priority', 'export_file',
            'custom_task', 'set_checked', 'set_attach', 'has_filters', 'fk_checked', 'fk_attach', 'is_default'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $software = Software::getFirstById($id);

        return View::make('admin.software.edit', compact('software'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $software = Software::getFirstById($id);

        if (!Software::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(Software::$messages);
        }

        Session::flash('scrollTo', 'software'.$id);
        $software->fill(Input::all());

        if (Input::has('deactivate') && Input::get('deactivate') == 1) {
            if ($software->is_deleted == 1) {
                $software->is_deleted = 0;
            } else {
                $software->is_deleted = 1;
            }
        }

        $software->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $software = Software::getFirstById($id);
        $software->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.delete_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function viewTaskModuleStructure($id)
    {
        $module_structure = Module_structure::where('fk_id', '=', $id)
            ->where('id_type', '=', '1')->first();
        $id_module_structure = $module_structure->id_module_structure;
        $fk_software = $module_structure->fk_software;
        $module_children = Module_structure::getModuleChildren($id_module_structure);
        return View::make('admin.software.ajax_task_module_structure', compact('module_children', 'fk_software'));
    }

    public function modal_dialog_edit_module($id_module_structure)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);

        $industries_types = Industry_type::all();
        if ($pr_module_structure->id_type == 1) {
            $module = Module::getFirstById($pr_module_structure->fk_id);
            return View::make('admin.software.modal_dialog_edit_module', compact('pr_module_structure', 'id_module_structure',
                'module', 'industries_types'));
        } else {
            $submodule = Submodule::getFirstById($pr_module_structure->fk_id);
            return View::make('admin.software.modal_dialog_edit_submodule', compact('pr_module_structure', 'id_module_structure',
                'submodule', 'industries_types'));
        }
    }

    public function modal_dialog_edit_project_module($id_module_structure)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);

        $industries_types = Industry_type::all();
        if ($pr_module_structure->id_type == 1) {
            $module = Module::getFirstById($pr_module_structure->fk_id);
            return View::make('admin.software.modal_dialog_edit_project_module', compact('pr_module_structure', 'id_module_structure',
                'module', 'industries_types'));
        } else {
            $submodule = Submodule::getFirstById($pr_module_structure->fk_id);
            return View::make('admin.software.modal_dialog_edit_project_submodule', compact('pr_module_structure', 'id_module_structure',
                'submodule', 'industries_types'));
        }
    }

    public function importExcel()
  	{
        // echo "<pre>"; dd(Input::all());
    		if(Input::hasFile('import_file')){
      			$path = Input::file('import_file')->getRealPath();
      			$data = Excel::load($path, function($reader) {})->get()->toArray();
        }

        // echo "<pre>";  dd($data);
        $task_col = getTaskColumnFromExcel($data[0]);
        foreach ($data as $key => $row) {
            foreach ($row as $cellKey => $cell) {
                if($cellKey === 'name_at_level_1' && strlen($cell)>0) {
                    $module = new Module();
                    $module->name_module = $cell;
                    $module->is_default = 1;
                    $module->save();
                      // print($cell . ";<br>");
                }

            }
            // echo "<br> -------------<br>";
        }

        // die;
    		return Redirect::back();
  	}

    public function download_software_excel($id_software, $id_module = null, $id_industry = null, $id_project = null) {
        if ($id_module) {
            $module_structure = Module_structure::getModuleByModuleId($id_module);
        } else {
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, 0);
        }

        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);

        // if ($project->is_empty_project == 1) {
        $software = Software::getFirstById($id_software);
        // echo "<pre>"; dd($module_structure);
        // Prepare File
        $data = array(
            'user' => $user->toArray(),
            'software' => $software->toArray(),
            'id_software' => $id_software,
            'module_structure' => $module_structure,
        );
        // dd($software->name_software);
        $export_file = new Project_export_files();
        $export_file->file_name = $software->name_software.'.xls';
        $export_file->file_type = 'xls';
        $export_file->software_id = $id_software;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $excelFile = Excel::create($software->name_software, function($excel) use ($data) {
            $excel->sheet('1', function($sheet) use ($data) {
                $sheet->cells('A1:Z1', function($cells) {
                    $cells->setBackground('#808080');
                    $cells->setFontColor('#ffffff');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });

                Session::forget('task_cell');
                Session::put('task_cell', 1);
                Session::forget('excel_rows');
                Session::put('excel_rows', 2);
                Session::forget('excel_files');
                Session::put('excel_files', 0);
                Module_structure::printTreeEXCELDetailsSoftwareTaskCell($data['module_structure'], $data['id_software'], 1, [], 1, [], $sheet);
                $mod = ['ID', 'INDEX'];
                for($i = 1; $i < Session::get('task_cell'); $i++) {
                // for($i = 1; $i < 6; $i++) {
                    $mod[] = 'LEVEL '.$i;
                }
                $mod[] = 'TASKS';
                $mod[] = 'CONTENT';
                $mod[] = 'PRIORITY';
                for($i = 1; $i < Session::get('excel_files')+1; $i++) {
                    $mod[] = 'ATTACHMENT ' . $i;
                }
                $sheet->row(1, $mod);
                Session::forget('excel_rows');
                Session::put('excel_rows', 1);
                $moduleStructure = Module_structure::printTreeEXCELDetailsSoftware($data['module_structure'], $data['id_software'], 1, [], 1, [], $sheet);
            });
        })->export('xls');
    }

    // download software attachments in zip format
    public function download_software_attachments($id_software, $id_module = null, $id_industry = null, $id_project = null) {
        if ($id_module) {
            $module_structure = Module_structure::getModuleByModuleId($id_module);
        } else {
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, 0);
        }

        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $software = Software::whereId_software($id_software)->first();
        $data = array(
            'user' => $user->toArray(),
            'software' => $software->toArray(),
            'id_software' => $id_software,
            'module_structure' => $module_structure,
        );
        Session::forget('check_for_attachments');
        Module_structure::getSoftwareAttachmentsZip($data['module_structure'], $id_software, 1, 'check', $id_industry, $id_project);

        if(Session::has('check_for_attachments')) {
            $file = tempnam("tmp", "zip");
            $zip = new ZipArchive();
            $zip->open($file, ZipArchive::OVERWRITE);
            Session::forget('task_cell_attachments');
            Session::put('task_cell_attachments', 1);
            Session::forget('rows_attachments');
            Session::put('rows_attachments', 2);
            Session::forget('rows_attachments');
            Session::put('rows_attachments', 2);

            // $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software, $data['id_project']);
            if ($id_module) {
                $module_structure = Module_structure::getModuleByModuleId($id_module);
            } else {
                $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, $id_project);
            }
            Module_structure::getSoftwareAttachmentsZip($module_structure, $id_software, 1, $zip, $id_industry);

            $zip->close();
            header('Content-Type: application/zip');
            header('Content-Length: ' . filesize($file));
            header('Content-Disposition: attachment; filename="' . $software->name_software . '_attachments.zip"');
            readfile($file);
            unlink($file);
        } else {
            return Redirect::back();
        }
    }

    public function upload_file_software($id)
    {
        if(Input::has('id_ms_task')) {
            $fk_ms_task = Input::get('id_ms_task');
        } else {
            $fk_ms_task = $id;
        }
        $file = Input::file('file');
        // check if is file
        if ($file) {
            $folder = date('m-Y');
            $file_name = Input::file('file')->getClientOriginalName();
            $extension = File::extension($file_name);
            $directory = 'images/attach_software/' . $folder . '/';
            $filename = $file_name;
            $upload_success = Input::file('file')->move($directory, $filename);

            $attachment = Software_attach::whereName_attachment($filename)
                ->where('folder_attachment', 'like', $folder)
                ->where('fk_ms_task', '=', $fk_ms_task)
                ->first();
            if (count($attachment) == 0) {
                $software_attachments = new Software_attach();
                $software_attachments->fk_ms_task = $fk_ms_task;
                $software_attachments->folder_attachment = $folder;
                $software_attachments->name_attachment = $filename;
                $software_attachments->extension = $extension;
                $software_attachments->save();
                Industry_attach::addAttachToAllIndustry($fk_ms_task, $folder, $directory, $filename, $extension);
            }
            return $upload_success;
        } else {
            return 0;
        }
    }

    public function remove_file_software()
    {
        $id_ms_task = 0;
        if (Input::has('id_software_attachment')) {
            $id_software_attachment = Input::get('id_software_attachment');
            $attachment = Software_attach::getFirstById($id_software_attachment);
        } else {
            $id_ms_task = Input::get('id_ms_task');
            $filename = Input::get('delete_file');
            $folder = date('m-Y');
            $attachment = Software_attach::where('fk_ms_task', '=', $id_ms_task)
                ->where('folder_attachment', 'like', $folder)
                ->where('name_attachment', 'like', $filename)->first();
        }

        if (count($attachment) == 1) {
            $folder = $attachment->folder_attachment;
            $id_ms_task = $attachment->fk_ms_task;
            $filename = $attachment->name_attachment;

            Industry_attach::deleteAttachFromAllIndustry($id_ms_task, $filename, $folder);
            $attachment->delete();
        }
        return $id_ms_task;
    }

    public function importExcel2() {
        $input = \Illuminate\Support\Facades\Input::all();
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get()->toArray();
        }
        $fk_software = $input['software_id'];
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());

        $software = Software::getFirstById($fk_software);
        $software->imported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $software->import_date = $date;
        $software->file_imported = Input::file('import_file')->getClientOriginalName();
        $software->save();

        $details = [];
        $details['module_id'] = '';
        $details['sub_module_ids'] = [];
        $details['task_ids'] = [];
        $details['last_id'] = '';
        $details['last_id_1'] = '';
        $details['last_id_2'] = '';
        $details['last_id_3'] = '';
        $details['last_id_4'] = '';
        $details['last_id_5'] = '';
        $details['id_type'] = 1;
        foreach($data as $rowKey => $row) {
            if($row['index'] == 1) {
                // edit module/submodule/task
                if($row['tasks'] != null) {
                    array_push($details['task_ids'], $row['id']);
                    $task = Task::where('id_task', '=', $row['id'])->get();
                    if($task->first()) {
                        $task->first()->name_task = $row['tasks'];
                        $task->first()->description_task = $row['content'];
                        $msTask = Ms_task::where('fk_task', '=', $row['id'])
                                ->where('is_default', '=', 1)
                                ->get();
                        if($msTask->first()) {
                            $msTask->first()->lvl_priority = $row['priority'];
                            $msTask->first()->save();
                        }
                        $task->first()->save();
                    }
                } else {
                    for($i = 1; $i <= 5; $i++) {
                        if($row['level_'.$i] !== null) {
                            if($i == 1) {
                                $details['sub_module_ids'] = [];
                                $details['task_ids'] = [];
                                $details['module_id'] = $row['id'];
                                $details['last_id'] = $row['id'];
                                $details['id_type'] = 1;
                                $module = Module::where('id_module', '=', $row['id'])->get();
                                if($module->first()) {
                                    $module->first()->name_module = $row['level_'.$i];
                                    $module->first()->save();
                                }
                                $details['last_id_1'] = $row['id'];
                            } else {
                                array_push($details['sub_module_ids'], $row['id']);
                                $sub_module = Submodule::where('id_submodule', '=', $row['id'])->get();
                                if($sub_module->first()) {
                                    $moduleStructure = Module_structure::where('fk_id', '=', $sub_module->first()->id_submodule)
                                                                        ->where('fk_software', '=', $fk_software)
                                                                        ->where('id_type', '=', 2)
                                                                        ->where('is_default', '=', 1)
                                                                        ->get()
                                                                        ->first();
                                    $lastId = $i-1;
                                    $TypeId = 2;
                                    if($lastId == 1)
                                        $TypeId = 1;
                                    $getModuleStructure = Module_structure::where('fk_software', '=', $fk_software)
                                            ->where('fk_id', '=', $details['last_id_'.$lastId])
                                            ->where('id_type', '=', $TypeId)
                                            ->where('is_default', '=', 1)
                                            ->get()
                                            ->first();
                                    $moduleStructure->fk_parent = $getModuleStructure->id_module_structure;
                                    $moduleStructure->level = $getModuleStructure->level+1;
                                    $moduleStructure->save();
                                    $sub_module->first()->name_submodule = $row['level_'.$i];
                                    $sub_module->first()->save();
                                }
                                $details['last_id_'.$i] = $row['id'];
                                $details['last_id'] = $row['id'];
                                $details['id_type'] = 2;
                            }
                        }
                    }
                }
            } elseif ($row['index'] == 2) {
                // add module/submodule/task
                 if($row['tasks'] != null) {
                    $getModuleStructure = Module_structure::where('fk_software', '=', $fk_software)
                                                            ->where('fk_id', '=', $details['last_id'])
                                                            ->where('id_type', '=', $details['id_type'])
                                                            ->where('is_default', '=', 1)
                                                            ->get()
                                                            ->first();
//                $fk_module_structure = 0;
                    $name_task = $row['tasks'];
                    $task_description = $row['content'];
                    $task_description = str_replace('<p><br></p>', '', $task_description);
                    $task = new Task();
                    $task->name_task = $name_task;
                    $task->description_task = $task_description;
                    $task->save();
                    $fk_task = $task->id_task;
                    $ms_task = Ms_task::where('fk_task', '=', $fk_task)
                        ->where('fk_module_structure', '=', $fk_module_structure)
                        ->first();
                    $ms_task = new Ms_task();
                    $ms_task->fk_task = $fk_task;
                    $ms_task->fk_module_structure = $getModuleStructure->id_module_structure;
                    $ms_task->is_default = 1;
                    $ms_task->is_checked = 0;
                    $ms_task->is_mandatory = 0;
                    if ($row['priority'] !== null) {
                        $ms_task->lvl_priority = $row['priority'];
                    }
                    $ms_task->save();
                } else {
                    for($i = 1; $i <= 5; $i++) {
                        if($row['level_'.$i] !== null) {
                            if($i == 1) {
                                $details['sub_module_ids'] = [];
                                $details['task_ids'] = [];

                                $module = new Module();
                                $module->name_module = $row['level_'.$i];
                                $module->is_default = 1;
                                $module->save();

                                $id_module = $module->id_module;

                                $structure_module = new Module_structure();
                                $structure_module->fk_software = $fk_software;
                                $structure_module->fk_id = $id_module;
                                $structure_module->fk_parent = 0;
                                $structure_module->id_type = 1;
                                $structure_module->level = $i;
                                $structure_module->fk_module_parent = $id_module;
                                $structure_module->is_default = 1;
                                $structure_module->save();
                                $details['last_id_1'] = $module->id_module;
                                $details['last_id'] = $module->id_module;
                                $details['module_id'] = $module->id_module;
                                $details['id_type'] = 1;
                            } else {
                                $lastId = $i-1;
                                $TypeId = 2;
                                if($lastId == 1)
                                    $TypeId = 1;
                                $getModuleStructure = Module_structure::where('fk_software', '=', $fk_software)
                                        ->where('fk_id', '=', $details['last_id_'.$lastId])
                                        ->where('id_type', '=', $TypeId)
                                        ->where('is_default', '=', 1)
                                        ->get()
                                        ->first();

                                $submodule = new Submodule();
                                $submodule->name_submodule = $row['level_'.$i];
                                $submodule->is_default = 1;
                                $submodule->save();

                                $id_submodule = $submodule->id_submodule;

                                $structure_module = new Module_structure();
                                $structure_module->fk_software = $fk_software;
                                $structure_module->fk_id = $id_submodule;
                                $structure_module->fk_parent = $getModuleStructure->id_module_structure;
                                $structure_module->id_type = 2;
                                $structure_module->level = $i;
                                $structure_module->fk_module_parent = $getModuleStructure->fk_module_parent;
                                $structure_module->is_default = 1;
                                $structure_module->save();
                                $structure_module_id = $structure_module->id_module_structure;
                                $allSubmoduleMsTasks = Ms_task::where('fk_module_structure', '=', $getModuleStructure->id_module_structure)->get();
                                if($allSubmoduleMsTasks->first()) {
                                    foreach($allSubmoduleMsTasks as $allSubmoduleMsTask) {
                                        $task = Ms_task::findOrFail($allSubmoduleMsTask->id_ms_task);
                                        $task->fk_module_structure = $structure_module_id;
                                        $task->save();
                                    }
                                }
                                array_push($details['sub_module_ids'], $row['id']);
                                $details['last_id'] = $id_submodule;
                                $details['id_type'] = 2;
                                $details['last_id_'.$i] = $id_submodule;
                            }
                        }
                    }
                }
            } elseif ($row['index'] == 3) {
                // delete module/submodule/task
                if($row['tasks'] != null) {
                    $msTask = Ms_task::where('fk_task', '=', $row['id'])
                            ->where('is_default', '=', 1)
                            ->get();
                    if($msTask->first()) {
                        $getMsTask = Ms_task::findOrFail($msTask->first()->id_ms_task)->delete();
                    }
                    $findAllMsTaskFromModelStructure = Ms_task::where('fk_module_structure', '=', $msTask->first()->fk_module_structure)
                                                            ->get();
                    if(count($findAllMsTaskFromModelStructure->toArray()) == 0) {
                        self::checkForDelete($msTask->first()->fk_module_structure, 1);
                    }
                } else {
                    for($i = 1; $i <= 5; $i++) {
                        if($row['level_'.$i] !== null) {
                            if($i == 1) {
                                $moduleStructure = Module_structure::where('fk_software', '=', $fk_software)
                                            ->where('fk_id', '=', $row['id'])
                                            ->where('id_type', '=', 1)
                                            ->where('is_default', '=', 1)
                                            ->get()
                                            ->first();
                                $parentId = $moduleStructure->id_module_structure;
                                self::deleteModuleChildrens($parentId);

                                $tasks = Ms_task::where('fk_module_structure', '=', $parentId)
                                    ->where('is_default', '=', 1)
                                    ->get();
                                if($tasks->first()) {
                                    foreach($tasks as $task) {
                                        $deleteTask = Ms_task::findOrFail($task->id_ms_task)->delete();
                                    }
                                }
                                $deleteModule = Module_structure::findOrFail($parentId)->delete();
                            } else {
                                $moduleStructure = Module_structure::where('fk_software', '=', $fk_software)
                                            ->where('fk_id', '=', $row['id'])
                                            ->where('id_type', '=', 2)
                                            ->where('is_default', '=', 1)
                                            ->get()
                                            ->first();
                                $parentId = $moduleStructure->id_module_structure;
                                self::deleteModuleChildrens($parentId);

                                $tasks = Ms_task::where('fk_module_structure', '=', $parentId)
                                    ->where('is_default', '=', 1)
                                    ->get();
                                if($tasks->first()) {
                                    foreach($tasks as $task) {
                                        $deleteTask = Ms_task::findOrFail($task->id_ms_task)->delete();
                                    }
                                }
                                self::checkForDelete($parentId, 0);
                                $deleteModule = Module_structure::findOrFail($parentId)->delete();
                            }
                        }
                    }
                }
            } elseif($row['index'] == 0) {
                for($i = 1; $i <= 5; $i++) {
                    if($row['level_'.$i] !== null) {
                        if($i == 1) {
                            $details['sub_module_ids'] = [];
                            $details['task_ids'] = [];
                            $details['module_id'] = $row['id'];
                            $details['last_id'] = $row['id'];
                            $details['id_type'] = 1;
                            $details['last_id_1'] = $row['id'];
                        } else {
                            array_push($details['sub_module_ids'], $row['id']);
                            $details['last_id'] = $row['id'];
                            $details['last_id_'.$i] = $row['id'];
                            $details['id_type'] = 2;
                        }
                    }
                }
            }
        }

        Session::flash('scrollTo', $fk_software);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.file_upload_success')]]);
        return Redirect::back()->withErrors($errors);
        // return Redirect::back();
    }

    public function checkForDelete($moduleStructureId, $delete) {
        $submodule = Module_structure::findOrFail($moduleStructureId);
        if($submodule->fk_parent != 0) {
            $parent = Module_structure::where('id_module_structure', '=', $submodule->fk_parent)
                                        ->where('is_default', '=', 1)
                                        ->get()
                                        ->first();
            $childrens = Module_structure::where('fk_parent', '=', $parent->id_module_structure)
                                        ->get();
            if(count($childrens->toArray()) == 1) {
                self::checkForDelete($parent->id_module_structure, 1);
            }
        }
        if($delete == 1) {
            $submodule->delete();
        }
    }

    public function deleteModuleChildrens($parentId) {
        $submodules = Module_structure::where('fk_parent', '=', $parentId)
                                        ->where('id_type', '=', 2)
                                        ->where('is_default', '=', 1)
                                        ->get();
        if($submodules->first()) {
            foreach ($submodules as $submodule) {
                $tasks = Ms_task::where('fk_module_structure', '=', $submodule->id_module_structure)
                                ->where('is_default', '=', 1)
                                ->get();
                if($tasks->first()) {
                    foreach($tasks as $task) {
                        $deleteTask = Ms_task::findOrFail($task->id_ms_task)->delete();
                    }
                }
                self::deleteModuleChildrens($submodule->id_module_structure);
                $submoduleDelete = Ms_task::findOrFail($submodule->id_module_structure)->delete();
            }
        }
    }

    public function download_software_pdf($id_software, $id_module = null, $id_industry = null, $id_project = null)
    {
        if ($id_module) {
            $module_structure = Module_structure::getModuleByModuleId($id_module);
        } else {
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, 0);
        }

        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $software = Software::getFirstById($id_software);
        $company = Company::getFirstById($user->fk_company);

        $export_file = new Project_export_files();
        $export_file->file_name = $software->name_software.'.pdf';
        $export_file->file_type = 'pdf';
        $export_file->software_id = $id_software;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $view = View::make('admin.software.download_software_pdf',
            compact('user', 'software', 'company', 'module_structure', 'id_industry', 'id_project'));
        $timestamp = time();
        File::put(public_path() . '/app/storage/'.$timestamp.'_pdf.html', $view);

        $options = array(
            'disable-smart-shrinking'
        );
        $pdf = new mikehaertl\wkhtmlto\Pdf($options);
        $pdf->binary = 'wkhtmltopdf';

        $pdf->addPage(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

        $return =  $pdf->send();
        unlink(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

        if (!$return) {
            throw new Exception('Could not create PDF: '.$pdf->getError());
        } else {
            return $return;
        }
    }

    // download software in word format
    public function download_software_word($id_software, $id_module = null, $id_industry = null, $id_project = null)
    {
        if ($id_module) {
            $module_structure = Module_structure::getModuleByModuleId($id_module);
        } else {
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, 0);
        }

        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $software = Software::getFirstById($id_software);
        $company = Company::getFirstById($user->fk_company);

        $export_file = new Project_export_files();
        $export_file->file_name = $software->name_software.'.doc';
        $export_file->file_type = 'doc';
        $export_file->software_id = $id_software;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $view = View::make('admin.software.download_software_word',
            compact('company', 'user', 'software', 'id_software', 'module_structure', 'id_industry', 'id_project'));
        $timestamp = time();
        File::put(public_path() . '/app/storage/'.$timestamp.'_word.html', $view);

        $file= public_path(). '/app/storage/'.$timestamp.'_word.html';

        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        );

        return Response::download($file, $software->name_software.'.doc', $headers)->deleteFileAfterSend(true);
    }
}
