@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('footer_scripts')
    <script type="text/javascript">
        $("#modalAddIndustry").on('hidden.bs.modal', function () {
            $("#modalAddIndustry").removeData('bs.modal');
        });
        $("#modalAddIndustry").on('shown.bs.modal', function () {
        });
    </script>
    <script>
        $(document).ready(function () {
            if ($('#scrollTo').length != 0) {
                $('#'+$('#scrollTo').val()).parents('.industry_category').children('input').trigger("click");
                $('html, body').animate({ scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50 }, 'slow');
            }
        });
    </script>
@stop
@section('content')
    <div class="panel widget light-widget panel-bd-top">
        <div class="panel-heading bordered">
            <div class=" row">
                <div class="col-xs-12">
                    <h3 class="mgtp-10">{{trans('messages.nav_industries') }}</h3>
                </div>
                <div class="col-xs-12">
                    <a href="{{ URL::route('industry.create') }}"
                       id="a_form_headquarter_0" data-toggle="modal" data-target="#modalAddIndustry"
                       class="btn static_add_button ls-light-blue-btn a_form_headquarter  btn-round btn-xxl">
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="title_static_add_button" id="div_a_form_headquarter_0">
                        {{trans('messages.nav_industries') }}  {{trans('messages.act_create') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.panel-header -->
        <div class="panel-body">
            <br>
            {{-- Confirmation Messages--}}
            @if( Session::has('error_message'))
                <div class="form-group has-error">
                    <label class="control-label" for="inputError">
                        <i class="fa fa-times-circle-o"></i> {{ Session::get('error_message') }}</label>
                </div>
            @endif
            @if( Session::has('success_message'))
                <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess">
                        <i class="fa fa-check"></i> {{ Session::get('success_message') }}</label>
                </div>
            @endif
            <section class="ac-container">
                @foreach($industry_types as $industry_type)
                    <div class="industry_category">
                        @if($industry_type->id_industry_type == 1)
                            <input id="ac-{{$industry_type->id_industry_type}}" name="accordion-1" type="checkbox" checked>
                        @else
                            <input id="ac-{{$industry_type->id_industry_type}}" name="accordion-1" type="checkbox">
                        @endif
                        <label for="ac-{{$industry_type->id_industry_type}}"><i class="fa {{$industry_type->font_icon}}"></i> <span class=""> {{$industry_type->name_industry_type}}</span></label>
                        <article class="ac-any">
                            <div class="article_content">
                                <?php
                                $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>
                                @if(count($industries)>0)
                                    <div class="row">
                                        @foreach($industries as $row)
                                            <div id="industry{{$row->id_industry}}" class="col-md-4">
                                                <div class="panel panel-info mg-10">
                                                    <div class="panel-body panel_industry">
                                                        <div class="row">
                                                            <div class="col-md-7 name_industry">
                                                                {{ $row->name_industry  }}
                                                            </div>
                                                            <div class="col-md-5 col-padding-0">
                                                                <div style="display: inline-block">
                                                                    <a href="{{ URL::route('industry.show',[$row->id_industry]) }}"
                                                                       class="btn btn-round btn-xl ls-light-green-btn">
                                                                        <i class="fa fa-sitemap"></i>
                                                                    </a>
                                                                </div>
                                                                <div style="display: inline-block">
                                                                    {{ Form::model($row,['route'=>['industry.update',$row->id_industry], 'method'=>'PATCH'])}}
                                                                    {{ Form::hidden('deactivate','1') }}
                                                                    @if($row->is_deleted == 1)
                                                                        <button type="submit" class="btn btn-round btn-xl ls-dark-blue-btn">
                                                                            <i class="fa fa-eye"></i>
                                                                        </button>
                                                                    @else
                                                                        <button type="submit" class="btn btn-round btn-xl btn-danger">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button>
                                                                    @endif
                                                                    {{ Form::close() }}
                                                                </div>
                                                                @if(strlen(strip_tags($row->description_industry)) > 1)
                                                                    <div style="display: inline-block">
                                                                        <button class="tooltip_div btn  btn-round btn-xl  btn-default" title="{{ strip_tags($row->description_industry)}}">
                                                                            <i class="fa fa-info"></i></button>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </article>
                    </div>
                @endforeach
            </section>
            <br>
        </div>
        <!-- /.box-body -->
    </div>
    @if(Session::has('scrollTo'))
        <input type="hidden" id="scrollTo" value="{{Session::get('scrollTo')}}">
    @endif
    <!-- Modal Industry -->
    <div class="modal fade" id="modalAddIndustry" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Industry End -->
@stop
