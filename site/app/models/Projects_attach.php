<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 8/6/2015
 * Time: 3:43 PM
 */
class Projects_attach extends Eloquent
{


    public static $rules = [
        'fk_project_row' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_projects_attach';
    protected $table = 'pr_projects_attach';
    protected $guarded = ['id_projects_attach'];
    protected $fillable = ['name_attachment', 'extension', 'fk_project_row'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Projects_attach::whereId_projects_attach($id)->first();
        return $list;
    }

    public static function getTasksWithAttachments($fk_project)
    {
        $fk_industry = Industry::getIndustryByProject($fk_project);
        $array = array();
        $attachments = Projects_attach::join('pr_projects_rows', 'pr_projects_rows.id_project_row', '=', 'pr_projects_attach.fk_project_row')
            ->where('fk_project', '=', $fk_project)
            ->get();

        $attachments_ind = Industry_attach::join('pr_industry_ms', 'fk_industry_ms', '=', 'id_industry_ms')
            ->where('fk_industry', '=', $fk_industry)
            ->get();

        foreach ($attachments as $attachment) {
            $array[] = $attachment->fk_ms_task;
        }
        foreach ($attachments_ind as $attachment) {
            $array[] = $attachment->fk_ms_task;
        }
        return array_unique($array);
    }

    public static function getAttachmentByTask($fk_ms_task, $fk_project)
    {
        $attachments = Projects_attach::join('pr_projects_rows', 'pr_projects_rows.id_project_row', '=', 'pr_projects_attach.fk_project_row')
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->where('fk_project', '=', $fk_project)
            ->get();
        return $attachments;
    }

    public static function getIdMsTaskByAttachment($id_projects_attach)
    {
        $projects_attach = Projects_attach::join('pr_projects_rows', 'pr_projects_rows.id_project_row', '=', 'pr_projects_attach.fk_project_row')
            ->where('id_projects_attach', '=', $id_projects_attach)
            ->first();
        if (count($projects_attach) == 1) {
            return $projects_attach->fk_ms_task;
        } else {
            return 0;
        }
    }

    public static function getIconByAttachment($extension)
    {
        $icon = '';
        $extension = strtolower($extension);
        $path = base_path() . '/images';
        $url = URL::asset('images');
        $file_path = $path . "/icons/" . $extension . "/" . $extension . "-48_32.png";
        if (file_exists($file_path)) {
            $file = $url . "/icons/" . $extension . "/" . $extension . "-48_32.png";
            $icon = "url('" . $file . "');";
        } else {
            $file = $url . "/icons/text/text-48_32.png";
            $icon = "url('" . $file . "');";
        }


//        switch (strtoupper($extension)) {
//            case 'JPG':
//                $icon = "url('" . Url::to('/') . "/images/attach_task/" . $id_project . "/" . $name_attachment . "') center top no-repeat; ";
//                break;
//            case 'JPEG':
//                $icon = "url('" . Url::to('/') . "/images/attach_task/" . $id_project . "/" . $name_attachment . "') center top no-repeat;";
//                break;
//            case 'PNG':
//                $icon = "url('" . Url::to('/') . "/images/attach_task/" . $id_project . "/" . $name_attachment . "') center top no-repeat;";
//                break;
//            case 'GIF':
//                $icon = "url('" . Url::to('/') . "/images/attach_task/" . $id_project . "/" . $name_attachment . "') center top no-repeat;";
//                break;
//            case 'BMP':
//                $icon = "url('" . Url::to('/') . "/images/attach_task/" . $id_project . "/" . $name_attachment . "') center top no-repeat;";
//                break;
//            default:
//                $file_path = $path . "\\icons\\" . $extension . "\\" . $extension . "-48_32.png";
//                if (file_exists($file_path)) {
//                    $file = $url . "/icons/" . $extension . "/" . $extension . "-48_32.png";
//                    $icon = "url('" . $file . "');";
//                } else {
//                    $file = $url . "/icons/text/text-48_32.png";
//                    $icon = "url('" . $file . "');";
//                }
//                break;
//        }
        return $icon;

    }

}
