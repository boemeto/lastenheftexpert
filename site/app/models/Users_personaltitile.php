<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 27.05.2015
 * Time: 12:43
 */
class Users_personal_title extends Eloquent
{


    public static $rules = [
        'name_personaltitle' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_personaltitle';
    protected $table = 'users_personaltitle';
    protected $guarded = ['id_personaltitle'];
    protected $fillable = ['name_personaltitle', 'description_personaltitle'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getPersonal_Title($with_select = 0)
    {
        $array = array();
        $lists = Users_personal_title::select('*')->orderBy('id_personaltitle', 'desc')->get();

        if ($with_select == 1) {
            $array[0] = 'Anrede';
        }
        foreach ($lists as $list) {
            $array[$list->id_personaltitle] = $list->name_personaltitle;
        }
        return $array;
    }

    public static function getUserPersonalTitle($id_user)
    {
        $fk_title = User::getFirstById($id_user)->fk_personal_title;
        $title = self::getFirstById($fk_title);
        if (count($title) > 0) {
            $name_title = $title->name_personaltitle;
        } else {
            $name_title = '';
        }

        return $name_title;
    }

    public static function getFirstById($id)
    {
        $list = self::where('id_personaltitle', '=', $id)->first();
        return $list;
    }

}