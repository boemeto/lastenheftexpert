<?php
if (Auth::check()) {
    $id_user = Auth::user()->id;
    $fk_company = Auth::user()->fk_company;
    $logo_company = Company::getLogoCompany($fk_company);
}
?>

        <!DOCTYPE html>
<html lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="language" content="DE">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/ios/fickle-logo-72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/ios/fickle-logo-72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/ios/fickle-logo-114.png"/>


    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

    <title>{{trans('messages.title_long')}}</title>

    <!--Page loading plugin Start -->
    <link href="{{ URL::asset('assets/css/plugins/pace.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('assets/js/pace.min.js') }}"></script>
    <!--Page loading plugin End   -->

    <!-- Plugin Css Bootstrap Strat -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/bootstrap-switch.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/jquery-ui.min.css') }}" rel="stylesheet">
    <!-- Plugin Css End -->
    <!-- Tooltips and toolbars -->
    <link rel="stylesheet" href="{{URL::to('assets/css/plugins/jquery.toolbars.css')}}"/>
    <!-- Tooltips and toolbars End -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/accordion.css')}}">

    {{--    <link href="{{ URL::asset('assets/css/plugins/ladda-themeless.min.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ URL::asset('assets/css/plugins/lightGallery.css') }}" rel="stylesheet">--}}
    <link href="{{ URL::asset('assets/css/plugins/switchery.min.css') }}" rel="stylesheet">
    {{--    <link href="{{ URL::asset('assets/css/plugins/humane_themes/bigbox.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ URL::asset('assets/css/plugins/humane_themes/libnotify.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ URL::asset('assets/css/plugins/humane_themes/jackedup.css') }}" rel="stylesheet">--}}
    <link href="{{ URL::asset('assets/js/bootstrap-fileinput-master/css/fileinput.min.css') }}" rel="stylesheet">
    {{--    <link href="{{ URL::asset('assets/css/plugins/smoothness.jquery-ui.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ URL::asset('assets/css/plugins/jquery.minicolors.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ URL::asset('assets/css/plugins/spectrum.css') }}" rel="stylesheet">--}}
    <link href="{{ URL::asset('assets/css/plugins/tinycolorpicker.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('assets/css/plugins/dropzone.css')}}" rel="stylesheet">
    <!-- Custom styles Style -->
    <link href="{{ URL::asset('assets/css/theme.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- Custom styles Style End-->
    <!--  DataTables style  -->
    <link href="{{ URL::asset('assets/js/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!-- Responsive Style For-->
    <link href="{{ URL::asset('assets/css/responsive.css') }}" rel="stylesheet">
    <!-- Responsive Style For-->
    <!-- Module menu tree style -->
    <link href="{{ URL::asset('assets/css/plugins/easyTree.css') }}" rel="stylesheet">
    <!-- Module menu tree style -->
    <!-- Checkbox/radio style -->
    <link href="{{ URL::asset('assets/css/plugins/icheck/skins/all.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/summernote.css')}}" rel="stylesheet">
    <!-- Checkbox/radio style -->
    <!-- Confirmation Messages -->
    <link href="{{ URL::asset('assets/css/plugins/amaranjs/amaran.min.css')}}" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- Google maps -  location API --}}
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    {{-- insert necessary scripts in header--}}
    @yield('header_scripts')
</head>
<body class=" deep-blue-color">
<!--Navigation Top Bar Start-->
<nav class="navigation  navigation-fixed-top">
    <div class=" ">
        <!--Logo text start-->
        <div class="header-logo">
            <a href="{{URL::to('/')}}" title="{{trans('messages.title_long')}}">
                <h1>   &nbsp;</h1>
                {{--                <img src="{{asset('images/logo-simbol_alb.png')}}">--}}
            </a>
        </div>
        <!--Logo text End-->
        <div class="top-navigation">
            <!--Collapse navigation menu icon start -->
            <div class="menu-control  ">
                <a href="javascript:void(0)">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            @if(strpos(URL::current(),'projects/') !== false)
                <div class="search-box">
                    <ul>
                        <li class="dropdown">
                           <form class="filterform" action="#">
                                <div class="span2" role="search">
                                    <a href="javascript:void(0)" id="showhide" class="closed">
                                        <span id="search_link" class="fa fa-search"> </span>
                                    </a>
                                    <form>
                                        <input type="search" class="filterinput search2" id="filterinput" placeholder="{{trans('messages.label_search')}}">
                                    </form>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            @endif

                <!--Collapse navigation menu icon end -->
                <!--Top Navigation Start-->
                <ul>
                    @yield('dropdown-menu')
                </ul>
                <!--Top Navigation End-->
        </div>
    </div>
</nav>
<!--Navigation Top Bar End-->
<section id="main-container">

    <!--Left navigation section start-->
    <section id="left-navigation">   <!--- Put active class in here fom minimize left bar at default---->
        <!--Left navigation user details start-->
        <div class="user-image" style="width:100%;">
            <img src="{{ URL::asset($logo_company)}}" alt="image">
        </div>

        <!--Left navigation user details end-->

        <!--Phone Navigation Menu icon start-->
        <div class="phone-nav-box ">
            <a class="phone-logo" href="{{URL::to('/')}}" title="{{trans('messages.title_long')}}">
                <h1>&nbsp;</h1>
            </a>
            <a class="phone-nav-control" href="javascript:void(0)">
                <span class="fa fa-bars"></span>
            </a>

            <div class="clearfix"></div>
        </div>
        <!--Phone Navigation Menu icon start-->

        <!--Left navigation start-->
        <ul class="mainNav"> <!--- Put active class in here fom minimize left bar at default---->
            @yield('sidebar-menu')
            <li class=" copyright_list">
                {{--                {{trans('messages.label_copyright')}}:--}}

                <img src="{{asset('images/logo-simbol_alb.png')}}">
                <br>
                <span>
                     {{trans('messages.title_long')}}
                </span>

            </li>
        </ul>

        <!--Left navigation end-->
    </section>
    <!--Left navigation section end-->
    <!--Page main section start-->
    <section id="min-wrapper"> <!--- Put active class in here fom minimize left bar at default---->
        <div id="main-content">
            <div class="container-fluid">
                {{ $errors->first("confirm_",'<div class="amaran_confirm">:message</div>') }}
                {{ $errors->first("error_",'<div class="amaran_error">:message</div>') }}

                        <!-- Main Content  Start-->
                @yield('content')
                        <!-- Main Content  End-->
            </div>
        </div>
    </section>
    <!--Page main section end -->

</section>
{{--<footer class="footer-1" id="footer">--}}
{{--<div class="vd_bottom ">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class=" col-xs-12">--}}
{{--<div class="copyright">--}}
{{--Copyright ©2016 Mevision. All Rights Reserved--}}
{{--</div>--}}
{{--</div>--}}
{{--</div><!-- row -->--}}
{{--</div><!-- container -->--}}
{{--</div>--}}
{{--</footer>--}}
        <!--Layout Script start -->
<script src="{{ URL::asset('assets/js/color.js') }}"></script>
<script src="{{ URL::asset('assets/js/lib/jquery-2.1.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/lib/jqueryui.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-switch.min.js') }}"></script>
<!--responsive -->
<script src="{{ URL::asset('assets/js/pages/layout.js') }}"></script>
<!--Layout Script end -->

<!-- Accordion Box Script Start -->
<script src="{{ URL::asset('assets/js/multipleAccordion.js') }}"></script>
<!-- Accordion Box Script End -->

<!--File input Script start -->
<script src="{{ URL::asset('assets/js/bootstrap-fileinput-master/js/fileinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.autosize.js') }}"></script>
<!--Form Script end -->

<!--Nano Scroll Script Start -->
<script src="{{ URL::asset('assets/js/jquery.nanoscroller.min.js') }}"></script>
<!--Nano Scroll Script End -->

<!--switchery Script Start -->
<script src="{{ URL::asset('assets/js/switchery.min.js') }}"></script>
<!--switchery Script End -->

<script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>

<!--Confirmation Messages Script-->
<script src="{{ URL::asset('assets/js/jquery.amaran.min.js') }}"></script>
<!--Confirmation Messages Script End-->

<!--Tooltip Bar Library Script Start -->
<script src="{{URL::to('assets/js/jquery.toolbar.min.js')}}"></script>
<!--Tooltip Bar Library Script End -->

<!--easing Library Script Start -->
<script src="{{ URL::asset('assets/js/lib/jquery.easing.js') }}"></script>
<!--easing Library Script End -->
<!--easypie Library Script Start -->
<script src="{{ URL::asset('assets/js/jquery.easypiechart.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/chart/raphael-min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.circliful.min.js') }}"></script>
<!--easypie Library Script Start -->
<script src="{{ URL::asset('assets/js/datatables/js/jquery.dataTables.min.js')}}"></script>
<!--bootstrap-progressbar Library script Start-->
<script src="{{ URL::asset('assets/js/bootstrap-progressbar.min.js') }}"></script>
<!--bootstrap-progressbar Library script End-->
<script src="{{ URL::asset('assets/js/unicode.js') }}"></script>
<script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/chart/jquery.sparkline.js') }}"></script>
<script src="{{ URL::asset('assets/js/chart/morris.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/icheck.min.js')}}"></script>

<script src="{{asset('assets/js/custom/scripts.js')}}"></script>
@yield('footer_scripts')

<script type="text/javascript">
        $("#showhide").click(function () {
           $(".search2").animate({width:'toggle'},500);
              $(this).toggleClass("closed");
        });
</script>
<script type="text/javascript">(function (d, n) {
        var s, a, p;
        s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = (document.location.protocol === "https:" ? "https:" : "http:") + "//cdn.nudgespot.com" + "/nudgespot.js";
        a = document.getElementsByTagName("script");
        p = a[a.length - 1];
        p.parentNode.insertBefore(s, p.nextSibling);
        window.nudgespot = n;
        n.init = function (t) {
            function f(n, m) {
                var a = m.split('.');
                2 == a.length && (n = n[a[0]], m = a[1]);
                n[m] = function () {
                    n.push([m].concat(Array.prototype.slice.call(arguments, 0)))
                }
            }

            n._version = 0.1;
            n._globals = [t];
            n.people = n.people || [];
            n.params = n.params || [];
            m = "track register unregister identify set_config people.delete people.create people.update people.create_property people.tag people.remove_Tag".split(" ");
            for (var i = 0; i < m.length; i++)f(n, m[i])
        }

    })(document, window.nudgespot || []);
    //    nudgespot.init("4eca06e0b7bbe3b1b340bc7272a24d46");
</script>
<script type="text/javascript">


    function show_msg_amaran() {
        if ($('.amaran_confirm').length > 0) {
            var confirm = $('.amaran_confirm').html();
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: confirm,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        }

        if ($('.amaran_error').length > 0) {
            var error = $('.amaran_error').html();
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: error,
                    bgcolor: '#D9534F',
                    color: '#fff'
                }
            });
        }
    }

    $(document).ready(function () {
        $('.icheck-green').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green',
            disabledClass: 'disabled'
        });
        $('.iradio-green').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green',
            disabledClass: 'disabled'
        });
        show_msg_amaran();
    });

    $('.tooltip_div').hover(function () {
        var tooltip = '<div class="tooltip"></div>';
        // Hover over code
        var title = $.trim($(this).attr('title'));
        if (title.length > 0) {
            $(this).data('tipText', title).removeAttr('title');
//            $(this).parent().append(tooltip);
            $('body').append(tooltip);
            $('.tooltip').html(title);
            $('.tooltip').fadeIn('slow');
        } else {
            $('body').append(tooltip);
        }
    }, function () {
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function (e) {
        var mousex = e.pageX - 50; //Get X coordinates
        var mousey = e.pageY + 25; //Get Y coordinates
        $('.tooltip').css({top: mousey, left: mousex})
    });
    $(function () {
        $('.data_table').DataTable({
            "bLengthChange": false,
            "bPaginate": false,
            "paging": false,
            "bInfo": false,
            "pagingType": "simple_numbers",
            "language": {
                "emptyTable": "{{  trans('pagination.emptyTable') }}",
                "info": "{{  trans('pagination.info') }}",
                "infoEmpty": "{{  trans('pagination.infoEmpty') }}",
                "infoFiltered": "{{  trans('pagination.infoFiltered') }}",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "{{  trans('pagination.lengthMenu') }}",
                "loadingRecords": "{{  trans('pagination.loadingRecords') }}",
                "processing": "{{  trans('pagination.processing') }}",
                "search": "",
                "sSearch": "",
                "searchPlaceholder": "{{  trans('messages.act_search') }}",
                "paginate": {
                    "first": "{{  trans('pagination.first') }}",
                    "last": "{{  trans('pagination.last') }}",
                    "next": "{{  trans('pagination.next') }}",
                    "previous": "{{  trans('pagination.previous') }}"
                },
                "aria": {
                    "sortAscending": "{{  trans('pagination.sortAscending') }}",
                    "sortDescending": "{{  trans('pagination.sortAscending') }}"
                }
            },
//            "dom": '<"top"f>rt<"bottom"ilp><"clear">'
        });

        $('#table_datatable').DataTable({
            "ordering": false,
            "iDisplayLength": 20,
            "aLengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "pagingType": "simple_numbers",
            "searching": false,
            "language": {
                "emptyTable": "{{  trans('pagination.emptyTable') }}",
                "info": "{{  trans('pagination.info') }}",
                "infoEmpty": "{{  trans('pagination.infoEmpty') }}",
                "infoFiltered": "{{  trans('pagination.infoFiltered') }}",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "{{  trans('pagination.lengthMenu') }}",
                "loadingRecords": "{{  trans('pagination.loadingRecords') }}",
                "processing": "{{  trans('pagination.processing') }}",
                "search": "",
                "sSearch": "",
                "searchPlaceholder": "{{  trans('messages.act_search') }}",
                "paginate": {
                    "first": "{{  trans('pagination.first') }}",
                    "last": "{{  trans('pagination.last') }}",
                    "next": "{{  trans('pagination.next') }}",
                    "previous": "{{  trans('pagination.previous') }}"
                },
                "aria": {
                    "sortAscending": "{{  trans('pagination.sortAscending') }}",
                    "sortDescending": "{{  trans('pagination.sortAscending') }}"
                }
            }
        });


    });

</script>
<script>
    $('.static_add_button').mouseover(function () {
        var id = $(this).attr('id');
        $('#div_' + id).show("slide", {direction: "right"}, 500);
        var color = $(this).data("color");
        if (color) {
            $('#div_' + id).css('color', color);
        }
    });
    $('.static_add_button').mouseleave(function () {
        var id = $(this).attr('id');
        $('#div_' + id).fadeOut(500);

    });
</script>
</body>
</html>
