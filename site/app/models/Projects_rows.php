<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 8/4/2015
 * Time: 1:45 PM
 */
class Projects_rows extends Eloquent
{


    public static $rules = [
        'fk_project' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_project_row';
    protected $table = 'pr_projects_rows';
    protected $guarded = ['id_project_row'];
    protected $fillable = ['fk_project', 'fk_user', 'fk_ms_task', 'task_description',
        'task_color', 'task_priority', 'is_checked_user',
        'is_changed', 'is_not_offer', 'is_control'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Projects_rows::whereId_project_row($id)->first();
        return $list;
    }

    public static function getFirstByKey($fk_ms_task, $fk_project)
    {
        $list = Projects_rows::where('fk_ms_task', '=', $fk_ms_task)
            ->where('fk_project', '=', $fk_project)
            ->first();
        return $list;
    }

    public static function getSelectedTasks($id_project)
    {
        $array_tasks_selected = array();
        $list = Projects_rows::whereFk_project($id_project)
                ->where('is_checked_user', '=', 1)->get();

        if (count($list) > 0) {
            foreach ($list as $ms_task) {
                $array_tasks_selected[] = $ms_task->fk_ms_task;
            }
        }
        return $array_tasks_selected;
    }
}
