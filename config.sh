#!/bin/bash

#MySql credentials
DBHOST=localhost
DBNAME=erp3
DBUSER=root
DBPASSWD=root

#SCRIPT SETTINGS
RUN_INIT_CONFIG=true
DELETE_DB=false

#SSL CERTIFICATE CONFIGURATION
country=RO
state=Bihor
locality=Oradea
organization=ERP3
organizationalunit=IT
email=server@localhost.com
commonname=localhost