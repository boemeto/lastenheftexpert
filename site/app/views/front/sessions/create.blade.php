@extends('front.layouts.login')
        <!-- include de scripts necessary for google maps locations -->
@section('header_scripts')
  @include('utils/maps_location')
@stop

@section('footer_scripts')
  @include('utils/validations_login')
  @include('utils/ajax')

<script>
    //         reload page if pressed on user icon
    jQuery(".login-user-icon").click(function () {
        location.reload();
    });
    jQuery("#search_company_form .input-group-addon").click(function () {
        var val = jQuery('#searched_company').val();
        if (val == '') {
            location.reload();
        }
        else {
            jQuery("#step_1").click();
        }
    });
    jQuery("#button_Previous").click(function () {
        jQuery("#step_1").click();
    });
    var headQuarterDetailsClone = $('#divViewHeadquarters').html();

    $(document).on('click', '#button_Next', function() {
        $("html, body").animate({scrollTop: 0}, 1000);
        if($('.company-headquarter-selected').val()!=undefined) {
            if($('#info_headquarter').val() == '') {
                $('#info_headquarter').hide();
            } else {
                $('#info_headquarter').show();
            }
            if($('#info_name_company').val() == '') {
                $('#info_name_company').hide();
            } else {
                $('#info_name_company').show();
            }

            $('.login-msg').hide();
            $('#search_company_form').hide();
            $('.next-btn').hide();
            $('#back_to_register_company').hide();
            $('#divViewHeadquarters').hide();
            $('.found-headquarters').hide();
            $('.register-step1').hide();
            // $('#step-user').childrens('.form-group').hide();
            $('.info_name_company_container').css('display', 'none');
            $('.info_headquarter_container').css('display', 'none');
            $('#step-user').show();
            $('#step-user').find('.container-fluid').show();
            $('.hq-selected-msg').show();
            $('.register-user-btn').show();
            $('#back_to_register_company2').show();
        } else {
            $('#step-Login').show();
            $('#step-Login').find('.company-register').show();
            $('.headquarter_not_selected').show();

            $('#step-user').hide();
        }
    });

    $(document).on('click', '#back_to_register_company2', function() {
        $("html, body").animate({scrollTop: 0}, 1000);
        $('#back_to_register_company2').hide();
        $('#step-user').find('.container-fluid').hide();
        $('.hq-selected-msg').hide();
        $('.register-user-btn').hide();

        $('#divViewHeadquarters').show();
        $('.register-step1').show();
        $('#next1').show();
        $('#search_company_form').show();
        $('#step-Login').show();
        $('.found-headquarters').show();
        $('#back_to_register_company').show();
        $('.login-msg').show();
    });

    $(document).on('click', '#back_to_register_company3', function() {
        $("html, body").animate({scrollTop: 0}, 1000);
        $('#back_to_register_company3').hide();
        $('#back_to_register_company').hide();
        $('#next3').hide();
        $('.register-msg').hide();
        $('#divViewHeadquarters').hide();
        $('.found-headquarters').hide();

        $('#next2').show();
        $('#search_company_form').show();
        $('#step-Login').show();
        $('.company_logo').show();

        $('.register-step1').show();
        $('#company_details_hide').show();
        $('#form-group_cui_company').show();
        $('.register-step1').show();

        $('.login-msg').show();
        $('.found-company').show();
        $('#step-Login').show();
    });

    $(document).on('click', '#back_to_register_company4', function() {
        $("html, body").animate({scrollTop: 0}, 1000);
        $('#back_to_register_company4').hide();
        $('.register-user-btn').hide();
        $('.hq-selected-msg').hide();
        $('#step-user').hide();

        $('#next3').show();
        $('#back_to_register_company3').show();
        $('.register-msg').show();
        $('#step-Login').show();
    });

    $(document).on('click', '#button_Back', function() {
        location.reload();
    });

    $(document).on('keyup change', '#name_company', function() {
        $('#info_name_company').val($(this).val());
    });
    $(document).on('keyup change', '#name_headquarter', function() {
        $('#info_headquarter').val($(this).val());
    });

    // $(document).on('click', '#id_headquarter option', function() {
    //     $('#headquarter_hide').hide();
    // });

    $(document).on('change', '#id_headquarter', function() {
        var thisSelect = $(this);
        setTimeout(function(){
            $('#info_headquarter').val($('#divViewHeadquarterDetails .form-group:first input').val());
            if(thisSelect.find('option:selected').val() == '0') {
                jQuery('#divViewHeadquarterDetails').css('display', 'block');
                $('#divViewHeadquarterDetails').html(headQuarterDetailsClone);
            } else {
                // alert('dsadas');
                // jQuery('#headquarter_hide').css('display', 'none');
                $('#headquarter_hide').hide();
                $('.headquarter_not_selected').hide();
            }
          }, 200);
    });

    jQuery(function () {
        function log(name, id, email, phone, website) {
            jQuery("#step_1").click();
            jQuery('#name_company').val(name);
            jQuery('#company_details_hide').hide();
            jQuery('#info_name_company').val(name);
            jQuery('#name_company').attr('readonly', true);
            jQuery('#email_company').val(email);
            jQuery('#email_company').attr('readonly', true);
            jQuery('#telephone_company').val(phone);
            jQuery('#telephone_company').attr('readonly', true);
            jQuery('#website_company').val(website);
            jQuery('#website_company').attr('readonly', true);
            jQuery('#id_company').val(id);
            jQuery('#form-group_file').hide();
            jQuery('#form-group_cui_company').hide();
            if (id != '0') {
                viewHeadquarters(id);
            }
            jQuery('.text-red').empty();
            jQuery("div").removeClass("has-error");
            setTimeout(function(){
                jQuery('#divViewHeadquarterDetails').css('display', 'block');
                // console.log(headQuarterDetailsClone);
                $('#divViewHeadquarterDetails').html(headQuarterDetailsClone);
            }, 300);
        }

        var availableTags = [
            @foreach($companies as $company)
            {
                id: "{{$company->id_company}}",
                value: "{{$company->name_company}}",
                email: "{{$company->email_company}}",
                phone: "{{$company->telephone_company}}",
                website: "{{$company->website_company}}"
            },
            @endforeach
        ];

        jQuery("#searched_company").autocomplete({
            source: availableTags,
            minLength: 3,
            select: function (event, ui) {
                $('#cui_company').removeAttr('required');
                $('#email_company').removeAttr('required');
                $('#email_headquarter').removeAttr('required');
                $('#next1').show();
                $('#next2').hide();
                $('#back_to_register_company').show();
                $('#divViewHeadquarters').show();
                $('.found-headquarters').show();
                $('.found-company').hide();
                $('#divViewHeadquarterDetails').hide();
                log(ui.item ? ui.item.value : "", ui.item ? ui.item.id : '0');
            }
        });
    });

    $( "#next2" ).click(function() {
      if(switchToInvalidTab('.register-step1')) {
          $("html, body").animate({scrollTop: 0}, 1000);
          var elements = $('.register-step1 input');
          for(var i = 0; i < elements.length; i++) {
              $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
          }

          $('.login-msg').hide();
          $('#search_company_form').hide();
          $('.found-company').hide();
          $('.register-step1').hide();
          $('#company_details_hide').hide();
          $('#form-group_file').hide();
          $('#next2').hide();

          $('#divViewHeadquarterDetails').show();
          $('.company-register').show();
          $('.register-msg').show();
          $('#divViewHeadquarters').show();
          $('#back_to_register_company3').show();
          $('#next3').show();
      }
    });

    $( "#next3" ).click(function() {
      if(switchToInvalidTab('#divViewHeadquarterDetails')) {
          $("html, body").animate({scrollTop: 0}, 1000);
          var elements = $('#divViewHeadquarterDetails input');
          for(var i = 0; i < elements.length; i++) {
              $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
          }

          $('.register-msg').hide();
          $('.info_name_company_container').hide();
          $('.info_headquarter_container').hide();
          $('#next3').hide();
          $('#step-Login').hide();
          $('#back_to_register_company3').hide();

          $('.hq-selected-msg').show();
          $('#step-user').show();
          $('.register-user-btn').show();
          $('#back_to_register_company4').show();
      }
    });

    $(".register-user-btn").click(function() {
        if(switchToInvalidTab('#step-user')) {
            // $("html, body").animate({scrollTop: 0}, 1000);
            var elements = $('#step-user input');
            for(var i = 0; i < elements.length; i++) {
                $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
            }
        }
    });

    function switchToInvalidTab(form_id)
    {
        var error = false;
        var status = 'Bitte füllen Sie dieses Feld aus!';
        var elements = $(form_id + ' input');
        for(var i = 0; i < elements.length; i++) {
            // console.log(elements[i]);
            if(elements[i].required) {
                if($(elements[i]).val().length <= 0 || $(elements[i]).val() === '') {
                    $("html, body").animate({scrollTop: 0}, 300);
                    $(elements[i]).css({"border-color": "red", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                    // $("<div class='text-red' style='margin-bottom: -20px;'>" + status + "</div>").insertAfter(elements[i]);
                    error = true;
                } else {
                    $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                }
            }
        }

        if (error) {
           return false;
        }
        return true;
    }

    function validateEmail(el) {
        // console.log(el[0].attributes['data_url_verify']);
        switch (el[0].id) {
            case 'email_company':
                var placeholder = "E-Mail Adresse des Unternehmens*";
                break;
            case 'email_headquarter':
                var placeholder = "E-Mail Adresse*";
                break;
            case 'email':
                var placeholder = "E-Mail Adresse*";
                break;
            default:
                break;
        }

        var url_route = $(el[0]).attr('data_url_verify');
        var msg = "Diese E-Mail ist bereits registriert, bitte geben Sie eine andere ein.";
        var email = $(el).val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email) == false) {
            $(el).val('');
            $(el).attr("placeholder", "Invalid email").placeholder;
            $('.email-exists').hide();
            $(el).css({"border-color": "red", "border-width": "1px", "border-style": "solid"});
        } else {
            var emailCheck = $(el).val();
            var ajax = $.ajax({
                 async: false,
                 url: url_route,
                 dataType: 'json',
                 method: 'post',
                 data: {
                     'emailCheck' : emailCheck,
                },
            });

            if (ajax.status == 422) {
                $(el).attr("placeholder", placeholder).placeholder;
                $(el).val('');
                $(el).nextAll('div').remove();
                $("<div class='text-red email-exists' style='margin-bottom: -20px;'>" + msg + "</div>").insertAfter(el);
                $(el).css({'border-color': 'red', 'border-width': '1px', 'border-style': 'solid'});
            } else {
                $(el).nextAll('div').remove();
                $(el).css({'border-color': 'white', 'border-width': '1px', 'border-style': 'solid'});
            }
        }
    }

    $(document).on('ready', function() {
        $('.company_logo').find('div.btn-file').find('i.glyphicon')
            .removeClass("glyphicon glyphicon-folder-open")
            .addClass( "fa fa-upload form-fa-icon form-control-feedback" );

        $('.company_logo').find('div.btn-file').contents().filter(function() {
            return this.nodeType == 3
        }).each(function(){
            this.textContent = this.textContent.replace('Browse Logo', 'Logo hochladen');
        });

        $('.company_logo').find('button.fileinput-remove-button').contents().filter(function() {
            return this.nodeType == 3
        }).each(function(){
            this.textContent = this.textContent.replace('Remove', 'Löschen');
        });

        $('.user_logo').find('div.btn-file').find('i.glyphicon')
            .removeClass("glyphicon glyphicon-folder-open")
            .addClass( "fa fa-upload form-fa-icon form-control-feedback" );

        $('.user_logo').find('div.btn-file').contents().filter(function() {
            return this.nodeType == 3
        }).each(function(){
            this.textContent = this.textContent.replace('Browse Image','Bild hochladen');
        });

        $('.user_logo').find('button.fileinput-remove-button').contents().filter(function() {
            return this.nodeType == 3
        }).each(function(){
            this.textContent = this.textContent.replace('Remove', 'Löschen');
        });
    });

</script>
@stop

@section('content')
    @if (strstr('Please validate your account', $errors->first()))
        <div class="reset-password-title">
            <div class="login-user-icon">REGISTRIERUNG BESTÄTIGEN</div>
        </div>
        <div class="recover-password-msg">
            Sie haben Ihre Registrierung erfolgreich vorgenommen.
            <br><br>
            Zur Bestätigung Ihres Konto haben wir Ihnen eine E-Mail gesendet.
            <br><br>
            Bitte klicken Sie auf den Bestätigungslink in der E-Mail, um die Registrierung final abzuschließen.
        </div>
        <div class="reset-password-hr"></div>
        <div class="input-group ls-group-input login-btn-box login-button">
            <a href="{{URL::to('/login')}}" class="register-now back-to-login logo-color-1">ANMELDEN</a>
        </div>
    @else
      <div class="login-content">
          <div class="login-user-icon">
              REGISTRIEREN
          </div>
      </div>
      <div class="login-msg">
         Sie möchten prüfen, ob Ihr Unternehmen bereits bei
         Lastenheft.Expert GmbH registriert ist?
      </div>
      <div class="register-msg" style="display: none;">
        Suchen Sie automatisch oder geben Sie manuell die
        Daten Ihres Standortes ein
      </div>
      <div class="hq-selected-msg" style="display: none;">
        Geben Sie Ihre persönliche Benutzerdaten ein
      </div>
      {{-- the search form --}}
      <div class="login-form">
          <!-- /.box-header -->
          <div id="search_company_form">
              {{-- email validatin message--}}
              {{-- {{ $errors->first('searched_company','<div class="text-red">:message</div>') }} --}}
              <div class="input-group ls-group-input search_field form-group has-feedback">
                  {{ Form::text("searched_company", $searched_company,["placeholder"=>"Name der Firma eingeben ...",
                    "class"=>"form-control login-input", 'id'=>'searched_company',"required"]) }}
                  <span class="glyphicon glyphicon-search form-control-feedback login-glyphicon"></span>
              </div>
          </div>
          <div class="found-company">
            <div class="register-hr"></div>
            <div class="search-form-msg">
              Geben Sie ein oder wählen die Daten Ihres Unternehmens aus
            </div>
          </div>
          <div class="formular form-horizontal ls_form">
              {{ Form::open(['route'=>'users.store','id'=>'registerCompanyForm','enctype'=>"multipart/form-data", 'novalidate']) }}
              <div id="wizard" class="swMain">
                  <ul class="anchor">
                      <li>
                          <a href="#step-Login" class="selected" isdone="1" rel="1" id="step_1">
                              <span class="stepNumber">1</span>
                              <span class="stepDesc">
                                  Step 1<br>
                                  <small>Company details</small>
                              </span>
                          </a>
                      </li>
                      <li>
                          <a href="#step-user" class="done" isdone="1" rel="2" id="step_2">
                              <span class="stepNumber">2</span>
                              <span class="stepDesc">
                                  Step 2<br>
                                  <small>User info</small>
                              </span>
                          </a>
                      </li>
                  </ul>
                  <div class="stepContainer">
                      <div id="step-Login" class="content" style="display: block;">
                          {{-- <h2 class="StepTitle">Company Details</h2> --}}
                          <div class="container-fluid company-register">
                              {{ Form::hidden('searched_company',$searched_company) }}
                              {{ Form::hidden('id_company',0,['id'=>'id_company']) }}
                              <div class="form-group has-feedback register-step1">
                                  {{ Form::text('name_company','', ['placeholder'=>"Name des Unternehmens*",
                                  'class'=>"form-control login-input height50", 'id'=>'name_company', 'required']) }}
                                  {{-- {{ $errors->first('name_company','<div class="text-red">:message</div>') }} --}}
                                  <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                              </div>
                              <div id="company_details_hide" >
                                  <div class="form-group has-feedback register-step1" id="form-group_cui_company">
                                      {{ Form::text('cui_company', '', ['placeholder'=>"Umsatzsteuer-Identifikations-Nr. (USt-ID)*", 'id'=>'cui_company','class'=>"form-control login-input height50", 'required']) }}
                                      <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                      {{-- {{ $errors->first('cui_company','<div class="text-red">:message</div>') }} --}}
                                  </div>
                                  <div class="form-group has-feedback register-step1">
                                      {{ Form::email('email_company', '', ['placeholder'=>"E-Mail Adresse des Unternehmens*", 'id'=>'email_company', 'class'=>"form-control login-input",
                                        'required', 'data_url_verify'=> action('SessionsController@checkCompanyEmail'), 'onblur'=>'validateEmail($(this));']) }}
                                      <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                                      {{-- {{ $errors->first('email_company','<div class="text-red">:message</div>') }} --}}
                                  </div>
                                  <div class="form-group has-feedback">
                                      {{ Form::text("telephone_company", "", ["placeholder"=>"Telefonnummer des Unternehmens", 'id'=>'telephone_company', "class"=>"form-control login-input" ]) }}
                                      <span class="glyphicon glyphicon-earphone form-control-feedback login-glyphicon"></span>
                                      {{-- {{ $errors->first("telephone_company","<div class='text-red'>:message</div>") }} --}}
                                  </div>
                                  <div class="form-group has-feedback">
                                      {{ Form::text('fax_company', '', ['placeholder'=>"Unternehmens-Faxnummer", 'id'=>'fax_company',
                                      'class'=>"form-control login-input"]) }}
                                      <i class="fa fa-fax form-fa-icon form-control-feedback"></i>
                                      {{-- {{ $errors->first('fax_company','<div class="text-red">:message</div>') }} --}}
                                  </div>
                                  <div class="form-group has-feedback">
                                      {{ Form::text('website_company', '', ['placeholder'=>"Webseite des Unternehmens ", 'id'=>'website_company', 'class'=>"form-control login-input"]) }}
                                      <span class="glyphicon glyphicon-globe form-control-feedback login-glyphicon"></span>
                                      {{-- {{ $errors->first('website_company','<div class="text-red">:message</div>') }} --}}
                                  </div>
                                  {{-- <div class="form-group has-feedback"> --}}
                                      {{-- {{ Form::file('logo', '', ['placeholder'=>"Logo hochladen ", 'id'=>'logo', 'class'=>"form-control login-input"]) }} --}}
                                      {{-- <i class="fa fa-upload form-fa-icon form-control-feedback"></i> --}}
                                      {{-- {{ $errors->first('website_company','<div class="text-red">:message</div>') }} --}}
                                  {{-- </div> --}}
                              </div>

                              {{--<div class="custom-file-upload">--}}
                              {{--{{Form::file('logo',[ 'id'=>'logo', 'placeholder'=>'Logo','class'=>"form-control upload"])}}--}}
                              {{--</div>--}}
                              <div class="form-group upload_logo_file company_logo" id="form-group_file">
                                  <input id="file-3" type="file" name="logo" multiple="flase">
                              </div>
                              <div class="clear"></div>

                              {{-- <div style="display: block; width: 100%;">
                                  <h3 style=" float: left">Headquarter </h3>
                                  <button class="add_field_button btn ls-light-blue-btn btn-flat" style="float: right">
                                  <i class="fa fa-plus-circle "></i> Add More Headquarters
                                  </button>
                                  <div style="clear: both"></div>
                              </div> --}}
                              <br>
                              <div class="found-headquarters" style="display: none;">
                                <div class="register-hr"></div>
                                <div class="search-form-msg">
                                  Wählen einen Standort Ihres Unternehmens aus
                                </div>
                              </div>
                              <div id="divViewHeadquarters">
                                  <div id="divViewHeadquarterDetails" style="display: none;">
                                      <div class="form-group headquarterDetails has-feedback">
                                          <div id="locationField">
                                              {{ Form::text("autocomplete[1]","",array( "placeholder"=>"Adresse suchen ...*",
                                              "class"=>"form-control", "id"=>"autocomplete1","onFocus"=>"initialize('1')","required")) }}
                                          </div>
                                          {{-- {{ $errors->first("autocomplete[1]","<div class='text-red'>:message</div>") }} --}}
                                          <span class="glyphicon glyphicon-map-marker form-control-feedback login-glyphicon"></span>
                                      </div>
                                      <div class="form-group has-feedback">
                                          {{ Form::text("name_headquarter[1]","",["placeholder"=>"Name des Standortes*", "class"=>"form-control login-input height50",
                                             'id'=>"name_headquarter", "required"]) }}
                                          {{-- {{ $errors->first("name_headquarter[1]","<div class='text-red'>:message</div>") }} --}}
                                          <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                      </div>
                                      <div id="address1">
                                          <div class="form-group has-feedback">
                                              {{ Form::text("street[1]","",[ "placeholder"=>"Straße*", "class"=>"form-control","id"=>"route1", "required"]) }}
                                              {{-- {{ $errors->first("street[1]","<div class='text-red'>:message</div>") }} --}}
                                              <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                          </div>
                                          <div class="form-group has-feedback">
                                              {{ Form::text("street_number[1]","",[ "placeholder"=>"Nr.*", "class"=>"form-control","id"=>"street_number1", 'required' ]) }}
                                              {{-- {{ $errors->first("street_number[1]","<div class='text-red'>:message</div>") }} --}}
                                              <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                          </div>
                                          <div class="form-group has-feedback">
                                              {{ Form::text("postal_code[1]","",[ "placeholder"=>"PLZ*", "class"=>"form-control", "id"=>"postal_code1", 'required' ]) }}
                                              {{-- {{ $errors->first("postal_code[1]","<div class='text-red'>:message</div>") }} --}}
                                              <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                          </div>
                                          <div class="form-group has-feedback">
                                              {{ Form::text("city[1]","",[ "placeholder"=>"Ort*", "class"=>"form-control", "id"=>"locality1", "required"]) }}
                                              {{-- {{ $errors->first("city[1]","<div class='text-red'>:message</div>") }} --}}
                                              <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                          </div>
                                          <div class="form-group has-feedback">
                                              {{ Form::text("country[1]","",[ "placeholder"=>"Land*", "class"=>"form-control", "id"=>"country1", "required"]) }}
                                              {{-- {{ $errors->first("country[1]","<div class='text-red'>:message</div>") }} --}}
                                              <span class="glyphicon glyphicon-pencil form-control-feedback login-glyphicon"></span>
                                          </div>
                                          {{ Form::hidden("state[1]","",[ "id"=>"administrative_area_level_11" ]) }}
                                          {{ Form::hidden("latitude[1]","",[  "id"=>"latitude1" ]) }}
                                          {{ Form::hidden("longitude[1]","",[ "id"=>"longitude1" ]) }}
                                      </div>
                                      <div class="form-group has-feedback">
                                          {{ Form::email("email_headquarter[1]","",[ "placeholder"=>"E-Mail Adresse*", "class"=>"form-control", 'id'=>'email_headquarter',
                                            'required', 'data_url_verify'=> action('SessionsController@checkHeadquarterEmail'), 'onblur'=>'validateEmail($(this))']) }}
                                          <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                                          {{-- {{ $errors->first("email_headquarter[1]","<div class='text-red'>:message</div>") }} --}}
                                      </div>
                                      <div class="form-group has-feedback">
                                          {{ Form::text("telephone_headquarter[1]","",[ "placeholder"=>"Telefon", "class"=>"form-control" ]) }}
                                          <span class="glyphicon glyphicon-earphone form-control-feedback login-glyphicon"></span>
                                          {{-- {{ $errors->first("telephone_headquarter[1]","<div class='text-red'>:message</div>") }} --}}
                                      </div>
                                      <div class="form-group has-feedback">
                                          {{ Form::text('fax_headquarter[1]','',[ 'placeholder'=>"Faxnummer", 'id'=>'fax_headquarter', 'class'=>"form-control"]) }}
                                          <i class="fa fa-fax form-control-feedback form-fa-icon"></i>
                                          {{-- {{ $errors->first('fax_headquarter[1]','<div class="text-red">:message</div>') }} --}}
                                      </div>


                                      <div class="input_fields_wrap">
                                      </div>
                                  </div>
                                  <div class="clear"></div>
                              </div>
                          </div>
                      </div>
                      <div id="step-user" class="content" style="display: none;">
                          {{-- <div class="form-group">
                              <a class="btn button_Previous" id="button_Previous">Previous</a>
                          </div> --}}
                          <div class="form-group info_name_company_container">
                              <input class="form-control" id="info_name_company" type="text" value="" readonly="readonly">
                          </div>
                          <div class="form-group info_headquarter_container">
                              <input readonly="readonly" id="info_headquarter" class="form-control" type="text">
                          </div>
                          {{-- <h2 class="StepTitle">User Information</h2> --}}
                          <div class="container-fluid">
                              {{-- <div class="form-group" style="text-align: center">
                                  @foreach($personal_titles as $key=>$value)
                                      {{Form::radio('fk_personal_title', $key, false,['class'=>"icheck-green",'id'=>'fk_personal_title'])}} {{$value}}
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                  @endforeach
                              </div> --}}
                              <div class="form-group has-feedback">
                                  {{ Form::select("fk_title",$personal_titles,"",[ "placeholder"=>"Name*", "class"=>"form-control login-input",'id'=>'fk_personal_title', "required"]) }}
                                  {{-- {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::select("fk_title",$titles,"",[ "placeholder"=>"Name*", "class"=>"form-control login-input",'id'=>'fk_title', "required"]) }}
                                  {{-- {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("first_name","",[ "placeholder"=>"Vorname*", "class"=>"form-control login-input",'id'=>'first_name', "required"]) }}
                                  <span class="glyphicon glyphicon-user form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("first_name",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("last_name","",[ "placeholder"=>"Nachname*", "class"=>"form-control login-input",'id'=>'last_name', "required"]) }}
                                  <span class="glyphicon glyphicon-user form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("last_name",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::email("email","",["placeholder"=>"E-Mail Adresse*", "class"=>"form-control login-input", 'id'=>'email',
                                    "required", 'data_url_verify'=> action('SessionsController@checkUserEmail'), 'onblur'=>'validateEmail($(this))']) }}
                                  <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("email",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::email("email_confirm","",["placeholder"=>"E-Mail Adresse wiederholen*", "class"=>"form-control login-input", 'id'=>'email_confirm',"required", 'onblur'=>'validateEmail($(this))']) }}
                                  <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("email_confirm",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::password("password",[ "placeholder"=>"Passwort*", "class"=>"form-control login-input",'id'=>'password', "required"]) }}
                                  <span class="glyphicon glyphicon-lock form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("password",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::password("password_confirm",[ "placeholder"=>"Passwort bestätigen*", "class"=>"form-control login-input",'id'=>'password_confirm', "required"]) }}
                                  <span class="glyphicon glyphicon-lock form-control-feedback login-glyphicon"></span>
                                  {{-- {{ $errors->first("password_confirm",'<div class="text-red">:message</div>') }} --}}
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("job_user","" ,["placeholder"=>"Funktion im Unternehmen", "class"=>"form-control login-input", 'id'=>'job_user']) }}
                                  {{-- {{ $errors->first("job_user",'<div class="text-red">:message</div>') }} --}}
                                  <i class="glyphicon glyphicon-briefcase form-control-feedback login-glyphicon"></i>
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("mobile_user","" ,["placeholder"=>"Mobile phone", "class"=>"form-control login-input", 'id'=>'mobile_user']) }}
                                  {{-- {{ $errors->first("mobile_user",'<div class="text-red">:message</div>') }} --}}
                                  <i class="fa fa-mobile-phone form-fa-icon form-control-feedback" style="font-size: 25px"></i>
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("telephone_user","",[ "placeholder"=>"Telephone", "class"=>"form-control login-input", 'id'=>'telephone_user']) }}
                                  {{-- {{ $errors->first("telephone_user",'<div class="text-red">:message</div>') }} --}}
                                  <span class="glyphicon glyphicon-earphone form-control-feedback login-glyphicon"></span>
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("social_xing",'',[ "placeholder"=>"Xing Account", "class"=>"form-control login-input", 'id'=>'social_xing']) }}
                                  {{-- {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }} --}}
                                  <i class="fa fa-xing form-fa-icon form-control-feedback"></i>
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("social_twitter",'' ,["placeholder"=>"Twitter Account", "class"=>"form-control login-input", 'id'=>'social_twitter']) }}
                                  {{-- {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }} --}}
                                  <i class="fa fa-twitter form-fa-icon form-control-feedback"></i>
                              </div>
                              <div class="form-group has-feedback">
                                  {{ Form::text("social_linkedin", '' ,["placeholder"=>"LinkedIn Account", "class"=>"form-control login-input", 'id'=>'social_twitter']) }}
                                  {{-- {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }} --}}
                                  <i class="fa fa-linkedin form-fa-icon form-control-feedback"></i>
                              </div>
                              <div class="form-group user_logo" id="form-group_file">
                                  <input id="file-image" type="file" name="logo_user" multiple="false">
                              </div>
                              {{ $errors->first("confirm_user",'<div class="text-green">:message</div>') }}
                              <!-- /.col -->
                          </div>
                      </div>
                      <div class="headquarter_not_selected">
                      </div>
                      <div class="form-group register-next back-to-create" id="back_to_register_company" style="display: none;">
                          <a class="btn button_Back login-submit-btn logo-color-3 fs15" id="button_Back">ZURÜCK</a>
                      </div>
                      <div class="form-group register-next back-to-create" id="back_to_register_company2" style="display: none;">
                          <a class="btn button_Back login-submit-btn logo-color-3 fs15" id="button_Back2">ZURÜCK</a>
                      </div>
                      <div class="form-group register-next back-to-create" id="back_to_register_company3" style="display: none;">
                          <a class="btn button_Back login-submit-btn logo-color-3 fs15" id="button_Back3">ZURÜCK</a>
                      </div>
                      <div class="form-group register-next back-to-create" id="back_to_register_company4" style="display: none;">
                          <a class="btn button_Back login-submit-btn logo-color-3 fs15" id="button_Back4">ZURÜCK</a>
                      </div>
                      <div class="input-group register-user-btn ls-group-input login-btn-box col-md-12 col-sm-12 col-xs-12" style="display: none">
                          {{Form::submit('JETZT REGISTRIEREN',['class'=>"btn ls-dark-btn ladda-button create-account-btn logo-color-2 col-md-12 col-sm-12 col-xs-12"])}}
                      </div>
                      <div class="form-group register-next next-btn register-new-company" id="next1" style="display: none;">
                          <a class="btn button_Next login-submit-btn logo-color-2 fs15" id="button_Next">WEITER</a>
                      </div>
                      <div class="form-group register-next next-btn register-new-company" id="next2">
                          <a class="btn button_Next login-submit-btn logo-color-2 fs15" id="button_Next2">WEITER</a>
                      </div>
                      <div class="form-group register-next next-btn register-new-company" id="next3" style="display: none;">
                          <a class="btn button_Next login-submit-btn logo-color-2 fs15" id="button_Next3">WEITER</a>
                      </div>
                      <div class="register-hr"></div>
                      <div class="input-group ls-group-input login-btn-box">
                          <a href="{{URL::to('/login')}}" class="register-now login-submit-btn logo-color-1 lh50 fs16">ANMELDEN</a>
                      </div>
                  </div>
              </div>
              <!-- End SmartWizard Content -->
              {{ Form::close() }}
              <div class="clear"></div>
          </div>
          <div class="clear"></div>
      </div>
      </div>
      <div class="clear"></div>
      </div>
    @endif
@stop
