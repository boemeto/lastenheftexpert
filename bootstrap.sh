#!/bin/bash

#MySql credentials
DBHOST=localhost
DBNAME=erp3
DBUSER=root
DBPASSWD=root

#SCRIPT SETTINGS
RUN_INIT_CONFIG=false
DELETE_DB=false

source /var/www/config.sh

start=$(date +%s.%N)

################################################
#USEFULL FUNCTIONS
################################################
download()
{
    local url=$1
	local dldto=${2:-false}
    echo -n "   "
	if [ $dldto = false ]; then
		wget --progress=dot $url 2>&1 | grep --line-buffered "%" | \
			sed -u -e "s,\.,,g" | awk '{printf("\b\b\b%3s", $2)}'
	else
		wget -O $dldto --progress=dot $url 2>&1 | grep --line-buffered "%" | \
			sed -u -e "s,\.,,g" | awk '{printf("\b\b\b%3s", $2)}'
	fi
    echo -ne "\b\b\b\b"
    echo "Complete"
}

################################################
#RUN INIT CONFIG
################################################

if [ $RUN_INIT_CONFIG = true ]; then
	

	#Preselect the options for phpMyAdmin configuration
	debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"

	#Install phpMyAdmin
	sudo apt-get -y install phpmyadmin

	#Install phing
	sudo pear channel-discover pear.phing.info
	sudo pear config-set preferred_state beta
	sudo pear install phing/phing

	#Install Midnight Commander
	sudo add-apt-repository ppa:eugenesan/ppa
	sudo apt-get update
	sudo apt-get install -y mc

	#Overwrite default-ssl and 000-default VirtualHosts
	cp -f /var/www/distribution/local/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
	cp -f /var/www/distribution/local/default.conf /etc/apache2/sites-available/000-default.conf

	#Disable scotchbox.local VirtualHost
	sudo a2dissite scotchbox.local

	#Create SSL
	sudo mkdir /etc/apache2/ssl
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

	#Enable SSL mode and activate SSL VirtualHost
	sudo a2enmod ssl
	sudo a2ensite default-ssl
	sudo service apache2 restart
fi

################################################
#CREATE PUBLIC_HTML DIR IF NOT EXIST
################################################

if [ ! -d "/var/www/site" ]; then
	mkdir /var/www/site/
fi

################################################
#DELETE DATABASE
################################################

if [ $DELETE_DB = true ]; then
	mysql -u$DBUSER -p$DBPASSWD -e "DROP DATABASE IF EXISTS $DBNAME"
fi

################################################
#CREATE DATABASE
################################################

mysql -u$DBUSER -p$DBPASSWD -e "CREATE DATABASE IF NOT EXISTS $DBNAME"

################################################
#RUN SQL INITIAL SCRIPT IF EXISTS
################################################
if [ -f "/var/www/distribution/db/initial.sql" ]; then
	mysql -u$DBUSER -p$DBPASSWD $DBNAME < /var/www/distribution/db/initial.sql
fi

################################################
#RUN SQL POST INSTALL IF EXISTS
################################################

if [ -f "/var/www/db/postInstall.sql" ]; then
	mysql -u$DBUSER -p$DBPASSWD $DBNAME < /var/www/distribution/db/postInstall.sql
fi


end=$(date +%s.%N)    
runtime=$(python -c "print(${end} - ${start})")

echo "Box bootstrap completed in $runtime"
