<script type="text/javascript">

    function customValMsg(msg) {
        document.addEventListener('DOMContentLoaded', function () {
            var inputs = document.getElementsByTagName('input');
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].oninvalid = function (e) {
                e.target.setCustomValidity('');
                if (!e.target.validity.valid)
                    e.target.setCustomValidity(msg);
                };
            }
        });
    }

    // error messages and regex strings
    var check_msg = 'Please check the field';
    var empty_msg = "<?php echo trans('validation.validate_empty_field'); ?>";
    var multiple_fields_msg = "<?php echo trans('validation.validate_multiple_fields'); ?>";
    var string_err = '^[A-Za-z0-9\\s\_\�\.\&\,�������??��????��]+$';
    var string_msg = 'Please use only letters or numbers';
    var email_err = '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
    var email_msg = 'Please provide a correct email';
    var number_err = '^[0-9\�\+\.,\\s]+$';
    var number_msg = 'Please use only numbers';
    var date_err = '^(20)*[0-3][0-9][-.][0-1][0-9][.-](20)*[0-3][0-9](\\s[0-9]{1,2}(:[0-9]{1,2}){0,2}){0,1}$';
    var date_msg = 'Please provide a correct date';
    var tel_err = '^[0-9\.\,\+\(\)\\s\-]{6,30}$';
    var tel_msg = 'Please provide a correct phone';

    // used for regex expressions
    function validateField(id, msg, regex) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);
        var string_err = new RegExp(regex);

        // if regex does not mach
        if (string_err.test(val) == false) {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make filed red
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length == 0) {
                $field.parent().append('<div class="text-red" id="error' + id + '">' + msg + '</div>'); // insert message
            }
            validation *= false;
        } else {
            $field.parent().attr('class', 'form-group has-feedback');// remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // if industry is not selected when create project
    function emptySelectIndustry(id) {
        validation = true;
        $field = jQuery('#' + id);
        // if field empty
        if ($field.text().length == 0) {
            $field.parent().find('.chosen-single').css('border', '1px solid #ff7878');
            $('#formConfirmOk')
                .find('#frm_body').html(multiple_fields_msg + '. ')
                .end().modal('show');
            validation *= false;
            $("html, body").animate({ scrollTop: 500 }, "slow");
        } else {
            $field.parent().attr('class', 'form-group has-feedback'); // remove red class
            validation *= true;
        }
        return validation;
    }

    // used for empty fields
    function emptyField(id) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);
        // if field empty
        if (val.length == 0) {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make field red
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length == 0) {
                $field.parent().append('<div class="text-red" id="error' + id + '">' + empty_msg + '</div>');// insert message
            }
            $('#formConfirmOk')
                .find('#frm_body').html(multiple_fields_msg + '. ')
                .end().modal('show');
            // $("html, body").animate({ scrollTop: 0 }, "slow");
            validation *= false;
        } else {
            $field.parent().attr('class', 'form-group has-feedback'); // remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // used for select fields
    function selectedField(id) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);
        // if not selected
        if (val == 0 || val == "") {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make field red
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length == 0) {
                $field.parent().append('<div class="text-red" id="error' + id + '">' + multiple_fields_msg + '</div>');// insert message
            }
            validation *= false;
        } else {
            $field.parent().attr('class', 'form-group has-feedback');// remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // only for radio button //  needs name of field
    function checkedFieldRadio(id) {
        var checkboxes = $("input[type='radio'][name='" + id + "']");
        var text = $("input[type='radio'][name='" + id + "']").attr('data_msg');
        if (!checkboxes.is(":checked")) {
            checkboxes.parent().parent().parent().css('border-color', 'red'); // make field red
            alert('check radio');
            $('#formConfirmOk')
                // .find('#frm_body').html(check_msg + ': ' + text)
                .find('#frm_body').html(multiple_fields_msg)
                .end().modal('show');
            // $("html, body").animate({ scrollTop: 0 }, "slow");
            validation *= false;
        } else {
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    function checkedFieldCheckbox(id) {
        $field = jQuery('#' + id);
        $text = jQuery('#' + id).attr('data_msg');
        if (!$field.is(":checked")) {
            $field.parent().parent().css('border-color', 'red');
            $('#formConfirmOk')
                // .find('#frm_body').html(check_msg + ': ' + $text)
                .find('#frm_body').html(multiple_fields_msg)
                .end().modal('show');
            validation *= false;
        } else {
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

</script>
