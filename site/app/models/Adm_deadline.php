<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:31
 */
class Adm_deadline extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'start_date' => 'required',
    ];
    public $primaryKey = 'id_deadline';
    protected $table = 'adm_deadlines';
    protected $guarded = ['id_deadline'];
    protected $fillable = ['start_date', 'end_date', 'fk_user', 'fk_sprint', 'comment'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_deadline::where('id_deadline', '=', $id)->first();
        return $list;
    }
}