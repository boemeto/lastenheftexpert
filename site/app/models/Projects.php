<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 7/23/2015
 * Time: 9:52 AM
 */
class Project extends Eloquent
{


    public static $rules = [
        'fk_user'     => 'required',
        'fk_company'  => 'required',
        'fk_industry' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_project';
    protected $table = 'pr_projects';
    protected $guarded = ['id_project'];
    protected $fillable = ['name_project', 'fk_industry', 'fk_company', 'fk_user', 'nr_employees', 'nr_users',
        'max_budget', 'start_project', 'implement_time', 'due_date', 'is_licitation', 'is_empty_project',
        'has_consultant', 'is_expired'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Project::whereId_project($id)->first();
        return $list;
    }

    public static function getAllProjects()
    {
        $list = Project::orderBy('fk_company')
            ->join('companies', 'fk_company', '=', 'id_company')->get();
        return $list;
    }

    public static function getUsersProjects($fk_user, $limit = null)
    {
        $user = User::getFirstById($fk_user);
        $fk_group = User_groups::getGroupByUser($fk_user);
        $fk_company = $user->fk_company;

        if ($limit === null) {
          if (in_array('2', $fk_group)) {

              $projects = Project::where('fk_company', '=', $fk_company)
                  ->get();
          } else {
              $user_projects = User_projects::where('fk_user', '=', $fk_user)->get();
              $arr_projects = array();
              if (count($user_projects) > 0) {
                  foreach ($user_projects as $user_project) {
                      $arr_projects[] = $user_project->fk_project;
                  }
              }
              $projects = Project::whereIn('id_project', $arr_projects)
                  ->get();
          }
        } else {
          if (in_array('2', $fk_group)) {

              $projects = Project::where('fk_company', '=', $fk_company)
                  ->orderBy('id_project', 'desc')
                  ->limit($limit)
                  ->get();
          } else {
              $user_projects = User_projects::where('fk_user', '=', $fk_user)->get();
              $arr_projects = array();
              if (count($user_projects) > 0) {
                  foreach ($user_projects as $user_project) {
                      $arr_projects[] = $user_project->fk_project;
                  }
              }
              $projects = Project::whereIn('id_project', $arr_projects)
                  ->orderBy('id_project', 'desc')
                  ->limit($limit)
                  ->get();
          }
        }
        return $projects;
    }

    public static function getUsersCommonProjects($fk_user, $fk_user_2)
    {
// select common projects

        $fk_group = User_groups::getGroupByUser($fk_user_2);

        if (in_array('2', $fk_group)) {
            $user_projects = User_projects::select('fk_project')->where('fk_user', '=', $fk_user)
                ->groupBy('fk_project')
                ->get();
        } else {
            $user_projects = User_projects::select('fk_project')->whereIn('fk_user', [$fk_user_2, $fk_user])
                ->groupBy('fk_project')
                ->havingRaw('COUNT(fk_project) > 1')
                ->get();
        }

        $arr_projects = array();
        if (count($user_projects) > 0) {
            foreach ($user_projects as $user_project) {
                $arr_projects[] = $user_project->fk_project;
            }
        }
        $projects = Project::whereIn('id_project', $arr_projects)
            ->get();
        return $projects;
    }

}
