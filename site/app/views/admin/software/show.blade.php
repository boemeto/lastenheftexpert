@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/selectize.bootstrap3.css')}}">
@stop
@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/software/dropzone.js')}}"></script>
    <!-- Module menu tree style -->
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
    @include('admin/software/tree_script')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/software/software_scripts.js')}}"></script>

    {{--<script src="{{ URL::asset('assets/js/jquery.sticky.js')}}"></script>--}}
    @include('admin/scripts/ajax_scripts')

    <script type="text/javascript">
        $(document).on('change', 'select.form-control', function() {
            var str = $('#pair_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0 && (str.toLowerCase().indexOf("aus­wäh­len") >= 0 || str.toLowerCase().indexOf("select") >= 0)) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-color-button', function() {
            var pair_color = $('#pair_color').text();
            if ((pair_color.toLowerCase().indexOf("select") >= 0 || pair_color.toLowerCase().indexOf("aus­wäh­len") >= 0) && $('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-colors', function() {
            if (($(this).children('a[data-name="- Select -"]').length != 0 || $(this).children('a[data-name="- Aus­wäh­len -"]').length != 0) && $('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).ready(function () {
            var str = $('#pair_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0 && (str.toLowerCase().indexOf("aus­wäh­len") >= 0 || str.toLowerCase().indexOf("select") >= 0)) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }

            if ($('#scrollTo').length != 0) {
                $('html, body').animate({
                    scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50
                }, 'slow');
            }
            hide_empty_modules();

            //notification if user wants to download attachments and software doesn't have any tasks with attachments
            $('.get-attachments-zip').on('click', function(e){
                var countProjectAttachments = document.getElementsByClassName('attachment_counter');
                var sum = 0;
                for(var i=0; i < countProjectAttachments.length; i++) {
                   sum += parseInt(countProjectAttachments[i].innerHTML);
                }
                if(sum === 0) {
                   e.preventDefault();
                   var msg = "<?php echo trans('messages.no_attachments'); ?>";
                   $('#formConfirmOk')
                     .find('#frm_body').html(msg)
                     .end().find('#frm_title').html('Info')
                     .end().modal('show');
                }
            });

        });

        function select_pair_color(pair_element) {
            $('[name=fk_pair]').val($(pair_element).attr('data-id'));
            $('#pair_color').text($(pair_element).attr('data-name'));
            $('.pair-color-preview').css({'background-color': $(pair_element).attr('data-color')});
        };

        function hide_empty_modules() {
            $(".parent_li").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }
            });
            $(".li_not_selectable").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }
            });
        }

        // edit an existing task
        function modal_dialog_edit_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_task/" + id;
                var url = "{{ action('SoftwareController@upload_file_software', ":id")}}";
                url = url.replace(':id', id);
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                // call dropzone - drag and drop files

                                Dropzone.autoDiscover = false;
                                $("#uploadFiles").dropzone({
                                    url: url,
                                    addRemoveLinks: true,
                                    success: function (file, response) {
                                        var imgName = response;
                                        var id_ms_task = id;
                                        var attached_files, new_val;
                                        var _ref = file.previewElement;
                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                        new_val = parseInt(attached_files) + 1;
                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                        file.previewElement.classList.add("dz-success");
                                    },
                                    error: function (file, response) {
                                        file.previewElement.classList.add("dz-error");
                                    }
                                });

                                $(document).on('click', '.button_delete_attachment', function() {
                                    var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
                                    var msg = "Delete attachment " + title + "?";
                                    var id = jQuery(this).attr('id');
                                    var id_submodule = id.replace('button_delete_attachment', '');
                                    // alert(title+'--'+msg+'--'+id_nr);
                                    $('#formConfirm').find('#frm_body').html(msg)
                                                     .end().modal('show');

                                    $("#frm_submit").addClass('button_delete_file-ok');
                                    $("#frm_submit").append('<input class="modal-input" type="hidden"/>');
                                    $('.modal-input').attr('data-software-attachment-id', id_submodule);
                                });

                                $(document).on('click', '#frm_cancel', function() {
                                    $("#frm_submit").removeClass('button_delete_file-ok');
                                    $("#frm_submit").find('.modal-input').remove();
                                });

                                $(document).on('click', '.close', function() {
                                    $("#frm_submit").removeClass('button_delete_file-ok');
                                    $("#frm_submit").find('.modal-input').remove();
                                });

                                // add module/submodule for all industries
                                $(document).on('click', '.button_delete_file-ok', function () {
                                    var id_submodule = $(this).find('.modal-input').attr('data-software-attachment-id');
                                    var data = {
                                        'id_software_attachment': id_submodule
                                    };

                                    $(this).find('.modal-input').remove();
                                    $(this).removeClass('button_delete_file-ok');
                                    $('#formConfirm').modal('hide');

                                    jQuery.post('{{route("ajax.remove_file_software")}}', data)
                                            .done(function (msg) {
                                                if (parseInt(msg) > 0) {
                                                    var attached_files, new_val, id_ms_task;
                                                    id_ms_task = parseInt(msg);
                                                    attached_files = $('#attachment_counter' + id_ms_task).html();
                                                    new_val = parseInt(attached_files) - 1;
                                                    $('#attachment_counter' + id_ms_task).html(new_val);
                                                }
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                // alert(xhr.responseText);
                                            });
                                    jQuery('#col-attachments' + id_submodule).hide();
                                });

                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                // alert(xhr.responseText);
                                            });
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
            setTimeout( function() {
                // var attachmentHeight = $('.attachment-section').height();
                // $('.attachment-section').css('position', 'absolute');
                // $('.attachment-section').css('top', $('#task_description').outerHeight());
                // $('.attachment-section').css('width', '100%');
                // $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 1000);
        }

        // open add new task for an existing submodule
        function modal_dialog_add_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task/" + id;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });

                                $(".dropzone").dropzone({uploadMultiple: true});

                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                $('.dz-preview').hover(function () {

                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex});
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // open add new task for a newly created module/submodule
        function modal_dialog_add_task_new(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task_new/" + id + "/<?php print $software->id_software; ?>";
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });

                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code
                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');

                                        var data = {
                                            'id_software_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                $('.dz-preview').hover(function () {

                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex});
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });

                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }


        // drag and drop order tasks/ module
        $(function () {

            $('.sortable_task').sortable({
                axis: 'y',
                opacity: 0.7,
                group: '.sortable_task',
                tolerance: 'pointer',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();
//                    alert(list_sortable)
                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable,
                    };

                    $.post('{{  route("ajax.set_order_adm_task") }}', data)
                            .done(function (msg) {
//                            alert('ok')
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                }
            }); // fin sortable
            // do this for every level
            for (var level = 0; level <= 5; level++) {
                $('.sortable_modules_' + level).sortable({
                    axis: 'y',
                    opacity: 0.7,
                    handle: 'span',
                    update: function (event, ui) {
                        var list_sortable = $(this).sortable('toArray').toString();
                        // change order in the database using Ajax
                        var data = {
                            'list_order': list_sortable
                        };

                        $.post('{{  route("ajax.set_order_adm_modules") }}', data)
                                .done(function (msg) {
                                    // success
                                })
                                .fail(function (xhr, textStatus, errorThrown) {
                                    //  alert(xhr.responseText);
                                });
                    }
                }); // fin sortable
            }
        });

    </script>
    <script type="text/javascript">
        $('.menu_title>a').on('click', function(e) {
            e.preventDefault();
            $(this).parent().siblings('.easy-tree-toolbar').find('.remove').find('button').on('click', function(ev) {
                ev.preventDefault();
                var title = $(this).attr('data-title');
                var msg = $(this).attr('data-message');
                var dataForm = $(this).attr('data-form');
                var url = $(this).attr('href');

              $('#formConfirm')
                .find('#frm_body').html(msg)
                .end().find('#frm_title').html(title)
                .end().modal('show');
            });
        });
        // var check_error = '{{$errors->first()}}';
        // if(check_error!='') {
        //     $.amaran({
        //         'theme': 'colorful',
        //         'content': {
        //             message: '{{$errors->first()}}',
        //             bgcolor: '#324e59',
        //             color: '#fff'
        //         }
        //     });
        // }
    </script>
    @include('utils/validations_project')
@stop
@section('content')
    <div class="loader_back">
        <div class="loader_gif"></div>
    </div>
    <div class="box box-default">
        <!-- /.box-header -->
        <div class="box-body industry_module_structure ">
            <div class="row">
                <div class="col-xs-12 tabs">
                    <ul class="nav nav-tabs nav-left icon-tab profile-navigation-tabs">
                      <li class="{{($has_filters)?'':'active'}}"><a data-toggle="tab" href="#home">{{trans('messages.label_details')}}</a></li>
                      <li class="{{($has_filters)?'active':''}}"><a data-toggle="tab" href="#filters">{{trans('messages.label_filters')}}</a></li>
                      <li><a data-toggle="tab" href="#export">{{trans('messages.label_export')}}</a></li>
                      <li><a data-toggle="tab" href="#invoices">{{trans('messages.nav_import')}}</a></li>
                      <li><a data-toggle="tab" href="#other-software">{{trans('messages.nav_other_software')}}</a></li>
                    </ul>
                    <div class="tab-content profile-navigation-content">
                        <div id="home" class="tab-pane fade {{($has_filters)?'':' in active'}}">
                            <div class="row">
                                <div class=" col-lg-10 col-xs-9">
                                    <div class="row">
                                        <div class="  col-sm-6 col-xs-12">
                                            <table class="table-responsive tab-table">
                                                <tr>
                                                    <th>{{trans('messages.label_software_id')}}:&nbsp;</th>
                                                    <td>{{ $software->id_software }} </td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_software_name')}}:&nbsp;</th>
                                                    <td> {{ $software->name_software }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_software_created_at')}}:&nbsp;</th>
                                                    <td> {{ date('d.m.Y', strtotime($software->created_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Character Counter:&nbsp;</th>
                                                    <td><span class="description_word_counter"></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-3" style="text-align: center">
                                </div>
                            </div>
                        </div>
                        <div id="filters" class="tab-pane fade {{($has_filters)?'in active':' '}}">
                          <div class="panel-body filter_row">
                              {{ Form::open(['route'=>['software.show',$software->id_software], 'method' => 'get', 'class'=>'filterform','id'=>"formSearch"]) }}
                              <div class=" col-xs-12 col-lg-12 pd-10">
                                <div class="row">
                                    {{-- filter by checked--}}
                                    {{-- <div class="col-xs-12 col-lg-4 col-sm-4  pd-10 pdtp-0">
                                        <div class="form-group">
                                            <label>{{trans('messages.label_checked')}}: </label>
                                            {{ Form::select("fk_checked",$set_checked,$fk_checked,[   "class"=>"form-control"  ]) }}
                                        </div>
                                    </div> --}}
                                    {{-- filter by priority--}}
                                    <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                        <div class="form-group">
                                            <label>{{trans('messages.label_priority')}}:</label>
                                            {{ Form::select("fk_priority",$priorities, $fk_priority,[ "placeholder"=>"Priority", "class"=>"form-control priority-filter"  ]) }}
                                        </div>
                                    </div>
                                    {{-- filter by attachment--}}
                                    <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                        <div class="form-group">
                                            <label>{{trans('messages.label_attachments')}}:</label>
                                            {{ Form::select("fk_attach",$set_attach, $fk_attach,[   "class"=>"form-control attach-filter"  ]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    {{-- filter by default task--}}
                                    <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                        <div class="form-group">
                                            <label>{{trans('messages.label_default_task')}}: </label>
                                            {{ Form::select("is_default",$custom_task,$is_default,[ "placeholder"=>"Default", "class"=>"form-control default-filter"  ]) }}
                                        </div>
                                    </div>
                                    {{-- filter by pairs--}}
                                    <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 pd-10 pdtp-0">
                                        <?php
                                        $selected_pair = Task_pair::getFirstById($fk_pair);
                                        if (count($selected_pair) == 1) {
                                            $selected_color = $selected_pair->color_pair;
                                        } else {
                                            $selected_color = '#fff';
                                        }
                                        ?>
                                        <div class="form-group">
                                            <label>{{trans('messages.label_pair')}}:</label>

                                            {{-- <select name="fk_pair" class="form-control" id="task_pair" style="background: {{$selected_color}}"> --}}
                                                {{-- <option value="0" style=" background: #fff;"> {{trans('messages.input_select')}}</option> --}}
                                                {{-- @foreach($pairs as $pair) --}}
                                                    {{-- <option value="{{$pair->id_pair}}" data-color="{{$pair->color_pair}}" style=" background: {{$pair->color_pair}}" {{($fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option> --}}
                                                {{-- @endforeach --}}
                                            {{-- </select> --}}

                                            <div class="dropdown">
                                                <button class="btn dropdown-toggle form-control select-color-button"
                                                type="button" data-toggle="dropdown" name="color">
                                                    <span id="pair_color" class="">
                                                        {{$selected_pair->name_pair!=null?$selected_pair->name_pair: trans('messages.input_select')}}
                                                    </span>
                                                    <span class="pair-color-preview" style="background: {{$selected_color}}"></span>
                                                    <span class="caret pull-right select-arrow-down"></span>
                                                </button>
                                                <ul class="dropdown-menu color-dropdown">
                                                    <input id="task_pair" type="hidden" name="fk_pair">
                                                    {{-- @if ($selected_pair->name_pair!=null) --}}
                                                      <li class="select-colors">
                                                        <a data-id="0" data-name="{{trans('messages.input_select')}}" data-color="" onclick="select_pair_color(this)" href="#">
                                                          {{trans('messages.input_select')}}
                                                        </a>
                                                      </li>
                                                    {{-- @endif --}}
                                                    @foreach($pairs as $pair)
                                                      <li class="select-colors">
                                                        <a data-id="{{$pair->id_pair}}" data-name="{{$pair->name_pair}}" data-color="{{$pair->color_pair}}" onclick="select_pair_color(this)" href="#">{{$pair->name_pair}}
                                                          <span class="select-color-preview pull-right" style="background:{{$pair->color_pair}}"></span>
                                                          </a>
                                                      </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                  <div class="filter-buttons col-xs-6 col-lg-2 col-md-2 col-sm-3  pd-10 pdtp-0">
                                      <button type="submit" class="btn ls-light-blue-btn btn-block">
                                          {{trans('messages.act_filter')}}
                                      </button>
                                  </div>
                                  <div class="filter-buttons reset-button col-xs-6 col-lg-2 col-md-2 col-sm-3  pd-10 pdtp-0">
                                      <a href="{{URL::to('software/'.$software->id_software)}}" class="btn ls-red-btn btn-block">
                                          {{trans('messages.act_reset_filters')}}
                                      </a>
                                  </div>
                                  <div class="filter-buttons-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                      <button type="submit" class="btn btn-block" disabled>
                                          {{trans('messages.act_filter')}}
                                      </button>
                                  </div>
                                  <div class="filter-buttons-inactive reset-button-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                      <button type="submit" class="btn btn-block" disabled>
                                          {{trans('messages.act_reset_filters')}}
                                      </button>
                                  </div>
                              </div>
                              {{Form::close()}}
                          </div>
                        </div>
                        <div id="export" class="tab-pane fade export-panel">
                            <div class="export-links">
                                <a href="{{URL::to('download_software_pdf/'.$software->id_software)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_word/'.$software->id_software)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_excel/'.$software->id_software)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_attachments/'.$software->id_software)}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                            <div class="{{$export_file->file_name!=''?'export-details':''}}">
                                <div class="row">
                                  @if ($export_file->file_name!='')
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.last_export')}}:</strong>
                                    </div>
                                    <div class="col-xs-6">{{ $export_file->file_name  }}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->date!='0000-00-00 00:00:00' && $export_file->date!=null)
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.exported_at')}}:</strong>
                                    </div>
                                    <?php
                                        $date = explode(' ', $export_file->created_at);
                                        $hours = explode(':',$date[1]);
                                        $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                        $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                    ?>
                                    <div class="col-xs-6">{{ $date }}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->exported_by!='')
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.label_author')}}:</strong>
                                    </div>
                                    <div class="col-xs-6">{{ $export_file->exported_by  }}</div>
                                  @endif
                                </div>
                            </div>
                        </div>
                        <div id="invoices" class="tab-pane fade">
                          <div style="display: block">
                              <div class="row">
                                  <div class="col-xs-1">
                                      <form action="{{ action("SoftwareController@importExcel2") }}" method="post" enctype="multipart/form-data">
                                          <div class="btn btn-round btn-info btn-file import-software-btn">
                                              <i class="fa fa-upload fa-4x"></i>
                                              <input type="hidden" name="software_id" value="{{ $software->id_software }}">
                                              <input type="file" name="import_file" onchange="this.form.submit();">
                                          </div>
                                      </form>
                                  </div>
                                  <div class="col-xs-5">
                                      <div class="panel-body import-software-details">
                                          <div class="import-details-show">
                                              <div class="row">
                                                @if ($software->file_imported!='')
                                                  <div class="col-xs-5">
                                                    <strong>{{trans('messages.file_name')}}:</strong>
                                                  </div>
                                                  <div class="col-xs-6">{{ $software->file_imported  }}</div>
                                                @endif
                                              </div>
                                              <div class="row">
                                                @if ($software->import_date!='0000-00-00 00:00:00')
                                                  <div class="col-xs-5">
                                                    <strong>{{trans('messages.last_update')}}:</strong>
                                                  </div>
                                                  <?php
                                                      $date = explode(' ', $software->updated_at);
                                                      $hours = explode(':',$date[1]);
                                                      $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                                      $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                                  ?>
                                                  <div class="col-xs-6">{{$date}}</div>
                                                @endif
                                              </div>
                                              <div class="row">
                                                @if ($software->imported_by!='')
                                                  <div class="col-xs-5">
                                                    <strong>{{trans('messages.label_author')}}:</strong>
                                                  </div>
                                                  <div class="col-xs-6">{{ $software->imported_by  }}</div>
                                                @endif
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div id="other-software" class="tab-pane fade">
                          @if(count($all_softwares)>1)
                              <table class="table table-responsive table-bottomless data_table ">
                                  <thead>
                                       <tr>
                                           {{-- <th>{{trans('messages.label_nr_crt')}}</th> --}}
                                           <th>{{trans('messages.nav_software')}}</th>
                                           <th>{{trans('messages.file_name')}}</th>
                                           <th>{{trans('messages.last_update')}}</th>
                                           <th>{{trans('messages.label_author')}}</th>
                                           <th class="text-center">{{trans('messages.label_action')}}</th>
                                       </tr>
                                  </thead>
                                  <tbody>
                                      <?php $current_software_id = $software->id_software; ?>
                                      @foreach($all_softwares as $other_software)
                                            <?php
                                            // dd($other_software->id_software);
                                                $date = explode(' ', $other_software->import_date);
                                                $hours = explode(':',$date[1]);
                                                $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                                $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                            ?>
                                          @if ($current_software_id!=$other_software->id_software)
                                            <tr>
                                                {{-- <td>{{$other_software->id_software}}</td> --}}
                                                <td>{{$other_software->name_software}}</td>
                                                <td>{{$other_software->file_imported}}</td>
                                                <td>{{$other_software->import_date!='0000-00-00 00:00:00'?$date:''}}</td>
                                                <td>{{$other_software->imported_by}}</td>
                                                <td class="text-center">
                                                  <div class="row">
                                                      <div style="display: inline-block">
                                                          <form action="{{ action("SoftwareController@importExcel2") }}" method="post" enctype="multipart/form-data">
                                                              <div class="btn btn-round btn-xl btn-info btn-file software-table">
                                                                  <i class="fa fa-upload"></i>
                                                                  <input type="hidden" name="software_id" value="{{ $other_software->id_software }}">
                                                                  <input type="file" name="import_file" onchange="this.form.submit();">
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <div style="display: inline-block">
                                                          <a href="{{ URL::route('software.show',[$other_software->id_software])}}" id="{{$other_software->id_software}}"
                                                             class="btn btn-round btn-xl ls-light-green-btn software-table">
                                                              <i class="fa fa-sitemap"></i>
                                                          </a>
                                                      </div>
                                                  </div>
                                                </td>
                                            </tr>
                                          @endif
                                      @endforeach
                                  </tbody>
                              </table>
                          @else
                            <h3>{{trans('messages.no_projects_found')}}</h3>
                          @endif
                        </div>
                    </div>
                    <!--End tabs container-->
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <div style="display: block">
                        <div class="task_page_tree" id="task_page_tree">
                            <div class="easy-tree ls-tree-view">
                                <ul class="ul-tree">
                                    <li id="0" class="software">
                                    <span class="menu_title " id="software{{$software->id_software}}">
                                        <span class="glyphicon glyphicon-minus-sign"></span>
                                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                        <a href="javascript:void(0); ">{{ $software->name_software }}</a>
                                    </span>
                                    <input type="hidden" name="software_id" value="{{$software->id_software}}" />
                                    <input type="hidden" name="module_id" value="0" />
                                    <ul class="sortable_modules_0" id="list">
                                        <!-- found in helpers.php  - returns the tree based on model structure and software, starting lvl 1 -->
                                        <?php printTreeAdmin($module_structure, $software->id_software, 1, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks);?>
                                    </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="task_page_details_parent">
                            <div class="task_page_details" id="task_page_details"></div>
                        </div>
                    </div>
                    @if(Session::has('scrollTo'))
                        <input type="hidden" id="scrollTo" value="{{Session::get('scrollTo')}}">
                    @endif

                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="iframe-overlay">
    </div>

    <!--Modal Export - START-->
    <div id="modal-export" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="padding-15">
                        <div class="export-links">
                            <div class="show-software-export" style="display: none;">
                                <a href="{{URL::to('download_software_pdf/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_word/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_excel/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_attachments/soft_id')}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Export - END-->

    @include('admin.alert_box.delete_confirm')
    @include('admin.alert_box.confirm')
    @include('admin.alert_box.confirm_yes_no')
    @include('admin.alert_box.module_confirm_yes_no')
@stop
