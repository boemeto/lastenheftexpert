@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
    @include('utils/maps_location')
@stop
@section('footer_scripts')
    @if(in_array('2', $fk_group) || $id_user == $id_user_logged)
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('.delete_user').click(function (event) {
                    var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        return true;
                    }
                    else {
                        event.preventDefault();
                        return false;
                    }
                });
                /* User effects start*/

                /* Show user edit form */
                $('.a_form_show').click(function (event) {
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_show_', "");
                    $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                    $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                    //   $('.details_user_form_hidden').hide();
                    $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                    $('#edit_form_hidden_' + id_nr).toggle('blind');
                    event.preventDefault();
                });
                $('.a_form_close').click(function (event) {
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_close_', "");
                    $('#edit_form_hidden_' + id_nr).hide('blind');
                    event.preventDefault();
                });

                /* User effects stop*/


                // image input functions
                animated_text_area();
                file_input_trigger();
            });
            function animated_text_area() {
                'use strict';

                $('.animatedTextArea').autosize({append: "\n"});
            }
            /*** file input Call ****/
            function file_input_trigger() {
                'use strict';


                $("#file-user").fileinput({
                    showCaption: true,
                    removeLabel: "",
                    removeIcon: '<i class="fa fa-times"></i> ',
                    browseLabel: " ",
                    initialCaption: 'upload image',
                    browseClass: "btn ls-light-blue-btn btn-upload",
                    browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                    dropZoneEnabled: false,
                    previewFileIcon: ' ',
                    fileType: "any",
                    'showUpload': false
                });

            }
        </script>
        @endif
        @stop
        @section('content')
                <!-- visible content start -->
        <div class="box box-profile-user">
            <!-- /.box-body -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 user-pic-body">
                        <div class="panel widget light-widget panel-bd-top">
                            <div class="panel-body">
                                <div class="user-profile-details">

                                    <ul class="social-links-user">
                                        <li>
                                            @if($user->social_xing)
                                                <a href="{{$user->social_xing}}" target="_blank"
                                                   class="btn ls-btn ls-xing-btn btn-round"><i
                                                            class="fa fa-xing"></i></a>
                                            @else
                                                <a href="#"
                                                   class="btn ls-blue-btn btn-round
                                                                       disabled"><i
                                                            class="fa fa-xing"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($user->social_linkedin)
                                                <a href="{{$user->social_linkedin}}" target="_blank"
                                                   class="btn ls-btn ls-linkedin-btn btn-round"><i
                                                            class="fa fa-linkedin"></i></a>
                                            @else
                                                <a href="#"
                                                   class="btn ls-blue-btn btn-round
                                                                       disabled"><i
                                                            class="fa fa-linkedin"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($user->social_twitter)
                                                <a href="{{$user->social_twitter}}" target="_blank"
                                                   class="btn ls-btn ls-twitter-btn  btn-round"><i
                                                            class="fa fa-twitter"></i></a>
                                            @else
                                                <a href="#" class="btn ls-blue-btn btn-round disabled">
                                                  <i class="fa fa-twitter"></i>
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="text-center vd_info-parent square_main_image">
                                    <?php $image = User::getLogoUser($id_user);    ?>
                                    <img src="{{ URL::asset($image)}}" alt="image"></div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="user_group">

                                            @if(in_array('2', $fk_group_user))
                                                {{trans('messages.label_company_admin')}}
                                            @else
                                                {{trans('messages.label_company_user')}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <h3 class="font-semibold mgbt-xs-5 center"> {{Users_title::getUserTitle($user->id)}}   {{ $user->first_name  .' '.$user->last_name  }}</h3>
                                <div class="center">{{$user->job_user}}  </div>
                                <div class="mgtp-20">
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                        <tr>
                                            <td>{{trans('messages.label_member_since')}}:</td>
                                            <td> {{date('d.m.Y' ,strtotime($user->created_at))}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 tabs">
                        <ul class="nav  nav-tabs  nav-left icon-tab profile-navigation-tabs ">
                            <li class="active"><a href="#profile" data-toggle="tab">
                                    {{trans('messages.nav_profile')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>
                            <li class=""><a href="#coworkers" class="user-google-location" data-toggle="tab">
                                    {{trans('messages.label_coworkers')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>
                            <li class=""><a href="#projects" class="user-google-location" data-toggle="tab">
                                    {{trans('messages.nav_projects')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>

                            {{--<li class=""><a href="#affiliates" class="user-google-location" data-toggle="tab">--}}
                            {{--<i class="fa fa-link "></i> <span>Affiliates</span></a></li>--}}

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content   profile-navigation-content">
                            <!-- Profile Tab Start -->
                            @include('front/users/profile_tab')
                                    <!-- Profile Tab End -->
                            <!-- Projects Tab Start -->
                            @include('front/users/projects_tab')
                                    <!-- Projects Tab End -->
                            <!-- Coworkers Tab Start -->
                            @include('front/users/coworkers_tab')
                                    <!-- Coworkers Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


@stop
