<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11-Aug-16
 * Time: 10:24
 */
class Invoice_row extends Eloquent
{


    public static $messages = [];
    public $primaryKey = 'id_invoice_row';
    protected $table = 'invoice_row';
    protected $guarded = ['id_invoice_row'];
    protected $fillable = ['fk_invoice', 'fk_project', 'qty', 'unit_price'];


    public static function getFirstById($id)
    {
        $list = self::where('id_invoice_row', '=', $id)->first();
        return $list;
    }

}