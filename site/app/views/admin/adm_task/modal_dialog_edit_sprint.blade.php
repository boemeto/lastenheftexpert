<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    Sprint bearbeiten
    <div class="clear"></div>
</div>
{{ Form::model($sprint,['route'=>['adm_sprint.update',$sprint->id_sprint], 'method'=>'PATCH'])}}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">

                {{  Form::text('name_sprint',$sprint->name_sprint,['class'=>'form-control','placeholder'=>'Sprintname'])  }}
            </div>
            <div class="form-group">

                {{  Form::textarea('comment',$sprint->comment,['class'=>'form-control','placeholder'=>'Beschreibung'])  }}

                {{  Form::hidden('fk_release',$id_release)  }}
            </div>
            <div class="form-group">
                <div class="modal-switch-box">
                    <input name="is_closed" {{($sprint->is_closed ==1)?'checked':''}}
                    class="switchCheckBox" id="is_closed" value="1"
                           type="checkbox" data-size="mini">
                    Abgeschlossen
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn ls-light-blue-btn btn-block">
        {{trans('messages.act_save')}}
    </button>


    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>


{{Form::close()}}
       

 