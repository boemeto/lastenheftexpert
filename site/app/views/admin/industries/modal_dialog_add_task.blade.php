<script src="{{ URL::asset('assets/js/industry/dropzone_add_new.js')}}"></script>

{{Form::open(['action'=>'TaskController@add_new_task_admin_industry','id'=>'dropzoneForm', 'class'=>'dropzone dz-clickable', 'enctype'=>"multipart/form-data" ])}}

<a href="javascript:void(0)" onclick="hide_modal_box();">
    <div class="hide_controller">
        <i class="fa fa-times"></i>
    </div>
</a>
<div class="row">
    <div class="row text-right">
        <div class="col-md-2 pull-right pd-5" style="margin-top: 2px; width: 70px">
            {{Form::checkbox('is_default' ,1,true,['class'=>'switchCheckBox','data-size'=>"mini", 'style'=>'display:inline-block'])}}
        </div>
        <div class="col-md-2 pull-right pd-5">
          <i class="fa fa-info-circle" title="{{trans('messages.label_default_task_standard')}}"></i>
        </div>
    </div>
    <div class="col-xs-12 mgtp-10">
        <div class="title_line"> {{trans('messages.l_new')}} {{trans('messages.nav_task')}}</div>
    </div>
    <div class="col-md-12 small">
        {{trans('messages.l_for')}} {{$name_module}}
    </div>
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_task', '', [
                                        'class'=>"form-control",
                                        "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                        "required",
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task" class="summernote" placeholder="{{trans('messages.l_description')}}"></textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_task_en', '', [
                                        'class'=>"form-control",
                                        "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task_en" class="summernote " placeholder="{{trans('messages.l_description')}}"></textarea>
                                </div>
                            </div>
                        </div>
                        <div>
                          {{Form::hidden('id_industry_ms', $ind_ms_task->id_industry_ms,["id"=>"id_industry_ms"])}}
                          {{Form::hidden('fk_module_structure', $id_module_structure)}}
                            <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                            <label for="ac-2">{{trans('messages.label_sidebar_attachments')}}</label>
                            <article class="ac-any">
                                <div class="article_content border-tb add-task">
                                    <div class="dz-message">
                                        <span></span>
                                        <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="row" style="margin: 10px 0px">
                            <div class="col-xs-3 border-pd5">
                                {{Form::checkbox('is_checked' ,1, false,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}}
                                {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                            </div>
                            <div class="col-xs-3 col-xs-offset-3 border-pd5">
                                {{Form::checkbox('is_mandatory' ,1, false,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}}
                                {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="lvl_priority" id="task_pair" class="task-priority-pair">
                                    <option value="">{{trans('messages.label_priority')}}</option>
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->lvl_priority}}" {{($pr_module_task->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                    $selected_pair = Task_pair::getFirstById($pr_module_task->fk_pair);
                                    if (count($selected_pair) == 1) {
                                        $selected_color = $selected_pair->color_pair;
                                    } else {
                                        $selected_color = '#fff';
                                    }
                                ?>
                                <select name="fk_pair" id="task_pair" class="task-priority-pair" style="background: {{$selected_color}}">
                                    <option value="0" style=" background: #fff;">{{trans('messages.label_pair')}} {{trans('messages.nav_task')}}</option>
                                    @foreach($pairs as $pair)
                                        <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($pr_module_task->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="check-all">
                            <button type="button" class="add-check-all-icon" title="Check all" data-message="{{trans('messages.check_all_industries')}}">
                                <i class="fa fa-check" aria-hidden="true" ></i>
                            </button>
                            <button type="button" class="add-check-all-icon-inactive" title="Check all">
                                <i class="fa fa-check" aria-hidden="true" ></i>
                            </button>
                            <button type="button" class="add-uncheck-all-icon" title="Uncheck all" data-message="{{trans('messages.uncheck_all_industries')}}"
                                 style="display: inline-block">
                                <i class="fa fa-remove"></i>
                            </button>
                            <button type="button" class="add-uncheck-all-icon-inactive" title="Uncheck all" style="display: none">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                        <div class="tab-content tab-border select_industry">
                            @foreach($industries_types as $industry_type)
                                <?php
                                    $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>
                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">
                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <div class="check-all-type">
                                        @if (count($industries)>0)
                                            <button type="button" class="add-check-all-type" title="Check all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.check_all_categ_industries')}}">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-check-all-type-inactive" title="Check all">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type" title="Uncheck all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.uncheck_all_categ_industries')}}" style="display: none">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type-inactive" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @elseif(count($industries) == 0)
                                            <button type="button" class="inactive-btn" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button type="button" class="inactive-btn" title="Check all">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @endif()
                                    </div>
                                    <hr>
                                    @foreach($industries as $industry)
                                        <div class="industry-container" style="display: block" data-industry-id={{$industry->id_industry}}>
                                            <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$id_ms_task}}"
                                                {{$industry->id_industry == $id_industry?'checked':''}}
                                                name="fk_industries[]" value="{{$industry->id_industry}}" style="display: inline-block"> {{$industry->name_industry}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value=""/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value=""/>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<br>
{{Form::hidden('fk_industry', $id_industry)}}

<button type="submit" class="btn ls-light-blue-btn btn-block">
    {{trans('messages.act_save')}}
</button>
<button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
    {{trans('messages.act_close')}}
</button>

{{Form::close()}}

<script type="text/javascript">
  $('#dropzoneForm').on('click', function() {
     $(document).one('DOMNodeInserted', '.dz-preview', function(e) {
        $(e.target.nodeName).find('.dz-preview').each(function() {
           $(this).insertAfter('.dz-message');
        });
      });
  });
</script>

<script src="{{ URL::asset('assets/js/software/software_select_industries.js')}}"></script>
<script src="{{ URL::asset('assets/js/industry/industry_scripts.js')}}"></script>
