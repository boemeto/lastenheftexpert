<script>
    $(document).ready(function () {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                // table for the next headquarters
                $(wrapper).append('<div>' + '<h4> Location ' + x + '</h4>' +
                '<div class="form-group has-feedback">' +
                '<input type="text" name="name_headquarter[' + x + ']" placeholder="Name Headquarter" class="form-control"  id="name_headquarter' + x + '"  required />' +

                '<div class="form-group has-feedback">' +
                '<div id="locationField">' +

                '<input type="text" name="autocomplete[' + x + ']" placeholder="Enter headquarter address" class="form-control"  id="autocomplete' + x + '" onFocus="initialize(' + x + ')" required />' +
                '</div></div> <table id="address' + x + '"  class="table table-bordered table-responsive"> <tr> <td> ' +
                '<input type="text" name="street[' + x + ']" placeholder="Street name" class="form-control"  id="route' + x + '"  required />' +
                '</td> <td>' +
                '<input type="text" name="street_number[' + x + ']" placeholder="Street number" class="form-control"  id="street_number' + x + '"  />' +
                '</td> </tr> <tr> <td>' +
                '<input type="text" name="city[' + x + ']" placeholder="City" class="form-control"  id="locality' + x + '"  required />' +
                ' </td> <td>' +
                '<input type="text" name="postal_code[' + x + ']" placeholder="Postal Code" class="form-control"  id="postal_code' + x + '"  />' +
                '</td> </tr> <tr> <td colspan="2">' +
                '<input type="text" name="state[' + x + ']" placeholder="State" class="form-control"  id="administrative_area_level_1' + x + '"   />' +
                ' </td></tr> <tr> <td colspan="2">' +
                '<input type="text" name="country[' + x + ']" placeholder="Country" class="form-control"  id="country' + x + '"  required />' +
                '<input type="hidden" name="latitude[' + x + ']"  id="latitude' + x + '"    />' +
                '<input type="hidden" name="longitude[' + x + ']"  id="longitude' + x + '"    />' +
                '</td></tr> </table>' +
                '<div class="form-group has-feedback">' +
                '<input type="text" name="telephone_headquarter[' + x + ']" placeholder="Telephone Number" class="form-control"  id="telephone_headquarter' + x + '"   />' +
                '</div>' +
                '<div class="form-group has-feedback">' +
                '<input type="email" name="email_headquarter[' + x + ']" placeholder="Email" class="form-control"  id="email_headquarter' + x + '"  required />' +

                '<a href="#" class="remove_field"> ' +
                '<i class="fa fa-minus-circle"></i> </a>' +
                '</div>' +
                '</div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').parent('div').remove();
            x--;
        });
    });
</script>
