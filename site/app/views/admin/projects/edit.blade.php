{{ Form::model($project,['route'=>['projects.update',$project->id_project], 'method'=>'PATCH'])}}
<div class="row">
    <div class="col-md-12">
        <a href="javascript:void(0)" onclick="hide_modal_box();">
            <div class="hide_controller delay_hide_controller">
                <i class="fa fa-times"></i>
            </div>
        </a>
        <div class="title_line">    {{$project->name_project }} </div>
    </div>
    <div class="col-md-12">
      {{ Form::text('name_project',  $project->getAttribute('name_project','de'), [
          'class'=>"form-control",
          "placeholder"=> trans('messages.nav_module').' ' .trans('messages.l_name'),
          "required",
          'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
          "oninput"=>"this.setCustomValidity('')"
        ]) }}
    </div>
    <div class="col-md-12">
        <br>
        <button type="submit" class='btn ls-light-blue-btn btn-block'>
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>

{{Form::close()}}

<script type="text/javascript">
    $(".delay_hide_controller").parent().hide();
    $(".delay_hide_controller").parent().fadeIn(500);
    $(document).on('click', '.delay_hide_controller', function() {
        $(".delay_hide_controller").parent().hide();
    });
</script>
