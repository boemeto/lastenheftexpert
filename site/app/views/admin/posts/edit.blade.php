<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title">{{trans('messages.act_edit')}} {{trans('messages.nav_posts_pages')}}</h3>

    <div class="clear"></div>
</div>
{{ Form::model($post,['route'=>['posts.update',$post->id_post], 'method'=>'PATCH'])}}

<div class="modal-body">
    <div class="row pd-10">

        <div class="col-md-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_post',  $post->getAttribute('name_post','de'), ['class'=>"form-control",'placeholder'=>trans('messages.l_name')]) }}
                {{ $errors->first('name_post','<div class="alert alert-danger">:message</div>') }}
                <span class="input-group-addon">DE</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('content_post', $post->getAttribute('content_post','de'), ['class'=>"summernote",'placeholder'=>trans('messages.l_description')]) }}
                {{ $errors->first('content_post','<div class="alert alert-danger">:message</div>') }}
            </div>

        </div>
        <div class="col-md-6 pd-5">
            <div class="input-group ls-group-input">
                {{ Form::text('name_post_en',  $post->getAttribute('name_post','en'), ['class'=>"form-control",'placeholder'=>trans('messages.l_name')]) }}
                {{ $errors->first('name_post_en','<div class="alert alert-danger">:message</div>') }}
                <span class="input-group-addon">EN</span>
            </div>
            <div class="form-group">
                {{ Form::textarea('content_post_en',  $post->getAttribute('content_post','en'), ['class'=>"summernote",'placeholder'=>trans('messages.l_description')]) }}
                {{ $errors->first('content_post_en','<div class="alert alert-danger">:message</div>') }}
            </div>
        </div>
        <div class="col-md-12">
            {{ Form::text('slug_post',  $post->slug_post, ['class'=>"form-control",'placeholder'=>trans('messages.label_slug')]) }}
        </div>
    </div>

    <br>
</div>
<div class="modal-footer">

    <button type="submit" class='btn ls-light-blue-btn'>
        {{trans('messages.act_save_close')}}
    </button>

    <button type="button" class="btn btn-danger" data-dismiss="modal">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{Form::close()}}


 
