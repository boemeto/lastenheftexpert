<?php
if (Auth::check()) {
    $user = Auth::user();
    $id_user = $user->id;
    $name_user = ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
    $fk_group = User_groups::getGroupByUser($id_user);
}
?>

@if (Session::has('super-admin-id'))
    <li class="dropdown">
        <!--All task drop down start-->
        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
            <i class="fa fa-retweet"></i> {{trans('messages.nav_choose_soft')}}
        </a>
        <div class="dropdown-menu right top-dropDown-1">
            <h4>{{trans('messages.nav_choose_soft')}}</h4>
            <ul class="goal-item">
                <li><a href="{{URL::to('change_to_admin')}}">Administration</a></li>
            </ul>
        </div>
    </li>
@elseif (in_array(1, $fk_group))
    <li class="dropdown">
        <!--All task drop down start-->
        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
            <i class="fa fa-retweet"></i> {{trans('messages.nav_choose_soft')}}
        </a>
        <div class="dropdown-menu right top-dropDown-1">
            <h4>{{trans('messages.nav_choose_soft')}}</h4>
            <ul class="goal-item">
                <li><a href="{{URL::to('admin_home')}}">Administration</a></li>

            </ul>
        </div>
    </li>
@endif
<li class="dropdown">
    <!--All task drop down start-->
    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
        <i class="fa fa-globe"></i> {{trans('messages.language')}}
    </a>
    <div class="dropdown-menu right top-dropDown-1">
        <h4>{{trans('messages.language')}}</h4>
        <ul class="goal-item">
            <li>  {{link_to_route('language.select', 'English', array('en'))}}</li>
            <li>  {{link_to_route('language.select', 'Deutsch', array('de'))}}</li>
        </ul>
    </div>
    <!--All task drop down end-->
</li>
<li class="dropdown">
    <?php $image = User::getLogoUser($id_user);    ?>
    <a class="dropdown-toggle with_image" data-toggle="dropdown" href="javascript:void(0)">
        <div class="mega-name">
            <img src="{{ URL::asset($image)}}" alt="image" class="dropdown-image">
            {{--{{$name_user}}--}}
            <i class="fa fa-angle-down"></i>
        </div>
    </a>
    <div class="dropdown-menu right top-dropDown-1">
        <h4>{{trans('messages.nav_user_profile')}}</h4>
        <ul class="goal-item">
            <li>
                <a href="{{URL::to('users/'.$id_user)}}">{{trans('messages.nav_edit_profile')}} </a></li>
            <li><a href="{{ URL::to('logout') }}"> {{trans('messages.act_logout')}}</a></li>
        </ul>
    </div>
</li>
