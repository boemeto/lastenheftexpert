<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 14.05.2015
 * Time: 15:57
 */
class Groups extends Eloquent
{


    public static $rules = [
        'name_group' => 'required',

    ];
    public static $messages = [];
    public $primaryKey = 'id_group';
    protected $table = 'groups';
    protected $guarded = ['id_group'];
    protected $fillable = ['name_group', 'permissions'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getAllGroups()
    {
        $array = array();
        $lists = Groups::select('*')->get();
        $array[0] = 'Select';
        foreach ($lists as $list) {
            $array[$list->id_group] = $list->name_group;
        }
        return $array;
    }

    public static function getGroupsIds($id_user = 0, $is_selectable_user = 0)
    {
        $array = array();
        if ($id_user == 0) {
            $array[0] = 'Select';
            $lists = Groups::select('*');
        } else {
            $lists = Groups::join('users_groups', 'users_groups.fk_group', '=', 'groups.id_group')
                ->where('fk_user', '=', $id_user);
        }

        if ($is_selectable_user == 1) {
            $lists = $lists->whereIs_selectable_user(1);
        }
        $lists = $lists->get();

        foreach ($lists as $list) {
            $array[$list->id_group] = $list->id_group;
        }
        return $array;
    }

    public static function getGroups($id_user = 0, $is_selectable_user = 0)
    {
        $array = array();
        if ($id_user == 0) {
            $array[0] = 'Select';
            $lists = Groups::select('*');
        } else {
            $lists = Groups::join('users_groups', 'users_groups.fk_group', '=', 'groups.id_group')
                ->where('fk_user', '=', $id_user);
        }

        if ($is_selectable_user == 1) {
            $lists = $lists->whereIs_selectable_user(1);
        }
        $lists = $lists->get();

        foreach ($lists as $list) {
            $array[$list->id_group] = $list->name_group;
        }
        return $array;

    }


}