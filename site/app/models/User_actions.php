<?php

class User_actions extends Eloquent
{


    protected $table = 'user_actions';
    public $primaryKey = 'id_user_action';
    protected $guarded = ['id_user_action'];
    protected $fillable = ['fk_user', 'fk_action'];

    public static $rules = [
        'fk_user' => 'required',
        'fk_action' => 'required',
    ];
    public static $messages = [];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }


}