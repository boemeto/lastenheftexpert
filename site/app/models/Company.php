<?php

class Company extends Eloquent
{


    public static $rules = [
        'name_company' => 'required|min:4|unique:company',
        'email_company' => 'required|min:4|unique:company',
    ];
    public static $messages = [];
    public $primaryKey = 'id_company';
    protected $table = 'companies';
    protected $guarded = ['id_company'];
    protected $fillable = ['name_company', 'cui_company', 'intracomunitar_nr', 'website_company', 'email_company',
        'telephone_company', 'fax_company', 'social_xing', 'social_linkedin', 'social_twitter',
        'users_company', 'size_company', 'logo_company', 'is_blocked'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Company::whereId_company($id)->first();
        return $list;
    }

    public static function getCompanyByEmail($email)
    { 
        $list = Company::whereEmail_company($email)->first();
        return $list;
    }

    public static function getCompanyName($id)
    {
        $name = Company::whereId_company($id)->first();
        return $name->name_company;
    }

    public static function getAllCompanies()
    {
        $array = array();
        $lists = Company::select('*')->get();
        $array[0] = 'Select';
        foreach ($lists as $list) {
            $array[$list->id_company] = $list->name_company;
        }
        return $array;
    }

    public static function getLogoCompany($id)
    {
        $list = Company::whereId_company($id)->first();
        if ($list->logo_company != '') {
            $logo = 'images/image_company/' . $list->logo_company;
        } else {
            $logo = 'images/image_company/company_default.png';
        }
        return $logo;
    }

}
