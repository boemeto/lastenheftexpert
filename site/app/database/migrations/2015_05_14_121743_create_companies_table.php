<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function ($table) {
            $table->increments('id_company')->unsigned();
            $table->string('name_company', 100)->nullable();
            $table->string('cui_company', 100)->nullable();
            $table->string('intracomunitar_nr', 100)->nullable();
            $table->string('website_company', 100)->nullable();
            $table->string('email_company', 100)->unique();
            $table->string('telephone_company', 30)->nullable();
            $table->string('fax_company', 100)->nullable();
            $table->string('social_xing', 200)->nullable();
            $table->string('social_linkedin', 200)->nullable();
            $table->string('social_twitter', 200)->nullable();
            $table->integer('users_company')->nullable();
            $table->integer('size_company')->nullable();
            $table->string('logo_company', 200)->nullable();
            $table->tinyInteger('is_blocked')->nullable()->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }

}
