<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11-Aug-16
 * Time: 10:16
 */
class Invoice_status extends Eloquent
{


    public static $messages = [];
    public $primaryKey = 'id_status';
    protected $table = 'invoices_status';
    protected $guarded = ['id_status'];
    protected $fillable = ['name_status', 'name_status_en', 'color'];


    public static function getFirstById($id)
    {
        $list = self::where('id_status', '=', $id)->first();
        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('invoices_status', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;

            default:
                return $this->attributes[$field];
                break;
        }
    }

}