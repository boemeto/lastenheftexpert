{{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id]])}}

<div class="col-md-12">
    <div class="title_line">  {{trans('messages.act_edit')}}  {{$user->first_name}} {{$user->last_name}}</div>
</div>
<div class="col-md-12" style="text-align: center">
    <?php $image_user = User::getLogoUser($user->id);
    ?>
    <img class="img-circle" alt="friends pic" src="{{ URL::asset($image_user)}}">
    <br> <br>
</div>
<div class="col-md-12">
    <div class="form-group has-feedback">
        {{ $errors->first("fk_group","<div class='text-red'>:message</div>") }}
        <select name="fk_consultant[]" multiple class="form-control">
            @foreach($groups as $id_group => $group)
                <option value="{{$id_group}}" {{ (array_key_exists($id_group,$user_groups)?'selected':'')}} >{{$group}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group has-feedback">
        {{ Form::select("fk_headquarter",$headquarters,$user->fk_headquarter,[ 'id'=>'fk_headquarter', "class"=>"form-control", "required"]) }}
        {{ $errors->first("fk_headquarter","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::select("fk_personal_title",$personal_titles,$user->fk_personal_title,[ 'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }}
        {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::select("fk_title",$titles,$user->fk_title,[ "class"=>"form-control",'id'=>'fk_title', "required"]) }}
        {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::text("first_name",$user->first_name,[ "placeholder"=>"First Name", "class"=>"form-control",'id'=>'first_name', "required"]) }}
        {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::text("last_name",$user->last_name,[ "placeholder"=>"Last Name", "class"=>"form-control",'id'=>'last_name', "required"]) }}
        {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        {{ Form::email("email",$user->email,["placeholder"=>"Email", "class"=>"form-control", 'id'=>'email',"readonly"]) }}
        {{ $errors->first("email",'<div class="text-red">:message</div>') }}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group">
        <input id="file-user" type="file" name="logo" multiple="false">
    </div>
    <br>
</div>
<br>
<div class="col-md-6">
    <button type="submit" class='btn ls-light-blue-btn btn-block'>
        {{trans('messages.act_save')}}
    </button>
</div>
<div class="col-md-6">
    <button type="button" class='btn btn-danger btn-block' onclick="hide_modal_box()">
        {{trans('messages.act_cancel')}}
    </button>
</div>

{{ Form::close() }}
