@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
    @include('utils/maps_location')

@stop
@section('footer_scripts')

    <script>
        jQuery(document).ready(function ($) {

            $('.delete_user').click(function (event) {
                var x = confirm("Are you sure you want to delete?");
                if (x) {
                    return true;
                }
                else {

                    event.preventDefault();
                    return false;
                }
            });


            // show edit user form
            /* Headquarter effects start*/

            /* Headquarter edit form*/
            $('.a_form_headquarter').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_headquarter_', "");
                $('.a_headquarter_details_hidden').not('#a_headquarter_details_hidden_' + id_nr).removeClass('active');
                $('#a_headquarter_details_hidden_' + id_nr).toggleClass("active");
//                $('.div_headquarter_details_hidden').hide();
                $('.edit_form_hidden_headquarter').not('#edit_form_hidden_headquarter' + id_nr).hide();
                $('#edit_form_hidden_headquarter' + id_nr).toggle('blind');
                event.preventDefault();
            });

            /* Headquarter effects stop*/

            /* User effects start*/

            /* Show user edit form */
            $('.a_form_show').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_show_', "");
                $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                //   $('.details_user_form_hidden').hide();
                $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                $('#edit_form_hidden_' + id_nr).toggle('blind');
                event.preventDefault();
            });

            /* User effects stop*/


            // image input functions
            animated_text_area();
            file_input_trigger();
        })
        ;
        function animated_text_area() {
            'use strict';

            $('.animatedTextArea').autosize({append: "\n"});
        }
        /*** file input Call ****/
        function file_input_trigger() {
            'use strict';

            @foreach($all_users as $user)
              $("#file-user<?php print $user->id ?>").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Image ...',
                fileType: "any",
                'showUpload': false
            });
            @endforeach
            $("#file-3").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Logo ...',
                fileType: "any",
                'showUpload': false
            });
            $("#file-user").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Image ...',
                fileType: "any",
                'showUpload': false
            });

        }
    </script>
    @stop

    @section('content')
            <!-- visible content start -->
    <div class="box box-profile-user">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">


                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="{{Url::to('/')}}"><i class="fa fa-home"></i></a></li>
                        <li class="active">Company details</li>
                    </ol>
                    <!--Top breadcrumb start -->
                    {{--{{ $errors->first("confirm_",'<div class="text-green">:message</div>') }}--}}
                </div>
                <div class="col-md-12  ls_company_header">
                    <!-- Details  About the Company -->
                    <!--User Details Start-->
                    <div class="ls-user-details">
                        <div class="row">
                            <!--Company Details start-->
                            <div class="user-detail col-md-3">
                                <div class="ls-user-grey">
                                    <h1>{{ $company->name_company }}</h1>
                                    <ul class="social-links-user">

                                        <li>
                                            @if($company->social_xing)
                                                <a href="{{$company->social_xing}}" target="_blank"
                                                   class="btn ls-red-btn btn-round"><i
                                                            class="fa fa-xing"></i></a>
                                            @else
                                                <a href="#"
                                                   class="btn ls-red-btn btn-round 
                                                                       disabled"><i
                                                            class="fa fa-xing"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($company->social_linkedin)
                                                <a href="{{$company->social_linkedin}}" target="_blank"
                                                   class="btn ls-red-btn btn-round"><i
                                                            class="fa fa-linkedin"></i></a>
                                            @else
                                                <a href="#"
                                                   class="btn ls-red-btn btn-round 
                                                                       disabled"><i
                                                            class="fa fa-linkedin"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($company->social_twitter)
                                                <a href="{{$company->social_twitter}}" target="_blank"
                                                   class="btn ls-red-btn btn-round"><i
                                                            class="fa fa-twitter"></i></a>
                                            @else
                                                <a href="#"
                                                   class="btn ls-red-btn btn-round 
                                                                       disabled"><i
                                                            class="fa fa-twitter"></i></a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>

                                <div class="user-pic">
                                    <a href="javascript:void(0);">
                                        <?php $image = User::getLogoUser($id_user);    ?>

                                        <img src="{{ URL::asset($image)}}"
                                             alt="image">
                                        {{--<button class="btn ls-orange-btn"><i class="fa fa-edit"></i></button>--}}
                                    </a>
                                </div>
                            </div>
                            <div class="ls-user-info  col-md-5">
                                <div class="ls-user-text">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <span class="user_headquarter">
                                                             {{--<i class=" fa fa-map-marker"></i>--}}
                                                {{$main_headquarter->name_headquarter}} <br>
                                                {{$main_headquarter->street}} {{$user->street_nr}} <br>
                                                {{$main_headquarter->postal_code}}, {{$main_headquarter->city}} <br>
                                                {{$main_headquarter->country}} <br>
                                                        </span>
                                        </div>
                                        <div class="col-xs-6" style="word-break: break-all">
                                            @if($company->telephone_company )
                                                <div class="fa_width_user">
                                                    <i class=" fa fa-phone"></i>
                                                </div>
                                                {{$company->telephone_company}}<br>
                                            @endif
                                            @if($company->fax_company )
                                                <div class="fa_width_user">
                                                    <i class=" fa fa-fax"></i>
                                                </div>
                                                {{$company->fax_company}}<br>
                                            @endif

                                            <div class="fa_width_user">
                                                <i class=" fa fa-envelope-o"></i>
                                            </div>
                                            <a href="mailto:{{$company->email_company}}">{{$company->email_company}}</a>
                                            <br>
                                            @if($company->website_company )
                                                <div class="fa_width_user">
                                                    <i class=" fa fa-globe"></i>
                                                </div>
                                                <a href="{{$company->website_company}}" target="_blank">
                                                    {{$company->website_company}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="ls-user-green">
                                    <div class="ls-user-feature">
                                        <ul class="ls-user-circles">

                                            <li>
                                                <!-- User Feature List Start-->

                                                <div class="circle-border-white">
                                                    <div class="circle-border-green circle-border-inner">
                                                        <i class="fa fa-users"></i>
                                                    <span class="circle-number">
                                                {{count($all_users)}} </span>
                                                        <span class="circle-title"> Personen</span>
                                                    </div>
                                                </div>

                                                <!-- User Feature List Finish-->
                                            </li>
                                            <li>
                                                <!-- User Feature List Start-->
                                                <div class="circle-border-white">
                                                    <div class="circle-border-green circle-border-inner">
                                                        <i class="fa fa-map-marker"></i>
                                                    <span class="circle-number">
                                                {{count($headquarters)}} </span>
                                                        <span class="circle-title"> Filialen</span>
                                                    </div>
                                                </div>

                                                <!-- User Feature List Finish-->
                                            </li>
                                            <li>
                                                <!-- User Feature List Start-->
                                                <div class="circle-border-white">
                                                    <div class="circle-border-green circle-border-inner">
                                                        <i class="fa fa-sitemap"></i>
                                                    <span class="circle-number">
                                                {{count($projects)}} </span>
                                                        <span class="circle-title"> Projekte</span>
                                                    </div>
                                                </div>

                                                <!-- User Feature List Finish-->
                                            </li>

                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="ls-user-text-circles">
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>
                            <!--Company Details Finish-->
                        </div>
                    </div>
                    <!--User Details Finish-->
                </div>
                <div class="col-md-12 ">
                    <!--Tabs  Start-->
                    <div class="user-profile-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  nav-justified icon-tab">
                            <li class="active"><a href="#friends" data-toggle="tab">
                                    <i class="fa fa-users"></i> <span>Personen</span></a></li>
                            <li class=""><a href="#about-me" class="user-google-location" data-toggle="tab">
                                    <i class="fa fa-map-marker"></i> <span>Filialen</span></a></li>
                            {{--<li class=""><a href="#affiliates" class="user-google-location" data-toggle="tab">--}}
                            {{--<i class="fa fa-link "></i> <span>Affiliates</span></a></li>--}}

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <!-- Users Tab Start -->
                            @include('front/users/users_tab')
                                    <!-- Users Tab End -->

                            <!-- Headquarters Tab Start -->
                            @include('front/users/headquarters_tab')
                                    <!-- Headquarters Tab End -->
                        </div>
                    </div>
                    <!--Tabs  End-->
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- visible content end -->


@stop