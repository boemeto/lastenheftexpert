@extends('front.layouts.login')
        <!-- include de scripts necessary for google maps locations -->
@section('header_scripts')
@stop

@section('content')
    <div class="reset-password-title">
        <div class="login-user-icon">PASSWORT VERGESSEN</div>
    </div>
    <div class="login-form">
        <!-- /.box-header -->
        <div id="search_company_form">
        </div>
        @if( $errors->first() == "A password recovery email has been sent to you")
          <div class="recover-password-msg">
            Sie haben das Zurücksetzen Ihres Passwortes angefordert. Dazu haben wir Ihnen eine E-Mail gesendet.
            <br><br>
            Bitte klicken Sie auf den Link in der  E-Mail, um das Zurücksetzen Ihres Passwortes vorzunehmen.
          </div>
        @else
          <div class="formular form-horizontal ls_form">
              <form method="POST" action="{{action('SessionsController@postForgotPassword')}}" class="reset-password-form">
                  {{-- {{ $errors->first("confirm_",'<div class="amaran_confirm">:message</div>') }} --}}
                  {{-- {{ $errors->first("error_",'<div class="amaran_error">:message</div>') }} --}}
                  <input type="hidden" name="_method" value="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  <div class="form-group has-feedback">
                      {{ Form::email("email","",["placeholder"=>"E-Mail Adresse", "class"=>"form-control login-input", 'id'=>'email', "required",
                          'onblur'=>'validateEmail($(this));']) }}
                      <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                      {{-- {{ $errors->first("email",'<div class="text-red">:message</div>') }} --}}
                  </div>
                  <div class="form-group has-feedback">
                      {{ Form::email("email_confirm","",["placeholder"=>"E-Mail Adresse wiederholen", "class"=>"form-control login-input", 'id'=>'email_confirm', "required",
                          'onblur'=>'validateEmail($(this));']) }}
                      <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
                      {{-- {{ $errors->first("email_confirm",'<div class="text-red">:message</div>') }} --}}
                  </div>
                  @if ($errors->first() == "validation.same")
                    <div class="recover-password-msg">
                      E-mail stimmt nicht überein
                    </div>
                  @elseif($errors->first() == 'Das Passwort kann nicht zurückgesetzt werden. Die angegebene E-Mail Adresse ist nicht registriert.')
                    <div class="recover-password-msg">
                      {{$errors->first()}}
                    </div>
                  @endif
                  <div class="input-group ls-group-input login-btn-box col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="login-submit-btn logo-color-2 btn ladda-button col-md-12 col-sm-12 col-xs-12">
                          PASSWORT ZURÜCKSETZEN
                      </button>
                  </div>
              <!-- End SmartWizard Content -->
              </form>
              <div class="clear"></div>
          </div>
        @endif
        <div class="reset-password-hr"></div>
        <div class="input-group ls-group-input login-btn-box">
            <a href="{{URL::to('/login')}}" class="register-now back-to-login logo-color-1">ANMELDEN</a>
        </div>
        <div class="clear"></div>
    </div>
    </div>
    <div class="clear"></div>
    </div>
@stop

@section('footer_scripts')
    @include('utils/validations_login')
    @include('utils/ajax')

    <script>
        $(document).on('click', '.login-submit-btn', function(e) {
            if(switchToInvalidTab('.reset-password-form')) {
                $("html, body").animate({scrollTop: 0}, 1000);
                var elements = $('.reset-password-form input');
                for(var i = 0; i < elements.length; i++) {
                    $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
                }
            } else {
                e.preventDefault();
            }
        });
    </script>
@stop
