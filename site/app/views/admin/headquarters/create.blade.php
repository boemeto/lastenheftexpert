{{ Form::open(['action'=>'HeadquartersController@store','id'=>'addHeadquarterForm']) }}

<div class="col-md-12">
    <div class="title_line">  {{trans('messages.act_add')}}   {{trans('messages.nav_headquarter')}}    {{trans('messages.l_for')}} {{  $company->name_company  }}  </div>
</div>
<div class="col-md-12">


    <div class="form-group has-feedback">
        {{ Form::text("name_headquarter[1]","",[ "placeholder"=>"Name Headquarter", "class"=>"form-control",'required' ]) }}
        {{ $errors->first("name_headquarter[1]","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::text("telephone_headquarter[1]","",[ "placeholder"=>"Telephone Number", "class"=>"form-control" ]) }}
        {{ $errors->first("telephone_headquarter[1]","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        {{ Form::email("email_headquarter[1]","",[ "placeholder"=>"Email", "class"=>"form-control", "required" ]) }}
        {{ $errors->first("email_headquarter[1]","<div class='text-red'>:message</div>") }}
    </div>
    <div class="form-group has-feedback">
        <div id="locationField">

            {{ Form::text("autocomplete[1]","",array( "placeholder"=>"Enter headquarter address",
            "class"=>"form-control", "id"=>"autocomplete1","onFocus"=>"initialize('1')","required")) }}
            {{ $errors->first("autocomplete[1]","<div class='text-red'>:message</div>") }}
        </div>
    </div>
    <table id="address1" class="table table-bordered table-responsive">
        <tr>
            <td>
                {{ Form::text("street[1]","",[ "placeholder"=>"Street name", "class"=>"form-control","id"=>"route1","required"]) }}
                {{ $errors->first("street[1]","<div class='text-red'>:message</div>") }}
            </td>

            <td>
                {{ Form::text("street_number[1]","",[ "placeholder"=>"Street number", "class"=>"form-control","id"=>"street_number1","required" ]) }}
                {{ $errors->first("street_number[1]","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>

            <td>
                {{ Form::text("city[1]","",[ "placeholder"=>"City", "class"=>"form-control","id"=>"locality1","required"]) }}
                {{ $errors->first("city[1]","<div class='text-red'>:message</div>") }}
            </td>
            <td>
                {{ Form::text("postal_code[1]","",[ "placeholder"=>"Postal Code", "class"=>"form-control","id"=>"postal_code1", "required"]) }}
                {{ $errors->first("postal_code[1]","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>

            <td colspan="2">
                {{ Form::text("state[1]","",[ "placeholder"=>"State", "class"=>"form-control","id"=>"administrative_area_level_11" ]) }}
                {{ $errors->first("state[1]","<div class='text-red'>:message</div>") }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {{ Form::text("country[1]","",[ "placeholder"=>"Country", "class"=>"form-control","id"=>"country1","required"]) }}
                {{ $errors->first("country[1]","<div class='text-red'>:message</div>") }}
                {{ Form::hidden("latitude[1]","",[  "id"=>"latitude1" ]) }}
                {{ Form::hidden("longitude[1]","",[ "id"=>"longitude1" ]) }}
            </td>
        </tr>
    </table>


    <div class="input_fields_wrap">
    </div>

    {{ $errors->first("confirm_user",'<div class="text-green">:message</div>') }}
    {{ Form::hidden("id_company",$company->id_company) }}
    {{ Form::hidden("redirect_link",'acompany') }}

    <br>
</div>
<br>
<div class="col-md-6">
    <button type="submit" class='btn ls-light-blue-btn btn-block'>
        {{trans('messages.act_save')}}
    </button>
</div>
<div class="col-md-6">
    <button type="button" class='btn btn-danger btn-block' onclick="hide_modal_box()">
        {{trans('messages.act_cancel')}}
    </button>
</div>


{{ Form::close() }}