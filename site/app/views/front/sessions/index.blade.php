@extends('front.layouts.login')

@section('content')
    <div class="login-content" xmlns="http://www.w3.org/1999/html">
        <div class="login-user-icon">LOGIN</div>
    </div>
    <div class="login-form">
        {{ Form::open(array('action' => 'SessionsController@check_login', 'class'=>"form-horizontal ls_form", 'role'=>"login", 'novalidate'))}}
        {{-- {{ $errors->first("confirm_",'<div class="amaran_confirm">:message</div>') }} --}}
        {{-- {{ $errors->first("error_",'<div class="amaran_error">:message</div>') }} --}}
        <div class="form-group has-feedback login-container">
            {{-- {{ $errors->first('email_','<div class="text-red">:message</div><br>') }} --}}
            {{ Form::email('email_','',[ 'placeholder'=>"E-Mail Adresse", 'class'=>"form-control login-input", 'required', 'onblur'=>'validateEmail($(this));']) }}
            <span class="glyphicon glyphicon-envelope form-control-feedback login-glyphicon"></span>
        </div>
        <div class="form-group has-feedback login-container">
            {{ Form::password('password_',['placeholder'=>"Passwort", 'class'=>"form-control login-input", 'required']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback login-glyphicon"></span>
            {{-- {{ $errors->first('password_','<div class="text-red">:message</div>') }} --}}
        </div>
        <div class="input-group ls-group-input login-btn-box">
            <a class="forgot-password" href="{{ URL::to('forgot-password') }}">Passwort vergessen?</a>
        </div>
        {{-- {{ $errors->first('confirm_','<div class="text-green">:message</div>') }} --}}
        @if ($errors->first() == 'Incorrect email or password')
          <div class="login-msg">
              Falsche E-Mail oder Passwort
          </div>
        @elseif ($errors->first() == 'Please wait until an admin accept or reject you request')
          <div class="login-msg">
              Sie müssen bitte warten bis Ihre Registrierung vom Administrator geprüft wird
          </div>
        @endif
        <div class="input-group ls-group-input login-btn-box login-container">
            <button class="btn login-submit-btn ladda-button logo-color-2 col-md-12 col-sm-12 col-xs-12" data-style="slide-down">
                ANMELDEN
            </button>
        </div>
        <div class="login-hr"></div>
        <div class="input-group ls-group-input login-btn-box login-paragraf">
            <p>Neu bei Lastenheft.Expert GmbH? </p>
        </div>
        <div class="login-btn-box">
            <a class="register-now login-register-btn logo-color-1" href="{{ URL::to('sessions/create') }}">JETZT REGISTRIEREN</a>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('footer_scripts')
    @include('utils/validations_login')
    <script>
        $(document).on('click', '.login-submit-btn', function(e) {
            if(switchToInvalidTab('.form-horizontal')) {
                $("html, body").animate({scrollTop: 0}, 1000);
                var elements = $('.form-horizontal input');
                for(var i = 0; i < elements.length; i++) {
                    $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
                }
            } else {
                e.preventDefault();
            }
        });
    </script>
@stop
