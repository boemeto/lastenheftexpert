<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 7/23/2015
 * Time: 9:58 AM
 */

?>
@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')
    {{--// custom scripts--}}
    @include('front.projects.project_scripts')
@stop

@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="{{Url::to('/')}}"><i class="fa fa-home"></i></a></li>
                        <li class="active"> {{$project->name_project}}</li>
                    </ol>
                    <!--Top breadcrumb stop -->
                    {{ $errors->first("confirm_",'<div class="text-green">:message</div>') }}
                </div>
                <div class="col-xs-12" style="margin-left: -20px">


                    <div class="easy-tree ls-tree-view">
                        <ul class="ul-tree">
                            <li><span class="menu_title" id="submodule0"><a
                                            href="javascript:void(0); ">{{$project->name_project}}</a></span>
                                <ul>
                                    @foreach($module_structure as $module)
                                        @if(Module_structure::hasChild($module->id_module_structure) == 1)
                                            <li {{($module->is_default == 0)?'class="li_editable"':''}}>
                                                <span class="menu_title"
                                                      id="submodule{{$module->id_module_structure}}">{{$module->id_name}}</span>
                                                <ul>
                                                    <?php  $module_structure_2 = Module_structure::getModuleByLvl
                                                    ($industry->id_industry, 2, $module->id_module_structure,
                                                            $project->id_project);
                                                    ?>
                                                    @foreach($module_structure_2 as $module_2)
                                                        @if(Module_structure::hasChild($module_2->id_module_structure) == 1)
                                                            <li {{($module_2->is_default == 0)?'class="li_editable"':''}}>
                                                                <span class="menu_title"
                                                                      id="submodule{{$module_2->id_module_structure}}">{{$module_2->id_name}}</span>
                                                                <ul>
                                                                    <?php  $module_structure_3 =
                                                                            Module_structure::getModuleByLvl
                                                                            ($industry->id_industry, 3,
                                                                                    $module_2->id_module_structure, $project->id_project);
                                                                    ?>
                                                                    @foreach($module_structure_3 as $module_3)
                                                                        @if(Module_structure::hasChild($module_3->id_module_structure) == 1)
                                                                            <li {{($module_3->is_default == 0)?'class="li_editable"':''}}>
                                                                                <span class="menu_title"
                                                                                      id="submodule{{$module_3->id_module_structure}}"
                                                                                        >{{$module_3->id_name}}</span>
                                                                                <ul>
                                                                                    <?php  $module_structure_4 =
                                                                                            Module_structure::getModuleByLvl
                                                                                            ($industry->id_industry, 4,
                                                                                                    $module_3->id_module_structure, $project->id_project);
                                                                                    ?>
                                                                                    @foreach($module_structure_4 as $module_4)
                                                                                        @if(Module_structure::hasChild($module_4->id_module_structure) == 1)
                                                                                            <li {{($module_4->is_default == 0)?'class="li_editable"':''}}>
                                                                                                <span
                                                                                                        class="menu_title"
                                                                                                        id="submodule{{$module_4->id_module_structure}}">{{$module_4->id_name}}</span>
                                                                                            </li>
                                                                                        @else
                                                                                            <li class="{{($module_4->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                                                        <span class="module-menu-title menu_title"
                                                                                                              id="submodule{{$module_4->id_module_structure}}">
                                                                        {{$module_4->id_name}}</span>

                                                                                                <div class="row 
                                                                                                margin_left_minus4"
                                                                                                        >
                                                                                                    <?php
                                                                                                    $pr_module_tasks = Ms_task::getTasksByStructure($module_4->id_module_structure, $project->id_project); ?>
                                                                                                    <div class="col-xs-2 ">


                                                                                                        <div
                                                                                                                class="circle_tasks_left"
                                                                                                                id="circle_tasks_left{{$module_4->id_module_structure}}">
                                                                                                            {{Ms_task::getCheckedTasks($module_4->id_module_structure, $project->id_project)}}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-xs-10 inside-with-border">
                                                                                                        <div class="row">
                                                                                                            <div class="col-xs-11">
                                                                                                                @if(count($pr_module_tasks)>0)
                                                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                                                        <?php $attachments =
                                                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                                                            <style type="text/css">
                                                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                    padding-right: 7px;
                                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 {
                                                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                                                }

                                                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                                    right: -30px;
                                                                                                                                }
                                                                                                                            </style>
                                                                                                                        @endif
                                                                                                                        <div class="panel-task-border"
                                                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                                                                <div class="panel-task-header">
                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                                                             class="colorPicker">
                                                                                                                                            <a class="color"
                                                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                                                <div class="colorInner"
                                                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                                            </a>

                                                                                                                                            <div class="track"
                                                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>


                                                                                                                                            <input type="hidden"
                                                                                                                                                   class="colorInput"
                                                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                                                        </div>
                                                                                                                                    </div>

                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <a href="javascript:void(0);"
                                                                                                                                           data-toggle="modal"
                                                                                                                                           data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                                                           title="">
                                                                                                                                            <i class="fa fa-info-circle"></i></a>

                                                                                                                                    </div>
                                                                                                                                    <div class="ls-button-group">
                                                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                                                            {{ Form::hidden('fk_module_structure', $module_4->id_module_structure) }}
                                                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                                                            <button type="submit"
                                                                                                                                                    style="color: #FF7878;"
                                                                                                                                                    class="btn-icon">
                                                                                                                                                <i class="fa fa-trash"></i>
                                                                                                                                            </button>
                                                                                                                                            {{ Form::close() }}

                                                                                                                                        @endif
                                                                                                                                    </div>
                                                                                                                                    <div class="ls-button-group float-right">
                                                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1, false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="panel-task-body">
                                                                                                                                    {{ substr($pr_module_task->name_task,0,250)}}

                                                                                                                                </div>
                                                                                                                                data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"

                                                                                                                                <div class="clear"></div>
                                                                                                                                {{--// modal fade level 4--}}
                                                                                                                                <div class="modal fade"
                                                                                                                                     id="modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                                                     tabindex="-1"
                                                                                                                                     role="dialog"
                                                                                                                                     aria-labelledby="myModalLabel"
                                                                                                                                     aria-hidden="true"
                                                                                                                                     data-keyboard="false"
                                                                                                                                     data-backdrop="static"
                                                                                                                                     style="display: none;">
                                                                                                                                    <div class="modal-dialog">
                                                                                                                                        <div class="modal-content"
                                                                                                                                             id="borderInnerTask{{$pr_module_task->id_ms_task}}">
                                                                                                                                            <button type="button"
                                                                                                                                                    class="close close_modal"
                                                                                                                                                    id="close_modal{{$pr_module_task->id_ms_task}}"
                                                                                                                                                    data-dismiss="modal"
                                                                                                                                                    aria-hidden="true">
                                                                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                                                                x
                                                                                                                                            </button>
                                                                                                                                            <div class="modal-header task-label-white">
                                                                                                                                                <div class="ls-button-group float-left">
                                                                                                                                                    <div class="colorInnerTask"
                                                                                                                                                         id="colorInnerTask{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                                                </div>
                                                                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,      false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                                                    ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                                                </div>
                                                                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                                                                    {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
                                                                                                                                                      ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}

                                                                                                                                                </div>


                                                                                                                                                <div class="clear"></div>
                                                                                                                                            </div>
                                                                                                                                            <div class="modal-body">

                                                                                                                                                {{Form::open()}}
                                                                                                                                                <h4>{{$pr_module_task->name_task}}
                                                                                                                                                </h4>

                                                                                                                                                <div class="modal_description_task">
                                                                                                                                                    {{$pr_module_task->description_task}}
                                                                                                                                                </div>

                                                                                <textarea style="width: 100%" class="summernote"
                                                                                          id="summernote{{$pr_module_task->id_ms_task}}">
                                                                                        {{$pr_module_task->task_description}}
                                                                                </textarea>


                                                                                                                                                <div class="modal_description_task user_comment_task"
                                                                                                                                                     id="user_comment_task{{$pr_module_task->id_ms_task}}">
                                                                                                                                                    @if(strlen($pr_module_task->task_description)>0)
                                                                                                                                                        <div
                                                                                                                                                                class="modal_description_task_content">
                                                                                                                                                            {{$pr_module_task->task_description}}
                                                                                                                                                        </div>
                                                                                                                                                    @endif
                                                                                                                                                </div>
                                                                                                                                                <div
                                                                                                                                                        class="modal_editor_buttons">

                                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                                       id="button_save_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                                       class="btn ls-light-blue-btn button_save_task btn-margin-right"
                                                                                                                                                       style="display: none">
                                                                                                                                                        <i class="fa fa-floppy-o"></i>
                                                                                                                                                    </a>
                                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                                       id="button_add_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                                       class="btn ls-light-blue-btn button_add_task btn-margin-right 
                                                                                                               {{(strlen($pr_module_task->task_description)>0)?'disabled':''}}">
                                                                                                                                                        <i class="fa fa-plus"></i>
                                                                                                                                                    </a>
                                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                                       id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                                       class="btn ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                                                        <i
                                                                                                                                                                class="fa fa-edit"></i>
                                                                                                                                                    </a>
                                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                                       id="button_delete_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                                       class="btn ls-red-btn button_delete_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                                                        <i
                                                                                                                                                                class="fa fa-remove"></i>
                                                                                                                                                    </a>
                                                                                                                                                </div>
                                                                                                                                                {{Form::close()}}

                                                                                                                                                <div class="clear"></div>


                                                                                                                                                {{ Form::open(['action'=>'ProjectsController@upload_file',
                                                                                                                                                 'class'=>'dropzone dz-clickable',
                                                                                                                                                 'id'=>'dropzoneForm'.$pr_module_task->id_ms_task,
                                                                                                                                                 'enctype'=>"multipart/form-data"]) }}
                                                                                                                                                {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                                                                                                                                {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                                                                                                                                                <div class="dz-message">
                                                                                                                                                    <h4>
                                                                                                                                                        Drag
                                                                                                                                                        Files
                                                                                                                                                        to
                                                                                                                                                        Upload
                                                                                                                                                    </h4>
                                                                                                                                                    <span>Or click to browse</span>

                                                                                                                                                </div>
                                                                                                                                                {{Form::close()}}

                                                                                                                                                @if(count($attachments)>0)
                                                                                                                                                    <div class="modal-attachments">
                                                                                                                                                        <div
                                                                                                                                                                class="row">
                                                                                                                                                            @foreach($attachments as $attachment)
                                                                                                                                                                <?php    $icon =
                                                                                                                                                                        Projects_attach::getIconByAttachment($attachment->extension) ?>
                                                                                                                                                                <div class="col-lg-2 col-attachments"
                                                                                                                                                                     id="col-attachments{{$attachment->id_projects_attach}}">
                                                                                                                                                                    <div class="file_attachments"
                                                                                                                                                                         style="background: {{$icon}}"
                                                                                                                                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                                                                                                                                         title="{{$attachment->name_attachment }}">
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="attachments_edit_buttons"
                                                                                                                                                                         id="attachments_edit_buttons{{$attachment->id_projects_attach}}">

                                                                                                                                                                        <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                                                                                                                                           download
                                                                                                                                                                           class="button_download_file"
                                                                                                                                                                           target="_blank">
                                                                                                                                                                            <i class="fa fa-download"></i>
                                                                                                                                                                        </a><br>
                                                                                                                                                                        <a href="javascript:void(0)"
                                                                                                                                                                           id="button_delete_file{{$attachment->id_projects_attach}}"
                                                                                                                                                                           class="button_delete_file">
                                                                                                                                                                            <i class="fa fa-trash-o"></i>
                                                                                                                                                                        </a>
                                                                                                                                                                    </div>

                                                                                                                                                                </div>
                                                                                                                                                            @endforeach
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                @endif
                                                                                                                                                <div class="modal-switch-area">
                                                                                                                                                    <div
                                                                                                                                                            class="modal-switch-box">
                                                                                                                                                        <input name="is_not_offer"
                                                                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                                                                               {{($pr_module_task->is_not_offer ==1)?'checked':''}}
                                                                                                                                                               id="is_not_offer{{$pr_module_task->id_ms_task}}"
                                                                                                                                                               type="checkbox"
                                                                                                                                                               data-size="mini">
                                                                                                                                                        Von
                                                                                                                                                        der
                                                                                                                                                        Ausschreibung
                                                                                                                                                        ausgeschlossen
                                                                                                                                                    </div>
                                                                                                                                                    <div class="modal-switch-box">
                                                                                                                                                        <input name="is_control"
                                                                                                                                                               {{($pr_module_task->is_control ==1)?'checked':''}}
                                                                                                                                                               class="switchCheckBox is_control"
                                                                                                                                                               id="is_control{{$pr_module_task->id_ms_task}}"
                                                                                                                                                               type="checkbox"
                                                                                                                                                               data-size="mini">
                                                                                                                                                        Kontrolfrage
                                                                                                                                                    </div>
                                                                                                                                                </div>


                                                                                                                                            </div>
                                                                                                                                            <div class="modal-footer">
                                                                                                                                                <button type="button"
                                                                                                                                                        class="btn ls-light-green-btn">
                                                                                                                                                    {{--data-dismiss="modal">--}}
                                                                                                                                                    Save
                                                                                                                                                    and
                                                                                                                                                    Close
                                                                                                                                                </button>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    @endforeach
                                                                                                                @endif
                                                                                                            </div>
                                                                                                            <div class="col-xs-1">
                                                                                                                <div class="add_new_task">
                                                                                                                    <a href="javascript:void(0)"
                                                                                                                       class="btn ls-light-blue-btn"
                                                                                                                       data-toggle="modal"
                                                                                                                       data-target="#addNewTask{{$module_4->id_module_structure}}"
                                                                                                                            >
                                                                                                                        <i class="fa fa-plus"></i>
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                                <div class="modal fade"
                                                                                                                     id="addNewTask{{$module_4->id_module_structure}}"
                                                                                                                     tabindex="-1"
                                                                                                                     role="dialog"
                                                                                                                     aria-labelledby="myModalLabel"
                                                                                                                     aria-hidden="true"
                                                                                                                     data-keyboard="false"
                                                                                                                     data-backdrop="static"
                                                                                                                     style="display: none;">
                                                                                                                    <div class="modal-dialog">
                                                                                                                        <div class="modal-content"
                                                                                                                             id="borderInnerTask">
                                                                                                                            <button type="button"
                                                                                                                                    class="close close_modal"
                                                                                                                                    id="close_modal_new{{$module_4->id_module_structure}}"
                                                                                                                                    data-dismiss="modal"
                                                                                                                                    aria-hidden="true">
                                                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                                                x
                                                                                                                            </button>
                                                                                                                            {{ Form::open
                                                                                                                            (['action'=>'ProjectsController@add_new_task','id'=>''])}}

                                                                                                                            <div class="modal-header task-label-white">
                                                                                                                                <div class="ls-button-group float-left">
                                                                                                                                    <div class="colorInnerTask"
                                                                                                                                         id="colorInnerTask"></div>
                                                                                                                                </div>
                                                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                                                    {{Form::checkbox('checkbox', 1,      true,['class'=>"icheck-green",    'id'=>'icheck-green',   'disabled'])}}
                                                                                                                                </div>
                                                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                                                    {{Form::select('set_priority',  $set_priority,  '',
                                                                                                                                      ['class'=>'set_priority','id'=>'set_priority'])}}

                                                                                                                                </div>


                                                                                                                                <div class="clear"></div>
                                                                                                                            </div>
                                                                                                                            <div class="modal-body new-task">


                                                                                                                                <h4>
                                                                                                                                    <input type="text"
                                                                                                                                           class="form-control"
                                                                                                                                           placeholder="Task Name"
                                                                                                                                           name="name_task"
                                                                                                                                           required>
                                                                                                                                </h4>

                                                                                    

                                                                                <textarea style="width: 100%" name="task_description"
                                                                                          class="summernote_new"
                                                                                          id="summernote_new" required>
                                                                                </textarea>

                                                                                                                                <div class="clear"></div>


                                                                                                                                <div class="modal-switch-area">
                                                                                                                                    <div
                                                                                                                                            class="modal-switch-box">
                                                                                                                                        <input name="is_not_offer"
                                                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                                                               value="1"
                                                                                                                                               id="is_not_offer"
                                                                                                                                               type="checkbox"
                                                                                                                                               data-size="mini">
                                                                                                                                        Von
                                                                                                                                        der
                                                                                                                                        Ausschreibung
                                                                                                                                        ausgeschlossen
                                                                                                                                    </div>
                                                                                                                                    <div class="modal-switch-box">
                                                                                                                                        <input name="is_control"
                                                                                                                                               value="1"
                                                                                                                                               class="switchCheckBox is_control"
                                                                                                                                               id="is_control"
                                                                                                                                               type="checkbox"
                                                                                                                                               data-size="mini">
                                                                                                                                        Kontrolfrage
                                                                                                                                    </div>
                                                                                                                                </div>


                                                                                                                            </div>
                                                                                                                            <div class="modal-footer">
                                                                                                                                <button type="submit"
                                                                                                                                        class="btn ls-light-green-btn">
                                                                                                                                    Save
                                                                                                                                    and
                                                                                                                                    Close
                                                                                                                                </button>
                                                                                                                                {{Form::hidden('fk_project',
                                                                                                                                $project->id_project)}}
                                                                                                                                {{Form::hidden('fk_module_structure',
                                                                                                                                $module_4->id_module_structure)}}
                                                                                                                            </div>

                                                                                                                            {{Form::close()}}
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </li>
                                                                        @else
                                                                            <li class="{{($module_3->is_default 
                                                                                == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                                      <span class="module-menu-title 
                                                                                      menu_title"
                                                                                            id="submodule{{$module_3->id_module_structure}}">
                                                                        {{$module_3->id_name}}</span>

                                                                                <div class="row margin_left_minus3">
                                                                                    <?php      $pr_module_tasks =
                                                                                            Ms_task::getTasksByStructure($module_3->id_module_structure, $project->id_project); ?>
                                                                                    <div class="col-xs-2 ">


                                                                                        <div
                                                                                                class="circle_tasks_left"
                                                                                                id="circle_tasks_left{{$module_3->id_module_structure}}">
                                                                                            {{Ms_task::getCheckedTasks($module_3->id_module_structure, $project->id_project)}}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-10 inside-with-border">
                                                                                        <div class="row ">
                                                                                            <div class="col-xs-11">

                                                                                                @if(count($pr_module_tasks)>0)
                                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                                        <?php $attachments =
                                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                                            <style type="text/css">
                                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                    padding-right: 7px;
                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 {
                                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                                }

                                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                                    right: -30px;
                                                                                                                }
                                                                                                            </style>
                                                                                                        @endif
                                                                                                        <div class="panel-task-border"
                                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                                                <div class="panel-task-header">
                                                                                                                    <div class="ls-button-group">
                                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                                             class="colorPicker">
                                                                                                                            <a class="color"
                                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                                <div class="colorInner"
                                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                            </a>

                                                                                                                            <div class="track"
                                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>


                                                                                                                            <input type="hidden"
                                                                                                                                   class="colorInput"
                                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="ls-button-group">
                                                                                                                        <a href="javascript:void(0);"
                                                                                                                           data-toggle="modal"
                                                                                                                           data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                                           title="">
                                                                                                                            <i class="fa fa-info-circle"></i></a>
                                                                                                                    </div>
                                                                                                                    <div class="ls-button-group">
                                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                                            {{ Form::hidden('fk_module_structure', $module_3->id_module_structure) }}
                                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                                            <button type="submit"
                                                                                                                                    style="color: #FF7878;"
                                                                                                                                    class="btn-icon">
                                                                                                                                <i class="fa fa-trash"></i>
                                                                                                                            </button>
                                                                                                                            {{ Form::close() }}

                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                    <div class="ls-button-group float-right">
                                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                                        false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="panel-task-body">
                                                                                                                    {{ substr
                                                                                                                    ($pr_module_task->name_task,0,250)}}

                                                                                                                </div>

                                                                                                                <div class="clear"></div>
                                                                                                                {{--// modal 
                                                                                                                fade level 3--}}
                                                                                                                <div class="modal fade"
                                                                                                                     id="modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                                     tabindex="-1"
                                                                                                                     role="dialog"
                                                                                                                     aria-labelledby="myModalLabel"
                                                                                                                     aria-hidden="true"
                                                                                                                     data-keyboard="false"
                                                                                                                     data-backdrop="static"
                                                                                                                     style="display: none;">
                                                                                                                    <div class="modal-dialog">
                                                                                                                        <div class="modal-content"
                                                                                                                             id="borderInnerTask{{$pr_module_task->id_ms_task}}">
                                                                                                                            <button type="button"
                                                                                                                                    class="close close_modal"
                                                                                                                                    id="close_modal{{$pr_module_task->id_ms_task}}"
                                                                                                                                    data-dismiss="modal"
                                                                                                                                    aria-hidden="true">
                                                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                                                x
                                                                                                                            </button>
                                                                                                                            <div class="modal-header task-label-white">
                                                                                                                                <div class="ls-button-group float-left">
                                                                                                                                    <div class="colorInnerTask"
                                                                                                                                         id="colorInnerTask{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                                </div>
                                                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                                                    false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                                    ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                                </div>
                                                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                                                    {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
                                                                                                                                      ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}

                                                                                                                                </div>


                                                                                                                                <div class="clear"></div>
                                                                                                                            </div>
                                                                                                                            <div class="modal-body">

                                                                                                                                {{Form::open()}}
                                                                                                                                <h4>{{$pr_module_task->name_task}}
                                                                                                                                </h4>

                                                                                                                                <div class="modal_description_task">
                                                                                                                                    {{$pr_module_task->description_task}}
                                                                                                                                </div>

                                                                                <textarea style="width: 100%" class="summernote"
                                                                                          id="summernote{{$pr_module_task->id_ms_task}}">
                                                                                        {{$pr_module_task->task_description}}
                                                                                </textarea>


                                                                                                                                <div class="modal_description_task user_comment_task"
                                                                                                                                     id="user_comment_task{{$pr_module_task->id_ms_task}}">
                                                                                                                                    @if(strlen($pr_module_task->task_description)>0)
                                                                                                                                        <div
                                                                                                                                                class="modal_description_task_content">
                                                                                                                                            {{$pr_module_task->task_description}}
                                                                                                                                        </div>
                                                                                                                                    @endif
                                                                                                                                </div>
                                                                                                                                <div
                                                                                                                                        class="modal_editor_buttons">

                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                       id="button_save_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                       class="btn ls-light-blue-btn button_save_task btn-margin-right"
                                                                                                                                       style="display: none">
                                                                                                                                        <i class="fa fa-floppy-o"></i>
                                                                                                                                    </a>
                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                       id="button_add_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                       class="btn ls-light-blue-btn button_add_task btn-margin-right 
                                                                                                               {{(strlen($pr_module_task->task_description)>0)?'disabled':''}}">
                                                                                                                                        <i class="fa fa-plus"></i>
                                                                                                                                    </a>
                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                       id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                       class="btn ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                                        <i
                                                                                                                                                class="fa fa-edit"></i>
                                                                                                                                    </a>
                                                                                                                                    <a href="javascript:void(0)"
                                                                                                                                       id="button_delete_task{{$pr_module_task->id_ms_task}}"
                                                                                                                                       class="btn ls-red-btn button_delete_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                                        <i
                                                                                                                                                class="fa fa-remove"></i>
                                                                                                                                    </a>
                                                                                                                                </div>
                                                                                                                                {{Form::close()}}

                                                                                                                                <div class="clear"></div>


                                                                                                                                {{ Form::open(['action'=>'ProjectsController@upload_file',
                                                                                                                                 'class'=>'dropzone dz-clickable',
                                                                                                                                 'id'=>'dropzoneForm'.$pr_module_task->id_ms_task,
                                                                                                                                 'enctype'=>"multipart/form-data"]) }}
                                                                                                                                {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                                                                                                                {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                                                                                                                                <div class="dz-message">
                                                                                                                                    <h4>
                                                                                                                                        Drag
                                                                                                                                        Files
                                                                                                                                        to
                                                                                                                                        Upload
                                                                                                                                    </h4>
                                                                                                                                    <span>Or click to browse</span>

                                                                                                                                </div>
                                                                                                                                {{Form::close()}}

                                                                                                                                @if(count($attachments)>0)
                                                                                                                                    <div class="modal-attachments">
                                                                                                                                        <div
                                                                                                                                                class="row">
                                                                                                                                            @foreach($attachments as $attachment)
                                                                                                                                                <?php    $icon =
                                                                                                                                                        Projects_attach::getIconByAttachment($attachment->extension) ?>
                                                                                                                                                <div class="col-lg-2 col-attachments"
                                                                                                                                                     id="col-attachments{{$attachment->id_projects_attach}}">
                                                                                                                                                    <div class="file_attachments"
                                                                                                                                                         style="background: {{$icon}}"
                                                                                                                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                                                                                                                         title="{{$attachment->name_attachment }}">
                                                                                                                                                    </div>
                                                                                                                                                    <div class="attachments_edit_buttons"
                                                                                                                                                         id="attachments_edit_buttons{{$attachment->id_projects_attach}}">

                                                                                                                                                        <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                                                                                                                           download
                                                                                                                                                           class="button_download_file"
                                                                                                                                                           target="_blank">
                                                                                                                                                            <i class="fa fa-download"></i>
                                                                                                                                                        </a><br>
                                                                                                                                                        <a href="javascript:void(0)"
                                                                                                                                                           id="button_delete_file{{$attachment->id_projects_attach}}"
                                                                                                                                                           class="button_delete_file">
                                                                                                                                                            <i class="fa fa-trash-o"></i>
                                                                                                                                                        </a>
                                                                                                                                                    </div>

                                                                                                                                                </div>
                                                                                                                                            @endforeach
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                @endif
                                                                                                                                <div class="modal-switch-area">
                                                                                                                                    <div
                                                                                                                                            class="modal-switch-box">
                                                                                                                                        <input name="is_not_offer"
                                                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                                                               {{($pr_module_task->is_not_offer ==1)?'checked':''}}
                                                                                                                                               id="is_not_offer{{$pr_module_task->id_ms_task}}"
                                                                                                                                               type="checkbox"
                                                                                                                                               data-size="mini">
                                                                                                                                        Von
                                                                                                                                        der
                                                                                                                                        Ausschreibung
                                                                                                                                        ausgeschlossen
                                                                                                                                    </div>
                                                                                                                                    <div class="modal-switch-box">
                                                                                                                                        <input name="is_control"
                                                                                                                                               {{($pr_module_task->is_control ==1)?'checked':''}}
                                                                                                                                               class="switchCheckBox is_control"
                                                                                                                                               id="is_control{{$pr_module_task->id_ms_task}}"
                                                                                                                                               type="checkbox"
                                                                                                                                               data-size="mini">
                                                                                                                                        Kontrolfrage
                                                                                                                                    </div>
                                                                                                                                </div>


                                                                                                                            </div>
                                                                                                                            <div class="modal-footer">
                                                                                                                                <button type="button"
                                                                                                                                        class="btn ls-light-green-btn">
                                                                                                                                    {{--data-dismiss="modal">--}}
                                                                                                                                    Save
                                                                                                                                    and
                                                                                                                                    Close
                                                                                                                                </button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="col-xs-1">
                                                                                                <div class="add_new_task">
                                                                                                    <a href="javascript:void(0)"
                                                                                                       class="btn 
                                                                                               ls-light-blue-btn"
                                                                                                       data-toggle="modal"
                                                                                                       data-target="#addNewTask{{$module_3->id_module_structure}}"
                                                                                                            >
                                                                                                        <i class="fa fa-plus"></i>
                                                                                                    </a>
                                                                                                </div>

                                                                                                <div class="modal fade"
                                                                                                     id="addNewTask{{$module_3->id_module_structure}}"
                                                                                                     tabindex="-1"
                                                                                                     role="dialog"
                                                                                                     aria-labelledby="myModalLabel"
                                                                                                     aria-hidden="true"
                                                                                                     data-keyboard="false"
                                                                                                     data-backdrop="static"
                                                                                                     style="display: none;">
                                                                                                    <div class="modal-dialog">
                                                                                                        <div class="modal-content"
                                                                                                             id="borderInnerTask">
                                                                                                            <button type="button"
                                                                                                                    class="close close_modal"
                                                                                                                    id="close_modal_new{{$module_3->id_module_structure}}"
                                                                                                                    data-dismiss="modal"
                                                                                                                    aria-hidden="true">
                                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                                x
                                                                                                            </button>
                                                                                                            {{ Form::open
                                                                                                            (['action'=>'ProjectsController@add_new_task','id'=>''])}}

                                                                                                            <div class="modal-header task-label-white">
                                                                                                                <div class="ls-button-group float-left">
                                                                                                                    <div class="colorInnerTask"
                                                                                                                         id="colorInnerTask"></div>
                                                                                                                </div>
                                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                                    {{Form::checkbox('checkbox', 1, true,['class'=>"icheck-green",    'id'=>'icheck-green',  'disabled'])}}
                                                                                                                </div>
                                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                                    {{Form::select('set_priority',  $set_priority,  '',
                                                                                                                      ['class'=>'set_priority','id'=>'set_priority'])}}

                                                                                                                </div>


                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                            <div class="modal-body new-task">


                                                                                                                <h4>
                                                                                                                    <input type="text"
                                                                                                                           class="form-control"
                                                                                                                           placeholder="Task Name"
                                                                                                                           name="name_task"
                                                                                                                           required>
                                                                                                                </h4>

                                                                                    

                                                                                <textarea style="width: 100%" name="task_description"
                                                                                          class="summernote_new"
                                                                                          id="summernote_new" required>
                                                                                </textarea>

                                                                                                                <div class="clear"></div>


                                                                                                                <div class="modal-switch-area">
                                                                                                                    <div
                                                                                                                            class="modal-switch-box">
                                                                                                                        <input name="is_not_offer"
                                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                                               value="1"
                                                                                                                               id="is_not_offer"
                                                                                                                               type="checkbox"
                                                                                                                               data-size="mini">
                                                                                                                        Von
                                                                                                                        der
                                                                                                                        Ausschreibung
                                                                                                                        ausgeschlossen
                                                                                                                    </div>
                                                                                                                    <div class="modal-switch-box">
                                                                                                                        <input name="is_control"
                                                                                                                               value="1"
                                                                                                                               class="switchCheckBox is_control"
                                                                                                                               id="is_control"
                                                                                                                               type="checkbox"
                                                                                                                               data-size="mini">
                                                                                                                        Kontrolfrage
                                                                                                                    </div>
                                                                                                                </div>


                                                                                                            </div>
                                                                                                            <div class="modal-footer">
                                                                                                                <button type="submit"
                                                                                                                        class="btn ls-light-green-btn">
                                                                                                                    Save
                                                                                                                    and
                                                                                                                    Close
                                                                                                                </button>
                                                                                                                {{Form::hidden('fk_project',
                                                                                                                $project->id_project)}}
                                                                                                                {{Form::hidden('fk_module_structure',
                                                                                                                $module_3->id_module_structure)}}
                                                                                                            </div>

                                                                                                            {{Form::close()}}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </li>
                                                        @else
                                                            <li class="{{($module_2->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                                     <span class="module-menu-title menu_title"
                                                                           id="submodule{{$module_2->id_module_structure}}">
                                                                        {{$module_2->id_name}}</span>

                                                                <div class="row margin_left_minus2">
                                                                    <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module_2->id_module_structure, $project->id_project); ?>
                                                                    <div class="col-xs-2 ">


                                                                        <div
                                                                                class="circle_tasks_left"
                                                                                id="circle_tasks_left{{$module_2->id_module_structure}}">
                                                                            {{Ms_task::getCheckedTasks
                                                                            ($module_2->id_module_structure, 
                                                                            $project->id_project)}}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-10 inside-with-border">
                                                                        <div class="row">
                                                                            <div class="col-xs-11">
                                                                                @if(count($pr_module_tasks)>0)
                                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                                        <?php $attachments =
                                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                                        @if($pr_module_task->task_color  != null &&
                                                                                            $pr_module_task->task_color != 'transparent')
                                                                                            <style type="text/css">
                                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                    padding-right: 7px;
                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 {
                                                                                                    background: {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                                }

                                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                                    right: -30px;
                                                                                                }
                                                                                            </style>
                                                                                        @endif
                                                                                        <div class="panel-task-border"
                                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                                            <div class="panel-task {{
                                                                                    ($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                                <div class="panel-task-header">
                                                                                                    <div class="ls-button-group">
                                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                                             class="colorPicker">
                                                                                                            <a class="color"
                                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                                <div class="colorInner"
                                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                                            </a>

                                                                                                            <div class="track"
                                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>

                                                                                                            <input type="hidden"
                                                                                                                   class="colorInput"
                                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="ls-button-group">
                                                                                                        <a href="javascript:void(0);"
                                                                                                           data-toggle="modal"
                                                                                                           data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                           title=""> <i
                                                                                                                    class="fa fa-info-circle
                                                                           "></i></a>
                                                                                                    </div>
                                                                                                    <div class="ls-button-group">
                                                                                                        <i class="fa fa-paperclip"></i>
                                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                                        @if($pr_module_task->is_default == 0)

                                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                                            {{ Form::hidden('fk_module_structure', $module_2->id_module_structure) }}
                                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                                            <button type="submit"
                                                                                                                    style="color: #FF7878;"
                                                                                                                    class="btn-icon">
                                                                                                                <i class="fa fa-trash"></i>
                                                                                                            </button>
                                                                                                            {{ Form::close() }}

                                                                                                        @endif
                                                                                                    </div>
                                                                                                    <div class="ls-button-group float-right">
                                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                        false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="panel-task-body">
                                                                                                    {{ substr
                                                                                                    ($pr_module_task->name_task,0,250)}}

                                                                                                </div>

                                                                                                <div class="clear"></div>
                                                                                                {{--// modal fade level 2--}}
                                                                                                <div class="modal fade"
                                                                                                     id="modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                                     tabindex="-1"
                                                                                                     role="dialog"
                                                                                                     aria-labelledby="myModalLabel"
                                                                                                     aria-hidden="true"
                                                                                                     data-keyboard="false"
                                                                                                     data-backdrop="static"
                                                                                                     style="display: none;">
                                                                                                    <div class="modal-dialog">
                                                                                                        <div class="modal-content"
                                                                                                             id="borderInnerTask{{$pr_module_task->id_ms_task}}">
                                                                                                            <button type="button"
                                                                                                                    class="close close_modal"
                                                                                                                    id="close_modal{{$pr_module_task->id_ms_task}}"
                                                                                                                    data-dismiss="modal"
                                                                                                                    aria-hidden="true">
                                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                                x
                                                                                                            </button>
                                                                                                            <div class="modal-header task-label-white">
                                                                                                                <div class="ls-button-group float-left">
                                                                                                                    <div class="colorInnerTask"
                                                                                                                         id="colorInnerTask{{$pr_module_task->id_ms_task}}"></div>
                                                                                                                </div>
                                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,
                                                                                                                    false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                                    ($pr_module_task->is_checked)?'disabled':'',
                                                                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                                </div>
                                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                                    {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
                                                                                                                      ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}

                                                                                                                </div>


                                                                                                                <div class="clear"></div>
                                                                                                            </div>
                                                                                                            <div class="modal-body">

                                                                                                                {{Form::open()}}
                                                                                                                <h4>{{$pr_module_task->name_task}}
                                                                                                                </h4>

                                                                                                                <div class="modal_description_task">
                                                                                                                    {{$pr_module_task->description_task}}
                                                                                                                </div>
                                                                                                                
                                                                                                                <textarea name="task_description" id="summernote{{$pr_module_task->id_ms_task}}" style="width: 100%; height: 100px;">
	 {{$pr_module_task->task_description}}
</textarea>


                                                                                                                <div class="modal_description_task user_comment_task"
                                                                                                                     id="user_comment_task{{$pr_module_task->id_ms_task}}">
                                                                                                                    @if(strlen($pr_module_task->task_description)>0)
                                                                                                                        <div
                                                                                                                                class="modal_description_task_content">
                                                                                                                            {{$pr_module_task->task_description}}
                                                                                                                        </div>
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                                <div
                                                                                                                        class="modal_editor_buttons">

                                                                                                                    <a href="javascript:void(0)"
                                                                                                                       id="button_save_task{{$pr_module_task->id_ms_task}}"
                                                                                                                       class="btn ls-light-blue-btn button_save_task btn-margin-right"
                                                                                                                       style="display: none">
                                                                                                                        <i class="fa fa-floppy-o"></i>
                                                                                                                    </a>
                                                                                                                    <a href="javascript:void(0)"
                                                                                                                       id="button_add_task{{$pr_module_task->id_ms_task}}"
                                                                                                                       class="btn ls-light-blue-btn button_add_task btn-margin-right 
                                                                                                               {{(strlen($pr_module_task->task_description)>0)?'disabled':''}}">
                                                                                                                        <i class="fa fa-plus"></i>
                                                                                                                    </a>
                                                                                                                    <a href="javascript:void(0)"
                                                                                                                       id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                                                                                                       class="btn ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                        <i
                                                                                                                                class="fa fa-edit"></i>
                                                                                                                    </a>
                                                                                                                    <a href="javascript:void(0)"
                                                                                                                       id="button_delete_task{{$pr_module_task->id_ms_task}}"
                                                                                                                       class="btn ls-red-btn button_delete_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                                        <i
                                                                                                                                class="fa fa-remove"></i>
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                                {{Form::close()}}

                                                                                                                <div class="clear"></div>


                                                                                                                {{ Form::open(['action'=>'ProjectsController@upload_file',
                                                                                                                 'class'=>'dropzone dz-clickable',
                                                                                                                 'id'=>'dropzoneForm'.$pr_module_task->id_ms_task,
                                                                                                                 'enctype'=>"multipart/form-data"]) }}
                                                                                                                {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                                                                                                {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                                                                                                                <div class="dz-message">
                                                                                                                    <h4>
                                                                                                                        Drag
                                                                                                                        Files
                                                                                                                        to
                                                                                                                        Upload
                                                                                                                    </h4>
                                                                                                                    <span>Or click to browse</span>

                                                                                                                </div>
                                                                                                                {{Form::close()}}

                                                                                                                @if(count($attachments)>0)
                                                                                                                    <div class="modal-attachments">
                                                                                                                        <div
                                                                                                                                class="row">
                                                                                                                            @foreach($attachments as $attachment)
                                                                                                                                <?php    $icon =
                                                                                                                                        Projects_attach::getIconByAttachment($attachment->extension) ?>
                                                                                                                                <div class="col-lg-2 col-attachments"
                                                                                                                                     id="col-attachments{{$attachment->id_projects_attach}}">
                                                                                                                                    <div class="file_attachments"
                                                                                                                                         style="background: {{$icon}}"
                                                                                                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                                                                                                         title="{{$attachment->name_attachment }}">
                                                                                                                                    </div>
                                                                                                                                    <div class="attachments_edit_buttons"
                                                                                                                                         id="attachments_edit_buttons{{$attachment->id_projects_attach}}">

                                                                                                                                        <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                                                                                                           download
                                                                                                                                           class="button_download_file"
                                                                                                                                           target="_blank">
                                                                                                                                            <i class="fa fa-download"></i>
                                                                                                                                        </a><br>
                                                                                                                                        <a href="javascript:void(0)"
                                                                                                                                           id="button_delete_file{{$attachment->id_projects_attach}}"
                                                                                                                                           class="button_delete_file">
                                                                                                                                            <i class="fa fa-trash-o"></i>
                                                                                                                                        </a>
                                                                                                                                    </div>

                                                                                                                                </div>
                                                                                                                            @endforeach
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                @endif
                                                                                                                <div class="modal-switch-area">
                                                                                                                    <div
                                                                                                                            class="modal-switch-box">
                                                                                                                        <input name="is_not_offer"
                                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                                               {{($pr_module_task->is_not_offer ==1)?'checked':''}}
                                                                                                                               id="is_not_offer{{$pr_module_task->id_ms_task}}"
                                                                                                                               type="checkbox"
                                                                                                                               data-size="mini">
                                                                                                                        Von
                                                                                                                        der
                                                                                                                        Ausschreibung
                                                                                                                        ausgeschlossen
                                                                                                                    </div>
                                                                                                                    <div class="modal-switch-box">
                                                                                                                        <input name="is_control"
                                                                                                                               {{($pr_module_task->is_control ==1)?'checked':''}}
                                                                                                                               class="switchCheckBox is_control"
                                                                                                                               id="is_control{{$pr_module_task->id_ms_task}}"
                                                                                                                               type="checkbox"
                                                                                                                               data-size="mini">
                                                                                                                        Kontrolfrage
                                                                                                                    </div>
                                                                                                                </div>


                                                                                                            </div>
                                                                                                            <div class="modal-footer">
                                                                                                                <button type="button"
                                                                                                                        class="btn ls-light-blue-btn">
                                                                                                                    {{--data-dismiss="modal">--}}
                                                                                                                    Save
                                                                                                                    and
                                                                                                                    Close
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                @endif
                                                                            </div>

                                                                            <div class="col-xs-1">
                                                                                <div class="add_new_task">
                                                                                    <a href="javascript:void(0)"
                                                                                       class="btn ls-light-blue-btn"
                                                                                       data-toggle="modal"
                                                                                       data-target="#addNewTask{{$module_2->id_module_structure}}"
                                                                                            >
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="modal fade"
                                                                                     id="addNewTask{{$module_2->id_module_structure}}"
                                                                                     tabindex="-1"
                                                                                     role="dialog"
                                                                                     aria-labelledby="myModalLabel"
                                                                                     aria-hidden="true"
                                                                                     data-keyboard="false"
                                                                                     data-backdrop="static"
                                                                                     style="display: none;">
                                                                                    <div class="modal-dialog">
                                                                                        <div class="modal-content"
                                                                                             id="borderInnerTask">
                                                                                            <button type="button"
                                                                                                    class="close close_modal"
                                                                                                    id="close_modal_new{{$module_2->id_module_structure}}"
                                                                                                    data-dismiss="modal"
                                                                                                    aria-hidden="true">
                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                x
                                                                                            </button>
                                                                                            {{ Form::open
                                                                                            (['action'=>'ProjectsController@add_new_task','id'=>''])}}

                                                                                            <div class="modal-header task-label-white">
                                                                                                <div class="ls-button-group float-left">
                                                                                                    <div class="colorInnerTask"
                                                                                                         id="colorInnerTask"></div>
                                                                                                </div>
                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                    {{Form::checkbox('checkbox', 1,    true,['class'=>"icheck-green",    'id'=>'icheck-green',   'disabled'])}}
                                                                                                </div>
                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                    {{Form::select('set_priority',  $set_priority,  '',
                                                                                                      ['class'=>'set_priority','id'=>'set_priority'])}}

                                                                                                </div>


                                                                                                <div class="clear"></div>
                                                                                            </div>
                                                                                            <div class="modal-body new-task">


                                                                                                <h4><input type="text"
                                                                                                           class="form-control"
                                                                                                           placeholder="Task Name"
                                                                                                           name="name_task"
                                                                                                           required>
                                                                                                </h4>

                                                                                    

                                                                                <textarea style="width: 100%" name="task_description"
                                                                                          class="summernote_new"
                                                                                          id="summernote_new" required>
                                                                                </textarea>

                                                                                                <div class="clear"></div>


                                                                                                <div class="modal-switch-area">
                                                                                                    <div
                                                                                                            class="modal-switch-box">
                                                                                                        <input name="is_not_offer"
                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                               value="1"
                                                                                                               id="is_not_offer"
                                                                                                               type="checkbox"
                                                                                                               data-size="mini">
                                                                                                        Von
                                                                                                        der
                                                                                                        Ausschreibung
                                                                                                        ausgeschlossen
                                                                                                    </div>
                                                                                                    <div class="modal-switch-box">
                                                                                                        <input name="is_control"
                                                                                                               value="1"
                                                                                                               class="switchCheckBox is_control"
                                                                                                               id="is_control"
                                                                                                               type="checkbox"
                                                                                                               data-size="mini">
                                                                                                        Kontrolfrage
                                                                                                    </div>
                                                                                                </div>


                                                                                            </div>
                                                                                            <div class="modal-footer">
                                                                                                <button type="submit"
                                                                                                        class="btn ls-light-blue-btn"
                                                                                                        data-dismiss="modal">
                                                                                                    Save and Close
                                                                                                </button>
                                                                                                {{Form::hidden('fk_project',
                                                                                                $project->id_project)}}
                                                                                                {{Form::hidden('fk_module_structure',
                                                                                                $module_2->id_module_structure)}}
                                                                                            </div>

                                                                                            {{Form::close()}}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                            <li class="{{($module->is_default == 0)?'li_editable not_selectable':'not_selectable'}}">
                                                  <span class="module-menu-title menu_title"
                                                        id="submodule{{$module->id_module_structure}}">
                                                                        {{$module->id_name}}</span>

                                                <div class="row margin_left_minus1">
                                                    <?php      $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $project->id_project); ?>
                                                    <div class="col-xs-2 ">
                                                        <div
                                                                class="circle_tasks_left"
                                                                id="circle_tasks_left{{$module->id_module_structure}}">
                                                            {{Ms_task::getCheckedTasks($module->id_module_structure, $project->id_project)}}
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-10 inside-with-border">
                                                        <div class="row">
                                                            <div class="col-xs-11">
                                                                @if(count($pr_module_tasks)>0)
                                                                    @foreach($pr_module_tasks as $pr_module_task)
                                                                        <?php $attachments =
                                                                                Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->id_project) ?>

                                                                        @if($pr_module_task->task_color  != null &&
                                                                            $pr_module_task->task_color != 'transparent')
                                                                            <style type="text/css">
                                                                                #panel-task-border{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                    padding-right: 7px;
                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #colorInner{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                    background-color: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #colorInnerTask{{$pr_module_task->id_ms_task}}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 {
                                                                                    background: {{$pr_module_task->task_color}};
                                                                                }

                                                                                #borderInnerTask{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                    border-right: 7px solid {{$pr_module_task->task_color}};
                                                                                }

                                                                                #close_modal{{$pr_module_task->id_ms_task}}                                                                                                                                                                                                                                                                                                                                                                                                                                {
                                                                                    right: -30px;
                                                                                }
                                                                            </style>
                                                                        @endif
                                                                        <div class="panel-task-border"
                                                                             id="panel-task-border{{$pr_module_task->id_ms_task}}">
                                                                            <div class="panel-task {{($pr_module_task->is_checked)?'panel_background_grey':''}}
                                                                            {{ ($pr_module_task->is_default == 0)  ?'panel_background_blue':''}}">
                                                                                <div class="panel-task-header">
                                                                                    <div class="ls-button-group">
                                                                                        <div id="colorPicker{{$pr_module_task->id_ms_task}}"
                                                                                             class="colorPicker">
                                                                                            <a class="color"
                                                                                               id="color{{$pr_module_task->id_ms_task}}">
                                                                                                <div class="colorInner"
                                                                                                     id="colorInner{{$pr_module_task->id_ms_task}}"></div>
                                                                                            </a>

                                                                                            <div class="track"
                                                                                                 id="track{{$pr_module_task->id_ms_task}}"></div>

                                                                                            <input type="hidden"
                                                                                                   class="colorInput"
                                                                                                   id="colorInput{{$pr_module_task->id_ms_task}}"/>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="ls-button-group">
                                                                                        <a href="javascript:void(0);"
                                                                                           data-toggle="modal"
                                                                                           data-target="#modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                           title=""> <i class="fa fa-info-circle
                                                                           "></i></a>

                                                                                    </div>
                                                                                    <div class="ls-button-group">
                                                                                        <i class="fa fa-paperclip"></i>
                                                                                        <span class="attachment_counter">{{count($attachments)}}</span>
                                                                                        @if($pr_module_task->is_default == 0)

                                                                                            {{ Form::open(['action'=>'ProjectsController@remove_new_task', 'class'=>'FormDeleteTask' ]) }}
                                                                                            {{ Form::hidden('fk_project', $project->id_project) }}
                                                                                            {{ Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) }}
                                                                                            {{ Form::hidden('fk_module_structure', $module->id_module_structure) }}
                                                                                            {{ Form::hidden('fk_task', $pr_module_task->fk_task) }}

                                                                                            <button type="submit"
                                                                                                    style="color: #FF7878;"
                                                                                                    class="btn-icon">
                                                                                                <i class="fa fa-trash"></i>
                                                                                            </button>
                                                                                            {{ Form::close() }}

                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="ls-button-group float-right">
                                                                                        {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1,  false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                        ($pr_module_task->is_checked)?'disabled':'',
                                                                                        ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel-task-body">
                                                                                    {{ substr($pr_module_task->name_task,0,250)}}
                                                                                </div>

                                                                                <div class="clear"></div>
                                                                                {{--// modal fade level 1--}}
                                                                                <div class="modal fade"
                                                                                     id="modalEditTask{{$pr_module_task->id_ms_task}}"
                                                                                     tabindex="-1" role="dialog"
                                                                                     aria-labelledby="myModalLabel"
                                                                                     aria-hidden="true"
                                                                                     data-keyboard="false"
                                                                                     data-backdrop="static"
                                                                                     style="display: none;">
                                                                                    <div class="modal-dialog">
                                                                                        <div class="modal-content"
                                                                                             id="borderInnerTask{{$pr_module_task->id_ms_task}}">
                                                                                            <button type="button"
                                                                                                    class="close close_modal"
                                                                                                    id="close_modal{{$pr_module_task->id_ms_task}}"
                                                                                                    data-dismiss="modal"
                                                                                                    aria-hidden="true">
                                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                                x
                                                                                            </button>
                                                                                            <div class="modal-header task-label-white">
                                                                                                <div class="ls-button-group float-left">
                                                                                                    <div class="colorInnerTask"
                                                                                                         id="colorInnerTask{{$pr_module_task->id_ms_task}}"></div>
                                                                                                </div>
                                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                                    {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1, false,['class'=>"icheck-green",    'id'=>'icheck-green'.$pr_module_task->id_ms_task,
                                                                                                    ($pr_module_task->is_checked)?'disabled':'',
                                                                                                    ($pr_module_task->is_checked || $pr_module_task->is_checked_user)?'checked':''])}}
                                                                                                </div>
                                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                                    {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
                                                                                                      ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}

                                                                                                </div>


                                                                                                <div class="clear"></div>
                                                                                            </div>
                                                                                            <div class="modal-body">

                                                                                                {{Form::open()}}
                                                                                                <h4>{{$pr_module_task->name_task}}
                                                                                                </h4>

                                                                                                <div class="modal_description_task">
                                                                                                    {{$pr_module_task->description_task}}
                                                                                                </div>

                                                                                <textarea style="width: 100%" class="summernote"
                                                                                          id="summernote{{$pr_module_task->id_ms_task}}">
                                                                                        {{$pr_module_task->task_description}}
                                                                                </textarea>


                                                                                                <div class="modal_description_task user_comment_task"
                                                                                                     id="user_comment_task{{$pr_module_task->id_ms_task}}">
                                                                                                    @if(strlen($pr_module_task->task_description)>0)
                                                                                                        <div
                                                                                                                class="modal_description_task_content">
                                                                                                            {{$pr_module_task->task_description}}
                                                                                                        </div>
                                                                                                    @endif
                                                                                                </div>
                                                                                                <div
                                                                                                        class="modal_editor_buttons">

                                                                                                    <a href="javascript:void(0)"
                                                                                                       id="button_save_task{{$pr_module_task->id_ms_task}}"
                                                                                                       class="btn ls-light-blue-btn button_save_task btn-margin-right"
                                                                                                       style="display: none">
                                                                                                        <i class="fa fa-floppy-o"></i>
                                                                                                    </a>
                                                                                                    <a href="javascript:void(0)"
                                                                                                       id="button_add_task{{$pr_module_task->id_ms_task}}"
                                                                                                       class="btn ls-light-blue-btn button_add_task btn-margin-right 
                                                                                                               {{(strlen($pr_module_task->task_description)>0)?'disabled':''}}">
                                                                                                        <i class="fa fa-plus"></i>
                                                                                                    </a>
                                                                                                    <a href="javascript:void(0)"
                                                                                                       id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                                                                                       class="btn ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                        <i
                                                                                                                class="fa fa-edit"></i>
                                                                                                    </a>
                                                                                                    <a href="javascript:void(0)"
                                                                                                       id="button_delete_task{{$pr_module_task->id_ms_task}}"
                                                                                                       class="btn ls-red-btn button_delete_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                                                                                        <i
                                                                                                                class="fa fa-remove"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                                {{Form::close()}}

                                                                                                <div class="clear"></div>


                                                                                                {{ Form::open(['action'=>'ProjectsController@upload_file',
                                                                                                 'class'=>'dropzone dz-clickable',
                                                                                                 'id'=>'dropzoneForm'.$pr_module_task->id_ms_task,
                                                                                                 'enctype'=>"multipart/form-data"]) }}
                                                                                                {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                                                                                {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                                                                                                <div class="dz-message">
                                                                                                    <h4>Drag
                                                                                                        Files to
                                                                                                        Upload
                                                                                                    </h4>
                                                                                                    <span>Or click to browse</span>

                                                                                                </div>
                                                                                                {{Form::close()}}

                                                                                                @if(count($attachments)>0)
                                                                                                    <div class="modal-attachments">
                                                                                                        <div
                                                                                                                class="row">
                                                                                                            @foreach($attachments as $attachment)
                                                                                                                <?php    $icon =
                                                                                                                        Projects_attach::getIconByAttachment($attachment->extension) ?>
                                                                                                                <div class="col-lg-2 col-attachments"
                                                                                                                     id="col-attachments{{$attachment->id_projects_attach}}">
                                                                                                                    <div class="file_attachments"
                                                                                                                         style="background: {{$icon}}"
                                                                                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                                                                                         title="{{$attachment->name_attachment }}">
                                                                                                                    </div>
                                                                                                                    <div class="attachments_edit_buttons"
                                                                                                                         id="attachments_edit_buttons{{$attachment->id_projects_attach}}">

                                                                                                                        <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                                                                                           download
                                                                                                                           class="button_download_file"
                                                                                                                           target="_blank">
                                                                                                                            <i class="fa fa-download"></i>
                                                                                                                        </a><br>
                                                                                                                        <a href="javascript:void(0)"
                                                                                                                           id="button_delete_file{{$attachment->id_projects_attach}}"
                                                                                                                           class="button_delete_file">
                                                                                                                            <i class="fa fa-trash-o"></i>
                                                                                                                        </a>
                                                                                                                    </div>

                                                                                                                </div>
                                                                                                            @endforeach
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endif
                                                                                                <div class="modal-switch-area">
                                                                                                    <div
                                                                                                            class="modal-switch-box">
                                                                                                        <input name="is_not_offer"
                                                                                                               class="switchCheckBox is_not_offer"
                                                                                                               {{($pr_module_task->is_not_offer ==1)?'checked':''}}
                                                                                                               id="is_not_offer{{$pr_module_task->id_ms_task}}"
                                                                                                               type="checkbox"
                                                                                                               data-size="mini">
                                                                                                        Von
                                                                                                        der
                                                                                                        Ausschreibung
                                                                                                        ausgeschlossen
                                                                                                    </div>
                                                                                                    <div class="modal-switch-box">
                                                                                                        <input name="is_control"
                                                                                                               {{($pr_module_task->is_control ==1)?'checked':''}}
                                                                                                               class="switchCheckBox is_control"
                                                                                                               id="is_control{{$pr_module_task->id_ms_task}}"
                                                                                                               type="checkbox"
                                                                                                               data-size="mini">
                                                                                                        Kontrolfrage
                                                                                                    </div>
                                                                                                </div>


                                                                                            </div>
                                                                                            <div class="modal-footer">
                                                                                                <button type="button"
                                                                                                        class="btn ls-light-blue-btn"
                                                                                                        data-dismiss="modal"
                                                                                                        >
                                                                                                    {{--data-dismiss="modal">--}}
                                                                                                    Save and
                                                                                                    Close
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>

                                                            <div class="col-xs-1">
                                                                <div class="add_new_task">
                                                                    <a href="javascript:void(0)"
                                                                       class="btn ls-light-blue-btn"
                                                                       data-toggle="modal"
                                                                       data-target="#addNewTask{{$module->id_module_structure}}"
                                                                            >
                                                                        <i class="fa fa-plus"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="modal fade"
                                                                     id="addNewTask{{$module->id_module_structure}}"
                                                                     tabindex="-1"
                                                                     role="dialog"
                                                                     aria-labelledby="myModalLabel"
                                                                     aria-hidden="true"
                                                                     data-keyboard="false"
                                                                     data-backdrop="static"
                                                                     style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content"
                                                                             id="borderInnerTask">
                                                                            <button type="button"
                                                                                    class="close close_modal"
                                                                                    id="close_modal_new{{$module->id_module_structure}}"
                                                                                    data-dismiss="modal"
                                                                                    aria-hidden="true">
                                                                                {{--<i class="fa fa-times"></i>--}}
                                                                                x
                                                                            </button>
                                                                            {{ Form::open
                                                                            (['action'=>'ProjectsController@add_new_task','id'=>''])}}

                                                                            <div class="modal-header task-label-white">
                                                                                <div class="ls-button-group float-left">
                                                                                    <div class="colorInnerTask"
                                                                                         id="colorInnerTask"></div>
                                                                                </div>
                                                                                <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                                                                    {{Form::checkbox('checkbox', 1,true,['class'=>"icheck-green",    'id'=>'icheck-green',
                                                                                   'disabled'])}}
                                                                                </div>
                                                                                <div class="ls-button-group
                                                                                float-right">

                                                                                    {{Form::select('set_priority',  $set_priority,  '',
                                                                                      ['class'=>'set_priority','id'=>'set_priority'])}}

                                                                                </div>


                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="modal-body new-task">
                                                                                <h4><input type="text"
                                                                                           class="form-control"
                                                                                           placeholder="Task Name"
                                                                                           name="name_task"
                                                                                           required>
                                                                                </h4>
                                                                           <textarea style="width: 100%" name="task_description"
                                                                                     class="summernote_new"
                                                                                     id="summernote_new"
                                                                                     required>
                                                                                </textarea>

                                                                                <div class="clear"></div>


                                                                                <div class="modal-switch-area">
                                                                                    <div
                                                                                            class="modal-switch-box">
                                                                                        <input name="is_not_offer"
                                                                                               class="switchCheckBox is_not_offer"
                                                                                               value="1"
                                                                                               id="is_not_offer"
                                                                                               type="checkbox"
                                                                                               data-size="mini">
                                                                                        Von
                                                                                        der
                                                                                        Ausschreibung
                                                                                        ausgeschlossen
                                                                                    </div>
                                                                                    <div class="modal-switch-box">
                                                                                        <input name="is_control"
                                                                                               value="1"
                                                                                               class="switchCheckBox is_control"
                                                                                               id="is_control"
                                                                                               type="checkbox"
                                                                                               data-size="mini">
                                                                                        Kontrolfrage
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn ls-light-blue-btn"
                                                                                        data-dismiss="modal">
                                                                                    Save and Close
                                                                                </button>
                                                                                {{Form::hidden('fk_project',
                                                                                $project->id_project)}}
                                                                                {{Form::hidden('fk_module_structure',
                                                                                $module->id_module_structure)}}
                                                                            </div>

                                                                            {{Form::close()}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </li>
                        </ul>

                    </div>
                    <div class="modal fade"
                         id="addNewTask"
                         tabindex="-1"
                         role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true"
                         data-keyboard="false"
                         data-backdrop="static"
                         style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content"
                                 id="borderInnerTask">
                                <button type="button"
                                        class="close close_modal"
                                        id="close_modal_new"
                                        data-dismiss="modal"
                                        aria-hidden="true">
                                    {{--<i class="fa fa-times"></i>--}}
                                    x
                                </button>
                                {{ Form::open
                                (['action'=>'ProjectsController@add_new_task','id'=>''])}}

                                <div class="modal-header task-label-white">
                                    <div class="ls-button-group float-left">
                                        <div class="colorInnerTask"
                                             id="colorInnerTask"></div>
                                    </div>
                                    <div class="ls-button-group
                                                                                float-right" style="padding: 3px;">
                                        {{Form::checkbox('checkbox', 1,  true,['class'=>"icheck-green",    'id'=>'icheck-green', 'disabled'])}}
                                    </div>
                                    <div class="ls-button-group
                                                                                float-right">

                                        {{Form::select('set_priority',  $set_priority,  '',
                                          ['class'=>'set_priority','id'=>'set_priority'])}}

                                    </div>


                                    <div class="clear"></div>
                                </div>
                                <div class="modal-body new-task">


                                    <h4>
                                        <input type="text"
                                               class="form-control"
                                               placeholder="Task Name"
                                               name="name_task"
                                               required>
                                    </h4>
<textarea style="width: 100%" name="task_description"
          class="summernote_new"
          id="summernote_new" required>
                                                                                </textarea>

                                    <div class="clear"></div>


                                    <div class="modal-switch-area">
                                        <div
                                                class="modal-switch-box">
                                            <input name="is_not_offer"
                                                   class="switchCheckBox is_not_offer"
                                                   value="1"
                                                   id="is_not_offer"
                                                   type="checkbox"
                                                   data-size="mini">
                                            Von
                                            der
                                            Ausschreibung
                                            ausgeschlossen
                                        </div>
                                        <div class="modal-switch-box">
                                            <input name="is_control"
                                                   value="1"
                                                   class="switchCheckBox is_control"
                                                   id="is_control"
                                                   type="checkbox"
                                                   data-size="mini">
                                            Kontrolfrage
                                        </div>
                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="submit"
                                            class="btn ls-light-green-btn" data-dismiss="modal">
                                        Save and
                                        Close
                                    </button>
                                    {{Form::hidden('fk_project',
                                    $project->id_project)}}

                                    {{Form::hidden('structure','',['id'=>'easyTreeStructure'])}}

                                </div>

                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


 
 
 