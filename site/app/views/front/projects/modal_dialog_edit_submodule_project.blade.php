{{ Form::model($pr_module_structure,['route'=>['submodule.update',$submodule->id_submodule], 'method'=>'PATCH'])}}
<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-md-12 mgtp-20">
        <div class="title_line">  {{ $submodule->name_submodule  }} </div>
    </div>
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="form-group">
                            {{ Form::text('name_submodule',  $submodule->getAttribute('name_submodule','de'), [
                                'class'=>"form-control",
                                "placeholder"=> trans('messages.nav_submodule').' ' .trans('messages.l_name'),
                                "required",
                                'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                "oninput"=>"this.setCustomValidity('')"
                              ]) }}
                        </div>
                        <div class="form-group">
                            <textarea name="description_submodule" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $submodule->getAttribute('description_submodule','de')}}</textarea>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$submodule->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$submodule->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        {{Form::hidden('id_module_structure' ,$id_module_structure)}}
        <button type="submit" class="btn ls-light-blue-btn btn-block">
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
        {{--<a href="{{URL::to('delete_task_from_structure/'.$row->id_ms_task)}}" class="btn btn-block btn-danger">--}}
        {{--{{trans('messages.nav_submodule')}} {{trans('messages.act_delete')}}--}}
        {{--</a>--}}

    </div>
</div>


{{Form::close()}}

<script type="text/javascript">
    $(".hide_controller").parent().hide();
    $(".hide_controller").parent().fadeIn(500);
    $(document).on('click', '.hide_controller', function() {
        $(".hide_controller").parent().hide();
    });
</script>
