{{ Form::model($pr_module_structure,['route'=>['submodule.update',$submodule->id_submodule], 'method'=>'PATCH'])}}


<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-md-12 text-right ">
        <div class="row">
            <div class="col-md-2 pull-right pd-5" style="margin-top: 4px; width: 70px">
                {{Form::checkbox('is_default' ,1, $pr_module_task->is_default,['class'=>'switchCheckBox','data-size'=>"mini", 'style'=>'display:inline-block'])}}
            </div>
            <div class="col-md-2 pull-right pd-5">
                <i class="fa fa-info-circle" title="{{trans('messages.label_default_module_standard')}}"></i>
            </div>
        </div>
    </div>
    <div class="col-xs-12 mgtp-10">
        <div class="title_line"> {{$submodule->name_submodule}}</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_submodule',  $submodule->getAttribute('name_submodule','de'), [
                                        'class'=>"form-control",
                                        "placeholder"=> trans('messages.nav_submodule').' ' .trans('messages.l_name'),
                                        "required",
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_submodule" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $submodule->getAttribute('description_submodule','de')}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_submodule_en', $submodule->getAttribute('name_submodule','en'), [
                                      'class'=>"form-control",
                                      "placeholder"=> trans('messages.nav_submodule').' ' .trans('messages.l_name'),
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_submodule_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $submodule->getAttribute('description_submodule','en')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  mgtp-10">
                            {{ Form::text('code_submodule',  $submodule->code_submodule, ['class'=>"form-control","placeholder"=> trans('messages.nav_submodule').' ' .trans('messages.l_code')]) }}
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span>
                                        </a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="check-all">
                          <button type="button" class="check-all-module" title="Check all" data-message="{{trans('messages.check_all_industries_submodule')}}"
                                  data-submodule-id="{{$id_module_structure}}" data-url="{{action('SubmoduleController@add_all_to_industries')}}" data-type="submodule">
                              <i class="fa fa-check" aria-hidden="true" ></i>
                          </button>
                          <button type="button" class="check-all-module-inactive" title="Check all" style="Display: none;">
                              <i class="fa fa-check" aria-hidden="true" ></i>
                          </button>
                          <button type="button" class="uncheck-all-module" title="Uncheck all" data-message="{{trans('messages.uncheck_all_industries_submodule')}}"
                                  data-submodule-id="{{$id_module_structure}}" data-url="{{action('SubmoduleController@remove_all_from_industries')}}" data-type="submodule">
                              <i class="fa fa-remove"></i>
                          </button>
                          <button type="button" class="uncheck-all-module-inactive" title="Uncheck all" style="Display: none;">
                              <i class="fa fa-remove"></i>
                          </button>
                        </div>
                        <div class="tab-content tab-border module-container">
                            @foreach($industries_types as $industry_type)
                                <?php
                                    $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>
                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}" id="ind{{$industry_type->id_industry_type}}">
                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <hr>
                                    <div class="check-all-type-module">
                                        @if (count($industries)>0)
                                            <button type="button" class="check-all-module-type" title="Check all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.check_all_categ_industries_submodule')}}" data-url="{{action('SubmoduleController@add_all_to_industries')}}"
                                                    data-submodule-id="{{$id_module_structure}}" data-type="submodule">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="check-all-module-type-inactive" title="Check all" style="Display: none;">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="uncheck-all-module-type" title="Uncheck all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.uncheck_all_categ_industries_submodule')}}" data-url="{{action('SubmoduleController@remove_all_from_industries')}}"
                                                    data-submodule-id="{{$id_module_structure}}" data-type="submodule">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            <button type="button" class="uncheck-all-module-type-inactive" title="Uncheck all" style="Display: none;">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @elseif(count($industries) == 0)
                                            <button type="button" class="inactive-btn" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button type="button" class="inactive-btn" title="Check all">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @endif()
                                    </div>
                                    @foreach($industries as $industry)
                                        <div class="industry-container" style="display: block">
                                            <div class="float-left">
                                                <input type="checkbox" disabled class='icheck-green'
                                                   {{(Industry_ms::isIndustryChildChecked($industry->id_industry, $id_module_structure) == true)?'checked':''}}
                                                   name="fk_industry[]" value="{{$industry->id_industry}}" style="display: inline-block">
                                                {{$industry->name_industry}}
                                            </div>
                                            <div class="float-right">
                                                @if(Industry_ms::isIndustryChildChecked($industry->id_industry, $id_module_structure) == true)
                                                    <a href="javascript:void(0)" id="remove_all_industry_{{$id_module_structure.'_'.$industry->id_industry}}"
                                                        class=" remove_all_industry btn btn-danger  btn-xs">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" id="remove_all_industry_{{$id_module_structure.'_'.$industry->id_industry}}"
                                                    class=" remove_all_industry btn btn-danger  btn-xs" disabled="disabled">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            @endif
                                            </div>
                                            <div class="float-right">
                                            @if(Industry_ms::isIndustryChildChecked($industry->id_industry, $id_module_structure) == false)
                                                <a href="javascript:void(0)" id="{{'add_all_industry_'.$id_module_structure.'_'.$industry->id_industry}}"
                                                    class=" add_all_industry btn ls-light-blue-btn  btn-xs">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" id="{{'add_all_industry_'.$id_module_structure.'_'.$industry->id_industry}}"
                                                    class=" add_all_industry btn ls-light-blue-btn  btn-xs" disabled="disabled"  style="width: 20px;">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            @endif
                                            </div>
                                            <div class="float-right mgr-5">
                                                <i class="fa fa-info-circle" style="color: #324E59; font-size: 18px; margin-top: 2px " title="{{trans('messages.label_add_all_industry')}}"></i>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$submodule->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$submodule->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        {{Form::hidden('id_module_structure' ,$id_module_structure)}}
        <button type="submit" class="btn ls-light-blue-btn btn-block">
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
        {{--<a href="{{URL::to('delete_task_from_structure/'.$row->id_ms_task)}}" class="btn btn-block btn-danger">--}}
        {{--{{trans('messages.nav_submodule')}} {{trans('messages.act_delete')}}--}}
        {{--</a>--}}
    </div>
</div>
<br>
<br>

{{Form::close()}}

<script>
    $(document).ready(function() {
        var total_industries_containers = $('.article_content').find('.industry-container').length;
        var total_industries_checked = 0;
        $('.article_content').find('.industry-container').each(function() {
            if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                total_industries_checked++;
            }
        });

        $('.article_content').find('.check-all-module').show();
        $('.article_content').find('.check-all-module-inactive').hide();
        $('.article_content').find('.uncheck-all-module').show();
        $('.article_content').find('.uncheck-all-module-inactive').hide();

        if(total_industries_containers == total_industries_checked) {
            $('.article_content').find('.check-all-module').hide();
            $('.article_content').find('.check-all-module-inactive').show();
            $('.article_content').find('.uncheck-all-module').show();
            $('.article_content').find('.uncheck-all-module-inactive').hide();
        }

        if(total_industries_checked == 0) {
            $('.article_content').find('.check-all-module').show();
            $('.article_content').find('.check-all-module-inactive').hide();
            $('.article_content').find('.uncheck-all-module').hide();
            $('.article_content').find('.uncheck-all-module-inactive').show();
        }

        $('.module-container').find('.tab-pane').each(function() {
            var this_category = $(this);
            var industries_containers = this_category.find('.industry-container').length;
            var industries_checked = 0;

            this_category.find('.industry-container').each(function() {
                if($(this).find('.add_all_industry').attr('disabled') == "disabled" ){
                    industries_checked++;
                }
            });

            this_category.find('.check-all-module-type').show();
            this_category.find('.check-all-module-type-inactive').hide();
            this_category.find('.uncheck-all-module-type').show();
            this_category.find('.uncheck-all-module-type-inactive').hide();
            
            if(industries_checked == 0) {
                this_category.find('.check-all-module-type').show();
                this_category.find('.check-all-module-type-inactive').hide();
                this_category.find('.uncheck-all-module-type').hide();
                this_category.find('.uncheck-all-module-type-inactive').show();
            }

            if(industries_containers == industries_checked) {
                this_category.find('.check-all-module-type').hide();
                this_category.find('.check-all-module-type-inactive').show();
                this_category.find('.uncheck-all-module-type').show();
                this_category.find('.uncheck-all-module-type-inactive').hide();
            }
        });
    });
</script>
