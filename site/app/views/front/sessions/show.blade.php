@extends('front.layouts.login')
        <!-- uses the front/layouts/login template -->
@section('content')
    <div class="login-content">
        <div class="login-user-icon">
            LOGIN-BEREICH AUSWÄHLEN
        </div>
    </div>
    <div class="login-msg">
      Sie haben sich als Super-Admin angemeldet.
      <br>
      Wählen Sie bitte aus, wie Sie vorgehen möchten!
    </div>
    {{-- the search form --}}
    <div class="login-form">
        <div class="input-group ls-group-input login-btn-box">
            <a href="{{URL::to('/')}}" class="register-now back-to-login logo-color-2">APPLIKATION</a>
        </div>
        <div class="height10"></div>
        <div class="input-group ls-group-input login-btn-box">
            <a href="{{URL::to('admin_home')}}" class="register-now back-to-login logo-color-1">ADMINISTRATION</a>
        </div>
    </div>
@stop
