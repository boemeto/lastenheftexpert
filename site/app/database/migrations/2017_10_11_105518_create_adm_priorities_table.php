<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmPrioritiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_priorities', function ($table) {
            $table->increments('id_priority')->unsigned();
            $table->string('name_priority', 10);
            $table->string('desc_priority', 200);
            $table->string('priority_en', 200)->nullable();
            $table->string('color_priority', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_priorities');
    }
}