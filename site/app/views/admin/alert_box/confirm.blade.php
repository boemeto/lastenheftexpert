<div class="modal fade col-md-6 col-md-offset-3 text-center" id="formConfirmOk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" >
      <div class="modal-header" style="background-color: #324E59; color: white;">
        <button type="button" class="close" data-dismiss="modal">
            <i class="fa fa-times"></i>
            <span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="frm_title">Check Required Fields</h4>
      </div>
      <div class="modal-body" id="frm_body" 
           style="
            margin: 20px;
            font-size: 16px;">
      </div>
      <div class="modal-footer text-center">
          <div class="row">
                 <div class="">
                      <button type="button" class="btn ls-light-blue-btn col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 text-center" data-dismiss="modal" id="frm_cancel" style='border-radius: 5px;'>Ok</button>
                 </div>  
         </div>
      </div>
    </div>
  </div>
</div>