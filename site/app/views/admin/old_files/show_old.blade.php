@extends('admin.layouts.default')
@section('dropdown-menu')
@include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
@include('admin.layouts.sidebar-menu')
@stop

@section('header_scripts')

@stop
@section('footer_scripts')

        <!-- Module menu tree style -->
<script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
@include('admin/software/tree_script')
<script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
{{--<script src="{{URL::asset('assets/js/drag_order/drag_order.js')}}"></script>--}}

<!-- Module menu tree style -->
<script type="text/javascript">

    jQuery(document).ready(function () {
        $('.loader_back').fadeOut(1000);


        jQuery('.close_tasks').click(function () {
            var li = $(this).parent().parent();

            if ($(this).hasClass('glyphicon-minus-sign')) {
                li.find('.margin_left_minus1').hide("fold");
                $(this).addClass('glyphicon-plus-sign')
                        .removeClass('glyphicon-minus-sign');

            } else {

                $(this).addClass('glyphicon-minus-sign')
                        .removeClass('glyphicon-plus-sign');
                li.find('.margin_left_minus1').show("fold");

            }

        });

        // show all functions closed
        function closeEasyTree() {
            var parent = $(".easy-tree").find('li').not('#project_name');
            var children = parent.find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                parent.children('span').find(
                        ' > span.glyphicon').addClass(
                        'glyphicon-plus-sign').removeClass(
                        'glyphicon-minus-sign');
            }
            $('.margin_left_minus1').hide();

        }

//        closeEasyTree();


        jQuery(".switchCheckBox").bootstrapSwitch();
        $('.summernote_new').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });


    });
    // nested filter
    (function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[":"].contains = jQuery.expr.createPseudo(function (arg) {
            return function (elem) {
                return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        // this function calls itself, to search in every level
        function hasChild(list) {
            // the searched value
            var filter = $('#filterinput').val();
            // for every children, do function:
            list.children('li').each(function () {
                // see if the list contains the filte
                if ($(this).find("span > a:contains(" + filter + ")").length > 0) {
                    // see if it has nested levels
                    if ($(this).find('ul :first').length > 0) {
                        $(this).show();
                        // recall function, searching the ul tag
                        hasChild($(this).find('ul :first').parent());
                    } else {
                        $(this).show();
                    }
                } else {
                    $(this).hide();
                }
            });
        }
        $('#filterinput').keyup(function () {
            // the id of the filtered list
            var list = '#list';
            // the searched value
            var filter = $('#filterinput').val();
            if (filter) {
                // call previous function
                hasChild($(list));
            } else {
                // show all
                $(list).find("li").show();
            }
            return false;
        });
    }(jQuery));

    $("#modalEditTask").on('hidden.bs.modal', function () {
        $("#modalEditTask").removeData('bs.modal');
    });
    $("#modalAddTask").on('hidden.bs.modal', function () {
        $("#modalAddTask").removeData('bs.modal');
    });
    $("#modalAddTask").on('shown.bs.modal', function () {
        jQuery(".switchCheckBox").bootstrapSwitch();
        jQuery('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    });

    $("#modalEditTask").on('shown.bs.modal', function () {

        jQuery(".switchCheckBox").bootstrapSwitch();
        $('.select_module').on('change', function () {
            return viewTaskModuleStructure($(this).val());
        });

        function getXMLHTTP() {

            var xmlhttp = false;
            try {
                xmlhttp = new XMLHttpRequest();
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    try {
                        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e1) {
                        xmlhttp = false;
                    }
                }
            }

            return xmlhttp;
        }

        function viewTaskModuleStructure(id) {

            if (id == 0) {

                document.getElementById('div_module_structure').innerHTML = ' Please select desired structure. ';
            }
            else {

                var strURL = "{{URL::to('/')}}/ajax/viewTaskModuleStructure/" + id;

                var req = getXMLHTTP();

                if (req) {
//                    alert(JSON.stringify(req))
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                document.getElementById('div_module_structure').innerHTML = req.responseText;
                            } else {
                                alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }

                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }


        function log(value) {

            jQuery('#task_pair').val(value);
//            jQuery('.text-red').empty();
//            jQuery("div").removeClass("has-error");

        }

        var availableTags = [
                @foreach($pairs  as $id=> $pair )
                {
                id: "{{$id}}",
                value: "{{$pair}}"

            },
            @endforeach

];
        jQuery("#task_pair").autocomplete({
            source: availableTags,
            minLength: 0,
            select: function (event, ui) {
                log(ui.item ? ui.item.value : "");
            }
        });


        jQuery('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    });

    // drag and drop search
    $(function () {
        $('#sortable').sortable({
            axis: 'y',
            opacity: 0.7,
            handle: 'span',
            update: function (event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
                // change order in the database using Ajax
                $.ajax({
                    url: 'set_order.php',
                    type: 'POST',
                    data: {list_order: list_sortable},
                    success: function (data) {
                        alert('success');
                        //finished
                    }
                });
            }
        }); // fin sortable

        $('.sortable_task').sortable({
            axis: 'y',
            opacity: 0.7,
            handle: 'span',
            update: function (event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
                // change order in the database using Ajax
                var data = {
                    'list_order': list_sortable,


                };

                $.post('{{  route("ajax.set_order_adm_task") }}', data)
                        .done(function (msg) {
//                            alert('ok')
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });
            }
        }); // fin sortable
        // do this for every level
        for (var level = 0; level <= 5; level++) {
            $('.sortable_modules_' + level).sortable({
                axis: 'y',
                opacity: 0.7,
                handle: 'span',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();
                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable
                    };

                    $.post('{{  route("ajax.set_order_adm_modules") }}', data)
                            .done(function (msg) {
                                // success
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                //  alert(xhr.responseText);
                            });
                }
            }); // fin sortable
        }

    });

</script>


@stop
@section('content')
    <div class="loader_back">
        <div class="loader_gif"></div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <a href="{{ URL::route('software.edit',[$software->id_software]) }}"
               class="btn ls-orange-btn btn-flat">
                <i class="fa fa-edit"></i>
            </a>
            <h3 class=" box-title">{{ $software->name_software }}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body software_module_structure">
            <div class="row">
                <div class="col-xs-6">
                    <h3 id="header">Search words
                        <form class="filterform" action="#">
                            <input class="filterinput" type="text" id="filterinput">
                        </form>
                    </h3>
                    <div class="easy-tree ls-tree-view">
                        <ul class="ul-tree">
                            <li id="project_name">  <span class="menu_title" id="submodule0">
                                      <span class="glyphicon glyphicon-minus-sign"></span>
                                    <a href="javascript:void(0); ">
                                        {{ $software->name_software }}
                                    </a>
                           </span>
                                <ul class="sortable_modules_0" id="list">

                                    <?php printTreeAdmin($module_structure, $software->id_software, 1);
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="task_page_details">&nbsp;</div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Modal Add mew task and structure -->
    <div class="modal fade" id="addNewTask" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" id="borderInnerTask">
                <button type="button" class="close close_modal"
                        id="close_modal_new" data-dismiss="modal" aria-hidden="true"> x
                </button>
                {{ Form::open(['action'=>'TaskController@add_new_task_admin' ])}}
                <div class="modal-header task-label-white">
                    Add new task
                    <div class="clear"></div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form_edit">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Task Name" name="name_task" required>
                                </div>
                                <div class="form-group">
                                    <textarea style="width: 100%" name="description_task" class="summernote_new" id="summernote_new"></textarea>
                                </div>
                                <div class="form-group">
                                    {{Form::checkbox('is_checked' ,1, false,['class'=>'switchCheckBox','data-size'=>"mini"])}} <label> Mandatory Task</label>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{Form::submit('Save and close', ['class'=>"btn ls-light-blue-btn"])}}
                    {{Form::hidden('structure','',['id'=>'easyTreeStructure'])}}
                    {{Form::hidden('fk_software',$software->id_software)}}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal Edit Task -->
    <div class="modal fade" id="modalEditTask" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Edit Task End -->

    <!-- Modal Add new task for existing structure -->
    <div class="modal fade" id="modalAddTask" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Add Task End -->
@stop
