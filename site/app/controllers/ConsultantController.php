<?php
use Illuminate\Support\MessageBag;

class ConsultantController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $consultants = Consultant::all();
        return View::make('admin.consultant.index', compact('consultants'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Input::hasFile('logo_user')) {
            $image_details = App::make('UsersController')->SaveImage('logo_user', "images/image_user/");
        } else {
            $image_details = null;
        }
        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }
        $consultant = new Consultant();
        $consultant->fill(Input::except('logo_user'));
        if ($name_image != null) {
            $consultant->image_consultant = $name_image;
        }

        $consultant->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = Consultant::getFirstById($id);

        if (Input::hasFile('logo_user')) {
            $image_details = App::make('UsersController')->SaveImage('logo_user', "images/image_user/");
        } else {
            $image_details = null;
        }
        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }
//        return var_dump(Input::except(array('logo_user')));
        // if edit profile
        $user->fill(Input::except('logo_user'));

        if ($name_image != null) {
            $user->image_consultant = $name_image;
        }
        $user->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);

        if (Input::has('redirect_link')) {
            $redirect_link = Input::get('redirect_link');
        }

        return Redirect::to($redirect_link)->withErrors($errors);
    }

    public function approve($id)
    {

        $user = Consultant::getFirstById($id);
        $user->blocked = 0;
        $user->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }

    // block desired user
    public function block($id)
    {

        $user = Consultant::getFirstById($id);
        $user->blocked = 1;
        $user->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_delete')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
