PROFICRM DEVELOPMENT SETUP (via Vagrant)

1. Edit config.sh

	#MySql credentials
	DBHOST=localhost
	DBNAME=justchecking
	DBUSER=root
	DBPASSWD=root

	#SCRIPT SETTINGS
	RUN_INIT_CONFIG=true
	DELETE_DB=false

	#SSL CERTIFICATE CONFIGURATION
	country=RO
	state=Bihor
	locality=Oradea
	organization=JUSTCHECKING
	organizationalunit=IT
	email=administrator@localhost.com
	commonname=localhost

2. Add additional sql scripts (Optional)
    
   a. If you need to run sql script BEFORE installing fresh copy then create distribution/db/initial.sql file and add your sql code.   
   b. If you need to run sql script AFTER installing fresh copy then create distribution/db/postInstall.sql file and add your sql code.

3. Run command "vagrant up" in command line from where your project is located.

4. SSH into vagrant using "vagrant ssh"

5. Install dependencies by going to /var/www/ and run "composer install", 
   Set enviroment: export PHINX_ENVIRONMENT=localhost,development,production 

6. Bring database up to date by running following command
   For local only: "php vendor/robmorgan/phinx/bin/phinx migrate"
   For dev & production: "vendor/bin/phinx migrate"

7. Go to https://localhost. Enjoy :)
