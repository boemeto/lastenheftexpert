/*
 * Author : Ali Aboussebaba
 * Email : bewebdeveloper@gmail.com
 * Website : http://www.bewebdeveloper.com
 * Subject : Dynamic Drag and Drop with jQuery and PHP
 */

$(function () {
    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            // change order in the database using Ajax
            $.ajax({
                url: 'set_order.php',
                type: 'POST',
                data: {list_order: list_sortable},
                success: function (data) {
                    alert('success');
                    //finished
                }
            });
        }
    }); // fin sortable

    $('.sortable_task').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable

    $('.sortable_modules_0').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
    $('.sortable_modules_1').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
    $('.sortable_modules_2').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
    $('.sortable_modules_3').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
    $('.sortable_modules_4').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
    $('.sortable_modules_5').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function (event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
//                alert(list_sortable)
            // change order in the database using Ajax
            var data = {
                'list_order': list_sortable,


            };

            $.post('{{  route("ajax.set_order_adm_task") }}', data)
                .done(function (msg) {
//                            alert('ok')
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                });
        }
    }); // fin sortable
});