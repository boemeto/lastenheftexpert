@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop


@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-pencil-square-o"></i>

            <h3 class="box-title">  {{trans('messages.edit')}}   {{trans('messages.module')}} {{$module->name_Software}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">

                {{ Form::model($module,['route'=>['module.update',$module->id_module], 'method'=>'PATCH'])}}
                <div class="col-lg-12">

                    <div class="form-group">
                        {{ Form::label('name_module','Name: ') }}
                        {{ Form::text('name_module', $module->name_module, ['class'=>"form-control"]) }}
                        {{ $errors->first('name_module','<div class="alert alert-danger">:message</div>') }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description_module','Description: ') }}
                        {{ Form::textarea('description_module', $module->description_module, ['class'=>"form-control"]) }}
                        {{ $errors->first('description_module','<div class="alert alert-danger">:message</div>') }}
                    </div>

                </div>
                <div class="col-lg-6">
                    <button type="submit" class="btn ls-light-blue-btn btn-flat ">
                        <i class="fa fa-pencil-square-o"></i> {{trans('messages.edit')}}
                    </button>

                </div>
                {{ Form::close()}}
                <div class="col-lg-6">
                    {{ Form::open(array('route' => array('module.destroy', $module->id_module), 'method' => 'delete')) }}
                    <button type="submit" class="btn ls-light-blue-btn btn-flat "
                            onclick="return confirm('{{trans('validation.confirmation_question')}}')">
                        <i class="fa fa-trash"></i>
                        {{trans('messages.permanently_delete')}}
                    </button>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>


@stop