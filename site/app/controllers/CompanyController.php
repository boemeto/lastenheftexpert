<?php
use Illuminate\Support\MessageBag;

class CompanyController extends \BaseController
{

    /**
     * View user's companies and headquarters
     *
     * @return Response
     */
    public function index()
    {

        $titles = Users_title::getTitle();
        $personal_titles_new = Users_personal_title::getPersonal_Title(1);
        $personal_titles = Users_personal_title::getPersonal_Title();

        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        // if user is company admin

        $user = User::getFirstById($id_user);
        $id_company = $user->fk_company;
        // get user's company details
        $company = Company::whereId_company($id_company)->first();
        // get the headquarters
        $main_headquarter = Company_headquarters::getMainHeadquarter($id_company);

        $headquarters = Company_headquarters::getHeadquartersLocationsNoBlocked($id_company);
        // total number of users
        $all_users = User::getAllUsersDetailsByCompany($id_company);
        $users_blocked = User::whereFk_company($id_company)
            ->whereBlocked(1)
            ->count();
        foreach ($headquarters as $headquarter) {
            $nr = 0;
            $users = User::whereFk_headquarter($headquarter->id_headquarter)->get();
            $nr = count($users);
            $nr_users_headquarter[$headquarter->id_headquarter] = $nr;
        }
        // dd($personal_titles_new);
        $projects = Project::whereFk_company($id_company)->get();
        // make view
        return View::make('front.company.index', compact('user', 'company', 'headquarters', 'fk_group', 'personal_titles_new',
            'nr_users_headquarter', 'all_users', 'users_blocked', 'titles', 'personal_titles', 'projects', 'main_headquarter'));


    }

    public function redirectFromDasboard($tab)
    {
        Session::flash('tab', $tab);
        return $this->index();
    }

    public function redirectFromDasboardAddNew($tab, $add_new)
    {
        Session::flash('tab', $tab);
        Session::flash('add_for', $add_new);
        return $this->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($type)
    {
        $titles = Users_title::getTitle();
        $personal_titles_new = Users_personal_title::getPersonal_Title(1);
        $personal_titles = Users_personal_title::getPersonal_Title();

        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        // if user is company admin

        $user = User::getFirstById($id_user);
        $id_company = $user->fk_company;
        // get user's company details
        $company = Company::whereId_company($id_company)->first();
        // get the headquarters
        $main_headquarter = Company_headquarters::getMainHeadquarter($id_company);

        $headquarters = Company_headquarters::getHeadquartersLocationsNoBlocked($id_company);
        // total number of users
        $all_users = User::getAllUsersDetailsByCompany($id_company);
        $users_blocked = User::whereFk_company($id_company)
            ->whereBlocked(1)
            ->count();
        foreach ($headquarters as $headquarter) {
            $nr = 0;
            $users = User::whereFk_headquarter($headquarter->id_headquarter)->get();
            $nr = count($users);
            $nr_users_headquarter[$headquarter->id_headquarter] = $nr;
        }

        $projects = Project::whereFk_company($id_company)->get();
        // make view
        return View::make('front.company.index', compact('type' ,'user', 'company', 'headquarters', 'fk_group', 'personal_titles_new',
            'nr_users_headquarter', 'all_users', 'users_blocked', 'titles', 'personal_titles', 'projects', 'main_headquarter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $company = Company::whereId_company($id)->first();
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (Input::hasFile('logo')) {
            $image_details = App::make('UsersController')->SaveImage('logo');
        } else {
            $image_details = null;
        }

        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }

        $company = Company::whereId_company($id)->first();
        if (Input::has('is_blocked')) {
            $is_blocked = Input::get('is_blocked');
            $company->is_blocked = $is_blocked;
        } else {
            $company->is_blocked = 0;
        }


        $company->fill(Input::except(array('logo', 'is_blocked')));
        if ($name_image) {
            $company->logo_company = $name_image;
        }
        $company->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
