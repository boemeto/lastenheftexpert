<?php

class AdmTasksController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $id_user = Auth::user()->id;
        $creator_user = User::getFirstById($id_user);

        $name_task = Input::get('name_task');
        $notes_task = Input::get('notes_task');
        $fk_sprint = Input::get('fk_sprint');
        $fk_status = Input::get('fk_status');
        $fk_priority = Input::get('fk_priority');
        $cost_task = Input::get('cost_task');
        $due_date = date('Y-m-d', strtotime(Input::get('due_date')));
        $sprint = Adm_sprint::getFirstById($fk_sprint);
        $status = Adm_status::getFirstById($fk_status);
        $priority = Adm_priority::getFirstById($fk_priority);
        $name_files = '';

        $id_task = Adm_task::createNewItem($name_task, $notes_task, $id_user, $fk_sprint, $fk_priority, $cost_task, $fk_status, $due_date);
        if (Input::hasFile('task_attachment')) {
            foreach (Input::file('task_attachment') as $file) {
                Adm_attachment::uploadFile($file, $id_task);
                $name_files .= $file->getClientOriginalName() . ', ';
            }
        }
        
        Session::flash('scrollTo', $id_task);
        $name_responsibles = '';
        // get and add responsible users
        $responsible = Input::get('fk_responsible');
        foreach ($responsible as $user) {
            if ($user != 0) {
                $user_details = User::getFirstById($user);
                $user_name = $user_details->first_name . ' ' . $user_details->last_name;
                $name_responsibles .= $user_name . ', ';
            }
        }
        $subject = 'Eine neue Aufgabe "' . $name_task . '" wurde hinzugefügt!';
        $site_url = URL::to('/');

        $message = 'Sehr geehrte Damen und Herren, <br>
                    im Taks-Manager wurde eine neue Aufgabe hinzugefügt. 
                    Einzelheiten dazu entnehmen Sie bitte weiter unten in dieser E-Mail:<br><br>';
        $message .= 'Erstellt von: ' . $creator_user->first_name . ' ' . $creator_user->last_name . '<br>';
        $message .= 'Sprint: ' . $sprint->name_sprint . '<br>';
        $message .= 'Aufgabenbezeichnung: ' . $name_task . '<br>';
        $message .= 'Beschreibung:  ' . $notes_task . '<br>';
        $message .= 'Priorität: ' . $priority->desc_priority . '<br>';
        $message .= 'Fälligkeit: ' . date('d.m.Y', strtotime($due_date)) . '<br>';
        $message .= 'Kosten: ' . $cost_task . ' € <br>';
        $message .= 'Status: ' . $status->name_status . '<br>';
        $message .= 'Verantwortliche: ' . $name_responsibles . '<br>';
        $message .= 'Anhänge: ' . $name_files . '<br><br>';
        $message .= 'Um die Aufgabe zu bearbeiten, melden Sie sich bitte mit Ihren Zugangsdaten unter folgendem Link ein:
                    Link zum Task-Manager: ' . $site_url . '<br>
                    Mit freundlichen Grüßen,<br>
                    Das Lastenheft.Expert Team';

        foreach ($responsible as $user) {
            if ($user != 0) {
                $responsibility = Adm_responsibility::where('fk_user', '=', $user)
                    ->where('fk_task', '=', $id_task)->first();
                if (count($responsibility) == 0) {
                    Adm_responsibility::createNewItem($user, $id_task);
                    $user_details = User::getFirstById($user);
                    $email = $user_details->email;
                    $user_name = $user_details->first_name . ' ' . $user_details->last_name;
                    send_mail($user_name, $email, $subject, $message);
                }
            }
        }

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $releases = Adm_release::all();
        $current_release = Adm_release::getFirstById($id);
        $sprints = Adm_sprint::where('fk_release', '=', $id)
            ->orderBy('is_closed', 'asc')
            ->orderBy('created_at', 'asc')
            ->get();
        $users = User::getUsersByGroup(1);
        $status = Adm_status::getAllStatus();

        return View::make('admin.adm_task.show', compact('releases', 'current_release', 'sprints', 'id', 'users', 'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {        
        $task = Adm_task::getFirstById($id);
        $task->name_task = Input::get('name_task');
        $task->notes_task = Input::get('notes_task');
        $task->fk_status = Input::get('fk_status');
        $task->fk_priority = Input::get('fk_priority');
        $task->cost_task = Input::get('cost_task');
        $task->due_date = date('Y-m-d', strtotime(Input::get('due_date')));
        $task->save();
        $id_task = $task->id_task;
        if (Input::hasFile('task_attachment')) {
            foreach (Input::file('task_attachment') as $file) {
                Adm_attachment::uploadFile($file, $id_task);
            }
        }
        $task_responsibles = Adm_responsibility::getResponsibleList($id_task);
        $responsible = Input::get('fk_responsible');
        $rep_int = array();
        foreach ($responsible as $user) {
            if ($user != 0) {
                $responsibility = Adm_responsibility::where('fk_user', '=', $user)
                    ->where('fk_task', '=', $id_task)->first();
                if (count($responsibility) == 0) {
                    $id_responsibility = Adm_responsibility::createNewItem($user, $id_task);
                }
                $rep_int[] = intval($user);
            }
        }

        foreach ($task_responsibles as $task_responsible) {

            if (!in_array($task_responsible, $rep_int)) {
                $responsibility = Adm_responsibility::where('fk_user', '=', $task_responsible)
                    ->where('fk_task', '=', $id_task)->first();
                $responsibility->delete();
            }
        }

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id_task)
    {
        Adm_comment::where('fk_task', '=', $id_task)->delete();
        Adm_attachment::where('fk_task', '=', $id_task)->delete();
        Adm_responsibility::where('fk_task', '=', $id_task)->delete();
        $task = Adm_task::getFirstById($id_task);
        $task->delete();
        return Redirect::back();
    }

    public function adm_tasks_dialog_add_task($id_release, $id_sprint)
    {
        $users = User::getAdminUserData(1);
        $fk_sprint = $id_sprint;
        $priorities = Adm_priority::getAllPriorities();
        $status = Adm_status::getAllStatus();

        return View::make('admin.adm_task.modal_dialog_add_task', compact('id_release', 'fk_sprint', 'users', 'priorities', 'status'));
    }

    public function adm_tasks_dialog_edit_task($id_task)
    {
        $task = Adm_task::getFirstById($id_task);
        $users = User::getAdminUserData(1);
        $status = Adm_status::getAllStatus();
        $priorities = Adm_priority::getAllPriorities();

        $arr_resp = Adm_responsibility::getResponsibilityByTask($id_task);
        foreach ($arr_resp as $arr) {
            $responsible[] = $arr->id;
        }


        return View::make('admin.adm_task.modal_dialog_edit_task', compact('id_task', 'status', 'priorities', 'users', 'task', 'responsible'));
    }

    public function adm_delete_attached_file($id_attachment)
    {
        $project_attach = Adm_attachment::getFirstById($id_attachment);
        $project_attach->delete();
        return Redirect::back();
    }

    public function delete_resp_task($id_task, $fk_user)
    {
        $responsibility = Adm_responsibility::where('fk_user', '=', $fk_user)
            ->where('fk_task', '=', $id_task)->first();
        $responsibility->delete();
        return Redirect::back();
    }


}
