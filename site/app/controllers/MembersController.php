<?php
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Session;

class MembersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $groups = Groups::getAllGroups();
        $users = User::orderBy('fk_company', 'asc')->get();
        return View::make('admin.members.index', compact('users', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function show($id_user)
    {
        allowOnlySuperAdmin();
        $id_user_logged = Auth::user()->id;
        $fk_group = Groups::getGroupsIds($id_user_logged, 1);

        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();

        $user = User::whereId($id_user)->first();
        $id_company = $user->fk_company;
        $id_headquarter = $user->fk_headquarter;
        // get user's company details
        $company = Company::whereId_company($id_company)->first();
        // get the headquarters
        $main_headquarter = Company_headquarters::getHeadquartersById($id_headquarter);

        $headquarters = Company_headquarters::getHeadquartersLocations($id_company);
        $headquarters_user = Company_headquarters::getHeadquartersSelect($company->id_company, 1);
        $all_users = User::getAllUsersDetailsByCompany($id_company);


        $users_blocked = User::whereFk_company($id_company)
            ->whereBlocked(1)
            ->count();
        foreach ($headquarters as $headquarter) {

            $users = User::whereFk_headquarter($headquarter->id_headquarter)->get();
            $nr = count($users);
            $nr_users_headquarter[$headquarter->id_headquarter] = $nr;
        }

        if (in_array('2', $fk_group) || $id_user == $id_user_logged) {
            $projects = Project::getUsersProjects($id_user);
        } else {
            $projects = Project::getUsersCommonProjects($id_user, $id_user_logged);
        }

        return View::make('admin.users.profile', compact('user', 'company', 'headquarters', 'fk_group',
            'nr_users_headquarter', 'all_users', 'users_blocked', 'titles', 'personal_titles',
            'projects', 'main_headquarter', 'id_headquarter', 'id_user', 'id_user_logged', 'headquarters_user'));
    }


    public function create_adm($fk_headquarter)
    {
        $headquarter = Company_headquarters::getFirstById($fk_headquarter);
        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();
        return View::make('admin.members.create', compact('titles', 'personal_titles', 'headquarter'));
    }

    public function create()
    {
        $id_headquarter = Input::get('id');
        $headquarter = Company_headquarters::getFirstById($id_headquarter);
        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();
        if (Auth::user()->fk_group == 1) {
            return View::make('admin.members.create', compact('titles', 'personal_titles', 'headquarter'));
        }
        return View::make('front.members.create', compact('titles', 'personal_titles', 'headquarter'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // dd(Input::all());
        $rules = [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|email|unique:users|min:4',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::except('password'))
                ->withErrors($validator);
        }
        if (Input::hasFile('logo_user')) {
            $image_details = App::make('UsersController')->SaveImage('logo_user', "images/image_user/");
        } else {
            $image_details = null;
        }
        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }
        $id_headquarter = Input::get('fk_headquarter');
        $user = new User;
        $user->fill(Input::except('logo_user'));

        if (!Input::has('fk_company')) {
            $fk_company = Company_headquarters::getCompanyByHeadquarter($id_headquarter);
            $user->fk_company = $fk_company;
        }

        $email = Input::get('email');
        $activation = substr(hash('sha256', $email), 2, 20);

        $user->activation_code = $activation;

        if (Input::has('password')) {
            $user->password = Hash::make(Input::get('password'));
            $password = '';
        } else {
            $password = str_random(6);
            $user->password = Hash::make($password);
        }

        if ($name_image != null) {
            $user->logo_user = $name_image;
        }

        $user->save();
        $activation_code = url() . "/activation?user=" . $user->id . "&hash=" . $activation;
        $data = array(
            'detail'   => $activation_code,
            'password' => $password,
            'user'     => $user
        );

//        if ($password) {
//            Mail::send('emails.new_user_pass', $data, function ($message) use ($user) {
//                $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('New user!');
//            });
//        } else {
//            Mail::send('emails.activate_user', $data, function ($message) use ($user) {
//                $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('New user!');
//            });
//        }

        if ($user->id > 0) {
            $user_group = new User_groups();
            $user_group->fk_user = $user->id;
            $user_group->fk_group = 2;
            $user_group->save();
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        $redirect_link = 'headquarters/' . $id_headquarter;
        if (Input::has('redirect_link')) {
            $redirect_link = Input::get('redirect_link');
        }
        if (Input::has('company_edit_user')) {
            Session::flash('go-to', URL::to('users/'.$user->id));
            return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
        }
        return Redirect::to($redirect_link)->withErrors($errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        // if user is company admin
        if (in_array(1, $fk_group)) {
            $user = User::getFirstById($id);
            $titles = Users_title::getTitle();
            $personal_titles = Users_personal_title::getPersonal_Title();
            $groups = Groups::getGroups();
            $headquarters = Company_headquarters::getHeadquartersSelect($user->fk_company);
            $user_groups = Groups::getGroups($id_user);
            return View::make('admin.members.edit', compact('user', 'titles', 'personal_titles', 'groups', 'headquarters', 'user_groups'));

        }
        return Redirect::to('404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::getFirstById($id);

        // if change password
        if (Input::has('password_old')) {
            //  if old password is correct
            if (Hash::check(Input::get('password_old'), $user->password)) {
                $rules = [
                    'password'         => 'required|min:2',
                    'password_confirm' => 'required|same:password|min:2',
                ];
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                    return Redirect::back()
                        ->withInput(Input::except('password'))
                        ->withErrors($validator);
                }
                if (Input::has('password')) {
                    $user->password = Hash::make(Input::get('password'));
                }
                $user->save();
                $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
                if (Input::has('redirect_link')) {
                    $redirect_link = Input::get('redirect_link');
                }
                return Redirect::to($redirect_link)->withErrors($errors);

            } else {
                $errors = new MessageBag(['password_old' => [Lang::get('validation.password_wrong')]]);
                return Redirect::Back()->withErrors($errors);
            }
        }
        if (Input::hasFile('logo_user')) {
            $image_details = App::make('UsersController')->SaveImage('logo_user', "images/image_user/");
        } else {
            $image_details = null;
        }
        if ($image_details != null || $image_details != '') {
            $array = explode('//', $image_details);
            $name_image = $array[0];
        } else {
            $name_image = null;
        }
//        return var_dump(Input::except(array('logo_user')));
        // if edit profile
        $user->fill(Input::except(array('logo_user', 'password')));
        if (Input::has('password')) {
            $user->password = Hash::make(Input::get('password'));
        }
        if ($name_image != null) {
            $user->logo_user = $name_image;
        }
        $user->save();
        $id_headquarter = $user->fk_headquarter;
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        $redirect_link = 'headquarters/' . $id_headquarter;
        if (Input::has('redirect_link')) {
            $redirect_link = Input::get('redirect_link');
        }
        if(Input::has('company_edit_user')) {
            Session::flash('go-to', URL::to('users/'.$id));
            return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
        }
        Session::flash('tab', 'employees');
        Session::flash('go-to', URL::to('members/'.$id));
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function delete_adm($id)
    {
        DB::beginTransaction();
        try {
            User_groups::where('fk_user', '=', $id)->delete();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
            return Redirect::back()->withErrors($errors);
        }
        try {
            $user = User::getFirstById($id);
            $user->delete();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
            return Redirect::back()->withErrors($errors);
        }

        DB::commit();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }
}
