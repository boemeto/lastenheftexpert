<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 19-Aug-16
 * Time: 12:54
 */
//'id_pack_row', 'name_field', 'description_pack', 'name_field_en', 'description_pack_en', 'fa-icon', 'fk_package'
class Package_row extends Eloquent
{

    public static $rules = [
        'name_package' => 'required',
    ];
    public $primaryKey = 'id_pack_row';
    protected $table = 'packages_row';
    protected $guarded = ['id_pack_row'];
    protected $fillable = ['name_field', 'description_pack', 'name_field_en', 'description_pack_en', 'fa_icon', 'fk_package'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }


    public static function getFirstById($id)
    {
        $list = self::where('id_pack_row', '=', $id)->first();
        return $list;
    }

    public static function getByPackage($id)
    {
        $list = self::where('fk_package', '=', $id)->get();
        return $list;
    }


    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('packages_row', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('packages_row', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'de':
                return $this->attributes[$field];
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }
}