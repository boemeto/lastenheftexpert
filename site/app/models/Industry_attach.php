<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 22-Feb-16
 * Time: 11:10
 */
class Industry_attach extends Eloquent
{


    public static $rules = [
        'fk_industry_ms' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_industry_attachment';
    protected $table = 'pr_industry_attachments';
    protected $guarded = ['id_industry_attachment'];
    protected $fillable = ['name_attachment', 'extension', 'folder_attachment', 'fk_industry_ms'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Industry_attach::where('id_industry_attachment', '=', $id)->first();
        return $list;
    }

    public static function getAttachmentsByInd_ms($id)
    {
        $list = Industry_attach::where('fk_industry_ms', '=', $id)->get();
        return $list;
    }

    public static function getAttachmentByMS_task($fk_ms_task, $fk_industry)
    {
        $attachments = Industry_attach::join('pr_industry_ms', 'fk_industry_ms', '=', 'id_industry_ms')
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->where('fk_industry', '=', $fk_industry)
            ->get();
        return $attachments;
    }

    public static function getAttachmentByTask($id_ms_task, $fk_industry)
    {
        $attachments = Industry_attach::join('pr_industry_ms', 'fk_industry_ms', '=', 'id_industry_ms')
            ->where('fk_ms_task', '=', $id_ms_task)
            ->where('fk_industry', '=', $fk_industry)
            ->get();
        return $attachments;
    }

    public static function getIdMsTaskByAttachment($id)
    {
        $attachments = Projects_attach::join('pr_industry_ms', 'id_industry_ms', '=', 'fk_industry_ms')
            ->where('id_industry_attachment', '=', $id)
            ->first();
        if (count($attachments) == 1) {
            return $attachments->fk_ms_task;
        } else {
            return 0;
        }

    }

    public static function getTasksWithAttachments($id, $id_industry)
    {
        $attachments = Industry_attach::join('pr_industry_ms', 'id_industry_ms', '=', 'fk_industry_ms')
            ->where('fk_module_structure', '=', $id)
            ->where('fk_industry', '=', $id_industry)
            ->get();
        foreach ($attachments as $attachment) {
            $array[] = $attachment->fk_ms_task;
        }
        return array_unique($array);
    }

    public static function getIconByAttachment($extension)
    {

        $extension = strtolower($extension);
        $path = base_path() . '/images';
        $url = URL::asset('images');
        $file_path = $path . "/icons/" . $extension . "/" . $extension . "-48_32.png";
        if (file_exists($file_path)) {
            $file = $url . "/icons/" . $extension . "/" . $extension . "-48_32.png";
            $icon = "url('" . $file . "');";
        } else {
            $file = $url . "/icons/text/text-48_32.png";
            $icon = "url('" . $file . "');";
        }

        return $icon;

    }

    public static function addAttachToAllIndustry($fk_ms_task, $folder, $directory_soft, $filename, $extension)
    {
        $industry_ms_tasks = Industry_ms::where('fk_ms_task', '=', $fk_ms_task)->get();
        if (count($industry_ms_tasks) > 0) {
            $directory_ind = 'images/attach_industry/' . $folder . '/';
            copy($directory_soft . '/' . $filename, $directory_ind . '/' . $filename);
            foreach ($industry_ms_tasks as $industry_ms) {
                $attachment_industry = Industry_attach::whereName_attachment($filename)
                    ->where('folder_attachment', 'like', $folder)
                    ->where('fk_industry_ms', '=', $industry_ms->id_industry_ms)->get();
                if (count($attachment_industry) == 0) {
                    $attachment_industry = new Industry_attach();
                    $attachment_industry->folder_attachment = $folder;
                    $attachment_industry->name_attachment = $filename;
                    $attachment_industry->extension = $extension;
                    $attachment_industry->fk_industry_ms = $industry_ms->id_industry_ms;
                    $attachment_industry->save();
                }
            }
        }
        return;
    }

    public static function addAttachToSelectedIndustry($fk_ms_task, $fk_industry, $folder, $directory_soft, $filename, $extension)
    {
        $industry_ms = Industry_ms::getFirstByKey($fk_ms_task, $fk_industry);
        if (count($industry_ms) > 0) {
            $attachment_industry = Industry_attach::whereName_attachment($filename)
                ->where('folder_attachment', 'like', $folder)
                ->where('fk_industry_ms', '=', $industry_ms->id_industry_ms)->get();
            if (count($attachment_industry) == 0) {
                $directory_ind = 'images/attach_industry/' . $folder . '/';
                copy($directory_soft . '/' . $filename, $directory_ind . '/' . $filename);
                $attachment_industry = new Industry_attach();
                $attachment_industry->folder_attachment = $folder;
                $attachment_industry->name_attachment = $filename;
                $attachment_industry->extension = $extension;
                $attachment_industry->fk_industry_ms = $industry_ms->id_industry_ms;
                $attachment_industry->save();
            }

        }
    }

    public static function deleteAttachFromSelectedIndustry($fk_ms_task, $fk_industry, $filename, $folder)
    {
        $industry_ms = Industry_ms::getFirstByKey($fk_ms_task, $fk_industry);
        if (count($industry_ms) > 0) {
            Industry_attach::whereName_attachment($filename)
                ->where('folder_attachment', 'like', $folder)
                ->where('fk_industry_ms', '=', $industry_ms->id_industry_ms)->delete();

        }
    }

    public static function deleteAttachFromAllIndustry($fk_ms_task, $filename, $folder)
    {
        $industry_ms_tasks = Industry_ms::where('fk_ms_task', '=', $fk_ms_task)->get();
        if (count($industry_ms_tasks) > 0) {
            foreach ($industry_ms_tasks as $industry_ms) {
                Industry_attach::whereName_attachment($filename)
                    ->where('folder_attachment', 'like', $folder)
                    ->where('fk_industry_ms', '=', $industry_ms->id_industry_ms)->delete();
            }
        }
    }


}
