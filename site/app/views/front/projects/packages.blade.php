@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')

@stop

@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel widget light-widget panel-bd-top ">
                        <div class="panel-heading ">
                            <h3 class="mgtp-10">
                                {{trans('messages.label_create_choose_package')}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row ls-android-pricing-table-wrap">
                                @foreach($packages as $package)
                                    <?php
                                      $package_row = Package_row::getByPackage($package->id_package);
                                    ?>
                                    <div class="col-ld-3 col-md-3 col-sm-3 col-xs-6 pd-5">
                                        <div class="android android-basic package-item" style="background: {{$package->color}}">
                                            <div class="row package_specification">
                                              <ul>
                                                  <li style="background: {{$package->color}}">
                                                      <p class="package_name">{{$package->name_package}}</p>
                                                      <p class="package_description">{{$package->description}}</p>
                                                      <div class="ls-bubble" style="background: {{$package->color}}">
                                                          <span class="ls-pricing"> {{formatNumbers( $package->price)}} €</span><span class="ls-pricing-m"> </span>
                                                          <br><span class="ls-pricing-m"> {{  $package->period }} {{ getDictionaryName( $package->period_type,7) }} </span>
                                                      </div>
                                                  </li>
                                                  @foreach($package_row as $row)
                                                      <li>
                                                          <h4>{{$row->name_field}}</h4>
                                                          <p><i class="fa {{$row->fa_icon}} fa-2x"></i><span>{{$row->description_pack}}</span></p>
                                                      </li>
                                                  @endforeach
                                                  <li style="background: {{$package->color}}">
                                                      @if($package->is_active ==0)
                                                        <a href="javascript:void(0) " disabled="disabled" class="btn free-btn disabled">{{trans('messages.label_package_inactive')}}</a>
                                                      @else
                                                          {{ Form::open(['action'=>'ProjectsController@packages','id'=>'FormNewProject']) }}
                                                            {{Form::submit(trans('messages.label_choose_plan'),['class'=>'btn free-btn'])}}
                                                            {{Form::hidden('id',$package->id_package)}}
                                                          {{ Form::close() }}
                                                      @endif
                                                  </li>
                                              </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-xs-12">
                                    {{trans('messages.label_package_help_text')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
