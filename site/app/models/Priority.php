<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 29-Oct-15
 * Time: 11:33
 */
class Priority extends Eloquent
{


    public static $rules = [
        'lvl_priority' => 'required',

    ];
    public static $messages = [];
    public $primaryKey = 'id_priority';
    protected $table = 'priorities';
    protected $guarded = ['id_priority'];
    protected $fillable = ['lvl_priority', 'color_priority'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = Priority::where('id_priority', '=', $id)->first();
        return $list;
    }

    public static function getAllPriorities($type_line = 0)
    {
        $array = array();
        $lists = Priority::select('*')->get();
        if ($type_line == 1) {
            $array[0] = '-';
        } else {
            $array[0] = trans('messages.input_select');
        }

        foreach ($lists as $list) {
            $array[$list->id_priority] = $list->lvl_priority;
        }
        return $array;
    }


}