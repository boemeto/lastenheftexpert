<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCompanyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company', function ($table) {
            $table->increments('id_company')->unsigned();
            $table->string('name_company', 200)->nullable();
            $table->string('logo_company', 200)->nullable();
            $table->string('name_user', 100)->nullable();
            $table->string('street', 200)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('zipcode', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('telephone', 30)->nullable();
            $table->string('fax', 30)->nullable();
            $table->string('web', 100)->nullable();
            $table->string('iban', 60)->nullable();
            $table->string('bic', 60)->nullable();
            $table->string('company_code', 100)->nullable();
            $table->string('company_number', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_company');
    }
}