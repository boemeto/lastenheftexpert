<!-- MiniColors Library Script Start -->
<script src='{{ URL::asset('assets/js/tinycolorpicker.js') }}'></script>
<!-- MiniColors Library Script Start -->
<script src="{{ URL::asset('assets/js/dropzone.js')}}"></script>
<!-- Module menu tree style -->
<script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
{{--<script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js')}}"></script>--}}
{{-- Summernote textarea--}}
<script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>


{{--// ColorPicker--}}
<script type="text/javascript">
    // enable colorpicker
    window.onload = function () {

        var x = document.getElementsByClassName("colorPicker");
        var i;
        for (i = 0; i < x.length; i++) {
            tinycolorpicker(x[i]);
        }

    };
    jQuery(document).ready(function () {


        $('.panel-task-body').hover(function () {
            var tooltip = '<div class="tooltip"></div>';
            // Hover over code
            var title = $.trim($(this).attr('title'));

            if (title.length > 0) {
                $(this).data('tipText', title).removeAttr('title');

//            $(this).parent().append(tooltip);
                $('body').append(tooltip);

                $('.tooltip').html(title);
                $('.tooltip').fadeIn('slow');
            } else {
                $('body').append(tooltip);
            }

        }, function () {

            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function (e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip').css({top: mousey, left: mousex})
        });


        $('.summernote_new').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
        // colorpicker - change task color
        jQuery(".track").click(function () {
            var palette = [
                "#FE2712", "#A7194B", "#8601AF",
                "#3D01A4", "#0247FE", "#0391CE",
                "#66B032", "#D0EA2B", "#FEFE33",
                "#FABC02", "#FB9902"];
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('track', "");
            var $input = jQuery('#colorInput' + id_nr);
            var hex = $input.val();
            if (jQuery.inArray(hex, palette) == -1) {
                jQuery('#panel-task-border' + id_nr).css("background", 'transparent');
                jQuery('#panel-task-border' + id_nr).css("padding-right", '0');
                jQuery('#colorInnerTask' + id_nr).css("background", 'url("../../assets/images/minicolor/culori-rotund.png")');
                jQuery('#colorInnerTask' + id_nr).css("background-size", '15px 15px');
                jQuery('#borderInnerTask' + id_nr).css("border-right", '0px solid transparent');
                jQuery('#close_modal' + id_nr).css("right", '-24px');

                var hex_color = 'transparent';
            } else {
                jQuery('#panel-task-border' + id_nr).css("background-color", hex);
                jQuery('#panel-task-border' + id_nr).css("padding-right", '7px');
                jQuery('#colorInnerTask' + id_nr).css("background", hex);
                jQuery('#borderInnerTask' + id_nr).css("border-right", '7px solid ' + hex);
                jQuery('#close_modal' + id_nr).css("right", '-30px');
                var hex_color = hex;
            }


            var data = {
                'hex': hex_color,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                            alert(JSON.stringify(data))
//                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });
    });
</script>

{{--Modal box--}}
<script type="text/javascript">


    $("#modalEditTask").on('hidden.bs.modal', function () {
        $("#modalEditTask").removeData('bs.modal');
    });
    $("#modalAddTaskModule").on('hidden.bs.modal', function () {
        $("#modalAddTaskModule").removeData('bs.modal');
    });
    $("#modalAddTaskModule").on('shown.bs.modal', function () {
        // call sumernote text editor
        jQuery('.summernote_new').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
        // call switchCheckBox
        jQuery(".switchCheckBox").bootstrapSwitch();
    });

    $("#modalEditTask").on('shown.bs.modal', function () {

        $('.col-attachments').hover(function () {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('col-attachments', "");
            $('#attachments_edit_buttons' + id_nr).toggle();
        });
        // call switchCheckBox
        jQuery(".switchCheckBox").bootstrapSwitch();
        // call sumernote text editor
        jQuery('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            codemirror: { // codemirror options
                theme: 'monokai'
            },
            toolbar: [
                ['font', ["bold", "italic", "underline", "superscript", "subscript", "strikethrough", "clear"]],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
        // call dropzone - drag and drop files
        $(".dropzone").dropzone({uploadMultiple: false});
        // save modal checked on click
        jQuery('input.icheck-modal').change(function (event) {
            var id = jQuery(this).attr('id');

            var myArray = id.split(' ');
            var id_nr = myArray[1].replace('icheck-modal', "");
            var id_circle = myArray[0].replace('modal_circle', "");
            var value_circle = jQuery('#circle_tasks_left' + id_circle).html();

            if (jQuery(this).prop('checked')) {
                var is_checked = 1;
                value_circle = parseInt(value_circle) + 1;
                jQuery("[id='id_circle" + id_circle + " icheck-green" + id_nr + "']").prop('checked', true);

            } else {
                var is_checked = 0;
                value_circle = parseInt(value_circle) - 1;
                jQuery("[id='id_circle" + id_circle + " icheck-green" + id_nr + "']").prop('checked', false);

            }
            jQuery('#circle_tasks_left' + id_circle).html(value_circle);


            var data = {
                'is_checked': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
                        //                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
            event.stopPropagation();
        });
        // set priority task on change
        jQuery('.set_priority').change(function (event) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('set_priority', "");
            var priority = jQuery(this).val();

            var data = {
                'task_priority': priority,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                        alert(JSON.stringify(data))
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

        });
        // delete attachment
        jQuery('.button_delete_file').click(function (event) {
            if (confirm("Are you sure?")) {
                // your deletion code

                var id = jQuery(this).attr('id');
                var id_nr = id.replace('button_delete_file', '');


                var data = {
                    'id_project_attach': id_nr
                };
                jQuery.post('{{  route("ajax.remove_file_project") }}', data)
                        .done(function (msg) {
                            // alert('success')
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });
                jQuery('#col-attachments' + id_nr).hide();

                return true;
            }
            return false;
        });
        // add task details
        jQuery('.button_add_task').click(function (event) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('button_add_task', "");
            jQuery('#summernote' + id_nr).next('.note-editor').show();
            jQuery(this).hide();
            jQuery('#button_save_task' + id_nr).show();
            jQuery('#user_comment_task' + id_nr).hide();
            jQuery('#button_edit_task' + id_nr).hide();
        });
        // edit task details
        jQuery('.button_edit_task').click(function (event) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('button_edit_task', "");
            jQuery('#summernote' + id_nr).next('.note-editor').show();
            jQuery(this).hide();
            jQuery('#button_save_task' + id_nr).show();
            jQuery('#user_comment_task' + id_nr).hide();
            jQuery('#button_add_task' + id_nr).hide();
        });
        // delete task details 
        jQuery('.button_delete_task').click(function (event) {

            // confirm empty text
            var confirm_dialog = confirm("Are you sure?");
            if (confirm_dialog == true) {
                var id = $(this).attr('id');
                var id_nr = id.replace('button_delete_task', "");
                jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                $(this).addClass('disabled');
                var data = {
                    'task_description': '~',
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'

                };
                // save to database
                jQuery.post('{{  route("ajax.update_project_task") }}', data)
                        .done(function (msg) {
//                            alert(msg)
//                            alert(JSON.stringify(data))
                        })
                        .fail(function (xhr, textStatus, errorThrown) {
                            alert(xhr.responseText);
                        });

                $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
                jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                $('#button_add_task' + id_nr).removeClass('disabled');
                $('#button_edit_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).trigger('blur');
                $('#button_add_task' + id_nr).show();
                $('#button_edit_task' + id_nr).show();
                $('#user_comment_task' + id_nr).show();
                $('#button_save_task' + id_nr).hide();
            }
        });
        // save task details
        jQuery('.button_save_task').click(function (event) {

            var id = $(this).attr('id');
            var id_nr = id.replace('button_save_task', "");
            var task_description = $.trim(jQuery('#summernote' + id_nr).parent().find('.note-editable').html());
            // if textarea is null
            if (task_description.length > 0 && task_description != '<br>') {

                if (!$('#user_comment_task' + id_nr).children('div').hasClass('modal_description_task_content')) {

                    $('#user_comment_task' + id_nr).append('<div class="modal_description_task_content"></div>');
                }
                $('#button_add_task' + id_nr).addClass('disabled');
                $('#button_edit_task' + id_nr).removeClass('disabled');
                $('#button_delete_task' + id_nr).removeClass('disabled');
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').show();
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').html(task_description);
                jQuery('#user_comment_task' + id_nr).show();

                var data = {
                    'task_description': task_description,
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'
                };
            }
            else {
                $('#button_add_task' + id_nr).removeClass('disabled');
                $('#button_edit_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).trigger('blur');
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
                jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                var data = {
                    'task_description': '~',
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'
                };
            }
            // save to database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                        alert(msg)
//                        alert(JSON.stringify(data))
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

            $(this).hide();
            jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
            $('#button_add_task' + id_nr).show();
            $('#button_edit_task' + id_nr).show();
            $('#user_comment_task' + id_nr).show();

        });
        // save task details and close modal
        jQuery('.button_save_and_close_task').click(function (event) {

            var id = $(this).attr('id');
            var id_nr = id.replace('button_save_and_close_task', "");
            var task_description = $.trim(jQuery('#summernote' + id_nr).parent().find('.note-editable').html());


            // if textarea is null
            if (task_description.length > 0 && task_description != '<br>') {

                if (!$('#user_comment_task' + id_nr).children('div').hasClass('modal_description_task_content')) {

                    $('#user_comment_task' + id_nr).append('<div class="modal_description_task_content"></div>');
                }

                $('#button_add_task' + id_nr).addClass('disabled');
                $('#button_edit_task' + id_nr).removeClass('disabled');
                $('#button_delete_task' + id_nr).removeClass('disabled');
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').show();
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').html(task_description);
                jQuery('#user_comment_task' + id_nr).show();

                var data = {
                    'task_description': task_description,
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'
                };

            }
            else {
                $('#button_add_task' + id_nr).removeClass('disabled');
                $('#button_edit_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).addClass('disabled');
                $('#button_delete_task' + id_nr).trigger('blur');
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
                $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
                jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                var data = {
                    'task_description': '~',
                    'id_ms_task': id_nr,
                    'id_project': '<?php print $project->id_project ?>'
                };
            }
            // save to database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                        alert(msg)
//                        alert(JSON.stringify(data))
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });

            $('#button_save_task' + id_nr).hide();
            jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
            $('#button_add_task' + id_nr).show();
            $('#button_edit_task' + id_nr).show();
            $('#user_comment_task' + id_nr).show();
            $('#modalEditTask').modal('hide');

        });
        $(".is_not_offer").on('switchChange.bootstrapSwitch', function (event, state) {

            if (state == true) {
                var is_checked = 1;
            }
            else {
                is_checked = 0;
            }
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('is_not_offer', "");


            var data = {
                'is_not_offer': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                            alert(JSON.stringify(data))
//                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });
        // is_not_offerselect box on ckeck
        $(".is_control").on('switchChange.bootstrapSwitch', function (event, state) {

            if (state == true) {
                var is_checked = 1;
            }
            else {
                is_checked = 0;
            }
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('is_control', "");


            var data = {
                'is_control': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                            alert(JSON.stringify(data))
//                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });

    });

</script>
{{--// Attachments --}}
<script type="text/javascript">
    $(document).ready(function () {


        $('.col-attachments').hover(function () {

            var id = jQuery(this).attr('id');
            var id_nr = id.replace('col-attachments', "");
            $('#attachments_edit_buttons' + id_nr).toggle();
        });


        $(".FormDeleteTask").submit(function (event) {
            var x = confirm("Are you sure you want to delete this task?");
            if (x) {
                return true;
            }
            else {

                event.preventDefault();
                return false;
            }

        });


        $(".switchCheckBox").bootstrapSwitch();

        // is_not_offerselect box on ckeck

        $(".is_not_offer").on('switchChange.bootstrapSwitch', function (event, state) {

            if (state == true) {
                var is_checked = 1;
            }
            else {
                is_checked = 0;
            }
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('is_not_offer', "");


            var data = {
                'is_not_offer': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                            alert(JSON.stringify(data))
//                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });
        // is_not_offerselect box on ckeck

        $(".is_control").on('switchChange.bootstrapSwitch', function (event, state) {

            if (state == true) {
                var is_checked = 1;
            }
            else {
                is_checked = 0;
            }
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('is_control', "");


            var data = {
                'is_control': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'

            };
            // save tot database
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
//                            alert(JSON.stringify(data))
//                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });


    });

</script>
<script type="text/javascript">
    //icheck change
    jQuery(document).ready(function () {
        function closeEasyTree() {
            var parent = $(".easy-tree").find('li').not('#project_name');
            var children = parent.find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                parent.children('span').find(
                        ' > span.glyphicon').addClass(
                        'glyphicon-plus-sign').removeClass(
                        'glyphicon-minus-sign');
            }

        }

        $('.loader_back').fadeOut(2000);

//        closeEasyTree();
        jQuery('input.icheck-green').change(function (event) {
            var id = jQuery(this).attr('id');

            var myArray = id.split(' ');
            var id_nr = myArray[1].replace('icheck-green', "");
            var id_circle = myArray[0].replace('id_circle', "");

            var value_circle = jQuery('#circle_tasks_left' + id_circle).html();
            if (jQuery(this).prop('checked')) {
                var is_checked = 1;
                value_circle = parseInt(value_circle) + 1;
                jQuery('#' + id).prop('checked', true);
            } else {
                var is_checked = 0;
                value_circle = parseInt(value_circle) - 1;
                jQuery('#' + id).prop('checked', false);
            }
            jQuery('#circle_tasks_left' + id_circle).html(value_circle);


            var data = {
                'is_checked': is_checked,
                'id_ms_task': id_nr,
                'id_project': '<?php print $project->id_project ?>'
            };
            jQuery.post('{{  route("ajax.update_project_task") }}', data)
                    .done(function (msg) {
                        //                            alert(msg)
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
            event.stopPropagation();
        });
    });
</script>


{{--// Easy TREE--}}
<script type="text/javascript">


    (function ($) {
        $.fn.EasyTree = function (options) {
            var defaults = {
                selectable: true,
                deletable: false,
                editable: false,
                addable: false,
                minOpenLevels: 1,
                i18n: {
                    deleteNull: 'Please select the item you want to delete.',
                    deleteConfirmation: 'Do you want to perform the delete operation?',
                    confirmButtonLabel: 'Confirm',
                    editNull: 'Please select the item you want to edit.',
                    editMultiple: 'You can only edit one item.',
                    addMultiple: 'Please select Add',
                    collapseTip: 'Collapse branch',
                    expandTip: 'Expand branch',
                    selectTip: 'Choose',
                    unselectTip: 'Deselect',
                    editTip: 'Edit',
                    addTip: 'Add',
                    deleteTip: 'Delete',
                    cancelButtonLabel: 'Cancel'
                }
            };


            var warningAlert = $('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var dangerAlert = $('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var createInput = $('<div class="input-group">' +
                    '<input type="text" class="tree-control">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm">Save</button>' +
                    '<button type="button" class="btn ls-red-btn cancel">Cancel</button>' +
                    ' </span>' +
                    ' </div> ');
            var editInput = $('<div class="input-group">' +
                    '<input type="text" class="easy-tree-editor">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm_edit">Save</button>' +
                    '<button type="button" class="btn ls-red-btn cancel_edit">Cancel</button>' +
                    ' </span>' +
                    ' </div> ');
            var createButton = $('<div class="create"><button class="btn ls-light-blue-btn btn-round disabled"><i ' +
                    'class="fa fa-plus"></i></button></div> ');
            var editButton = $('<div class="edit"><button class="btn ls-orange-btn btn-round disabled"><i ' +
                    'class="fa fa-edit"></i></button></div>');
            var deleteButton = $('<div class="remove"><button class="btn  ls-red-btn btn-round disabled"><i ' +
                    'class="fa fa-remove"></i></button></div>');
            var optionBox = '<div class="row">' +
                    '<div class="col-xs-2"></div>' +
                    '<div class="col-xs-10 inside-with-border">' +
                    '<a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onClick="modal_dialog_add_task_new(0)"> ' +
                    '<i class="fa fa-plus"></i> </a>' +
                    '</div>' +
                    '</div>';

            options = $.extend(defaults, options);

            this.each(function () {


                var easyTree = $(this);
                /*              $.each($(easyTree).find('ul > li'), function () {
                 var text;
                 if ($(this).is('li:has(ul)')) {
                 var children = $(this).find(' > ul');
                 $(children).remove();
                 text = $(this).find('span.menu_title').text();
                 if ($(this).hasClass('li_editable')) {
                 $(this).find('span.menu_title').html('<span class="glyphicon glyphicon-minus-sign"></span>' +
                 '<a href="javascript: void(0);">' + text + '</a> ' +
                 '<i class="fa fa-pencil" style="font-size: 13px"></i>');
                 } else {
                 $(this).find('span.menu_title').html('<span class="glyphicon glyphicon-minus-sign"></span><a href="javascript:' +
                 ' void(0);">' + text + '</a>');
                 }
                 $(this).append(children);
                 }
                 else {
                 text = $(this).find('span.menu_title').text();
                 if ($(this).hasClass('li_editable')) {
                 $(this).find('span.menu_title').html('<span class="glyphicon"></span>' +
                 '<a href="javascript: void(0);">' + text + '</a> ' +
                 '<i class="fa fa-pencil " style="font-size: 13px"></i>  ');
                 } else {
                 $(this).find('span.menu_title').html('<span class="glyphicon"></span><a href="javascript:' +
                 ' void(0);">' + text + '</a>');
                 }

                 }
                 }); */


                $(easyTree).find('li:has(ul)').addClass('parent_li').find('span.menu_title').attr('title', options.i18n.collapseTip);

                // add easy tree toolbar dom
                if (options.deletable || options.editable || options.addable) {

                    $(easyTree).find('li > span.menu_title').after('<div class="easy-tree-toolbar"  style="display:none;"></div>');
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(deleteButton);
//                    $(easyTree).prepend('<div class="easy-tree-toolbar" style="display:block;"></div> ');

                }


                // addable
                if (options.addable) {

                    $(easyTree).find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('.create > button').click(function () {

                        var createBlock = $(this).parent();
                        if (!$(createBlock).find('input').hasClass('tree-control')) {
                            $(createBlock).after(createInput);
                        }

                        $(createInput).find('input').focus();


                        $(createInput).find('.confirm').click(function () {
                            if ($(createInput).find('input').val() === '')
                                return;
                            var selected = getSelectedItems();


                            var item = $('<li class="li_editable"><span class="menu_title" id="0">' +
                                    '<span class="glyphicon glyphicon-minus-sign close_tasks"></span>' +
                                    '<a href="javascript: void(0);"> ' +
                                    $(createInput).find('input').val() + '</a> ' +
                                    '<i class="fa fa-exclamation" style="font-size: 13px"></i>' +
                                    '</span>' + optionBox + '</li>');
                            $(item).find('a.addSubmoduleTask').attr('data-id', 1);

                            $(item).find(' > span.menu_title > span').attr('title', options.i18n.collapseTip);
                            $(item).find(' > span.menu_title > a').attr('title', options.i18n.selectTip);


                            if (selected.length <= 0) {
                                $(easyTree).find(' > ul').append($(item));
                                $(item).find('.easy-tree-toolbar').append(createButton);
                                $(item).find('.easy-tree-toolbar').append(editButton);
                                $(item).find('.easy-tree-toolbar').append(deleteButton);
                            } else if (selected.length > 1) {
                                $(easyTree).prepend(warningAlert);
                                $(easyTree).find('.alert .alert-content').text(options.i18n.addMultiple);

                            } else {
                                if ($(selected).hasClass('parent_li')) {
                                    $(selected).find(' > ul').append(item);
                                    $(item).find('span.menu_title').after('<div class="easy-tree-toolbar" style="display:none;"></div>');
                                    $(item).find('.easy-tree-toolbar').append(createButton);
                                    $(item).find('.easy-tree-toolbar').append(editButton);
                                    $(item).find('.easy-tree-toolbar').append(deleteButton);
                                } else {
                                    $(selected).find('div.row').remove();
//                                    $(selected).find('div.create').remove();
                                    $(selected).addClass('parent_li').find('  span.menu_title > span').addClass('glyphicon-minus-sign');
                                    $(selected).append($('<ul></ul>')).find(' > ul').append(item);
                                    $(item).find('span.menu_title').after('<div class="easy-tree-toolbar" style="display:none;"></div>');
                                    $(item).find('.easy-tree-toolbar').append(createButton);
                                    $(item).find('.easy-tree-toolbar').append(editButton);
                                    $(item).find('.easy-tree-toolbar').append(deleteButton);

                                }

                            }
//                            var var_id = $(selected).closest('li').find('.menu_title').attr('id');
//                            var id_nr = var_id.replace('submodule_', "");
//                            alert($(item).find('.menu_title').find('a').html());

                            var parents_list = '';
                            var parents = $(item).find('.menu_title').parents("li");
                            var level_par = parents.length - 1;

                            $(item).find('.row').addClass('margin_left_minus' + String(level_par));
                            for (var i = parents.length - 1; i >= 0; i--) {
                                var var_id = $(parents[i]).find('.menu_title').attr('id');
                                var var_name = $(parents[i]).find('.menu_title').find('a').html();
                                var id_nr = var_id.replace('submodule', "");
                                var name = $.trim(var_name.replace('_', " "));
                                var name = $.trim(name.replace('/', ""));
                                parents_list = parents_list + '_' + name + '_' + id_nr;
                            }
//                            $('#easyTreeStructure').val(parents_list);
                            $(item).find('a.addSubmoduleTask').attr('onClick', "modal_dialog_add_task_new('" + parents_list + "')");
                            $(createInput).find('input').val('');
                            if (options.selectable) {
                                $(item).find('li > span.menu_title > a').attr('title', options.i18n.selectTip);
                                $(item).find(' > span > a').click(function (e) {

                                    var li = $(this).parent().parent();
                                    var span = $(this).parent();

                                    $('.easy-tree-toolbar').not($(li).children('.easy-tree-toolbar')).hide();


                                    if (li.hasClass('li_selected')) {
                                        $(span).find('a').attr('title', options.i18n.selectTip);
                                        $(li).removeClass('li_selected');
                                        $('.easy-tree-toolbar').hide();
                                    }
                                    else {
                                        $(easyTree).find('li.li_selected').removeClass('li_selected');
                                        $(span).find('a').attr('title', options.i18n.unselectTip);
                                        $(li).addClass('li_selected');

                                        if (!$(li).hasClass('not_selectable') || $(li).hasClass('li_editable')) {
                                            $(span).next('.easy-tree-toolbar').show();
                                            $(span).next('.easy-tree-toolbar').append(createButton);
                                            $(span).next('.easy-tree-toolbar').append(editButton);
                                            $(span).next('.easy-tree-toolbar').append(deleteButton);

                                        }

                                    }


                                    if (options.deletable || options.editable || options.addable) {
                                        var selected = getAddableItems();
                                        var li_parents = $(this).parents("li").length;
                                        if (options.addable) {
                                            if ((selected.length <= 0 || selected.length > 1) || li_parents > 4) {
                                                $(easyTree).find('.easy-tree-toolbar .create > button').addClass('disabled');
                                            }
                                            else {
                                                $(easyTree).find('.easy-tree-toolbar .create > button').removeClass('disabled');
                                            }
                                        }

                                        var selected = getEditableItems();
                                        if (options.editable) {
                                            if (selected.length <= 0 || selected.length > 1)
                                                $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                            else
                                                $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                                        }

                                        var selected = getDeletableItems();
                                        if (options.deletable) {
                                            if (selected.length <= 0 || selected.length > 1)
                                                $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                            else
                                                $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                                        }

                                    }

                                    e.stopPropagation();
                                });
                            }
                            $(createInput).remove();
                        });
                        $(createInput).find('.cancel').text(options.i18n.cancelButtonLabel);
                        $(createInput).find('.cancel').click(function () {
                            $(createInput).remove();
                        });
                    });
                }

                // editable
                if (options.editable) {
//           
                    $(easyTree).find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('.edit > button').attr('title', options.i18n.editTip).click(function () {


//                        $(easyTree).find('input.easy-tree-editor').remove();
                        $(easyTree).find('li > span.menu_title > a:hidden').show();
                        var selected = getEditableItems();
                        if (selected.length <= 0) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editNull);
                        }
                        else if (selected.length > 1) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editMultiple);
                        }
                        else {
                            var value = $.trim($(selected).find(' > span.menu_title > a').text());
                            $(selected).find(' > span.menu_title > a').hide();
                            $(selected).find(' > span.menu_title > i').hide();
                            $(selected).find(' > span.menu_title > span').hide();

                            if (!$(selected).find(' > span.menu_title >input').hasClass('easy-tree-editor')) {
                                $(selected).find(' > span.menu_title').append(editInput);
                            }
                            var editor = $(editInput).find(' input.easy-tree-editor');
                            $(editor).val(value);
                            $(editor).focus();
                            $(editInput).find('.confirm_edit').click(function () {

                                if ($.trim($(editor).val() !== '')) {
                                    var editor_val = $(editor).val();

                                    var id = $(selected).find('.menu_title').attr('id');
                                    var id_nr = id.replace('submodule', "");


                                    var data = {
                                        'id_module_structure': id_nr,
                                        'editor_val': editor_val
                                    };

                                    jQuery.post('{{  route("ajax.edit_module_structure") }}', data)
                                            .done(function (msg) {
//                                            alert(JSON.stringify(data))
//                                            alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(JSON.stringify(data));
                                                alert(xhr.responseText);
                                            });


                                    $(selected).find(' > span.menu_title > a').html($(editor).val());
                                    $(editInput).remove();
                                    $(selected).find(' > span.menu_title > a').show();
                                    $(selected).find(' > span.menu_title > i').show();
                                    $(selected).find(' > span.menu_title > span').show();
                                }


                            });
                            $(editInput).find('.cancel_edit').click(function () {
                                $(editInput).remove();
                                $(selected).find(' > span.menu_title > a').show();
                                $(selected).find(' > span.menu_title > i').show();
                                $(selected).find(' > span.menu_title > span').show();
                            });


                        }
                    });
                }

                // deletable
                if (options.deletable) {

                    $(easyTree).find('.easy-tree-toolbar').append(deleteButton);
                    $(easyTree).find('.remove > button').click(function () {
                        var selected = getEditableItems();
                        if (selected.length <= 0) {
//                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.deleteNull);
                        } else {
//                            $(easyTree).prepend(dangerAlert);
                            var x = confirm("Are you sure you want to delete this Module, his Submodules and the " +
                                    "associated tasks?");
                            if (x) {
                                var id = $(selected).find('.menu_title').attr('id');
                                var id_nr = id.replace('submodule', "");
                                var child_id = '';
                                $(selected).find('.menu_title').each(function () {
                                    var id_ch = $(this).attr('id');
                                    var id_nr_ch = id_ch.replace('submodule', "");
                                    child_id = child_id + '_' + String(id_nr_ch);
                                });


                                var data = {
                                    'id_module_structure': id_nr,
                                    'id_project': '<?php print $project->id_project; ?>',
                                    'child_structure': child_id

                                };

                                jQuery.post('{{  route("ajax.delete_module_structure") }}', data)
                                        .done(function (msg) {
//                                            alert(JSON.stringify(data))
//                                            alert(msg)
                                        })
                                        .fail(function (xhr, textStatus, errorThrown) {
                                            alert(JSON.stringify(data));
                                            alert(xhr.responseText);
                                        });
                                $(selected).find(' ul ').remove();
                                if ($(selected).parent('ul').find(' > li').length <= 1) {
                                    $(selected).parents('li').removeClass('parent_li').find(' > span.menu_title > span').removeClass('glyphicon-minus-sign');
                                    $(selected).parent('ul').remove();
                                }
                                $(selected).remove();
                            }


                        }
                    });
                }

                // collapse or expand
                $(easyTree).delegate(' li.parent_li > span.menu_title', 'click', function (e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(':visible')) {
                        children.hide('fast');
                        $(this).attr('title', options.i18n.expandTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-plus-sign')
                                .removeClass('glyphicon-minus-sign');
                    } else {
                        children.show('fast');
                        $(this).attr('title', options.i18n.collapseTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-minus-sign')
                                .removeClass('glyphicon-plus-sign');
                    }
                    e.stopPropagation();
                });

                // selectable, only single select
                if (options.selectable) {

//                    alert('selectable');

                    $(easyTree).find('li > span.menu_title > a').attr('title', options.i18n.selectTip);
                    $(easyTree).find('li > span.menu_title > a').click(function (e) {
                        var li = $(this).parent().parent();

//                        if (li.hasClass('not_selectable')) {
//                            li.find('.row').toggle("fade", 1000);
//                        }
                        $('.easy-tree-toolbar').not($(li).children('.easy-tree-toolbar')).hide();


                        if (li.hasClass('li_selected')) {
                            $(this).attr('title', options.i18n.selectTip);
                            $(li).removeClass('li_selected');
                            $('.easy-tree-toolbar').hide();
                        }
                        else {
                            $(easyTree).find('li.li_selected').removeClass('li_selected');
                            $(this).attr('title', options.i18n.unselectTip);
                            $(li).addClass('li_selected');

                            if (!$(li).hasClass('not_selectable') || $(li).hasClass('li_editable')) {
                                $(this).parent().next('.easy-tree-toolbar').show();
                            }
                        }

                        if (options.deletable || options.editable || options.addable) {
                            var selected = getAddableItems();
                            var li_parents = $(this).parents("li").length;
                            hide_modal_box();
                            if (options.addable) {

                                if ((selected.length <= 0 || selected.length > 1) || li_parents > 4) {
//                                    alert('trues');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').addClass('disabled');
                                }
                                else {
//                                    alert('falses');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').removeClass('disabled');
                                }
                            }
                            var selected = getEditableItems();
                            if (options.editable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                            }
                            var selected = getDeletableItems();
                            if (options.deletable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                            }

                        }

                        e.stopPropagation();

                    });
                }


                var getAddableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_selectable');
                };

                // Get selected items
                var getSelectedItems = function () {
                    return $(easyTree).find('li.li_selected');
                };

                var getEditableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable');
                };

                var getDeletableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable').not('li.li_not_deletable');
                };

            });
        };

    })(jQuery);

</script>
 