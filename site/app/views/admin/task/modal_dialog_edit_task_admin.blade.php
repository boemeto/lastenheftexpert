<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-xs-12 text-right">
      <div class="row">
        {{-- {{ dd($pr_module_task); }} --}}
        <div class="col-md-2 pull-right">
          {{Form::checkbox('is_default',1,$pr_module_task->is_default, ['class'=>'switchCheckBox inline is_default_task','id'=>'is_default_task_'.$pr_module_task->id_ms_task,'data-size'=>"mini"])}}
          <a href="{{URL::to('delete_task_from_structure/'.$pr_module_task->id_ms_task)}}"
             class="button_delete_task btn inline btn-round btn-l btn-block btn-danger"
             data-form="{{$pr_module_task->id_ms_task}}"
             data-toggle="modal" data-target="#confirmDelete"
             data-title="Delete Task"
             data-message="{{trans('validation.confirmation_delete_task')}}">
              <i class="fa fa-trash"></i>
          </a>
        </div>
        <div class="col-md-2 pull-right" style="margin-top: -2px">
          <i class="fa fa-info-circle" title=" {{trans('messages.label_default_task_standard')}}"></i>
        </div>
      </div>
    </div>
    <div class="col-xs-12 mgtp-10 ">
        <div class="title_line"> {{($pr_module_task->is_default == 0)?'<i class="fa fa-bell-o" style="color: #D9534F"></i>':''}}  {{ $task->name_task  }} </div>
    </div>
    <div class="col-md-12">
        <section class="ac-container attachment-section">
            <div id="task-attachments">
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1" class="attachment-label">{{trans('messages.label_sidebar_attachments')}}</label>
                <article class="ac-any attachment-article">
                    <div class="article_content">
                      <div class="">
                        {{Form::open(['action'=>'SoftwareController@upload_file_software', 'class'=>'dropzone dz-clickable','id'=>'dropzoneForm'.$pr_module_task->id_ms_task, 'enctype'=>"multipart/form-data"]) }}
                          {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                          <div class="dz-message">
                              <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                          </div>
                          {{Form::close()}}
                      </div>
                        @if(count($attachments)>0)
                            <div class="modal-attachments">
                                <div class="row">
                                    @foreach($attachments as $attachment)
                                        <?php    $icon = Software_attach::getIconByAttachment($attachment->extension) ?>
                                        <div class="col-xs-3 col-attachments"
                                             id="col-attachments{{$attachment->id_software_attachment}}">
                                            <div style="display: block; width: 100%">
                                                <a href="{{ Url::to('/')}}/images/attach_software/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                   download class="button_download_file" target="_blank">
                                                    <div class="file_attachments" style="background: {{$icon}} "
                                                         id="div_file_{{$attachment->id_software_attachment}}"
                                                         title="{{$attachment->name_attachment }}">
                                                    </div>
                                                </a>
                                                <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment->id_software_attachment}}">
                                                    <a href="{{ Url::to('/')}}/images/attach_software/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                       class="button_download_file" target="_blank">
                                                        <i class="fa fa-search"></i>
                                                    </a><br>
                                                    <a href="javascript:void(0)"
                                                       id="button_delete_file{{$attachment->id_software_attachment}}"
                                                       class="button_delete_file">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
{{ Form::model($pr_module_task,['route'=>['task.update',$pr_module_task->fk_task], 'method'=>'PATCH'])}}
<div class="row">
    <div class="col-md-12">
        <section class="ac-container description-section">
            <div id="task_description">
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2" class="description-label">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-4 border-pd5">
                                {{Form::radio('edit_task',2,true,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_current_task')}}
                            </div>
                            <div class="col-xs-4 col-xs-offset-2 border-pd5">
                                {{Form::radio('edit_task',1,false,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_all_tasks')}}
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_task',  $task->getAttribute('name_task','de'), ['class'=>"form-control","placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'), "required",
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','de')}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_task_en', $task->getAttribute('name_task','en'), ['class'=>"form-control","placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                      "required",
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"]) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','en')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mgtp-10">
                            {{ Form::text('code_task',  $task->code_task, ['class'=>"form-control","placeholder"=> trans('messages.nav_task_code') ]) }}
                        </div>
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-3 border-pd5">
                                {{Form::checkbox('is_checked' ,1, $pr_module_task->is_checked,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                            </div>
                            <div class="col-xs-3 col-xs-offset-3 border-pd5">
                                {{Form::checkbox('is_mandatory' ,1, $pr_module_task->is_mandatory,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="lvl_priority" id="task_pair" class="task-priority-pair">
                                    <option value="">{{trans('messages.label_priority')}}</option>
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->lvl_priority}}" {{($pr_module_task->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                    $selected_pair = Task_pair::getFirstById($pr_module_task->fk_pair);
                                    if (count($selected_pair) == 1) {
                                        $selected_color = $selected_pair->color_pair;
                                    } else {
                                        $selected_color = '#fff';
                                    }
                                ?>
                                <select name="fk_pair" id="task_pair" class="task-priority-pair" style="background: {{$selected_color}}">
                                    <option value="0" style=" background: #fff">{{trans('messages.label_pair')}} {{trans('messages.nav_task')}}</option>
                                    @foreach($pairs as $pair)
                                        <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($pr_module_task->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="tab-content tab-border select_industry">
                            @foreach($industries_types as $industry_type)
                                <?php  $industries = Industry::getIndustryByType($industry_type->id_industry_type);?>
                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">
                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <hr>
                                    @foreach($industries as $industry)
                                        <div style="display: block">
                                            <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$pr_module_task->id_ms_task}}"
                                                   {{(Industry_ms::isIndustryChecked($industry->id_industry, $pr_module_task->id_ms_task) == true)?'checked':''}}
                                                   name="fk_industry[]" value="{{$industry->id_industry}}" style="display: inline-block"> {{$industry->name_industry}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac4">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$task->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$task->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
        {{Form::hidden('id_ms_task' ,$id_ms_task)}}
        <button type="submit" class="btn ls-light-blue-btn btn-block">
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>
{{Form::close()}}
<script type="text/javascript">
    $('.button_delete_task').on('click', function(e) {
          e.preventDefault();
          var title = $(this).attr('data-title');
          var msg = $(this).attr('data-message');
          var dataForm = $(this).attr('data-form');
          var url = $(this).attr('href');

        $('#formConfirm')
          .find('#frm_body').html(msg)
          .end().find('#frm_title').html(title)
          .end().modal('show');

          $('#formConfirm').find('#frm_submit').attr('href', url);
    });
</script>
<script type="text/javascript">
  $('.button_delete_file').on('click', function(e) {
     e.preventDefault();
     $('.confirm_button_delete_file').removeClass('disabled');
     var data_id = $(this).attr('id');
     var url = $(this).attr('href');
     var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
     var msg = "Delete attachment " + title + "?";

     $('#formConfirm').find('#frm_body').html(msg).end().modal('show');
     $('#frm_submit').addClass('confirm_button_delete_file').attr('data_id', data_id);

     $('.confirm_button_delete_file').click(function (event) {
        var id = jQuery(this).attr('data_id');
        var id_nr = id.replace('button_delete_file', '');
        var data = {
            'id_software_attachment': id_nr
        };
        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
          .done(function (msg) {
              if (parseInt(msg) > 0) {
                  var attached_files, new_val, id_ms_task;
                  id_ms_task = parseInt(msg);
                  attached_files = $('#attachment_counter' + id_ms_task).html();
                  new_val = parseInt(attached_files) - 1;
                  $('#attachment_counter' + id_ms_task).html(new_val);
              }
          })
          .fail(function (xhr, textStatus, errorThrown) {
              alert(xhr.responseText);
          });
       jQuery('#col-attachments' + id_nr).hide();
       $('#formConfirm').modal('hide');
      });
});
</script>
<script type="text/javascript">
    $('.attachment-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) + 50;
            $('#task_description').css('margin-bottom', task_margin_bottom+'px');
            $(this).removeClass('is_closed');
        } else {
            var heightAttachments = $('.attachment-label').parent().find('article:first').height();
            $('#task_description').css('margin-bottom','50px');
            $(this).attr('data-task-margin', heightAttachments);
            $(this).addClass('is_closed');
       }
    });

    $('.description-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) ;
            $('#task_description').css('margin-bottom', task_margin_bottom+'0px');
            $(this).removeClass('is_closed');
            var attachmentHeight = $('.attachment-section').height();
            setTimeout(function() {
                $('.attachment-section').css('top', $('#task_description').outerHeight());
                    var attachmentHeight = $('.attachment-section').height();
                    $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
        } else {
            $('#task_description').css('margin-bottom','0px');
            $(this).addClass('is_closed');
            setTimeout(function() {
                var topHeight = $('#task_description').outerHeight() + 5;
                $('.attachment-section').css('top', topHeight);
                var attachmentHeight = $('.attachment-section').height();
                $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
       }
  });
</script>
