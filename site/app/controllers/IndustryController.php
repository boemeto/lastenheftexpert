<?php
use Illuminate\Support\MessageBag;

class IndustryController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        // helper table - for 1, 0 - Yes , No
        $yesNo = Dictionary::getOptions();
        // get the industries, paginated
        $industry_types = Industry_type::all();

        // sort by name

        return View::make('admin.industries.index', compact('yesNo', 'industry_types'));
    }

    public function index_tabs()
    {
        // helper table - for 1, 0 - Yes , No
        $yesNo = Dictionary::getOptions();
        // get the industries, paginated
        $industry_types = Industry_type::all();

        // sort by name

        return View::make('admin.industries.index_with_tabs', compact('yesNo', 'industry_types'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        allowOnlySuperAdmin();
        $industry_types = Industry_type::getIndustry_types();

        return View::make('admin.industries.create', compact('industry_types'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $industry = new Industry;
        $industry->fill(Input::all());
        $industry->save();
        Session::flash('scrollTo', 'industry'.$industry->id_industry);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.add_industry_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        allowOnlySuperAdmin();
        $yesNo = Dictionary::getOptions();
        $priorities = Priority::getAllPriorities();
        $industry = Industry::getFirstById($id);
        $software = Module_structure::getSoftwareByIndustry($id);
        $projects = Module_structure::getProjectByIndustry($id);
        $pairs = Task_pair::all();
        $custom_task = Dictionary::getOptions(4, 1);
        $set_checked = Dictionary::getOptions(5, 1);
        $set_attach = Dictionary::getOptions(6, 1);
        $has_filters = 0;
        $export_file = Project_export_files::getLastByIndustryId($id);

        if (Input::has('meta_tags')) {
            $meta_tags = Input::get('meta_tags');
        } else {
            $meta_tags = '';
        }
        if (Input::has('address_code')) {
            $address_code = Input::get('address_code');
        } else {
            $address_code = '';
        }
        if (Input::has('fk_pair')) {
            $has_filters = 1;
            $fk_pair = Input::get('fk_pair');
        } else {
            $fk_pair = 0;
        }
        if (Input::has('fk_priority')) {
            $has_filters = 1;
            $fk_priority = Input::get('fk_priority');
        } else {
            $fk_priority = 0;
        }
        if (Input::has('fk_checked')) {
            $has_filters = 1;
            $fk_checked = Input::get('fk_checked');
        } else {
            $fk_checked = '';
        }
        if (Input::has('fk_attach')) {
            $has_filters = 1;
            $fk_attach = Input::get('fk_attach');
        } else {
            $fk_attach = '';
        }
        if (Input::has('is_default')) {
            $has_filters = 1;
            $is_default = Input::get('is_default');
        } else {
            $is_default = 0;
        }

        return View::make('admin.industries.show', compact('projects', 'software', 'industry', 'yesNo', 'pairs', 'fk_pair',
          'address_code', 'meta_tags', 'priorities', 'fk_priority', 'custom_task', 'set_checked', 'set_attach', 'fk_checked',
          'fk_attach', 'is_default', 'export_file', 'has_filters'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $yesNo = Dictionary::getOptions();
        $industry = Industry::whereId_industry($id)->first();
        $industry_type = Industry_type::getIndustry_types();
        $consultants = Consultant::getAllConsultants();
        $consult_industry = Consultant::getAllConsultants($id);

        return View::make('admin.industries.edit', compact('industry', 'yesNo', 'industry_type', 'consultants', 'consult_industry'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $industries = Industry::getFirstById($id);
        if (Input::has('deactivate') && Input::get('deactivate') == 1) {
            if ($industries->is_deleted == 1) {
                $industries->is_deleted = 0;
            } else {
                $industries->is_deleted = 1;
            }

            $industries->save();
        }

        $industry_consultants = Consultant::getAllConsultants($id);
        $consultants = Input::get('fk_consultant');
        $cons_int = array();
        foreach ($consultants as $consultant) {
            if ($consultant != 0) {
                $ind_consultant = Industry_consultant::getFirstByKey($id, $consultant);
                if (count($ind_consultant) == 0) {
                    $ind_consultant = new Industry_consultant();
                    $ind_consultant->fk_industry = $id;
                    $ind_consultant->fk_consultant = $consultant;
                    $ind_consultant->save();
                }
                $cons_int[] = intval($consultant);
            }
        }

        foreach ($industry_consultants as $id_consultant => $name_consultant) {
            if (!in_array($id_consultant, $cons_int)) {
                $ind_consultant = Industry_consultant::getFirstByKey($id, $id_consultant);
                $ind_consultant->delete();
            }
        }
        $industries->fill(Input::all());
        $industries->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $industries = Industry::getFirstById($id);
        $industries->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.delete_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function edit_module_s_admin()
    {
        $id_module_structure = Input::get('id_module_structure');
        $editor_val = trim(Input::get('editor_val'));
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);
        if (count($pr_module_structure) == 1) {
            $id = $pr_module_structure->fk_id;
            $type = $pr_module_structure->id_type;
            if ($type == 1) {
                $module = Module::getFirstById($id);
                if (count($module) == 1) {
                    $module->name_module = $editor_val;
                    $module->save();
                }
            } elseif ($type == 2) {
                $submodule = Submodule::getFirstById($id);
                if (count($submodule) == 1) {
                    $submodule->name_submodule = $editor_val;
                    $submodule->save();
                }
            }
        }
        return 'Edited successfully';
    }

    public function download_industry_excel($id_industry) {
        allowOnlySuperAdmin();
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);

        // if ($project->is_empty_project == 1) {
        $industry = Industry::getFirstById($id_industry);
        $software = Module_structure::getSoftwareByIndustry($id_industry);
        $projects = Module_structure::getProjectByIndustry($id_industry);

        $export_file = new Project_export_files();
        $export_file->file_name = $industry->name_industry.'.xls';
        $export_file->file_type = 'xls';
        $export_file->industry_id = $id_industry;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        // Prepare File
        $data = array(
            'user' => $user->toArray(),
            'industry' => $industry->toArray(),
            'id_industry' => $id_industry,
            // 'module_structure' => $module_structure,
            'software' => $software->toArray(),
            'projects' => $projects->toArray(),
        );

        $excelFile = Excel::create($industry->name_industry, function($excel) use ($data) {
            $excel->sheet('1', function($sheet) use ($data) {
                $sheet->cells('A1:Z1', function($cells) {
                    $cells->setBackground('#808080');
                    $cells->setFontColor('#ffffff');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });

                Session::forget('task_cell');
                Session::put('task_cell', 1);
                Session::forget('excel_rows');
                Session::put('excel_rows', 2);
                Session::forget('excel_files');
                Session::put('excel_files', 0);

                foreach($data['software'] as $soft) {
                    $id_software = $soft['id_software'];
                    $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software);
                    $newRow = Session::get('excel_rows') + 1;
                    Module_structure::printTreeEXCELDetailsIndustryTaskCell($module_structure, $id_software, $data['id_industry'],  1, [], 1, [], $sheet);
                }

                $mod = [];
                for($i = 1; $i < Session::get('task_cell')+1; $i++) {
                // for($i = 1; $i < 6; $i++) {
                    $mod[] = 'EBENE '.$i;
                }
                $mod[] = 'AUFGABE';
                $mod[] = 'BESCHREIBUNG';
                $mod[] = 'GEWICHTUNG';
                for($i = 1; $i < Session::get('excel_files')+1; $i++) {
                    $mod[] = $i . '. ANALGE';
                }
                $sheet->row(1, $mod);
                Session::forget('excel_rows');
                Session::put('excel_rows', 2);
                foreach($data['software'] as $soft) {
                    $id_software = $soft['id_software'];
                    $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software);
                    $newRow = Session::get('excel_rows') + 1;
                    if($newRow % 2 == 1) {
                        $cells = 'A'.$newRow.':'.'Z'.$newRow;
                        $sheet->cells($cells, function($cells) {
                            $cells->setBackground('#e2efd7');
                        });
                    }
                    Session::put('excel_rows', $newRow);
                    $sheet->row($newRow, array($soft['name_software']));
                    Module_structure::printTreeEXCELDetailsIndustry($module_structure, $id_software, $data['id_industry'], 1, [], 1, [], $sheet);
                }
            });
        })->export('xls');
    }

    // download industry attachments in zip format
    public function download_industry_attachments($id_industry) {
        allowOnlySuperAdmin();
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $industry = Industry::getFirstById($id_industry);
        $software = Module_structure::getSoftwareByIndustry($id_industry);
        $projects = Module_structure::getProjectByIndustry($id_industry);
        $data = array(
            'user' => $user->toArray(),
            'industry' => $industry->toArray(),
            'id_industry' => $id_industry,
            'software' => $software->toArray(),
            'projects' => $projects->toArray(),
        );
        Session::forget('check_for_attachments');

        foreach($data['software'] as $soft) {
            $id_software = $soft['id_software'];
            $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software);
            Module_structure::getIndustryAttachmentsZip($module_structure, $id_software, $data['id_industry'], 1, 'check');
        }

        if(Session::has('check_for_attachments')) {
            $file = tempnam("tmp", "zip");
            $zip = new ZipArchive();
            $zip->open($file, ZipArchive::OVERWRITE);
            Session::forget('task_cell_attachments');
            Session::put('task_cell_attachments', 1);
            Session::forget('rows_attachments');
            Session::put('rows_attachments', 2);
            Session::forget('rows_attachments');
            Session::put('rows_attachments', 2);

            foreach($data['software'] as $soft) {
                $id_software = $soft['id_software'];
                $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software);
                Module_structure::getIndustryAttachmentsZip($module_structure, $id_software, $data['id_industry'], 1, $zip);
            }
            // die;
            $zip->close();
            header('Content-Type: application/zip');
            header('Content-Length: ' . filesize($file));
            header('Content-Disposition: attachment; filename="' . $industry->name_industry . '_attachments.zip"');
            readfile($file);
            unlink($file);
        } else {
            return Redirect::back();
        }
    }

    public function upload_file_industry($data)
    {
        if(Input::has('id_industry_ms')) {
            $fk_industry_ms = Input::get('id_industry_ms');
        } else {
            $data = explode('_', $data);
            $fk_industry_ms = $data[0];
            $fk_industry = $data[1];
        }

        $file = Input::file('file');
        // check if is file
        if ($file) {
            $folder = date('m-Y');
            $filename = $file->getClientOriginalName();
            $extension = end(explode('.', $filename));
            $directory = 'images/attach_software/' . $folder . '/';
            // Industry_attach::addAttachToAllIndustry($fk_industry_ms, $folder, $directory, $filename, $extension);
            Industry_attach::addAttachToSelectedIndustry($fk_industry_ms, $fk_industry, $folder, $directory, $filename, $extension);
        } else {
            return 0;
        }
    }

    public function remove_file_industry()
    {
        if (Input::has('id_industry_attachment')) {
            $id_industry_attachment = Input::get('id_industry_attachment');
            $attachment = Industry_attach::getFirstById($id_industry_attachment);
            if (count($attachment) > 0) {
                $fk_industry_ms = $attachment->fk_industry_ms;
                $attachment->delete();
                return $fk_industry_ms;
            }
        } else {
            $fk_industry_ms = Input::get('id_industry_ms');
            $file_name = Input::get('delete_file');
            $folder = date('m-Y');
            $attachment = Industry_attach::where('fk_industry_ms', '=', $fk_industry_ms)
                ->where('folder_attachment', 'like', $folder)
                ->where('name_attachment', 'like', $file_name)->first();
            if (count($attachment) == 1) {
                $fk_industry_ms = $attachment->fk_industry_ms;
                $attachment->delete();
                return $fk_industry_ms;
            }
        }
        return 0;
    }

    public function download_industry_pdf($id_industry)
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $industry = Industry::getFirstById($id_industry);
        $softwares = Module_structure::getSoftwareByIndustry($id_industry);
        // echo "<pre>", dd($industry->name_industry);

        // $module_structure = Module_structure::getModuleByLvl($id_industry, 1, 0, 0);
        $company = Company::getFirstById($user->fk_company);

        $export_file = new Project_export_files();
        $export_file->file_name = $industry->name_industry.'.pdf';
        $export_file->file_type = 'pdf';
        $export_file->industry_id = $id_industry;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();
        $view = View::make('admin.industries.download_industry_pdf',
            compact('user', 'industry', 'company',  'softwares'));

        $timestamp = time();
        File::put(public_path() . '/app/storage/'.$timestamp.'_pdf.html', $view);

        $options = array(
            'disable-smart-shrinking'
        );
        $pdf = new mikehaertl\wkhtmlto\Pdf($options);
        $pdf->binary = 'wkhtmltopdf';

        $pdf->addPage(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

        $return =  $pdf->send();
        unlink(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

        if (!$return) {
            throw new Exception('Could not create PDF: '.$pdf->getError());
        } else {
            return $return;
        }
    }

    public function download_industry_word($id_industry)
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $industry = Industry::getFirstById($id_industry);
        $softwares = Module_structure::getSoftwareByIndustry($id_industry);
        // echo "<pre>", dd($industry);

        // $module_structure = Module_structure::getModuleByLvl($id_industry, 1, 0, 0);
        $company = Company::getFirstById($user->fk_company);

        $export_file = new Project_export_files();
        $export_file->file_name = $industry->name_industry.'.pdf';
        $export_file->file_type = 'pdf';
        $export_file->industry_id = $id_industry;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $view = View::make('admin.industries.download_industry_word',
            compact('company', 'user', 'industry', 'softwares', 'id_software', 'module_structure'));

        $timestamp = time();
        File::put(public_path() . '/app/storage/'.$timestamp.'_word.html', $view);

        $file= public_path(). '/app/storage/'.$timestamp.'_word.html';

        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        );

        return Response::download($file, $industry->name_industry.'.doc', $headers)->deleteFileAfterSend(true);
    }
}
