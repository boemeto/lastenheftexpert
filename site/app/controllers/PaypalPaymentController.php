<?php

class PaypalPaymentController extends BaseController {

    /**
     * object to authenticate the call.
     * @param object $_apiContext
     */
    private $_apiContext;

    /**
     * Set the ClientId and the ClientSecret.
     * @param
     *string $_ClientId
     *string $_ClientSecret
     */
    private $_ClientId = 'AQ5ouSQaDsGiEdRu_jKm9rmGYxyg3F6UibX4deOVoFtg4u0-AkXyn_S3BC3vCf7EDpfPTLNLMH13vXhd';
    private $_ClientSecret='EB6k3biCzF85fazZSqSrXJmnNFsC4IH4gcfAD1ze6SDCesCVKEW1S1ol2QBjjJu2JRC4qBZ8eyjDZpWw';

    /*
     *   These construct set the SDK configuration dynamiclly,
     *   If you want to pick your configuration from the sdk_config.ini file
     *   make sure to update you configuration there then grape the credentials using this code :
     *   $this->_cred= Paypalpayment::OAuthTokenCredential();
    */
    public function __construct()
    {

        // ### Api Context
        // Pass in a `ApiContext` object to authenticate
        // the call. You can also send a unique request id
        // (that ensures idempotency). The SDK generates
        // a request id if you do not pass one explicitly.

        $this->_apiContext = Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);

    }

    /*
    * Display form to process payment using credit card
    */
    public function create()
    {
        return View::make('payment.order');
    }

    public function index()
    {
        echo "<pre>";

        $payments = Paypalpayment::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);
        dd($payments);
    }

    public function show($payment_id)
    {
       $payment = Paypalpayment::getById($payment_id,$this->_apiContext);
       dd($payment);
    }

    /*
    * Process payment using credit card
    */
    public function store()
    {
        $data = Input::all();

        $id_package = Input::get('id_package');
        $package = Package::getFirstById($id_package);
        $paypalPrice = $package->price*1.19;
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");

        $item1 = Paypalpayment::item();
        $item1->setName('Create Project ' . $data['name_project'])
                ->setDescription('Project package ' . $package->name_package)
                ->setCurrency('EUR')
                ->setQuantity('1')
                ->setPrice($paypalPrice);

        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1));

        $details = Paypalpayment::details();
        $details->setSubtotal($paypalPrice);

        $amount = Paypalpayment::amount();
        $amount->setCurrency("EUR")
            ->setTotal($paypalPrice)
            ->setDetails($details);

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $baseUrl = "http://erp3/";
        $redirectUrls = Paypalpayment::redirectUrls();
        $redirectUrls->setReturnUrl(action('ProjectsController@storePaypal', ['data' => $data]));
        // dd($id_package);
        // Session::put($id_package);
        $redirectUrls->setCancelUrl(action('ProjectsController@getPaymentPaypalStatus'));

        $payment = Paypalpayment::payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_apiContext);
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        $approvalUrl = $payment->getApprovalLink();
        echo "Created Payment Using PayPal. Please visit the URL to Approve.Payment <a href={$approvalUrl}>{$approvalUrl}</a>";

        if(isset($approvalUrl)) {
            // redirect to paypal
            return Redirect::away($approvalUrl);
        }
    }

    public function extendProjectWithPaypal()
    {
        $project_id = Input::get('project_id');
        $project = Project::getFirstById($project_id);
        $subpackage_id = Input::get('subpackage_id');
        $subpackage = Subpackages::getFirstByPackageId($subpackage_id);
        $paypalPrice = $subpackage->subpackage_price*1.19;

        $data = ['project_id' => $project_id, 'subpackage_id' => $subpackage_id];
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");

        $item1 = Paypalpayment::item();
        $item1->setName('Extend Project')
                ->setDescription('Extend period for project ' . $project->name_project . ' with ' . $subpackage->subpackage_period . ' months.')
                ->setCurrency('EUR')
                ->setQuantity('1')
                ->setTax($paypalPrice)
                ->setPrice($paypalPrice);

        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1));

        $details = Paypalpayment::details();
        $details->setSubtotal($paypalPrice);

        $amount = Paypalpayment::amount();
        $amount->setCurrency("EUR")
            ->setTotal($paypalPrice)
            ->setDetails($details);

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $baseUrl = "http://erp3/";
        $redirectUrls = Paypalpayment::redirectUrls();
        $redirectUrls->setReturnUrl(action('PurchasesController@postCheckoutPaypal', ['data' => $data]));
        $redirectUrls->setCancelUrl(action('PurchasesController@getPaymentPaypalStatus', ['id' => $project_id]));

        $payment = Paypalpayment::payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_apiContext);
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        $approvalUrl = $payment->getApprovalLink();
        // echo "Created Payment Using PayPal. Please visit the URL to Approve.Payment <a href={$approvalUrl}>{$approvalUrl}</a>";

        if(isset($approvalUrl)) {
            // redirect to paypal
            return Redirect::away($approvalUrl);
        }
    }
}
