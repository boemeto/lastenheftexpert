<?php
use Illuminate\Support\MessageBag;

class SessionsController extends \BaseController
{

    // check if user is logged in, return login form
    public function index($confirm_user)
    {
        //if user logged in, enter the site
        if (Auth::check()) {

            $id_user = Auth::user()->id;
            $user_groups = User_groups::getGroupByUser($id_user);
            $user = User::getFirstById($id_user);
            $company = Company::getFirstById($user->fk_company);
            $projects = Project::getUsersProjects($id_user, 5);
            $users_company = User::getAllUsersDetailsByCompany($user->fk_company, 5);
            $headquarters = Company_headquarters::getHeadquartersLocations($user->fk_company, 2);
            $invoices = Invoice::getInvoicesByCompany($user->fk_company, null, 3);
            // dd($users_company);
            if (in_array('2', $user_groups) || in_array('3', $user_groups)) {
                return View::make('front.home', compact('user', 'company', 'projects', 'invoices', 'users_company', 'headquarters', 'user_groups'));
            }
        }

        //if user not logged in, go to login page
        return View::make('front.sessions.index', compact('confirm_user'));
    }

    public function newUserLogin() {
        return View::make('front.sessions.newUserLogin');
    }

    public function checkCompanyEmail() {
        // dd(Input::all());
        $checkEmail = Input::get('emailCheck');
        $result = Company::getCompanyByEmail($checkEmail);
        $errors = 'Diese E-Mail ist bereits registriert, bitte geben Sie eine andere ein.';
        if (count($result)>0) {
            return Response::json($errors, 422);
        }
    }

    public function checkHeadquarterEmail() {
        // dd(Input::all());
        $checkEmail = Input::get('emailCheck');
        $result = Company_headquarters::getHeadquarterByEmail($checkEmail);
        $errors = 'Diese E-Mail ist bereits registriert, bitte geben Sie eine andere ein.';
        if (count($result)>0) {
            return Response::json($errors, 422);
        }
    }

    public function checkUserEmail() {
        // dd(Input::all());
        $checkEmail = Input::get('emailCheck');
        $result = User::getUserByEmail($checkEmail);
        $errors = 'Diese E-Mail ist bereits registriert, bitte geben Sie eine andere ein.';
        if (count($result)>0) {
            return Response::json($errors, 422);
        }
    }

    // check the authentication
    public function check_login()
    {
        // validators
        $errors = new MessageBag;
        $rules = array(
            'email_'    => 'required|min:4|email', // username must be at least 4 characters
            'password_' => 'required|min:4'  // password must be at least 4 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)// send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            // create our user data for the authentication
            $errors = 'Login failed';
            try {
                // Login credentials
                $credentials = array(
                    'email'    => Input::get('email_'),
                    'password' => Input::get('password_')
                );
                // Authenticate the user
                $user = Sentry::authenticate($credentials, false);
            } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
                $errors = 'Login field is required.';
            } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
                $errors = 'Password field is required.';
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
                $errors = 'Wrong password, try again.';
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                $errors = 'User was not found.';
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
                $errors = 'User is not activated.';
            } // The following is only required if the throttling is enabled
            catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
                $errors = 'User is suspended.';
            } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
                $errors = 'User is banned.';
            }

            if (Auth::attempt($credentials) && count($user) > 0 && $user->activated == 1 && $user->blocked != 1) {
                $lang = $user->language;

                // if  company is not approved yet
                $id_company = $user->fk_company;
                $company = Company::getFirstById($id_company);
                if ($company->is_blocked == 1) {
                    Auth::logout();
                    $errors = new MessageBag(['error_' => [Lang::get('validation.company_blocked')]]);
                    return Redirect::back()->withErrors($errors)
                        ->withInput(Input::except('password_'));
                }

//                // if user is blocked by company admin
//                if ($user->blocked == 1) {
//                    Auth::logout();
//                    $errors = new MessageBag(['error_' => [Lang::get('validation.admin_activation')]]);
//                    return Redirect::back()->withErrors($errors)
//                        ->withInput(Input::except('password_'));
//                }

                // save every login action
                $user_actions = new User_actions();
                $user_actions->fk_user = $user->id;
                $user_actions->fk_action = 1; // login action
                $user_actions->save();

                // open site in preferred language
                Session::put('lang', $lang);

                $user_groups = User_groups::getGroupByUser($user->id);
                if (count($user_groups) == 1) {
                    if ($user_groups[0] == 1) {
                        // if user is super admin
                        return Redirect::to('admin_home');
                    }
                    if ($user_groups[0] == 2 || $user_groups[0] == 3) {
                        // if simple user or company admin
                        return Redirect::to('/');
                    }
                } else {
                    return Redirect::to('sessions/' . $user->id);
                }

            } else {
                if (Auth::attempt(array('email' => Input::get('email_'), 'password' => Input::get('password_')))) {
                    $user = Auth::user();
                    if (!empty($user) && $user->is_deleted) {
                        // if user account has been rejected
                        $errors = new MessageBag(['error_' => ['Your account has been rejected']]);
                    } elseif(!empty($user) && $user->activated == 0) {
                        // if user account is not activated
                        $errors = new MessageBag(['error_' => ['Please validate your account']]);
                    } elseif (!empty($user) && $user->blocked) {
                        // if decizion about user has not been made by admin
                        $errors = new MessageBag(['error_' => ['Please wait until an admin accept or reject you request']]);
                    }
                } else {
                    // if email or password is incorrect
                    $errors = new MessageBag(['error_' => [Lang::get('validation.login')]]);
                }
                Auth::logout();
                return Redirect::back()->withErrors($errors)
                    ->withInput(Input::except('password_'));
            }
        }
    }

    public function show($id)
    {
        $user_groups = User_groups::getGroupByUser($id);
        return View::make('front.sessions.show', compact('user_groups'));
    }

    // display register page
    public function create()
    {
        //if user logged in
        if (Auth::check()) {
            return Redirect::to('/logout');
        }
        // get all companies
        $companies = Company::all();
        $searched_company = '';
        if (Input::has('searched_company')) {
            $searched_company = Input::get('searched_company');
        }
        $titles = Users_title::getTitle();
        $personal_titles = Users_personal_title::getPersonal_Title();
        //if user not logged in
        return View::make('front.sessions.create', compact('titles', 'personal_titles', 'searched_company', 'companies'));
    }

    // function used in register -> ajax, for displaying the headquarters list
    public function viewHeadquarters($id)
    {
        // get all headquarters for the select field
        $headquarters = Company_headquarters::getHeadquartersSelect($id);
        return View::make('front.sessions.viewHeadquarters', compact('headquarters'));
    }

    // function used in register -> ajax, for displaying the headquarters details
    public function viewHeadquarterDetails($id)
    {
        // get details for the chosen headquarter
        $headquarter = Company_headquarters::getHeadquartersById($id);
        return View::make('front.sessions.viewHeadquarterDetails', compact('headquarter'));
    }

// return home view for super admin users
    public function admin_home()
    {
        if (Auth::check()) {
            $id_user = Auth::user()->id;
            $user = User::getFirstById($id_user);
            $company = Company::getFirstById($user->fk_company);
            $projects = Project::getUsersProjects($id_user, 6);
            $users_company = User::getAllUsersDetailsByCompany($user->fk_company, 6);
            $headquarters = Company_headquarters::getHeadquartersLocations($user->fk_company, 3);
            $invoices = Invoice::getInvoicesByCompany($user->fk_company, null, 3);
            $id_user = Auth::user()->id;
            $user_groups = User_groups::getGroupByUser($id_user);

            if (in_array('1', $user_groups)) {
                return View::make('admin.home', compact('user', 'company', 'projects', 'invoices', 'users_company', 'headquarters', 'user_groups'));
            } else {
                return Redirect::route('sessions.index');
            }
        }
        //if user not logged in, redirect to login
        return Redirect::route('sessions.index');
    }

    /**
     * Activate User Form, activation by mail
     *
     * @return Response
     */

    public function activation()
    {
        // get the hash code from link
        $activation_code = Input::get('hash');
        $id_user = Input::get('user');

        // check if the values are correct
        $user = User::whereActivation_code($activation_code)->first();
        if (count($user) == 0) {
            $errors = new MessageBag(['email_' => [Lang::get('validation.no_user_found')]]);
            $searched_company = '';
            return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
        }
        // if user is already activated, return message
        if ($user->activated == 1) {
            $errors = new MessageBag(['confirm_' => [Lang::get('validation.already_activated')]]);
            return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
        } else {
            // activate the user
            $user->activated = 1;
            $user->activated_at = date('Y-m-d H:i:s');
            $user->save();
            $errors = new MessageBag(['confirm_' => [Lang::get('validation.confirm_activation')]]);
            // redirect to login
            return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
        }
    }

    public function acceptUser($id) {
        $user = Auth::user();
        if($user) {
            $user_groups = User_groups::getGroupByUserAndCompany($user->id, $user->fk_company);
            $user = User::findOrFail($id);
            if($user->is_deleted ) {
                $errors = new MessageBag(['error_' => [Lang::get('validation.user_already_deleted')]]);
                return Redirect::action('SessionsController@index')->withErrors($errors);
            } elseif ($user->blocked == 0) {
                $errors = new MessageBag(['error_' => [Lang::get('validation.user_already_accepted')]]);
                Session::flash('go-to', URL::to('users/'.$id));
                return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
            } elseif($user_groups['fk_group'] == 1 || $user_groups['fk_group'] == 2) {
                $user->blocked = 0;
                $user->save();
                $headquarter = Company_headquarters::findOrFail($user->fk_headquarter);
                $headquarter->is_blocked = 0;
                $headquarter->save();
                Mail::send('emails.account_accepted', $user, function ($message) use ($user) {
                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
                });
                $errors = new MessageBag(['confirm_' => [Lang::get('validation.accept_user')]]);
                Session::flash('go-to', URL::to('users/'.$id));
                return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
            } else {
                $errors = new MessageBag(['error_' => [Lang::get('validation.access_denied')]]);
            }
        } else {
            $errors = new MessageBag(['error_' => [Lang::get('validation.log_for_accept_user')]]);
        }
        return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
    }

    public function declineUser($id) {
        if(Auth::user()) {
            $user_groups = User_groups::where('fk_user', '=', Auth::user()->id)->get()->first();
            $user = User::findOrFail($id);
            if($user->is_deleted ) {
                $errors = new MessageBag(['error_' => [Lang::get('validation.user_already_deleted')]]);
                return Redirect::action('SessionsController@index')->withErrors($errors);
            } elseif ($user->blocked == 0) {
                $errors = new MessageBag(['error_' => [Lang::get('validation.user_already_accepted')]]);
                Session::flash('go-to', URL::to('users/'.$id));
                return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
            } elseif($user_groups['fk_group'] == 1 || $user_groups['fk_group'] == 2) {
                $user->is_deleted = 1;
                $user->save();
                Mail::send('emails.account_rejected', $user, function ($message) use ($user) {
                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activated!');
                });
                $errors = new MessageBag(['confirm_' => [Lang::get('validation.rejected_user')]]);
                return Redirect::action('SessionsController@index')->withErrors($errors);
            } else {
                $errors = new MessageBag(['error_' => [Lang::get('validation.access_denied')]]);
            }
        } else {
            $errors = new MessageBag(['error_' => [Lang::get('validation.log_for_accept_user')]]);
        }
        return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
    }

    public function employeesList($id) {
        if(Auth::user()) {
            Session::flash('go-to', URL::to('users/'.$id));
            return Redirect::action('CompanyController@show', 'employees')->withErrors($errors);
        } else {
            $errors = new MessageBag(['error_' => [Lang::get('validation.login_first')]]);
        }
        return Redirect::action('SessionsController@index', compact('searched_company'))->withErrors($errors);
    }


    /**
     * Approve pending members
     *
     * @param  int $id
     * @return Response
     */
    public function change($id)
    {
        User::changeUser($id);
        return Redirect::action('UsersController@show', $id);
    }

    public function change_to_admin()
    {
        if(Session::has('super-admin-id'))
        {
            Auth::loginUsingId(Session::get('super-admin-id'));
            Session::forget('super-admin-id');
            return Redirect::action('SessionsController@admin_home');
        }
        else
        {
            return Redirect::back();
        }
    }

    /**
     * Logout
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        // logout user
        Auth::logout();
        Session::flush();
        // redirect to login
        return Redirect::route('sessions.index');
    }

    public function getForgotPassword() {
        return View::make('front.sessions.resetPassword');
    }

    public function postForgotPassword(){
        $errors = new MessageBag;
        $rules = array(
            'email'            => 'required|email|min:4',
            'email_confirm'    => 'required|same:email|min:4',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator);// send back all errors to the login form
        } else {
            $user = User::getUserByEmail(Input::get('email'));
            if($user) {
                $user = User::findOrFail($user[0]->id);
                $user->reset_password_code = str_random(30);
                $user->save();
                $user->link_reset_password = url() . "/recover-password/" . $user->reset_password_code;
                $data = [
                    'user' => $user
                ];
                Mail::send('emails.forget_password', $data, function ($message) use ($user) {
                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Forget Password!');
                });
                $errors = new MessageBag(['confirm_' => [Lang::get('validation.email_reset_password')]]);
                return Redirect::back()->withErrors($errors);
            }
            $errors = new MessageBag(['error_' => [Lang::get('validation.user_not_found')]]);
            return Redirect::action('SessionsController@getForgotPassword')->withErrors($errors);
        }
    }

    public function recoverPassword($token) {
        return View::make('front.sessions.recover', compact('token'));
    }

    public function postRecoverPassword($token) {
        $errors = new MessageBag;
        $rules = array(
            'password'         => 'required|min:4',
            'password_confirm' => 'required|same:password|min:4',
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors = new MessageBag(['error_' => 'password did not match']);
            return Redirect::back()->withErrors($errors);// send back all errors to the login form
        } else {
            $user = User::getUserByRememberToken($token);
            if($user) {
                $user = User::findOrFail($user[0]->id);
                $user->password = Hash::make(Input::get('password'));
                $user->save();
                $errors = new MessageBag(['confirm_' => 'password ok']);
                return Redirect::back()->withErrors($errors);
                // return Redirect::action('SessionsController@index')->withErrors($errors);
            }
            $errors = new MessageBag(['error_' => [Lang::get('validation.user_not_found')]]);
            return Redirect::action('SessionsController@index')->withErrors($errors);
        }
    }
}
