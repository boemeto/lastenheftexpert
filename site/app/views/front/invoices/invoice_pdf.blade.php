<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="language" content="DE">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">
    {{--<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ URL::asset('assets/css/theme.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">--}}
    <title>{{trans('messages.title_long')}}</title>
    <style type="text/css">


        body {
            font-family: Helvetica;
            background: #ffffff;

            font-size: 13px;
        }

        table th {
            text-align: left;
        }

        .table-responsive {
            width: 100%;
        }

        .address {
            display: block;
            margin: 0;
            font-style: normal;
        }

        .footer_invoice {
            font-size: 10px;
            color: #999999;
        }

        .logo-invoice img {
            height: 66px;
            float: right;
        }

        table td {
            vertical-align: middle;
        }

        .table-invoice td {
            font-size: 12px;
        }

        .admin_details_invoice {
            font-size: 10px;
            text-decoration: underline;
        }

        .mgtp-20 {
            margin-top: 20px;
        }

        .mgtp-10 {
            margin-top: 10px;
        }

        .mgbt-10 {
            margin-bottom: 10px;
        }

        .mgbt-20 {
            margin-bottom: 20px;
        }

        .text-right {
            text-align: right;
        }

        p {
            line-height: 14px;
            margin-top: 10px;
            margin-bottom: 5px;
        }

        .footer {
            position: fixed;
            bottom: 70px;
        }

        .table_packages td, .table_packages th {
            padding: 5px;
        }

        .invoice-template .table_packages th {
            font-weight: bold;
            font-size: 15px;
        }

    </style>
</head>
<body>
<div class="invoice-template">
    <div class="row">
        <div class="mgbt-20 logo-invoice " style="text-align: right">
            <?php $image = Admin_company::getLogoCompany($admin_company->id_company);  ?>
            <img src="{{ URL::asset($image)}}" alt="image" align="right">
        </div>
    </div>
    <div class="mgbt-20 mgtp-20 admin_details_invoice">
        <br> {{$admin_company->name_company}} | Inh. Marius Toader - Bärenschanzstr. 83 - 90429 Nürnberg
    </div>

    <table>
        <tr class="table-responsive">
            <td style="width: 400px">
                <div class="address">
                    {{$company->name_company}}<br>
                    {{Users_personal_title::getUserPersonalTitle($user->id)}}
                    {{Users_title::getUserTitle($user->id)}}
                    {{$user->first_name }} {{$user->last_name}}<br>
                    {{$main_headquarter->street}} {{$main_headquarter->street_nr}}<br>
                    {{$main_headquarter->postal_code}} {{$main_headquarter->city}} <br>
                    {{$main_headquarter->country}} <br>
                </div>
            </td>
            <td style="text-align: right;" class="text-right">
                <table class="table-invoice" style="width: 300px">
                    <tr>
                        <th>{{trans('messages.label_invoice_nr')}}</th>
                        <th class="text-right">{{$invoice->inv_serial}} - {{$invoice->inv_number}} / {{date('Y', strtotime($invoice->inv_date))}}</th>
                    </tr>
                    <tr>
                      {{-- <pre>{{dd($invoice)}} --}}
                        <td>{{trans('messages.label_invoice_project')}}:</td>
                        <td class="text-right">{{date('d.m.Y', strtotime($invoice->initial_date))}} - {{date('d.m.Y', strtotime($invoice->end_date))}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('messages.label_invoice_date')}}</td>
                        <td class="text-right">{{date('d.m.Y', strtotime($invoice->inv_date))}} </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>{{trans('messages.project_nr')}}</td>
                        <td class="text-right">{{getProjectId($project->id_project)}} </td>
                    </tr>
                    <tr>
                        <td>{{trans('messages.label_client_code')}}</td>
                        <td class="text-right">{{getClientId($company->id_company)}}</td>
                    </tr>
                    <tr>
                        <td>{{trans('messages.label_company_cui')}}</td>
                        <td class="text-right">{{$company->cui_company}} </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="row">
        <div class=" mgtp-20 mgbt-20">
            <div class="row">
                <div class="col-xs-12">
                    <h2>{{trans('messages.nav_invoice')}}</h2>
                </div>
                <div class="col-xs-12">
                    <div class="mgtp-10 mgbt-10">
                        Sehr geehrte{{($user->fk_personal_title == 2)?'r ':' '}}
                        {{Users_personal_title::getUserPersonalTitle($user->id)}}
                        {{Users_title::getUserTitle($user->id)}}
                        {{$user->first_name}} {{$user->last_name}},
                    </div>
                    für die erbrachten Leistungen bei der Pflege und Weiterentwicklung Ihrer Webplattformen in den Monaten {{date('m Y', strtotime($project->created_at))}} , erlauben wir uns Ihnen folgende Position in Rechnung zu stellen:
                </div>
            </div>
        </div>
        <table class="table-responsive table_packages mgbt-20">
            <tr style="background: #F0F0F0;">
                <th>{{trans('messages.label_position')}}</th>
                <th>{{trans('messages.label_product_name')}}</th>
                <th style="text-align: center;">{{trans('messages.label_quantity')}}</th>
                <th style="text-align: center;">{{trans('messages.label_unit_price')}}</th>
                <th style="text-align: center;">{{trans('messages.label_subtotal')}}</th>
            </tr>
            <tr style="line-height: normal">
                <td style="text-align: center;">1</td>
                <td>
                  {{date('d.m.Y', strtotime($project->created_at)) != date('d.m.Y', strtotime($invoice->inv_date)) ?trans('messages.package_renewal'):''}} {{trans('messages.nav_package')}} {{$package->name_package}} {{trans('messages.until')}} {{date('d.m.Y', strtotime($project->due_date))}}
                  <br>
                  ({{trans('messages.label_project')}}: {{$project->name_project}})
                </td>
                <td style="text-align: center;">1.</td>
                <td style="text-align: center;">
                  <b>
                    {{formatToGermanyPrice( $invoice->unit_price )}}
                    <i class="fa fa-eur"></i> €
                  </b>
                </td>
                <td class="pd-10 pull-right" style="text-align: right; margin-top: 3px">
                  <span class="font-normal">
                    <b>
                      {{formatToGermanyPrice( $invoice->unit_price )}}
                      <i class="fa fa-eur"></i> €
                    </b>
                  </span>
                </td>
            </tr>
            <tr style="background: #F0F0F0;">
                <td></td>
                <td class="pd-10" colspan="3"><b>{{trans('messages.label_total_pay')}}</b></td>
                <td class="pd-10 pull-right" style="text-align: right;">
                  <span class="font-normal">
                    <b>
                      {{formatToGermanyPrice( $invoice->unit_price )}}
                      <i class="fa fa-eur"></i> €
                    </b>
                  </span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="pd-10" colspan="3">{{trans('messages.label_tva_sum')}}</td>
                <?php $tva = 0.19 * $invoice->unit_price;
                $total = $invoice->unit_price + $tva;
                ?>
                <td class="pd-10 pull-right" style="text-align: right;">
                  <span class=" font-normal">
                    <b>
                      {{formatToGermanyPrice($tva )}}
                      <i class="fa fa-eur"></i> €
                    </b>
                  </span>
                </td>
            </tr>
            <tr style="background: #F0F0F0;">
                <td></td>
                <td class="pd-10" colspan="3"><span class="font-sm font-normal">{{trans('messages.label_total_sum')}}</span></td>
                <td class="pd-10 pull-right" style="text-align: right;">
                  <span class="font-sm font-normal">
                    <b>
                      {{formatToGermanyPrice($total)}}
                      <i class="fa fa-eur"></i> €
                    </b>
                  </span>
                </td>
            </tr>
        </table>
        <div class="mgtp-20 mgbt-20">
            <p>
                Zahlungsbedingungen: Zahlung innerhalb von 14 Tagen ab Rechnungseingang ohne Abzüge.</p>
            <p>
                Bitte uberweisen Sie den gesamten Betrag unter Angabe der Rechnungsnummer als Verwendungszweck
                auf das unten angegebene Konto.
            </p>
            <p>
                Uber weitere Aufträge bzw. Weiterempfehlungen Ihrerseits würde wir uns sehr freuen.</p>
            <p> Vielen Dank für Ihr Vertrauen und die gute Zusammenarbeit.</p>
            <p class="mgtp-20">
                Mit freundlichen Grüßen
            </p>
            <p>
                Ihr {{$admin_company->name_company}} Team
            </p>
        </div>
    </div>
    <div class="footer">
        <table class=" footer_invoice mgtp-10 table-responsive">
            <tr>
                <td>
                    <div>  {{$admin_company->name_company}} | {{$admin_company->name_user}}</div>
                    <div>  {{$admin_company->street}}  </div>
                    <div>  {{$admin_company->zip_code}} {{$admin_company->city}}</div>
                    <div>  {{$admin_company->country}}  </div>
                </td>
                <td>
                    <div> Tel: {{$admin_company->telephone}}  </div>
                    <div> Fax: {{$admin_company->fax}}  </div>
                    <div> {{trans('messages.label_email')}}:
                        <a href="mailto:{{$admin_company->email}}" target="_blank">
                            {{$admin_company->email}} </a></div>
                    <div> Web: <a href="{{$admin_company->web}}" target="_blank"> {{$admin_company->web}} </a></div>
                </td>
                <td>
                    <div> USt.-ID: {{$admin_company->company_code}}  </div>
                    <div> Steuer-Nr: {{$admin_company->company_number}}  </div>
                    <div> Inhaber: {{$admin_company->name_user}}  </div>
                </td>
                <td>
                    <div>  {{$admin_company->bank}}  </div>
                    <div> IBAN: {{$admin_company->iban}}  </div>
                    <div> BIC: {{$admin_company->bic}}  </div>
                </td>
            </tr>
        </table>
    </div>
</div>

</body>
</html>
