<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 14.05.2015
 * Time: 15:51
 */
class Company_headquarters extends Eloquent
{


    public static $rules = [

    ];
    public static $messages = [];
    public $primaryKey = 'id_headquarter';
    protected $table = 'company_headquarters';
    protected $guarded = ['id_headquarter'];
    protected $fillable = ['name_headquarter', 'email_headquarter', 'telephone_headquarter', 'fax_headquarter',
        'social_xing', 'social_linkedin', 'social_twitter', 'is_main', 'fk_company', 'fk_location', 'blocked',];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getHeadquarterByEmail($email)
    {
        $list = Company_headquarters::whereEmail_headquarter($email)->first();
        return $list;
    }

    public static function getHeadquartersLocations($id_company = null, $limit = null)
    {
        if ($id_company == null) {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->orderBy('id_headquarter', 'desc')
                ->get();
        } else {
          if($limit != null) {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->whereFk_company($id_company)
                ->orderBy('id_headquarter', 'desc')
                ->limit($limit)
                ->get();
          } else {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->whereFk_company($id_company)
                ->orderBy('id_headquarter', 'desc')
                ->get();
          }
        }
        return $headquarters;
    }

    public static function getHeadquartersLocationsNoBlocked($id_company = null)
    {
        if ($id_company == null) {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->where('company_headquarters.is_blocked', '=', 0)
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->orderBy('is_main', 'desc')
                ->get();
        } else {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->where('company_headquarters.is_blocked', '=', 0)
                ->whereFk_company($id_company)
                ->orderBy('is_main', 'desc')
                ->get();
        }
        return $headquarters;
    }

    public static function getMainHeadquarter($id_company, $false = null)
    {

        $headquarters = DB::table('company_headquarters')
            ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
            ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
            ->whereFk_company($id_company)
            ->where('is_main', '=', '1')
            ->first();


        return $headquarters;
    }

    public static function getHeadquartersById($id_headquarter = null)
    {
        if ($id_headquarter == null) {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->get();
        } else {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->whereId_headquarter($id_headquarter)
                ->first();
        }
        return $headquarters;
    }

    public static function getHeadquartersSelect($id_company = null, $is_first_row = 0)
    {
        $array = array();
        if ($id_company == null) {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->get();
        } else {
            $headquarters = DB::table('company_headquarters')
                ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
                ->select('*', 'company_headquarters.created_at', 'company_headquarters.updated_at')
                ->whereFk_company($id_company)
                ->get();
        }
        if ($is_first_row == 1) {
            $array[0] = 'Filiale';
        }
        foreach ($headquarters as $headquarter) {
            $array[$headquarter->id_headquarter] = $headquarter->name_headquarter . ', ' . $headquarter->street
                . ', ' . $headquarter->city . ', ' . $headquarter->country;
        }

        return $array;
    }

    public static function getCompanyByHeadquarter($id)
    {
        $headquarters = Company_headquarters::getFirstById($id);

        return $headquarters->fk_company;
    }

    public static function getFirstById($id)
    {
        $list = Company_headquarters::whereId_headquarter($id)->first();
        return $list;
    }


}
