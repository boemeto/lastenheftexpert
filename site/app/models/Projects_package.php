<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 19-Aug-16
 * Time: 12:54
 */
class Projects_package extends Eloquent
{
    public $primaryKey = 'id_project_package';
    protected $table = 'pr_projects_packages';
    protected $guarded = ['id_project_package'];
    protected $fillable = ['fk_project', 'fk_package'];

    public static function getFirstById($id)
    {
        $list = self::where('id_project_package', '=', $id)->first();
        return $list;
    }

    public static function getCustomItem($id_project, $id_package)
    {
        $customPackage = self::where('fk_project', '=', $id_project)->where('fk_package', '=', $id_package)->first();
        return $customPackage;
    }

    public static function getCurrentPackage($id)
    {
        $package = self::where('fk_package', '=', $id)->first();
        return $package;
    }
}
