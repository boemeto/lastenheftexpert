<?php

return array(
    // set your paypal credential
    'client_id' => 'AQ5ouSQaDsGiEdRu_jKm9rmGYxyg3F6UibX4deOVoFtg4u0-AkXyn_S3BC3vCf7EDpfPTLNLMH13vXhd',
    'secret' => 'EB6k3biCzF85fazZSqSrXJmnNFsC4IH4gcfAD1ze6SDCesCVKEW1S1ol2QBjjJu2JRC4qBZ8eyjDZpWw',
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
