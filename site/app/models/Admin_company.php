<?php

class Admin_company extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_company' => 'required',
    ];
    public $primaryKey = 'id_company';
    protected $table = 'admin_company';
    protected $guarded = ['id_company'];
    protected $fillable = ['name_company', 'logo_company', 'name_user', 'street', 'city', 'country', 'zip_code', 'email',
        'telephone', 'fax', 'web', 'iban', 'bic', 'bank', 'company_code', 'company_number'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = self::where('id_company', '=', $id)->first();
        return $list;
    }

    public static function getLogoCompany($id)
    {
        $list = self::whereId_company($id)->first();
        if ($list->logo_company != '') {
            $logo = 'images/image_company/' . $list->logo_company;
        } else {
            $logo = 'images/image_company/company_default.png';
        }
        return $logo;
    }


}