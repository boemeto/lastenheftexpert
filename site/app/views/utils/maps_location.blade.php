<script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.


    var autocomplete = {};

    function initialize(n) {
        geolocate(n);
        autocomplete[n] = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */
                (document.getElementById('autocomplete' + n)),
                {types: ['geocode']});
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete[n], 'place_changed', function () {
            fillInAddress(n);
        });
    }

    function fillInAddress(n) {
        // Get the place details from the autocomplete object.

        var place = autocomplete[n].getPlace();


        //   alert(JSON.stringify(place));

        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            switch (addressType) {
                case 'street_number' :
                    document.getElementById('street_number' + n).value = place.address_components[i]['short_name'];
                    break;
                case 'route' :
                    document.getElementById('route' + n).value = place.address_components[i]['long_name'];
                    break;
                case 'locality' :
                    document.getElementById('locality' + n).value = place.address_components[i]['long_name'];
                    break;
                case 'administrative_area_level_1' :
                    document.getElementById('administrative_area_level_1' + n).value = place.address_components[i]['long_name'];
                    break;
                case 'country' :
                    document.getElementById('country' + n).value = place.address_components[i]['long_name'];
                    break;
                case 'postal_code' :
                    document.getElementById('postal_code' + n).value = place.address_components[i]['short_name'];
                    break;
            }

        }
        var longitude = place.geometry.location['F'];
        var latitude = place.geometry.location['A'];
        document.getElementById('latitude' + n).value = latitude;
        document.getElementById('longitude' + n).value = longitude;

    }

    function geolocate(n) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete['n'].setBounds(circle.getBounds());

            });

        }
    }

</script>