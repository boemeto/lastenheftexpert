@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <style type="text/css">
        p {
            line-height: 14px;
            margin: 0 0 10px 0 !important;
        }
    </style>

@stop
@section('content')
    <div class="panel widget light-widget panel-bd-top  ">
        <div class="panel-heading no-title"></div>
        <div class="panel-body" style="padding:40px;">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs 12">
                    <div class="invoice-template">
                        <div class="row mgbt-20">
                            <div class="mgbt-20 logo-invoice">
                                <?php $image = Admin_company::getLogoCompany($admin_company->id_company);  ?>
                                <img src="{{ URL::asset($image)}}" alt="image">
                            </div>
                        </div>
                        <div class="mgbt-20 admin_details_invoice">
                            <br> {{$admin_company->name_company}} | {{$admin_company->street}} - {{$admin_company->zip_code}} {{$admin_company->city}}
                        </div>
                        <div class="row">
                            <div class=" col-lg-7 col-xs-7">
                                <address>
                                    {{$company->name_company}}<br>
                                    {{Users_personal_title::getUserPersonalTitle($user->id)}}
                                    {{Users_title::getUserTitle($user->id)}}
                                    {{$user->first_name }} {{$user->last_name}}<br>
                                    {{$main_headquarter->street}} {{$main_headquarter->street_nr}}<br>
                                    {{$main_headquarter->postal_code}} {{$main_headquarter->city}} <br>
                                    {{$main_headquarter->country}} <br>
                                </address>
                            </div>
                            <div class="col-lg-5 col-xs-5">
                                <table class="table table-responsive table-no-padding table-invoice">
                                    <tr>
                                        <th>{{trans('messages.label_invoice_nr')}}</th>
                                        <th class="text-right">{{$invoice->inv_serial}} - {{$invoice->inv_number}} / {{date('Y', strtotime($invoice->inv_date))}}</th>
                                    </tr>
                                    <tr>
                                      <?php
                                          $duration = $package->period;
                                          $initialDate = date('d.m.Y', strtotime($invoice->initial_date));
                                          $effectiveDate = date('d.m.Y', strtotime($invoice->end_date));
                                      ?>
                                        <td>{{trans('messages.label_invoice_project')}}:</td>
                                        <td class="text-right">{{$initialDate}} - {{$effectiveDate}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('messages.label_invoice_date')}}</td>
                                        <td class="text-right">{{date('d.m.Y', strtotime($invoice->inv_date))}} </ td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('messages.project_nr')}}</td>
                                        <td class="text-right">{{getProjectId($project->id_project)}} </td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('messages.label_client_code')}}</td>
                                        <td class="text-right">{{getClientId($company->id_company)}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{trans('messages.label_company_cui')}}</td>
                                        <td class="text-right">{{$company->cui_company}} </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" mgbt-20">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h1>{{trans('messages.nav_invoice')}}</h1>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="mgtp-10 mgbt-10">
                                            {{($user->fk_personal_title == 2)? trans('messages.label_dear_m'): trans('messages.label_dear') }}
                                            {{Users_personal_title::getUserPersonalTitle($user->id)}}
                                            {{Users_title::getUserTitle($user->id)}}
                                            {{$user->first_name}} {{$user->last_name}},
                                        </div>
                                        für die erbrachten Leistungen bei der Pflege und Weiterentwicklung
                                        Ihrer Webplattformen in den Monaten
                                        {{date('m Y', strtotime($project->created_at))}} ,
                                        erlauben wir uns Ihnen folgende Position in Rechnung zu stellen:
                                    </div>
                                </div>
                            </div>
                            <table class="table table-condensed table_packages ">
                                <tr style="background: #F0F0F0;">
                                    <th>{{trans('messages.label_position')}}</th>
                                    <th>{{trans('messages.label_product_name')}}</th>
                                    <th class="text-center">{{trans('messages.label_quantity')}}</th>
                                    <th class="text-center">{{trans('messages.label_unit_price')}}</th>
                                    <th class="pull-right">{{trans('messages.label_subtotal')}}</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>
                                      {{date('d.m.Y', strtotime($project->created_at)) != date('d.m.Y', strtotime($invoice->inv_date)) ?trans('messages.package_renewal'):''}} {{trans('messages.nav_package')}} {{$package->name_package}} {{trans('messages.until')}} {{$effectiveDate}}
                                      <br>
                                      ({{trans('messages.label_project')}}: {{$project->name_project}})
                                    </td>
                                    <td class="text-center">1.</td>
                                    <td class="text-center">
                                      <b>{{formatToGermanyPrice( $invoice->unit_price )}} </b>
                                      <i class="fa fa-eur"></i>
                                    </td>
                                    <td class="pd-10 pull-right" style="margin-top: 3px">
                                        <span class="font-normal"><b>{{formatToGermanyPrice( $invoice->unit_price )}} </b><i class="fa fa-eur"></i></span>
                                    </td>
                                </tr>
                                <tr style="background: #F0F0F0;">
                                    <td></td>
                                    <td class="pd-10" colspan="3"><b>{{trans('messages.label_total_pay')}}</b></td>
                                    <td class="pd-10 pull-right">
                                        <span class="font-normal"><b>{{formatToGermanyPrice($invoice->unit_price)}} </b>
                                            <i class="fa fa-eur"></i>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="pd-10" colspan="3">{{trans('messages.label_tva_sum')}}</td>
                                    <?php $tva = 0.19 * $invoice->unit_price;
                                    $total = $invoice->unit_price + $tva;
                                    ?>
                                    <td class="pd-10 pull-right">
                                      <span class="font-normal">
                                        <b>{{formatToGermanyPrice($tva)}}</b>
                                        <i class="fa fa-eur"></i>
                                      </span>
                                    </td>
                                </tr>
                                <tr style="background: #F0F0F0;">
                                    <td></td>
                                    <td class="pd-10" colspan="3"><span class="font-sm font-normal">{{trans('messages.label_total_sum')}}</span></td>
                                    <td class="pd-10 pull-right">
                                        <span class="font-sm font-normal">
                                          <b>{{formatToGermanyPrice( $total)}} </b>
                                            <i class="fa fa-eur"></i>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <div class="mgtp-20 mgbt-20">
                                <p>
                                    Zahlungsbedingungen: Zahlung innerhalb von {{$invoice->payment_term}} Tagen ab Rechnungseingang ohne Abzüge.</p>
                                <p>
                                    Bitte uberweisen Sie den gesamten Betrag unter Angabe der Rechnungsnummer als Verwendungszweck
                                    auf das unten angegebene Konto.
                                </p>
                                <p>
                                    Uber weitere Aufträge bzw. Weiterempfehlungen Ihrerseits würde wir uns sehr freuen.</p>
                                <p> Vielen Dank für Ihr Vertrauen und die gute Zusammenarbeit.</p>
                                <p class="mgtp-20">
                                    Mit freundlichen Grüßen
                                </p>
                                <p>
                                    Ihr {{$admin_company->name_company}} Team
                                </p>
                            </div>
                        </div>
                        <div class="row footer_invoice mgtp-10">
                            <div class="col-lg-3 col-sm-3 col-xs-6">
                                <div>  {{$admin_company->name_company}} | {{$admin_company->name_user}}</div>
                                <div>  {{$admin_company->street}}  </div>
                                <div>  {{$admin_company->zip_code}} {{$admin_company->city}}</div>
                                <div>  {{$admin_company->country}}  </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-xs-6">
                                <div> Tel: {{$admin_company->telephone}}  </div>
                                <div> Fax: {{$admin_company->fax}}  </div>
                                <div> {{trans('messages.label_email')}}:
                                    <a href="mailto:{{$admin_company->email}}" target="_blank">
                                        {{$admin_company->email}} </a></div>
                                <div> Web: <a href="{{$admin_company->web}}" target="_blank"> {{$admin_company->web}} </a></div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-xs-6">
                                <div> USt.-ID: {{$admin_company->company_code}}  </div>
                                <div> Steuer-Nr: {{$admin_company->company_number}}  </div>
                                <div> Inhaber: {{$admin_company->name_user}}  </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-xs-6">
                                <div>  {{$admin_company->bank}}  </div>
                                <div> IBAN: {{$admin_company->iban}}  </div>
                                <div> BIC: {{$admin_company->bic}}  </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-xs-12"></div>
                <div class="col-lg-3 col-md-3 col-sm-8 col-xs-12 " style="background-color: #FCFCFC; min-height: 1000px; padding: 30px ">
                    <div class="row">
                        <div class="col-lg-12 col-md-12  col-sm-5 col-xs-5 ">
                            <div class="title_line">
                                {{trans('messages.invoice_details')}}
                            </div>
                            <div class="row invoice-right">
                                <div class="col-xs-12">
                                    <span class="label label-{{Invoice::getInvoiceStatusColor($invoice->id_invoice)}}">{{Invoice::getInvoiceStatus($invoice->id_invoice)}}</span>
                                </div>
                            </div>
                            <div class="row invoice-right mgtp-10">
                                <div class=" col-lg-6 col-xs-12">
                                    <i class="fa fa-file-text-o"></i> {{trans('messages.label_invoice_nr')}}:
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    <span> {{$invoice->inv_serial}} - {{$invoice->inv_number}} / {{date('Y', strtotime($invoice->inv_date))}}</span>
                                </div>
                            </div>
                            <div class="row invoice-right">
                                <div class=" col-lg-6 col-xs-12">
                                    <i class="fa fa-building-o"></i> {{trans('messages.label_client')}}:
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    <span> {{$company->name_company}}</span>
                                </div>
                            </div>
                            <div class="row invoice-right">
                                <div class="col-lg-6 col-xs-12">
                                    <i class="fa fa-sitemap"></i> {{trans('messages.label_project_name')}}:
                                </div>

                                <div class="col-lg-6 col-xs-12">
                                    <span>   {{$project->name_project}}</span>
                                </div>
                            </div>
                            <div class="row invoice-right">
                                <div class="col-lg-6 col-xs-12">
                                    <i class="fa fa-eur"></i> {{trans('messages.label_balance')}}:
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                  <span>
                                    {{$invoice->fk_status==3?"0,00 ": formatToGermanyPrice($total)}}
                                    <i class="fa fa-euro"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="row invoice-right">
                                <div class="col-lg-6 col-xs-12">
                                    <i class="fa fa-clock-o"></i> {{trans('messages.label_payment_time')}}:
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    <span> {{$invoice->payment_term}} {{trans('messages.label_days')}} </span>
                                </div>
                            </div>
                            <div class="row invoice-right">
                                <div class="col-lg-6 col-xs-12">
                                    <i class="fa fa-credit-card"></i> {{trans('messages.label_payment_method')}}:
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    <span>
                                      @if ($invoice->payment_method=="card")
                                         {{trans('messages.label_creditcard')}}
                                      @endif
                                    </span>
                                    <span>
                                      ({{trans('messages.label_on')}} {{date('d.m.Y', strtotime($invoice->inv_date))}})
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 mgtp-20">
                                    <a href="{{URL::to('invoice_pdf/'.$invoice->id_invoice)}}" target="_blank" class="btn btn-block dld_pdf" type="button" style="padding: 3px 0px;">
                                        <i class="fa fa-download append-icon"></i>{{trans('messages.save_as_pdf')}}</a>
                                </div>
                                <div class="last_dld">
                                    {{$invoice->last_dld!="0000-00-00"?"Zuletzt heruntergeladen am: " . date('d.m.Y', strtotime($invoice->last_dld)):''}}
                                </div>
                                <div class="col-xs-12 mgtp-10">
                                    <hr>
                                </div>
                            </div>
                            @if(count($invoices) > 0)
                                <div class="row">
                                    <div class=" col-xs-12">
                                        <div class="title_line mgtp-10 ">
                                            {{trans('messages.project_invoices')}}
                                        </div>
                                        <div class="row invoice-right mgtp-10">
                                            @foreach($invoices as $invoice_row)
                                                @if( $invoice_row->id_invoice != $invoice->id_invoice )
                                                    <div class=" col-xs-12">
                                                        <a href="{{URL::to('invoice/'.$invoice_row->id_invoice)}}">
                                                            {{$invoice_row->inv_serial}} -
                                                            {{$invoice_row->inv_number}} / {{date('Y', strtotime($invoice_row->inv_date))}}
                                                            <span class="label pull-right other-invoices label-{{Invoice::getInvoiceStatusColor($invoice_row->id_invoice)}}">{{Invoice::getInvoiceStatus($invoice_row->id_invoice)}}</span>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
