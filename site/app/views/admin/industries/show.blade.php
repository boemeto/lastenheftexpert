@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('header_scripts')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/selectize.bootstrap3.css')}}">

@stop
@section('footer_scripts')

    <script src="{{ URL::asset('assets/js/industry/dropzone.js')}}"></script>
    <!-- Module menu tree style -->
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
    @include('admin/industries/tree_script')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('assets/js/software/software_scripts.js')}}"></script>
    <script src="{{ URL::asset('assets/js/industry/industry_scripts.js')}}"></script>

    @include('admin/scripts/ajax_scripts')

    <script type="text/javascript">
        $(document).on('change', 'select.form-control', function() {
            var str = $('#pair_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0 && (str.toLowerCase().indexOf("aus­wäh­len") >= 0 || str.toLowerCase().indexOf("select") >= 0)) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-color-button', function() {
            var pair_color = $('#pair_color').text();
            if ((pair_color.toLowerCase().indexOf("select") >= 0 || pair_color.toLowerCase().indexOf("aus­wäh­len") >= 0) && $('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).on('click', '.select-colors', function() {
            if (($(this).children('a[data-name="- Select -"]').length != 0 || $(this).children('a[data-name="- Aus­wäh­len -"]').length != 0) && $('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }
        });

        $(document).ready(function () {
            var str = $('#pair_color').text();
            if($('.priority-filter').val() == 0 && $('.attach-filter').val() == 0 && $('.default-filter').val() == 0 && (str.toLowerCase().indexOf("aus­wäh­len") >= 0 || str.toLowerCase().indexOf("select") >= 0)) {
                $('.filter-buttons').hide();
                $('.filter-buttons-inactive').show();
            } else {
                $('.filter-buttons').show();
                $('.filter-buttons-inactive').hide();
            }

            hide_empty_modules();
            //notification if user wants to download attachments and industry doesn't have any tasks with attachments
            $('.get-attachments-zip').on('click', function(e){
                var countProjectAttachments = document.getElementsByClassName('attachment_counter');
                var sum = 0;
                for(var i=0; i < countProjectAttachments.length; i++) {
                   sum += parseInt(countProjectAttachments[i].innerHTML);
                }
                if(sum === 0) {
                   e.preventDefault();
                   var msg = "<?php echo trans('messages.no_attachments'); ?>";
                   $('#formConfirmOk')
                     .find('#frm_body').html(msg)
                     .end().find('#frm_title').html('Info')
                     .end().modal('show');
                }
            });
        });

        function select_pair_color(pair_element) {
            $('[name=fk_pair]').val($(pair_element).attr('data-id'));
            $('#pair_color').text($(pair_element).attr('data-name'));
            $('.pair-color-preview').css({'background-color': $(pair_element).attr('data-color')});
        };

        function hide_empty_modules() {
            $(".parent_li").not('.software').not('.industry').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }
            });
            $(".li_not_selectable").not('.software').not('.industry').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }

            });
        }
        // edit an existing task
        function modal_dialog_edit_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_task_industry/" + id + "/" + '<?php print $industry->id_industry ?>';
                var url = "{{ action('IndustryController@upload_file_industry', ":id")}}";
                var fk_industry = <?php print $industry->id_industry ?>;
                url = url.replace(':id', id + "_" + fk_industry);
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                // edit task details
                                jQuery('.button_edit_task').click(function (event) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('button_edit_task', "");
                                    jQuery('#summernote' + id_nr).next('.note-editor').show();
                                    jQuery('#industry_task_description' + id_nr).hide();
                                    $('#button_delete_task' + id_nr).removeClass('disabled');
                                    $('#button_cancel_task' + id_nr).removeClass('disabled');
                                    $(this).trigger('blur');
                                    $(this).addClass('disabled');
                                });

                                jQuery('.button_delete_task').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to delete?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_delete_task', "");
                                        jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                                        jQuery('#industry_task_description' + id_nr).html('Please press Save to submit the changes');
                                        jQuery('#industry_task_description' + id_nr).show();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task' + id_nr).removeClass('disabled');
                                        $('#button_cancel_task' + id_nr).addClass('disabled');
                                    }
                                });

                                jQuery('.button_cancel_task').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to  cancel?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_cancel_task', "");
                                        var text = jQuery('#industry_task_description' + id_nr).html();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editable').html(text);
                                        jQuery('#industry_task_description' + id_nr).show();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task' + id_nr).removeClass('disabled');
                                        $('#button_delete_task' + id_nr).removeClass('disabled');
                                    }
                                });

                                // edit task details en
                                jQuery('.button_edit_task_en').click(function (event) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('button_edit_task_en', "");
                                    jQuery('#summernote_en' + id_nr).next('.note-editor').show();
                                    jQuery('#industry_task_description_en' + id_nr).hide();
                                    $('#button_delete_task_en' + id_nr).removeClass('disabled');
                                    $('#button_cancel_task_en' + id_nr).removeClass('disabled');
                                    $(this).trigger('blur');
                                    $(this).addClass('disabled');
                                });

                                jQuery('.button_delete_task_en').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to delete?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_delete_task_en', "");
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editable').html('');
                                        jQuery('#industry_task_description_en' + id_nr).html('Please press Save to submit the changes');
                                        jQuery('#industry_task_description_en' + id_nr).show();
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task_en' + id_nr).removeClass('disabled');
                                        $('#button_cancel_task_en' + id_nr).addClass('disabled');
                                    }
                                });

                                jQuery('.button_cancel_task_en').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to  cancel?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_cancel_task_en', "");
                                        var text = jQuery('#industry_task_description_en' + id_nr).html();
                                        jQuery('#industry_task_description_en' + id_nr).show();
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editable').html(text);
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task_en' + id_nr).removeClass('disabled');
                                        $('#button_delete_task_en' + id_nr).removeClass('disabled');
                                    }
                                });

                                $(document).on('click', '.button_delete_attachment', function() {
                                    var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
                                    var msg = "Delete attachment " + title + "?";
                                    var id = jQuery(this).attr('id');
                                    var id_submodule = id.replace('button_delete_attachment', '');
                                    // alert(title+'--'+msg+'--'+id_nr);
                                    $('#formConfirm').find('#frm_body').html(msg)
                                                     .end().modal('show');

                                    $("#frm_submit").addClass('button_delete_file-ok');
                                    $("#frm_submit").append('<input class="modal-input" type="hidden"/>');
                                    $('.modal-input').attr('data-software-attachment-id', id_submodule);
                                });

                                $(document).on('click', '#frm_cancel', function() {
                                    $("#frm_submit").removeClass('button_delete_file-ok');
                                    $("#frm_submit").find('.modal-input').remove();
                                });

                                $(document).on('click', '.close', function() {
                                    $("#frm_submit").removeClass('button_delete_file-ok');
                                    $("#frm_submit").find('.modal-input').remove();
                                });

                                // add module/submodule for all industries
                                $(document).on('click', '.button_delete_file-ok', function () {
                                    var id_submodule = $(this).find('.modal-input').attr('data-software-attachment-id');
                                    var data = {
                                        'id_industry_attachment': id_submodule
                                    };

                                    $(this).find('.modal-input').remove();
                                    $(this).removeClass('button_delete_file-ok');
                                    $('#formConfirm').modal('hide');

                                    jQuery.post('{{route("ajax.remove_file_industry")}}', data)
                                            .done(function (msg) {
                                                if (parseInt(msg) > 0) {
                                                    var attached_files, new_val, id_ms_task;
                                                    // id_ms_task = parseInt(msg);
                                                    attached_files = $('#attachment_counter' + id).html();
                                                    new_val = parseInt(attached_files) - 1;
                                                    $('#attachment_counter' + id).html(new_val);
                                                }
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                // alert(xhr.responseText);
                                            });
                                    jQuery('#col-attachments' + id_submodule).hide();
                                });

                                // call dropzone - drag and drop files
                                // $(".dropzone").dropzone({uploadMultiple: false});
                                Dropzone.autoDiscover = false;


                                $("#uploadFiles").dropzone({
                                    url: url,
                                    fk_industry: fk_industry,
                                    addRemoveLinks: true,
                                    success: function (file, response) {
                                        var imgName = response;
                                        var id_ms_task = id;

                                        var attached_files, new_val;
                                        var _ref = file.previewElement;
                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                        new_val = parseInt(attached_files) + 1;
                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                        file.previewElement.classList.add("dz-success");
                                    },
                                    error: function (file, response) {
                                        file.previewElement.classList.add("dz-error");
                                    }
                                });

                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });
                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
                                    var id = jQuery(this).attr('id');
                                    var ms_task_id = $(this).attr('data-ms-task-id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };
                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
                                                if(msg == 1) {
                                                    $('#'+ms_task_id).find('.fa-bell-o').remove();
                                                } else {
                                                    $('#'+ms_task_id).append('<i class="fa fa-bell-o" style="font-size: 13px"></i>');
                                                }
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                // alert(xhr.responseText);
                                            });
                                });
                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
            setTimeout( function() {
                    // var attachmentHeight = $('.attachment-section').height();
                    // $('.attachment-section').css('position', 'absolute');
                    // $('.attachment-section').css('top', $('#task_description').outerHeight());
                    // $('.attachment-section').css('width', '100%');
                    // $('#task_description').css('margin-bottom', attachmentHeight+'px');
                }, 1000);
        }
        // edit an existing task
        function modal_dialog_edit_project_task(id, id_project) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_project_task_industry/" + id + "/" + '<?php print $industry->id_industry ?>' + "/"+ id_project;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                // edit task details
                                jQuery('.button_edit_task').click(function (event) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('button_edit_task', "");
                                    jQuery('#summernote' + id_nr).next('.note-editor').show();
                                    jQuery('#industry_task_description' + id_nr).hide();
                                    $('#button_delete_task' + id_nr).removeClass('disabled');
                                    $('#button_cancel_task' + id_nr).removeClass('disabled');
                                    $(this).trigger('blur');
                                    $(this).addClass('disabled');
                                });

                                jQuery('.button_delete_task').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to delete?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_delete_task', "");
                                        jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                                        jQuery('#industry_task_description' + id_nr).html('Please press Save to submit the changes');
                                        jQuery('#industry_task_description' + id_nr).show();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task' + id_nr).removeClass('disabled');
                                        $('#button_cancel_task' + id_nr).addClass('disabled');
                                    }
                                });

                                jQuery('.button_cancel_task').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to  cancel?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_cancel_task', "");
                                        var text = jQuery('#industry_task_description' + id_nr).html();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editable').html(text);
                                        jQuery('#industry_task_description' + id_nr).show();
                                        jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task' + id_nr).removeClass('disabled');
                                        $('#button_delete_task' + id_nr).removeClass('disabled');
                                    }
                                });

                                // edit task details en
                                jQuery('.button_edit_task_en').click(function (event) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('button_edit_task_en', "");
                                    jQuery('#summernote_en' + id_nr).next('.note-editor').show();
                                    jQuery('#industry_task_description_en' + id_nr).hide();
                                    $('#button_delete_task_en' + id_nr).removeClass('disabled');
                                    $('#button_cancel_task_en' + id_nr).removeClass('disabled');
                                    $(this).trigger('blur');
                                    $(this).addClass('disabled');
                                });

                                jQuery('.button_delete_task_en').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to delete?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_delete_task_en', "");
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editable').html('');
                                        jQuery('#industry_task_description_en' + id_nr).html('Please press Save to submit the changes');
                                        jQuery('#industry_task_description_en' + id_nr).show();
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task_en' + id_nr).removeClass('disabled');
                                        $('#button_cancel_task_en' + id_nr).addClass('disabled');
                                    }
                                });

                                jQuery('.button_cancel_task_en').click(function (event) {
                                    // confirm empty text
                                    var confirm_dialog = confirm("Are you sure you want to  cancel?");
                                    if (confirm_dialog == true) {
                                        var id = $(this).attr('id');
                                        var id_nr = id.replace('button_cancel_task_en', "");
                                        var text = jQuery('#industry_task_description_en' + id_nr).html();
                                        jQuery('#industry_task_description_en' + id_nr).show();
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editable').html(text);
                                        jQuery('#summernote_en' + id_nr).parent().find('.note-editor').hide();
                                        $(this).addClass('disabled');
                                        $(this).trigger('blur');
                                        $('#button_edit_task_en' + id_nr).removeClass('disabled');
                                        $('#button_delete_task_en' + id_nr).removeClass('disabled');
                                    }
                                });

                                // call dropzone - drag and drop files
                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code

                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');

                                        var data = {
                                            'id_industry_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_industry") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    // alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });
                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
                                    var id = jQuery(this).attr('id');
                                    var ms_task_id = $(this).attr('data-ms-task-id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };
                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
                                                if(msg == 1) {
                                                    $('#'+ms_task_id).find('.fa-bell-o').remove();
                                                } else {
                                                    $('#'+ms_task_id).append('<i class="fa fa-bell-o" style="font-size: 13px"></i>');
                                                }
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });


                                });
                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        // open add new task for an existing submodule
        function modal_dialog_add_task_industry(id_module_structure, id_industry) {
            if (id_module_structure == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task_industry/" + id_module_structure + "/<?php print $industry->id_industry; ?>";;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });

                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code
                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');

                                        var data = {
                                            'id_software_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                $('.dz-preview').hover(function () {

                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex});
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });

                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // open add new task for a newly created module/submodule
        function modal_dialog_add_task_new_industry(id_module_structure, id_software) {
            if (id_module_structure == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task_new_industry/" + id_module_structure + "/" + id_software + "/<?php print $industry->id_industry; ?>";
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });

                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {
                                        // your deletion code
                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');

                                        var data = {
                                            'id_software_attachment': id_nr
                                        };
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();

                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                            .done(function (msg) {
//                                                alert(msg)
                                            })
                                            .fail(function (xhr, textStatus, errorThrown) {
                                                alert(xhr.responseText);
                                            });

                                });

                                $('.dz-preview').hover(function () {

                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex});
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });

                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

    </script>
    <script type="text/javascript">
        // drag and drop order tasks/ module
        $(function () {
            $('.sortable_task').sortable({
                axis: 'y',
                group: '.sortable_task',
                opacity: 0.7,
                tolerance: 'pointer',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();

                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable,
                        'fk_industry': '<?php print $industry->id_industry  ?>'

                    };

                    $.post('{{  route("ajax.set_order_adm_industry_task") }}', data)
                            .done(function (msg) {
//                                alert(JSON.stringify(msg))
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                }
            }); // fin sortable
            // do this for every level
            for (var level = 0; level <= 6; level++) {
                $('.sortable_modules_' + level).sortable({
                    axis: 'y',
                    opacity: 0.7,
                    handle: 'span',
                    update: function (event, ui) {
                        var list_sortable = $(this).sortable('toArray').toString();
                        // change order in the database using Ajax
                        var data = {
                            'list_order': list_sortable,
                            'fk_industry': '<?php print $industry->id_industry  ?>'
                        };
                        $.post('{{  route("ajax.set_order_adm_industry_modules") }}', data)
                                .done(function (msg) {
//                                    alert(JSON.stringify(msg))
                                })
                                .fail(function (xhr, textStatus, errorThrown) {
                                    //  alert(xhr.responseText);
                                });
                    }
                }); // fin sortable
            }
        });
        $(document).ready(function () {
            if ($('#scrollTo').length != 0) {
                $('html, body').animate({ scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50 }, 'slow');
            }
        });
    </script>
    <script type="text/javascript">
        $('.menu_title>a').on('click', function(e) {
            e.preventDefault();
            $(this).parent().siblings('.easy-tree-toolbar').find('.remove').find('button').on('click', function(ev) {
                ev.preventDefault();
                var title = $(this).attr('data-title');
                var msg = $(this).attr('data-message');
                var dataForm = $(this).attr('data-form');
                var url = $(this).attr('href');

                $('#formConfirm')
                  .find('#frm_body').html(msg)
                  .end().find('#frm_title').html(title)
                  .end().modal('show');
            });
        });
    </script>
@stop
@section('content')

    <div class="box box-default">
        <!-- /.box-header -->
        <div class="box-body industry_module_structure">
            <div class="row">
                <div class="col-xs-12 tabs">
                    <ul class="nav nav-tabs nav-left icon-tab profile-navigation-tabs">
                      <li class="{{($has_filters)?'':'active'}}"><a data-toggle="tab" href="#home">{{trans('messages.label_details')}}</a></li>
                      <li class="{{($has_filters)?'active':''}}"><a data-toggle="tab" href="#filters">{{trans('messages.label_filters')}}</a></li>
                      <li><a data-toggle="tab" href="#export">{{trans('messages.label_export')}}</a></li>
                      <li><a data-toggle="tab" href="#invoices">{{trans('messages.nav_import')}}</a></li>
                    </ul>
                    <div class="tab-content profile-navigation-content">
                        <div id="home" class="tab-pane fade {{($has_filters)?'':' in active'}}">
                            <div class="row">
                                <div class=" col-lg-10 col-xs-9">
                                    <div class="row">
                                        <div class="  col-sm-6 col-xs-12">
                                            <table class="table-responsive tab-table">
                                                <tr>
                                                    <th>{{trans('messages.label_industry_id')}}:&nbsp;</th>
                                                    <td>{{ $industry->id_industry  }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_industry_name')}}:&nbsp;</th>
                                                    <td>{{ $industry->name_industry }}</td>
                                                </tr>
                                                <tr>
                                                    <th>{{trans('messages.label_industry_created_at')}}:&nbsp;</th>
                                                    <td> {{ date('d.m.Y', strtotime($industry->created_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Character Counter:&nbsp;</th>
                                                    <td><span class="description_word_counter"></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xs-3" style="text-align: center">
                                </div>
                            </div>
                        </div>
                        <div id="filters" class="tab-pane fade {{($has_filters)?'in active':' '}}">
                            <div class="panel-body filter_row">
                                {{ Form::open(['route'=>['industry.show',$industry->id_industry ], 'method' => 'get', 'class'=>'filterform','id'=>"formSearch"]) }}
                                <div class=" col-xs-12 col-lg-12 pd-10">
                                    <div class="row">
                                        {{-- filter by checked--}}
                                        {{-- <div class="col-xs-12 col-lg-4 col-sm-4  pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label>{{trans('messages.label_checked')}}: </label>
                                                {{ Form::select("fk_checked",$set_checked,$fk_checked,[   "class"=>"form-control"  ]) }}
                                            </div>
                                        </div> --}}
                                        {{-- filter by priority--}}
                                        <div class="col-xs-12 col-lg-4 col-sm-4  pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label>{{trans('messages.label_priority')}}:</label>
                                                {{ Form::select("fk_priority",$priorities,$fk_priority,[  "class"=>"form-control priority-filter"  ]) }}
                                            </div>
                                        </div>
                                        {{-- filter by attachment--}}
                                        <div class="col-xs-12 col-lg-4 col-sm-4  pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label>{{trans('messages.label_attachments')}}:</label>
                                                {{ Form::select("fk_attach",$set_attach,$fk_attach,[   "class"=>"form-control attach-filter"  ]) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{-- filter by default task--}}
                                        <div class="col-xs-12 col-lg-4 col-sm-4 pd-10 pdtp-0">
                                            <div class="form-group">
                                                <label>{{trans('messages.label_default_task')}}: </label>
                                                {{ Form::select("is_default",$custom_task,$is_default,[ "class"=>"form-control default-filter"  ]) }}
                                            </div>
                                        </div>
                                        {{-- filter by pairs--}}
                                        <div class="col-xs-12 col-lg-4 col-sm-4 pd-10 pdtp-0">
                                            <?php
                                                $selected_pair = Task_pair::getFirstById($fk_pair);
                                                if (count($selected_pair) == 1) {
                                                    $selected_color = $selected_pair->color_pair;
                                                } else {
                                                    $selected_color = '#fff';
                                                }
                                            ?>
                                            <div class="form-group">
                                                <label>{{trans('messages.label_pair')}}:</label>
                                                {{-- <select name="fk_pair" class="form-control" id="task_pair" style="background: {{$selected_color}}"> --}}
                                                    {{-- <option value="0" style=" background: #fff;"> {{trans('messages.input_select')}}</option> --}}
                                                    {{-- @foreach($pairs as $pair) --}}
                                                        {{-- <option value="{{$pair->id_pair}}" data-color="{{$pair->color_pair}}" style=" background: {{$pair->color_pair}}" {{($fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option> --}}
                                                    {{-- @endforeach --}}
                                                {{-- </select> --}}
                                                <div class="dropdown">
                                                    <button class="btn dropdown-toggle form-control select-color-button"
                                                    type="button" data-toggle="dropdown" name="color">
                                                        <span id="pair_color" class="">
                                                            {{$selected_pair->name_pair!=null?$selected_pair->name_pair: trans('messages.input_select')}}
                                                        </span>
                                                        <span class="pair-color-preview" style="background: {{$selected_color}}"></span>
                                                        <span class="caret pull-right select-arrow-down"></span>
                                                    </button>
                                                    <ul class="dropdown-menu color-dropdown">
                                                        <input id="task_pair" type="hidden" name="fk_pair">
                                                        {{-- @if ($selected_pair->name_pair!=null) --}}
                                                          <li class="select-colors">
                                                            <a data-id="0" data-name="{{trans('messages.input_select')}}" data-color="" onclick="select_pair_color(this)" href="#">
                                                              {{trans('messages.input_select')}}
                                                            </a>
                                                          </li>
                                                        {{-- @endif --}}
                                                        @foreach($pairs as $pair)
                                                          <li class="select-colors">
                                                            <a data-id="{{$pair->id_pair}}" data-name="{{$pair->name_pair}}" data-color="{{$pair->color_pair}}" onclick="select_pair_color(this)" href="#">{{$pair->name_pair}}
                                                              <span class="select-color-preview pull-right" style="background:{{$pair->color_pair}}"></span>
                                                              </a>
                                                          </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="filter-buttons col-xs-6 col-lg-2 col-md-2 col-sm-3  pd-10 pdtp-0">
                                        <button type="submit" class="btn ls-light-blue-btn btn-block">
                                            {{trans('messages.act_filter')}}
                                        </button>
                                    </div>
                                    <div class="filter-buttons col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                        <a href="{{URL::to('industry/'.$industry->id_industry)}}" class="btn ls-red-btn btn-block">
                                            {{trans('messages.act_reset_filters')}}
                                        </a>
                                    </div>
                                    <div class="filter-buttons-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                        <button type="submit" class="btn btn-block" disabled>
                                            {{trans('messages.act_filter')}}
                                        </button>
                                    </div>
                                    <div class="filter-buttons-inactive col-xs-6 col-lg-2 col-md-2 col-sm-3 pd-10 pdtp-0">
                                        <button type="submit" class="btn btn-block" disabled>
                                            {{trans('messages.act_reset_filters')}}
                                        </button>
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                        <div id="export" class="tab-pane fade export-panel">
                            <div class="export-links">
                                <a href="{{URL::to('download_industry_pdf/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_word/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_excel/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_attachments/'.$industry->id_industry)}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                            <div class="{{$export_file->file_name!=''?'export-details':''}}">
                                <div class="row">
                                  @if ($export_file->file_name!='')
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.last_export')}}:</strong>
                                    </div>
                                    <div class="col-xs-6">{{ $export_file->file_name  }}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->date!='0000-00-00 00:00:00' && $export_file->date!=null)
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.exported_at')}}:</strong>
                                    </div>
                                    <?php
                                        $date = explode(' ', $export_file->created_at);
                                        $hours = explode(':',$date[1]);
                                        $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                        $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                    ?>
                                    <div class="col-xs-6">{{ $date }}</div>
                                  @endif
                                </div>
                                <div class="row">
                                  @if ($export_file->exported_by!='')
                                    <div class="col-xs-3">
                                      <strong>{{trans('messages.label_author')}}:</strong>
                                    </div>
                                    <div class="col-xs-6">{{ $export_file->exported_by  }}</div>
                                  @endif
                                </div>
                            </div>
                        </div>
                        <div id="invoices" class="tab-pane fade">
                          <div style="display: block">
                              <form action="{{ action("SoftwareController@importExcel2") }}" method="post" enctype="multipart/form-data">
                                  <div class="btn btn-round btn-info btn-file import-software-btn">
                                      <i class="fa fa-upload fa-4x"></i>
                                      <input type="hidden" name="software_id" value="{{ $software->id_software }}">
                                      <input type="file" name="import_file" onchange="this.form.submit();">
                                  </div>
                              </form>
                          </div>
                        </div>
                    </div>
                    <!--End tabs container-->
                </div>
            </div>
            <div class="panel  ">
                <div class="panel-body">
                    <div style="display: block">
                        <div class="task_page_tree" id="task_page_tree">
                            <input type="hidden" name="industry_id" value="{{$industry->id_industry }}" />
                            <div class="easy-tree ls-tree-view">
                                <ul class="ul-tree">
                                    <li class="li_not_selectable industry">
                                <span class="menu_title" id="industry{{$industry->id_industry }}">
                                    <span class="glyphicon glyphicon-minus-sign"></span>
                                    <span class="click_item glyphicon glyphicon-sort-by-attributes"></span>
                                    <a href="javascript:void(0); ">
                                        {{$industry->name_industry }}
                                    </a>
                                    </span><input type="hidden" name="software_id" value="0" />
                                        <ul id="list">
                                            @foreach($software as $soft)
                                            <li class="software">
                                                <span class="menu_title" id="software{{$soft->id_software}}">
                                                    <span class="glyphicon glyphicon-minus-sign"></span>
                                                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                                    <a href="javascript:void(0); ">
                                                        {{ $soft->name_software }}
                                                    </a>
                                                </span>
                                                <input type="hidden" name="software_id" value="{{$soft->id_software}}" />
                                                <input type="hidden" name="module_id" value="0" />
                                                <ul class="sortable_modules_0">
                                                    <?php
                                                    $module_structure = Module_structure::getModuleByIndustryParent($industry->id_industry, $soft->id_software);
                                                    ?>
                                                            <!-- found in helpers.php  - returns the tree based on industry, model structure and software, starting lvl 1 -->
                                                    <?php
                                                    printTreeAdminIndustry($module_structure, $industry->id_industry, $soft->id_software, 1, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks);
                                                    ?>
                                                </ul>
                                            </li>
                                            @endforeach
                                            @foreach($projects as $project)
                                                <?php
                                                    // dd($project);
                                                    $module_structure = Module_structure::getModuleByLvlWithDefault(5, 1, 0, $project->id_project);
                                                    $set_priority = Priority::getAllPriorities(1);
                                                ?>
                                                <?php
                                                    printProjectTreeAdminIndustry($module_structure, $industry->id_industry, $project->id_project, 1, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks);
                                                ?>
                                            @endforeach
                                        </ul>

                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="task_page_details_parent">
                            <div class="task_page_details" id="task_page_details"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Session::has('scrollTo'))
            <input type="hidden" id="scrollTo" value="{{Session::get('scrollTo')}}">
        @endif
        <!-- /.box-body -->
    </div>
    <div class="iframe-overlay">
    </div>
    <!--Modal Export - START-->
    <div id="modal-export" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="padding-15">
                        <div class="export-links">
                            <div class="show-industries-export" style="display: none;">
                                <a href="{{URL::to('download_industry_pdf/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_word/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_excel/'.$industry->id_industry)}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_industry_attachments/'.$industry->id_industry)}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                            <div class="show-software-export" style="display: none;">
                                <a href="{{URL::to('download_software_pdf/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/pdf-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_word/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/word-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_excel/soft_id')}}" class="trigger-attachments-zip" target="_blank">
                                    <img src="{{asset('images/excel-icon.png')}}" class="projects_img">
                                </a>
                                <a href="{{URL::to('download_software_attachments/soft_id')}}" class="get-attachments-zip" target="_blank">
                                    <img src="{{asset('images/zip.png')}}" class="projects_img">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Export - END-->
    @include('admin.alert_box.delete_confirm')
    @include('admin.alert_box.confirm')
    @include('admin.alert_box.confirm_yes_no')
    @include('admin.alert_box.module_confirm_yes_no')
@stop
