<?php

// route for language [en,de]
Route::get('language/{lang}', array(
        'as'   => 'language.select',
        'uses' => 'LanguageController@select'
    )
);
Route::resource('lang', 'LanguageController');
Route::resource('chat', 'ChatController');
Route::post('ajax/process', array(
    'as'   => 'process',
    'uses' => 'ChatController@process'
));
Route::post('process', 'ChatController@process');
// resources
// path to users controller : index, show, edit, create
Route::resource('users', 'UsersController');
// path to session controller : index(login page), show(choose application page), create (register page)
Route::resource('sessions', 'SessionsController');
Route::get('new-user-login', 'SessionsController@newUserLogin');
Route::get('forgot-password', 'SessionsController@getForgotPassword');
Route::post('post-forgot-password', 'SessionsController@postForgotPassword');
Route::get('recover-password/{token}', 'SessionsController@recoverPassword');
Route::post('post-recover-password/{token}', 'SessionsController@postRecoverPassword');
Route::resource('exporter', 'ExporterController');

// logout
Route::any('logout', 'SessionsController@destroy');

// routes for register / login /
Route::any('/', 'SessionsController@index');
Route::any('login', 'SessionsController@index');


Route::any('registerUser', 'SessionsController@registerUser');
Route::any('activation', 'SessionsController@activation');
Route::any('accept-user/{id}', 'SessionsController@acceptUser');
Route::any('decline-user/{id}', 'SessionsController@declineUser');
Route::any('employees-list/{id}', 'SessionsController@employeesList');
Route::any('check_login', 'SessionsController@check_login');

//  ajax for headquarters details
Route::get('ajax/viewHeadquarters/{id}', array(
    'as'   => 'id',
    'uses' => 'SessionsController@viewHeadquarters'
));
Route::get('ajax/viewHeadquarterDetails/{id}', array(
    'as'   => 'id',
    'uses' => 'SessionsController@viewHeadquarterDetails'
));

Route::post('/checkCompanyEmail', ['uses' => 'SessionsController@checkCompanyEmail']);
Route::post('/checkHeadquarterEmail', ['uses' => 'SessionsController@checkHeadquarterEmail']);
Route::post('/checkUserEmail', ['uses' => 'SessionsController@checkUserEmail']);

// if user logged in front
Route::group(array('before' => 'auth'), function () {

    Route::resource('invoice', 'InvoiceController');
    // path to members controller : index, show, edit, create
    Route::resource('members', 'MembersController');
    Route::get('members/create_adm/{id}', array(
        'as'   => 'id',
        'uses' => 'MembersController@create_adm'));
    Route::get('members/delete_adm/{id}', array(
        'as'   => 'id',
        'uses' => 'MembersController@delete_adm'));
    Route::get('members/approve/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@approve'));
    Route::get('members/admin-approve/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@admin_approve'));
    Route::get('members/block/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@block'));
    Route::get('members/admin-block/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@admin_block'));
    Route::get('members/change_group/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@change_group'));
    Route::get('members/change_group_by_admin/{id}', array(
        'as'   => 'id',
        'uses' => 'UsersController@change_group_by_admin'));
    Route::get('profile', 'UsersController@profile');

    // path to session affiliates : index, show, edit, create
    Route::resource('affiliates', 'AffiliationController');
    Route::any('affiliates.accept', 'AffiliationController@accept');

    // path to session company : index, show, edit, create
    Route::resource('company', 'CompanyController');
    Route::get('/companyTab/{tab}', 'CompanyController@redirectFromDasboard');
    Route::get('/companyTab/{tab}/add-for/{add}', 'CompanyController@redirectFromDasboardAddNew');
    Route::resource('acompany', 'CompanyAdmController');
    Route::get('acompany/delete_adm/{id}', array(
        'as'   => 'id',
        'uses' => 'CompanyAdmController@delete_adm'));
    Route::get('approve-company/{id}', array(
        'as'   => 'id',
        'uses' => 'CompanyAdmController@approveCompany'));
    Route::get('block-company/{id}', array(
        'as'   => 'id',
        'uses' => 'CompanyAdmController@blockCompany'));

    // path to session headquarters : index, show, edit, create
    Route::resource('headquarters', 'HeadquartersController');
    Route::get('headquarters/create_adm/{id}', array(
        'as'   => 'id',
        'uses' => 'HeadquartersController@create_adm'));

    Route::get('headquarters/delete_adm/{id}', array(
        'as'   => 'id',
        'uses' => 'HeadquartersController@delete_adm'));
    Route::get('headquarters/make_main/{id}', array(
        'as'   => 'id',
        'uses' => 'HeadquartersController@make_main'));
    Route::get('headquarters/admin_make_main/{id}', array(
        'as'   => 'id',
        'uses' => 'HeadquartersController@admin_make_main'));


    // path to projects controller : index, show, edit, create
    Route::resource('projects', 'ProjectsController');
    Route::any('pack', 'ProjectsController@packages');
    // add / remove users per project
    Route::any('projects_add_users', 'ProjectsController@projects_add_users');

    Route::get('projects_remove_users/{id}', array(
        'as'   => 'id',
        'uses' => 'ProjectsController@projects_remove_users'));

    //purchases
    Route::get('/purchases', [
      'uses' => 'PurchasesController@getCheckout',
      'as' => 'purchases'
    ]);

    Route::post('/purchases/{id}', [
      'uses' => 'PurchasesController@postCheckout',
      'as' => 'purchases'
    ]);

    // project - extend period
    Route::post('/projects/update_due_date', ['uses' => 'ProjectsController@update_due_date']);

    // project - upload attachment
    Route::any('upload', 'ProjectsController@upload_file');
    // project - update tasks
    Route::post('select', array('as'   => 'ajax.update_project_task',
                                'uses' => 'ProjectsController@update_project_task'));

    Route::get('modal_dialog_edit_task_project/{id_ms_task}/{id_project}', array(
        'as'   => 'id',
        'uses' => 'ProjectsController@modal_dialog_edit_task_project'));
    Route::get('modal_dialog_add_task_project/{id_module_structure}/{id_industry}/{id_project}', array(
        'as'   => 'modal_dialog_add_task_project',
        'uses' => 'ProjectsController@modal_dialog_add_task_project'));
    Route::get('modal_dialog_add_task_new_project/{id_module_structure}/{id_industry}/{id_project}', array(
        'as'   => 'modal_dialog_add_task_new_project',
        'uses' => 'ProjectsController@modal_dialog_add_task_new_project'));
    Route::get('modal_dialog_edit_module_project/{id_module_structure}', array(
        'as'   => 'modal_dialog_edit_module_project',
        'uses' => 'ProjectsController@modal_dialog_edit_module_project'));


    Route::post('set_order_projects', array('as'   => 'ajax.set_order_projects',
                                            'uses' => 'ProjectsController@set_order_projects'));
    Route::post('set_order_front_task', array('as'   => 'ajax.set_order_front_task',
                                              'uses' => 'ProjectsController@set_order_front_task'));

    Route::get('/download_project_pdf/{bar}', array(
        'as'   => 'bar',
        'uses' => 'ProjectsController@download_project_pdf'
    ));
    Route::get('download_project_word/{bar}', 'ProjectsController@download_project_word');
    Route::get('download_word_html/{bar}', 'ProjectsController@download_word_html');

    Route::get('download_project_excel/{bar}', 'ProjectsController@download_project_excel');
    Route::get('download_excel_html/{bar}', 'ProjectsController@download_excel_html');

    Route::get('download_software_excel/{bar}', 'SoftwareController@download_software_excel');
    Route::get('download_software_excel/{bar}/{module}/{industry}/{project}', 'SoftwareController@download_software_excel');
    Route::get('download_software_excel/{bar}/{module}', 'SoftwareController@download_software_excel');
    Route::get('download_software_excel/{bar}/{module}/{industry}', 'SoftwareController@download_software_excel');
    Route::get('download_industry_excel/{bar}', 'IndustryController@download_industry_excel');

    // 'MaatwebsiteDemoController' = 'SoftwareController'
    Route::get('importExport', 'SoftwareController@importExport');
    Route::post('importExcel', 'SoftwareController@importExcel');

    Route::get('download_project_attachments/{bar}', 'ProjectsController@download_project_attachments');
    Route::get('download_software_attachments/{bar}', 'SoftwareController@download_software_attachments');
    Route::get('download_software_attachments/{bar}/{module}/{industry}/{project}', 'SoftwareController@download_software_attachments');
    Route::get('download_software_attachments/{bar}/{module}', 'SoftwareController@download_software_attachments');
    Route::get('download_software_attachments/{bar}/{module}/{industry}', 'SoftwareController@download_software_attachments');
    Route::get('download_industry_attachments/{bar}', 'IndustryController@download_industry_attachments');

    Route::any('upload_task', 'ProjectsController@projects_add_users');
    Route::any('add_new_task', 'ProjectsController@add_new_task');
    Route::any('add_new_task_empty_project', 'ProjectsController@add_new_task_empty_project');

    Route::any('remove_new_task', 'ProjectsController@remove_new_task');

    // handle module structure
    Route::post('delete_structure',
        array('as'   => 'ajax.delete_module_structure',
              'uses' => 'ProjectsController@delete_module_structure'));
    Route::post('edit_structure', array('as'   => 'ajax.edit_module_structure',
                                        'uses' => 'ProjectsController@edit_module_structure'));

    // project handle attachments
    Route::post('select_file', array('as'   => 'ajax.remove_file_project',
                                     'uses' => 'ProjectsController@remove_file_project'));
    Route::any('remove_file_project', 'ProjectsController@remove_file_project');

    Route::post('paypal-store/{$package_id}', array(
        'as' => 'payment',
        'uses' => 'PaypalPaymentController@store',
    ));

    Route::get('storePaypal', array(
        'as' => 'storePaypal',
        'uses' => 'ProjectsController@storePaypal',
    ));

    Route::post('inactiveProject', array(
        'as' => 'inactiveProject',
        'uses' => 'ProjectsController@storeInactiveProject',
    ));

    // this is after make the payment, PayPal redirect back to your site
    Route::get('getPaymentPaypalStatus', array(
        'as' => 'getPaymentPaypalStatus',
        'uses' => 'ProjectsController@getPaymentPaypalStatus',
    ));

    Route::post('paypal-extend-project/{$subpackage_id}', array(
        'as' => 'payment',
        'uses' => 'PaypalPaymentController@extendProjectWithPaypal',
    ));

    Route::get('checkoutPaypal', array(
        'as' => 'checkoutPaypal',
        'uses' => 'PurchasesController@postCheckoutPaypal',
    ));

    // this is after make the payment, PayPal redirect back to your site
    Route::get('getPaymentPaypalStatusExtend', array(
        'as' => 'getPaymentPaypalStatusExtend',
        'uses' => 'PurchasesController@getPaymentPaypalStatus',
    ));

    //create new project with sofort payment
    Route::post('sofort-store/{$package_id}', array(
        'as' => 'payment',
        'uses' => 'SofortPaymentController@store',
    ));

});

// if admin is logged in
Route::group(array('before' => 'auth'), function () {
    Route::get('/download_software_pdf/{bar}', array(
        'as'   => 'bar',
        'uses' => 'SoftwareController@download_software_pdf'
    ));
    Route::get('download_software_pdf/{bar}/{module}/{industry}/{project}', 'SoftwareController@download_software_pdf');
    Route::get('download_software_pdf/{bar}/{module}', 'SoftwareController@download_software_pdf');
    Route::get('download_software_pdf/{bar}/{module}/{industry}', 'SoftwareController@download_software_pdf');

    Route::get('/download_industry_pdf/{bar}', array(
        'as'   => 'bar',
        'uses' => 'IndustryController@download_industry_pdf'
    ));

    Route::get('download_software_word/{bar}', 'SoftwareController@download_software_word');
    Route::get('download_software_word/{bar}/{module}/{industry}/{project}', 'SoftwareController@download_software_word');
    Route::get('download_software_word/{bar}/{module}', 'SoftwareController@download_software_word');
    Route::get('download_software_word/{bar}/{module}/{industry}', 'SoftwareController@download_software_word');
    Route::get('download_industry_word/{bar}', 'IndustryController@download_industry_word');

    // Route::resource('export_files', 'Project_export_files');
    Route::post('importExcel2', 'SoftwareController@importExcel2');
    Route::resource('posts', 'PostsController');
    Route::resource('projekte', 'ProjectsAdmController');
    Route::resource('packs', 'PackagesController');
    Route::resource('packs_row', 'PackagesRowController');
    // invoices admin route
    Route::resource('invoices_a', 'InvoiceAdmController');
    Route::post('update_adm_company', 'CompanyAdmController@update_adm_company');
    // path to admin's profile / change password
    Route::get('profile_admin', 'UsersController@profile_admin');
    Route::get('password_admin', 'UsersController@password_admin');

    // admin dashboard
    Route::get('admin_home', 'SessionsController@admin_home');
    Route::get('session/change/{id}', array(
        'as'   => 'id',
        'uses' => 'SessionsController@change'));
    Route::get('change_to_admin', 'SessionsController@change_to_admin');

    // path to industry : index, show, edit, create
    Route::resource('industry', 'IndustryController');
    Route::post('edit_structure_admin',
        array('as'   => 'ajax.edit_module_s_admin',
              'uses' => 'IndustryController@edit_module_s_admin'));

    Route::get('index_tabs', 'IndustryController@index_tabs');

    Route::post('invoice/edit', ['uses' => 'InvoiceAdmController@edit']);
    Route::post('update_status', ['uses' => 'ProjectsController@updateProjectStatus']);

    // path to software : index, show, edit, create
    Route::resource('software', 'SoftwareController');
    Route::post('upload_file_software/{id}', 'SoftwareController@upload_file_software');
    Route::post('upload_file_industry/{id}', 'IndustryController@upload_file_industry');
    Route::get('invoice_list/{id_project}', 'InvoiceController@invoice_list');
    Route::get('invoice_pdf/{id}', 'InvoiceController@invoice_pdf');
    // edit module / submodule details
    Route::get('modal_dialog_edit_module/{id_module_structure}', array(
        'as'   => 'id_module_structure',
        'uses' => 'SoftwareController@modal_dialog_edit_module'));
    Route::get('modal_dialog_edit_project_module/{id_module_structure}', array(
        'as'   => 'id_module_structure',
        'uses' => 'SoftwareController@modal_dialog_edit_project_module'));

    Route::get('emailTasks', ['uses' => 'TaskController@approve_tasks']);

    //  ajax for task module structure
    Route::get('ajax/viewTaskModuleStructure/{id}', array(
        'as'   => 'id',
        'uses' => 'SoftwareController@viewTaskModuleStructure'
    ));

    Route::get('ajax/lock_module', array(
        'as'   => 'lock_module',
        'uses' => 'ModuleController@lockModule'
    ));

    // path to consultant : index, show, edit, create
    Route::resource('consultant', 'ConsultantController');

    Route::get('consultant/approve/{id}', array(
        'as'   => 'id',
        'uses' => 'ConsultantController@approve'));
    Route::get('consultant/block/{id}', array(
        'as'   => 'id',
        'uses' => 'ConsultantController@block'));

    // path to software : index, show, edit, create
    Route::resource('module', 'ModuleController');

    // path to software : index, show, edit, create
    Route::resource('submodule', 'SubmoduleController');

    // administration -  add all tasks of selected module / submodule to an industry ajax
    Route::post('add_all_to_industry', array(
        'as'   => 'ajax.add_all_to_industry',
        'uses' => 'SubmoduleController@add_all_to_industry'));

    // administration -  add all tasks of selected module / submodule to an industry category ajax
    Route::post('add_all_to_industries', array(
        'as'   => 'ajax.add_all_to_industries',
        'uses' => 'SubmoduleController@add_all_to_industries'));

    // administration -  remove all tasks of selected module / submodule from an industry ajax
    Route::post('remove_all_from_industry', array(
        'as'   => 'ajax.remove_all_from_industry',
        'uses' => 'SubmoduleController@remove_all_from_industry'));

    // administration -  remove all tasks of selected module / submodule from an industry category ajax
    Route::post('remove_all_from_industries', array(
        'as'   => 'ajax.remove_all_from_industries',
        'uses' => 'SubmoduleController@remove_all_from_industries'));

    Route::post('delete_module_structure_industry',
        array('as'   => 'ajax.delete_module_structure_industry',
              'uses' => 'SubmoduleController@delete_module_structure_industry'));

    Route::post('delete_module_structure_software',
        array('as'   => 'ajax.delete_module_structure_software',
              'uses' => 'SubmoduleController@delete_module_structure_software'));

    // path to software : index, show, edit, create
    Route::resource('task', 'TaskController');

    // administration - se tasks order ajax
    Route::post('set_order_adm_task', array('as'   => 'ajax.set_order_adm_task',
                                            'uses' => 'TaskController@set_order_adm_task'));

    // administration - se tasks order ajax
    Route::post('set_order_adm_industry_task', array('as'   => 'ajax.set_order_adm_industry_task',
                                                     'uses' => 'TaskController@set_order_adm_industry_task'));


    // administration - se modules / submodules order ajax
    Route::post('set_order_adm_modules', array('as'   => 'ajax.set_order_adm_modules',
                                               'uses' => 'ModuleController@set_order_adm_modules'));

    // administration - se modules / submodules order ajax
    Route::post('set_order_adm_industry_modules', array('as'   => 'ajax.set_order_adm_industry_modules',
                                                        'uses' => 'ModuleController@set_order_adm_industry_modules'));

    // administration - add selected task to industry
    Route::post('check_admin_industry_task', array('as'   => 'ajax.check_admin_industry_task',
                                                   'uses' => 'TaskController@check_admin_industry_task'));

    Route::post('check_admin_all_industry_task', array('as'   => 'ajax.check_admin_all_industry_task',
                                                   'uses' => 'TaskController@check_admin_all_industry_task'));

    // add new task + structure
    Route::any('add_new_task_admin', 'TaskController@add_new_task_admin');
    // Route::post('add_new_task_admin', 'TaskController@add_new_task_admin');
    // add new task + structure + link to industry
    Route::any('add_new_task_admin_industry', 'TaskController@add_new_task_admin_industry');

    Route::any('duplicate_software_task/{task_id}', array('as'   => 'ajax.duplicate_software_task',
                                                   'uses' => 'TaskController@duplicate_software_task'));

    // edit tasks details in task page
    Route::get('modal_dialog_edit_task_admin/{id_ms_task}', array(
        'as'   => 'id_ms_task',
        'uses' => 'TaskController@modal_dialog_edit_task_admin'));

    // edit tasks details in software page
    Route::get('modal_dialog_edit_task/{id_ms_task}', array(
        'as'   => 'id_ms_task',
        'uses' => 'TaskController@modal_dialog_edit_task'));

    // edit tasks details in industry page
    Route::get('modal_dialog_edit_task_industry/{id_ms_task}/{fk_industry}', array(
        'as'   => 'modal_dialog_edit_task_industry',
        'uses' => 'TaskController@modal_dialog_edit_task_industry'));

    // edit project tasks details in industry page
    Route::get('modal_dialog_edit_project_task_industry/{id_ms_task}/{fk_industry}/{id_project}', array(
        'as'   => 'modal_dialog_edit_project_task_industry',
        'uses' => 'TaskController@modal_dialog_edit_project_task_industry'));

    // add new task to software - in an existing module / submodule
    Route::get('modal_dialog_add_task/{id_module_structure}', array(
        'as'   => 'id_module_structure',
        'uses' => 'TaskController@modal_dialog_add_task'));

    // add new task to software - in an existing module / submodule + link it to industry
    Route::get('modal_dialog_add_task_industry/{id_module_structure}/{id_industry}', array(
        'as'   => 'modal_dialog_add_task_industry',
        'uses' => 'TaskController@modal_dialog_add_task_industry'));

    // add new task to software - to a new module / submodule
    Route::get('modal_dialog_add_task_new/{id_module_structure}/{id_software}', array(
        'as'   => 'modal_dialog_add_task_new',
        'uses' => 'TaskController@modal_dialog_add_task_new'));

    // add new task to software - to a new module / submodule + link it to industry
    Route::get('modal_dialog_add_task_new_industry/{id_module_structure}/{id_software}/{id_industry}', array(
        'as'   => 'modal_dialog_add_task_new_industry',
        'uses' => 'TaskController@modal_dialog_add_task_new_industry'));

    // delete task from a module / submodule
    Route::get('delete_task_from_structure/{id_ms_task?}', array(
        'as'   => 'id_ms_task',
        'uses' => 'TaskController@delete_task_from_structure'));

    // delete project
    Route::get('project.destroy/{id_project?}', array(
        'as'   => 'project.destroy',
        'uses' => 'ProjectsController@destroy'));

    // project handle attachments
    Route::post('select_file_soft', array('as'   => 'ajax.remove_file_software',
                                          'uses' => 'SoftwareController@remove_file_software'));
    Route::any('remove_file_software', 'SoftwareController@remove_file_software');

    Route::post('set_task_default', array('as'   => 'ajax.set_task_default',
                                          'uses' => 'TaskController@set_task_default'));

    Route::post('select_file_ind', array('as'   => 'ajax.remove_file_industry',
                                         'uses' => 'IndustryController@remove_file_industry'));
    Route::post('remove_file_industry', 'IndustryController@remove_file_industry');
    // task manager routes

    Route::resource('adm_task', 'AdmTasksController');
    Route::resource('adm_sprint', 'AdmSprintsController');
    Route::resource('adm_comments', 'AdmCommentsController');

    Route::get('adm_tasks_dialog_add_task/{id_release}/{id_sprint}', array(
        'as'   => 'adm_tasks_dialog_add_task',
        'uses' => 'AdmTasksController@adm_tasks_dialog_add_task'));

    Route::get('adm_tasks_dialog_edit_task/{id_task}', array(
        'as'   => 'adm_tasks_dialog_edit_task',
        'uses' => 'AdmTasksController@adm_tasks_dialog_edit_task'));

    Route::get('adm_tasks_dialog_add_sprint/{id_release}', array(
        'as'   => 'adm_tasks_dialog_add_sprint',
        'uses' => 'AdmSprintsController@adm_tasks_dialog_add_sprint'));

    Route::get('adm_tasks_dialog_edit_sprint/{id_sprint}', array(
        'as'   => 'adm_tasks_dialog_edit_sprint',
        'uses' => 'AdmSprintsController@adm_tasks_dialog_edit_sprint'));

    Route::post('upload_file_adm_task', "AdmTasksController@upload_file_adm_task");
    Route::post('add_comment_task', "AdmTasksController@add_comment_task");
    Route::get('adm_delete_attached_file/{id_attachment}', array(
        'as'   => 'adm_delete_attached_file',
        'uses' => 'AdmTasksController@adm_delete_attached_file'));
    Route::get('delete_resp_task/{id_task}/{fk_user}', array(
        'as'   => 'delete_resp_task',
        'uses' => 'AdmTasksController@delete_resp_task'));
});

Online::updateCurrent();
Online::expireSession();

// API controller/ functions
Route::any('/api/help', 'ApiController@index');
Route::any('/api/loginUser', 'ApiController@loginUser');
Route::any('/api/getCompany', 'ApiController@getCompany');
Route::any('/api/getUser', 'ApiController@getUser');
Route::any('/api/getTasks_module', 'ApiController@getTasks_module');
Route::any('/api/getTasks_module_leg', 'ApiController@getTasks_module_leg');
Route::get('/api/views/{bar}', array(
    'as'   => 'bar',
    'uses' => 'ApiController@view_help'
));
