@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')

@stop

@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="panel widget light-widget panel-bd-top ">
                    <div class="panel-heading ">
                        <h3 class="mgtp-10">
                            {{trans('messages.nav_projects')}}
                        </h3>
                    </div>
                    <div class="panel-body table_invoice">
                        <table class="table table-condensed data_table">
                            <thead>
                            <tr>
                                <th>{{trans('messages.label_nr_crt')}}</th>
                                <th>{{trans('messages.label_project_name')}}</th>
                                <th>{{trans('messages.label_industry_type')}}</th>
                                <th>{{trans('messages.label_industry')}}</th>
                                <th>{{trans('messages.label_created_at')}}</th>
                                <th>{{trans('messages.label_due_date')}}</th>
                                <th>{{trans('messages.label_creator')}}</th>
                                <th class="text-center">{{trans('messages.label_status')}}</th>
                                <th>{{trans('messages.label_action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <?php

                                $id_industry = $project->fk_industry;
                                $industry = Industry::getFirstById($id_industry);
                                $industry_type = Industry_type::getFirstById($industry->fk_type);
                                $software = Module_structure::getSoftwareByIndustry($id_industry);
                                $company = Company::getFirstById($project->fk_company);
                                $admin_user = User::getFirstById($project->fk_user);
                                ?>
                                <tr>
                                    <td>{{getProjectId($project->id_project)}}</td>
                                    <td>{{$project->name_project}}</td>
                                    <td>{{$industry_type->name_industry_type}}  </td>
                                    <td>{{$industry->name_industry}}</td>
                                    <td>{{date('d.m.Y',strtotime($project->created_at))}} </td>
                                    <td>{{date('d.m.Y',strtotime($project->due_date))}}</td>
                                    <td>{{ $admin_user->first_name .' '. $admin_user->last_name }}</td>
                                    <td class="table-project-status">
                                        <?php $status = is_project_expired($project->id_project); ?>
                                            {{($status==1)?'<span class="user_project label label-success ">'.trans('messages.l_active').'</span>':' '}}
                                            {{($status==2)?'<span class="user_project label label-warning">'.trans('messages.label_blocked').'</span>':' '}}
                                            {{($status==3)?'<span class="user_project label label-danger">'.trans('messages.l_expired').'</span>':' '}}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{URL::to('projects/'.$project->id_project)}}" class=" btn btn-round ls-orange-btn"><i class="fa fa-pencil-square-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="after_table">
                            @if(in_array('2',$fk_group))
                                <a href="{{URL::to('pack')}}" title=" " id="a_form_show_0" class="static_add_button a_form_show btn btn-round btn-xxl  ls-light-blue-btn  ">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="title_static_add_button" id="div_a_form_show_0">
                                    {{trans('messages.add_new_project')}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
