@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
@stop
@section('footer_scripts')
  <script type="text/javascript">
     $(document).on("click", ".delete_project", function (e) {
         e.preventDefault();
         $('.confirm_delete_keyword').removeClass('disabled');
         var data_id = $(this).attr('id');
         var msg = $(this).attr('data-msg');
          $('#formConfirm')
             .find('#frm_body').html(msg)
             .end().modal('show');
          $('#frm_submit').addClass('confirm_delete_keyword').attr('data_id', data_id);
          $('#formConfirm').on('click', '.confirm_delete_keyword', function(ev) {
             var id = $(this).attr('data_id');
             var form = $("[id=" + id + "]").parent();
             $(form).submit();
         });
      });

      $(document).ready(function(){
         $('.select-project-status').on('click', function(){
            $('#'+$(this).attr('data-id')).removeClass(function (index, className) {
                      return (className.match (/(^|\s)label-\S+/g) || []).join(' ');
                  }).addClass("label-"+$(this).attr('data-color'));
            $('#'+$(this).attr('data-id')).children('span').text($(this).attr('data-status'));
            var project_id = $(this).attr('data-id');
            var project_status = $(this).attr('data-status-id');
            var token = $('input[name="_token"]').val();

            $.ajax({
                url: 'update_status',
                type: 'post',
                data: {
                    '_token' : token,
                    'project_id': project_id,
                    'project_status': project_status
                },
                success: function (data) {}
           });
         });
      });
  </script>
@stop

@section('content')
    <div class="box">
        <!-- /.box-body -->
        <div class="box-body">
            <div class="row">
                <div class="panel widget light-widget panel-bd-top ">
                    <div class="panel-heading ">
                        <h3 class="mgtp-10">
                            {{trans('messages.nav_projects')}}
                        </h3>
                    </div>
                    <div class="panel-body table_invoice">
                        <table class="table table-condensed   data_table">
                            <thead>
                            <tr>
                                <th>{{trans('messages.label_nr_crt')}}</th>
                                <th>{{trans('messages.label_project_name')}}</th>
                                <th>{{trans('messages.name_company')}}</th>
                                <th>{{trans('messages.label_industry_type')}}</th>
                                <th>{{trans('messages.label_industry')}}</th>
                                <th>{{trans('messages.label_due_date')}}</th>
                                <th>{{trans('messages.label_project_type')}}</th>
                                <th class="text-center">{{trans('messages.label_status')}}</th>
                                <th class="text-center">{{trans('messages.label_action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <?php
                                $id_industry = $project->fk_industry;
                                $industry = Industry::getFirstById($id_industry);
                                $industry_type = Industry_type::getFirstById($industry->fk_type);                               ?>
                                <tr>
                                    <td>{{getProjectId($project->id_project)}}</td>
                                    <td>{{$project->name_project}}</td>
                                    <td>{{$project->name_company}}  </td>
                                    <td>{{$industry_type->name_industry_type}}</td>
                                    <td>{{$industry->name_industry}}</td>
                                    <td>{{date('d.m.Y',strtotime($project->due_date))}}</td>
                                    <td>
                                      {{($project->is_empty_project)?trans('messages.without_tree'):trans('messages.with_tree')}}
                                      {{ getDictionaryName($pack->fk_type_project,8)  }}
                                    </td>
                                    <td class="table-project-status">
                                        @if ($project->status == 3)
                                            <span class="label label-danger" style="display: block;">{{trans('messages.l_expired')}}</span>
                                        @else
                                          <div class="dropdown">
                                              <div class="dropdown-toggle" data-toggle="dropdown">
                                                  <?php
                                                      if($project->status == 1) {
                                                          $projectStatus = trans('messages.l_active');
                                                          $projectLabel = 'success';
                                                      } elseif ($project->status == 2) {
                                                          $projectStatus = trans('messages.label_blocked');
                                                          $projectLabel = 'warning';
                                                      }
                                                      // elseif ($project->status == 3) {
                                                      //     $projectStatus = trans('messages.l_expired');
                                                      //     $projectLabel = 'danger';
                                                      // }
                                                  ?>
                                                  <span id="{{$project->id_project}}" class="projects-status label label-{{$projectLabel}}">
                                                      <span>{{$projectStatus}}</span>
                                                      <i class="fa fa-caret-down table-select-arrow" aria-hidden="true"></i>
                                                      <ul class="dropdown-menu projects-status-dropdown">
                                                        <li class="select-project-status" data-id="{{$project->id_project}}" data-status-id="1" data-status="{{trans('messages.l_active')}}" data-color="success">
                                                            <div class="select-status-color label label-success">{{trans('messages.l_active')}}</div>
                                                        </li>
                                                        <li class="select-project-status" data-id="{{$project->id_project}}" data-status-id="2" data-status="{{trans('messages.label_blocked')}}" data-color="warning">
                                                            <div class="select-status-color label label-warning">{{trans('messages.label_blocked')}}</div>
                                                        </li>
                                                        {{-- <li class="select-project-status" data-id="{{$project->id_project}}" data-status-id="3" data-status="{{trans('messages.l_expired')}}" data-color="danger">
                                                            <div class="select-status-color label label-danger">{{trans('messages.l_expired')}}</div>
                                                        </li> --}}
                                                      </ul>
                                                  </span>
                                              </div>
                                          </div>
                                        @endif
                                    </td>
                                    <td class="edit_buttons text-center row">
                                      <div style="display: inline-block">
                                        <a href="{{URL::to('projects/'.$project->id_project)}}" class="btn btn-round ls-orange-btn"><i class="fa fa-pencil-square-o"></i></a>
                                      </div>
                                      <div style="display: inline-block">
                                        {{ Form::open(array('route' => array('projects.destroy', $project->id_project), 'method' => 'delete')) }}
                                        <button type="submit"
                                              class="btn ls-red-btn btn-round delete_project "
                                              id="{{$project->id_project}}"
                                              data-toggle="modal" data-target="#confirmDelete"
                                              data-title="Delete Project"
                                              data-msg="{{trans('validation.confirmation_delete_project')}}">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        {{ Form::close()}}
                                      </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.alert_box.delete_confirm')
@stop
