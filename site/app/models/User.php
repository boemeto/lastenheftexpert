<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;
use Illuminate\Support\Facades\Session;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    public static $rules = [
        'password' => 'required|min:4',
        'email'    => 'required|email|min:4|unique:users',
    ];
    public static $messages = [];
    public $primaryKey = 'id';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $guarded = ['id'];
    protected $fillable = ['email', 'password', 'first_name', 'last_name', 'activated', 'blocked',
        'fk_company', 'fk_headquarter', 'fk_personal_title', 'fk_title', 'job_user', 'language', 'permissions',
        'activation_code', 'activated_at', 'persist_code', 'last_login', 'reset_password_code', 'remember_token',
        'social_linkedin', 'social_twitter', 'social_xing', 'telephone_user', 'mobile_user', 'logo_user',
        'is_deleted'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getFirstById($id)
    {
        $list = User::whereId($id)->first();
        return $list;
    }

    public static function getLogoUser($id)
    {
        $list = User::whereId($id)->first();
        $logo = 'images/image_user/' . $list->logo_user;

        if ($list->logo_user == null || !file_exists('images/image_user/' . $list->logo_user)) {
           $logo = 'images/image_user/profile_default.png';
        }

        return $logo;
    }

    public static function getUsersByCompany($fk_company, $auth_user = null)
    {
        if ($auth_user == null) {
            $users = User::whereFk_company($fk_company)->where('is_deleted', '!=', '1')->get();
        } else {
            $users = User::whereFk_company($fk_company)
                ->where('id', '!=', $auth_user)
                ->where('is_deleted', '!=', '1')
                ->get();
        }
        return $users;
    }

    public static function getUsersByCompanyHeadquarter($fk_company, $fk_headquarter)
    {

        $users = User::whereFk_company($fk_company)
            ->where('fk_headquarter', '=', $fk_headquarter)
            ->get();
        return $users;

    }

    public static function getAllUsersDetailsByCompany($fk_company, $limit = null)
    {
        if ($limit === null) {
          $users = DB::table('users')
              ->selectRaw('*, users.blocked, users.created_at, users.updated_at, users.social_xing, users.social_linkedin, users.social_twitter,  ( select fk_group from users_groups where  users.id = users_groups.fk_user order by fk_group desc limit 1) as fk_group ')
              ->join('company_headquarters', 'users.fk_headquarter', '=', 'company_headquarters.id_headquarter')
              ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
              ->join('users_groups', 'users_groups.fk_user', '=', 'users.id')
              // ->orderBy('users.blocked', 'desc')
              // ->orderBy('users_groups.fk_group', 'asc')
              // ->orderBy('users.created_at', 'desc')
              // ->orderBy('last_name', 'asc')
              ->orderBy('id', 'desc')
              ->groupBy('id')
              ->where('users.fk_company', '=', $fk_company)
              ->where('users.is_deleted', '!=', '1')
              ->get();
        } else {
          $users = DB::table('users')
              ->selectRaw('*, users.blocked, users.created_at, users.updated_at, users.social_xing, users.social_linkedin, users.social_twitter,  ( select fk_group from users_groups where  users.id = users_groups.fk_user order by fk_group desc limit 1) as fk_group ')
              ->join('company_headquarters', 'users.fk_headquarter', '=', 'company_headquarters.id_headquarter')
              ->join('locations', 'company_headquarters.fk_location', '=', 'locations.id_location')
              ->join('users_groups', 'users_groups.fk_user', '=', 'users.id')
              // ->orderBy('users.blocked', 'desc')
              // ->orderBy('users_groups.fk_group', 'asc')
              // ->orderBy('users.created_at', 'desc')
              // ->orderBy('last_name', 'asc')
              ->orderBy('id', 'desc')
              ->groupBy('id')
              ->where('users.fk_company', '=', $fk_company)
              ->where('users.is_deleted', '!=', '1')
              ->limit($limit)
              ->get();
        }
        return $users;
    }

    public static function getUserByEmail($email)
    {
        $user = DB::table('users')
            ->where('users.email', '=', $email)
            ->get();

        return $user;
    }

    public static function getUserByRememberToken($token)
    {
        $user = DB::table('users')
            ->where('users.reset_password_code', '=', $token)
            ->get();

        return $user;
    }

    public static function getUsersByHeadquarter($fk_hedquarter)
    {

        $users = User::whereFk_headquarter($fk_hedquarter)
            ->where('is_deleted', '=', '0')->get();
        return $users;
    }

    public static function getUsersByGroup($fk_group)
    {
        $array = array();
        $lists = User::select('*')
            ->join('users_groups', 'id', '=', 'fk_user')
            ->where('fk_group', '=', $fk_group)->get();
        $array[0] = 'Select';
        foreach ($lists as $list) {
            $array[$list->id] = $list->first_name . ' ' . $list->last_name;
        }
        return $array;
    }

    public static function getUsersObjectsByGroup($fk_group)
    {

        $lists = User::select('*')
            ->join('users_groups', 'id', '=', 'fk_user')
            ->where('fk_group', '=', $fk_group)->get();

        return $lists;
    }

    public static function getUsersObjectsByGroupCompany($fk_group, $fk_company)
    {

        $lists = User::select('*')
            ->join('users_groups', 'id', '=', 'fk_user')
            ->where('fk_group', '=', $fk_group)
            ->where('fk_company', '=', $fk_company)->get();

        return $lists;
    }

    public static function getAdminUserData($fk_group = 1)
    {
        $lists = User::select('*')
            ->join('users_groups', 'id', '=', 'fk_user')
            ->where('fk_group', '=', $fk_group)->get();
        return $lists;
    }


    public static function createNewUser(
        $email, $password, $first_name, $last_name, $blocked, $fk_company,
        $fk_headquarter, $fk_personal_title, $fk_title, $job_user, $activation_code,
        $social_linkedin, $social_twitter, $social_xing, $telephone_user, $mobile_user,
        $logo_user, $is_deleted = 0, $fk_group = 3, $activated)
    {
        $user = new User;
        $user->email = $email;
        $user->activated = $activated;
        $user->fk_personal_title = $fk_personal_title;
        $user->fk_title = $fk_title;
        $user->job_user = $job_user;
        $user->last_name = $last_name;
        $user->first_name = $first_name;
        $user->mobile_user = $mobile_user;
        $user->telephone_user = $telephone_user;
        $user->password = Hash::make($password);
        $user->fk_company = $fk_company;
        $user->fk_headquarter = $fk_headquarter;
        $user->activation_code = $activation_code;
        $user->social_linkedin = $social_linkedin;
        $user->social_twitter = $social_twitter;
        $user->social_xing = $social_xing;
        $user->blocked = $blocked;
        $user->logo_user = $logo_user;
        $user->is_deleted = $is_deleted;
        $user->save();
        $id_user = $user->id;
        User_groups::createNewGroup($id_user, $fk_group);

        return $id_user;
    }

    public static function changeUser($id)
    {
        $currentUser = Auth::user();
        Session::put('super-admin-id', $currentUser->id);
        $user = User::join('users_groups', 'users_groups.fk_user', '=', 'users.id')
                ->where('users.id', '=', $id)
                ->where('users_groups.fk_group', '!=', 1)
                ->get()
                ->first();
        Auth::loginUsingId($user->id);
        Session::put('user_group_id', $user->fk_group);
    }

}
