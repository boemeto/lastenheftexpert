<?php
use Illuminate\Support\MessageBag;

class SubmoduleController extends \BaseController
{
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        // dd(Input::all());
        $submodule = Submodule::getFirstById($id);
        $submodule->fill(Input::all());
        $current_date = date("Y-m-d H:i:s");
        if (Input::has('is_default') && Input::get('is_default') == 1) {
            $submodule->is_default = 1;
            $module->locked_at = "0000-00-00 00:00:00";
            $is_default = 1;
        } else {
            $submodule->is_default = 0;
            $module->locked_at = $current_date;
            $is_default = 0;
        }

        $this->selectAllChildrenForEditDefault(Input::get('id_module_structure'), $submodule->is_default);
        $submodule->save();

        $id_module_structure = Input::get('id_module_structure');
        Session::flash('scrollTo', $id_module_structure);

        $mod_str = Module_structure::getFirstById($id_module_structure);
        $mod_str->is_default = $is_default;

        if($is_default == 0) {
            $mod_str->locked_at = $current_date;
        } else {
            $mod_str->locked_at = "0000-00-00 00:00:00";
        }

        $mod_str->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function add_all_to_industry()
    {
        $id_module_structure = Input::get('id_module_structure');
        $id_industry = Input::get('id_industry');
        $this->selectAllChildren($id_module_structure, $id_industry);

        $message = trans('validation.industry_checked_all');
        return $message;
    }

    public function add_all_to_industries()
    {
        $id_module_structure = Input::get('id_module_structure');
        if (Input::has('id_industry_category')) {
            $category_id = Input::get('id_industry_category');
            $id_industries = Industry::whereFk_type($category_id)->lists('id_industry');
        } else {
            $id_industries = Industry::lists('id_industry');
        }

        foreach ($id_industries as $id_industry) {
            $this->selectAllChildren($id_module_structure, $id_industry);
        }

        $message = trans('validation.industry_checked_all');
        return $message;
    }

    public function selectAllChildren($id_module_structure, $id_industry)
    {
        if (Module_structure::hasChild($id_module_structure)) {
            $module_children = Module_structure::getModuleChildren($id_module_structure);
            foreach ($module_children as $module_child) {
                self::selectAllChildren($module_child->id_module_structure, $id_industry);
            }
        } else {
            $module_tasks = Ms_task::getSoftwareTasks($id_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $id_ms_task = $module_task->id_ms_task;
                    $industry_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                        ->where('fk_ms_task', '=', $id_ms_task)->first();
                    if (count($industry_ms) == 0) {
                        Industry_ms::createNewItem($id_industry, $id_ms_task, $id_module_structure);

                        $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                        if (count($attachments) > 0) {
                            foreach ($attachments as $attachment) {
                                $folder = $attachment->folder_attachment;
                                $directory_soft = 'images/attach_software/' . $folder . '/';
                                $filename = $attachment->name_attachment;
                                $extension = $attachment->extension;
                                Industry_attach::addAttachToSelectedIndustry($id_ms_task, $id_industry, $folder, $directory_soft, $filename, $extension);
                            }
                        }
                    }
                }
            }
        }
//        return;
    }

    public function selectAllChildrenForEditDefault($id_module_structure, $is_default)
    {

        $current_date = date("Y-m-d H:i:s");
        if (Module_structure::hasChild($id_module_structure)) {
            $module_children = Module_structure::getModuleChildren($id_module_structure);
            foreach ($module_children as $module_child) {
                $module_structure = Module_structure::getFirstById($module_child->id_module_structure);
                $module_structure->is_default = $is_default;
                if($is_default == 0) {
                    $module_structure->locked_at = $current_date;
                    $module_structure->is_default = 0;
                    $module_structure->is_from_industry = 0;
                } else {
                    $module_structure->locked_at = "0000-00-00 00:00:00";
                    $module_structure->is_default = 1;
                    $module_structure->is_from_industry = 1;
                }
                $module_structure->save();
                self::selectAllChildrenForEditDefault($module_child->id_module_structure, $is_default);
            }
        } else {

            $module_tasks = Ms_task::getSoftwareTasks($id_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $id_ms_task = $module_task->id_ms_task;
                    $ms_task = Ms_task::getFirstById($id_ms_task);
                    $ms_task->is_default = $is_default;
                    if($is_default == 0) {
                        $ms_task->locked_at = $current_date;
                        $ms_task->is_from_industry = 0;
                    } else {
                        $ms_task->locked_at = "0000-00-00 00:00:00";
                        $ms_task->is_from_industry = 1;
                    }
                    $ms_task->save();
                }
            }
        }
    }

    public function remove_all_from_industry()
    {
        $id_module_structure = Input::get('id_module_structure');
        $id_industry = Input::get('id_industry');
        $this->removeAllChildren($id_module_structure, $id_industry);

        $message = trans('validation.industry_unchecked_all');
        return $message;
    }

    public function remove_all_from_industries()
    {
        $id_module_structure = Input::get('id_module_structure');
        if (Input::has('id_industry_category')) {
            $category_id = Input::get('id_industry_category');
            $id_industries = Industry::whereFk_type($category_id)->lists('id_industry');
        } else {
            $id_industries = Industry::lists('id_industry');
        }

        foreach ($id_industries as $id_industry) {
            $this->removeAllChildren($id_module_structure, $id_industry);
        }

        $message = trans('validation.industry_unchecked_all');
        return $message;
    }


    public function removeAllChildren($id_module_structure, $id_industry)
    {
        if (Module_structure::hasChild($id_module_structure)) {
            $module_children = Module_structure::getModuleChildren($id_module_structure);
            foreach ($module_children as $module_child) {
                self::removeAllChildren($module_child->id_module_structure, $id_industry);
            }
        } else {
            $module_tasks = Ms_task::getSoftwareTasks($id_module_structure);
            if (count($module_tasks) > 0) {
                foreach ($module_tasks as $module_task) {
                    $id_ms_task = $module_task->id_ms_task;
                    $industry_ms = Industry_ms::where('fk_industry', '=', $id_industry)
                        ->where('fk_ms_task', '=', $id_ms_task)->first();
                    if (count($industry_ms) > 0) {
                        $attachments = Software_attach::getAttachmentByMS_task($id_ms_task);
                        if (count($attachments) > 0) {
                            foreach ($attachments as $attachment) {
                                $folder = $attachment->folder_attachment;
                                $filename = $attachment->name_attachment;
                                Industry_attach::deleteAttachFromSelectedIndustry($id_ms_task, $id_industry, $filename, $folder);
                            }
                        }
                        $industry_ms->delete();
                    }
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $submodule = Submodule::getFirstById($id);
        $submodule->delete();
        return Redirect::route('submodule.index')->with('success_message', trans('validation.delete_success'));
    }


    public function delete_module_structure_industry()
    {
        $id_industry = Input::get('id_industry');
        $child_structure = Input::get('child_structure');
        if (strlen($child_structure) > 0) {
            $children_structure = explode('_', $child_structure);
        } else {
            $children_structure = array();
        }

        $is_deletable = false;
        $id_module_structure = $children_structure[1];

        unset($children_structure[0]);
        $length = count($children_structure);
        for ($i = 1; $i <= $length; $i++) {
            if ($children_structure[$i] != 0) {
                $has_child = Module_structure::hasChild($children_structure[$i]);
                if ($has_child == 0) {
                    $pr_module_tasks = Ms_task::getTasksByStructureIndustry($children_structure[$i], $id_industry);
                    if (count($pr_module_tasks) > 0) {
                        foreach ($pr_module_tasks as $task) {
                            $project_task = Projects_rows::whereFk_ms_task($task->id_ms_task)->first();
                            $ms_task = Ms_task::getFirstById($task->id_ms_task);

                            if( (is_null($project_task) && $ms_task->is_checked == 1) || $project_task->is_checked_user == 1 || $ms_task->is_mandatory == 1) {
                                $is_deletable = false;
                                $message = trans('validation.module_has_active_tasks');
                                return $message;
                            } else {
                                $is_deletable = true;
                            }
                        }
                    }
                }
            }
        }

        if($is_deletable) {
            foreach($children_structure as $module_structure_id) {
                $pr_module_structure = Module_structure::getFirstById($module_structure_id);
                $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module_structure_id, $id_industry);
                if (count($pr_module_tasks) > 0) {
                    foreach ($pr_module_tasks as $task) {
                        $ms_task = Ms_task::getFirstById($task->id_ms_task);
                        $ms_task->delete();
                    }
                }

                $pr_module_structure->delete();
            }
            $message = trans('validation.delete_success');
            return $message;
        }
    }


    public function delete_module_structure_software()
    {
        $child_structure = Input::get('child_structure');
        if (strlen($child_structure) > 0) {
            $children_structure = explode('_', $child_structure);
        } else {
            $children_structure = array();
        }

        $is_deletable = false;
        $id_module_structure = $children_structure[1];
        unset($children_structure[0]);
        $length = count($children_structure);
        for ($i = 1; $i <= $length; $i++) {
            if ($children_structure[$i] != 0) {
                $has_child = Module_structure::hasChild($children_structure[$i]);
                if ($has_child == 0) {
                    $pr_module_tasks = Ms_task::getTasksByStructure($children_structure[$i]);
                    if (count($pr_module_tasks) > 0) {
                        foreach ($pr_module_tasks as $task) {
                            $project_task = Projects_rows::whereFk_ms_task($task->id_ms_task)->first();
                            $ms_task = Ms_task::getFirstById($task->id_ms_task);
                            if( (is_null($project_task) && $ms_task->is_checked == 1) || $project_task->is_checked_user == 1 || $ms_task->is_mandatory == 1) {
                                $is_deletable = false;
                                $message = trans('validation.module_has_active_tasks');
                                return $message;
                            } else {
                                $is_deletable = true;
                            }
                        }
                    }
                }
            }
        }

        if($is_deletable) {
            foreach($children_structure as $module_structure_id) {
                $pr_module_structure = Module_structure::getFirstById($module_structure_id);
                $pr_module_tasks = Ms_task::getTasksByStructure($module_structure_id);

                if (count($pr_module_tasks) > 0) {
                    foreach ($pr_module_tasks as $task) {
                        $ms_task = Ms_task::getFirstById($task->id_ms_task);
                        $ms_task->delete();
                    }
                }

                $pr_module_structure->delete();
            }
            $message = trans('validation.delete_success');
            return $message;
        }
    }
}
