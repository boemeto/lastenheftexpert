<?php
return LanguageTrans::getList('en', 1);
return array(
    /*
      |--------------------------------------------------------------------------
      | Messages Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the template library to build
      | the buttons and other messages. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */
    // title
    'title_long'             => 'Lastenheft.Expert',
    'title_short'            => 'Lastenheft',

    // actions
    'act_permanently_delete' => 'Delete permanently',
    'act_add'                => 'Add',
    'act_delete'             => 'Delete',
    'act_undelete'           => 'Show',
    'act_link'               => 'Link',
    'act_edit'               => 'Modify',
    'act_modify'             => 'Modify',
    'act_details'            => 'Details',
    'act_cancel'             => 'Cancel',
    'act_save_close'         => 'Save and Close',
    'act_close'              => 'Close',
    'act_save'               => 'Save',
    'act_create'             => 'create',
    'Yes'                    => 'Yes',
    'No'                     => 'No',
    'language'               => 'Language',

    // navigation
    'nav_dashboard'          => 'Dashboard',
    'nav_software'           => 'Software',
    'nav_industry'           => 'Industry',
    'nav_industries'         => 'Industries',
    'nav_settings'           => 'Settings',
    'nav_module'             => 'Module',
    'nav_submodule'          => 'Submodule',
    'nav_task'               => 'Task',
    'nav_tasks'              => 'Tasks',
    'nav_new_project'        => 'Add new project',
    'nav_companies'          => 'Companies',
    'nav_company'            => 'Company',
    'nav_users'              => 'Users',
    'nav_user'               => 'User',
    'nav_logout'             => 'Logout',
    'nav_task_manager'       => 'Task Manager',
    'nav_edit_profile'       => 'Edit Profile',
    'nav_change_password'    => 'Change password',
    'nav_profile'            => 'Profile',
    'nav_lists'              => 'Lists',
    'nav_choose_soft'        => 'Choose Soft',
    'nav_consultants'        => 'Consultants',
    'nav_consultant'         => 'Consultants',
    'nav_posts_pages'        => 'Posts Pages',
    'nav_headquarter'        => 'Headquarter',

    'label_details'         => 'General Data',
    'label_mandatory'       => 'Mandatory',
    'label_checked'         => 'Checked',
    'label_suggested'       => 'Suggested',
    'label_priority'        => 'Priority',
    'label_pair'            => 'Pair',
    'label_select_industry' => 'Select Industry',
    'label_synonyms'        => 'Synonymes / Tags',
    'label_address_code'    => 'Address-Code Software-Vendor',
    'label_default'         => 'Standard',
    'label_is_not_offer'    => 'Is not offer',
    'label_is_control'      => 'Control Question',
    'label_attachments'     => 'Attachment',
    'label_full_name'       => 'Full name',
    'label_first_name'      => 'First name',
    'label_last_name'       => 'Last name',
    'label_telephone'       => 'Telephone',
    'label_email'           => 'Email',
    'label_job'             => 'Job',
    'label_mobile'          => 'Mobile phone',
    'label_is_licitation'   => 'Participates in a licitation',
    'label_has_consultant'  => 'Project needs consultant',
    'label_slug'            => 'Slug Link',
    'label_module_project'  => 'Project has predefined modules',
    'label_empty_project'   => 'Project is empty',

    // table headers
    'th_id'                 => 'Kennzeichnung',
    'th_name'               => 'Name',
    'th_description'        => 'Description',
    'th_type'               => 'Type',
    'th_is_deleted'         => 'Is deactivated',

    // form labels
    'l_industry_type'       => 'Industry type',
    'l_name'                => 'Name',
    'l_new'                 => 'New',
    'l_for'                 => 'for',
    'l_edit_all_tasks'      => 'Edit all Tasks',
    'l_edit_current_task'   => 'Edit current Task',
    'l_description'         => 'Description',
    'l_german'              => '[DE]',
    'l_rom'                 => '[RO]',
    'l_eng'                 => '[EN]',
    'l_code'                => 'code',
);
?>