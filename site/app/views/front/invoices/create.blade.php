@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('footer_scripts')
@stop
@section('content')

    <div class="panel widget light-widget panel-bd-top  ">
        <div class="panel-heading no-title"></div>
        <div class="panel-body">
            <div class="row">
                <div class="vd_pricing-table">
                    <div class="col-md-6 col-lg-3">
                        <div class="plan">
                            <div class="plan-header vd_bg-dark-blue vd_green text-center vd_info-parent">
                                <h3 class="pd-20 mgbt-xs-0">BASIC</h3>
                                <div class="price vd_bg-black-30"><span class="main"><span class="font-light">$</span><span class="font-bold">10</span> </span> <span class="suffix">99</span> <span class="text vd_bdt-green">/month</span></div>
                                <i class="caret-pos fa fa-caret-up vd_white"></i></div>
                            <div class="features vd_bg-white font-sm content-list">
                                <ul class="list-wrapper">

                                    <li><i class="fa fa-edit mgr-10"></i> <b> Custom </b> Project</li>
                                    <li><i class="fa fa-file-pdf-o mgr-10"></i> <b>PDF</b> Download</li>

                                </ul>
                                <br>
                                {{Form::open(['action'=>'InvoiceController@store']) }}
                                {{Form::hidden('id_package', '1')}}
                                {{Form::hidden('id_project', $id_project)}}
                                <div class="pd-20 text-center">
                                    {{Form::submit(trans('messages.act_buy'),['class'=>'btn vd_btn vd_bg-green btn-lg'])}}
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 mgbt-20  ">
                        <div class="plan">
                            <div class="plan-header vd_bg-dark-blue vd_yellow text-center vd_info-parent">
                                <h3 class="pd-20 mgbt-xs-0">PERSONAL</h3>
                                <div class="price vd_bg-black-30"><span class="main"><span class="font-light">$</span><span class="font-bold">30</span> </span> <span class="suffix">99</span> <span class="text vd_bdt-yellow">/month</span></div>
                                <i class="caret-pos fa fa-caret-up vd_white"></i></div>
                            <div class="features vd_bg-white font-sm content-list">
                                <ul class="list-wrapper">
                                    <li><i class="fa fa-edit mgr-10"></i> <b> Custom </b> Project</li>
                                    <li><i class="fa fa-file-pdf-o mgr-10"></i> <b>PDF</b> Download</li>
                                    <li><i class="fa fa-file-word-o mgr-10"></i> <b>WORD</b> Download</li>
                                    <li><i class="fa fa-file-excel-o mgr-10"></i> <b>Excel</b> Download</li>
                                </ul>
                                <br>
                                <div class="pd-20 text-center">
                                    {{Form::open(['action'=>'InvoiceController@store']) }}
                                    {{Form::hidden('id_package', '2')}}
                                    {{Form::hidden('id_project', $id_project)}}
                                    <div class="pd-20 text-center">
                                        {{Form::submit(trans('messages.act_buy'),['class'=>'btn vd_btn vd_bg-green btn-lg'])}}
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="plan featured">
                            <div class="plan-header vd_bg-red vd_white text-center vd_info-parent">
                                <h3 class="pd-20 mgbt-xs-0">FULL</h3>
                                <div class="price vd_bg-black-30">
                                    <span class="main">
                                        <span class="font-light">$</span>
                                        <span class="font-bold">99</span> 
                                    </span>
                                    <span class="suffix">99</span> <span class="text">/month</span></div>
                                <i class="caret-pos fa fa-caret-up vd_white"></i></div>
                            <div class="features vd_bg-white font-sm content-list">
                                <ul class="list-wrapper">
                                    <li><i class="fa fa-edit mgr-10"></i> <b> Custom </b> Project</li>
                                    <li><i class="fa fa-file-pdf-o mgr-10"></i> <b>PDF</b> Download</li>
                                    <li><i class="fa fa-file-word-o mgr-10"></i> <b>WORD</b> Download</li>
                                    <li><i class="fa fa-file-excel-o mgr-10"></i> <b>Excel</b> Download</li>
                                    <li><i class="fa fa-briefcase   mgr-10"></i> <b>Professional</b> Counseling</li>
                                </ul>
                                <br>
                                <div class="pd-20 text-center"> {{Form::open(['action'=>'InvoiceController@store']) }}
                                    {{Form::hidden('id_package', '3')}}
                                    {{Form::hidden('id_project', $id_project)}}
                                    <div class="pd-20 text-center">
                                        {{Form::submit(trans('messages.act_buy'),['class'=>'btn vd_btn vd_bg-green btn-lg'])}}
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- vd_pricing-table -->

            </div>
        </div>
    </div>


@stop