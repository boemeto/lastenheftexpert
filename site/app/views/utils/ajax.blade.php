<script type="text/javascript">
    function getXMLHTTP() {
        var xmlhttp = false;
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e1) {
                    xmlhttp = false;
                }
            }
        }

        return xmlhttp;
    }
    function viewHeadquarters(id) {

        var strURL = "{{URL::to('/')}}/ajax/viewHeadquarters/" + id;

        var req = getXMLHTTP();

        if (req) {

            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {
                        document.getElementById('divViewHeadquarters').innerHTML = req.responseText;
                    } else {
                        alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                    }
                }
            };
            req.open("GET", strURL, true);
            req.send(null);
        }

    }
    function viewHeadquarterDetails(id) {

        if (id == 0) {
            document.getElementById('divViewHeadquarterDetails').innerHTML = '';
        }
        else {
            var strURL = "{{URL::to('/')}}/ajax/viewHeadquarterDetails/" + id;

            var req = getXMLHTTP();

            if (req) {

                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            document.getElementById('divViewHeadquarterDetails').innerHTML = req.responseText;
                        } else {
                            alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }
    function viewTaskModuleStructure(id) {
        if (id == 0) {
            document.getElementById('div_module_structure').innerHTML = '';
        }
        else {
            var strURL = "{{URL::to('/')}}/ajax/viewTaskModuleStructure/" + id;

            var req = getXMLHTTP();

            if (req) {

                req.onreadystatechange = function () {
                    if (req.readyState == 4) {
                        // only if "OK"
                        if (req.status == 200) {
                            document.getElementById('div_module_structure').innerHTML = req.responseText;
                        } else {
                            alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                        }
                    }
                };
                req.open("GET", strURL, true);
                req.send(null);
            }
        }
    }


</script>