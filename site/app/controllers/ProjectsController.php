<?php

use DB;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Session;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class ProjectsController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {
            $id_user = Auth::user()->id;
            $fk_group = User_groups::getGroupByUser($id_user);
            $projects = Project::getUsersProjects($id_user);
            return View::make('front.projects.index', compact('projects', 'fk_group'));
        } else {
            return App::abort(404);
        }
    }

    //  first step of create project. !!! view packages -> goes to view create
    public function create()
    {
        allowOnlyCompanyAdmin();
        $packages = Package::getActivePackages();
        return View::make('front.projects.packages', compact('packages'));
    }

    /**
     * Show the form for creating a new project.
     * !!! view projects create
     */
    public function packages()
    {
        // allowOnlyCompanyAdmin();
        // if (Input::has('id')) {
        //     $id_package = Input::get('id');
        //     $package = Package::getFirstById($id_package);
        //     if (count($package) > 0) {
        //         $package_row = Package_row::getByPackage($id_package);
        //         $id_user = Auth::user()->id;
        //         $user = User::getFirstById($id_user);
        //         $industry_types = Industry_type::all();
        //         $fk_company = $user->fk_company;
        //         $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        //         $budget = Dictionary::getOptions(3);
        //         $users_projects = User::getUsersByCompany($fk_company, $id_user);
        //         $terms = Posts::getFirstBySlug('terms_cond');
        //         return View::make('front.projects.create', compact('industry_types', 'user', 'users_projects', 'headquarters', 'budget', 'terms', 'package', 'package_row'));
        //     }
        // }
        // return App::abort(404);
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $industry_types = Industry_type::all();
        $packages = Package::all();
        $fk_company = $user->fk_company;
        $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        $budget = Dictionary::getOptions(3);
        $users_projects = User::getUsersByCompany($fk_company, $id_user);
        $terms = Posts::getFirstBySlug('terms_cond');
        return View::make('front.projects.create', compact('industry_types', 'user', 'users_projects', 'headquarters', 'budget', 'terms', 'packages'));
    }

    /**
     * Store a newly created project in storage.
     *
     * @return Response
     */
    public function store()
    {
        // dd(Input::all());
        if (Input::has('id_package')) {
            $id_package = Input::get('id_package');
            $package = Package::getFirstById($id_package);
            if (count($package) > 0) {
                $currDate = date('Y-m-d');

                $pack_period = '+' . $package->period . ' ';
                if ($package->period_type == 1 || $package->period_type == 2) {
                    $pack_period .= 'months';
                }
                if ($package->period_type == 3 || $package->period_type == 4) {
                    $pack_period .= 'years';
                }

                $endDate = date('Y-m-d', strtotime($pack_period, strtotime($currDate)));
                $fk_industry = Input::get('fk_industry');
                $id_user = Auth::user()->id;
                $user = User::getFirstById($id_user);
                $last_project = Project::max('id_project') + 1;
                $length = strlen($last_project);
                $empty_project = $package->fk_type_project;
                $zeroes = 5 - $length;
                $name = Input::get('name_project');
                $project = new Project();
                $project->name_project = $name;
                $project->fk_industry = Input::get('fk_industry');
                $project->fk_user = $id_user;
                $project->fk_company = $user->fk_company;
                $project->nr_employees = Input::get('nr_employees');
                $project->nr_users = Input::get('nr_users');
                $project->max_budget = Input::get('max_budget');
                $project->start_project = Input::get('start_project');
                $project->implement_time = Input::get('implement_time');
                $project->due_date = $endDate;
                if ($empty_project == 2) {
                    $project->is_empty_project = 1;
                } else {
                    $project->is_empty_project = 0;
                }

                $project->save();
                $id_project = $project->id_project;
                $user_projects = new User_projects();
                $user_projects->fk_user = $id_user;
                $user_projects->fk_project = $id_project;
                $user_projects->is_owner = 1;
                $user_projects->save();
                if (count(Input::get('fk_user')) > 0) {
                    foreach (Input::get('fk_user') as $fk_user) {
                        $user_projects = new User_projects();
                        $user_projects->fk_user = $fk_user;
                        $user_projects->fk_project = $id_project;
                        $user_projects->save();
                    }
                }

                if ($empty_project == 1) {
                    $checked_tasks = Ms_task::getCheckedTaskByIndustry($fk_industry);
                    foreach ($checked_tasks as $task) {
                        if($task->is_default == 1) {
                            $projects_rows = new Projects_rows();
                            $projects_rows->fk_project = $id_project;
                            $projects_rows->fk_user = $id_user;
                            $projects_rows->fk_ms_task = $task->id_ms_task;
                            $projects_rows->is_checked_user = 1;
                            $projects_rows->save();
                        }
                    }
                } elseif ($empty_project == 2) {
                    $empty_project_soft = 5;
                    $module_structures = Module_structure::where('fk_software', '=', $empty_project_soft)
                        ->where('fk_project', '=', null)->orderBy('id_module_structure')->get();
                    foreach ($module_structures as $ms) {
                        $project_ms = new Module_structure();
                        $project_ms->fk_project = $id_project;
                        $project_ms->fk_id = $ms->fk_id;
                        $project_ms->fk_software = $ms->fk_software;
                        $project_ms->corespondent_module_structure = $ms->id_module_structure;

                        $parent_ms = Module_structure::where('corespondent_module_structure', '=', $ms->fk_parent)
                            ->where('fk_project', '=', $id_project)->first();
                        if (count($parent_ms) == 1) {
                            $project_ms->fk_parent = $parent_ms->id_module_structure;
                        } else {
                            $project_ms->fk_parent = 0;
                        }

                        $project_ms->id_type = $ms->id_type;
                        $project_ms->level = $ms->level;
                        $project_ms->fk_module_parent = $ms->fk_module_parent;
                        $project_ms->ms_order = $ms->ms_order;
                        $project_ms->is_default = 0;
                        $project_ms->save();
                        $id_project_ms = $project_ms->id_module_structure;
                        $ms_tasks = Ms_task::where('fk_module_structure', '=', $ms->id_module_structure)
                            ->where('fk_project', '=', null)->get();
                        foreach ($ms_tasks as $ms_task) {
                            $project_ms_task = new Ms_task();
                            $project_ms_task->fk_task = $ms_task->fk_task;
                            $project_ms_task->fk_pair = $ms_task->fk_pair;
                            $project_ms_task->fk_company = $user->fk_company;
                            $project_ms_task->fk_project = $id_project;
                            $project_ms_task->fk_module_structure = $id_project_ms;
                            $project_ms_task->task_order = $ms_task->task_order;
                            $project_ms_task->is_default = 0;
                            $project_ms_task->save();
                        }
                    }
                }

                //sending STRIPE charge
                $price = $package->price*1.19;
                $apiKey = Config::get('stripe.stripe.secret');
                $stripe = Stripe::make($apiKey);
                $token = Input::all()['stripeToken'];

                $customer = $stripe->customers()->create([
                                'email' => Input::all()['stripeEmail'],
                                'source'  => $token
                ]);

                $charge = $stripe->charges()->create([
                    'customer' => $customer['id'],
                    'currency' => 'EUR',
                    'amount'   => $price,
                ]);

                $date = date('Y.m.d');
                $id_company = $user->fk_company;

                $tva = 0.19 * $package->price;

                $invoice = new Invoice();
                $invoice->inv_serial = 'LE';
                $invoice->inv_number = Invoice::getInvoiceNumber();
                $invoice->inv_date = $date;
                $invoice->fk_company = $id_company;
                $invoice->fk_project = $id_project;
                $invoice->fk_user = $id_user;
                $invoice->unit_price = $package->price;
                $invoice->payment_term = $package->payment_interval;
                $invoice->sum_paid = $package->price + $tva;
                $invoice->fk_status = 3;
                $invoice->initial_date = $date;
                $invoice->end_date = $project->due_date;
                $invoice->invoice_period = $package->period;
                $invoice->payment_method = 'card';
                $invoice->save();

                $projects_packages = new Projects_package();
                $projects_packages->is_base_package = $package->is_base_package;
                $projects_packages->fk_project	    = $project->id_project;
                $projects_packages->fk_package	    = $package->id_package;

                $projects_packages->save();

                return Redirect::action('ProjectsController@show', ['id' => $id_project]);
            }
        }
        return App::abort(404);
    }

    public function updateProjectStatus()
    {
        $project = Project::getFirstById(Input::get('project_id'));
        $project->status = Input::get('project_status');
        $project->save();
    }

    public function storeInactiveProject()
    {
        // dd(Input::all());
        if (Input::has('id_package')) {
            $id_package = Input::get('id_package');
            $package = Package::getFirstById($id_package);
            if (count($package) > 0) {
                $currDate = date('Y-m-d');
                $pack_period = '+' . $package->period . ' ';
                if ($package->period_type == 1 || $package->period_type == 2) {
                    $pack_period .= 'months';
                }
                if ($package->period_type == 3 || $package->period_type == 4) {
                    $pack_period .= 'years';
                }
                $endDate = date('Y-m-d', strtotime($pack_period, strtotime($currDate)));
                $fk_industry = Input::get('fk_industry');
                $id_user = Auth::user()->id;
                $user = User::getFirstById($id_user);
                $last_project = Project::max('id_project') + 1;
                $length = strlen($last_project);
                $empty_project = $package->fk_type_project;
                $zeroes = 5 - $length;
                $name = Input::get('name_project');
                $project = new Project();
                $project->name_project = $name;
                $project->fk_industry = Input::get('fk_industry');
                $project->fk_user = $id_user;
                $project->fk_company = $user->fk_company;
                $project->nr_employees = Input::get('nr_employees');
                $project->nr_users = Input::get('nr_users');
                $project->max_budget = Input::get('max_budget');
                $project->start_project = Input::get('start_project');
                $project->implement_time = Input::get('implement_time');
                $project->due_date = $endDate;
                $project->status = 2;
                if ($empty_project == 2) {
                    $project->is_empty_project = 1;
                } else {
                    $project->is_empty_project = 0;
                }
                $project->save();

                $id_project = $project->id_project;
                $user_projects = new User_projects();
                $user_projects->fk_user = $id_user;
                $user_projects->fk_project = $id_project;
                $user_projects->is_owner = 1;
                $user_projects->save();
                if (count(Input::get('fk_user')) > 0) {
                    foreach (Input::get('fk_user') as $fk_user) {
                        $user_projects = new User_projects();
                        $user_projects->fk_user = $fk_user;
                        $user_projects->fk_project = $id_project;
                        $user_projects->save();
                    }
                }

                if ($empty_project == 1) {
                    $checked_tasks = Ms_task::getCheckedTaskByIndustry($fk_industry);
                    foreach ($checked_tasks as $task) {
                        $projects_rows = new Projects_rows();
                        $projects_rows->fk_project = $id_project;
                        $projects_rows->fk_user = $id_user;
                        $projects_rows->fk_ms_task = $task->id_ms_task;
                        $projects_rows->is_checked_user = 1;
                        $projects_rows->save();
                    }

                } elseif ($empty_project == 2) {
                    $empty_project_soft = 5;
                    $module_structures = Module_structure::where('fk_software', '=', $empty_project_soft)
                        ->where('fk_project', '=', null)->orderBy('id_module_structure')->get();
                    foreach ($module_structures as $ms) {
                        $project_ms = new Module_structure();
                        $project_ms->fk_project = $id_project;
                        $project_ms->fk_id = $ms->fk_id;
                        $project_ms->fk_software = $ms->fk_software;
                        $project_ms->corespondent_module_structure = $ms->id_module_structure;

                        $parent_ms = Module_structure::where('corespondent_module_structure', '=', $ms->fk_parent)
                            ->where('fk_project', '=', $id_project)->first();
                        if (count($parent_ms) == 1) {
                            $project_ms->fk_parent = $parent_ms->id_module_structure;
                        } else {
                            $project_ms->fk_parent = 0;
                        }

                        $project_ms->id_type = $ms->id_type;
                        $project_ms->level = $ms->level;
                        $project_ms->fk_module_parent = $ms->fk_module_parent;
                        $project_ms->ms_order = $ms->ms_order;
                        $project_ms->is_default = 0;
                        $project_ms->save();
                        $id_project_ms = $project_ms->id_module_structure;
                        $ms_tasks = Ms_task::where('fk_module_structure', '=', $ms->id_module_structure)
                            ->where('fk_project', '=', null)->get();
                        foreach ($ms_tasks as $ms_task) {
                            $project_ms_task = new Ms_task();
                            $project_ms_task->fk_task = $ms_task->fk_task;
                            $project_ms_task->fk_pair = $ms_task->fk_pair;
                            $project_ms_task->fk_company = $user->fk_company;
                            $project_ms_task->fk_project = $id_project;
                            $project_ms_task->fk_module_structure = $id_project_ms;
                            $project_ms_task->task_order = $ms_task->task_order;
                            $project_ms_task->is_default = 0;
                            $project_ms_task->save();
                        }
                    }
                }
                $date = date('Y.m.d');
                $id_company = $user->fk_company;

                $tva = 0.19 * $package->price;

                $invoice = new Invoice();
                $invoice->inv_serial = 'LE';
                $invoice->inv_number = Invoice::getInvoiceNumber();
                $invoice->inv_date = $date;
                $invoice->fk_company = $id_company;
                $invoice->fk_project = $id_project;
                $invoice->fk_user = $id_user;
                $invoice->unit_price = $package->price;
                $invoice->payment_term = $package->payment_interval;
                $invoice->sum_paid = $package->price + $tva;
                $invoice->fk_status = 1;
                $invoice->initial_date = $date;
                $invoice->end_date = $project->due_date;
                $invoice->invoice_period = $package->period;
                $invoice->payment_method = 'bank transfer';
                $invoice->save();

                $projects_packages = new Projects_package();
                $projects_packages->is_base_package = $package->is_base_package;
                $projects_packages->fk_project	    = $project->id_project;
                $projects_packages->fk_package	    = $package->id_package;

                $projects_packages->save();

                return Redirect::action('ProjectsController@show', ['id' => $id_project]);
            }
        }
        return App::abort(404);
    }

    //paypal payment failed or canceled
    public function getPaymentPaypalStatus()
    {
        // echo "<pre>"; dd(Input::all());
        return Redirect::to('/');
        // dd(Session::get($id_package));
        // return Redirect::action('ProjectsController@packages', ['id' => $id]);
    }

    private $_api_context;
    /**
     * Store a newly created project in storage, payied with paypal.
     *
     * @return Response
     */
    public function storePaypal()
    {
        // echo "<pre>"; dd(Input::all());
        if (Input::get('data')['id_package'] != '') {
            $id_package = Input::get('data')['id_package'];
            $package = Package::getFirstById($id_package);
            if (count($package) > 0) {
                $currDate = date('Y-m-d');

                $pack_period = '+' . $package->period . ' ';
                if ($package->period_type == 1 || $package->period_type == 2) {
                    $pack_period .= 'months';
                }
                if ($package->period_type == 3 || $package->period_type == 4) {
                    $pack_period .= 'years';
                }

                $endDate = date('Y-m-d', strtotime($pack_period, strtotime($currDate)));
                $fk_industry = Input::get('data')['fk_industry'];
                $id_user = Auth::user()->id;
                $user = User::getFirstById($id_user);
                $last_project = Project::max('id_project') + 1;
                $length = strlen($last_project);
                $empty_project = $package->fk_type_project;
                $zeroes = 5 - $length;
                $name = Input::get('data')['name_project'];
                $project = new Project();
                $project->name_project = $name;
                $project->fk_industry = Input::get('data')['fk_industry'];
                $project->fk_user = $id_user;
                $project->fk_company = $user->fk_company;
                $project->nr_employees = Input::get('data')['nr_employees'];
                $project->nr_users = Input::get('data')['nr_users'];
                $project->max_budget = Input::get('data')['max_budget'];
                $project->start_project = Input::get('data')['start_project'];
                $project->implement_time = Input::get('data')['implement_time'];
                $project->due_date = $endDate;
                if ($empty_project == 2) {
                    $project->is_empty_project = 1;
                } else {
                    $project->is_empty_project = 0;
                }

                $project->save();
                $id_project = $project->id_project;
                $user_projects = new User_projects();
                $user_projects->fk_user = $id_user;
                $user_projects->fk_project = $id_project;
                $user_projects->is_owner = 1;
                $user_projects->save();

                $fk_users = json_decode(Input::get('data')['fk_user']);
                if ($fk_users > 0) {
                    foreach ($fk_users as $fk_user) {
                        $user_projects = new User_projects();
                        $user_projects->fk_user = $fk_user;
                        $user_projects->fk_project = $id_project;
                        $user_projects->save();
                    }
                }

                if ($empty_project == 1) {
                    $checked_tasks = Ms_task::getCheckedTaskByIndustry($fk_industry);
                    foreach ($checked_tasks as $task) {
                        $projects_rows = new Projects_rows();
                        $projects_rows->fk_project = $id_project;
                        $projects_rows->fk_user = $id_user;
                        $projects_rows->fk_ms_task = $task->id_ms_task;
                        $projects_rows->is_checked_user = 1;
                        $projects_rows->save();
                    }

                } elseif ($empty_project == 2) {
                    $empty_project_soft = 5;
                    $module_structures = Module_structure::where('fk_software', '=', $empty_project_soft)
                        ->where('fk_project', '=', null)->orderBy('id_module_structure')->get();
                    foreach ($module_structures as $ms) {
                        $project_ms = new Module_structure();
                        $project_ms->fk_project = $id_project;
                        $project_ms->fk_id = $ms->fk_id;
                        $project_ms->fk_software = $ms->fk_software;
                        $project_ms->corespondent_module_structure = $ms->id_module_structure;

                        $parent_ms = Module_structure::where('corespondent_module_structure', '=', $ms->fk_parent)
                            ->where('fk_project', '=', $id_project)->first();
                        if (count($parent_ms) == 1) {
                            $project_ms->fk_parent = $parent_ms->id_module_structure;
                        } else {
                            $project_ms->fk_parent = 0;
                        }

                        $project_ms->id_type = $ms->id_type;
                        $project_ms->level = $ms->level;
                        $project_ms->fk_module_parent = $ms->fk_module_parent;
                        $project_ms->ms_order = $ms->ms_order;
                        $project_ms->is_default = 0;
                        $project_ms->save();
                        $id_project_ms = $project_ms->id_module_structure;
                        $ms_tasks = Ms_task::where('fk_module_structure', '=', $ms->id_module_structure)
                            ->where('fk_project', '=', null)->get();
                        foreach ($ms_tasks as $ms_task) {
                            $project_ms_task = new Ms_task();
                            $project_ms_task->fk_task = $ms_task->fk_task;
                            $project_ms_task->fk_pair = $ms_task->fk_pair;
                            $project_ms_task->fk_company = $user->fk_company;
                            $project_ms_task->fk_project = $id_project;
                            $project_ms_task->fk_module_structure = $id_project_ms;
                            $project_ms_task->task_order = $ms_task->task_order;
                            $project_ms_task->is_default = 0;
                            $project_ms_task->save();
                        }
                    }
                }

                $date = date('Y.m.d');
                $id_company = $user->fk_company;

                $tva = 0.19 * $package->price;

                $invoice = new Invoice();
                $invoice->inv_serial = 'LE';
                $invoice->inv_number = Invoice::getInvoiceNumber();
                $invoice->inv_date = $date;
                $invoice->fk_company = $id_company;
                $invoice->fk_project = $id_project;
                $invoice->fk_user = $id_user;
                $invoice->unit_price = $package->price;
                $invoice->payment_term = $package->payment_interval;
                $invoice->sum_paid = $package->price + $tva;
                $invoice->fk_status = 3;
                $invoice->initial_date = $date;
                $invoice->end_date = $project->due_date;
                $invoice->invoice_period = $package->period;
                $invoice->payment_method = 'paypal';
                $invoice->save();

                $projects_packages = new Projects_package();
                $projects_packages->is_base_package = $package->is_base_package;
                $projects_packages->fk_project	    = $project->id_project;
                $projects_packages->fk_package	    = $package->id_package;

                $projects_packages->save();

                return Redirect::action('ProjectsController@show', ['id' => $id_project]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $project = Project::getFirstById($id);

        return View::make('admin.projects.edit', compact('project'));
    }

    /**
     * Shows the form for editing the specified project.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $has_filters = 0;
        $has_tasks = 0;
        // user details
        $id_user = Auth::user()->id;

        $user = User::getFirstById($id_user);
        $id_company = $user->fk_company;
        $user_projects = User_projects::where('fk_user', '=', $id_user)
            ->where('fk_project', '=', $id)->get();
        $projects = Project::getUsersProjects($id_user);

        if (count($user_projects) == 0) {
            return App::abort(404);
        }
        $user_projects->fk_user = $id_user;

        // project details
        $project = Project::whereId_project($id)->first();
        if (count($project) > 0) {
            if ($user->fk_company != $project->fk_company) {
                return App::abort(404);
            }
        } else {
            return App::abort(404);
        }

        $id_industry = $project->fk_industry;
        $industry = Industry::getFirstById($id_industry);
        $industry_type = Industry_type::getFirstById($industry->fk_type);
        $software = Module_structure::getSoftwareByIndustry($id_industry);
        $company = Company::getFirstById($project->fk_company);
        $admin_user = User::getFirstById($project->fk_user);
        $set_priority = Priority::getAllPriorities(1);

        // industry project
        $yesNo = Dictionary::getOptions();

        $priorities = Priority::getAllPriorities();
        $set_colors = Task_colors::all();
        $pairs = Task_pair::all();
        $custom_task = Dictionary::getOptions(4, 1);
        $set_checked = Dictionary::getOptions(5, 1);
        $set_attach = Dictionary::getOptions(6, 1);
        $status = is_project_expired($id);

        $export_file = Project_export_files::getLastByProjectId($project->id_project);

        if ($status == 2 || $status == 3) {
            $terms = Posts::getFirstBySlug('terms_cond');
            $main_pack_project = Package::getPackagesByProject($id, 1);
            $package = Projects_package::getCurrentPackage($main_pack_project->fk_package);
            $current_package = Package::getFirstById($main_pack_project->fk_package);
            $packages = Package::getActivePackages();
            $subpackages = Subpackages::getSubpackages($current_package->id_package);
            $project_invoices = Invoice::getInvoicesByProject($id);
            return View::make('front.projects.project_expired', compact('project', 'industry', 'terms', 'industry_type', 'company', 'admin_user',
            'project_invoices', 'status', 'main_pack_project', 'current_package', 'packages', 'subpackages', 'export_file', 'projects'));
        }


        if (Input::has('fk_checked')) {
            $has_filters = 1;
            $fk_checked = Input::get('fk_checked');
        } else {
            $fk_checked = '';
        }
        if (Input::has('fk_attach')) {
            $has_filters = 1;
            $fk_attach = Input::get('fk_attach');
        } else {
            $fk_attach = '';
        }
        if (Input::has('is_default')) {
            $has_filters = 1;
            $is_default = Input::get('is_default');
        } else {
            $is_default = null;
        }
        if (Input::has('meta_tags')) {
            $has_filters = 1;
            $meta_tags = Input::get('meta_tags');
        } else {
            $meta_tags = '';
        }
        if (Input::has('address_code')) {
            $has_filters = 1;
            $address_code = Input::get('address_code');
        } else {
            $address_code = '';
        }
        if (Input::has('fk_pair')) {
            $has_filters = 1;
            $fk_pair = Input::get('fk_pair');
        } else {
            $fk_pair = 0;
        }
        if (Input::has('fk_priority')) {
            $has_filters = 1;
            $fk_priority = Input::get('fk_priority');
        } else {
            $fk_priority = 0;
        }
        if (Input::has('color')) {
            $has_filters = 1;
            $color = Input::get('color');
        } else {
            $color = '';
        }

        $main_pack_project = Package::getPackagesByProject($id, 1);

        $package = Projects_package::getCurrentPackage($main_pack_project->fk_package);
        $current_package = Package::getFirstById($main_pack_project->fk_package);
        $subpackages = Subpackages::getSubpackages($current_package->id_package);
        $terms = Posts::getFirstBySlug('terms_cond');
        $project_invoices = Invoice::getInvoicesByProject($id);

        if ($project->is_empty_project == 1) {
            $id_software = 5;
            $module_structure = Module_structure::getModuleByLvlWithDefault($id_software, 1, 0, $id);
            return View::make('front.projects.edit', compact('project', 'id_company', 'user', 'industry', 'set_priority',
                'industry_type', 'module_structure', 'id_software', 'admin_user', 'company', 'projects',
                'yesNo', 'priorities', 'pairs', 'meta_tags', 'address_code', 'fk_pair', 'fk_priority', 'color',
                'set_colors', 'has_filters', 'fk_attach', 'fk_checked', 'set_checked', 'set_attach', 'has_tasks', 'project_invoices',
                 'subpackages', 'terms', 'current_package', 'export_file'
            ));
        } else {
            return View::make('front.projects.show', compact('project', 'id_company', 'user', 'industry', 'set_priority',
                'industry_type', 'software', 'admin_user', 'company', 'has_filters', 'projects',
                'yesNo', 'priorities', 'pairs', 'meta_tags', 'address_code', 'fk_pair', 'fk_priority', 'color', 'project_invoices',
                'set_colors', 'custom_task', 'is_default', 'fk_attach', 'fk_checked', 'set_checked', 'set_attach', 'has_tasks', 'subpackages',
                 'terms', 'current_package', 'export_file'));
        }
    }

    public function download_project_pdf($id_project)
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);

        // project details
        $project = Project::getFirstById($id_project);

        $id_industry = $project->fk_industry;
        $fk_company = $project->fk_company;
        $industry = Industry::getFirstById($id_industry);
        $company = Company::getFirstById($fk_company);

        $export_file = new Project_export_files();
        $export_file->file_name = $project->name_project.'.pdf';
        $export_file->file_type = 'pdf';
        $export_file->project_id = $id_project;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        $industry_type = Industry_type::getFirstById($industry->fk_type);

        if ($project->is_empty_project == 1) {
            $id_software = 5;
            $software = Software::getFirstById($id_software);
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, $id_project);

            $view = View::make('front.pdf_pages.download_pdf_empty_projects',
                compact('company', 'project', 'user', 'industry_type', 'industry', 'id_project', 'id_industry',
                    'headquarters', 'module_structure', 'software', 'id_software'));

            $timestamp = time();
            File::put(public_path() . '/app/storage/'.$timestamp.'_pdf.html', $view);

            $options = array(
                'disable-smart-shrinking'
            );
            $pdf = new mikehaertl\wkhtmlto\Pdf($options);
            $pdf->binary = 'wkhtmltopdf';

            $pdf->addPage(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

            $return =  $pdf->send();
            unlink(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

            if (!$return) {
                throw new Exception('Could not create PDF: '.$pdf->getError());
            } else {
                return $return;
            }
        } else {
            $software = Module_structure::getSoftwareByIndustry($id_industry);
            $view = View::make('front.pdf_pages.download_pdf',
                compact('company', 'project', 'user', 'industry_type', 'industry', 'id_project', 'id_industry',
                    'headquarters', 'module_structure', 'software'));

            $timestamp = time();
            File::put(public_path() . '/app/storage/'.$timestamp.'_pdf.html', $view);

            $options = array(
                'disable-smart-shrinking'
            );
            $pdf = new mikehaertl\wkhtmlto\Pdf($options);
            $pdf->binary = 'wkhtmltopdf';

            $pdf->addPage(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

            $return =  $pdf->send();
            unlink(public_path() . '/app/storage/'.$timestamp.'_pdf.html');

            if (!$return) {
                throw new Exception('Could not create PDF: '.$pdf->getError());
            } else {
                return $return;
            }
        }
    }

    // download project in word format
    public function download_project_word($id_project)
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);

        // project details
        $project = Project::getFirstById($id_project);
        $id_industry = $project->fk_industry;
        $fk_company = $project->fk_company;
        $industry = Industry::getFirstById($id_industry);
        $company = Company::getFirstById($fk_company);
        $image = Company::getLogoCompany($company->id_company);
        $image_url = URL::asset($image);

        $export_file = new Project_export_files();
        $export_file->file_name = $project->name_project.'.doc';
        $export_file->file_type = 'doc';
        $export_file->project_id = $id_project;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        $industry_type = Industry_type::getFirstById($industry->fk_type);

        if ($project->is_empty_project == 1) {
            $id_software = 5;
            $software = Software::getFirstById($id_software);
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, $id_project);

            $view = View::make('front.word_pages.download_word_empty_projects',
                compact('company', 'project', 'user', 'industry_type', 'industry', 'id_project', 'id_industry',
                    'headquarters', 'module_structure', 'software', 'id_software'));
        } else {
            $software = Module_structure::getSoftwareByIndustry($id_industry);
            $view =  View::make('front.word_pages.download_word',
                compact('company', 'project', 'user', 'industry_type', 'industry', 'id_project', 'id_industry',
                    'headquarters', 'module_structure', 'software', 'id_software'));
        }

        $timestamp = time();
        File::put(public_path() . '/app/storage/'.$timestamp.'_word.html', $view);

        $file= public_path(). '/app/storage/'.$timestamp.'_word.html';

        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        );

        return Response::download($file, $project->name_project.'.doc', $headers)->deleteFileAfterSend(true);
    }

    // download project attachments in zip format
    public function download_project_attachments($id_project) {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);

        // project details
        $project = Project::getFirstById($id_project);
        $id_industry = $project->fk_industry;
        $fk_company = $project->fk_company;
        $industry = Industry::getFirstById($id_industry);
        $company = Company::getFirstById($fk_company);
        $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        $industry_type = Industry_type::getFirstById($industry->fk_type);

        if ($project->is_empty_project == 1) {
            $id_software = 5;
            $software = Software::getFirstById($id_software);
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, $id_project);
            // Prepare File
            Session::forget('check_for_attachments');
            $data = array(
                'company' => $company->toArray(),
                'user' => $user->toArray(),
                'project' => $project->toArray(),
                'industry_type' => $industry_type->toArray(),
                'industry' => $industry->toArray(),
                'software' => $software->toArray(),
                'id_software' => $id_software,
                'id_industry' => $id_industry,
                'id_project' => $id_project,
                'module_structure' => $module_structure,
            );
            Module_structure::getEmptyProjectAttachmentsZip($data['module_structure'], $data['id_software'], $data['id_industry'], 1, $data['id_project'], [], 1, [], 'check');
            if(Session::has('check_for_attachments')) {
                $file = tempnam("tmp", "zip");
                $zip = new ZipArchive();
                $zip->open($file, ZipArchive::OVERWRITE);

                Session::forget('task_cell_attachments');
                Session::put('task_cell_attachments', 1);
                Session::forget('rows_attachments');
                Session::put('rows_attachments', 2);
                Session::forget('files_attachments');
                Session::put('files_attachments', 0);
                Module_structure::getEmptyProjectAttachmentsZip($data['module_structure'], $data['id_software'], $data['id_industry'], 1, $data['id_project'], [], 1, [], $zip);
                $zip->close();
                header('Content-Type: application/zip');
                header('Content-Length: ' . filesize($file));
                header('Content-Disposition: attachment; filename="'.$project->name_project.' attachments.zip"');
                readfile($file);
                unlink($file);
            } else {
                return Redirect::back();
            }
        } else {
            $software = Module_structure::getSoftwareByIndustry($id_industry);
            $data = array(
                'company' => $company->toArray(),
                'user' => $user->toArray(),
                'project' => $project->toArray(),
                'industry_type' => $industry_type->toArray(),
                'industry' => $industry->toArray(),
                'software' => $software->toArray(),
                'id_software' => $id_software,
                'id_industry' => $id_industry,
                'id_project' => $id_project,
                'module_structure' => $module_structure,
            );
            Session::forget('check_for_attachments');
            foreach($data['software'] as $soft) {
                $id_software = $soft['id_software'];
                $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software, $data['id_project']);
                Module_structure::getAttachmentsZip($module_structure, $id_software, $data['id_industry'], 1, $data['id_project'], 'check');
            }
            if(Session::has('check_for_attachments')) {
                $file = tempnam("tmp", "zip");
                $zip = new ZipArchive();
                $zip->open($file, ZipArchive::OVERWRITE);
                Session::forget('task_cell_attachments');
                Session::put('task_cell_attachments', 1);
                Session::forget('rows_attachments');
                Session::put('rows_attachments', 2);
                Session::forget('rows_attachments');
                Session::put('rows_attachments', 2);
                foreach($data['software'] as $soft) {
                    $id_software = $soft['id_software'];
                    $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software, $data['id_project']);
                    Module_structure::getAttachmentsZip($module_structure, $id_software, $data['id_industry'], 1, $data['id_project'], $zip);
                }

                $zip->close();
                header('Content-Type: application/zip');
                header('Content-Length: ' . filesize($file));
                header('Content-Disposition: attachment; filename="'.$project->name_project.' attachments.zip"');
                readfile($file);
                unlink($file);
            } else {
                return Redirect::back();
            }
        }
    }

    public function download_project_excel($id_project) {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        // project details
        $project = Project::getFirstById($id_project);

        $id_industry = $project->fk_industry;
        $fk_company = $project->fk_company;
        $industry = Industry::getFirstById($id_industry);
        $company = Company::getFirstById($fk_company);

        $headquarters = Company_headquarters::getHeadquartersLocations($fk_company);
        $industry_type = Industry_type::getFirstById($industry->fk_type);

        $export_file = new Project_export_files();
        $export_file->file_name = $project->name_project.'.xls';
        $export_file->file_type = 'xls';
        $export_file->project_id = $id_project;
        $export_file->exported_by = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $timezone = date_default_timezone_get('Europe/Berlin');
        $date = date('Y-m-d h:m:s', time());
        $export_file->date = $date;
        $export_file->save();

        if ($project->is_empty_project == 1) {
            $id_software = 5;
            $software = Software::getFirstById($id_software);
            $module_structure = Module_structure::getModuleByLvl($id_software, 1, 0, $id_project);
            // Prepare File
            $data = array(
                'company' => $company->toArray(),
                'user' => $user->toArray(),
                'project' => $project->toArray(),
                'industry_type' => $industry_type->toArray(),
                'industry' => $industry->toArray(),
                'software' => $software->toArray(),
                'id_software' => $id_software,
                'id_industry' => $id_industry,
                'id_project' => $id_project,
                'module_structure' => $module_structure,
            );

            $excelFile = Excel::create($project->name_project, function($excel) use ($data) {
                $excel->sheet('1', function($sheet) use ($data) {
                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setBackground('#808080');
                        $cells->setFontColor('#ffffff');
                        $cells->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                        ));
                    });

                    Session::forget('task_cell');
                    Session::put('task_cell', 1);
                    Session::forget('excel_rows');
                    Session::put('excel_rows', 2);
                    Session::forget('excel_files');
                    Session::put('excel_files', 0);
                    Module_structure::printTreeEXCELDetailsEmptyProjectTaskCell($data['module_structure'], $data['id_software'], $data['id_industry'], 1, $data['id_project'], [], 1, [], $sheet);
                    $mod = [];
                    for($i = 1; $i < Session::get('task_cell')+1; $i++) {
                        $mod[] = 'EBENE '.$i;
                    }
                    $mod[] = 'BESCHREIBUNG';
                    $mod[] = 'GEWICHTUNG';
                    for($i = 1; $i < Session::get('excel_files')+1; $i++) {
                        $mod[] = $i . '. ANALGE';
                    }
                    $sheet->row(1, $mod);
                    Session::forget('excel_rows');
                    Session::put('excel_rows', 2);
                    $moduleStructure = Module_structure::printTreeEXCELDetailsEmptyProject($data['module_structure'], $data['id_software'], $data['id_industry'], 1, $data['id_project'], [], 1, [], $sheet);
                });
            })->export('xls');
        } else {
            $software = Module_structure::getSoftwareByIndustry($id_industry);

//            $file = tempnam("tmp", "zip");
//            $zip = new ZipArchive();
//            $zip->open($file, ZipArchive::OVERWRITE);

            $data = array(
                'company' => $company->toArray(),
                'user' => $user->toArray(),
                'project' => $project->toArray(),
                'industry_type' => $industry_type->toArray(),
                'industry' => $industry->toArray(),
                'software' => $software->toArray(),
                'id_software' => $id_software,
                'id_industry' => $id_industry,
                'id_project' => $id_project,
                'module_structure' => $module_structure,
            );

            $excelFile = Excel::create($project->name_project, function($excel) use ($data) {
                $excel->sheet('1', function($sheet) use ($data) {
                    $sheet->cells('A1:Z1', function($cells) {
                        $cells->setBackground('#808080');
                        $cells->setFontColor('#ffffff');
                        $cells->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                        ));
                    });

                    Session::forget('task_cell');
                    Session::put('task_cell', 1);
                    Session::forget('excel_rows');
                    Session::put('excel_rows', 2);
                    Session::forget('excel_files');
                    Session::put('excel_files', 0);
                    Session::forget('excel_check_tasks');
                    Session::put('excel_check_tasks', null);

                    foreach($data['software'] as $soft) {
                        $id_software = $soft['id_software'];
                        $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software, $data['id_project']);
                        $newRow = Session::get('excel_rows') + 1;
                        Module_structure::printTreeExcelDetailsTaskCell($module_structure, $id_software, $data['id_industry'], 1, $data['id_project'], $sheet);
                    }

                    $mod = [];
                    $msCheck=[];
                    // dd(Session::get('excel_check_tasks'));
                    $task_column = 1;
                    for($i = 1; $i < Session::get('task_cell')+2; $i++) {
                        $mod[] = 'EBENE '.$i;
                        $task_column = $i;
                    }
                    $mod[] = 'BESCHREIBUNG';
                    $mod[] = 'GEWICHTUNG';
                    for($i = 1; $i < Session::get('excel_files')+1; $i++) {
                        $mod[] = $i . '. ANALGE';
                    }

                    $sheet->row(1, $mod);
                    Session::forget('excel_rows');
                    Session::put('excel_rows', 2);
                    foreach($data['software'] as $soft) {
                        $id_software = $soft['id_software'];
                        $module_structure = Module_structure::getModuleByIndustryParent($data['id_industry'], $id_software, $data['id_project']);
                        $newRow = Session::get('excel_rows') + 1;
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                        Session::put('excel_rows', $newRow);
                        $sheet->row($newRow, array($soft['name_software']));
                        Module_structure::printTreeExcelDetails($module_structure, $id_software, $data['id_industry'], 1, $data['id_project'], $sheet);
                    }
                });
            })->export('xls');
        }
    }

    public function download_word_html($id_project)
    {
        return View::make('front.pdf_pages.download_word');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update_due_date()
    {
        //dd(Input::all()['package_id']);
        $newPackageId = Input::all()['package_id'];
        $projectId = Input::all()['project_id'];
        $date = date('d.m.Y');
        $project = Project::getFirstById($projectId);

        $nextDate = date('Y-m-d', strtotime("+$project->month_available months", strtotime($date)));
        // $project->due_date = $nextDate;
        // $project->status = 1;
        // $project->update();

        $id_package = Package::getPackagesByProject($project->id_project, 1)->fk_package;
        $package = Package::getFirstById($id_package);

        $projectPackage = Projects_package::getCustomItem($projectId, $id_package);
        $projectPackage->fk_package = $newPackageId;
        $projectPackage->update();

        $id_company = $user->fk_company;
        $tva = 0.19 * $package->price;

        $dateInvoice = date('Y.m.d');
        $invoice = new Invoice();
        $invoice->inv_serial = 'LE';
        $invoice->inv_number = Invoice::getInvoiceNumber();
        $invoice->inv_date = $dateInvoice;
        $invoice->fk_company = $project->fk_company;
        $invoice->fk_project = $project->id_project;
        $invoice->fk_user = $project->fk_user;
        $invoice->unit_price = $package->price;
        $invoice->payment_term = $package->payment_interval;
        $invoice->sum_paid = $package->price + $tva;
        $invoice->save();

        $due_date = date('d.m.Y', strtotime($project->due_date));
        return $due_date;
    }

    public function update($id)
    {
        $project = Project::getFirstById($id);
        Session::flash('scrollTo', 'project'.$id);
        $project->fill(Input::all());
        if (Input::has('deactivate') && Input::get('deactivate') == 1) {
            if ($project->is_deleted == 1) {
                $project->is_deleted = 0;
            } else {
                $project->is_deleted = 1;
            }
        }
        $project->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
        return Redirect::back()->withErrors($errors);
    }

    // add users to an existing project

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $project = Project::getFirstById($id);
        $project->delete();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.delete_success')]]);
        return Redirect::back()->withErrors($errors);
    }


    // remove user from an existing project

    public function projects_add_users()
    {
        $fk_project = Input::get('fk_project');
        foreach (Input::get('fk_user_project') as $fk_user) {
            $user_project = User_projects::whereFk_user($fk_user)
                ->whereFk_project($fk_project)->first();
            if (count($user_project) == 0) {
                $new_project = new User_projects();
                $new_project->fk_user = $fk_user;
                $new_project->fk_project = $fk_project;
                $new_project->save();
            }
        }
        return Redirect::Back();
    }

    public function projects_remove_users($id)
    {

        $user_project = User_projects::whereId_user_project($id)->first();
        if (count($user_project) > 0) {
            $user_project->delete();
        }

        return Redirect::Back();
    }

    /**
     * Update an existing task of the project
     */

    public function update_project_task()
    {
        // dd(Input::all());
        $id_user = Auth::user()->id;
        $fk_project = Input::get('id_project');
        $fk_ms_task = Input::get('id_ms_task');

        $projects_rows = Projects_rows::whereFk_project($fk_project)
            ->whereFk_ms_task($fk_ms_task)->first();
        if (count($projects_rows) == 0) {
            $projects_rows = new Projects_rows();
            $projects_rows->fk_project = $fk_project;
            $projects_rows->fk_ms_task = $fk_ms_task;
        }
        $projects_rows->fk_user = $id_user;
        // update different fields
        $modified = 'none';
        if (Input::has('hex')) {
            $projects_rows->task_color = Input::get('hex');
            $modified = 'Task color was modified successfully';
        }
        if (Input::has('is_checked')) {
            $projects_rows->is_checked_user = Input::get('is_checked');
            $ms_task = Ms_task::getFirstById($fk_ms_task);
            $ms_task->is_checked = Input::get('is_checked');
            $ms_task->save();
            $modified = 'Task checkbox was modified successfully';
        }
        if (Input::has('is_control')) {
            $projects_rows->is_control = Input::get('is_control');
            $modified = 'is_control';
        }
        if (Input::has('is_not_offer')) {
            $projects_rows->is_not_offer = Input::get('is_not_offer');
            $modified = 'is_not_offer';
        }
        if (Input::has('task_description')) {
            $task_description = Input::get('task_description');
            if ($task_description == '~') {
                $projects_rows->task_description = '';
            } else {
                $projects_rows->task_description = $task_description;
            }
            $modified = 'Task description was modified successfully';
        }
        if (Input::has('task_priority')) {
            $projects_rows->task_priority = Input::get('task_priority');
            $modified = 'Task priority was modified successfully';
        }
        $projects_rows->save();
        // dd($projects_rows);
        return $modified;
    }

    /**
     * Remove an attachment from the task
     */
    public function remove_file_project()
    {
        if (Input::has('id_project_attach')) {
            $id_project_attach = Input::get('id_project_attach');
            $attachment = Projects_attach::getFirstById($id_project_attach);
            if (count($attachment) > 0) {
                $fk_ms_task = Projects_attach::getIdMsTaskByAttachment($id_project_attach);
                $attachment->delete();
                return $fk_ms_task;
            }

        } else {
            $fk_ms_task = Input::get('id_ms_task');
            $fk_project = Input::get('id_project');
            $filename = Input::get('delete_file');
            $projects_rows = Projects_rows::whereFk_project($fk_project)
                ->whereFk_ms_task($fk_ms_task)->first();
            $id_project_row = $projects_rows->id_project_row;
            $attachment = Projects_attach::whereName_attachment($filename)
                ->whereFk_project_row($id_project_row)
                ->first();
            if (count($attachment) > 0) {
                $attachment->delete();
                return $fk_ms_task;
            }
        }
        return 0;
    }

    /**
     * Upload an attachment on the task
     */
    public function upload_file()
    {
        $id_user = Auth::user()->id;
        $fk_ms_task = Input::get('id_ms_task');
        $fk_project = Input::get('id_project');

        $file = Input::file('file');
        $date = date('d.m.Y');
        // check if is file
        if ($file) {

            $projects_rows = Projects_rows::whereFk_project($fk_project)
                ->whereFk_ms_task($fk_ms_task)->first();
            if (count($projects_rows) == 0) {
                $projects_rows = new Projects_rows();
                $projects_rows->fk_project = $fk_project;
                $projects_rows->fk_ms_task = $fk_ms_task;
            }
            $projects_rows->fk_user = $id_user;
            $projects_rows->save();

            $id_project_row = $projects_rows->id_project_row;
            //      $images = $file->getFilename();
            $file_name = Input::file('file')->getClientOriginalName();
            $extension = File::extension($file_name);
            $directory = 'images/attach_task/' . $fk_project;
            $filename = $file_name;

            $upload_success = Input::file('file')->move($directory, $filename);

            $attachment = Projects_attach::whereName_attachment($filename)
                ->whereFk_project_row($id_project_row)
                ->first();
            if (count($attachment) == 0) {
                $project_attach = new Projects_attach();
                $project_attach->fk_project_row = $id_project_row;
                $project_attach->name_attachment = $filename;
                $project_attach->extension = $extension;
                $project_attach->save();
            }
            return $upload_success;
        } else {
            return 0;
        }
    }

    /**
    //  * Add a custom task for an existing project
     */
    public function add_new_task()
    {
        $fk_project = Input::get('fk_project');
        $fk_industry = Input::get('fk_industry');
        $project = Project::getFirstById($fk_project);

        if (Input::has('fk_module_structure')) {
            $fk_module_structure = Input::get('fk_module_structure');
            $module_structure = Module_structure::getFirstById($fk_module_structure);
            $fk_software = $module_structure->fk_software;
        } else {
            $fk_module_structure = 0;
            $fk_software = Input::get('fk_software');
        }

        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $fk_company = $user->fk_company;

        $name_task = Input::get('name_task');
        if (Input::has('task_description')) {
            $task_description = Input::get('task_description');
        } else {
            $task_description = '';
        }

        $task = Task::getFirstByName($name_task, $task_description);
        // dd(count($task));
        if (count($task) == 0 && $name_task !== null) {
            $task = new Task();
            $task->name_task = $name_task;
            if (Input::has('name_task_en')) {
                $task->name_task_en = Input::get('name_task_en');
            }
            else {
                $task->name_task_en = $name_task;
            }

            $task->description_task = $task_description;
            if (Input::has('description_task_en')) {
                $task->description_task_en = Input::get('description_task_en');
            }
            else {
                $task->description_task_en = $task_description;
            }
            $task->code_task = '';
            $task->meta_tags = '';
            $task->address_code = '';

            $task->fk_company = $fk_company;
            $task->save();
        }

        $fk_task = $task->id_task;
        if (Input::has('structure')) {
            $structure = Input::get('structure');
            $structure_array = explode('_', $structure);
            if ($fk_software == 0) {
                if (strpos($structure_array[4], 'software') === 0) {
                    $fk_software = str_replace('software', '', $structure_array[4]);
                }
            }
            unset($structure_array[0], $structure_array[1], $structure_array[2], $structure_array[3], $structure_array[4]);

            $length = count($structure_array) + 4;
            $last_structure = 0;
            $level = 1;

            for ($i = 5; $i <= $length; $i += 2) {
                if ($structure_array[$i + 1] != 0) {
                    $last_structure = $structure_array[$i + 1];
                } else {
                    if ($last_structure == 0) {
                        $module = Module::where('name_module', '=', $structure_array[$i])->first();
                        if (count($module) == 0) {
                            $module = new Module();
                            $module->name_module = $structure_array[$i];
                            $module->is_default = 0;
                            $module->fk_company = $fk_company;
                            $module->save();
                        }
                        $id_module = $module->id_module;
                        $structure_module = Module_structure::hasPath($fk_software, $id_module, 0, $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_module;
                            $structure_module->fk_parent = 0;
                            $structure_module->id_type = 1;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $id_module;
                            $structure_module->is_default = 0;
                            $structure_module->fk_project = $fk_project;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    } else {
                        $submodule = Submodule::where('name_submodule', '=', $structure_array[$i])->first();
                        if (count($submodule) == 0) {
                            $submodule = new Submodule();
                            $submodule->name_submodule = $structure_array[$i];
                            $submodule->is_default = 0;
                            $submodule->fk_company = $fk_company;
                            $submodule->save();
                        }
                        $id_submodule = $submodule->id_submodule;
                        $module_parrent = Module_structure::getFirstById($last_structure);
                        $structure_module = Module_structure::hasPath($fk_software, $id_submodule, $last_structure,
                            $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_submodule;
                            $structure_module->fk_parent = $last_structure;
                            $structure_module->id_type = 2;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $module_parrent->fk_module_parent;
                            $structure_module->is_default = 0;
                            $structure_module->fk_project = $fk_project;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    }
                }
                $level++;
            }
            $fk_module_structure = $last_structure;
        }

        $ms_task = Ms_task::where('fk_task', '=', $fk_task)
            ->where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_project', '=', $fk_project)
            ->first();

        if (count($ms_task) == 0) {
            $ms_task = new Ms_task();
            $ms_task->fk_task = $fk_task;
            $ms_task->fk_module_structure = $fk_module_structure;
            $ms_task->is_checked = 1;
            $ms_task->is_default = 0;
            $ms_task->fk_company = $fk_company;
            $ms_task->fk_project = $fk_project;
            $ms_task->save();
        }
            // echo "<pre> ";   dd($ms_task);
        $fk_ms_task = $ms_task->id_ms_task;

        if ($project->is_empty_project !== 1) {
            $industry_ms = Industry_ms::where('fk_industry', '=', $fk_industry)
                ->where('fk_ms_task', '=', $fk_ms_task)
                ->where('fk_module_structure', '=', $fk_module_structure)
                ->first();
            if (count($industry_ms) == 0) {
                $industry_ms = new Industry_ms();
                $industry_ms->fk_industry = $fk_industry;
                $industry_ms->fk_ms_task = $fk_ms_task;
                $industry_ms->fk_module_structure = $fk_module_structure;
                $industry_ms->save();
            }
        }

        $projects_rows = Projects_rows::where('fk_project', '=', $fk_project)
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->first();
        if (count($projects_rows) == 0) {
            $projects_rows = new Projects_rows();
            $projects_rows->fk_project = $fk_project;
            $projects_rows->fk_ms_task = $fk_ms_task;
        }

        $projects_rows->fk_user = $id_user;
        $projects_rows->task_description = $task_description;
        $projects_rows->is_checked_user = 1;
        if (Input::has('set_priority')) {
            $projects_rows->task_priority = Input::get('set_priority');
        } else {
            $projects_rows->task_priority = 1;
        }
        if (Input::get('is_control') == 1) {
            $projects_rows->is_control = 1;
        } else {
            $projects_rows->is_control = 0;
        }
        if (Input::get('is_not_offer') == 1) {
            $projects_rows->is_not_offer = 1;
        } else {
            $projects_rows->is_not_offer = 0;
        }

        $projects_rows->save();

        $fk_project = Input::get('fk_project');
        $files = Input::get('files');
        // check if is file
        if ($files) {
            $id_project_row = $projects_rows->id_project_row;
            foreach($files as $file) {
                $extension = end(explode('.', $file));
                $directory = 'images/attach_task/' . $fk_project;
                $project_attach = new Projects_attach();
                $project_attach->fk_project_row = $id_project_row;
                $project_attach->name_attachment = $file;
                $project_attach->extension = $extension;
                $project_attach->save();
            }
        }

        Session::flash('scrollTo', 'panel-task'.$fk_ms_task);
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function add_new_task_empty_project()
    {
        $fk_project = Input::get('fk_project');
        $fk_industry = Input::get('fk_industry');
        $project = Project::getFirstById($fk_project);

        if (Input::has('fk_module_structure')) {
            $fk_module_structure = Input::get('fk_module_structure');
            $module_structure = Module_structure::getFirstById($fk_module_structure);
            $fk_software = $module_structure->fk_software;
        } else {
            $fk_module_structure = 0;
            $fk_software = 5;
        }
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $fk_company = $user->fk_company;

        $name_task = Input::get('name_task');
        if (Input::has('task_description')) {
            $task_description = Input::get('task_description');
        } else {
            $task_description = '';
        }

        $task = Task::getFirstByName($name_task, $task_description);
        if (count($task) == 0) {
            $task = new Task();
            $task->name_task = $name_task;
            if (Input::has('name_task_en')) {
                $task->name_task_en = Input::get('name_task_en');
            }
            if (Input::has('description_task_en')) {
                $task->description_task_en = Input::get('description_task_en');
            }
            $task->fk_company = $fk_company;
            $task->save();
        }
        $fk_task = $task->id_task;

        if (Input::has('structure')) {
            $structure = Input::get('structure');
//
            $structure_array = explode('_', $structure);

            unset($structure_array[0], $structure_array[1], $structure_array[2]);

            $length = count($structure_array) + 2;

            $last_structure = 0;
            $level = 1;

            for ($i = 3; $i <= $length; $i += 2) {
                if ($structure_array[$i + 1] != 0) {
                    $last_structure = $structure_array[$i + 1];
                } else {
                    if ($last_structure == 0) {
                        $module = Module::where('name_module', '=', $structure_array[$i])->first();
                        if (count($module) == 0) {
                            $module = new Module();
                            $module->name_module = $structure_array[$i];
                            $module->is_default = 0;
                            $module->fk_company = $fk_company;
                            $module->save();
                        }
                        $id_module = $module->id_module;
                        $structure_module = Module_structure::hasPath($fk_software, $id_module, 0, $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_module;
                            $structure_module->fk_parent = 0;
                            $structure_module->id_type = 1;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $id_module;
                            $structure_module->is_default = 0;
                            $structure_module->fk_project = $fk_project;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    } else {
                        $submodule = Submodule::where('name_submodule', '=', $structure_array[$i])->first();
                        if (count($submodule) == 0) {
                            $submodule = new Submodule();
                            $submodule->name_submodule = $structure_array[$i];
                            $submodule->is_default = 0;
                            $submodule->fk_company = $fk_company;
                            $submodule->save();
                        }
                        $id_submodule = $submodule->id_submodule;
                        $module_parrent = Module_structure::getFirstById($last_structure);
                        $structure_module = Module_structure::hasPath($fk_software, $id_submodule, $last_structure,
                            $level);
                        if (count($structure_module) == 0) {
                            $structure_module = new Module_structure();
                            $structure_module->fk_software = $fk_software;
                            $structure_module->fk_id = $id_submodule;
                            $structure_module->fk_parent = $last_structure;
                            $structure_module->id_type = 2;
                            $structure_module->level = $level;
                            $structure_module->fk_module_parent = $module_parrent->fk_module_parent;
                            $structure_module->is_default = 0;
                            $structure_module->fk_project = $fk_project;
                            $structure_module->save();
                        }
                        $last_structure = $structure_module->id_module_structure;
                    }
                }
                $level++;
            }
//            return var_dump($structure_array);
            $fk_module_structure = $last_structure;
        }
        $ms_task = Ms_task::where('fk_task', '=', $fk_task)
            ->where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_project', '=', $fk_project)
            ->first();
        if (count($ms_task) == 0) {
            $ms_task = new Ms_task();
            $ms_task->fk_task = $fk_task;
            $ms_task->fk_module_structure = $fk_module_structure;
            $ms_task->is_checked = 1;
            $ms_task->is_default = 0;
            $ms_task->fk_company = $fk_company;
            $ms_task->fk_project = $fk_project;
            $ms_task->save();
        }
        $fk_ms_task = $ms_task->id_ms_task;
        if ($project->is_empty_project !== 1) {
            $industry_ms = Industry_ms::where('fk_industry', '=', $fk_industry)
                ->where('fk_ms_task', '=', $fk_ms_task)
                ->where('fk_module_structure', '=', $fk_module_structure)
                ->first();
            if (count($industry_ms) == 0) {
                $industry_ms = new Industry_ms();
                $industry_ms->fk_industry = $fk_industry;
                $industry_ms->fk_ms_task = $fk_ms_task;
                $industry_ms->fk_module_structure = $fk_module_structure;
                $industry_ms->save();
            }
        }
        $projects_rows = Projects_rows::where('fk_project', '=', $fk_project)
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->first();
        if (count($projects_rows) == 0) {
            $projects_rows = new Projects_rows();
            $projects_rows->fk_project = $fk_project;
            $projects_rows->fk_ms_task = $fk_ms_task;
        }
        $projects_rows->fk_user = $id_user;
        $projects_rows->task_description = $task_description;
        $projects_rows->is_checked_user = 1;
        if (Input::has('set_priority')) {
            $projects_rows->task_priority = Input::get('set_priority');
        } else {
            $projects_rows->task_priority = 1;
        }
        if (Input::get('is_control') == 1) {
            $projects_rows->is_control = 1;
        } else {
            $projects_rows->is_control = 0;
        }
        if (Input::get('is_not_offer') == 1) {
            $projects_rows->is_not_offer = 1;
        } else {
            $projects_rows->is_not_offer = 0;
        }
        $projects_rows->save();

        $files = Input::get('files');
        // check if is file
        if ($files) {
            $id_project_row = $projects_rows->id_project_row;
            foreach($files as $file) {
                $extension = end(explode('.', $file));
                $directory = 'images/attach_task/' . $fk_project;
                $project_attach = new Projects_attach();
                $project_attach->fk_project_row = $id_project_row;
                $project_attach->name_attachment = $file;
                $project_attach->extension = $extension;
                $project_attach->save();
            }
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function remove_new_task()
    {
        $id_user = Auth::user()->id;
        $user = User::getFirstById($id_user);
        $fk_company = $user->fk_company;
        $fk_module_structure = Input::get('fk_module_structure');
        $fk_project = Input::get('fk_project');
        $fk_ms_task = Input::get('fk_ms_task');
        $fk_task = Input::get('fk_task');

        $projects_rows = Projects_rows::where('fk_project', '=', $fk_project)
            ->where('fk_ms_task', '=', $fk_ms_task)
            ->where('fk_project', '=', $fk_project)
            ->first();

        if (count($projects_rows) == 1) {
            $projects_rows->delete();
        }

        $ms_task = Ms_task::where('fk_task', '=', $fk_task)
            ->where('fk_module_structure', '=', $fk_module_structure)
            ->where('fk_company', '=', $fk_company)
            ->where('fk_project', '=', $fk_project)
            ->first();

        if (count($ms_task) == 1) {
            $ms_task->delete();
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }

    public function delete_module_structure()
    {
        $child_structure = Input::get('child_structure');

        $id_project = Input::get('id_project');
        if (strlen($child_structure) > 0) {
            $children_structure = explode('_', $child_structure);
        } else {
            $children_structure = array();
        }

        unset($children_structure[0]);
        $length = count($children_structure);
        for ($i = 1; $i <= $length; $i++) {
            if ($children_structure[$i] != 0) {
                $has_child = Module_structure::hasChild($children_structure[$i]);
                if ($has_child == 0) {
                    $pr_module_tasks = Ms_task::getTasksByStructure($children_structure[$i], $id_project);
                    if (count($pr_module_tasks) > 0) {
                        foreach ($pr_module_tasks as $task) {
                            $ms_task = Ms_task::getFirstById($task->id_ms_task);
                            if ($ms_task->is_default == 0) {
                                $ms_task->delete();
                            }
                        }
                    }
                    $pr_module_structure = Module_structure::getFirstById($children_structure[$i]);
                    if ($pr_module_structure->is_default == 0) {
                        $pr_module_structure->delete();
                    }
                } else {
                    $pr_module_structure = Module_structure::getFirstById($children_structure[$i]);
                    if ($pr_module_structure->is_default == 0) {
                        $pr_module_structure->delete();
                    }
                }
            }
        }


        return 'Deleted successfully';
    }

    public function edit_module_structure()
    {
        $id_module_structure = Input::get('id_module_structure');
        $editor_val = trim(Input::get('editor_val'));
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);
        if (count($pr_module_structure) == 1) {
            if ($pr_module_structure->is_default == 0) {
                $id = $pr_module_structure->fk_id;
                $type = $pr_module_structure->id_type;
                if ($type == 1) {
                    $module = Module::getFirstById($id);
                    if (count($module) == 1) {
                        $module->name_module = $editor_val;
                        $module->save();
                    }
                } elseif ($type == 2) {
                    $submodule = Submodule::getFirstById($id);
                    if (count($submodule) == 1) {
                        $submodule->name_submodule = $editor_val;
                        $submodule->save();
                    }
                }
            }
        }
        return 'Edited successfully';

    }

    public function modal_dialog_edit_task_project($id_ms_task, $id_project)
    {
        $project = Project::getFirstById($id_project);
        $fk_industry = $project->fk_industry;

        $pr_project_row = DB::table('pr_projects_rows')->whereFk_ms_task($id_ms_task)->first();
        $pr_module_task = Ms_task::getOneTaskByKey($id_ms_task, $id_project);

        $set_priority = Priority::getAllPriorities(1);
        $attachments = Projects_attach::getAttachmentByTask($id_ms_task, $id_project);
        $attachments_industry = Industry_attach::getAttachmentByTask($id_ms_task, $fk_industry);
        return View::make('front.projects.modal_dialog_edit_task_project', compact('pr_module_task', 'set_priority', 'project', 'attachments', 'attachments_industry', 'id_ms_task', 'id_project', 'pr_project_row'));
    }

    public function modal_dialog_add_task_project($id_module_structure, $id_industry, $id_project)
    {
        $set_priority = Priority::getAllPriorities();
        $set_priority[0] = '-';
        $module_structure = Module_structure::getModuleStructureOne($id_module_structure);
        $id_industry = DB::table('pr_projects')->where('id_project', '=', $id_project)->pluck('fk_industry');
        return View::make('front.projects.modal_dialog_add_task_project', compact('id_project', 'id_industry', 'id_module_structure', 'set_priority', 'module_structure'));
    }

    public function modal_dialog_add_task_new_project($id_module_structure, $id_industry, $id_project)
    {
        $set_priority = Priority::getAllPriorities();
        $project = Project::getFirstById($id_project);

        return View::make('front.projects.modal_dialog_add_task_new_project', compact('id_project', 'id_industry', 'id_module_structure', 'set_priority', 'project'));
    }

    public function modal_dialog_edit_module_project($id_module_structure)
    {
        $pr_module_structure = Module_structure::getFirstById($id_module_structure);

        if ($pr_module_structure->id_type == 1) {
            $module = Module::getFirstById($pr_module_structure->fk_id);
            return View::make('front.projects.modal_dialog_edit_module_project', compact('pr_module_structure', 'id_module_structure',
                'module'));
        } else {
            $submodule = Submodule::getFirstById($pr_module_structure->fk_id);
            return View::make('front.projects.modal_dialog_edit_submodule_project', compact('pr_module_structure', 'id_module_structure',
                'submodule'));
        }
    }

    public function set_order_projects()
    {
        $fk_project = Input::get('fk_project');
        $list_order = Input::get('list_order');
        $list = explode(',', $list_order);
        $i = 1;
        $project = Project::getFirstById($fk_project);
        if ($project->is_empty_project == 1) {
            foreach ($list as $id) {
                $task = Module_structure::getFirstById($id);
                $task->ms_order = $i;
                $task->save();
                $i++;
            }
        } else {
            foreach ($list as $id) {
                $task = Projects_ms_order::getFirstByKey($id, $fk_project);
                if (count($task) > 0) {
                    $task->ms_proj_order = $i;
                    $task->save();
                } else {
                    $task = new Projects_ms_order();
                    $task->fk_module_structure = $id;
                    $task->fk_project = $fk_project;
                    $task->ms_proj_order = $i;
                    $task->save();
                }
                $i++;
            }
        }

        return trans('validation.message_success_order');
    }

    public function set_order_front_task()
    {
        $user_id = Auth::user()->id;
        $fk_project = Input::get('fk_project');
        $list_order = Input::get('list_order');

        $list = explode(',', $list_order);
        $i = 1;
        foreach ($list as $id) {
            $fk_ms_task = str_replace('panel-task-border', '', $id);
            $project_row = Projects_rows:: getFirstByKey($fk_ms_task, $fk_project);
            if (count($project_row) > 0) {
                $project_row->task_proj_order = $i;
                $project_row->save();

            } else {
                $project_row = new Projects_rows();
                $project_row->fk_ms_task = $fk_ms_task;
                $project_row->fk_user = $user_id;
                $project_row->fk_project = $fk_project;
                $project_row->task_proj_order = $i;
                $project_row->save();
            }
            $i++;
        }
        return trans('validation.message_success_order');
    }
}
