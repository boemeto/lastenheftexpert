@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    @include('utils/maps_location')

@stop
@section('footer_scripts')
    @if(in_array('2',$fk_group))
        @include('utils/validations_login')
        <script type="text/javascript">
            $(document).on('click', '.add_new_user', function(e) {
                e.preventDefault();
                if(checkRequiredFields('#add_new_user')) {
                    $('#add_new_user').submit();
                }
            });

            $(document).on('click', '#add_new_user', function(e) {
                $("#add_new_user *[required]" ).each(function() {
                    $(this).on("blur", function(){
                        if($(this).val() || $(this).val().length > 0 || $(this).val() !== '') {
                            $(this).css({'border-color': '#DFDFDF'});
                        }
                    });
                });
            });

            $(document).on('click', '.add_new_headquarter', function(e) {
                e.preventDefault();
                if(checkRequiredFields('#addHeadquarterForm')) {
                    $('#addHeadquarterForm').submit();
                }
            });

            $(document).on('click', '#addHeadquarterForm', function(e) {
                $("#addHeadquarterForm *[required]" ).each(function() {
                    $(this).on("blur", function(){
                        if($(this).val() || $(this).val().length > 0 || $(this).val() !== '') {
                            $(this).css({'border-color': '#DFDFDF'});
                        }
                    });
                });
            });

            jQuery(document).ready(function ($) {
                if($('.session_tab').length != 0) {
                    $('.'+ $('.session_tab').val() +'_tab').find('a').trigger('click');
                    var name = $('.session_tab').val();
                    name = name.substring(0, name.length - 1);
                    name = name.charAt(0).toUpperCase() + name.slice(1);
                    setTimeout(function(){
                        $("h3.font-semibold:contains('"+name+"')").children('a').trigger('click');
                    }, 300);
                    if($('.session_go_to').length != 0) {
                        setTimeout(function(){
                            console.log($('.session_go_to').val());
                            $('html, body').animate({scrollTop: $('.employees-list').find('[href="'+$('.session_go_to').val()+'"]').parents('.center').position().top}, 'slow');
                        }, 300);
                    }
                }
                if($('.tab_value').length != 0) {
                    history.replaceState({}, null, "/company");
                    $('.'+ $('.tab_value').val() +'_tab').find('a').trigger('click');
                    if($('.session_go_to').length != 0) {
                        setTimeout(function(){
                            $('html, body').animate({scrollTop: $('.employees-list').find('[href="'+$('.session_go_to').val()+'"]').parents('.center').position().top}, 'slow');
                        }, 300);
                        }
                }

                var clickAction = 1;
                $('.prev-members').on('click', function() {
                    var index = $(this).parent().find('div:nth-child(2)').find('.col-centered:visible').last().index();
                    var index2 = $(this).parent().find('div:nth-child(2)').find('.col-centered:visible').first().index();
                    var numberFirst1 = index2;
                    var numberSecond1 = index2 - 1;
                    var numberThird1 = index2 - 2;
                    var numberFirst = index + 1;
                    var numberSecond = index - 1;
                    var numberThird = index;
                    if(clickAction) {
                        var thisClick = $(this);
                        clickAction = 0;
                        $(this).parents('.user-friend-list').find('.next-members').removeAttr('disabled');
                        $(this).parents('.user-friend-list').find('.next-members').find('i').css('cursor', 'pointer');
                        $(this).parents('.user-friend-list').find('.next-members').find('i').css('opacity', '1');
                        if(numberFirst1 >= 1) {
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberFirst+')').delay( 50 ).fadeOut('fast');
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberFirst1+')').delay( 400 ).fadeIn('fast');
                            if(numberThird >3) {
                                $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberSecond+')').delay( 250 ).fadeOut('fast');
                            }
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberSecond1+')').delay( 600 ).fadeIn('fast');
                            if(numberThird+1 >4) {
                                $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberThird+')').delay( 450 ).fadeOut('fast');
                            }
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberThird1+')').delay( 800 ).fadeIn('fast');
                        }
                        setTimeout( function() {
                            if(thisClick.parent().find('div:nth-child(2)').find('.col-centered:visible').first().index() == 0) {
                                thisClick.find('i').css('cursor', 'not-allowed');
                                thisClick.find('i').css('opacity', '.4');
                            }
                            clickAction = 1;
                        }, 960);
                    }
                });
                $('.next-members').on('click', function() {
                    var index = $(this).parent().find('div:nth-child(2)').find('.col-centered:visible').last().index();
                    var index2 = $(this).parent().find('div:nth-child(2)').find('.col-centered:visible').first().index();
                    var count = $(this).parent().find('div:nth-child(2)').find('.col-centered').length;
                    var numberFirst1 = index2 + 1;
                    var numberSecond1 = index2 + 2;
                    var numberThird1 = index2 + 3;
                    var numberFirst = index + 2;
                    var numberSecond = index + 3;
                    var numberThird = index + 4;
                    if(clickAction) {
                        var thisClick = $(this);
                        clickAction = 0;
                        $(this).parents('.user-friend-list').find('.prev-members').removeAttr('disabled');
                        $(this).parents('.user-friend-list').find('.prev-members').find('i').css('cursor', 'pointer');
                        $(this).parents('.user-friend-list').find('.prev-members').find('i').css('opacity', '1');
                        $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberFirst+')').delay( 400 ).fadeIn('fast');
                        if(numberFirst - 1 < count) {
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberFirst1+')').delay( 50 ).fadeOut('fast');
                        }
                        $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberSecond+')').delay( 600 ).fadeIn('fast');
                        if(numberFirst < count) {
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberSecond1+')').delay( 250 ).fadeOut('fast');
                        }
                        $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberThird+')').delay( 800 ).fadeIn('fast');

                        if(numberFirst+1 < count) {
                            $(this).parent().find('div:nth-child(2)').find('.col-centered:nth-child('+numberThird1+')').delay( 450 ).fadeOut('fast');
                        }
                        setTimeout( function() {
                            if(thisClick.parent().find('div:nth-child(2)').find('.col-centered:visible').last().index() + 1 == count) {
                                thisClick.attr('disabled','disabled');
                                thisClick.find('i').css('cursor', 'not-allowed');
                                thisClick.find('i').css('opacity', '.4');
                            }
                            clickAction = 1;
                        }, 960);
                    }
                });
                $('.delete_user').click(function (event) {
                      event.preventDefault();
                       var url = $(this).attr('href');
                       var name = $(this).attr("data_name");

                       $('#formConfirm')
                          .find('#frm_body').html('Delete User ' + name + '?')
                          .end().modal('show');

                       $('#formConfirm').find('#frm_submit').attr('href', url);
                });
                // show edit user form
                /* Headquarter effects start*/
                // add new
                $('#a_form_headquarter_0').click(function (event) {
                    $('html,body').animate({
                      scrollTop: 0
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_headquarter_', "");
                    $('.a_headquarter_details_hidden').not('#a_headquarter_details_hidden_' + id_nr).removeClass('active');

                    $('#a_headquarter_details_hidden_' + id_nr).toggleClass("active");
//                $('.div_headquarter_details_hidden').hide();
                    $('.edit_form_hidden_headquarter').not('#edit_form_hidden_headquarter' + id_nr).hide();
                    $('#edit_form_hidden_headquarter' + id_nr).toggle('blind');
                    $('.bg-grey').removeClass('active');
                    $('#edit_form_hidden_headquarter' + id_nr).find('.bg-grey').addClass('active');
                    $('#edit_form_hidden_headquarter' + id_nr).parents('.bg-grey').addClass('active');
                    event.preventDefault();
                });

                /* Headquarter edit form*/
                $('.a_form_headquarter').click(function (event) {
                    $('html,body').animate({
                      scrollTop: $(this).offset().top - 10
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_headquarter_', "");
                    $('.a_headquarter_details_hidden').not('#a_headquarter_details_hidden_' + id_nr).removeClass('active');

                    $('#a_headquarter_details_hidden_' + id_nr).toggleClass("active");
//                $('.div_headquarter_details_hidden').hide();
                    $('.edit_form_hidden_headquarter').not('#edit_form_hidden_headquarter' + id_nr).hide();
                    $('#edit_form_hidden_headquarter' + id_nr).toggle('blind');
                    $('.bg-grey').removeClass('active');
                    $('#edit_form_hidden_headquarter' + id_nr).find('.bg-grey').addClass('active');
                    $('#edit_form_hidden_headquarter' + id_nr).parents('.bg-grey').addClass('active');

                    event.preventDefault();
                });
                $('.a_form_headquarter_close').click(function (event) {
                    var parent_id = $(this).attr('id').split('_').pop();
                    var parent = $('#a_details_form_show_' + parent_id);
                    $('html,body').animate({
                      scrollTop: parent.offset().top - 180
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_headquarter_close_', "");
                    $('#edit_form_hidden_headquarter' + id_nr).hide('blind');
                    event.preventDefault();
                });
                /* Headquarter effects stop*/
                /* User effects start*/

                $('.a_company_show').click(function (event) {
                    $('.edit_company_hidden').toggle('blind');
                    event.preventDefault();
                });
                // add new
                $('#a_form_show_0').click(function (event) {
                    $('html,body').animate({
                      scrollTop: 0
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_show_', "");
                    $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                    $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                    //   $('.details_user_form_hidden').hide();
                    $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                    $('#edit_form_hidden_' + id_nr).toggle('blind');
                    $('.bg-grey').removeClass('active');
                    $('#edit_form_hidden_' + id_nr).find('.bg-grey').addClass('active');
                    $('#edit_form_hidden_' + id_nr).parents('.bg-grey').addClass('active');
                    event.preventDefault();
                });

                /* Show user edit form */
                $('.a_form_show').click(function (event) {
                    $('html,body').animate({
                      scrollTop: $(this).offset().top - 10
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_show_', "");
                    $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                    $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                    //   $('.details_user_form_hidden').hide();
                    $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                    $('#edit_form_hidden_' + id_nr).toggle('blind');
                    $('.bg-grey').removeClass('active');
                    $('#edit_form_hidden_' + id_nr).find('.bg-grey').addClass('active');
                    $('#edit_form_hidden_' + id_nr).parents('.bg-grey').addClass('active');
                    event.preventDefault();
                });
                /* Hide user edit form */
                $('.a_form_close').click(function (event) {
                    var parent_id = $(this).attr('id').split('_').pop();
                    var parent = $('#a_details_form_show_' + parent_id);
                    $('html,body').animate({
                      scrollTop: parent.offset().top - 180
                    }, 800);
                    var id = jQuery(this).attr('id');
                    var id_nr = id.replace('a_form_close_', "");
                    $('#edit_form_hidden_' + id_nr).hide('blind');
                    event.preventDefault();
                });

                /* User effects stop*/


                // image input functions
                animated_text_area();
                file_input_trigger();
            });
            function animated_text_area() {
                'use strict';
                $('.animatedTextArea').autosize({append: "\n"});
            }
            /*** file input Call ****/
            function file_input_trigger() {
                'use strict';
                @foreach($all_users as $user)

                    $("#file-user<?php print $user->id ?>").fileinput({
                    showCaption: true,
                    removeLabel: "",
                    removeIcon: '<i class="fa fa-times"></i> ',
                    browseLabel: " ",
                    initialCaption: 'upload image',
                    browseClass: "btn ls-light-blue-btn btn-upload",
                    browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                    dropZoneEnabled: false,
                    previewFileIcon: ' ',
                    fileType: "any",
                    'showUpload': false
                });
                @endforeach
                $("#file-3").fileinput({
                    showCaption: true,
                    removeLabel: "",
                    removeIcon: '<i class="fa fa-times"></i> ',
                    browseLabel: " ",
                    initialCaption: 'upload image',
                    browseClass: "btn ls-light-blue-btn btn-upload",
                    browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                    dropZoneEnabled: false,
                    previewFileIcon: ' ',
                    fileType: "any",
                    'showUpload': false
                });
                $("#file-user").fileinput({
                    showCaption: true,
                    removeLabel: "",
                    removeIcon: '<i class="fa fa-times"></i> ',
                    browseLabel: " ",
                    initialCaption: 'upload image',
                    browseClass: "btn ls-light-blue-btn btn-upload",
                    browseIcon: '<i class="fa  fa-cloud-upload"></i> ',
                    dropZoneEnabled: false,
                    previewFileIcon: ' ',
                    fileType: "any",
                    'showUpload': false
                });

            }
        </script>
        @endif
        @stop

        @section('content')

                <!-- visible content start -->
        <div class="box box-profile-user">
            <!-- /.box-body -->
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 user-pic-body">
                        <div class="panel widget light-widget panel-bd-top">
                            <div class="panel-body">
                                <div class="user-profile-details">
                                    @if (Session::has('tab'))
                                        <input type="hidden" class="session_tab" value="{{Session::get('tab')}}">
                                    @endif
                                    @if (Session::has('go-to'))
                                        <input type="hidden" class="session_go_to" value="{{Session::get('go-to')}}">
                                    @endif
                                    <ul class="social-links-user">
                                        <li>
                                            @if($company->social_xing)
                                                <a href="{{$company->social_xing}}" target="_blank" class="btn ls-btn ls-xing-btn btn-round">
                                                    <i class="fa fa-xing"></i></a>
                                            @else
                                                <a href="#" class="btn ls-blue-btn btn-round disabled">
                                                    <i class="fa fa-xing"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($company->social_linkedin)
                                                <a href="{{$company->social_linkedin}}" target="_blank" class="btn ls-btn ls-linkedin-btn btn-round">
                                                    <i class="fa fa-linkedin"></i></a>
                                            @else
                                                <a href="#" class="btn ls-blue-btn btn-round  disabled">
                                                    <i class="fa fa-linkedin"></i></a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($company->social_twitter)
                                                <a href="{{$company->social_twitter}}" target="_blank" class="btn ls-btn ls-twitter-btn  btn-round">
                                                    <i class="fa fa-twitter"></i></a>
                                            @else
                                                <a href="#" class="btn ls-blue-btn btn-round disabled">
                                                    <i class="fa fa-twitter"></i></a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="text-center vd_info-parent square_main_image">
                                    <?php $image = Company::getLogoCompany($company->id_company);  ?>
                                    <img src="{{ URL::asset($image)}}" alt="image"></div>

                                <h3 class="font-semibold mgbt-xs-5 center"> {{$main_headquarter->name_headquarter}} </h3>
                                <div class="center">{{$main_headquarter->street}} {{$user->street_nr}}</div>
                                <div class="mgtp-20">
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                        <tr>
                                            <td>{{trans('messages.label_member_since')}}:</td>
                                            <td> {{date('d.m.Y' ,strtotime($main_headquarter->created_at))}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="center">
                                        @if($company->is_blocked)
                                            <a href="{{URL::to('approve-company/'.$company->id_company)}}" title=" Approve company" class="btn btn-round ls-light-blue-btn">
                                                <i class="fa fa-unlock"></i>
                                            </a>
                                        @else
                                            <a href="{{URL::to('block-company/'.$company->id_company)}}" title=" Block company" class="btn btn-round ls-red-btn">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 tabs">
                        <ul class="nav  nav-tabs  nav-left icon-tab profile-navigation-tabs ">
                            <li class="active"><a href="#details" data-toggle="tab">
                                    {{trans('messages.label_details')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>
                            <li class="headquarters_tab"><a href="#headquarters" class="user-google-location" data-toggle="tab">
                                    {{trans('messages.nav_headquarters')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>
                            <li class="employees_tab"><a href="#coworkers" class="user-google-location" data-toggle="tab">
                                    {{trans('messages.label_employees')}}
                                    <span class="menu-active"><i class="fa fa-caret-up"></i></span>
                                </a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content   profile-navigation-content">
                            <!-- details Tab Start -->
                            @include('admin/company/details_tab')
                                    <!-- details Tab End -->
                            <!-- headquarters Tab Start -->
                            @include('admin/company/headquarters_tab')
                                    <!-- headquarters Tab End -->
                            <!-- Coworkers Tab Start -->
                            @include('admin/company/users_tab')
                                    <!-- Coworkers Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
