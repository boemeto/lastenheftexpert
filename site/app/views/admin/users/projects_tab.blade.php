<div class="tab-pane fade table_align_search" id="projects">
    <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
        <i class="fa fa-sitemap mgr-10 profile-icon"></i>
        {{trans('messages.nav_projects')}}
    </h3>
    <table class="table table-responsive data_table">
        <thead>
        <tr>
            <th>Nr.</th>
            <th>{{trans('messages.label_project_name')}}</th>
            <th>{{trans('messages.label_start_date')}}</th>
            <th>{{trans('messages.label_end_date')}}</th>
            <th>{{trans('messages.label_status')}}</th>
            <th>{{trans('messages.label_price')}}</th>
            <th>{{trans('messages.label_action')}}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{$project->name_project}}</td>
                    <td>{{date('d.m.Y', strtotime($project->created_at))}}</td>
                    <td>{{date('d.m.Y', strtotime($project->due_date))}}</td>
                    <td>
                        <?php $status = is_project_expired($project->id_project); ?>
                        {{($status==1)?'<span class="user_project label label-success ">'.trans('messages.l_active').'</span>':' '}}
                        {{($status==2)?'<span class="user_project label label-warning">'.trans('messages.l_pending').'</span>':' '}}
                        {{($status==3)?'<span class="user_project label label-danger">'.trans('messages.l_expired').'</span>':' '}}
                    </td>
                    <td>
                        <?php
                            $totalCost = getProjectTotalCost($project->id_project);
                            $totalCost = formatToGermanyPrice($totalCost);
                        ?>
                        <b class="pull-right">
                          {{$totalCost}} <i class="fa fa-euro"></i>
                        </b>
                    </td>
                    <td class="menu-action rounded-btn text-center">
                        <a class="btn btn-round menu-icon vd_bg-yellow" href="{{url('projects/'.$project->id_project)}}" data-placement="top" data-toggle="tooltip" data-original-title="edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        {{-- <a class="btn btn-round  menu-icon vd_bg-red" data-placement="top" data-toggle="tooltip" data-original-title="delete">
                            <i class="fa fa-trash"></i>
                        </a> --}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
