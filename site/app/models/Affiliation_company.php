<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 14.05.2015
 * Time: 16:00
 * use to store the data of the affiliated companies
 */
class Affiliation_company extends Eloquent
{


    public static $rules = [
        'fk_company' => 'required',
        'fk_affiliation' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_affiliation_company';
    protected $table = 'affiliation_company';

    // validation rules
    protected $guarded = ['id_affiliation_company'];
    protected $fillable = ['fk_company', 'fk_affiliation', 'is_pending'];

    // validation function

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    // get the data by table id
    public static function getFirstById($id)
    {
        $list = Affiliation_company::whereId_affiliation_company($id)->first();
        return $list;
    }

    // get the data of the affiliated companies
    public static function getAffiliatedCompanies($id_company = '')
    {
        // if id company is set, get the data of the affiliated companies if the current company
        if ($id_company != '') {
            $companies_affiliation = DB::table('affiliation_company')->whereFk_company($id_company);
            $affiliated_companies = DB::table('affiliation_company')->whereFk_affiliation($id_company)
                ->union($companies_affiliation)->get();
        } else {
            // get all the companies that are affiliated (for super admin)
            $companies_affiliation = DB::table('affiliation_company');
            $affiliated_companies = DB::table('affiliation_company')
                ->union($companies_affiliation)->get();
        }

        return $affiliated_companies;
    }

// returns an array of names and ids of the affiliated companies
    public static function getAffiliatedIds($id_company = '')
    {

        if ($id_company != '') {
            // if id company is set, get the data of the affiliated companies if the current company
            $companies_affiliation = DB::table('affiliation_company')->whereFk_company($id_company);
            $lists = DB::table('affiliation_company')->whereFk_affiliation($id_company)
                ->union($companies_affiliation)->get();
        } else {
            // get all the companies that are affiliated (for super admin)
            $companies_affiliation = DB::table('affiliation_company');
            $lists = DB::table('affiliation_company')
                ->union($companies_affiliation)->get();
        }
        $array = array();


        foreach ($lists as $list) {
            $array[$list->fk_company] = $list->fk_company;
            $array[$list->fk_affiliation] = $list->fk_affiliation;
        }

        $companies = Company::whereIn('id_company', $array)->get();
// foreach company, get the names and the ids
        foreach ($companies as $list) {
            $array[$list->id_company] = $list->name_company;
        }
        return $array;
    }

}
