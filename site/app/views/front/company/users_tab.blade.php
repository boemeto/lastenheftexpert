<div class="tab-pane fade " id="coworkers">
    <div class="row">
        <div class="col-md-12">
            <h3 class="mgbt-xs-15 mgtp-10 font-semibold add_employee">
                <i class="fa fa-users mgr-10 profile-icon"></i>
                {{trans('messages.label_employees')}}
                @if(in_array('2',$fk_group))
                    <a href="javascript:void(0)"
                       id="a_form_show_0"
                       class="btn static_add_button ls-light-blue-btn btn-round btn-xxl">
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="title_static_add_button" id="div_a_form_show_0">
                        {{trans('messages.add_employees')}}
                    </div>
                @endif
            </h3>
        </div>
    </div>
    <div class="row">
        <div class=" edit_form_hidden col-lg-3 col-md-4 col-sm-6 col-xs-12 pd-10 center coworker-item" id="edit_form_hidden_0">
            <div class="bg-grey  pd-15">
                {{ Form::open(['action'=>'MembersController@store','id'=>'add_new_user','enctype'=>"multipart/form-data", 'novalidate']) }}
                <input type="hidden" name="company_edit_user" value="1">
                <div class="form-group mgtp-10">
                    <input id="file-user" type="file" name="logo_user" multiple="flase">
                </div>
                <div class="form-group ">
                    <select id="fk_personal_title" class="form-control" name="fk_personal_title" required="required">
                        <option value="" disabled selected>{{$personal_titles_new[0]}}</option>
                        @foreach($personal_titles_new as $key => $value)
                          @if ($key != 0)
                            <option value="{{$key}}">{{$value}}</option>
                          @endif
                        @endforeach
                    </select>
                    {{-- {{ Form::select("fk_personal_title",$personal_titles_new,'',[ 'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }} --}}
                    {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}
                </div>
                <div class="form-group">
                    <?php $headquarters_user = Company_headquarters::getHeadquartersSelect($company->id_company, 1)?>
                    <select id="fk_headquarter" class="form-control" name="fk_headquarter" required="required">
                        <option value="" disabled selected>{{$headquarters_user[0]}}</option>
                        @foreach($headquarters_user as $key => $value)
                          @if ($key != 0)
                            <option value="{{$key}}">{{$value}}</option>
                          @endif
                        @endforeach
                    </select>
                    {{-- {{ Form::select("fk_headquarter",$headquarters_user,'',[  'id'=>'fk_headquarter', "class"=>"form-control", "required"]) }} --}}
                    {{ $errors->first("fk_headquarter","<div class='text-red'>:message</div>") }}
                </div>
                <div class="form-group">
                    <select id="fk_title" class="form-control" name="fk_title">
                        <option value="" disabled selected>{{$titles[0]}}</option>
                        @foreach($titles as $key => $value)
                          @if ($key != 0)
                            <option value="{{$key}}">{{$value}}</option>
                          @endif
                        @endforeach
                    </select>
                    {{-- {{ Form::select("fk_title", $titles, null, ["class"=>"form-control", 'id'=>'fk_title', "required"]) }} --}}
                    {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::text("first_name",'',[ "placeholder"=>trans('messages.label_first_name'), "class"=>"form-control",'id'=>'first_name', "required"]) }}
                    {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::text("last_name",'',[ "placeholder"=>trans('messages.label_last_name'), "class"=>"form-control",'id'=>'last_name', "required"]) }}
                    {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group  has-feedback">
                    {{ Form::email("email",'',["placeholder"=>trans('messages.label_email'), "class"=>"form-control", 'id'=>'email',"required",
                        'data_url_verify'=> action('SessionsController@checkUserEmail'), 'onblur'=>'validateEmail($(this));']) }}
                    {{ $errors->first("email",'<div class="text-red" style="height: 40px;">:message</div>') }}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("job_user",'' ,["placeholder"=>trans('messages.label_job'), "class"=>"form-control",
                    'id'=>'job_user']) }}
                    {{ $errors->first("job_user",'<div class="text-red">:message</div>') }}
                    <i class="glyphicon glyphicon-briefcase form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("mobile_user",'' ,["placeholder"=>trans('messages.label_mobile'), "class"=>"form-control",    'id'=>'mobile_user']) }}
                    {{ $errors->first("mobile_user",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-mobile     form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("telephone_user",'' ,["placeholder"=>trans('messages.label_phone'), "class"=>"form-control", 'id'=>'telephone_user']) }}
                    {{ $errors->first("telephone_user",'<div class="text-red">:message</div>') }}
                    <span class="glyphicon glyphicon-earphone    form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_xing",'' ,[       "placeholder"=>"Xing Account", "class"=>"form-control",        'id'=>'social_xing']) }}
                    {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-xing            form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_linkedin",'',[ "placeholder"=>"LinkedIn Account", "class"=>"form-control", 'id'=>'social_twitter']) }}
                    {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-linkedin form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    {{ Form::text("social_twitter",'',[     "placeholder"=>"Twitter Account", "class"=>"form-control",     'id'=>'social_twitter']) }}
                    {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                    <i class="fa fa-twitter              form-control-feedback"></i>
                </div>
                {{Form::hidden('redirect_link','company')}}
                <div class="row">
                    <div class="col-xs-6" style="padding: 0;">
                        {{Form::submit( trans('messages.act_save'),['class'=>"btn btn-block add_new_user ls-light-blue-btn"])}}
                    </div>
                    <div class="col-xs-6" style="padding: 0 0 0 4px;">
                        <button type="button" class="btn btn-danger btn-block a_form_close" id="a_form_close_0">{{trans('messages.act_close')}}</button>

                    </div>
                </div>
                {{ Form::close(); }}
                        <!-- /.col -->
            </div>
        </div>
        @foreach($all_users as $user)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pd-10  center">
                <div class="bg-grey pd-15">
                    <div class="employees-list">
                        <ul class="social-links-user">
                            <li>
                                @if($user->social_xing)
                                    <a href="{{$user->social_xing}}" target="_blank" class="btn ls-xing-btn btn-round">
                                        <i class="fa fa-xing"></i></a>
                                @else
                                    <a href="#" class="btn ls-red-btn btn-round disabled">
                                        <i class="fa fa-xing"></i></a>
                                @endif
                            </li>
                            <li>
                                @if($user->social_linkedin)
                                    <a href="{{$user->social_linkedin}}" target="_blank" class="btn  ls-linkedin-btn btn-round">
                                        <i class="fa fa-linkedin"></i></a>
                                @else
                                    <a href="#" class="btn ls-red-btn btn-round disabled">
                                        <i class="fa fa-linkedin"></i></a>
                                @endif
                            </li>
                            <li>
                                @if($user->social_twitter)
                                    <a href="{{$user->social_twitter}}" target="_blank" class="btn ls-twitter-btn btn-round">
                                        <i class="fa fa-twitter"></i></a>
                                @else
                                    <a href="#" class="btn ls-red-btn btn-round disabled">
                                        <i class="fa fa-twitter"></i></a>
                                @endif
                            </li>
                        </ul>
                        <a href="{{URL::to('users/'.$user->id)}}" id="a_details_form_show_{{$user->id}}">
                            <div class="user-logo">
                                <?php $image_user = User::getLogoUser($user->id); ?>
                                <img class="img-circle" alt="" src="{{ URL::asset($image_user)}}">
                            </div>
                        </a>
                        @if($user->blocked == 1)
                            <span class="is-busy ls-friend-status"></span>
                        @else
                            <span class="is-online ls-employee-status"></span>
                        @endif
                        <div class="employees_details">
                            <div class="employees_header mgbt-md-5">
                                {{$user->first_name}}  {{$user->last_name}}
                            </div>
                            <p>
                                <div class="headquarter">{{$user->name_headquarter}}</div>
                                <address>
                                    {{$user->street}} {{$user->street_nr}} <br>
                                    {{$user->postal_code}}, {{$user->city}} <br>
                                    {{$user->country}} <br>
                                </address>
                            </p>
                            <div class="div_headquarter_details   mgtp-10">
                                <div class="fa_width_user">
                                    <i class="glyphicon glyphicon-briefcase"></i>
                                </div>
                                {{ $user->job_user? $user->job_user:'<i>'.trans('messages.unavailable').'</i>'}}
                                <br>

                                <div class="fa_width_user">
                                    <i class=" fa fa-mobile"></i>
                                </div>
                                {{ $user->mobile_user? $user->mobile_user:'<i>'.trans('messages.unavailable').'</i>'}}
                                <br>

                                <div class="fa_width_user">
                                    <i class="fa fa-phone"></i>
                                </div>
                                {{ $user->telephone_user? $user->telephone_user:'<i>'.trans('messages.unavailable').'</i>'}}
                                <br>

                                <div class="fa_width_user">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                            </div>
                        </div>
                        @if(in_array('2',$fk_group))
                            <div class="edit-details-headquarters mgtp-15">
                                <ul class="menu_user">
                                    @if($user->id !=  Auth::user()->id && $user->blocked == 1)
                                        <li>
                                            <a href="{{URL::to('members/approve',[$user->id])}}" title="{{trans('messages.act_approve_user')}}" class="btn btn-round ls-light-blue-btn">
                                                <i class="fa fa-check"></i>
                                            </a></li>
                                    @else
                                        <li>
                                            <a href="{{URL::to('members/approve',[$user->id])}}" class="btn   btn-round  ls-light-blue-btn disabled">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </li>
                                    @endif
                                    <li><a href="javascript:void(0)" id="a_form_show_{{$user->id}}" title="{{trans('messages.act_edit')}}" class="btn   btn-round  ls-orange-btn a_form_show">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </li>
                                    {{--!! TO DOOOO--}}
                                    @if($user->id !=  Auth::user()->id  )
                                        <li>
                                            <a href="{{URL::to('members/block', [$user->id])}}" title="{{trans('messages.delete_user')}}" data_name="{{$user->first_name}}  {{$user->last_name}}" class="btn  btn-round  ls-red-btn delete_user">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{URL::to('members/block', [$user->id])}}" class="btn ls-red-btn  btn-round  delete_user disabled">
                                                <i class="fa fa-trash"></i>
                                            </a></li>
                                    @endif
                                    @if($user->id !=  Auth::user()->id)
                                        @if($user->fk_group == 3)
                                            <li>
                                                <a href="{{URL::to('members/change_group',[$user->id])}}" title="{{trans('messages.act_set_admin_user')}}" class="btn  btn-round  ls-light-green-btn">
                                                    <i class="fa fa-black-tie"></i>
                                                </a></li>

                                        @else
                                            <li>
                                                <a href="{{URL::to('members/change_group', [$user->id])}}" title="{{trans('messages.act_set_simple_user')}}" class="btn  btn-round  ls-light-green-btn">
                                                    <i class="fa fa-user"></i></a>
                                            </li>
                                        @endif
                                    @else
                                        @if($user->fk_group == 3)
                                            <li>
                                                <a href="javascript:void(0)" title="{{trans('messages.act_set_admin_user')}}" class="btn  btn-round  ls-light-green-btn disabled">
                                                    <i class="fa fa-black-tie"></i>
                                                </a></li>

                                        @else
                                            <li>
                                                <a href="javascript:void(0)" title="Set as simple user" class="btn  btn-round  ls-light-green-btn disabled">
                                                    <i class="fa fa-user"></i></a>
                                            </li>
                                        @endif
                                    @endif

                                </ul>
                                <div class="row edit_form_hidden" id="edit_form_hidden_{{$user->id;}}">
                                    <?php $headquarters_user = Company_headquarters::getHeadquartersSelect($user->fk_company); ?>
                                    {{ Form::model($user, ['method'=>'PATCH', 'route'=>['members.update',$user->id],'enctype'=>"multipart/form-data"])}}
                                    <input type="hidden" name="company_edit_user" value="1">
                                    <div class="form-group mgtp-10">
                                        <input id="file-user{{ $user->id }}" type="file" name="logo_user" multiple="false">
                                    </div>
                                    <div class="form-group ">
                                        {{ Form::select("fk_personal_title",$personal_titles,$user->fk_personal_title,[ 'id'=>'fk_personal_title', "class"=>"form-control", "required"]) }}
                                        {{ $errors->first("fk_personal_title","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::select("fk_headquarter",$headquarters_user,$user->fk_headquarter,[  'id'=>'fk_headquarter', "class"=>"form-control", "required"]) }}
                                        {{ $errors->first("fk_headquarter","<div class='text-red'>:message</div>") }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::select("fk_title",$titles,$user->fk_title,[   "class"=>"form-control",'id'=>'fk_title', "required"]) }}
                                        {{ $errors->first("fk_title",'<div class="text-red">:message</div>') }}

                                    </div>
                                    <div class="form-group  has-feedback">
                                        {{ Form::text("first_name",$user->first_name,[ "placeholder"=>trans('messages.label_first_name'), "class"=>"form-control",'id'=>'first_name', "required"]) }}
                                        {{ $errors->first("first_name",'<div class="text-red">:message</div>') }}
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group  has-feedback">
                                        {{ Form::text("last_name",$user->last_name,[ "placeholder"=>trans('messages.label_last_name'), "class"=>"form-control",'id'=>'last_name', "required"]) }}
                                        {{ $errors->first("last_name",'<div class="text-red">:message</div>') }}
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group  has-feedback">
                                        {{ Form::email("email",$user->email,["placeholder"=>trans('messages.label_email'), "class"=>"form-control", 'id'=>'email',"readonly"]) }}
                                        {{ $errors->first("email",'<div class="text-red">:message</div>') }}
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>

                                    <div class="form-group has-feedback">
                                        {{ Form::text("job_user",$user->job_user ,[ "placeholder"=>trans('messages.label_job'), "class"=>"form-control",
                                        'id'=>'job_user']) }}
                                        {{ $errors->first("job_user",'<div class="text-red">:message</div>') }}
                                        <i class="glyphicon glyphicon-briefcase form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("mobile_user",$user->mobile_user ,[
                                        "placeholder"=>trans('messages.label_mobile'), "class"=>"form-control",     'id'=>'mobile_user']) }}
                                        {{ $errors->first("mobile_user",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-mobile
                                                            form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("telephone_user",$user->telephone_user ,[
                                        "placeholder"=>trans('messages.label_phone'), "class"=>"form-control",
                                        'id'=>'telephone_user']) }}
                                        {{ $errors->first("telephone_user",'<div class="text-red">:message</div>') }}
                                        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_xing",$user->social_xing ,[
                                        "placeholder"=>"Xing Account", "class"=>"form-control",
                                        'id'=>'social_xing']) }}
                                        {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-xing form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_linkedin",$user->social_linkedin ,[
                                        "placeholder"=>"LinkedIn Account", "class"=>"form-control",
                                        'id'=>'social_twitter']) }}
                                        {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-linkedin form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        {{ Form::text("social_twitter",$user->social_twitter ,[
                                        "placeholder"=>"Twitter Account", "class"=>"form-control",
                                        'id'=>'social_twitter']) }}
                                        {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                                        <i class="fa fa-twitter form-control-feedback"></i>
                                    </div>

                                    {{Form::hidden('redirect_link','company')}}
                                        <div class="row">
                                            <div class="col-xs-6" style="padding: 0;">
                                                {{Form::submit( trans('messages.act_save') ,['class'=>"btn btn-block  ls-light-blue-btn"])}}
                                            </div>
                                            <div class="col-xs-6" style="padding: 0 0 0 4px;">
                                                <button type="button" class="btn btn-danger btn-block a_form_close" id="a_form_close_{{$user->id}}">{{trans('messages.act_close')}}</button>
                                            </div>
                                        </div>
                                    {{ Form::close(); }}
                                </div>
                                <!-- User Details End -->
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

 @include('admin.alert_box.delete_confirm')
