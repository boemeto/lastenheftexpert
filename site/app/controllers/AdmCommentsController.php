<?php

class AdmCommentsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $id_task = Input::get('id_task');
        $id_user = Auth::user()->id;
        $comment = Input::get('comment');
        $comments = new Adm_comment();
        $comments->fk_user = $id_user;
        $comments->comment = $comment;
        $comments->fk_task = $id_task;
        $comments->save();
        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $comment = Adm_comment::getFirstById($id);
        $comment->delete();
        return Redirect::back();
    }


}
