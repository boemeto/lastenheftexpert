<?php


function formatNumbers($number)
{
    $nr = number_format($number, 2, ',', '.') . ' ';
    return $nr;
}

function getProjectId($id)
{
    $text = 'P' . str_pad($id, 4, 0, STR_PAD_LEFT);
    return $text;
}

function getClientId($id)
{
    $text = 'C' . str_pad($id, 4, 0, STR_PAD_LEFT);
    return $text;
}

function getDictionaryName($value, $id_dictionary)
{
    $dictionary = Dictionary::getOptions($id_dictionary);
    if (array_key_exists($value, $dictionary)) {
        return $dictionary[$value];
    } else {
        return $value;
    }
}

function is_project_expired($id_project)
{
//A. - la creare proiect
//   -  statusul este automat "1  - Active" -timp de 14 zile daca nu a platit sau a platit
//   -  daca trec 14 zile, e "2 -  Peding" - > pana la 30 de zile cand trece in "3-  Expired"
//   -  daca a platit, e status "1 - Active " ->  pana la 30 de zile cand trece in 3-  Expired
//
//B - la prelungire proiect
//      a - daca mai are valabilitate
//          - daca o prelungedte in cele 14 zile ca e pending dar nu a paltit prima factura
//          - numai daca a platit factura atunci apare prelungirea
//
//      - daca o prelungeste in cele 30 zile -> si nu a platit prelungirea -> intra in pending
//      - sa nu apara prelungirile pana nu a platit toate facturile care le are pe proiect
//
//        - daca ai facut storno - > automat se schimba statusul expired


    $currentDate = date('Y-m-d');
    $project = Project::getFirstById($id_project);

    $due_date = $project->due_date;
    if ($currentDate > $due_date) {
        $project->status = 3;
        $project->save();
    }
    return $project->status;
}

function getProjectTotalCost($project_id) {
    $totalCost = array();
    $projectInvoices = DB::table('invoices')->whereFk_project($project_id)->get();
    foreach($projectInvoices as $projectInvoice) {
        $totalCost[] = $projectInvoice->sum_paid;
    }
    return array_sum($totalCost);
}

function daysBetweenDates($startDate, $endDate)
{
    $start = strtotime($startDate);
    $end = strtotime($endDate);

    $days_between = ceil(abs($end - $start) / 86400);
    return $days_between;
}

function formatToGermanyPrice($price) {
    $price = str_replace(',00', '', number_format($price, 2, ',', '.'));
    return $price;
}

function formatPackagePrice($price) {
    $price = number_format($price, 0, '.', '.');
    return $price;
}

function daysPercentPassed($startDate, $endDate)
{
    $today = date('Y-m-d');
    $days_passed = daysBetweenDates($startDate, $today);
    $days_total = daysBetweenDates($startDate, $endDate);

    $percent = round($days_passed / $days_total * 100);
    if ($percent > 100) {
        $percent = 100;
    }
    return $percent;

}

function calculate_id()
{
    $rand = randomString(5);
    $date = strtotime("now");
    $id = $rand . $date;
    return $id;
}

function randomString($len = 5)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $len; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function send_mail($user, $email, $subject, $message, $template = 'send_mail')
{
    $success_count = \Mail::send('emails.' . $template, ['text' => $message], function ($message) use ($subject, $email, $user) {
        $message->from('taskmanager@mevision.de', 'Task Manager');
        $message->to($email, $user)->subject($subject);
    });
    if ($success_count == 1) {
        return 1;
    } else {
        return 0;
    }
}

function getTaskColumnFromExcel($excellArray) {
    $task_col = '';
    foreach($excellArray as $name => $cell) {
        if(stristr($name, 'name_at_level_')) {
            $task_col = $name;
        }
    }
    return $task_col;
}

function send_mail_attach($user, $email, $subject, $message, $attachment, $template = 'send_mail')
{

    $success_count = Mail::send('emails.' . $template, ['text' => $message],
        function ($message) use ($subject, $attachment, $user, $email) {
            $message->from('taskmanager@mevision.de', 'Task Manager');
            $message->to($email, $user)->subject($subject);
            $message->attach($attachment);

        });
    if ($success_count == 1) {
        return 1;
    } else {
        return 0;
    }
}



// print software tree in admin, function used in view software / show
function printTreeAdmin($module_structure, $fk_software, $lvl = 1, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0, $has_tasks = 0)
{
    $lvl++;
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
            ?>
            <li id="<?php print $module->id_module_structure ?>">
                <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                    <span class="glyphicon glyphicon-minus-sign"></span>
                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                    <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                    <?php print ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : '' ?>
                </span>
                <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />

                <ul class="sortable_modules_<?php print $lvl ?>">
                    <?php  printTreeAdmin($module_structure, $fk_software, $lvl, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks); ?>
                </ul>
            </li>
            <?php
        } else {
            ?>
            <li class="li_not_selectable" id="<?php print $module->id_module_structure ?>">
                <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                    <span class="glyphicon glyphicon-minus-sign close_tasks"></span>
                    <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                    <?php echo ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                </span>
                <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />

                <div class="row border_row margin_left_minus<?php print $lvl ?>">
                    <?php
                    if ($fk_pair == 0) {
                        $task_pairs = Ms_task::getTasksPairsByStructure($module->id_module_structure);
                    } else {
                        $task_pairs = Ms_task::getTaskPairsByPair($module->id_module_structure, $fk_pair);
                    }
                    ?>
                <div class="col-xs-2  "></div>
                <div class="col-xs-10 inside-with-border">
                    <div class="row">
                        <div class=" col-md-11 col-xs-10">
                            <ul class="no-lines sortable_task">
                                    <?php
                  foreach ($task_pairs as $fk_pair_order => $id_ms_task) {
                      $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);

                      ?>
                      <li id="<?php print $id_ms_task ?>">
                        <ul class="sortable_task inner_nested_child">
                        <?php
                        foreach ($pr_module_tasks as $pr_module_task) {
                            $attachments = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                            ?>
                            <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                                <span class="fa fa-angle-right "></span>
                                <input type="hidden" name="description_counter" value="<?php echo strlen($pr_module_task->description_task); ?>" />
                                <span class="tasks_title">
                                   <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>)">
                                       <?php print   $pr_module_task->name_task ?>
                                       <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>
                                       <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                                   </a>
                                </span> &nbsp;&nbsp;
                                  <div class="admin_attachments">
                                      <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>"><?php print count($attachments) ?></span>
                                      <i class="fa fa-paperclip"></i>
                                  </div>
                                  <?php
                                  if ($pr_module_task->name_pair) {
                                      print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                                  }
                                  ?>
                                  <?php echo ($pr_module_task->is_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                              </li>
                            <?php } ?>
                          </ul>
                      </li>
                    <?php
                  }
            if ($fk_pair == 0) {
                $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, null, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);

                foreach ($pr_module_tasks as $pr_module_task) {
                    $attachments = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                    ?>
                    <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                        <span class="fa fa-angle-double-right "></span>
                        <input type="hidden" name="description_counter" value="<?php echo strlen($pr_module_task->description_task); ?>" />
                        <span class="tasks_title">
                            <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>)">
                                <?php print   $pr_module_task->name_task ?>
                                <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>
                                <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                            </a>
                        </span>&nbsp;&nbsp;
                        <div class="admin_attachments">
                            <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>"><?php print count($attachments) ?></span>
                            <i class="fa fa-paperclip"></i>
                        </div>
                        <?php
                        if ($pr_module_task->name_pair) {
                            print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                        }
                        ?>
                        <?php echo ($pr_module_task->is_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                    </li>
                <?php }
            } ?>
                  </ul>
                </div>
                <div class="col-md-1 col-xs-2">
                    <a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onclick="modal_dialog_add_task(<?php print $module->id_module_structure ?>)"> <i class="fa fa-plus"></i> </a>
                </div>
            </div>
        </div>
    </div>
</li>
<?php
        }

    }
}

// print industry tree in admin, function used in view industry / show
function printTreeAdminIndustry($module_structure, $fk_industry, $fk_software, $lvl = 1, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0, $has_tasks = 0)
{
    $lvl++;
    // echo "<pre>";dd($module_structure);
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            if (Industry_ms::isIndustryChildChecked($fk_industry, $module->id_module_structure) == true) {

                $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $fk_industry, $lvl, $module->id_module_structure, 0);
                ?>
                <li id="<?php print $module->id_module_structure ?>">
                    <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                        <span class="glyphicon glyphicon-minus-sign"></span>
                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                        <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                        <?php print ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : '' ?>
                    </span>
                    <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                    <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />
                    <ul class="sortable_modules_<?php print $lvl ?>">
                        <?php printTreeAdminIndustry($module_structure, $fk_industry, $fk_software, $lvl, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks); ?>
                    </ul>
                </li>
                <?php
            }
        } else {
            if (Industry_ms::isIndustryChildChecked($fk_industry, $module->id_module_structure) == true) {
              ?>
              <li class="li_not_selectable" id="<?php print $module->id_module_structure ?>">
                    <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                      <span class="glyphicon glyphicon-minus-sign close_tasks"></span>
                      <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                      <?php echo ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                    </span>
                    <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                    <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />
                    <div class="row border_row margin_left_minus<?php print $lvl ?>">
                        <?php
                          if ($fk_pair == 0) {
                              $task_pairs = Ms_task::getTasksPairsByStructureIndustry($module->id_module_structure, $fk_industry);
                          } else {
                              $task_pairs = Ms_task::getTaskPairsByPairIndustry($module->id_module_structure, $fk_industry, $fk_pair);
                          }
                        ?>
                        <div class="col-xs-2 "></div>
                        <div class="col-xs-10 inside-with-border">
                            <div class="row">
                                <div class="col-md-11 col-xs-10">
                                    <ul class="no-lines sortable_task">
                                        <?php
                                      if ($fk_pair > 0) {
                                        foreach ($task_pairs as $fk_pair_order => $id_ms_task) {
                                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $fk_industry, 0, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                          //   $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $fk_project, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);

                                            ?>
                                            <li id="<?php print $id_ms_task ?>">
                                                <ul class="sortable_task inner_nested_child">
                                                    <?php
                                                    foreach ($pr_module_tasks as $pr_module_task) {
                                                          // $attachments = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $fk_industry);
                                                      //    $attachments = Industry_attach::where('fk_industry_ms', '=', $pr_module_task->id_ms_task)->get();
                                                         $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $fk_industry);
                                                         $industry_attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                                        ?>
                                                        <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                                                            <input type="hidden" name="description_counter" value="<?php echo strlen($pr_module_task->description_task); ?>" />
                                                            <span class="fa fa-angle-right "></span>
                                                            <span class="tasks_title">
                                                                <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>)">
                                                                    <?php print  $pr_module_task->name_task ?>
                                                                    <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>

                                                                    <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>

                                                                </a> &nbsp;&nbsp;
                                                            </span>
                                                            <div class="admin_attachments">
                                                                <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>"><?php print count($industry_attachments) ?></span>
                                                                    <i class="fa fa-paperclip"></i>
                                                            </div>
                                                            <?php
                                                            if ($pr_module_task->name_pair) {
                                                                print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                                                            }
                                                            ?>
                                                            <?php echo ($pr_module_task->is_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>

                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php
                                          }
                                        }

                                        if ($fk_pair == 0) {
                                          //   $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $fk_project, 0, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $fk_industry, 0, 0, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            foreach ($pr_module_tasks as $pr_module_task) {
                                                $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $fk_industry);
                                                $industry_attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                                $software_attachments = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);

                                                $attachments = count($industry_attachments);
                                                ?>

                                                <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                                                    <input type="hidden" name="description_counter" value="<?php echo strlen($pr_module_task->description_task); ?>" />
                                                    <span class="fa fa-angle-double-right "></span>
                                                    <span class="tasks_title">
                                                        <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>)">
                                                            <?php print $pr_module_task->name_task ?>
                                                            <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>
                                                            <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                                                        </a>
                                                    </span> &nbsp;&nbsp;
                                                    <div class="admin_attachments">
                                                        <span class="attachment_counter" stuff="" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>"><?php print $attachments ?></span>
                                                        <i class="fa fa-paperclip"></i>
                                                    </div>
                                                    <?php
                                                    if ($pr_module_task->name_pair) {
                                                        print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                                                    }

                                                    ?>
                                                    <?php echo ($pr_module_task->is_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>

                                                </li>
                                                <?php
                                            }
                                        } ?>
                                    </ul>
                                </div>
                                <div class="col-md-1 col-xs-2">
                                    <a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onclick="modal_dialog_add_task_industry(<?php print $module->id_module_structure ?>)"> <i class="fa fa-plus"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
              <?php
            }
        }
    }
}

// print industry tree in admin, function used in view industry / show
function printProjectTreeAdminIndustry($module_structure, $fk_industry, $fk_project, $lvl = 1, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0, $has_tasks = 0)
{
    $lvl++;
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
                    $module_structure = Module_structure::getModuleByLvlWithDefault(5, $lvl, $module->id_module_structure, $fk_project);
                ?>
                <li id="<?php print $module->id_module_structure ?>">
                    <span class="menu_title is_project" id="submodule<?php print $module->id_module_structure ?>">
                        <span class="glyphicon glyphicon-minus-sign"></span>
                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                            <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                        <?php print ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : '' ?>
                    </span>
                    <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                    <input type="hidden" name="module_level" value="<?php print $module->level; ?>" />
                    <input type="hidden" name="module_parent" value="<?php print $module->fk_parent; ?>" />
                    <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />
                    <ul class="sortable_modules_<?php print $lvl ?>">
                        <?php printProjectTreeAdminIndustry($module_structure, $fk_industry, $fk_project, $lvl, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks); ?>
                    </ul>
                </li>
                <?php
        } else {
                ?>
                <li class="li_not_selectable" id="<?php print $module->id_module_structure ?>">
                    <span class="menu_title is_project" id="submodule<?php print $module->id_module_structure ?>">
                        <span class="glyphicon glyphicon-minus-sign close_tasks"></span>
                        <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                        <?php echo ($module->is_default == 0) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                    </span>
                    <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                    <input type="hidden" name="module_level" value="<?php print $module->level; ?>" />
                    <input type="hidden" name="module_parent" value="<?php print $module->fk_parent; ?>" />
                    <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />
                    <div class="row border_row margin_left_minus<?php print $lvl ?>">
                        <?php
                            if ($fk_pair == 0) {
                                $task_pairs = Ms_task::getTasksPairsByStructureIndustry($module->id_module_structure, $fk_industry);
                            } else {
                                $task_pairs = Ms_task::getTaskPairsByPairIndustry($module->id_module_structure, $fk_industry, $fk_pair);
                            }
                        ?>
                        <div class="col-xs-2 "></div>
                        <div class="col-xs-10 inside-with-border">
                            <div class="row">
                                <div class="col-md-11 col-xs-10">
                                    <ul class="no-lines sortable_task">
                                        <?php
                                        foreach ($task_pairs as $fk_pair_order => $id_ms_task) {
                                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $fk_industry, 0, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            ?>
                                            <li id="<?php print $id_ms_task ?>">
                                                <ul class="sortable_task inner_nested_child">
                                                    <?php
                                                    foreach ($pr_module_tasks as $pr_module_task) {
                                                        // $attachments = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $fk_industry);
                                                          $attachments = Industry_attach::where('fk_industry_ms', '=', $pr_module_task->id_ms_task)->get();
                                                          // dd($attachments);
                                                        ?>
                                                        <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                                                        <span class="fa fa-angle-right "></span>
                                                        <span class="tasks_title">
                                 <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>)">
                                     <?php print   $pr_module_task->name_task ?>
                                     <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>
                                     <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                                 </a> &nbsp;&nbsp; </span>
                                                            <div class="admin_attachments">
                                                                <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_industry_ms ?>"><?php print count($attachments) ?></span>
                                                                    <i class="fa fa-paperclip"></i>
                                                            </div>
                                                            <?php
                                                            if ($pr_module_task->name_pair) {
                                                                print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                                                            }
                                                            ?>
                                                            <?php echo ($pr_module_task->is_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>

                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php
                                        }
                                        if ($fk_pair == 0) {
                                            $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $fk_project, 0, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            // $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $fk_industry, 0, null, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            foreach ($pr_module_tasks as $pr_module_task) {
                                                $attachments = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $fk_industry);
                                                // $attachments = Industry_attach::where('fk_industry_ms', '=', $pr_module_task->id_ms_task)->get();
                                                      // dd($pr_module_task);
                                                ?>
                                                <li id="<?php print $pr_module_task->id_ms_task ?>" class="field">
                                                    <span class="fa fa-angle-double-right "></span>
                                                <span class="tasks_title">
                   <a href="javascript:void(0)" class="adm_task_title editSubmoduleTask <?php print ($pr_module_task->is_from_industry == 1) ? 'is_from_industry' : '' ?>" onclick="modal_dialog_edit_project_task(<?php print $pr_module_task->id_ms_task ?>, <?php print $fk_project ?>)">
                       <?php print   $pr_module_task->name_task ?>
                       <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>

                       <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                   </a>    </span> &nbsp;&nbsp;
                                                    <div class="admin_attachments">
                                                        <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_industry_ms ?>"><?php print count($attachments) ?></span>
                                                        <i class="fa fa-paperclip"></i>
                                                    </div>
                                                    <?php
                                                      if ($pr_module_task->name_pair) {
                                                          print '<div class="btn btn-round btn-xs" style="background: ' . $pr_module_task->color_pair . '">&nbsp;</div>';
                                                      }
                                                    ?>
                                                    <?php echo ($pr_module_task->ms_tasks_default == 0) ? '<i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                                                </li>
                                              <?php
                                            }
                                        } ?>
                                    </ul>
                                </div>
                                <div class="col-md-1 col-xs-2">
                                    <a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onclick="modal_dialog_add_task_industry(<?php print $module->id_module_structure ?>)"> <i class="fa fa-plus"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php
        }
    }
}

// print project tree in frontend, for a  non empty project
function printTreeProject($module_structure, $fk_industry, $fk_software, $id_project, $set_priority, $lvl = 1, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0, $has_tasks = 0)
{
    $lvl++;
    $current_project = Project::getFirstById($id_project);
    $current_project_created_at = strtotime($current_project->created_at);

    foreach ($module_structure as $module) {
        $module_locked_at = strtotime($module->locked_at);
        // dd($module_locked_at);
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                if (Industry_ms::isIndustryChildChecked($fk_industry, $module->id_module_structure) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $fk_industry, $lvl, $module->id_module_structure, $id_project);
                    ?>
                    <li class="<?php print ($module->is_default == 0 && ($module->locked_at == '0000-00-00 00:00:00' || $module->locked_at == null)) ? '' : 'li_not_editable' ?>" id="<?php print $module->id_module_structure ?>">
                        <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                            <span class="glyphicon glyphicon-minus-sign"></span>
                            <span class="glyphicon glyphicon-sort-by-attributes"></span>
                            <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                            <?php print ($module->is_default == 0 && ($module->locked_at == '0000-00-00 00:00:00' || $module->locked_at == null)) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : '' ?>
                        </span>
                        <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                        <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />

                        <ul class="sortable_modules_<?php print $lvl ?>">
                            <!--                        // recall this function for the next levels -->
                            <?php $has_tasks = printTreeProject($module_structure, $fk_industry, $fk_software, $id_project, $set_priority, $lvl, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks); ?>
                        </ul>
                    </li>
                    <?php
                }
            }
        } else {
            $module_locked_at = strtotime($module->locked_at);
            if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                if (Industry_ms::isIndustryChildChecked($fk_industry, $module->id_module_structure) == true) {
                    ?>
                    <li class="<?php print ($module->is_default == 0 && $module->locked_at == '0000-00-00 00:00:00') ? 'li_not_selectable' : 'li_not_selectable li_not_editable' ?>" id="<?php print $module->id_module_structure ?>">
                        <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                            <span class="glyphicon glyphicon-minus-sign close_tasks"></span>
                            <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                            <?php echo ($module->is_default == 0 && ($module->locked_at == '0000-00-00 00:00:00' || $module->locked_at == null)) ? ' <i class="fa fa-bell-o " style="font-size: 13px"></i>' : ''; ?>
                        </span>
                        <input type="hidden" name="software_id" value="<?php print $fk_software; ?>" />
                        <input type="hidden" name="module_id" value="<?php print $module->id_module_structure; ?>" />

                        <div class="row border_row margin_left_minus<?php print $lvl ?>">
                            <?php
                                if ($fk_pair == 0) {
                                    $task_pairs = Ms_task::getTasksPairsByStructureIndustry($module->id_module_structure, $fk_industry, 1);
                                } else {
                                    $task_pairs = Ms_task::getTaskPairsByPairIndustry($module->id_module_structure, $fk_industry, $fk_pair, 1);
                                }
                            ?>
                            <div class="col-xs-2 ">
                                <div class="circle_tasks_left" id="circle_tasks_left<?php print $module->id_module_structure ?>">
                                    <?php print Ms_task::getCheckedTasks($module->id_module_structure, $fk_industry, $id_project); ?>
                                </div>
                            </div>
                            <div class="col-xs-10 inside-with-border">
                                <div class="row">
                                    <div class="col-md-11 col-xs-10 task_draggable">
                                        <?php
                                        $idsTasks = [];
                                        foreach ($task_pairs as $fk_pair_order => $id_ms_task) {
                                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry1($module->id_module_structure, $fk_industry, $id_project, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            // $pr_module_tasks = Ms_task::getTasksByStructureIndustry1($module->id_module_structure, $fk_industry, $id_project, $fk_pair_order, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            ?>
                                            <div class="task_group_div task_draggable">
                                                <?php
                                                if (count($pr_module_tasks) > 0) {
                                                    $has_tasks = 1;
                                                    foreach ($pr_module_tasks as $pr_module_task) {
                                                        $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                                        $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $fk_industry);
                                                        ?>
                                                        <style type="text/css">
                                                            <?php if ($pr_module_task->task_color != null && $pr_module_task->task_color != 'transparent') {?>
                                                                #panel-task-border<?php print $pr_module_task->id_ms_task; ?> {
                                                                    padding-right: 7px;
                                                                    background-color: <?php print$pr_module_task->task_color; ?>;
                                                                }

                                                                #colorInner<?php print$pr_module_task->id_ms_task; ?> {
                                                                    background-color: <?php print $pr_module_task->task_color; ?>;
                                                                }

                                                                #colorInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                                    background: <?php print $pr_module_task->task_color; ?>;
                                                                }

                                                                #borderInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                                    border-right: 7px solid <?php print $pr_module_task->task_color; ?>;
                                                                }
                                                            <?php } ?>

                                                            <?php if($pr_module_task->is_default == 0 && $module->locked_at == '0000-00-00 00:00:00'){ ?>
                                                                #panel-task<?php print$pr_module_task->id_ms_task; ?> {
                                                                    /*border: 1px solid #FF7878;*/
                                                                }

                                                                #panel-task-header<?php print$pr_module_task->id_ms_task; ?> {
                                                                    /*border-top: 1px solid #FF7878;*/
                                                                }
                                                                <?php }?>
                                                                <?php if(strpos($pr_module_task->color_pair,'#')!== false){ ?>
                                                                #panel-task-body<?php print $pr_module_task->id_ms_task?> {
                                                                    background-color: <?php print $pr_module_task->color_pair ?>;
                                                                }
                                                            <?php } ?>
                                                        </style>
                                                        <div class="panel-task-border task_group_item" id="panel-task-border<?php print $pr_module_task->id_ms_task ?>">
                                                            <div class="panel-task" id="panel-task<?php print $pr_module_task->id_ms_task ?>">
                                                                <?php
                                                                    $title = '';
                                                                    $task_description = trim((str_replace('<br>', ' ', $pr_module_task->task_description)));
                                                                    $description_task = trim((str_replace('', '', $pr_module_task->description_task)));
                                                                    if($task_description === '' || $task_description === null) {
                                                                        $title .= $description_task . ' ' . $task_description;
                                                                    } else {
                                                                        $title .= $task_description;
                                                                    }
                                                                    $title = preg_replace('~<[^<>]+?>~ims', ' ', $title);
                                                                ?>
                                                                <div class="panel-task-body" id="panel-task-body<?php print $pr_module_task->id_ms_task ?>" title="<?php print  $title ?>"
                                                                         onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>,<?php print $id_project ?>)">
                                                                        <?php print $pr_module_task->name_task ?>
                                                                        <?php print ($pr_module_task->is_default == 0 && ($module->locked_at == '0000-00-00 00:00:00' || $module->locked_at == null) ) ? '<i class="fa fa-bell-o"></i> ' : '' ?>
                                                                    <span class="hidden"> <?php print strip_tags($pr_module_task->description_task) ?> </span>
                                                                    <span class="hidden"> <?php print strip_tags($pr_module_task->task_description) ?> </span>
                                                                    <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                                                                </div>
                                                                <div class="panel-task-header" id="panel-task-header<?php print $pr_module_task->id_ms_task ?>">
                                                                    <div class="ls-button-group">
                                                                        <div id="colorPicker<?php print $pr_module_task->id_ms_task ?>" class="colorPicker">
                                                                            <a class="color" id="color<?php print $pr_module_task->id_ms_task ?>">
                                                                                <div class="colorInner" id="colorInner<?php print $pr_module_task->id_ms_task ?>"></div>
                                                                            </a>
                                                                            <div class="track" id="track<?php print $pr_module_task->id_ms_task ?>"></div>
                                                                            <input type="hidden" class="colorInput" id="colorInput<?php print $pr_module_task->id_ms_task ?>"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ls-button-group">
                                                                        <?php print Form::select('set_priority', $set_priority, $pr_module_task->task_priority, ['class' => 'set_priority_task', 'id' => 'set_priority_task' . $pr_module_task->id_ms_task]) ?>
                                                                    </div>
                                                                    <div class="ls-button-group">
                                                                        <i class="fa fa-paperclip"></i>
                                                                <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>">
                                                                    <?php print count($attachments) + count($attachments_industry) ?></span>
                                                                        <?php if ($pr_module_task->is_default == 0 && $module->locked_at == '0000-00-00 00:00:00') { ?>
                                                                            <?php print  Form::open(['action' => 'ProjectsController@remove_new_task', 'class' => 'FormDeleteTask']) ?>
                                                                            <?php print  Form::hidden('fk_project', $id_project) ?>
                                                                            <?php print  Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) ?>
                                                                            <?php print  Form::hidden('fk_module_structure', $module->id_module_structure) ?>
                                                                            <?php print  Form::hidden('fk_task', $pr_module_task->fk_task) ?>
                                                                            <button type="submit" class="btn-icon ">
                                                                                <i class="fa fa-trash red-color"></i>
                                                                            </button>
                                                                            <?php print  Form::close() ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="ls-button-group float-right">
                                                                        <?php if (intval($pr_module_task->fk_pair) > 0) { ?>
                                                                            <input type="radio" name="radio_<?php print $module->id_module_structure . '_' . $pr_module_task->fk_pair ?>"
                                                                                   class="iradio-green iradio-project" value="1" <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project); ?>
                                                                                   id="id_circle<?php print $pr_module_task->fk_module_structure ?> iradio-green<?php print $pr_module_task->id_ms_task; ?>">
                                                                        <?php } else { ?>
                                                                            <?php if ($pr_module_task->is_mandatory) { ?>
                                                                                <i class="fa fa-check-square-o green-color"></i>
                                                                            <?php } else { ?>
                                                                                <input type="checkbox" name="checkbox<?php print  $pr_module_task->id_ms_task ?>" class="icheck-green icheck-project" value="1"
                                                                                       id="id_circle<?php print $pr_module_task->fk_module_structure ?> icheck-green<?php print $pr_module_task->id_ms_task; ?>" <?php print  Ms_task::isTaskCheckedDefaultProject($pr_module_task->id_ms_task, $id_project); ?>
                                                                                    <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project, 1); ?> >
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    <?php } } ?>
                                                </div>
                                            <?php }

                                        if ($fk_pair == 0) {
                                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry1($module->id_module_structure, $fk_industry, $id_project, null, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            // $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $fk_project, 0, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);
                                            if (count($pr_module_tasks) > 0) {
                                                $has_tasks = 1;
                                                foreach ($pr_module_tasks as $pr_module_task) {
                                                  $pr_module_task_locked_at = strtotime($pr_module_task->locked_at);
                                                  if($pr_module_task->locked_at == 'null' || $pr_module_task->locked_at == '0000-00-00 00:00:00' || ($pr_module_task_locked_at > $current_project_created_at)) {
                                                    $project = Project::getFirstById($id_project);
                                                    $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                                    $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->fk_industry);
                                                    ?>
                                                    <style type="text/css">
                                                        <?php
                                                            if ($pr_module_task->task_color != null && $pr_module_task->task_color != 'transparent') {
                                                        ?>
                                                        #panel-task-border<?php print $pr_module_task->id_ms_task; ?> {
                                                            padding-right: 7px;
                                                            background-color: <?php print$pr_module_task->task_color; ?>;
                                                        }

                                                        #colorInner<?php print$pr_module_task->id_ms_task; ?> {
                                                            background-color: <?php print $pr_module_task->task_color; ?>;
                                                        }

                                                        #colorInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                            background: <?php print $pr_module_task->task_color; ?>;
                                                        }

                                                        #borderInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                            border-right: 7px solid <?php print $pr_module_task->task_color; ?>;
                                                        }

                                                        <?php } ?>
                                                        <?php if($pr_module_task->is_default == 0 && $module->locked_at == '0000-00-00 00:00:00'){ ?>
                                                        #panel-task<?php print$pr_module_task->id_ms_task; ?> {
                                                            /*border: 1px solid #FF7878;*/
                                                                min-width: 140px !important;
                                                        }

                                                        #panel-task-header<?php print$pr_module_task->id_ms_task; ?> {
                                                            /*border-top: 1px solid #FF7878;*/
                                                        }

                                                        <?php }?>

                                                        <?php if(strpos($pr_module_task->color_pair,'#')!== false){ ?>
                                                        #panel-task-body<?php print $pr_module_task->id_ms_task?> {
                                                            background-color: <?php print $pr_module_task->color_pair ?>;
                                                        }

                                                        <?php }
                                                        ?>
                                                    </style>
                                                    <div class="panel-task-border task_group_item" id="panel-task-border<?php print $pr_module_task->id_ms_task ?>">
                                                        <div class="panel-task" id="panel-task<?php print $pr_module_task->id_ms_task ?>">
                                                            <?php

                                                            $title = '';
                                                            $task_description = trim((str_replace('<br>', ' ', $pr_module_task->task_description)));
                                                            $description_task = trim((str_replace('', '', $pr_module_task->description_task)));

                                                            if($task_description === '' || $task_description === null) {
                                                                $title .= $description_task . ' ' . $task_description;
                                                            } else {
                                                                $title .= $task_description;
                                                            }
                                                            $title = preg_replace('~<[^<>]+?>~ims', ' ', $title);
                                                            ?>
                                                            <div class="panel-task-body" id="panel-task-body<?php print $pr_module_task->id_ms_task ?>" title="<?php print  $title ?>"
                                                                 onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>,<?php print $id_project ?>)">
                                                                  <?php print $pr_module_task->name_task ?>
                                                                  <?php print ($pr_module_task->is_default == 0 && ($pr_module_task->locked_at == '0000-00-00 00:00:00')) ? '<i class="fa fa-bell-o"></i> ' : '' ?>
                                                                <span class="hidden"><?php print strip_tags($pr_module_task->description_task) ?></span>
                                                                <span class="hidden"><?php print strip_tags($pr_module_task->task_description) ?></span>
                                                                <span class="hidden"><?php print $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?></span>
                                                            </div>
                                                            <div class="panel-task-header" id="panel-task-header<?php print $pr_module_task->id_ms_task ?>">
                                                                <div class="ls-button-group">
                                                                    <div id="colorPicker<?php print $pr_module_task->id_ms_task ?>" class="colorPicker">
                                                                        <a class="color" id="color<?php print $pr_module_task->id_ms_task ?>">
                                                                            <div class="colorInner" id="colorInner<?php print $pr_module_task->id_ms_task ?>"></div>
                                                                        </a>
                                                                        <div class="track" id="track<?php print $pr_module_task->id_ms_task ?>"></div>
                                                                        <input type="hidden" class="colorInput" id="colorInput<?php print $pr_module_task->id_ms_task ?>"/>
                                                                    </div>
                                                                </div>
                                                                <div class="ls-button-group">
                                                                    <?php print Form::select('set_priority', $set_priority, $pr_module_task->task_priority, ['class' => 'set_priority_task', 'id' => 'set_priority_task' . $pr_module_task->id_ms_task]) ?>
                                                                </div>
                                                                <div class="ls-button-group">
                                                                    <i class="fa fa-paperclip"></i>
                                                                <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>">
                                                                    <?php print count($attachments) + count($attachments_industry) ?></span>
                                                                    <?php if ($pr_module_task->is_default == 0 && ($pr_module_task->locked_at == '0000-00-00 00:00:00')) { ?>
                                                                        <?php print Form::open(['action' => 'ProjectsController@remove_new_task', 'class' => 'FormDeleteTask']) ?>
                                                                        <?php print Form::hidden('fk_project', $id_project) ?>
                                                                        <?php print Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) ?>
                                                                        <?php print Form::hidden('fk_module_structure', $module->id_module_structure) ?>
                                                                        <?php print Form::hidden('fk_task', $pr_module_task->fk_task) ?>
                                                                        <button type="submit"
                                                                                class="delete_task btn-icon"
                                                                                data-toggle="modal"
                                                                                data-target="#confirmDelete"
                                                                                data-title="Delete Task"
                                                                                data-id="<?php print($pr_module_task->fk_task) ?>"
                                                                                data-message="<?php print trans('validation.confirmation_delete_task') ?>">
                                                                            <i class="fa fa-trash red-color"></i>
                                                                        </button>
                                                                        <?php print Form::close() ?>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="ls-button-group float-right">
                                                                    <?php if (intval($pr_module_task->fk_pair) > 0) { ?>
                                                                        <input type="radio" name="radio_<?php print $module->id_module_structure . '_' . $pr_module_task->fk_pair ?>"
                                                                               class="iradio-green iradio-project" value="1" <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project); ?>
                                                                               id="id_circle<?php print $pr_module_task->fk_module_structure ?> iradio-green<?php print $pr_module_task->id_ms_task; ?>">
                                                                    <?php } else { ?>
                                                                        <?php if ($pr_module_task->is_mandatory) { ?>
                                                                            <i class="fa fa-check-square-o green-color"></i>
                                                                        <?php } else {
                                                                            ?>
                                                                            <input type="checkbox" name="checkbox<?php print  $pr_module_task->id_ms_task ?>" class="icheck-green icheck-project" value="1"
                                                                                   id="id_circle<?php print $pr_module_task->fk_module_structure ?> icheck-green<?php print $pr_module_task->id_ms_task; ?>"
                                                                                   <?php
                                                                                        // print Ms_task::isTaskCheckedDefaultProject($pr_module_task->id_ms_task, $id_project);
                                                                                        if($pr_module_task->is_default == 0 && ($pr_module_task->locked_at == '0000-00-00 00:00:00')) {
                                                                                            print "disabled";
                                                                                        }
                                                                                   ?>
                                                                                <?php
                                                                                    print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project, 1);
                                                                                ?> >
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                              }
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="col-md-1 col-xs-2">
                                        <a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onclick="modal_dialog_add_task(<?php print $module->id_module_structure ?>,<?php print $fk_industry ?>,<?php print $id_project ?>)"> <i class="fa fa-plus"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                   <?php
                }
            }
        }
    }
    return $has_tasks;
}

// print project tree in frontend, for an   empty project
function printTreeEmptyProject($module_structure, $fk_industry = 0, $fk_software, $id_project, $set_priority, $lvl = 1, $fk_pair = 0, $meta_tags = '', $address_code = '', $fk_priority = 0, $color = '', $is_default = null, $fk_checked = 0, $fk_attach = 0, $has_tasks = 0)
{
    $lvl++;
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            $module_structure = Module_structure::getModuleByLvlWithDefault($fk_software, $lvl, $module->id_module_structure, $id_project);
            ?>
            <li class="<?php print ($module->is_default == 0) ? '' : 'li_not_editable' ?>" id="<?php print $module->id_module_structure ?>">
                <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                    <span class="glyphicon glyphicon-minus-sign"></span>
                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                    <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                </span>
                <ul class="sortable_modules_<?php print $lvl ?>">
                    <!--                        // recall this function for the next levels -->
                    <?php $has_tasks = printTreeEmptyProject($module_structure, $fk_industry, $fk_software, $id_project, $set_priority, $lvl, $fk_pair, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach, $has_tasks); ?>
                </ul>
            </li>
            <?php
        } else {
            ?>
            <li class="<?php print ($module->is_default == 0) ? 'li_not_selectable' : 'li_not_selectable li_not_editable' ?>" id="<?php print $module->id_module_structure ?>">
                <span class="menu_title" id="submodule<?php print $module->id_module_structure ?>">
                    <span class="glyphicon glyphicon-minus-sign close_tasks"></span>
                    <a href="javascript:void(0); "><?php print $module->id_name ?></a>
                </span>
                <div class="row border_row margin_left_minus<?php print $lvl ?>">
                    <?php
                      $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, $id_project, 0, $meta_tags, $address_code, $fk_priority, $color, $is_default, $fk_checked, $fk_attach);

                    ?>
                    <div class="col-xs-2 ">
                        <div class="circle_tasks_left"
                             id="circle_tasks_left<?php print $module->id_module_structure ?>">
                            <?php print Ms_task::getCheckedTasks($module->id_module_structure, $fk_industry, $id_project) ?>
                        </div>
                    </div>
                    <div class="col-xs-10 inside-with-border">
                        <div class="row">
                            <div class="col-md-11 col-xs-10 task_draggable">
                                <?php
                                if (count($pr_module_tasks) > 0) {
                                    $has_tasks = 1;
                                    foreach ($pr_module_tasks as $pr_module_task) {
                                        $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                        ?>
                                        <style type="text/css">
                                            <?php
                                                    if ($pr_module_task->task_color != null && $pr_module_task->task_color != 'transparent') {
                                            ?>
                                            #panel-task-border<?php print $pr_module_task->id_ms_task; ?> {
                                                padding-right: 7px;
                                                background-color: <?php print$pr_module_task->task_color; ?>;
                                            }

                                            #colorInner<?php print$pr_module_task->id_ms_task; ?> {
                                                background-color: <?php print $pr_module_task->task_color; ?>;
                                            }

                                            #colorInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                background: <?php print $pr_module_task->task_color; ?>;
                                            }

                                            #borderInnerTask<?php print $pr_module_task->id_ms_task; ?> {
                                                border-right: 7px solid <?php print $pr_module_task->task_color; ?>;
                                            }

                                            <?php } ?>

                                            <?php if(strpos($pr_module_task->color_pair,'#')!== false){ ?>
                                            #panel-task-body<?php print $pr_module_task->id_ms_task?> {
                                                background-color: <?php print $pr_module_task->color_pair ?>;
                                            }

                                            <?php }
                                            ?>

                                        </style>

                                        <div class="panel-task-border" id="panel-task-border<?php print $pr_module_task->id_ms_task ?>">
                                            <div class="panel-task" id="panel-task<?php print $pr_module_task->id_ms_task ?>">
                                                <?php
                                                $title = '';
                                                $task_description = trim((str_replace('<br>', ' ', $pr_module_task->task_description)));
                                                $description_task = trim((str_replace('', '', $pr_module_task->description_task)));
                                                // if (strlen($description_task) > 0) {
                                                //     $title .= $description_task;
                                                //     if (strlen($task_description) > 0) {
                                                //         $title .= '<hr>';
                                                //     }
                                                // }
                                                // if (strlen($task_description) > 0) {
                                                //     $title .= $task_description;
                                                // }

                                                if($task_description === '' || $task_description === null) {
                                                    $title .= $description_task . ' ' . $task_description;
                                                } else {
                                                    $title .= $task_description;
                                                }

                                                $title = preg_replace('~<[^<>]+?>~ims', ' ', $title);

                                                ?>
                                                <div class="panel-task-body" id="panel-task-body<?php print $pr_module_task->id_ms_task ?>" title="<?php print $title ?>"
                                                     onclick="modal_dialog_edit_task(<?php print $pr_module_task->id_ms_task ?>,<?php print $id_project ?>)">

                                                    <?php print $pr_module_task->name_task ?>

                                                    <span class="hidden">  <?php print strip_tags($pr_module_task->description_task) ?>       </span>
                                                    <span class="hidden">  <?php print strip_tags($pr_module_task->task_description) ?>       </span>
                                                    <span class="hidden"> <?php print  $pr_module_task->meta_tags . ' ' . $pr_module_task->address_code; ?>  </span>
                                                </div>
                                                <div class="panel-task-header" id="panel-task-header<?php print $pr_module_task->id_ms_task ?>">
                                                    <div class="ls-button-group">
                                                        <div id="colorPicker<?php print $pr_module_task->id_ms_task ?>" class="colorPicker">
                                                            <a class="color" id="color<?php print $pr_module_task->id_ms_task ?>">
                                                                <div class="colorInner" id="colorInner<?php print $pr_module_task->id_ms_task ?>"></div>
                                                            </a>
                                                            <div class="track" id="track<?php print $pr_module_task->id_ms_task ?>"></div>
                                                            <input type="hidden" class="colorInput" id="colorInput<?php print $pr_module_task->id_ms_task ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="ls-button-group">
                                                        <?php print Form::select('set_priority', $set_priority, $pr_module_task->task_priority, ['class' => 'set_priority_task', 'id' => 'set_priority_task' . $pr_module_task->id_ms_task]) ?>
                                                    </div>
                                                    <div class="ls-button-group">
                                                        <i class="fa fa-paperclip"></i>
                                                        <span class="attachment_counter" id="attachment_counter<?php print $pr_module_task->id_ms_task ?>"><?php print count($attachments) ?></span>
                                                        <?php if ($pr_module_task->is_default == 0) { ?>
                                                            <?php print  Form::open(['action' => 'ProjectsController@remove_new_task', 'class' => 'FormDeleteTask']) ?>
                                                            <?php print  Form::hidden('fk_project', $id_project) ?>
                                                            <?php print  Form::hidden('fk_ms_task', $pr_module_task->id_ms_task) ?>
                                                            <?php print  Form::hidden('fk_module_structure', $module->id_module_structure) ?>
                                                            <?php print  Form::hidden('fk_task', $pr_module_task->fk_task) ?>
                                                            <button type="submit"
                                                                    class="delete_task btn-icon"
                                                                    data-toggle="modal"
                                                                    data-target="#confirmDelete"
                                                                    data-title="DeleteTask"
                                                                    data-id="<?php print($pr_module_task->fk_task) ?>"
                                                                    data-message="<?php print trans('validation.confirmation_delete_task') ?>">
                                                                <i class="fa fa-trash red-color"></i>
                                                            </button>
                                                            <?php print  Form::close() ?>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="ls-button-group float-right">
                                                        <?php if (intval($pr_module_task->fk_pair) > 0) { ?>
                                                            <input type="radio" name="radio_<?php print $module->id_module_structure . '_' . $pr_module_task->fk_pair ?>"
                                                                   class="iradio-green iradio-project" value="1" <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project); ?>
                                                                   id="id_circle<?php print $pr_module_task->fk_module_structure ?> iradio-green<?php print $pr_module_task->id_ms_task; ?>">
                                                        <?php } else { ?>
                                                            <?php if ($pr_module_task->is_mandatory) { ?>
                                                                <i class="fa fa-check-square-o green-color"></i>
                                                            <?php } else { ?>
                                                                <input type="checkbox" name="checkbox<?php print  $pr_module_task->id_ms_task ?>" class="icheck-green icheck-project" value="1"
                                                                       id="id_circle<?php print $pr_module_task->fk_module_structure ?> icheck-green<?php print $pr_module_task->id_ms_task; ?>"
                                                                    <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $id_project); ?> >
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <?php
                                    } ?>
                                  <?php
                                }
                              ?>
                            </div>
                            <div class="col-md-1 col-xs-2">
                                <a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onclick="modal_dialog_add_task(<?php print $module->id_module_structure ?>,<?php print $fk_industry ?>,<?php print $id_project ?>)"> <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <?php
        }
    }


    return $has_tasks;
}


function wordTreeProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $section)
{
    $multilevelNumberingStyleName = 'multilevel';
    $lvl++;
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                $section->addTitle($module->id_name, $lvl);
                wordTreeProject($module_structure, $fk_software, $id_industry, $lvl, $id_project, $section);
            }
        } else {
            if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);

                if (count($pr_module_tasks) > 0) {
                    $section->addTitle($module->id_name, $lvl);
                    foreach ($pr_module_tasks as $pr_module_task) {
                        $section->addTitle($pr_module_task->name_task, $lvl);
                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                            $html = $pr_module_task->task_description;
                        } else {
                            $html = $pr_module_task->description_task;
                        }
                        $html .= '<br>';

                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);

                        $section->addText('Gewichtung: ' . $pr_module_task->task_priority);
                        $section->addTextBreak(2);
                    }


                }
            }
        }
    }
}

function wordTreeProject2($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $section, $key1, $first = '')
{
    $array = [];
    $multilevelNumberingStyleName = 'multilevel';
    if (empty($first)) {
        $first = [];
    }
    foreach ($module_structure as $key2 => $module) {
        $key2++;
        if (Module_structure::hasChild($module->id_module_structure) == 1) {
            if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $module->id_module_structure, $id_project);
                $section->addTitle($module->id_name, $lvl);
                array_push($first, '<div style="margin-left: 50px;  "><h3>'.$key1.'.'.$key2.' '. $module->id_name . '</h3></div><br>');
                wordTreeProject2($module_structure, $fk_software, $id_industry, $lvl, $id_project, $section, $key1, $first);
            }
        } else {
            if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);

                if (count($pr_module_tasks) > 0) {
                    array_push($array, '<h3>'.$key1.'.'.$key2.' '. $module->id_name . '</h3><br>');
                    foreach ($pr_module_tasks as $key3 => $pr_module_task) {
                        $key3++;
                        array_push($array, '<h3>'.$key1.' '.$key2.' '.$key3.' '. $pr_module_task->name_task . '</h3><br>');
                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                            $html = $pr_module_task->task_description;
                        } else {
                            $html = $pr_module_task->description_task;
                        }
                        $html .= '<br>';
                        array_push($array, $html);
//                                        $content .= 'Gewichtung: ' . $pr_module_task->task_priority.'<br>';

                    }
                }
            }
        }
    }
    array_push($array, $first);
    return $array;
}

function wordTreeEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $section)
{
    $lvl++;
    foreach ($module_structure as $module) {
        if (Module_structure::hasChild($module->id_module_structure) == 1) {

            $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
            $section->addTitle($module->id_name, $lvl);

            wordTreeEmptyProject($module_structure, $fk_software, $id_industry, $lvl, $id_project, $section);


        } else {
            $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);

            if (count($pr_module_tasks) > 0) {
                $section->addTitle($module->id_name, $lvl);

                foreach ($pr_module_tasks as $pr_module_task) {
                    $section->addTitle($pr_module_task->name_task, $lvl);
//

                    if (strlen(trim($pr_module_task->task_description)) > 3) {
                        $html = $pr_module_task->task_description;
                    } else {
                        $html = $pr_module_task->description_task;
                    }
                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);

                    if ($pr_module_task->is_not_offer == 1) {
                        $section->addText('NO');
                    } else {
                        $section->addText('YES');
                    }
                    $section->addText('Von der Ausschreibung ausgeschlossen  ');
                    if ($pr_module_task->is_control == 1) {
                        $section->addText('YES');
                    } else {
                        $section->addText('NO');
                    }
                    $section->addText('Kontrolfrage ');
                }

            }
        }
    }
}

function allowOnlySuperAdmin()
{
    if (Auth::check()) {
        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        if (in_array(1, $fk_group)) {
        } else {
            return App::abort(404);
        }
    } else {
        return App::abort(404);
    }
}

function allowOnlyCompanyAdmin()
{
    if (Auth::check()) {
        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        if (in_array(2, $fk_group)) {

        } else {
            return App::abort(404);
        }
    } else {
        return App::abort(404);
    }
}


?>
