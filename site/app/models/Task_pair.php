<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 04-Dec-15
 * Time: 09:55
 */
class Task_pair extends Eloquent
{


    public static $messages = [];
    public $primaryKey = 'id_pair';
    protected $table = 'pr_tasks_pair';
    protected $guarded = ['id_pair'];
    protected $fillable = ['name_pair'];

    public static function getFirstById($id)
    {
        $list = Task_pair::where('id_pair', '=', $id)->first();
        return $list;
    }

    public static function getFirstByName($id)
    {
        $list = Task_pair::where('name_pair', '=', $id)->first();
        return $list;
    }

    public static function getAllPairsList()
    {
        $array = array();
        $lists = Task_pair::select('*')->get();

        foreach ($lists as $list) {
            $array[$list->id_pair] = $list->name_pair;
        }
        return $array;
    }


}