@extends('front.layouts.login')
        <!-- include de scripts necessary for google maps locations -->
@section('header_scripts')
@stop

@section('footer_scripts')
    @include('utils/validations_login')
    @include('utils/ajax')
    <script>
        $(document).on('click', '.login-submit-btn', function(e) {
            if(switchToInvalidTab('.reset-password-form')) {
                $("html, body").animate({scrollTop: 0}, 1000);
                var elements = $('.reset-password-form input');
                for(var i = 0; i < elements.length; i++) {
                    $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
                }
            } else {
                e.preventDefault();
            }
        });
    </script>
@stop

@section('content')
    <div class="reset-password-title">
        <div class="login-user-icon">NEUES PASSWORT VERGEBEN</div>
    </div>
    {{-- the search form --}}
    <div class="login-form">
        @if ($errors->first() == "password ok")
          <div class="login-msg">
            Sie haben Ihr Passwort erfolgreich geändert.
            <br><br>
            Sie können Ihr Passwort jederzeit auch in Ihren persönlichen Einstellungen ändern.
          </div>
        @else
            <!-- /.box-header -->
            <div class="formular form-horizontal ls_form">
                <form method="POST" action="{{action('SessionsController@postRecoverPassword', $token)}}" class="reset-password-form">
                    {{-- {{ $errors->first("confirm_",'<div class="amaran_confirm">:message</div>') }} --}}
                    {{-- {{ $errors->first("error_",'<div class="amaran_error">:message</div>') }} --}}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group has-feedback">
                        {{ Form::password("password",[ "placeholder"=>"Passwort eingeben", "class"=>"form-control login-input",'id'=>'password', "required"]) }}
                        <span class="glyphicon glyphicon-lock form-control-feedback login-glyphicon"></span>
                        {{-- {{ $errors->first("password",'<div class="text-red">:message</div>') }} --}}
                    </div>
                    <div class="form-group has-feedback">
                        {{ Form::password("password_confirm",[ "placeholder"=>"Passwort wiederholen", "class"=>"form-control login-input",'id'=>'password_confirm', "required"]) }}
                        <span class="glyphicon glyphicon-lock form-control-feedback login-glyphicon"></span>
                        {{-- {{ $errors->first("password_confirm",'<div class="text-red">:message</div>') }} --}}
                    </div>
                    @if ($errors->first() == "password did not match")
                      <div class="recover-password-msg">Passwort stimmt nicht überein</div>
                    @endif
                    <div class="input-group ls-group-input login-btn-box col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="login-submit-btn logo-color-2 btn ladda-button col-md-12 col-sm-12 col-xs-12" >
                            PASSWORT ÄNDERN
                        </button>
                    </div>
                <!-- End SmartWizard Content -->
                </form>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>

        @endif
        <div class="reset-password-hr"></div>
        <div class="input-group ls-group-input login-btn-box">
            <a href="{{URL::to('/login')}}" class="register-now back-to-login logo-color-1">ANMELDEN</a>
        </div>
    </div>

    </div>
    <div class="clear"></div>
    </div>
@stop
