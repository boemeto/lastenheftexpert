@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    @include('utils/maps_location')
@stop
@section('footer_scripts')
@stop
@section('content')
    <div class="panel widget light-widget panel-bd-top ">
        <div class="panel-heading  ">
            <h3 class="mgtp-10"> {{trans('messages.nav_companies')}}</h3>
        </div>
        <div class="panel-body table_invoice">
            <table class="table table-condensed  table-responsive  data_table">
                <thead>
                <tr>
                    <th>{{trans('messages.label_name')}}</th>
                    <th>{{trans('messages.label_customer_id')}}</th>
                    <th>{{trans('messages.label_street_and_nr')}}</th>
                    <th>{{trans('messages.label_city')}}</th>
                    <th>{{trans('messages.label_state')}}</th>
                    <th>{{trans('messages.label_country')}}</th>
                    <th>{{trans('messages.label_registered_on')}}</th>
                    <th>{{trans('messages.label_action')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <?php
                    $main_headquarter = Company_headquarters::getMainHeadquarter($company->id_company);
                    ?>
                    <tr>
                        <td>{{$company->name_company}}</td>
                        <td class="pull-left">{{getClientId($company->id_company)}}</td>
                        <td>{{$main_headquarter->street}}</td>
                        <td>{{$main_headquarter->city}}</td>
                        <td>{{$main_headquarter->state}}</td>
                        <td>{{$main_headquarter->country}}</td>
                        <td class="pull-left">{{date('d.m.Y', strtotime($company->created_at))}}</td>
                        <td>
                            <a href="{{URL::to('acompany/'.$company->id_company.'/edit')}}" class="btn btn-round   ls-orange-btn"><i class="fa fa-pencil-square-o"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
