<li class=""><a href="#job-history" data-toggle="tab">
        <i class="fa fa-sitemap"></i> <span>Projects</span></a></li>

<!-- Projects Tab Start -->
<div class="tab-pane fade" id="job-history">
    <div class="row">
        <div class="user-friend-list">

            {{-- projects details--}}
            @if(count($projects) > 0)
                @foreach($projects as $project)
                    <?php
                    $users_projects = User_projects::getSelectableUsersByProject($project->id_project);
                    $users_selected = User_projects::getSelectedUsersByProject($project->id_project);
                    ?>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="ls-user-feature clearfix">
                                    <ul>
                                        <li>
                                            <a href="{{Url::to('projects/'.$project->id_project
                                                                    .'/edit')}}">
                                                <div id="ls-user-following" class="circliful">
                                                    <span class="circle-text" style="  font-size: 14px;"> 20%</span>
                                                    <span class="circle-info">Completed</span>
                                                    <canvas width="112" height="112"></canvas>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="user_header">
                                    <a href="{{Url::to('projects/'.$project->id_project.'/edit')}}" class="a_details_form_show_name">
                                        {{ $project->name_project }}
                                    </a>
                                </div>
                                <span class="user-projects">
                                   {{Industry::getNameById($project->fk_industry)}}
                                    <br>
                                </span>
                                <ul class="menu_user">
                                    <li>
                                        <a href="#">
                                            Delete
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{Url::to('projects/'.$project->id_project.'/edit')}}">Edit</a>
                                    </li>
                                </ul>
                                <?php $project_modules =
                                        Module_structure::getModuleList($project->fk_industry); ?>
                                <div class="table-responsive ls-table ">
                                    <table class="table table-projects-status">
                                        <tbody>
                                        @foreach($project_modules as $project_module)
                                            <tr>
                                                <td>{{$project_module->name_module}}</td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-success"
                                                             role="progressbar"
                                                             aria-valuetransitiongoal="50"></div>
                                                    </div>
                                                </td>
                                                <td> 50%</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="ls-user-feature clearfix">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div id="ls-user-following"
                                                     class="circliful">
                                                                            <span class="circle-text" style="  font-size: 14px;">
                                                                                {{count($users_selected)}}</span>
                                                    <span class="circle-info">Users</span>
                                                    <canvas width="112"
                                                            height="112"></canvas>
                                                </div>
                                            </a>
                                        </li>
                                        @if(count($users_selected) >0)
                                            @foreach($users_selected as $user_selected)
                                                <?php    $image_user_proj = User::getLogoUser
                                                ($user_selected->fk_user);  ?>
                                                <li>

                                                    <img class="img-circle" alt="friends
                                                                                pic" src="{{ URL::asset
                                                                                ($image_user_proj)}}">
                                                    @if($user_selected->is_owner == 0)
                                                        <a href="{{Url::to('projects_remove_users/' .$user_selected->id_user_project)}}" onclick="return confirm('Are ' +'you sure you want to delete ' +   'this user?')">
                                                           <span class="remove-user ls-friend-status">
                                                                      <i class="fa fa-times-circle"></i>
                                                                                </span></a>
                                                    @endif
                                                </li>
                                            @endforeach
                                        @endif
                                        @if(count($users_projects)>0)
                                            <li class="light-green-color">
                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#modalAddUsersProjects{{$project->id_project}}"
                                                   title="Add Users">
                                                    <div id="ls-user-following" class="circliful">
                                                                            <span class="circle-text"
                                                                                  style="  font-size: 14px;">
                                                                                <i class="fa
                                                                                  fa-plus"></i>
                                                                            </span>
                                                                                <span
                                                                                        class="circle-info">
                                                                                    Add Users</span>
                                                        <canvas width="112" height="112"></canvas>
                                                    </div>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                @if(count($users_projects)>0)
                                    <div class="modal fade"
                                         id="modalAddUsersProjects{{$project->id_project}}"
                                         tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true"
                                         style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header label-light-green white">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal"
                                                            aria-hidden="true">x
                                                    </button>
                                                    <h4 class="modal-title"
                                                        id="myModalLabelSuccess">
                                                        Add user</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="media ls-friends-info-wrap">

                                                    </div>
                                                    <div class="ls-friends-about">

                                                        {{ Form::model($project,
                                                        ['method'=>'POST',
                                                        'action'=>['ProjectsController@projects_add_users']])}}
                                                        <table class="table">
                                                            <tr>
                                                                <th> Select</th>
                                                                <th> User name</th>
                                                            </tr>
                                                            @foreach($users_projects as $users_project)
                                                                <tr>
                                                                    <td>
                                                                        {{ Form::checkbox('fk_user_project[]',
                                                                        $users_project->id, null, ['class' => 'icheck-square-green select_users']) }}
                                                                    </td>
                                                                    <td> {{$users_project->first_name}}
                                                                        {{$users_project->last_name}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button"
                                                            class="btn ls-red-btn"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    {{Form::hidden('fk_project',
                                                    $project->id_project)}}
                                                    {{ Form::submit("Add Users",["class" =>
                                                    "btn ls-light-green-btn" ]) }}
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
</div>
<!-- Projects Tab End -->
