<?php
use Illuminate\Support\MessageBag;

class HeadquartersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource for headquarter table.
     *
     * @return Response
     */
    public function create_adm($fk_company)
    {
        allowOnlySuperAdmin();
        $company = Company::getFirstById($fk_company);
        return View::make('admin.headquarters.create', compact('fk_company', 'company'));

    }


    /**
     * Store a newly created resource in storage.
     * Stores a new headquarter
     * @return Response
     */
    public function store()
    {

        // get the input fields
        $company_id = Input::get('id_company');
        $autocomplete_fields = Input::get('autocomplete');
        $street_fields = Input::get('street');
        $street_number_fields = Input::get('street_number');
        $city_fields = Input::get('city');
        $state_fields = Input::get('state');
        $country_fields = Input::get('country');
        $postal_code_fields = Input::get('postal_code');
        $latitude_fields = Input::get('latitude');
        $longitude_fields = Input::get('longitude');
        $telephone_fields = Input::get('telephone_headquarter');
        $email_fields = Input::get('email_headquarter');
        $name_fields = Input::get('name_headquarter');

        $headquarters = array();
// foreach headquarter (you can add more than one headquarter at once)
        foreach ($autocomplete_fields as $key => $autocomplete) {
// save the locations
            $location = new Location;
            $location->address_string = $autocomplete;
            $location->street = $street_fields[$key];
            $location->street_nr = $street_number_fields[$key];
            $location->city = $city_fields[$key];
            $location->country = $country_fields[$key];
            $location->state = $state_fields[$key];
            $location->postal_code = $postal_code_fields[$key];
            $location->latitude = $latitude_fields[$key];
            $location->longitude = $longitude_fields[$key];
            $location->save();
            // save the headquarters
            $headquarter = new Company_headquarters();
            $headquarter->fk_location = $location->id_location;
            $headquarter->fk_company = $company_id;
            $headquarter->name_headquarter = $name_fields[$key];
            $headquarter->telephone_headquarter = $telephone_fields[$key];
            $headquarter->email_headquarter = $email_fields[$key];
            $headquarter->save();
        }
        $id_headquarter = $headquarter->id_headquarter;

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        $redirect_link = 'headquarters/' . $id_headquarter;
        if (Input::has('company_edit_headquarter')) {
            return Redirect::action('CompanyController@show', 'headquarters')->withErrors($errors);
        }
        if (Input::has('redirect_link')) {
            $redirect_link = Input::get('redirect_link');
        }
        return Redirect::to($redirect_link)->withErrors($errors);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $headquarter = Company_headquarters::getHeadquartersById($id);
        $id_user = Auth::user()->id;
        $fk_group = User_groups::getGroupByUser($id_user);
        // if user is company admin
        if (in_array(1, $fk_group)) {
            return View::make('admin.headquarters.edit', compact('headquarter'));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
// update headquarter's location
        $location = Location::whereId_location(Input::get('id_location'))->first();
        $location->address_string = Input::get('autocomplete');
        $location->street = Input::get('street');
        $location->street_nr = Input::get('street_number');
        $location->city = Input::get('city');
        $location->country = Input::get('country');
        $location->state = Input::get('state');
        $location->postal_code = Input::get('postal_code');
        $location->latitude = Input::get('latitude');
        $location->longitude = Input::get('longitude');
        $location->save();

// update the headquarter's detals
        $headquarter = Company_headquarters::whereId_headquarter($id)->first();
        $headquarter->name_headquarter = Input::get('name_headquarter');
        $headquarter->telephone_headquarter = Input::get('telephone_headquarter');
        $headquarter->email_headquarter = Input::get('email_headquarter');
        $headquarter->fax_headquarter = Input::get('fax_headquarter');
        $headquarter->social_xing = Input::get('social_xing');
        $headquarter->social_linkedin = Input::get('social_linkedin');

        $headquarter->social_twitter = Input::get('social_twitter');
        $headquarter->save();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        if (Input::has('redirect_link')) {
            $redirect_link = Input::get('redirect_link');
        } else {
            if(Input::has('admin_change_headquarter')) {
                Session::flash('tab', 'headquarters');
                return Redirect::action('CompanyAdmController@edit', $headquarter->fk_company)->withErrors($errors);
            } else {
                return Redirect::action('CompanyController@show', 'headquarters')->withErrors($errors);
            }
        }
        return Redirect::to($redirect_link)->withErrors($errors);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function make_main($id)
    {
        $headquarter = Company_headquarters::getFirstById($id);

        $heaquarter_main = Company_headquarters::getMainHeadquarter($headquarter->fk_company);
        $heaquarter1 = Company_headquarters::getFirstById($heaquarter_main->id_headquarter);
        $heaquarter1->is_main = 0;
        $heaquarter1->save();

        $headquarter = Company_headquarters::getFirstById($id);
        $headquarter->is_main = 1;
        $headquarter->save();


        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::action('CompanyController@show', 'headquarters')->withErrors($errors);
    }

    public function admin_make_main($id)
    {
        $headquarter = Company_headquarters::getFirstById($id);

        $heaquarter_main = Company_headquarters::getMainHeadquarter($headquarter->fk_company);
        $heaquarter1 = Company_headquarters::getFirstById($heaquarter_main->id_headquarter);
        $heaquarter1->is_main = 0;
        $heaquarter1->save();

        $headquarter = Company_headquarters::getFirstById($id);
        $headquarter->is_main = 1;
        $headquarter->save();


        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        Session::flash('tab', 'headquarters');
        return Redirect::action('CompanyAdmController@edit', $headquarter->fk_company)->withErrors($errors);
    }

    public function delete_adm($id_headquarter)
    {
        DB::beginTransaction();

        try {
            User::where('fk_headquarter', '=', $id_headquarter)->delete();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
            return Redirect::back()->withErrors($errors);
        }

        try {
            Company_headquarters::where('id_headquarter', '=', $id_headquarter)->delete();
        } catch (\Exception $e) {
            DB::rollback();
            $errors = new MessageBag(['error_' => [Lang::get('validation.message_error_delete')]]);
            return Redirect::back()->withErrors($errors);
        }


        DB::commit();
        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);


    }

}
