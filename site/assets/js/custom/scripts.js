/* Export Modal - START */
$(document).on('click', '.btn-export-modal', function() {
    var software_id = $(this).closest('li').find('[name="software_id"]').val();
    var module_id = $(this).closest('li').find('[name="module_id"]').val();
    var industry_id = $(this).parents('.task_page_tree').find('[name="industry_id"]').val();
    var project_id = $(this).parents('.task_page_tree').find('[name="project_id"]').val();

    if (software_id != '0') {
        $('#modal-export .show-project-export').hide();
        $('#modal-export .show-industries-export').hide();
        $('#modal-export .show-software-export').show();

        $('#modal-export .show-software-export > a').each(function() {
            var href = $(this).attr('href');
            href = href.split('/');

            if (module_id) {
                href[4] = software_id;

                if (industry_id) {
                    href[5] = module_id;
                    href[6] = industry_id;
                } else {
                    if (module_id != '0') {
                        href[5] = module_id;
                    } else {
                        if (href.length > 5) {
                            href = href.slice(0,-1);
                        }
                    }
                }

                if (project_id) {
                    href[7] = project_id;
                }
            } else {
                if (href.length > 6) {
                    href = href.slice(0,-1);
                }
                if (href.length > 5) {
                    href = href.slice(0,-1);
                }
                if (href.length > 4) {
                    href = href.slice(0,-1);
                }
                href[4] = software_id;
            }

            href = href.join('/');

            $(this).attr('href',href);
        });
    } else {
        $('#modal-export .show-software-export').hide();
        $('#modal-export .show-project-export').show();
        $('#modal-export .show-industries-export').show();
    }
});
$(document).on('click', '#modal-export .show-software-export > a', function() {
    $('#modal-export').modal('hide');
});
/* Export Modal - END */

$(document).on('click', '.trigger-attachments-zip', function() {
    var href = $(this).closest('div').find('.get-attachments-zip').attr('href');

    window.location.href = href;
});