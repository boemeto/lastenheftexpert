@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <link href="{{ URL::asset('assets/css/plugins/fileinput.min.css') }}" rel="stylesheet">
@stop
@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>

    <!--File input Script start -->
    <script src="{{ URL::asset('assets/js/fileinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.autosize.js') }}"></script>
    <!--Form Script end -->
    <script type="text/javascript">

        // open add new task for an existing submodule
        function modal_dialog_edit_company(id) {
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/acompany/" + id + "/edit";

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);
                                $(".switchCheckBox").bootstrapSwitch();
                                animated_text_area();
                                file_input_trigger();
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_add_company() {
            var id = 100;
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/acompany/create";

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);

                                animated_text_area();
                                file_input_trigger();
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_delete_company(id) {
            var x = confirm('Are you sure?');
            if (x) {
                window.location.href = "{{URL::to('/')}}/acompany/delete_adm/" + id;
            } else {

            }
        }

        function modal_dialog_edit_headquarter(id) {
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/headquarters/" + id + "/edit";

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);


                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_add_headquarter(id) {
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/headquarters/create_adm/" + id;

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);


                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_delete_headquarter(id) {
            var x = confirm('Are you sure?');
            if (x) {
                window.location.href = "{{URL::to('/')}}/headquarters/delete_adm/" + id;
            } else {

            }
        }


        function modal_dialog_edit_user(id) {
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/members/" + id + "/edit";

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);

                                animated_text_area();
                                file_input_trigger();
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_add_user(id) {
            if (id == 0) {

                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {


                var strURL = "{{URL::to('/')}}/members/create_adm/" + id;

                var req = getXMLHTTP();

                if (req) {

                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {

                                jQuery('#task_page_details').html(req.responseText);

                                animated_text_area();
                                file_input_trigger();
                                show_modal_box();

                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }

        }

        function modal_dialog_delete_user(id) {
            var x = confirm('Are you sure?');
            if (x) {
                window.location.href = "{{URL::to('/')}}/members/delete_adm/" + id;
            } else {

            }
        }

        function animated_text_area() {
            'use strict';

            $('.animatedTextArea').autosize({append: "\n"});
        }

        /*** file input Call ****/
        function file_input_trigger() {
            'use strict';
            $("#file-user").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Logo ...',
                fileType: "any",
                'showUpload': false
            });
        }

    </script>
    @include('admin/company/tree_script')
    @include('admin/scripts/ajax_scripts')
    @include('utils/maps_location')
@stop

@section('content')
    <div class="box box-default">
        <div class="hide_controller">
            <a href="#" onclick="show_modal_box();"><i class="fa fa-arrow-circle-left"></i> </a>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">
                <button name="add_company" type="button" class="btn ls-light-blue-btn" onclick="modal_dialog_add_company()">
                    <i class="fa fa-plus"></i>
                </button>

                {{trans('messages.nav_companies')}} </h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="easy-tree ls-tree-view">
                    <ul class="ul-tree">
                        @foreach($companies as $company)
                            <li id="{{$company->id_company}}" class="company li_editable">
                                <span class="glyphicon glyphicon-minus-sign"></span>
                                <span class="menu_title" id="company{{$company->id_company}}">
                                    <a href="javascript:void(0); " {{($company->is_blocked == 1)?"style='color:red'":""}}>
                                        {{ $company->name_company }}
                                    </a>
                          </span>
                                <ul>
                                    <?php
                                    $headquarters = Company_headquarters::getHeadquartersLocations($company->id_company); ?>
                                    @foreach($headquarters as $headquarter)
                                        <li id="{{$headquarter->id_headquarter}}" class="headquarter">
                                            <span class="glyphicon glyphicon-minus-sign"></span>
                                <span class="menu_title" id="headquarter{{$headquarter->id_headquarter}}">
                                        
                                                         <a href="javascript:void(0); ">
                                                             {{ $headquarter->name_headquarter }}
                                                         </a>
                          </span>

                                            <ul>
                                                <?php
                                                $users = User::getUsersByCompanyHeadquarter($company->id_company, $headquarter->id_headquarter); ?>
                                                @foreach($users as $user)
                                                    <li id="{{$user->id}}" class="user li_not_selectable">
                                                   
                                <span class="menu_title" id="user{{$user->id}}">
                                                         <a href="javascript:void(0); ">
                                                             {{ $user->first_name }} {{ $user->last_name }}
                                                         </a>
                          </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div id="task_page_details_parent">
                <div class="task_page_details" id="task_page_details"></div>
            </div>
        </div>

        <!-- /.box-body -->

    </div>

    <div class="iframe-overlay">
        {{-- onclick="hide_modal_box()"--}}
    </div>


@stop