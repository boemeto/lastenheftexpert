<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Feb-16
 * Time: 10:48
 */
?>
@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <link href="{{ URL::asset('assets/css/plugins/fileinput.min.css') }}" rel="stylesheet">
    @stop
    @section('footer_scripts')
            <!--File input Script start -->
    <script src="{{ URL::asset('assets/js/fileinput.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.autosize.js') }}"></script>
    <!--Form Script end -->
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            $('.delete_user').click(function (event) {
                var x = confirm("Are you sure you want to block user?");
                if (x) {
                    return true;
                }
                else {

                    event.preventDefault();
                    return false;
                }
            });
            /* Show user edit form */
            $('.a_form_show').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_show_', "");
                $('.a_details_form_show_name').not('#a_details_form_show_name_' + id_nr).removeClass('active');
                $('#a_details_form_show_name_' + id_nr).toggleClass("active");
                //   $('.details_user_form_hidden').hide();
                $('.edit_form_hidden').not('#edit_form_hidden_' + id_nr).hide();
                $('#edit_form_hidden_' + id_nr).toggle('blind');
                event.preventDefault();
            });
            /* Hide user edit form */
            $('.a_form_close').click(function (event) {
                var id = jQuery(this).attr('id');
                var id_nr = id.replace('a_form_close_', "");
                $('#edit_form_hidden_' + id_nr).hide('blind');
                event.preventDefault();
            });
            // image input functions
            animated_text_area();
            file_input_trigger();

        });
        function animated_text_area() {
            'use strict';

            $('.animatedTextArea').autosize({append: "\n"});
        }
        /*** file input Call ****/
        function file_input_trigger() {
            'use strict';

            @foreach($consultants as $user)
              $("#file-user<?php print $user->id_consultant ?>").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Image ...',
                fileType: "any",
                'showUpload': false
            });
            @endforeach
            $("#file-3").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Logo ...',
                fileType: "any",
                'showUpload': false
            });
            $("#file-user").fileinput({
                showCaption: false,
                browseClass: "btn ls-light-blue-btn",
                browseLabel: 'Image ...',
                fileType: "any",
                'showUpload': false
            });

        }
    </script>
    @stop

    @section('content')


            <!-- visible content start -->


    <div class="panel widget light-widget panel-bd-top">
        <div class="panel-heading bordered">
            <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-users mgr-10 profile-icon"></i> {{trans('messages.nav_consultants')}}</h3>
        </div>
        <div class="panel-body">
            <div class=" row  content-grid  ">
                @foreach($consultants as $user)
                    <?php $image_user = Consultant::getLogoUser($user->id_consultant);
                    ?>
                    <div class="col-lg-3 col-xs-4 friends-tab consultant_list">
                        <div>


                            <div class="menu-icon">
                                <ul class="social-links-user small">
                                    <li>
                                        @if($user->social_xing)
                                            <a href="{{$user->social_xing}}" target="_blank" class="btn ls-red-btn btn-round"><i class="fa fa-xing"></i></a>
                                        @else
                                            <a href="#" class="btn ls-red-btn btn-round disabled"><i class="fa fa-xing"></i></a>
                                        @endif
                                    </li>
                                    <li>
                                        @if($user->social_linkedin)
                                            <a href="{{$user->social_linkedin}}" target="_blank" class="btn ls-red-btn btn-round"><i class="fa fa-linkedin"></i></a>
                                        @else
                                            <a href="#" class="btn ls-red-btn btn-round  disabled"><i class="fa fa-linkedin"></i></a>
                                        @endif
                                    </li>
                                    <li>
                                        @if($user->social_twitter)
                                            <a href="{{$user->social_twitter}}" target="_blank" class="btn ls-red-btn btn-round">
                                                <i class="fa fa-twitter"></i></a>
                                        @else
                                            <a href="#"
                                               class="btn ls-red-btn btn-round disabled">
                                                <i class="fa fa-twitter"></i></a>
                                        @endif
                                    </li>
                                </ul>
                                <img src="{{ URL::asset($image_user)}}" class="img-circle" alt="example image" data-pin-nopin="true"></div>

                            <div class="menu-text"> {{$user->name_consultant}}
                                <span class="menu-info">
                                <span class="menu-date">{{$user->tel_consultant}}</span>
                                <span class="menu-date">{{$user->email_consultant}}</span>
                                <div class="menu-action">

                                    <ul class="menu_user pd-0">
                                        <li><a href="javascript:void(0)"
                                               id="a_form_show_{{$user->id_consultant}}"
                                               class="btn ls-orange-btn btn-round  btn-xl a_form_show">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </li>
                                        @if($user->blocked == 1)
                                            <li>
                                                <a href="{{URL::to('consultant/approve',[$user->id_consultant])}}"
                                                   title="Approve user" class="btn btn-round btn-xl ls-light-blue-btn">
                                                    <i class="fa fa-check"></i>
                                                </a></li>
                                        @else
                                            <li>
                                                <a href="{{URL::to('consultant/block', [$user->id_consultant])}}"
                                                   class="btn ls-red-btn btn-round  btn-xl delete_user ">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </li>
                                        @endif

                                    </ul>
                                </div>
                            </span>
                            </div>
                        </div>
                        <!-- User Edit Form Start -->
                        <div class="row edit_form_hidden" id="edit_form_hidden_{{$user->id_consultant;}}" style="display: none; margin-top: 15px;">

                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-8">

                                {{ Form::model($user, ['method'=>'PATCH', 'route'=>['consultant.update',$user->id_consultant],'enctype'=>"multipart/form-data"])}}

                                <div class="form-group">
                                    <input id="file-user{{ $user->id_consultant }}" type="file" name="logo_user" multiple="false">
                                </div>

                                <div class="form-group  has-feedback">
                                    {{ Form::text("name_consultant",$user->name_consultant,[ "placeholder"=>trans('messages.label_full_name'), "class"=>"form-control",'id'=>'first_name', "required"]) }}
                                    {{ $errors->first("name_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>

                                <div class="form-group  has-feedback">
                                    {{ Form::email("email_consultant",$user->email_consultant,["placeholder"=>trans('messages.label_email'), "class"=>"form-control", 'id'=>'email',"readonly"]) }}
                                    {{ $errors->first("email_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                    {{ Form::text("tel_consultant",$user->tel_consultant ,["placeholder"=>trans('messages.label_telephone'), "class"=>"form-control",
                                    'id'=>'tel_consultant']) }}
                                    {{ $errors->first("tel_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_xing",$user->social_xing ,["placeholder"=>"Xing Account", "class"=>"form-control",
                                    'id'=>'social_xing']) }}
                                    {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-xing form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_linkedin",$user->social_linkedin ,["placeholder"=>"LinkedIn Account", "class"=>"form-control",
                                    'id'=>'social_twitter']) }}
                                    {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-linkedin
                                                            form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_twitter",$user->social_twitter ,["placeholder"=>"Twitter Account", "class"=>"form-control",
                                    'id'=>'social_twitter']) }}
                                    {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-twitter form-control-feedback"></i>
                                </div>

                                {{Form::hidden('redirect_link','consultant')}}

                                <div class="row">
                                    <div class="col-xs-6" style="padding: 0;">
                                        {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block  ls-light-blue-btn"])}}
                                    </div>
                                    <div class="col-xs-6" style="padding: 0 0 0 4px;">
                                        <button type="button" class="btn btn-danger btn-block a_form_close" id="a_form_close_{{$user->id_consultant}}">{{trans('messages.act_close')}}</button>
                                    </div>
                                </div>

                                {{ Form::close(); }}
                                        <!-- /.col -->
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                        <!-- User Edit Form End -->


                    </div>
                @endforeach
                <div class=" col-lg-3 col-xs-4  friends-tab consultant_list">
                    <div class="row">


                        <div class=" col-xs-12">
                            <div class=" edit_form_hidden" id="edit_form_hidden_0" style="display:  none; margin-top: 15px;">

                                {{ Form::open(['action'=>'ConsultantController@store','id'=>'','enctype'=>"multipart/form-data"]) }}

                                <div class="form-group">
                                    <input id="file-user" type="file" name="logo_user" multiple="false">
                                </div>


                                <div class="form-group  has-feedback">
                                    {{ Form::text("name_consultant",'',[ "placeholder"=>trans('messages.label_full_name'), "class"=>"form-control",'id'=>'name_consultant', "required"]) }}
                                    {{ $errors->first("name_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>

                                <div class="form-group  has-feedback">
                                    {{ Form::email("email_consultant",'',["placeholder"=>trans('messages.label_email'), "class"=>"form-control", 'id'=>'email_consultant',"required"]) }}
                                    {{ $errors->first("email_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>


                                <div class="form-group has-feedback">
                                    {{ Form::text("tel_consultant",'' ,["placeholder"=>trans('messages.label_telephone'), "class"=>"form-control",         'id'=>'tel_consultant']) }}
                                    {{ $errors->first("tel_consultant",'<div class="text-red">:message</div>') }}
                                    <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_xing",'' ,[
                                    "placeholder"=>"Xing Account", "class"=>"form-control",
                                    'id'=>'social_xing']) }}
                                    {{ $errors->first("social_xing",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-xing
                                                            form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_linkedin",'',[
                                    "placeholder"=>"LinkedIn Account", "class"=>"form-control",
                                    'id'=>'social_twitter']) }}
                                    {{ $errors->first("social_linkedin",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-linkedin
                                                            form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    {{ Form::text("social_twitter",'',[
                                    "placeholder"=>"Twitter Account", "class"=>"form-control",
                                    'id'=>'social_twitter']) }}
                                    {{ $errors->first("social_twitter",'<div class="text-red">:message</div>') }}
                                    <i class="fa fa-twitter
                                                            form-control-feedback"></i>
                                </div>

                                {{Form::hidden('redirect_link','consultant')}}

                                <div class="row">
                                    <div class="col-xs-6" style="padding: 0;">
                                        {{Form::submit(trans('messages.act_save'),['class'=>"btn btn-block  ls-light-blue-btn"])}}
                                    </div>
                                    <div class="col-xs-6" style="padding: 0 0 0 4px;">
                                        <button type="button" class="btn btn-danger btn-block a_form_close" id="a_form_close_0">{{trans('messages.act_close')}}</button>

                                    </div>
                                </div>

                                {{ Form::close(); }}
                                        <!-- /.col -->


                            </div>
                        </div>
                    </div>


                    <a href="javascript:void(0)" id="a_form_show_0" class="btn static_add_button ls-light-blue-btn a_form_show  btn-round btn-xxl">
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="title_static_add_button" id="div_a_form_show_0">
                        {{trans('messages.act_create')}}   {{trans('messages.l_new')}} {{trans('messages.nav_consultant')}}
                    </div>

                </div>
            </div>
        </div>

    </div>



@stop
