<div class="page" id="cover-page">
    <div class="company-detail">
        <div class="col-grey">
            <h1>{{ $company->name_company }}</h1>


        </div>
        <div class="company-image">
            <?php $image = Company::getLogoCompany($company->id_company) ?>
            <img src="{{ URL::asset($image)}}" alt="image">
            </a>
        </div>
        <div class="col-dark-grey">

        </div>
    </div>
    <div class="project-details">
        <h2>{{$project->name_project}}</h2>

        <p>Date: {{date('d.m.Y')}}</p>

        <p>Author: {{$user->first_name}} {{$user->last_name}}</p>

        <p>Version: 0.0.1</p>
    </div>
</div>
<div class="page" id="project-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3>{{$project->name_project}}</h3>
        </div>
        <div class="logo-image ">
            <img src="{{ URL::asset($image)}}" alt="image">
        </div>
        <div class=" logo-col-grey"></div>
    </div>
    <div class="industry-text">
        Ihre ausgew�hlte Branchenausrichtung
    </div>
    <div class="industry-type">
        {{$industry_type->name_industry_type}}
    </div>
    <div class="industry-name">
        {{$industry->name_industry}}
    </div>

    <div class="industry-text">
        Termine und Projektbudget.
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Employees number</h3>

        <div class="project_employees_circle">
            {{$project->nr_employees}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Users number <br> &nbsp;</h3>

        <div class="project_employees_circle">
            {{$project->nr_users}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Max Budget (mii �)</h3>

        <div class="project_employees_circle red">
            {{$project->max_budget}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Project start <br> &nbsp;</h3>

        <div class="project_employees_circle dark_grey">
            {{$project->start_project}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Implementation time</h3>

        <div class="project_employees_circle dark_grey">
            {{$project->implement_time}}
        </div>
    </div>

    <div class="industry-text">
        Beteiligte Filialen f�r die Implementierung
    </div>
    @foreach($headquarters as $headquarter)
        <?php
        $headquarters_users = User::getUsersByHeadquarter($headquarter->id_headquarter);
        ?>
        <div class="div_headquarters">
            <div class="headquarter_image">
                <img class="img-circle" alt=""
                     src="{{ URL::asset('images/headquarter.png')}}">
            </div>
            <div class="headquarter_details">
                <h3 class="headquarter_head"> {{$headquarter->name_headquarter}}</h3>
                {{$headquarter->street}} {{$headquarter->street_nr}}<br>
                {{$headquarter->city}}<br>
                {{$headquarter->postal_code}}<br>
                {{$headquarter->country}}<br>
            </div>
            <div class="headquarter_users">
                @foreach($headquarters_users as $user)
                    <div class="headquarter_users_details">
                        <?php $image_user = User::getLogoUser($user->id);
                        ?>
                        <div class="users_image">
                            <img class="img-circle" alt="users pic"
                                 src="{{ URL::asset($image_user)}}">
                        </div>
                        <div class="users_details">
                            <h3 class="headquarter_head">{{$user->first_name.' '.$user->last_name}}</h3>
                            {{$user->job_user}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    @endforeach
</div>
<div class="page" id="manu-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3>Inhaltsverzeichnis</h3>
        </div>
        <div class="logo-image ">
            <img src="{{ URL::asset($image)}}" alt="image">
        </div>
        <div class=" logo-col-grey"></div>
    </div>


    <ol class="menu_index">
        @foreach($module_structure as $module)
            @if(Module_structure::hasChild($module->id_module_structure) == 1)
                <li>
                    {{$module->id_name}}
                    <ol>
                        <?php  $module_structure_2 = Module_structure::getModuleByLvl($industry->id_industry, 2, $module->id_module_structure, $project->id_project);
                        ?>
                        @foreach($module_structure_2 as $module_2)
                            @if(Module_structure::hasChild($module_2->id_module_structure) == 1)
                                <li>
                                    {{$module_2->id_name}}
                                    <ol>
                                        <?php  $module_structure_3 = Module_structure::getModuleByLvl($industry->id_industry, 3, $module_2->id_module_structure, $project->id_project);
                                        ?>
                                        @foreach($module_structure_3 as $module_3)
                                            @if(Module_structure::hasChild($module_3->id_module_structure) == 1)
                                                <li>
                                                    {{$module_3->id_name}}
                                                    <ol>
                                                        <?php  $module_structure_4 = Module_structure::getModuleByLvl($industry->id_industry, 4, $module_3->id_module_structure, $project->id_project);
                                                        ?>
                                                        @foreach($module_structure_4 as $module_4)
                                                            @if(Module_structure::hasChild($module_4->id_module_structure) == 1)
                                                                <li>
                                                                    {{$module_4->id_name}}
                                                                </li>
                                                            @else
                                                                <li>

                                                                    <a href="#{{$module_4->id_module_structure}}">{{$module_4->id_name}}</a>
                                                                </li>
                                                            @endif
                                                        @endforeach

                                                    </ol>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="#{{$module_3->id_module_structure}}">{{$module_3->id_name}}</a>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ol>
                                </li>
                            @else
                                <li>
                                    <a href="#{{$module_2->id_module_structure}}">{{$module_2->id_name}}</a>
                                </li>
                            @endif
                        @endforeach

                    </ol>
                </li>
            @else
                <li>
                    <a href="#{{$module->id_module_structure}}">{{$module->id_name}}</a>
                </li>
            @endif

        @endforeach
    </ol>
</div>