<button type="button"
        class="close close_modal"
        data-dismiss="modal"
        aria-hidden="true">
    x
</button>
<div class="modal-header task-label-white">
    <h3 class="box-title">{{ $post->getAttribute('name_post')}} </h3>

    <div class="clear"></div>
</div>


<div class="modal-body">
    <div class="row">


        <div class="col-md-12 pd-10">
            {{ $post->getAttribute('content_post')}}
        </div>
    </div>

    <br>
</div>
 