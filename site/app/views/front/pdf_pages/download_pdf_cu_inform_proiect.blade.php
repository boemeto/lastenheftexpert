<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ URL::asset('assets/css/pdf/global.css') }}" rel="stylesheet">
    <style type="text/css">
        
    </style>

</head>

<body>
<?php $image = Company::getLogoCompany($company->id_company) ?>
<div class="page" id="cover-page">
    <div class="company-detail">
        <div class="col-grey">
            <h1>{{ $company->name_company }}</h1>
        </div>
        <div class="company-image">
            <?php $image = Company::getLogoCompany($company->id_company) ?>
            <img src="{{ URL::asset($image)}}" alt="image">
            </a>
        </div>
        <div class="col-dark-grey">

        </div>
    </div>
    <div class="project-details">
        <h2 class="title">{{$project->name_project}}</h2>
        <p class="subtitle">Date: {{date('d.m.Y', strtotime($project->created_at))}}</p>
    </div>
    <div class="project-details-footer">
        <p class="subtitle text-left">Author: {{$user->first_name}} {{$user->last_name}}</p>
        <p class="subtitle text-left">Version: 0.0.1</p>
    </div>
</div>
<div class="page" id="project-page">
    <div class="right-logo">
        <div class="logo-image ">
            <img src="{{ URL::asset($image)}}" alt="image">
        </div>
        <div class=" logo-col-grey"></div>
    </div>
    <div class="industry-text">
        Ihre ausgewählte Branchenausrichtung
    </div>
    <div class="industry-type">
        {{$industry_type->name_industry_type}}
    </div>
    <div class="industry-name">
        {{$industry->name_industry}}
    </div>

    <div class="industry-text">
        Termine und Projektbudget.
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Employees number</h3>

        <div class="project_employees_circle">
            {{$project->nr_employees}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Users number <br> &nbsp;</h3>

        <div class="project_employees_circle">
            {{$project->nr_users}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Max Budget (mii €)<br>&nbsp;</h3>

        <div class="project_employees_circle red">
            {{$project->max_budget}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Project start <br> &nbsp;</h3>

        <div class="project_employees_circle dark_grey">
            {{$project->start_project}}
        </div>
    </div>
    <div class="project_employees_box">
        <h3 class="ls-header">Implementation time</h3>

        <div class="project_employees_circle dark_grey">
            {{$project->implement_time}}
        </div>
    </div>

    <div class="industry-text">
        Beteiligte Filialen für die Implementierung
    </div>
    @foreach($headquarters as $headquarter)
        <?php
        $nr = 1;
        $headquarters_users = User::getUsersByHeadquarter($headquarter->id_headquarter);
        ?>
        <div class="div_headquarters">
            <div class="row">
                <div class="col-xs-4">
                    <div class="headquarter_image">
                        <img class="img-circle" alt=""
                             src="{{ URL::asset('images/headquarter.png')}}">
                    </div>
                    <div class="headquarter_details">
                        <h3 class="headquarter_head"> {{$headquarter->name_headquarter}}</h3>
                        {{$headquarter->street}} {{$headquarter->street_nr}}<br>
                        {{$headquarter->city}}<br>
                        {{$headquarter->postal_code}}<br>
                        {{$headquarter->country}}<br>
                    </div>
                </div>
                @foreach($headquarters_users as $user)
                    <?php
                    if ($nr % 3 == 0) {
                        print '<br>';
                    }
                    $nr++;
                    ?>
                    <div class="col-xs-4">
                        <div class="headquarter_users_details">
                            <?php $image_user = User::getLogoUser($user->id);
                            ?>
                            <div class="users_image">
                                <img class="img-circle" alt="users pic"
                                     src="{{ URL::asset($image_user)}}">
                            </div>
                            <div class="users_details">
                                <h3 class="headquarter_head">{{$user->first_name.' '.$user->last_name}}</h3>
                                {{$user->job_user}}
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>


        </div>

    @endforeach
</div>
<div class="page" id="manu-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3>Inhaltsverzeichnis</h3>
        </div>
        <div class="logo-image ">
            <img src="{{ URL::asset($image)}}" alt="image">
        </div>
        <div class=" logo-col-grey"></div>
    </div>


    <ol class="menu_index">
        @foreach($software as $soft)
            <?php
            $id_software = $soft->id_software;
            $module_structure = Module_structure::getModuleByIndustryParent($id_industry, $id_software, $id_project);
            ?>
            <li> {{$soft->name_software}}
                <ol>
                    <?php      Module_structure::printTreePDFContents($module_structure, $id_software, $id_industry, 1, $id_project) ?>
                </ol>
            </li>
        @endforeach
    </ol>
</div>
<div class="page" id="tasks-page">
    <div class="right-logo">
        <div class="logo-col-white">
            <h3></h3>
        </div>
        <div class="logo-image ">
            <img src="{{ URL::asset($image)}}" alt="image">
        </div>
        <div class=" logo-col-grey"></div>
    </div>


    <ol class="menu_index">
        @foreach($software as $soft)
            <?php
            $id_software = $soft->id_software;
            $module_structure = Module_structure::getModuleByIndustryParent($id_industry, $id_software, $id_project);
            ?>
            <li> {{$soft->name_software}}
                <ol>
                    <?php  Module_structure::printTreePDFDetails($module_structure, $id_software, $id_industry, 1, $id_project) ?>
                </ol>
            </li>
        @endforeach
    </ol>


</div>
</body>
</html>