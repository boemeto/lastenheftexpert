@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-plus-square-o"></i>

            <h3 class="box-title">{{trans('messages.add')}} {{trans('messages.module')}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            {{ Form::open(['route'=>'module.store'])}}

            <div class="form-group">
                {{ Form::label('name_module','Name: ') }}
                {{ Form::text('name_module', '', ['class'=>"form-control"]) }}
                {{ $errors->first('name_module','<div class="alert alert-danger">:message</div>') }}
            </div>
            <div class="form-group">
                {{ Form::label('description_module','Description: ') }}
                {{ Form::textarea('description_module', '', ['class'=>"form-control"]) }}
                {{ $errors->first('description_module','<div class="alert alert-danger">:message</div>') }}
            </div>


            <div>
                <button type="submit" class="btn ls-light-blue-btn btn-flat ">
                    <i class="fa  fa-plus-square-o"></i> {{trans('messages.add')}}
                </button>

            </div>

            {{ Form::close()}}
        </div>
        <!-- /.box-body -->
    </div>


@stop