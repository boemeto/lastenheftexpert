<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-md-9 col-xs-8">
        <div class="title_line"> {{$pr_module_task->name_task}} </div>
    </div>
    <div class="col-md-3 col-xs-4">
        <div class="ls-button-group float-right">
            <div class="colorInnerTask" id="colorInnerTask{{$pr_module_task->id_ms_task}}" style="background:{{$pr_module_task->task_color}}"></div>
        </div>
        <div class="ls-button-group float-right">
            {{Form::select('set_priority',  $set_priority,   $pr_module_task->task_priority,
              ['class'=>'set_priority','id'=>'set_priority'.$pr_module_task->id_ms_task])}}
        </div>
        <div class="ls-button-group      float-right" style="padding: 3px;">
            <?php if (intval($pr_module_task->fk_pair) > 0) { ?>
            <input type="radio" disabled name="radio_<?php print $module->id_module_structure . '_' . $pr_module_task->fk_pair ?>"
                   class="icheck-modal iradio-project" value="1" <?php print  Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $project->id_project); ?> >
            <?php }else{?>
            {{Form::checkbox('checkbox'.$pr_module_task->id_ms_task, 1, false,['class'=>"icheck-green icheck-modal",    'id'=>'modal_circle'.$pr_module_task->fk_module_structure.' icheck-modal'.$pr_module_task->id_ms_task,
               Ms_task::isTaskCheckedDefaultProject($pr_module_task->id_ms_task, $project->id_project, $project->is_empty_project),
               Ms_task::isTaskCheckedProject($pr_module_task->id_ms_task, $project->id_project)])}}
            <?php }?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 project-body">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_sidebar_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="modal_description_task">
                            {{$pr_module_task->description_task}}
                        </div>
                        <textarea style="width: 100%" class="summernote" id="summernote{{$pr_module_task->id_ms_task}}">
                            {{$pr_module_task->task_description}}
                        </textarea>
                        <div class="modal_description_task user_comment_task"
                             id="user_comment_task{{$pr_module_task->id_ms_task}}">
                            @if(strlen(strip_tags($pr_module_task->task_description))>1)
                                <div class="modal_description_task_content">
                                    {{$pr_module_task->task_description}}
                                </div>
                            @endif
                        </div>
                        <div class="modal_editor_buttons">
                            <a href="javascript:void(0)" id="button_save_task{{$pr_module_task->id_ms_task}}"
                                class="btn btn-round btn-l ls-light-blue-btn button_save_task btn-margin-right" style="display: none">
                                <i class="fa fa-floppy-o"></i>
                            </a>
                            <a href="javascript:void(0)" id="button_add_task{{$pr_module_task->id_ms_task}}"
                               class="btn btn-round btn-l ls-light-blue-btn button_add_task btn-margin-right
                                {{(strlen($pr_module_task->task_description)>0)?'disabled':''}}">
                                <i class="fa fa-plus"></i>
                            </a>
                            <a href="javascript:void(0)" id="button_edit_task{{$pr_module_task->id_ms_task}}"
                                class="btn btn-round btn-l ls-orange-btn button_edit_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0)" id="button_delete_task{{$pr_module_task->id_ms_task}}"
                                class="btn btn-round btn-l btn-danger button_delete_task btn-margin-right {{(strlen($pr_module_task->task_description)>0)?'':'disabled'}}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2">{{trans('messages.label_sidebar_attachments')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        {{Form::open(['action'=>'ProjectsController@upload_file', 'class'=>'dropzone dz-clickable','id'=>'dropzoneForm'.$pr_module_task->id_ms_task,         'enctype'=>"multipart/form-data"]) }}
                        {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                        {{Form::hidden('id_project',   $project->id_project,["id"=>"id_project"])}}
                        <div class="dz-message">
                            <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                        </div>
                        {{Form::close()}}
                        @if(count($attachments)>0 || count($attachments_industry)>0)
                            <div class="modal-attachments">
                                <div class="row">
                                    @foreach($attachments as $attachment)
                                        <?php    $icon = Projects_attach::getIconByAttachment($attachment->extension) ?>
                                        <div class="col-xs-3 col-attachments"
                                             id="col-attachments{{$attachment->id_projects_attach}}">
                                            <div style="display: block; width: 100%">
                                                <a href="{{ Url::to('/')}}/images/attach_task/{{$project->id_project}}/{{$attachment->name_attachment}}"
                                                   download class="button_download_file" target="_blank">
                                                    <div class="file_attachments"
                                                         style="background: {{$icon}} "
                                                         id="div_file_{{$attachment->id_projects_attach}}"
                                                         title="{{$attachment->name_attachment }}">
                                                    </div>
                                                </a>
                                                <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment->id_projects_attach}}">
                                                    <a href="javascript:void(0)"
                                                       id="button_delete_file{{$attachment->id_projects_attach}}"
                                                       class="button_delete_file">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                    @foreach($attachments_industry  as $attachment)
                                        <?php    $icon = Industry_attach::getIconByAttachment($attachment->extension) ?>
                                        <div class="col-xs-3 col-attachments" id="col-attachments1_{{$attachment->id_industry_attachment}}">
                                            <div style="display: block; width: 100%">
                                                <a href="{{ Url::to('/')}}/images/attach_industry/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                   download class="button_download_file" target="_blank">
                                                    <div class="file_attachments"
                                                         style="background: {{$icon}} "
                                                         id="div_file_1_{{$attachment->id_industry_attachment}}"
                                                         title="{{$attachment->name_attachment }}">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="attachments_edit_buttons" id="attachments_edit_buttons1_{{$attachment->id_industry_attachment}}">
                                                <a href="{{ Url::to('/')}}/images/attach_industry/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                   class="button_download_file" target="_blank">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                {{-- <input id="ac-3" name="accordion-1" type="checkbox" checked="checked"> --}}
                {{-- <label for="ac-3">{{trans('messages.label_sidebar_switch_buttons')}}</label> --}}
                <article class="ac-any">
                    <div class="article_content">
                        <div class="modal-switch-area">
                            <div class="modal-switch-box">
                                {{-- <input name="is_not_offer" class="switchCheckBox is_not_offer" {{($pr_module_task->is_not_offer ==1)?'checked':''}}
                                id="is_not_offer{{$pr_module_task->id_ms_task}}" type="checkbox" data-size="mini"> --}}
                                {{-- {{trans('messages.label_is_not_offer')}} --}}
                            </div>
                            <div class="modal-switch-box">
                                {{-- <input name="is_control" {{($pr_module_task->is_control ==1)?'checked':''}}   class="switchCheckBox is_control" id="is_control{{$pr_module_task->id_ms_task}}" type="checkbox" data-size="mini"> --}}
                                {{-- {{trans('messages.label_is_control')}} --}}
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        <br>
        <a href="javascript:void(0)" class="btn btn-danger btn-block" onclick="hide_modal_box()"> {{trans('messages.act_close')}} </a>
    </div>
</div>
<br>
<br>

<script type="text/javascript">
        $('.button_delete_task').on('click', function(e) {
              e.preventDefault();
              $('.confirm_button_delete_task').removeClass('disabled');
              var id = $(this).attr('id');
              var msg = "Delete details?";
              //var dataForm = $(this).attr('data-form');
             var url = $(this).attr('href');

            $('#formConfirm')
              .find('#frm_body').html(msg)
              .end().modal('show');

             $('#frm_submit').addClass('confirm_button_delete_task').attr('id', id);

            $('.confirm_button_delete_task').click(function (event) {
                    var id = $(this).attr('id');
                    var id_nr = id.replace('button_delete_task', "");
                    jQuery('#summernote' + id_nr).parent().find('.note-editable').html('');
                    $(this).addClass('disabled');
                    var data = {
                        'task_description': '~',
                        'id_ms_task': id_nr,
                        'id_project': '<?php print $project->id_project ?>'
                    };
                    // save to database
                    jQuery.post('{{  route("ajax.update_project_task") }}', data)
                            .done(function (msg) {
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                    // $('#user_comment_task' + id_nr).find('.modal_description_task_content').hide();
                    $('#user_comment_task' + id_nr).find('.modal_description_task_content').html('');
                    jQuery('#summernote' + id_nr).parent().find('.note-editor').hide();
                    $('#button_add_task' + id_nr).removeClass('disabled');
                    $('#button_edit_task' + id_nr).addClass('disabled');
                    $('#button_delete_task' + id_nr).addClass('disabled');
                    $('#button_delete_task' + id_nr).trigger('blur');
                    $('#button_add_task' + id_nr).show();
                    $('#button_edit_task' + id_nr).show();
                    $('#user_comment_task' + id_nr).show();
                    $('#button_save_task' + id_nr).hide();

                    $('#formConfirm').modal('hide');
            });

        });
</script>

<script type="text/javascript">
        $('.button_delete_file').on('click', function(e) {
              e.preventDefault();
              $('.confirm_button_delete_file').removeClass('disabled');
              var data_id = $(this).attr('id');

              //var dataForm = $(this).attr('data-form');
             var url = $(this).attr('href');
             var title = $(this).parent().siblings('a').children('.file_attachments').attr('title');
             var msg = "Delete attachment " + title + "?";

            $('#formConfirm')
              .find('#frm_body').html(msg)
              .end().modal('show');

             $('#frm_submit').addClass('confirm_button_delete_file').attr('data_id', data_id);

            $('.confirm_button_delete_file').click(function (event) {
                    var id = jQuery(this).attr('data_id');
                    var id_nr = id.replace('button_delete_file', '');
                    var data = {
                        'id_project_attach': id_nr
                    };
                    jQuery.post('{{  route("ajax.remove_file_project") }}', data)
                            .done(function (msg) {
                                if (parseInt(msg) > 0) {
                                    var attached_files, new_val, id_ms_task;
                                    id_ms_task = parseInt(msg);
                                    attached_files = $('#attachment_counter' + id_ms_task).html();
                                    new_val = parseInt(attached_files) - 1;
                                    $('#attachment_counter' + id_ms_task).html(new_val);
                                }
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                    jQuery('#col-attachments' + id_nr).hide();
                    $('#formConfirm').modal('hide');
            });
        });
</script>
