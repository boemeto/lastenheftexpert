<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 19-Aug-16
 * Time: 12:54
 */
//'id_package', 'name_package', 'name_package_en', 'description', 'description_en', 'price', 'period', 'period_type', 'is_deleted', 'fk_type_project'
class Package extends Eloquent
{
    public static $rules = [
        'name_package' => 'required',
    ];
    public $primaryKey = 'id_package';
    protected $table = 'packages';
    protected $guarded = ['id_package'];
    protected $fillable = ['name_package', 'color', 'name_package_en', 'description', 'description_en', 'price', 'period', 'period_type', 'is_active', 'fk_type_project'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }


    public static function getFirstById($id)
    {
        $list = self::where('id_package', '=', $id)->first();
        return $list;
    }
    
    public static function getLastPackage()
    {
        $item = self::orderBy('created_at', 'desc')->first();
        return $item;
    }

    public static function getActivePackages($is_new_project = 0)
    {
        if ($is_new_project == 2) {
            $list = self::where('is_new_project', '=', '0')
                ->orderBy('price')
                ->get();
        } elseif ($is_new_project == 1) {
            $list = self::where('is_new_project', '=', '1')
                ->orderBy('price')
                ->get();
        } else {
            $list = self::orderBy('is_new_project')
                ->orderBy('price')
                ->get();
        }

        return $list;
    }

    public static function getPackagesByProject($id_project, $is_main_project = 0)
    {
        if ($is_main_project == 1) {
            $list = Projects_package::where('fk_project', '=', $id_project)
                ->where('is_base_package', '=', 1)
                ->first();
        } else {
            $list = Projects_package::where('fk_project', '=', $id_project)
                ->get();
        }

        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('packages', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('packages', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'de':
                return $this->attributes[$field];
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }
}
