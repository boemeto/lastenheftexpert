{{ $errors->first("name_headquarter[1]","<div class='text-red'>:message</div>") }}
<div class="form-group">
    {{ Form::text("name_headquarter[1]",$headquarter->name_headquarter,[ "placeholder"=>"Name Headquarter", "readonly", "class"=>"form-control company-headquarter-selected" ]) }}
</div>

<div id="headquarter_hide" >
    {{-- <div id="address1"> --}}
        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("autocomplete[1]",'',array( "placeholder"=>"", "class"=>"form-control", "id"=>"autocomplete1","onFocus"=>"initialize('1')")) }} --}}
        {{-- </div> --}}
        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("postal_code[1]",'',[ "placeholder"=>"ZIP", "class"=>"form-control","readonly","id"=>"postal_code1"]) }} --}}
            {{-- {{ $errors->first("postal_code[1]","<div class='text-red'>:message</div>") }} --}}
        {{-- </div> --}}
        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("street[1]",'',[ "placeholder"=>"Street name", "class"=>"form-control","readonly","id"=>"route1"]) }} --}}
            {{-- {{ $errors->first("street[1]","<div class='text-red'>:message</div>") }} --}}
        {{-- </div> --}}

        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("street_number[1]",'',[ "placeholder"=>"Street number","readonly" , "class"=>"form-control","id"=>"street_number1" ]) }} --}}
            {{-- {{ $errors->first("street_number[1]","<div class='text-red'>:message</div>") }} --}}
        {{-- </div> --}}

        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("city[1]",'',[ "placeholder"=>"City", "class"=>"form-control","readonly","id"=>"locality1"]) }} --}}
            {{-- {{ $errors->first("city[1]","<div class='text-red'>:message</div>") }} --}}
        {{-- </div> --}}
        {{-- <div class="form-group"> --}}
            {{-- {{ Form::text("country[1]",'',[ "placeholder"=>"Country", "class"=>"form-control","readonly","id"=>"country1"]) }} --}}
            {{-- {{ $errors->first("country[1]","<div class='text-red'>:message</div>") }} --}}
        {{-- </div> --}}


    {{-- </div> --}}


    {{-- <div class="form-group has-feedback"> --}}
        {{-- {{ Form::text("telephone_headquarter[1]",'',[ "placeholder"=>"Telephone Number","readonly", "class"=>"form-control" ]) }} --}}
        {{-- <span class="glyphicon glyphicon-earphone form-control-feedback"></span> --}}
        {{-- {{ $errors->first("telephone_headquarter[1]","<div class='text-red'>:message</div>") }} --}}
    {{-- </div> --}}

    {{-- <div class="form-group has-feedback"> --}}
        {{-- {{ Form::email("email_headquarter[1]",'',[ "placeholder"=>"Email","readonly", "class"=>"form-control" ]) }} --}}
        {{-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> --}}
        {{-- {{ $errors->first("email_headquarter[1]","<div class='text-red'>:message</div>") }} --}}
    {{-- </div> --}}
</div>
