<script type="text/javascript">

    // error messages and regex strings
    var check_msg = 'Please check this field';
    var empty_msg = "Please fill out this field";
    var string_err = '^[A-Za-z0-9\\s\_\â€“\.\&\,ÃœÃ–Ã„Ã¼Ã¶Ã¤ÃŸÄ‚ÄƒÃŽÃ®ÅžÅŸÅ¢Å£Ã‚Ã¢]+$';
    var string_msg = 'Please use only letters or numbers';
    var email_err = '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
    var email_msg = 'Please provide a correct email';
    var number_err = '^[0-9\â€“\+\.,\\s]+$';
    var number_msg = 'Please use only numbers';
    var date_err = '^(20)*[0-3][0-9][-.][0-1][0-9][.-](20)*[0-3][0-9](\\s[0-9]{1,2}(:[0-9]{1,2}){0,2}){0,1}$';
    var date_msg = 'Please provide a correct date';
    var tel_err = '^[0-9\.\,\+\(\)\\s\-]{6,30}$';
    var tel_msg = 'Please provide a correct phone';

    // form submit
    jQuery('#registerCompanyForm').submit(function (event) {

        jQuery('.text-red').empty();
        validation = true;

        validation *= checkedField('fk_personal_title');
        validation *= validateField('first_name', string_msg, string_err);
        validation *= validateField('last_name', string_msg, string_err);
        validation *= validateField('email', email_msg, email_err);
        validation *= validateField('email_confirm', email_msg, email_err);
        validation *= validateField('password', string_msg, string_err);
        validation *= validateField('password_confirm', string_msg, string_err);
        var val_field_email = jQuery('#email').val();
        var val_field_email_confirm = jQuery('#email_confirm').val();
        var val_field_password = jQuery('#password').val();
        var val_field_password_confirm = jQuery('#password_confirm').val();

        if (val_field_email != val_field_email_confirm) {
            jQuery('#email_confirm').nextAll('div.text-red').remove();
            jQuery('#email_confirm').parent().append('<div class="text-red" id="error_email_confirm">' + 'E-Mail stimmt nicht Ã¼berein' + '</div>'); // insert message
            validation *= false;
        }
        if (val_field_password != val_field_password_confirm) {
            jQuery('#password_confirm').nextAll('div.text-red').remove();
            jQuery('#password_confirm').parent().append('<div class="text-red" id="error_field_password_confirm">' + 'Das Passwort stimmt nicht Ã¼berein' + '</div>'); // insert message
            validation *= false;
        }

        if (validation == false) {
            event.preventDefault(); // prevent submitting
        }
        else {
            return true;
        }
    });
    //    jQuery('#registerUserForm').submit(function (event) {
    //        validation = true;
    //
    //        validation *= checkedField('id_headquarter');
    //        validation *= selectedField('fk_personal_title');
    //        if (validation == false) {
    //            //alert('error');
    //            event.preventDefault(); // prevent submitting
    //        }
    //        else {
    //
    //            return true;
    //        }
    //    });
    jQuery('#affiliateCompanyForm').submit(function (event) {
        validation = true;

        validation *= checkedField('id_company');

        if (validation == false) {
            event.preventDefault(); // prevent submitting
        }
        else {
            return true;
        }
    });

    // validate form on pressing the next button
    jQuery("#button_Next").click(function (event) {
        validation = true;
        var id_company = jQuery('#id_company').val();
        if (id_company == 0) {
            validation *= validateField('name_company', string_msg, string_err);
            validation *= validateField('name_headquarter', string_msg, string_err);
            validation *= validateField('email_company', email_msg, email_err);
            validation *= validateField('postal_code1', string_msg, string_err);
            validation *= validateField('route1', string_msg, string_err);
            validation *= validateField('locality1', string_msg, string_err);
            validation *= validateField('country1', string_msg, string_err);
        }
        else {
            if (jQuery('#id_headquarter').length > 0) {
                var id_headquarter = jQuery('#id_headquarter').val();
                // alert($('#name_headquarter').val());
                if (id_headquarter == 0) {
                    if($('#name_headquarter').val() != '' && $('#autocomplete1').val() != '' && $('#postal_code1') != '' && $('#route1') != '' && $('#name_headquarter').val() != undefined) {
                        validation = true;
                    } else {
                        // jQuery('#errorid_headquarter').html('Please choose a headquarter or complete the headquarter form!');
                        jQuery('.headquarter_not_selected').html('Bitte wÃ¤hlen Sie eine Filiale aus oder tragen Sie eine neue Filiale ein');
                        validation = false;
                    }
                }
                else {
                    validation = true;
                }
            } else {
                validation = true;
            }
        }

        if (validation == false) {
            event.preventDefault(); // prevent submitting
        }
        else {
            jQuery("#step_2").click();
        }
    });

    // used for regex expressions
    function validateField(id, msg, regex) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);

        var string_err = new RegExp(regex);

        // if regex does not mach
        if (string_err.test(val) == false) {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make filed red
            var $myDiv = jQuery('#error' + id);
            // if ($myDiv.length == 0) {
            //     $field.parent().append('<div class="text-red" id="error' + id + '">' + msg + '</div>'); // insert message
            // }
            validation *= false;
        }
        else {

            $field.parent().attr('class', 'form-group has-feedback');// remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // used for empty fields
    function emptyField(id) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);
        // if field empty
        if (val.length == 0) {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make field red
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length == 0) {
                $field.parent().append('<div class="text-red" id="error' + id + '">' + empty_msg + '</div>');// insert message
            }
            validation *= false;
        }
        else {
            $field.parent().attr('class', 'form-group has-feedback'); // remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // used for select fields
    function selectedField(id) {
        validation = true;
        val = jQuery('#' + id).val();
        $field = jQuery('#' + id);
        // if not selected
        if (val == 0 || val == "") {
            $field.parent().attr('class', 'form-group has-feedback has-error'); // make field red
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length == 0) {
                $field.parent().append('<div class="text-red" id="error' + id + '">' + empty_msg + '</div>');// insert message
            }

            validation *= false;
        }
        else {
            $field.parent().attr('class', 'form-group has-feedback');// remove red class
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    // only for radio button
    function checkedField(id) {
        var checkboxes = $("input[type='radio']");
        $field = jQuery('#' + id);
        if (!checkboxes.is(":checked")) {
            var $myDiv = jQuery('#error' + id);
            // if ($myDiv.length == 0) {
            //     $field.parent().parent().append('<div class="text-red" id="error' + id + '">' + check_msg + '</div>');// insert message
            // }
            // validation *= false;
        }
        else {
            var $myDiv = jQuery('#error' + id);
            if ($myDiv.length > 0) {
                $myDiv.remove();// remove message
            }
            validation *= true;
        }
        return validation;
    }

    function validateEmail(el) {
        // console.log(el[0].attributes['data_url_verify']);
        switch (el[0].id) {
            case 'email_company':
                var placeholder = "E-Mail Adresse des Unternehmens*";
                break;
            case 'email_headquarter':
                var placeholder = "E-Mail Adresse*";
                break;
            case 'email':
                var placeholder = "E-Mail Adresse*";
                break;
            default:
                break;
        }

        var url_route = $(el[0]).attr('data_url_verify');
        var msg = "{{trans('messages.email_exists')}}";
        var email = $(el).val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email) == false) {
            $(el).val('');
            $(el).attr("placeholder", "Invalid email").placeholder;
            $('.email-exists').hide();
            $(el).css({"border-color": "red", "border-width": "1px", "border-style": "solid"});
        } else {
            var emailCheck = $(el).val();
            var ajax = $.ajax({
                 async: false,
                 url: url_route,
                 dataType: 'json',
                 method: 'post',
                 data: {
                     'emailCheck' : emailCheck,
                },
            });
            var el_border_color = $(el).css('border-color');
            if (ajax.status == 422) {
                $(el).attr("placeholder", placeholder).placeholder;
                $(el).val('');
                $(el).nextAll('div').remove();
                $("<div class='text-red email-exists' style='margin-bottom: -20px;'>" + msg + "</div>").insertAfter(el);
                $(el).css({'border-color': 'red', 'border-width': '1px', 'border-style': 'solid'});
            } else {
                $(el).nextAll('div').remove();
                $(el).css({'border-color': '' + el_border_color + '', 'border-width': '1px', 'border-style': 'solid'});
            }
        }
    }

    function switchToInvalidTab(form_id)
    {
        var error = false;
        var status = 'Bitte füllen Sie dieses Feld aus!';
        var elements = $(form_id + ' input');
        for(var i = 0; i < elements.length; i++) {
            if(elements[i].required) {
                if($(elements[i]).val().length <= 0 || $(elements[i]).val() === '') {
                    $("html, body").animate({scrollTop: 0}, 300);
                    $(elements[i]).css({"border-color": "red", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                    // $("<div class='text-red' style='margin-bottom: -20px;'>" + status + "</div>").insertAfter(elements[i]);
                    error = true;
                } else {
                    $(elements[i]).css({"border-color": "white", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                }
            }
        }

        if (error) {
            return false;
        }
        return true;
    }

    function checkRequiredFields(form_id)
    {
        var error = false;
        var status = 'Bitte füllen Sie dieses Feld aus!';
        var elements = $(form_id + ' *[required]');

        for(var i = 0; i < elements.length; i++) {
            if(elements[i].required) {
                var el_border_color = $(elements[i]).css('border-color');
                if(!$(elements[i]).val() || $(elements[i]).val().length <= 0 || $(elements[i]).val() === '') {
                    $("html, body").animate({scrollTop: 0}, 100);
                    $(elements[i]).css({"border-color": "red", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                    // $("<div class='text-red' style='margin-bottom: -20px;'>" + status + "</div>").insertAfter(elements[i]);
                    error = true;
                } else {
                    $(elements[i]).css({"border-color": "" + el_border_color + "", "border-width": "1px", "border-style": "solid"});
                    $(elements[i]).nextAll('div').remove();
                }
            }
        }

        if (error) {
            return false;
        }
        return true;
    }

</script>
