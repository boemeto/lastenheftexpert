<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ URL::asset('assets/css/pdf/global.css') }}" rel="stylesheet">
    <style type="text/css">
        
    </style>
</head>
<body>
<?php $image = Company::getLogoCompany($company->id_company) ?>
<div class="page" id="cover-page">
    <div class="company-detail">
        <div class="col-grey">
            <h1 style="padding-top: 20px; font-weight: bold">{{ $company->name_company }}</h1>
        </div>
        <div class="company-image">
            <?php $image = Company::getLogoCompany($company->id_company) ?>
            <img src="{{ URL::asset($image)}}" alt="image">
            </a>
        </div>
        <div class="col-dark-grey">
        </div>
    </div>
    <div class="project-details">
        <h2 class="title">{{$industry->name_industry}}</h2>
        <p class="subtitle">{{trans('messages.label_software_created_at')}}: {{date('d.m.Y', strtotime($industry->created_at))}}</p>
    </div>
    <div class="project-details-footer">
        <p class="subtitle text-left">{{trans('messages.label_date')}}: {{date('d.m.Y')}}</p>
        <p class="subtitle text-left">{{trans('messages.label_author')}}: {{$user->first_name}} {{$user->last_name}}</p>
    </div>
</div>
<div class="page" id="manu-page">
    <ol class="menu_index" style="margin-top: 55px;">
        @foreach($softwares as $soft)
            <?php
                $id_software = $soft->id_software;
                $module_structure = Module_structure::getModuleByIndustryParent($industry->id_industry, $id_software);
            ?>
            <li> {{$soft->name_software}}
                <ol>
                    <?php Module_structure::printIndustryTreePDFContents($module_structure, $id_software, $industry->id_industry, 1) ?>
                </ol>
            </li>
        @endforeach
    </ol>
</div>
<div class="header">
    <table>
        <tr>
            <div class="right-logo">
                <div class="logo-image ">
                    <img src="{{ URL::asset($image)}}" alt="image">
                </div>
                <div class=" logo-col-grey"></div>
            </div>
        </tr>
    </table>
</div>
<div class="page-break-before" id="tasks-page" style="top:2cm;">
    <ol class="menu_index">
        @foreach($softwares as $soft)
            <?php
                $id_software = $soft->id_software;
                $module_structure = Module_structure::getModuleByIndustryParent($industry->id_industry, $id_software);
            ?>
            <li> {{$soft->name_software}}
                <ol>
                    <?php Module_structure::printIndustryTreePDFDetails($module_structure, $id_software, $industry->id_industry, 1) ?>
                </ol>
            </li>
        @endforeach
    </ol>
</div>
</body>
</html>
