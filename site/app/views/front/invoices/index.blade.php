@extends('front.layouts.default')
@section('dropdown-menu')
    @include('front.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('front.layouts.sidebar-menu')
@stop
@section('footer_scripts')
@stop
@section('content')
    <div class="panel widget light-widget panel-bd-top  ">
        <div class="panel-heading "><h3 class="mgtp-10"> {{trans('messages.nav_invoices')}}</h3></div>
        <div class="panel-body table_invoice ">
            <table class="table table-bottomless data_table ">
                <thead>
                <tr>
                    <th>{{trans('messages.label_nr_crt')}}</th>
                    <th>{{trans('messages.label_project_name')}}</th>
                    <th>{{trans('messages.label_date')}}</th>
                    <th class="text-center">{{trans('messages.label_payment_term')}}</th>
                    <th class="text-center">{{trans('messages.label_sum')}}</th>
                    <th class="text-center">{{trans('messages.label_status')}}</th>
                    <th class="text-center">{{trans('messages.label_action')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <tr>
                        <td>{{$invoice->inv_serial}}-{{$invoice->inv_number}}</td>
                        <td>{{Invoice::getProjectName($invoice->id_invoice)}}</td>
                        <td>{{date('d.m.Y',strtotime($invoice->inv_date))}}</td>
                        <td class="text-center">{{date('d.m.Y',strtotime($invoice->inv_date. '+ 14 days'))}}</td>
                        <td class="pull-right" style="padding-right: 25%">
                            <b>{{formatToGermanyPrice($invoice->sum_paid)}}</b>
                            <i class="fa fa-euro"></i>
                        </td>
                        <td class="text-center">
                          <span class="invoice-status label label-{{Invoice::getInvoiceStatusColor($invoice->id_invoice)}}" style="cursor: default;">
                            {{Invoice::getInvoiceStatus($invoice->id_invoice)}}
                          </span>
                        </td>
                        <td class="menu-action text-center">
                            <a href="{{URL::to('invoice/'.$invoice->id_invoice)}}" class="btn btn-round ls-light-blue-btn">
                                <i class="fa fa-eye"></i> </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
