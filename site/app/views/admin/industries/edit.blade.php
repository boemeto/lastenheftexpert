{{ Form::model($industry,['route'=>['industry.update',$industry->id_industry], 'method'=>'PATCH'])}}
<div class="row">
    <div class="col-md-12">
        <a href="javascript:void(0)" onclick="hide_modal_box();">
            <div class="hide_controller delay_hide_controller">
                <i class="fa fa-times"></i>
            </div>
        </a>
        <div class="title_line">   {{trans('messages.nav_industry')}} {{$industry->name_industry}} </div>
    </div>
    <div class="col-md-12 ">
        <section class="ac-container">
            <div>
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="form-group">
                            {{ Form::select("fk_type",$industry_type,$industry->fk_type,[ "class"=>"form-control",  "required"]) }}
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="input-group ls-group-input">
                                    {{ Form::text('name_industry', $industry->getAttribute('name_industry','de'), [
                                        'class'=>"form-control",
                                        'placeholder'=>trans('messages.l_name'),
                                        'required',
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                    <span class="input-group-addon">DE</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::textarea('description_industry',   $industry->getAttribute('description_industry','de'), ['class'=>"summernote",'placeholder'=>trans('messages.l_description')]) }}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="input-group ls-group-input">
                                    {{ Form::text('name_industry_en',  $industry->getAttribute('name_industry','en'), [
                                        'class'=>"form-control",
                                        'placeholder'=>trans('messages.l_name'),
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                    <span class="input-group-addon">EN</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::textarea('description_industry_en',  $industry->getAttribute('description_industry','en'), ['class'=>"summernote",'placeholder'=>trans('messages.l_description')]) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        <section class="ac-container">
            <div>
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2">{{trans('messages.nav_consultants')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="form-group">

                            <select name="fk_consultant[]" multiple class="form-control">
                                @foreach($consultants as $id_consultant => $consultant)
                                    <option value="{{$id_consultant}}" {{ (array_key_exists($id_consultant,$consult_industry)?'selected':'')}} >{{$consultant}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        <br>
        <button type="submit" class='btn ls-light-blue-btn btn-block'>
            {{trans('messages.act_save')}}
        </button>
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>
{{Form::close()}}

<script type="text/javascript">
    $(".delay_hide_controller").parent().hide();
    $(".delay_hide_controller").parent().fadeIn(500);
    $(document).on('click', '.delay_hide_controller', function() {
        $(".delay_hide_controller").parent().hide();
    });
</script>
