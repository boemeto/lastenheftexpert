@section('footer_scripts')
  <script src="{{ URL::asset('assets/js/jquery-ui.js') }}"></script>
@stop

<?php
    $projects_rows = Projects_rows::where('fk_ms_task', '=', $pr_module_task->id_ms_task)->first();
?>

<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-md-12 text-right ">
      <div class="row">
        <div class="col-md-2 pull-right"  style="{{count($projects_rows)==0?'width:13%;':''}}">
          {{Form::checkbox('is_default',1,$pr_module_task->is_default, ['class'=>'switchCheckBox is_default_task inline','data-size'=>"mini", 'data-ms-task-id'=>$pr_module_task->id_ms_task,'id'=>'is_default_task_'.$pr_module_task->id_ms_task])}}
            @if (count($projects_rows)!=0)
               <a href="{{URL::to('delete_task_from_structure/'.$id_ms_task)}}"
                  class="formConfirm btn inline btn-round btn-l btn-block btn-danger"
                  data-form="{{$id_ms_task}}"
                  data-toggle="modal" data-target="#confirmDelete"
                  data-title="Delete Task"
                  data-message="{{trans('validation.confirmation_delete_task')}}">
                   <i class="fa fa-trash"></i>
               </a>
           @endif
        </div>
        <div class="col-md-2 pull-right" style="margin-top: -1px; margin-right: 4px;">
          <i class="fa fa-info-circle" title="{{trans('messages.label_default_task_standard')}}"></i>
        </div>
      </div>
    </div>
    <div class="  col-xs-12 mgtp-10">
        <div class="title_line"> {{ $task->name_task }} </div>
    </div>
</div>

{{ Form::model($pr_module_task,['route'=>['task.update',$pr_module_task->fk_task], 'class'=>'dz-clickable remove-border', 'method'=>'PATCH'])}}
<div class="row">
    <div class="col-md-12">
       <section class="ac-container description-section">
            <div id="task_description">
                <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-2" class="description-label">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-4 border-pd5">
                                {{Form::radio('edit_task',2,true,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_current_task')}}
                            </div>
                            <div class="col-xs-4 col-xs-offset-2 border-pd5">
                                {{Form::radio('edit_task',1,false,['class'=>'icheck-green'])}} {{ trans('messages.l_edit_all_tasks')}}
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_task',  $task->getAttribute('name_task','de'), [
                                        'class'=>"form-control",
                                        "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                        "required",
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <b>Character Counter:</b> {{ strlen($task->getAttribute('description_task','de')) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','de')}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_task_en', $task->getAttribute('name_task','en'), [
                                        'class'=>"form-control",
                                        "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                        'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                        "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <b>Character Counter:</b> {{ strlen($task->getAttribute('description_task','en')) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','en')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <section class="ac-container attachment-section">
                                <div id="task-attachments">
                                    <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                                    <label for="ac-1" class="attachment-label">{{trans('messages.label_sidebar_attachments')}}</label>
                                    <article class="ac-any">
                                        <div class="dropzone article_content border-tb" id="uploadFiles">
                                            {{-- {{dd($ind_ms_task->id_industry_ms)}} --}}
                                            {{Form::hidden('id_industry_ms', $ind_ms_task->id_industry_ms, ["id"=>"id_industry_ms"])}}
                                            {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                            <div class="dz-message row">
                                                <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                                            </div>
                                        </div>
                                    </article>
                                    <article class="ac-any">
                                        <div class="article_content">
                                            {{-- {{Form::open(['action'=>'IndustryController@upload_file_industry', 'class'=>'dropzone dz-clickable','id'=>'dropzoneForm'.$ind_ms_task->id_industry_ms, 'enctype'=>"multipart/form-data"]) }} --}}
                                            {{-- {{Form::hidden('id_industry_ms',$ind_ms_task->id_industry_ms,["id"=>"id_industry_ms"])}} --}}
                                            {{-- <div class="dz-message">
                                                <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                                            </div> --}}
                                            {{-- {{Form::close()}} --}}
                                            @if(count($attachments)>0)
                                                <div class="modal-attachments">
                                                    <div class="row">
                                                        @foreach($attachments as $attachment)
                                                            <?php
                                                            // dd($attachment);
                                                            $icon = Industry_attach::getIconByAttachment($attachment['extension']) ?>
                                                            <div class="col-xs-3 col-attachments"
                                                                 id="col-attachments{{$attachment['id_industry_attachment']}}">
                                                                <div style="display: block; width: 100%">
                                                                    <a href="{{ Url::to('/')}}/images/attach_industry/{{$attachment['folder_attachment']}}/{{$attachment['name_attachment']}}"
                                                                       download class="button_download_file" target="_blank">
                                                                        <div class="file_attachments"
                                                                             style="background: {{$icon}} "
                                                                             id="div_file_{{$attachment['id_industry_attachment']}}"
                                                                             title="{{$attachment['name_attachment'] }}">
                                                                        </div>
                                                                    </a>
                                                                    <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment['id_industry_attachment']}}">
                                                                        <a href="javascript:void(0)"
                                                                           id="button_delete_attachment{{$attachment['id_industry_attachment']}}"
                                                                           class="button_delete_attachment">
                                                                            <i class="fa fa-trash-o"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </article>
                                </div>
                            </section>
                        </div>
                        <div class="form-group mgtp-10">
                            {{ Form::text('code_task',  $task->code_task, ['class'=>"form-control","placeholder"=> trans('messages.nav_task_code') ]) }}
                        </div>
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-3 border-pd5">
                                {{Form::checkbox('is_checked' ,1, $pr_module_task->is_checked,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                            </div>
                            <div class="col-xs-3 col-xs-offset-3 border-pd5">
                                {{Form::checkbox('is_mandatory' ,1, $pr_module_task->is_mandatory,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}} {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="lvl_priority" id="task_pair" class="task-priority-pair">
                                    <option value="">{{trans('messages.label_priority')}}</option>
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->lvl_priority}}" {{($pr_module_task->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                    $selected_pair = Task_pair::getFirstById($pr_module_task->fk_pair);
                                    if (count($selected_pair) == 1) {
                                        $selected_color = $selected_pair->color_pair;
                                    } else {
                                        $selected_color = '#fff';
                                    }
                                ?>
                                <select name="fk_pair" id="task_pair" class="task-priority-pair" style="background: {{$selected_color}}">
                                    <option value="0" style=" background: #fff">{{trans('messages.label_pair')}} {{trans('messages.nav_task')}}</option>
                                    @foreach($pairs as $pair)
                                        <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($pr_module_task->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="check-all">
                            <button type="button" class="check-all-icon" title="Check all" data-message="{{trans('messages.check_all_industries')}}"
                                    data-task-id="{{$id_ms_task}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}">
                                <i class="fa fa-check" aria-hidden="true" ></i>
                            </button>
                            <button type="button" class="check-all-icon-inactive" title="Check all">
                                <i class="fa fa-check" aria-hidden="true" ></i>
                            </button>
                            <button type="button" class="uncheck-all-icon" title="Uncheck all" data-message="{{trans('messages.uncheck_all_industries')}}"
                                    data-task-id="{{$id_ms_task}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}">
                                <i class="fa fa-remove"></i>
                            </button>
                            <button type="button" class="uncheck-all-icon-inactive" title="Uncheck all">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                        <div class="tab-content tab-border select_industry">
                            @foreach($industries_types as $industry_type)
                                <?php
                                $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>
                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">
                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <div class="check-all-type">
                                        @if (count($industries)>0)
                                            <button type="button" class="add-check-all-type" title="Check all"
                                                    data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.check_all_categ_industries')}}"
                                                    data-url="{{action('TaskController@check_admin_all_industry_task')}}"
                                                    data-task-id="{{$id_ms_task}}">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-check-all-type-inactive" title="Check all">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type" title="Uncheck all"
                                                    data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.uncheck_all_categ_industries')}}"
                                                    data-url="{{action('TaskController@check_admin_all_industry_task')}}"
                                                    data-task-id="{{$id_ms_task}}">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type-inactive" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @elseif(count($industries) == 0)
                                            <button type="button" class="inactive-btn" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button type="button" class="inactive-btn" title="Check all">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @endif()
                                    </div>
                                    <hr>
                                    @foreach($industries as $industry)
                                        <div class="industry-container" style="display: block">
                                            <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$id_ms_task}}"
                                                   data-url="{{action('TaskController@check_admin_industry_task')}}"
                                                   {{(Industry_ms::isIndustryChecked($industry->id_industry, $id_ms_task) == true)?'checked':''}}
                                                   name="fk_industry[]" value="{{$industry->id_industry}}" style="display: inline-block"> {{$industry->name_industry}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach

                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$task->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-5" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-5">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$task->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>

{{Form::hidden('id_ms_task' ,$id_ms_task)}}
{{-- {{Form::hidden('fk_industry' ,$fk_industry)}} --}}
<button type="submit" class="btn ls-light-blue-btn btn-block">
    {{trans('messages.act_save')}}
</button>
<button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
    {{trans('messages.act_close')}}
</button>
<br>
{{Form::close()}}

<script type="text/javascript">
    $('.formConfirm').on('click', function(e) {
          e.preventDefault();
          var title = $(this).attr('data-title');
          var msg = $(this).attr('data-message');
          var dataForm = $(this).attr('data-form');
          var url = $(this).attr('href');

        $('#formConfirm')
          .find('#frm_body').html(msg)
          .end().find('#frm_title').html(title)
          .end().modal('show');

          $('#formConfirm').find('#frm_submit').attr('href', url);
    });

    $('.attachment-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) + 50;
            $('#task_description').css('margin-bottom', task_margin_bottom+'px');
            $(this).removeClass('is_closed');
        } else {
            var heightAttachments = $('.attachment-label').parent().find('article:first').height();
            $('#task_description').css('margin-bottom','50px');
            $(this).attr('data-task-margin', heightAttachments);
            $(this).addClass('is_closed');
       }
    });

    $('.description-label').on('click', function() {
        if($(this).hasClass('is_closed')) {
            var task_margin_bottom = parseInt($(this).attr('data-task-margin')) ;
            $('#task_description').css('margin-bottom', task_margin_bottom+'0px');
            $(this).removeClass('is_closed');
            var attachmentHeight = $('.attachment-section').height();
            setTimeout(function() {
                 $('.attachment-section').css('top', $('#task_description').outerHeight());
                    var attachmentHeight = $('.attachment-section').height();
                    $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
        } else {
            $('#task_description').css('margin-bottom','0px');
            $(this).addClass('is_closed');
            setTimeout(function() {
                var topHeight = $('#task_description').outerHeight() + 5;
                $('.attachment-section').css('top', topHeight);
                var attachmentHeight = $('.attachment-section').height();
                $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 30);
       }
    });

    $(document).ready(function() {
        var all_checked_inputs = $(".select_industry input:checked").length;
        $('.select_industry .tab-pane').each(function(){
            var category_checked_inputs = $(this).find('input:checked').length;
            var category_input_containers= $(this).find('.industry-container').length;

            if (category_checked_inputs == 0) {
                $(this).find('.add-check-all-type').show();
                $(this).find('.add-check-all-type-inactive').hide();
                $(this).find('.add-uncheck-all-type').hide();
                $(this).find('.add-uncheck-all-type-inactive').show();
            }

            if (category_checked_inputs > 0 && category_checked_inputs < category_input_containers) {
                $(this).find('.add-check-all-type').show();
                $(this).find('.add-check-all-type-inactive').hide();
                $(this).find('.add-uncheck-all-type').show();
                $(this).find('.add-uncheck-all-type-inactive').hide();
            }

            if(category_checked_inputs == category_input_containers) {
                $(this).find('.add-check-all-type').hide();
                $(this).find('.add-check-all-type-inactive').show();
                $(this).find('.add-uncheck-all-type').show();
                $(this).find('.add-uncheck-all-type-inactive').hide();
            }
        });

        if($(".select_industry input:checked").length == 0) {
            $('.check-all-icon').show();
            $('.check-all-icon-inactive').hide();
            $('.uncheck-all-icon').hide();
            $('.uncheck-all-icon-inactive').show();
        }

        if($(".select_industry input:checked").length == $(".industry-container").length) {
            $('.check-all-icon').hide();
            $('.check-all-icon-inactive').show();
            $('.uncheck-all-icon').show();
            $('.uncheck-all-icon-inactive').hide();
        }

        if($(".select_industry input:checked").length > 0 && $(".select_industry input:checked").length < $(".industry-container").length) {
            $('.check-all-icon').show();
            $('.check-all-icon-inactive').hide();
            $('.uncheck-all-icon').show();
            $('.uncheck-all-icon-inactive').hide();
        }
    });
</script>
