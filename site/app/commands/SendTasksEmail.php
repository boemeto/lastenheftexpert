<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendTasksEmail extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // your update function here
//        $user->email = 'cimpanbogdantest@gmail.com';
//        $user->first_name = 'FirstName';
//        $user->last_name = 'LastName';
//        $notAprovedTasks = Ms_task::where('is_default', '=', 0)->get();
//        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
//        Mail::send('emails.tasks', ['user' => $user, 'data' => $notAprovedTasks], function ($message) use ($user) {
//            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Tasks list');
//        });
        
        Mail::send('emails.tasks', [], function($message)
        {
            $message->to('cimpanbogdantest@gmail.com', 'Test Mail')->subject('Test');
        });
        $this->info('Test has fired.');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [

        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [

        ];
    }

}