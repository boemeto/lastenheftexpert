<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_tasks', function ($table) {
            $table->increments('id_task')->unsigned();
            $table->string('name_task', 250);
            $table->text('notes_task');
            $table->date('due_date');
            $table->string('cost_task', 200);
            $table->integer('fk_creator');
            $table->integer('fk_sprint');
            $table->integer('fk_status');
            $table->integer('fk_priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_tasks');
    }
}