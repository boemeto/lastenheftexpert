@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop


@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-building-o"></i>

            <h3 class="box-title">API </h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body" style="margin: auto; width: 80%;">
            {{$view_name}}
            <?php
            switch ($view_name) {
            case 'loginUser':
            ?>

            {{ Form::open(array('action' => 'ApiController@'.$view_name))}}

            {{ Form::label('name_device','name_device: ') }}
            {{ Form::text('name_device','',["class"=>"form-control"]) }}
            {{ $errors->first('name_device','<div class="alert alert-danger">:message</div>') }} <br>
            {{ Form::label('code_device','code_device: ') }}
            {{ Form::text('code_device','',["class"=>"form-control"]) }}
            {{ $errors->first('code_device','<div class="alert alert-danger">:message</div>') }} <br>
            {{ Form::label('email','email: ') }}
            {{ Form::text('email','',["class"=>"form-control"]) }}
            {{ $errors->first('email','<div class="alert alert-danger">:message</div>') }} <br>
            {{ Form::label('password','password: ') }}
            {{ Form::password('password',["class"=>"form-control"]) }}
            {{ $errors->first('password','<div class="alert alert-danger">:message</div>') }} <br>
            {{ Form::label('gcm_id','gcm_id: ') }}
            {{ Form::text('gcm_id','',["class"=>"form-control"]) }}
            {{ $errors->first('gcm_id','<div class="alert alert-danger">:message</div>') }} <br>
            {{ Form::label('apple_id','apple_id: ') }}
            {{ Form::text('apple_id','',["class"=>"form-control"]) }}
            {{ $errors->first('apple_id','<div class="alert alert-danger">:message</div>') }} <br>

            {{ Form::submit('Send',["class"=>"btn  btn-primary"])}}
            {{ Form::close() }}
            <?php
            break;
            case 'getUser':
            ?>

            {{ Form::open(array('action' => 'ApiController@'.$view_name))}}


            {{ Form::label('id_user','id_user: ') }}
            {{ Form::text('id_user','',["class"=>"form-control"]) }}
            {{ $errors->first('id_user','<div class="alert alert-danger">:message</div>') }} <br>

            {{ Form::submit('Send',["class"=>"btn  btn-primary"])}}
            {{ Form::close() }}
            <?php
            break;
            case 'getCompany':
            ?>

            {{ Form::open(array('action' => 'ApiController@'.$view_name))}}


            {{ Form::label('id_user','id_user: ') }}
            {{ Form::text('id_user','',["class"=>"form-control"]) }}
            {{ $errors->first('id_user','<div class="alert alert-danger">:message</div>') }} <br>

            {{ Form::submit('Send',["class"=>"btn  btn-primary"])}}
            {{ Form::close() }}
            <?php
            break;
            default:
                break;
            }
            ?>
        </div>


    </div>
@stop

