<?php

class Submodule extends Eloquent
{


    public static $rules = [
        'name_submodule' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_submodule';
    protected $table = 'pr_submodules';
    protected $guarded = ['id_submodule'];
    protected $fillable = ['code_submodule', 'name_submodule', 'name_submodule_en', 'description_submodule', 'description_submodule_en', 'meta_tags', 'address_code', 'fk_company', 'is_default'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Submodule::whereId_submodule($id)->first();
        return $list;
    }

    public function setAttribute($field, $value)
    {
        $this->attributes[$field] = $value;
        return;
    }

    public function getAttribute($field, $lang = '')
    {
        if ($lang == '') {
            $lang = Session::get('lang');
        }

        switch ($lang) {
            case 'en':
                if (Schema::hasColumn('pr_submodules', $field . '_en') && $this->attributes[$field . '_en'] != '') {
                    return $this->attributes[$field . '_en'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            case 'ro':
                if (Schema::hasColumn('pr_submodules', $field . '_ro') && $this->attributes[$field . '_ro'] != '') {
                    return $this->attributes[$field . '_ro'];
                } else {
                    return $this->attributes[$field];
                }
                break;
            default:
                return $this->attributes[$field];
                break;
        }


    }

}