<?php
use Illuminate\Support\MessageBag;

class PostsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $posts = Posts::all();
        return View::make('admin.posts.index', compact('posts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        allowOnlySuperAdmin();
        return View::make('admin.posts.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Posts::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(Posts::$messages);
        }
        $post = new Posts();
        $post->fill(Input::all());
        $post->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $post = Posts::getFirstById($id);

        return View::make('admin.posts.show', compact('post'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $post = Posts::getFirstById($id);

        return View::make('admin.posts.edit', compact('post'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (!Posts::isValid(Input::all())) {
            return Redirect::back()->withInput()
                ->withErrors(Posts::$messages);
        }
        $post = Posts::getFirstById($id);
        $post->fill(Input::all());
        $post->save();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $post = Posts::getFirstById($id);

        $post->delete();

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }


}
