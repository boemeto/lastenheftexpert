<?php


use Illuminate\Support\MessageBag;



class PackagesController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        allowOnlySuperAdmin();
        $list = Package::all();
        return View::make('admin.packages.index', compact('list'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        allowOnlySuperAdmin();
        $period_type = Dictionary::getOptions(7, 1);
        $project_type = Dictionary::getOptions(8, 1);
        $package_types = PackageType::all();
        return View::make('admin.packages.create', compact('period_type', 'project_type', 'package_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $subpackages = new Subpackages();
        //echo "<pre>";dd(Input::all());
        $list = new Package();
        $list->fill(Input::all());
        $list->save();

        $lastPackage = Package::getLastPackage();
        //dd($lastPackage);

        $l = count(Input::all()["subpackage_name"]);

        for ($i=1; $i<=$l; $i++) {
            $subpackages = new Subpackages();
            $subpackages->subpackage_name = Input::all()['subpackage_name'][$i];
            $subpackages->subpackage_period = Input::all()['subpackage_period'][$i];
            $subpackages->subpackage_price = Input::all()['subpackage_price'][$i];
            $subpackages->package_id = $lastPackage->id_package;
            $subpackages->subpackage_type_id = $i;
            $subpackages->save();
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_add')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $pack = Package::getFirstById($id);
        $rows = Package_row::getByPackage($id);
        return View::make('admin.packages.show', compact('pack', 'rows'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        allowOnlySuperAdmin();
        $list = Package::getFirstById($id);
        $period_type = Dictionary::getOptions(7, 1);
        $active = Dictionary::getOptions(1, 1);
        $project_type = Dictionary::getOptions(8, 1);

        $subpackages = Subpackages::getSubpackages($list->id_package);
        $package_types = PackageType::all();
        return View::make('admin.packages.edit', compact('list', 'period_type', 'active', 'project_type', 'subpackages', 'package_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $list = Package::getFirstById($id);
        $list->fill(Input::all());
        $list->save();

        $subpackage = Subpackages::getSubpackages($id);
        $l = count(Input::all()["subpackage_name"]);

        if ($subpackage[0] == null) {
            for ($i=1; $i<=$l; $i++) {
                $subpackages = new Subpackages();
                $subpackages->subpackage_name = Input::all()['subpackage_name'][$i];
                $subpackages->subpackage_period = Input::all()['subpackage_period'][$i];
                $subpackages->subpackage_price = Input::all()['subpackage_price'][$i];
                $subpackages->package_id = $id;
                $subpackages->subpackage_type_id = $i;
                $subpackages->save();
            }
        } else {
            for ($i=1, $j=0; $i<=$l; $i++, $j++) {
                $subpackage[$j]->subpackage_name = Input::all()['subpackage_name'][$i];
                $subpackage[$j]->subpackage_period = Input::all()['subpackage_period'][$i];
                $subpackage[$j]->subpackage_price = Input::all()['subpackage_price'][$i];
                $subpackage[$j]->save();
            }
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_edit')]]);
        return Redirect::back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $list = Package::getFirstById($id);
        $list->delete();
        $subpackages = Subpackages::getSubpackages($id);
        foreach($subpackages as $subpackage) {
            $subpack = Subpackages::getFirstByPackageId($subpackage->subpackage_id);
            $subpack->delete();
        }

        $errors = new MessageBag(['confirm_' => [Lang::get('validation.message_success_delete')]]);
        return Redirect::back()->withErrors($errors);
    }
}
