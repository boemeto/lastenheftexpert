<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 14.05.2015
 * Time: 15:56
 */
class Company_industries extends Eloquent
{


    public static $rules = [
        'fk_headquarter' => 'required',
        'fk_industry' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_company_industry';
    protected $table = 'company_industries';
    protected $guarded = ['id_company_industry'];
    protected $fillable = ['fk_headquarter', 'fk_industry'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Company_industries::whereId_company_industry($id)->first();
        return $list;
    }

}