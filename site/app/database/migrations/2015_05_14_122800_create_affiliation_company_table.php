<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliationCompanyTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliation_company', function ($table) {
            $table->increments('id_affiliation_company')->unsigned();
            $table->integer('fk_company')->nullable();
            $table->integer('fk_affiliation')->nullable();
            $table->tinyInteger('is_pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('affiliation_company');
    }

}
