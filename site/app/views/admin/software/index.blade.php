@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('footer_scripts')
    <script type="text/javascript">
        $("#modalAddSoft").on('hidden.bs.modal', function () {
            $("#modalAddSoft").removeData('bs.modal');
        });

        $("#modalAddSoft").on('shown.bs.modal', function () {

        });

        $(document).on('change', "input[name='import_file']", function() {
            this.form.submit();
        });

        if ($('#scrollTo').length != 0) {
            $('html, body').animate({
                scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50
            }, 'slow');
        }
    </script>
    @include('utils/validations_project')
@stop
@section('content')
    <div class="panel widget light-widget panel-bd-top">
        <div class="panel-heading bordered">
            <h3 class="mgtp-10">{{trans('messages.nav_software') }}</h3>
        </div>
        <a href="{{ URL::route('software.create') }}"
           id="a_form_headquarter_0" data-toggle="modal" data-target="#modalAddSoft"
           class="btn static_add_button ls-light-blue-btn a_form_headquarter  btn-round btn-xxl">
            <i class="fa fa-plus"></i>
        </a>
        <div class="title_static_add_button" id="div_a_form_headquarter_0">
            {{trans('messages.nav_software') }}  {{trans('messages.act_create') }}
        </div>
        <!-- /.panel-header -->
        <div class="panel-body ">
            <br>

            @if(count($software) > 0)
                <div class="row">
                    @foreach($software as $row)
                        <div class="col-md-4">
                            <div class="panel widget light-widget mg-10 panel-bd-top ">
                                <div class="panel-heading software_header  bordered">
                                    <div class=" row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <h3 class="panel-title software-title"> {{ $row->name_software  }} </h3>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
                                            <div class="row">
                                                <div style="display: inline-block">
                                                    <form action="{{ action("SoftwareController@importExcel2") }}" method="post" enctype="multipart/form-data">
                                                        <div class="btn btn-round btn-xl btn-info btn-file">
                                                            <i class="fa fa-upload"></i>
                                                            <input type="hidden" name="software_id" value="{{ $row->id_software }}">
                                                            <input type="file" name="import_file" onchange="this.form.submit();">
                                                        </div>
                                                    </form>
                                                </div>
                                                <div style="display: inline-block">
                                                    <a href="{{ URL::route('software.show',[$row->id_software]) }}" id="{{$row->id_software}}"
                                                       class="btn btn-round btn-xl ls-light-green-btn">
                                                        <i class="fa fa-sitemap"></i>
                                                    </a>
                                                </div>
                                                {{--<div style="display: inline-block">--}}
                                                {{--{{ Form::model($row,['route'=>['software.update',$row->id_software], 'method'=>'PATCH'])}}--}}
                                                {{--{{ Form::hidden('deactivate','1') }}--}}
                                                {{--@if($row->is_deleted == 1)--}}
                                                {{--<button type="submit" class="btn btn-round btn-xl ls-light-blue-btn">--}}
                                                {{--<i class="fa fa-check"></i>--}}
                                                {{--</button>--}}
                                                {{--@else--}}
                                                {{--<button type="submit" class="btn btn-round btn-xl btn-danger">--}}
                                                {{--<i class="fa fa-trash"></i>--}}
                                                {{--</button>--}}
                                                {{--@endif--}}
                                                {{--{{ Form::close() }}--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body panel_software">
                                    <div class="software-description">
                                      {{$row->description_software}}
                                    </div>
                                    @if ($row->file_imported!='')
                                      <hr>
                                    @endif
                                    <div class="import-details">
                                      <div class="row">
                                        @if ($row->file_imported!='')
                                          <div class="col-xs-4">
                                            <strong>{{trans('messages.file_name')}}:</strong>
                                          </div>
                                          <div class="col-xs-7 col-xs-offset-1">{{ $row->file_imported  }}</div>
                                        @endif
                                      </div>
                                      <div class="row">
                                        @if ($row->import_date!='0000-00-00 00:00:00')
                                          <div class="col-xs-5">
                                            <strong>{{trans('messages.last_update')}}:</strong>
                                          </div>
                                          <?php
                                              $date = explode(' ', $row->updated_at);
                                              $hours = explode(':',$date[1]);
                                              $hours = '[' . $hours[0] . ':' . $hours[1] . ']';
                                              $date = date('d.m.Y', strtotime($date[0])) . ' ' .  $hours;
                                          ?>
                                          <div class="col-xs-6 software-upload-date">{{$date}}</div>
                                        @endif
                                      </div>
                                      <div class="row">
                                        @if ($row->imported_by!='')
                                          <div class="col-xs-5">
                                            <strong>{{trans('messages.label_author')}}:</strong>
                                          </div>
                                          <div class="col-xs-6">{{ $row->imported_by  }}</div>
                                        @endif
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{$pagination}}
            @endif
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Modal Software -->
    <div class="modal fade" id="modalAddSoft" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Software End -->
    @if(Session::has('scrollTo'))
        <input type="hidden" id="scrollTo" value="{{Session::get('scrollTo')}}">
    @endif
@stop
