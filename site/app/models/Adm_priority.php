<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 02-Nov-15
 * Time: 10:33
 */
class Adm_priority extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_priority' => 'required',
    ];
    public $primaryKey = 'id_priority';
    protected $table = 'adm_priorities';
    protected $guarded = ['id_priority'];
    protected $fillable = ['name_priority', 'desc_priority', 'color_priority'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_priority::where('id_priority', '=', $id)->first();
        return $list;
    }

    public function getAllPriorities()
    {
        $lists = Adm_priority::all();
        $array[0] = 'Priorität';
        foreach ($lists as $list) {
            $array[$list->id_priority] = $list->desc_priority;
        }
        return $array;
    }

}