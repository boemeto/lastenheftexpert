<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:38
 */
class Ind_module extends Eloquent
{


    public static $rules = [
        'fk_module' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_ind_module';
    protected $table = 'pr_ind_modules';
    protected $guarded = ['id_ind_module'];
    protected $fillable = ['fk_industry',
        'fk_module'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Soft_module::whereId_soft_module($id)->first();
        return $list;
    }

    public static function getModuleList($id_industry)
    {
        $array = array();
        $lists = Ind_module::whereFk_industry($id_industry)->get();

        foreach ($lists as $list) {

            $array[$list->id_ind_module] = $list->fk_module;
        }
        return $array;
    }

}