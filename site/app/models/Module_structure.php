<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 10.06.2015
 * Time: 13:37
 */
use Projects_attach;

class Module_structure extends Eloquent
{


    public static $rules = [
        'fk_software' => 'required',
    ];
    public static $messages = [];
    public $primaryKey = 'id_module_structure';
    protected $table = 'pr_module_structure';
    protected $guarded = ['id_module_structure'];
    protected $fillable = ['fk_software', 'fk_id', 'fk_parent', 'id_type', 'level', 'fk_company'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return false;
    }

    public static function getModule_structureByModule($id, $type)
    {
        $list = self::where('fk_id', '=', $id)
            ->where('id_type', '=', $type)
            ->get();
        return $list;
    }

    public static function getModuleStructureOne($id_module_structure)
    {
        $list = DB::table('pr_module_structure')
            ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
            ->leftJoin('pr_modules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                    ->where('pr_module_structure.id_type', '=', '1');
            })
            ->leftJoin('pr_submodules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                    ->where('pr_module_structure.id_type', '=', '2');
            })
            ->where('id_module_structure', '=', $id_module_structure)
            ->first();
        return $list;
    }

    public static function getModuleStructure($fk_software = null, $fk_id = null)
    {

        if ($fk_software == null) {
            $list = DB::table('pr_module_structure')
                ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                });

        } else {
            if ($fk_id == null) {
                $list = DB::table('pr_module_structure')
                    ->selectRaw("pr_module_structure.*, coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                    ->leftJoin('pr_modules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                            ->where('pr_module_structure.id_type', '=', '1');
                    })
                    ->leftJoin('pr_submodules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                            ->where('pr_module_structure.id_type', '=', '2');
                    })
                    ->where('pr_module_structure.fk_software', '=', $fk_software);

            } else {
                $list = DB::table('pr_module_structure')
                    ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                    ->leftJoin('pr_modules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                            ->where('pr_module_structure.id_type', '=', '1');
                    })
                    ->leftJoin('pr_submodules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                            ->where('pr_module_structure.id_type', '=', '2');
                    })
                    ->where('pr_module_structure.fk_software', '=', $fk_software)
                    ->where('pr_module_structure.fk_id', '=', $fk_id);
            }
        }

        return $list;
    }

    public static function getModuleStructureIndustry($fk_industry = null, $fk_id = null, $fk_software = null)
    {
        if ($fk_software == null) {
            $list = DB::table('pr_module_structure')
                ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                });

        } else {
            if ($fk_id == null) {
                $list = DB::table('pr_module_structure')
                    ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                    ->leftJoin('pr_modules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                            ->where('pr_module_structure.id_type', '=', '1');
                    })
                    ->leftJoin('pr_submodules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                            ->where('pr_module_structure.id_type', '=', '2');
                    })
                    ->where('pr_module_structure.fk_software', '=', $fk_software);

            } else {
                $list = DB::table('pr_module_structure')
                    ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                    ->leftJoin('pr_modules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                            ->where('pr_module_structure.id_type', '=', '1');
                    })
                    ->leftJoin('pr_submodules', function ($join) {
                        $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                            ->where('pr_module_structure.id_type', '=', '2');
                    })
                    ->where('pr_module_structure.fk_software', '=', $fk_software)
                    ->where('fk_id.fk_software', '=', $fk_id);
            }
        }

        return $list;

    }

    public static function getModuleList($fk_software)
    {
        $array = array();
        $lists = Module_structure::wherefk_software($fk_software)
            ->whereId_type(1)
            ->get();

        if (count($lists) > 0) {
            foreach ($lists as $list) {
                $array[] = $list->fk_id;
            }
            $modules = Module::whereIn('id_module', $array)->get();
            return $modules;
        } else {
            return array();
        }
    }

    public static function hasPath($fk_software, $id_module, $fk_parent, $level)
    {
        $lists = Module_structure::where('fk_software', '=', $fk_software)
            ->where('fk_id', '=', $id_module)
            ->where('fk_parent', '=', $fk_parent)
            ->where('level', '=', $level)
            ->first();
        return $lists;

    }

    public static function getModuleParents($id_module_structure)
    {
        $array_modules = array();
        $array_modules[] = $id_module_structure;
        $parent = Module_structure::getModuleOneParent($id_module_structure);
        $array_modules[] = $parent;
        while ($parent > 0) {
            $parent = Module_structure::getModuleOneParent($parent);
            if ($parent != 0) {
                $array_modules[] = $parent;
            }
        }
        return array_reverse($array_modules);

    }

    public static function getModuleOneParent($id_module_structure)
    {
        $list = Module_structure::getFirstById($id_module_structure);
        $parent = $list->fk_parent;
        return $parent;

    }

    public static function getFirstById($id)
    {
        $list = Module_structure::whereId_module_structure($id)->first();
        return $list;
    }

    public static function getModuleChildren($id_module_structure)
    {
        $module_structure = Module_structure::getFirstById($id_module_structure);
        $fk_software = $module_structure->fk_software;
        $children = DB::table('pr_module_structure')
            ->selectRaw("pr_module_structure.*,
                coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
            ->leftJoin('pr_modules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                    ->where('pr_module_structure.id_type', '=', '1');
            })
            ->leftJoin('pr_submodules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                    ->where('pr_module_structure.id_type', '=', '2');
            })
            ->where('fk_parent', '=', $id_module_structure)
            ->where('fk_software', '=', $fk_software)
            ->get();
        return $children;
    }

    public static function printTreePDFContentsEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                print   '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                    print '<ol>';
                        self::printTreePDFContentsEmptyProject($module_structure, $fk_software, $id_industry, $lvl, $id_project);
                    print '</ol>';
                print '</li>';
            } else {
                print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a></li>';
            }
        }
    }

    public static function hasChild($id_module_structure)
    {
        $list = Module_structure::whereFk_parent($id_module_structure)->first();
        if (count($list) == 0) {
            $count = 0;
        } else {
            $count = 1;
        }
        return $count;
    }

    public static function getModuleByLvl($fk_software, $level, $fk_parent = 0, $fk_project = NULL)
    {
        if ($fk_project == NULL) {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.id_module_structure, pr_module_structure.is_default ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                ->orderBy('ms_order', 'asc')->get();
        } else {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_projects_ms_order', function ($join) use ($fk_project) {
                    $join->on('pr_projects_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_projects_ms_order.fk_project', '=', $fk_project);
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                ->where(function ($query) use ($fk_project) {
                    $query->where('pr_module_structure.is_default', '=', 0)
                        ->where('pr_module_structure.fk_project', '=', $fk_project);
                })
                ->orderBy('ms_proj_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        }
        return $list;
    }

    public static function getModuleByModuleId($module_id)
    {
        $list = DB::table('pr_module_structure')
            ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
            ->leftJoin('pr_modules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                    ->where('pr_module_structure.id_type', '=', '1');
            })
            ->leftJoin('pr_submodules', function ($join) {
                $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                    ->where('pr_module_structure.id_type', '=', '2');
            })
//            ->where('fk_parent', '=', $module_id)
            ->orWhere('id_module_structure', '=', $module_id)
            ->orderBy('ms_order', 'asc')->get();

        return $list;
    }

    public static function getModuleByLvlWithDefault($fk_software, $level, $fk_parent = 0, $fk_project = NULL)
    {
        if ($fk_project == NULL) {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                ->orderBy('ms_order', 'asc')->get();
        } else {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_projects_ms_order', function ($join) use ($fk_project) {
                    $join->on('pr_projects_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_projects_ms_order.fk_project', '=', $fk_project);
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                ->where(function ($query) use ($fk_project) {
                    $query->where('pr_module_structure.fk_project', '=', $fk_project);
                })
                ->orderBy('ms_proj_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        }
        return $list;
    }

    public static function printTreePDFContents($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                    print   '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                    print '<ol>';
                    self::printTreePDFContents($module_structure, $fk_software, $id_industry, $lvl, $id_project);
                    print '</ol>';
                    print '</li>';
                }
            } else {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    print   '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                    print '</li>';
                }
            }
        }
    }

    public static function printTreePDFContentWithTasks($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project)
    {
        $lvl++;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        print   '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                            print '<ol>';
                                self::printTreePDFContentWithTasks($module_structure, $fk_software, $id_industry, $lvl, $id_project);
                            print '</ol>';
                        print '</li>';
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            print '<li class="menu_title"> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                                print  '<ol>';
                                foreach ($pr_module_tasks as $pr_module_task) {
                                    print'<li>' . $pr_module_task->name_task . ' </li>';
                                }
                                print  '</ol>';
                            print'</li>';
                        }
                    }
                }
            }
        }
    }

    public static function printIndustryTreePDFContents($module_structure, $fk_software, $id_industry, $lvl = 1)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if(Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                    print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                        print '<ol>';
                            self::printIndustryTreePDFContents($module_structure, $fk_software, $id_industry, $lvl);
                        print '</ol>';
                    print '</li>';
                }
            } else {
                if(Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a></li>';
                }
            }
        }
    }

    public static function printIndustryTreePDFDetails($module_structure, $fk_software, $id_industry, $lvl = 1)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if(Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                    print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                        print '<ol>';
                            self::printIndustryTreePDFDetails($module_structure, $fk_software, $id_industry, $lvl);
                        print '</ol>';
                    print '</li>';
                }
            }
            else {
                if(Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $id_industry, 0, 0);
                    if (count($pr_module_tasks) > 0) {
                        print '<li class="menu_title"> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                            print '<ol>';
                            foreach ($pr_module_tasks as $pr_module_task) {
                                if($pr_module_task->is_default == 1) {
                                    $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                                    $attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                    print '<li>' . $pr_module_task->name_task . ' </li>';
                                    print '<div class="level1">';
                                        if(count($attachments) > 0) {
                                            print trans('messages.label_attachments') . ':';
                                            print '<div class="attachements">';
                                            foreach($attachments as $key=>$attachment) {
                                                $key++;
                                                print '<p>';
                                                print $key .'. '.$attachment->name_attachment;
                                                print '</p>';
                                            }
                                            print '</div>';
                                            print '<br/>';
                                        }
                                        print '<div class="description_task">' . nl2br($pr_module_task->description_task) . '</div>';
                                    print '</div>';
                                }
                            }
                            print '</ol>';
                        print'</li>';
                    }
                }
            }
        }
    }

    public static function printSoftwareTreePDFContents($module_structure, $fk_software, $lvl = 1)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
                    print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                        print '<ol>';
                            self::printSoftwareTreePDFContents($module_structure, $fk_software, $lvl);
                        print '</ol>';
                    print '</li>';
                }
            } else {
                if ($module->is_default == 1) {
                    print '<li><a href="#' . $module->id_module_structure . '">' . $module->id_name . '</a>';
                    print '</li>';
                }
            }
        }
    }

    public static function printSoftwareTreePDFDetails($module_structure, $fk_software, $lvl = 1, $id_industry = null)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, null);
                    print '<li> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                        print '<ol>';
                            self::printSoftwareTreePDFDetails($module_structure, $fk_software, $lvl, $id_industry);
                        print '</ol>';
                    print '</li>';
                }
            } else {
                if($module->is_default == 1) {
                    if ($id_industry) {
                        $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $id_industry);
                    } else {
                        $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure);
                    }
                    if (count($pr_module_tasks) > 0) {
                        print '<li class="menu_title"><a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                            print  '<ol>';
                            foreach ($pr_module_tasks as $pr_module_task) {
                                if($pr_module_task->ms_tasks_default == 1) {
                                    if ($id_industry) {
                                        $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                                        $attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                    } else {
                                        $attachments = Software_attach::getAttachmentsNamesByMS_task($pr_module_task->id_ms_task);
                                    }

                                    print '<li>' . $pr_module_task->name_task . '</li>';

                                    print '<div class="level1">';

                                        if (nl2br($pr_module_task->description_task)) {
                                            print '<div class="description_task">' . nl2br($pr_module_task->description_task) . '</div>';
                                            print '<br/>';
                                        }

                                        if($pr_module_task->lvl_priority) {
                                            print "<div> Gewichtung: " . $pr_module_task->lvl_priority . " </div>";
                                            print '<br/>';
                                        }

                                        if(count($attachments) > 0) {
                                            print trans('messages.label_attachments') . ':';
                                            print '<div class="attachements">';
                                            foreach($attachments as $key=>$attachment) {
                                                $key++;
                                                print '<p>';
                                                print $key .'. '.$attachment->name_attachment;
                                                print '</p>';
                                            }
                                            print '</div>';
                                            print '<br/>';
                                        }
                                    print '</div>';
                                }
                            }
                            print '</ol>';
                        print'</li>';
                    }
                }
            }
        }
    }

    public static function printSoftwareTreeWordContents($module_structure, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, null);
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        $actualKeys = explode('.', $newKey);
                        print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">'. trim($newKey, '.'). ($newKey ?'.': '') .$key.'. '. utf8_decode($module->id_name) . '</a>';
                            print '<p>';
                            if(empty($actualKeys))
                                $actualKeys = [];
                            array_push($actualKeys, $key);
                            // Session::forget('word_keys');
                            Session::push('word_keys', $key);
                            self::printSoftwareTreeWordContentsChieldrens($module_structure, $fk_software, $lvl, $actualKeys);
                            print '</p>';
                        print '</p>';
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') .$key.'. '. utf8_decode($module->id_name) . '</a></p>';
                        Session::forget('word_keys');
                        // Session::push('word_keys', $key);
                    }
                }
            }
        }
    }

    public static function printSoftwareTreeWordContentsChieldrens($module_structure, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        $newKey = implode('.',$keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        $keys = explode('.', $newKey);
                        print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') . $key . '. ' . utf8_decode($module->id_name) . '</a>';
                            print '<p>';
                                if(empty($keys))
                                    $keys = [];
                                array_push($keys, $key);
                                // Session::forget('word_keys');
                                Session::push('word_keys', $key);
                                self::printSoftwareTreeWordContents($module_structure, $fk_software, $lvl, $keys);
                            print '</p>';
                        print '</p>';
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.').($newKey ?'.': '').$key.'. '. utf8_decode($module->id_name) . '</a></p>';
                        Session::forget('word_keys');
                        // Session::push('word_keys', $newKey);
                    }
                }
            }
        }
    }

    public static function printSoftwareTreeWordDetails($module_structure, $fk_software, $lvl = 1, $keys, $id_industry = 0)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, null);
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        $actualKeys = explode('.', $newKey);
                        print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">'. trim($newKey, '.'). ($newKey ?'.': '') .$key.'. '. utf8_decode($module->id_name) . '</a></b>';
                            print '<p>';
                                if(empty($actualKeys))
                                    $actualKeys = [];
                                array_push($actualKeys, $key);
                                Session::forget('word_keys');
                                Session::push('word_keys', $key);
                                self::printSoftwareTreeWordDetailssChieldrens($module_structure, $fk_software, $lvl, $actualKeys);
                            print '</p>';
                        print '</p>';
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') .$key.'. '. utf8_decode($module->id_name) . '</a></b></p>';
                        Session::forget('word_keys');

                        if ($id_industry) {
                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $id_industry);
                        } else {
                            $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure);
                        }

                        foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                            if($pr_module_task->is_default == 1) {
                                $task_key++;
                                if ($id_industry) {
                                    $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                                    $attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                } else {
                                    $attachments = Software_attach::getAttachmentsNamesByMS_task($pr_module_task->id_ms_task);
                                }

                                print '<p style="margin-left:'.$marginLeft.'cm;"><b>' .trim($newKey, '.').'.'.$key.'.'.$task_key.'. ' . utf8_decode($pr_module_task->name_task) . '</b></p>';

                                print '<br><div class="task-div level1" style="margin-left:'.$marginLeft.'cm;">';
                                    if(count($attachments) > 0) {
                                        print '<div class="description_task">' . trans('messages.label_attachments') . ': ';
                                            foreach($attachments as $attachment) {
                                                print $attachment->name_attachment . '; ';
                                            }
                                        print '</div>';
                                    }
                                    print '<div class="description_task">' . utf8_decode($pr_module_task->description_task) . '</div>';
                                    if($pr_module_task->lvl_priority) {
                                        print "<div> Gewichtung: " . $pr_module_task->lvl_priority . " </div>";
                                    }
                                print '<hr></div><br>';
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printSoftwareTreeWordDetailssChieldrens($module_structure, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        $newKey = implode('.',$keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if ($module->is_default == 1) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        $keys = explode('.', $newKey);
                        print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') . $key . '. ' . utf8_decode($module->id_name) . '</a></b>';
                            print '<p>';
                                if(empty($keys))
                                    $keys = [];
                                array_push($keys, $key);
                                Session::forget('word_keys');
                                Session::push('word_keys', $key);
                                self::printSoftwareTreeWordDetails($module_structure, $fk_software, $lvl, $keys);
                            print '</p>';
                        print '</p>';
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        print   '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.').($newKey ?'.': '').$key.'. '. utf8_decode($module->id_name) . '</a></b></p>';
                        Session::forget('word_keys');
                        $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure);
                        foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                            if($pr_module_task->is_default == 1) {
                                $task_key++;
                                $attachments = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                                print '<p><b>' .trim($newKey, '.').'.'.$key.'.'.$task_key.'. '. utf8_decode($pr_module_task->name_task) . '</b></p>';
                                print '<br><div class="task-div level1">';
                                    if(count($attachments) > 0) {
                                        print '<div class="description_task" style="margin-left:'.$marginLeft.'cm;">' . trans('messages.label_attachments') . ': ';
                                            foreach($attachments as $attachment) {
                                                print $attachment->name_attachment . '; ';
                                            }
                                        print '</div><br>';
                                    }
                                    print '<div class="description_task">' . utf8_decode($pr_module_task->description_task) . '</div>';
                                print '<hr></div><br>';
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printIndustryTreeWordContents($module_structure, $id_industry, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    if ($module->is_default == 1) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $actualKeys = explode('.', $newKey);
                            print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">'. trim($newKey, '.'). ($newKey ?'.': '') .$key.'. '. $module->id_name . '</a>';
                                print '<p>';
                                    if(empty($actualKeys))
                                        $actualKeys = [];
                                    array_push($actualKeys, $key);
                                    Session::forget('word_keys');
                                    Session::push('word_keys', $key);
                                    self::printIndustryTreeWordContentsChildrens($module_structure, $id_industry, $fk_software, $lvl, $actualKeys);
                                print '</p>';
                            print '</p>';
                        }
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') .$key.'. '. $module->id_name . '</a></p>';
                            Session::forget('word_keys');
                        }
                    }
                }
            }
        }
    }

    public static function printIndustryTreeWordContentsChildrens($module_structure, $id_industry, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        $newKey = implode('.',$keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    if ($module->is_default == 1) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $keys = explode('.', $newKey);
                            print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') . $key . '. ' . $module->id_name . '</a>';
                                print '<p>';
                                    if(empty($keys))
                                        $keys = [];
                                    array_push($keys, $key);
                                    Session::forget('word_keys');
                                    Session::push('word_keys', $key);
                                    self::printIndustryTreeWordContents($module_structure, $id_industry, $fk_software, $lvl, $keys);
                                print '</p>';
                            print '</p>';
                        }
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' . trim($newKey, '.').($newKey ?'.': '').$key.'. '. $module->id_name . '</a></p>';
                            Session::forget('word_keys');
                        }
                    }
                }
            }
        }
    }

    public static function printIndustryTreeWordDetails($module_structure, $id_industry, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    if ($module->is_default == 1) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $fk_industry, $lvl, $module->id_module_structure, 0);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $actualKeys = explode('.', $newKey);
                            print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">'. trim($newKey, '.'). ($newKey ?'.': '') .$key.'. '. $module->id_name . '</a></b>';
                                print '<p>';
                                    if(empty($actualKeys))
                                        $actualKeys = [];
                                    array_push($actualKeys, $key);
                                    Session::forget('word_keys');
                                    Session::push('word_keys', $key);
                                    self::printIndustryTreeWordDetailssChieldrens($module_structure, $id_industry, $fk_software, $lvl, $actualKeys);
                                print '</p>';
                            print '</p>';
                        }
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') .$key.'. '. $module->id_name . '</a></b></p>';
                            Session::forget('word_keys');
                            $pr_module_tasks = Ms_task::getTasksByStructureIndustry($module->id_module_structure, $id_industry, 0, 0);
                            foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                if($pr_module_task->is_default == 1) {
                                    $task_key++;
                                    $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                                    $attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                    print '<p><b>' .trim($newKey, '.').'.'.$key.'.'.$task_key.'. '.$pr_module_task->name_task . '</b></p>';
                                    print '<br><div class="task-div level1"';
                                        if(count($attachments) > 0) {
                                            print '<div class="description_task" style="margin-left:'.$marginLeft.'cm;">' . trans('messages.label_attachments') . ': ';
                                                foreach($attachments as $attachment) {
                                                    print $attachment->name_attachment . '; ';
                                                }
                                            print '</div><br>';
                                        }
                                        print '<div class="description_task" style="margin-left:'.$marginLeft.'cm;">' . strip_tags($pr_module_task->description_task) . '</div>';
                                    print '<hr></div><br>';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printIndustryTreeWordDetailssChieldrens($module_structure, $id_industry, $fk_software, $lvl = 1, $keys)
    {
        $lvl++;
        $newKey = implode('.',$keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                    if ($module->is_default == 1) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $fk_industry, $lvl, $module->id_module_structure, 0);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $keys = explode('.', $newKey);
                            print '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.') . ($newKey ?'.': '') . $key . '. ' . $module->id_name . '</a></b>';
                                print '<p>';
                                    if(empty($keys))
                                        $keys = [];
                                    array_push($keys, $key);
                                    Session::forget('word_keys');
                                    Session::push('word_keys', $key);
                                    self::printIndustryTreeWordDetails($module_structure, $id_industry, $fk_software, $lvl, $keys);
                                print '</p>';
                            print '</p>';
                        }
                    }
                }
            } else {
                if ($module->is_default == 1) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            print   '<p style="margin-left:'.$marginLeft.'cm;"><b><a name="' . $module->id_module_structure . '">' . trim($newKey, '.').($newKey ?'.': '').$key.'. '. $module->id_name . '</a></b></p>';
                            Session::forget('word_keys');
                            $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure);
                            foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                if($pr_module_task->is_default == 1) {
                                    $task_key++;
                                    $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                                    $attachments = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                                    print '<p><b>' .trim($newKey, '.').'.'.$key.'.'.$task_key.'. '.$pr_module_task->name_task . '</b></p>';
                                    print '<br><div class="task-div level1">';
                                        if(count($attachments) > 0) {
                                            print '<div class="description_task" style="margin-left:'.$marginLeft.'cm;">' . trans('messages.label_attachments') . ': ';
                                                foreach($attachments as $attachment) {
                                                    print $attachment->name_attachment . '; ';
                                                }
                                            print '</div><br>';
                                        }
                                        print '<div class="description_task" style="margin-left:'.$marginLeft.'cm;">' . strip_tags($pr_module_task->description_task) . '</div>';
                                    print '<hr></div><br>';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printTreeExcelContents($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $row = 20)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                    return array( $module->id_module_structure, $module->id_name);
                    $row++;
                    self::printTreeExcelContents($module_structure, $fk_software, $id_industry, $lvl, $id_project, $row);
                }
            } else {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    return array( $module->id_module_structure, $module->id_name);
                }
            }
        }
    }

    public static function printTreeWordDetails($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $actualKeys = explode('.', $newKey);
                            print   '<h3><p style="margin-left:'.$marginLeft.'cm;"><a name="' . $module->id_module_structure . '">' .$newKey.'.'.$key.'.'. utf8_decode($module->id_name) . '</a>';
                            print '</p></h3>';
                            if(empty($actualKeys))
                                $actualKeys = [];
                            array_push($actualKeys, $key);
                            Session::push('word_keys', $key);
                            self::printTreeWordDetailsChieldrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $actualKeys);
                        }
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                                $key++;
                                $marginLeft = $lvl * 0.50;
                                print   '<h3><p style="margin-left:'.$marginLeft.'cm;"><a name="' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a>';
                                    print  '<p></h3>';
                                    foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                        $tasMarginLeft = $marginLeft + 0.5;
                                        $task_key++;
                                        print '<h3><p style="margin-left:'.$tasMarginLeft.'cm;"><a name="' . $module->id_module_structure . '-'. $task_key.'">' .$newKey.'.'.$key.'.'.$task_key.'. '. utf8_decode($pr_module_task->name_task) . ' </a></p></h3>';
                                        $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                        $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_industry);

                                        if ($pr_module_task->task_color) {
                                            print "<div style='margin-left:".$tasMarginLeft."cm;'> Farbe: <span style='color: ".$pr_module_task->task_color."'>&bull;</span></div>";
                                        }

                                        if(count($attachments) > 0 || count($attachments_industry) > 0) {
                                            print '<div class="description_task" style="margin-left:'.$tasMarginLeft.'cm;">' . trans('messages.label_attachments') . ': ';
                                                foreach($attachments as $attachment) {
                                                    print $attachment->name_attachment . '; ';
                                                }
                                                foreach($attachments_industry as $attachment_industry) {
                                                    print $attachment_industry->name_attachment . '; ';
                                                }
                                            print '</div>';
                                        }
                                        print '<div class="task-div level1" style="margin-left:'.$tasMarginLeft.'cm;">';
                                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                                            print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->task_description)) . '</div> ';
                                        } else {
                                            print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->description_task)) . '</div> ';
                                        }
                                        if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                                            print "<br><div style='margin-left:".$tasMarginLeft."cm;'> Gewichtung: " . $pr_module_task->task_priority . " </div>";
                                        }
                                        print '<hr></div>';
                                    }
                                    print  '</p>';
                                print'</p>';
                                Session::forget('word_keys');
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printTreeWordDetailsChieldrens($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        $lvl++;
        $newKey = implode('.',Session::get('word_keys'));
        $key = 0;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $actualKeys = explode('.', $newKey);
                            print   '<h3><p style="margin-left:'.$marginLeft.'cm;"><a name="' . $module->id_module_structure . '">' .$newKey.'.'.$key.'.'. utf8_decode($module->id_name) . '</a>';
                            print '</p></h3>';
                            if(empty($actualKeys))
                                $actualKeys = [];
                            array_push($actualKeys, $key);
                            Session::push('word_keys', $key);
                            self::printTreeWordDetailsChieldrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $actualKeys);
                        }
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                                $key++;
                                $marginLeft = $lvl * 0.50;
                                print '<h3><p style="margin-left:'.$marginLeft.'cm;"><a name="' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a>';
                                print '<p></h3>';
                                foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                    $tasMarginLeft = $marginLeft + 0.5;
                                    $task_key++;
                                    print'<h3><p style="margin-left:'.$tasMarginLeft.'cm;"><a name="' . $module->id_module_structure . '-'. $task_key.'">' .$newKey.'.'.$key.'.'.$task_key.'. '. utf8_decode($pr_module_task->name_task) . ' </a></p></h3>';
                                    $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                    $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_industry);
                                    print '<div style="margin-left: '.$tasMarginLeft.'cm;>"';

                                    if ($pr_module_task->task_color) {
                                        print "<div> Farbe: <span style='color: ".$pr_module_task->task_color."'>&bull;</span></div>";
                                    }

                                    if(count($attachments) > 0 || count($attachments_industry) > 0) {
                                        print '<div class="description_task">' . trans('messages.label_attachments') . ': ';
                                            foreach($attachments as $attachment) {
                                                print $attachment->name_attachment . '; ';
                                            }
                                            foreach($attachments_industry as $attachment_industry) {
                                                print $attachment_industry->name_attachment . '; ';
                                            }
                                        print '</div>';
                                    }
                                    print '<div class="task-div level1">  ';
                                    if (strlen(trim($pr_module_task->task_description)) > 3) {
                                        print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->task_description)) . '</div> ';
                                    } else {
                                        print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->description_task)) . '</div> ';
                                    }

                                    if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                                        print "<br><div> Gewichtung: " . $pr_module_task->task_priority . " </div>";
                                    }
                                    print '<hr></div>';
                                    print '</div>';
                                }
                                print '</p>';
                                print '</p>';
                                Session::forget('word_keys');
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printTreeWordContents($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        $lvl++;
        Session::forget('word_keys');
        $newKey = implode('.',$keys);
        Session::put('word_keys', $keys);
        $key = 0;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            $actualKeys = explode('.', $newKey);
                            print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'.'.$key.'.'. utf8_decode($module->id_name) . '</a>';
                                print '<p>';
                                if(empty($actualKeys))
                                    $actualKeys = [];
                                array_push($actualKeys, $key);
                                Session::push('word_keys', $key);
                                self::printTreeWordContentsChieldrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $actualKeys);
                                print '</p>';
                            print '</p>';
                        }
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                                $key++;
                                $marginLeft = $lvl * 0.50;
                                print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a></p>';
                                foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                    $taskMarginLeft = $marginLeft + 0.5;
                                    $task_key++;
                                    print '<p style="margin-left:'.$taskMarginLeft.'cm;"><a href="#' . $module->id_module_structure . '-' . $task_key.'">' .$newKey.'.'.$key.'.'.$task_key.'. '. utf8_decode($pr_module_task->name_task) . ' </a></p>';
                                }
                                Session::forget('word_keys');
                            }
                        }
                    }
                }
            }
        }
    }

    public static function printTreeWordContentsChieldrens($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        $lvl++;
        $newKey = implode('.',$keys);
        $key = 0;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                    if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                        $key++;
                        $marginLeft = $lvl * 0.50;
                        $keys = explode('.', $newKey);
                        print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a>';
                            print '<p>';
                                if(empty($keys))
                                    $keys = [];
                                array_push($keys, $key);
                                Session::push('word_keys', $key);
                                self::printTreeWordContents($module_structure, $fk_software, $id_industry, $lvl, $id_project, $keys);
                            print '</p>';
                        print '</p>';
                    }
                }
            } else {
                if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                    $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                    if (count($pr_module_tasks) > 0) {
                        if($module->id_name && $module->id_name != '' && $module->id_name !== null) {
                            $key++;
                            $marginLeft = $lvl * 0.50;
                            print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a></p>';
                            foreach ($pr_module_tasks as $task_key => $pr_module_task) {
                                $taskMarginLeft = $marginLeft + 0.5;
                                $task_key++;
                                print '<p style="margin-left:'.$taskMarginLeft.'cm;"><a href="#' . $module->id_module_structure . '-' . $task_key.'">' .$newKey.'.'.$key.'.'.$task_key.'. '. utf8_decode($pr_module_task->name_task) . ' </a></p>';
                            }
                            Session::forget('word_keys');
                        }
                    }
                }
            }
        }
    }

    public static function getModuleByLvlIndustry($fk_software, $id_industry, $level, $fk_parent = 0, $fk_project = null)
    {
        if ($fk_project == null) {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                ->orderBy('ms_ind_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        } else {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_projects_ms_order', function ($join) use ($fk_project) {
                    $join->on('pr_projects_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_projects_ms_order.fk_project', '=', $fk_project);
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->where('level', '=', $level)
                // ->where('pr_module_structure.fk_project', '=', $fk_project)
                // ->where(function ($query) use ($fk_project) {
                //     $query->where('pr_module_structure.is_default', '=', 1)
                //         ->orWhere(function ($query) use ($fk_project) {
                //             $query->where('pr_module_structure.is_default', '=', 0)
                //                 ->where('pr_module_structure.fk_project', '=', $fk_project);
                //         });
                // })
                ->orderBy('ms_proj_order', 'asc')
                ->orderBy('ms_ind_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        }
        return $list;
    }

    public static function getModuleBySoftwareId($fk_software, $id_industry, $fk_parent = 0, $fk_project = null)
    {
        if ($fk_project == null) {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->orderBy('ms_ind_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        } else {
            $list = DB::table('pr_module_structure')
                ->selectRaw(" pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_projects_ms_order', function ($join) use ($fk_project) {
                    $join->on('pr_projects_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_projects_ms_order.fk_project', '=', $fk_project);
                })
                ->where('fk_software', '=', $fk_software)
                ->where('fk_parent', '=', $fk_parent)
                ->orderBy('ms_proj_order', 'asc')
                ->orderBy('ms_ind_order', 'asc')
                ->orderBy('ms_order', 'asc')->get();
        }
        return $list;
    }

    public static function printTreePDFDetailsEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project)
    {
        $lvl++;
        foreach ($module_structure as $module) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                print '<li> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    print '<div class="description_task">' . $description->description_module . '</div> ';
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    print '<div class="description_task">' . $description->description_submodule . '</div> ';
                }
                print '<ol>';
                self::printTreePDFDetailsEmptyProject($module_structure, $fk_software, $id_industry, $lvl, $id_project);
                print '</ol>';
                print '</li>';
            } else {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    print '<li class="menu_title"> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                        if($module->id_type == 1) {
                            $description = Module::findOrFail($module->fk_module_parent);
                            print '<div class="description_task">' . $description->description_module . '</div> ';
                        } else {
                            $description = Submodule::findOrFail($module->fk_id);
                            print '<div class="description_task">' . $description->description_submodule . '</div> ';
                        }
                        print  '<ol>';
                        foreach ($pr_module_tasks as $pr_module_task) {
                            print '<li>' . $pr_module_task->name_task . ' </li>';
                            $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                            $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_industry);
                            if(count($attachments) > 0 || count($attachments_industry) > 0) {
                                print '<div class="description_task">' . trans('messages.label_attachments') . ': ';
                                    foreach($attachments as $attachment) {
                                        print $attachment->name_attachment . '; ';
                                    }
                                    foreach($attachments_industry as $attachment_industry) {
                                        print $attachment_industry->name_attachment . '; ';
                                    }
                                print '</div>';
                            }
                            print '<div class="task-div level1">  ';
                                if (strlen(trim($pr_module_task->task_description)) > 3) {
                                    print '<div class="description_task">' . $pr_module_task->task_description . '</div> ';
                                } else {
                                    print '<div class="description_task">' . $pr_module_task->description_task . '</div> ';
                                }
                            print '</div>';
                            if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                                print "<br><div> Gewichtung: " . $pr_module_task->task_priority . " </div>";
                            }
                        }
                        print  '</ol>';
                    print'</li>';
                }
            }
        }
    }

    public static function printTreeEXCELDetailsEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
              if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project)) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                if($is_first == 1) {
                    $keys = [];
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);
                    $sheet->row($newRow, [$module->id_name]);
                    if($newRow % 2 == 1) {
                        $cells = 'A'.$newRow.':'.'Z'.$newRow;
                        $sheet->cells($cells, function($cells) {
                            $cells->setBackground('#e2efd7');
                        });
                    }
                } else {
                    $mod = [];
                    for($i = 2; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }
                    $mod[] = $module->id_name;
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);
                    $sheet->row($newRow, $mod);
                    if($newRow % 2 == 1) {
                        $cells = 'A'.$newRow.':'.'Z'.$newRow;
                        $sheet->cells($cells, function($cells) {
                            $cells->setBackground('#e2efd7');
                        });
                    }
                }
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    $array[$lvl]['description'] = $description->description_module;
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    $array[$lvl]['description'] = $description->description_submodule;
                }
                if(empty($keys))
                    $keys = [];
                array_push($keys, $key);
                self::printTreeEXCELDetailsEmptyProject($module_structure, $fk_software, $id_industry, $lvl, $id_project, $array, 0, $keys, $sheet);
              }
            } else {
              if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                $keys = [];
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $sheet->row($newRow, [$module->id_name]);
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                    } else {
                        $mod = [];
                        for($i = 2; $i < $lvl; $i++) {
                            $mod[$i] = '';
                        }
                        $mod[] = $module->id_name;
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $sheet->row($newRow, $mod);
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $mod = [];
                    for($i = 1; $i < Session::get('task_cell'); $i++) {
                        $mod[$i] = '';
                    }
                    $is_first_task = 1;
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        $mod2 = $mod;
                        $pr_module_task_key++;
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $mod2[] = $pr_module_task->name_task;
                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                            $mod2[] = strip_tags($pr_module_task->task_description);
                        } else {
                            $mod2[] = strip_tags($pr_module_task->description_task);
                        }
                        $mod2[] = $pr_module_task->task_priority;
                        $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                        $newFolder = 1;
                        foreach($attachs as $attach) {
                            Session::push('excel_files', $attach->name_attachment);
                            $mod2[] = $attach->name_attachment;
                        }
                        $sheet->row($newRow, $mod2);
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                    }
                 }
              }
            }
        }
    }

    public static function getEmptyProjectAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $array, $is_first, $keys, $zip)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                if($is_first == 1) {
                    $keys = [];
                    $newRow = Session::get('rows_attachments') + 1;
                    Session::put('rows_attachments', $newRow);
                } else {
                    $newRow = Session::get('rows_attachments') + 1;
                    Session::put('rows_attachments', $newRow);
                }
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    $array[$lvl]['description'] = $description->description_module;
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    $array[$lvl]['description'] = $description->description_submodule;
                }
                if(empty($keys))
                    $keys = [];
                array_push($keys, $key);
                self::getEmptyProjectAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl, $id_project, $array, 0, $keys, $zip);
            } else {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                        $keys = [];
                        $newRow = Session::get('rows_attachments') + 1;
                        Session::put('rows_attachments', $newRow);
                    } else {
                        $newRow = Session::get('rows_attachments') + 1;
                        Session::put('rows_attachments', $newRow);
                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $is_first_task = 1;
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                        $newFolder = 1;
                        if($attachs->first())
                            foreach($attachs as $attach) {
                                if($zip == 'check') {
                                    Session::put('check_for_attachments', 1);
                                } else {
                                    if($newFolder == 1) {
                                        if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                            $newFolder = 0;
                                            $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                        }
                                    } else {
                                        $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                    }
                                    Session::push('files_attachments', $attach->name_attachment);
                                }
                          }
                    }
                }
            }
        }
    }

    public static function printTreeEXCELDetailsEmptyProjectTaskCell($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
              if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                if($is_first == 1) {
                    $keys = [];
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);
                } else {
                    $mod = [];
                    for($i = 2; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }
                    $mod[] = $module->id_name;
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);
                }
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    $array[$lvl]['description'] = $description->description_module;
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    $array[$lvl]['description'] = $description->description_submodule;
                }
                if(empty($keys))
                    $keys = [];
                array_push($keys, $key);
                self::printTreeEXCELDetailsEmptyProjectTaskCell($module_structure, $fk_software, $id_industry, $lvl, $id_project, $array, 0, $keys, $sheet);
            }
          } else {
            if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                        $keys = [];
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                    } else {
                    $mod = [];
                    for($i = 2; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }

                    $mod[] = $module->id_name;
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);

                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $mod = [];
                    for($i = 1; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }
                    $is_first_task = 1;
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        $mod2 = $mod;
                        $pr_module_task_key++;
                        $newRow = Session::get('excel_rows') + 1;
                        if(Session::get('task_cell') < count($mod)+1) {
                            Session::put('task_cell', count($mod)+1);
                        }
                        $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                        if(Session::get('excel_files') < count($attachs)) {
                            Session::put('excel_files', count($attachs));
                        }
                    }
                }
             }
          }
       }
    }

    public static function printTreeEXCELDetailsSoftware($module_structure, $fk_software, $lvl = 1,  $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
              if ((Module_structure::hasChild($module->id_module_structure) == 1) && ($module->is_default == 1)) {
                  $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                  // echo "<pre>";dd($module_structure);
                  if($is_first == 1) {
                      $keys = [];
                      $newRow = Session::get('excel_rows') + 1;
                      Session::put('excel_rows', $newRow);
                      $sheet->row($newRow, [$module->fk_id, 0,$module->id_name]);
                      if($newRow % 2 == 1) {
                          $cells = 'A'.$newRow.':'.'Z'.$newRow;
                          $sheet->cells($cells, function($cells) {
                              $cells->setBackground('#e2efd7');
                          });
                      }
                  } else {
                      $mod = [$module->fk_id, 0];
                      for($i = 2; $i < $lvl; $i++) {
                          $mod[$i] = '';
                      }
                      $mod[] = $module->id_name;
                      $newRow = Session::get('excel_rows') + 1;
                      Session::put('excel_rows', $newRow);
                      $sheet->row($newRow, $mod);
                      if($newRow % 2 == 1) {
                          $cells = 'A'.$newRow.':'.'Z'.$newRow;
                          $sheet->cells($cells, function($cells) {
                              $cells->setBackground('#e2efd7');
                          });
                      }
                  }
                  if($module->id_type == 1) {
                      $description = Module::findOrFail($module->fk_module_parent);
                      $array[$lvl]['description'] = $description->description_module;
                  } else {
                      $description = Submodule::findOrFail($module->fk_id);
                      $array[$lvl]['description'] = $description->description_submodule;
                  }
                  if(empty($keys))
                      $keys = [];
                  array_push($keys, $key);
                  self::printTreeEXCELDetailsSoftware($module_structure, $fk_software, $lvl, $array, 0, $keys, $sheet);
              }
            else {
              if($module->is_default == 1) {
                $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                        $keys = [];
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $sheet->row($newRow, [$module->fk_id, 0,$module->id_name]);
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                    } else {
                        $mod = [$module->fk_id, 0];
                        for($i = 2; $i < $lvl; $i++) {
                            $mod[$i] = '';
                        }
                        $mod[] = $module->id_name;
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $sheet->row($newRow, $mod);
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $mod = [$module->fk_id, 0];
                    for($i = 1; $i < Session::get('task_cell'); $i++) {
                        $mod[$i] = '';
                    }
                    array_push($mod, []);
                    $is_first_task = 1;
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        if($pr_module_task->is_default === 1) {
                            $mod[0] = $pr_module_task->fk_task;
                            $mod[1] = 0;
                            $mod2 = $mod;
                            $pr_module_task_key++;
                            $newRow = Session::get('excel_rows') + 1;
                            Session::put('excel_rows', $newRow);
                            $mod2[] = $pr_module_task->name_task;
                            if (strlen(trim($pr_module_task->task_description)) > 3) {
                                $mod2[] = strip_tags($pr_module_task->task_description);
                            } else {
                                $mod2[] = strip_tags($pr_module_task->description_task);
                            }

                            $mod2[] = $pr_module_task->lvl_priority;

                            $attachs = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                            $newFolder = 1;
                            foreach($attachs as $attach) {
                                Session::push('excel_files', $attach->name_attachment);
                                $mod2[] = $attach->name_attachment;
                            }
                            $sheet->row($newRow, $mod2);
                            if($newRow % 2 == 1) {
                                $cells = 'A'.$newRow.':'.'Z'.$newRow;
                                $sheet->cells($cells, function($cells) {
                                    $cells->setBackground('#e2efd7');
                                });
                            }
                        }
                    }
                }
            }
        }
      }
    }

    public static function printTreeEXCELDetailsSoftwareTaskCell($module_structure, $fk_software, $lvl = 1, $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
            if (Module_structure::hasChild($module->id_module_structure) == 1 && $module->is_default == 1) {
              $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
              if($is_first == 1) {
                  $keys = [];
                  $newRow = Session::get('excel_rows') + 1;
                  Session::put('excel_rows', $newRow);
              } else {
                  $mod = [];
                  for($i = 2; $i < $lvl; $i++) {
                      $mod[$i] = '';
                  }
                  $mod[] = $module->id_name;
                  $newRow = Session::get('excel_rows') + 1;
                  Session::put('excel_rows', $newRow);
              }
              if($module->id_type == 1) {
                  $description = Module::findOrFail($module->fk_module_parent);
                  $array[$lvl]['description'] = $description->description_module;
              } else {
                  $description = Submodule::findOrFail($module->fk_id);
                  $array[$lvl]['description'] = $description->description_submodule;
              }
              if(empty($keys))
                  $keys = [];
              array_push($keys, $key);
              self::printTreeEXCELDetailsSoftwareTaskCell($module_structure, $fk_software, $lvl, $array, 0, $keys, $sheet);
          } else {
                $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
                // echo "<pre>"; dd($pr_module_tasks);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                        $keys = [];
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                    } else {
                    $mod = [];
                    for($i = 2; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }

                    $mod[] = $module->id_name;
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);

                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $mod = [];
                    for($i = 1; $i < $lvl; $i++) {
                        $mod[$i] = '';
                    }
                    $is_first_task = 1;
                    // echo "<pre>"; dd($pr_module_tasks);
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        $mod2 = $mod;
                        $pr_module_task_key++;
                        $newRow = Session::get('excel_rows') + 1;
                        if(Session::get('task_cell') < count($mod)+1) {
                            Session::put('task_cell', count($mod)+1);
                        }
                        // echo "<pre>";dd($pr_module_task);
                        $attachs = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                        if(Session::get('excel_files') < count($attachs)) {
                            Session::put('excel_files', count($attachs));
                        }
                    }
                }
            }
        }
    }
    public static function printTreeEXCELDetailsIndustry($module_structure, $fk_software, $id_industry, $lvl = 1,  $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            $key++;
            $is_first_submodule = 1;
            if ((Module_structure::hasChild($module->id_module_structure) == 1) && ($module->is_default == 1)) {
              if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                $newRow = Session::get('excel_rows') + 1;
                Session::put('excel_rows', $newRow);
                $mod = [];
                for($i = 2; $i < $lvl+1; $i++) {
                    $mod[$i] = '';
                }
                $mod[] = $module->id_name;

                if($newRow % 2 == 1) {
                    $cells = 'A'.$newRow.':'.'Z'.$newRow;
                    $sheet->cells($cells, function($cells) {
                        $cells->setBackground('#e2efd7');
                    });
                }
                $sheet->row($newRow, $mod);
                array_push($keys, $key);
                self::printTreeEXCELDetailsIndustry($module_structure, $fk_software, $id_industry, $lvl, $array, 0, $keys, $sheet);
             }
          }
          else {
            if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
              if($module->is_default == 1) {
                $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
                if (count($pr_module_tasks) > 0) {
                    $newRow = Session::get('excel_rows') + 1;
                    Session::put('excel_rows', $newRow);
                    $mod = [];
                    for($i = 2; $i < $lvl+1; $i++) {
                        $mod[$i] = '';
                    }

                    $mod[] = $module->id_name;
                    if(strlen(implode('', $mod)) > 0) {
                        $sheet->row($newRow, $mod);
                    }

                    if($newRow % 2 == 1) {
                        $cells = 'A'.$newRow.':'.'Z'.$newRow;
                        $sheet->cells($cells, function($cells) {
                            $cells->setBackground('#e2efd7');
                        });
                    }
                    $mod = [];
                    for($i = 1; $i <= Session::get('task_cell'); $i++) {
                        $mod[$i] = '';
                    }
                    $is_first_task = 1;
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        if($pr_module_task->is_default === 1) {
                            $mod2 = $mod;
                            $pr_module_task_key++;
                            $newRow = Session::get('excel_rows') + 1;
                            Session::put('excel_rows', $newRow);
                            $mod2[] = $pr_module_task->name_task;
                            if (strlen(trim($pr_module_task->task_description)) > 3) {
                                $mod2[] = strip_tags($pr_module_task->task_description);
                            } else {
                                $mod2[] = strip_tags($pr_module_task->description_task);
                            }

                            $mod2[] = $pr_module_task->lvl_priority;

                            $attachs = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $id_industry);
                            $newFolder = 1;
                            foreach($attachs as $attach) {
                                Session::push('excel_files', $attach->name_attachment);
                                $mod2[] = $attach->name_attachment;
                            }

                            $id_project_row = Projects_rows::where('fk_ms_task', '=', $pr_module_task->id_ms_task)
                            ->where('fk_project', '=', $pr_module_task->fk_project)->get();
                            $project_attachments = Projects_attach::where('fk_project_row', '=', $id_project_row[0]->id_project_row)->get();

                            foreach($project_attachments as $project_attachment) {
                                Session::push('excel_files', $project_attachment->name_attachment);
                                $mod2[] = $project_attachment->name_attachment;
                            }

                            $sheet->row($newRow, $mod2);
                            if($newRow % 2 == 1) {
                                $cells = 'A'.$newRow.':'.'Z'.$newRow;
                                $sheet->cells($cells, function($cells) {
                                    $cells->setBackground('#e2efd7');
                                });
                            }
                        }
                     }
                  }
               }
            }
         }
      }
   }

    public static function printTreeEXCELDetailsIndustryTaskCell($module_structure, $fk_software, $id_industry, $lvl = 1, $array, $is_first, $keys, $sheet)
    {
        $lvl++;
        foreach ($module_structure as $key => $module) {
            // echo "<pre>"; dd($module_structure);
            $key++;
            $is_first_submodule = 1;
            if (Module_structure::hasChild($module->id_module_structure) == 1 && $module->is_default == 1) {
              $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
              if($is_first == 1) {
                  $keys = [];
                  $newRow = Session::get('excel_rows') + 1;
                  Session::put('excel_rows', $newRow);
              } else {
                  $mod = [];
                  for($i = 2; $i < $lvl; $i++) {
                      $mod[$i] = '';
                  }
                  $mod[] = $module->id_name;
                  $newRow = Session::get('excel_rows') + 1;
                  Session::put('excel_rows', $newRow);
              }
              if($module->id_type == 1) {
                  $description = Module::findOrFail($module->fk_module_parent);
                  $array[$lvl]['description'] = $description->description_module;
              } else {
                  $description = Submodule::findOrFail($module->fk_id);
                  $array[$lvl]['description'] = $description->description_submodule;
              }
              if(empty($keys))
                  $keys = [];
              array_push($keys, $key);
              self::printTreeEXCELDetailsIndustryTaskCell($module_structure, $fk_software, $id_industry, $lvl, $array, 0, $keys, $sheet);
          } else {
                $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
                if (count($pr_module_tasks) > 0) {
                    if($is_first == 1) {
                        $keys = [];
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                    }
                    else {
                        $mod = [];
                        for($i = 2; $i < $lvl; $i++) {
                            $mod[$i] = '';
                        }

                        $mod[] = $module->id_name;
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                    }
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                    }
                    $mod = [];
                    for($i = 1; $i < $lvl+1; $i++) {
                        $mod[$i] = '';
                    }
                    $is_first_task = 1;
                    // echo "<pre>"; dd($pr_module_tasks);
                    foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                        $mod2 = $mod;
                        $pr_module_task_key++;
                        $newRow = Session::get('excel_rows') + 1;
                        if(Session::get('task_cell') < count($mod)+1) {
                            Session::put('task_cell', count($mod)+1);
                        }

                        $attachs = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $id_industry);
                        if(Session::get('excel_files') < count($attachs)) {
                            Session::put('excel_files', count($attachs));
                        }
                    }
                }
            }
        }
    }

    public static function printTreeWordContentsEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys = [])
    {
        $lvl++;
        Session::forget('word_keys');
        print '<br />';
        foreach ($module_structure as $key => $module) {
            $key++;
            $keys = [];
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                Session::put('word_keys');
                $marginLeft = ($lvl-2) * 0.50;
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$key.'. '. utf8_decode($module->id_name) . '</a>';
                print '<p>';
                array_push($keys, $key);
                Session::push('word_keys', $key);
                self::printTreeWordContentsEmptyProjectChildrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $keys, $is_first = 1);
                print '</p>';
                print '</p>';


            } else {
                $marginLeft = ($lvl-2) * 0.50;
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$key.'. '. utf8_decode($module->id_name) . '</a>';
                print '<p>';
                foreach ($pr_module_tasks as $taskKey => $pr_module_task) {
                    $taskKey++;
                    $marginLeftTask = $marginLeft + 0.50;
                    print'    <p style="margin-left:'.$marginLeftTask.'cm;"><a href="#' .$key.'-'. $taskKey . '">' .$key.'.'. $taskKey. '. '.utf8_decode($pr_module_task->name_task) . ' </a></p>';
                }
                print '</p>';
                print '</p>';
                Session::forget('word_keys');
            }
        }
    }

    public static function printTreeWordContentsEmptyProjectChildrens($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        $lvl++;
        $newKey = implode('.',Session::get('word_keys'));
        foreach ($module_structure as $key => $module) {
            $key++;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $marginLeft = ($lvl-2) * 0.50;
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                print   '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'. ' .$key.'. '. utf8_decode($module->id_name) . '</a>';
                print '<p>';
                array_push($keys, $key);
                Session::push('word_keys', $key);
                self::printTreeWordContentsEmptyProjectChildrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $keys);
                print '</p>';
                print '</p>';

            } else {
                $marginLeft = ($lvl-2) * 0.50;
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                print '<p style="margin-left:'.$marginLeft.'cm;"><a href="#' . $module->id_module_structure . '">' .$newKey.'.'.$key.'. '. utf8_decode($module->id_name) . '</a>';
                print '<p>';
                foreach ($pr_module_tasks as $taskKey => $pr_module_task) {
                    $taskKey++;
                    $marginLeftTask = $marginLeft + 0.50;
                    print'    <p style="margin-left:'.$marginLeftTask.'cm;"><a href="#' . implode('-',implode('.',Session::get('word_keys'))).'-'.$key . '">' .$newKey.'.'.$key.'.'. $taskKey. '. '.utf8_decode($pr_module_task->name_task) . ' </a></p>';
                }
                print '</p>';
                print '</p>';
                Session::forget('word_keys');
            }
        }
    }

    public static function printTreeWordDetailsEmptyProject($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $keys)
    {
        Session::forget('word_keys');
        $lvl++;
        $margin = $lvl-2;
        foreach ($module_structure as $key => $module) {
            $key++;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
              if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                $marginLeft = $margin * 0.5;
                print '<h3><p style="margin-left:'.$marginLeft.'cm;"> <a name="' . $module->id_module_structure . '">'  .$key.'. '. utf8_decode($module->id_name) . ' </a>';
                print '</p></h3>';
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    print '<div class="description_task">' . $description->description_module . '</div> ';
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    print '<div class="description_task">' . $description->description_submodule . '</div> ';
                }
                if(empty($keys))
                    $keys = [];
                array_push($keys, $key);
                Session::push('word_keys', $key);
                self::printTreeWordDetailsEmptyProjectChieldrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $keys);
              }
            } else {
              if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    $marginLeft = $margin * 0.5;
                    print '<h3><p class="menu_title" style="margin-left:'.$marginLeft.'cm;"> <a name="' . $module->id_module_structure . '">'.$key .'. ' . utf8_decode($module->id_name) . ' </a>';
                    print  '<p></h3>';
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                        print '<div class="description_task">' . $description->description_module . '</div> ';
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                        print '<div class="description_task">' . $description->description_submodule . '</div> ';
                    }
                    foreach ($pr_module_tasks as $taskKey => $pr_module_task) {
                        $taskKey++;
                        $marginLeftTask = $marginLeft + 0.5;
                        print'    <h3><p style="margin-left:'.$marginLeftTask.'cm;"><a name="' . $key.'-'. $taskKey . '">' .$key.'.'. $taskKey. '. '.utf8_decode($pr_module_task->name_task) . ' </a></p></h3>';

                        print '<div class="task-div level1">  ';
                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                            print '<div class="description_task">' . utf8_decode($pr_module_task->task_description) . '</div> ';
                        } else {
                            print '<div class="description_task">' . utf8_decode($pr_module_task->description_task) . '</div> ';
                        }
                        if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                            print "<br><div> Gewichtung: " . $pr_module_task->task_priority . " </div>";
                        }
                        print '<hr></div>';
                    }
                    print  '</p>';
                    print '</p>';
                    Session::forget('word_keys');
                }
            }
        }
      }
    }

    public static function printTreeWordDetailsEmptyProjectChieldrens($module_structure, $fk_software, $id_industry, $lvl = 0, $id_project, $keys)
    {
        $lvl++;
        $newKey = implode('.',Session::get('word_keys'));
        $margin = $lvl-2;
        foreach ($module_structure as $key => $module) {
            $key++;
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, $id_project);
                $marginLeft = $margin * 0.5;
                print '<h3><p style="margin-left:'.$marginLeft.'cm;"> <a name="' . $module->id_module_structure . '">'.$newKey.'.'  .$key.'. '. utf8_decode($module->id_name) . ' </a>';
                print '</p></h3>';
                if($module->id_type == 1) {
                    $description = Module::findOrFail($module->fk_module_parent);
                    print '<div class="description_task">' . $description->description_module . '</div> ';
                } else {
                    $description = Submodule::findOrFail($module->fk_id);
                    print '<div class="description_task">' . $description->description_submodule . '</div> ';
                }
                if(empty($keys))
                    $keys = [];
                array_push($keys, $key);
                Session::push('word_keys', $key);
                self::printTreeWordDetailsEmptyProjectChieldrens($module_structure, $fk_software, $id_industry, $lvl, $id_project, $keys);

            } else {
                $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                if (count($pr_module_tasks) > 0) {
                    $marginLeft = $margin * 0.5;
                    print '<h3><p class="menu_title" style="margin-left:'.$marginLeft.'cm;"> <a name="' . $module->id_module_structure . '">' .$newKey.'. '.$key.' ' . utf8_decode($module->id_name) . ' </a>';
                    print  '<p></h3>';
                    if($module->id_type == 1) {
                        $description = Module::findOrFail($module->fk_module_parent);
                        print '<div class="description_task">' . $description->description_module . '</div> ';
                    } else {
                        $description = Submodule::findOrFail($module->fk_id);
                        print '<div class="description_task">' . $description->description_submodule . '</div> ';
                    }
                    foreach ($pr_module_tasks as $taskKey => $pr_module_task) {
                        $taskKey++;
                        $marginLeftTask = $marginLeft + 0.5;
                        print'    <h3><p style="margin-left:'.$marginLeftTask.'cm;"><a name="' . implode('-',Session::get('word_keys')).'-'.$key . '">' .$newKey.'.'.$key.'.'. $taskKey.'. '.utf8_decode($pr_module_task->name_task) . ' </p></h3>';

                        print '<div class="task-div level1">  ';
                        if (strlen(trim($pr_module_task->task_description)) > 3) {
                            print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->task_description)) . '</div> ';
                        } else {
                            print '<div class="description_task">' . utf8_decode(nl2br($pr_module_task->description_task)) . '</div> ';
                        }

                        if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                            print "<br><div> Gewichtung: " . $pr_module_task->task_priority . " </div>";
                        }
                        print '<hr></div>';
                    }
                    print  '</p>';
                    print'</p>';
                    Session::forget('word_keys');
                }
            }
        }
    }

    public static function printTreePDFDetails($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project)
    {
        $lvl++;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        print '<li> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                            print '<ol>';
                                self::printTreePDFDetails($module_structure, $fk_software, $id_industry, $lvl, $id_project);
                            print '</ol>';
                        print '</li>';
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            print '<li class="menu_title"> <a name="' . $module->id_module_structure . '">' . $module->id_name . ' </a>';
                                print  '<ol>';
                                foreach ($pr_module_tasks as $pr_module_task) {
                                    $attachments = Projects_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_project);
                                    $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $id_industry);

                                    print'<li>' . $pr_module_task->name_task . ' </li>';
                                    print '<div class="level1"> ';

                                    if ($pr_module_task->task_color) {
                                        print "<div>Farbe: <span class='bullet' style='background-color: ".$pr_module_task->task_color."'></span></div>";
                                        print '<br/>';
                                    }

                                    if (strlen(trim($pr_module_task->task_description)) > 3) {
                                        print '<div class="description_task">' . nl2br($pr_module_task->task_description) . '</div>';
                                        print '<br/>';
                                    } else {
                                        print '<div class="description_task">' . nl2br($pr_module_task->description_task) . '</div>';
                                        print '<br/>';
                                    }

                                    if($pr_module_task->task_priority != '-' && $pr_module_task->task_priority != '0') {
                                        print "<div>Gewichtung: " . $pr_module_task->task_priority . " </div>";
                                        print '<br/>';
                                    }

                                    if(count($attachments) > 0 || count($attachments_industry) > 0) {
                                        if(count($attachments) > 0) {
                                            print trans('messages.label_attachments') . ':';
                                            print '<div class="attachements">';
                                            foreach($attachments as $key=>$attachment) {
                                                $key++;
                                                print '<p>';
                                                print $key .'. '.$attachment->name_attachment;
                                                print '</p>';
                                            }
                                            print '</div>';
                                            print '<br/>';
                                        }

                                        if(count($attachments_industry) > 0) {
                                            print trans('messages.label_attachments') . ':';
                                            print '<div class="attachements">';
                                            foreach($attachments_industry as $key=>$attachment_industry) {
                                                $key++;
                                                print '<p>';
                                                print $key .'. '.$attachment_industry->name_attachment;
                                                print '</p>';
                                            }
                                            print '</div>';
                                            print '<br/>';
                                        }
                                    }
                                    print '</div>';
                                }
                                print  '</ol>';
                            print'</li>';
                        }
                    }
                }
            }
        }
    }

    public static function printTreeExcelDetails($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $sheet)
    {
        $lvl++;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project)) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        $mod = [];
                        for($i = 2; $i < $lvl+1; $i++) {
                            $mod[$i] = '';
                        }
                        $mod[] = $module->id_name;
                        if($newRow % 2 == 1) {
                            $cells = 'A'.$newRow.':'.'Z'.$newRow;
                            $sheet->cells($cells, function($cells) {
                                $cells->setBackground('#e2efd7');
                            });
                        }
                        $sheet->row($newRow, $mod);
                        self::printTreeExcelDetails($module_structure, $fk_software, $id_industry, $lvl, $id_project, $sheet);
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            $newRow = Session::get('excel_rows') + 1;
                            Session::put('excel_rows', $newRow);
                            $mod = [];
                            for($i = 2; $i < $lvl+1; $i++) {
                                $mod[$i] = '';
                            }
                            $mod[] = $module->id_name;
                            if(strlen(implode('', $mod)) > 0) {
                                $sheet->row($newRow, $mod);
                            }
                            // $sheet->row($newRow, $mod);
                            if($newRow % 2 == 1) {
                                $cells = 'A'.$newRow.':'.'Z'.$newRow;
                                $sheet->cells($cells, function($cells) {
                                    $cells->setBackground('#e2efd7');
                                });
                            }
                            $mod = [];
                            for($i = 1; $i <= Session::get('task_cell'); $i++) {
                                $mod[$i] = '';
                            }
                            foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                                $mod2 = $mod;
                                $pr_module_task_key++;
                                $newRow = Session::get('excel_rows') + 1;
                                Session::put('excel_rows', $newRow);
                                $mod2[] = $pr_module_task->name_task;
                                if (strlen(trim($pr_module_task->task_description)) > 3) {
                                    $mod2[] = strip_tags($pr_module_task->task_description);
                                } else {
                                    $mod2[] = strip_tags($pr_module_task->description_task);
                                }
                                $mod2[] = $pr_module_task->task_priority;
                                $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                                $project = Project::getFirstById($id_project);
                                $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->fk_industry);

                                $newFolder = 1;
                                foreach($attachs as $attach) {
                                    Session::push('excel_files', $attach->name_attachment);
                                    $mod2[] = $attach->name_attachment;
                                }
                                foreach($attachments_industry as $attach) {
                                    Session::push('excel_files', $attach->name_attachment);
                                    $mod2[] = $attach->name_attachment;
                                }
                                if(strlen(implode('', $mod2)) > 0) {
                                    $sheet->row($newRow, $mod2);
                                }

                                if($newRow % 2 == 1) {
                                    $cells = 'A'.$newRow.':'.'Z'.$newRow;
                                    $sheet->cells($cells, function($cells) {
                                        $cells->setBackground('#e2efd7');
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $zip)
    {
        $lvl++;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        $newRow = Session::get('rows_attachments') + 1;
                        Session::put('rows_attachments', $newRow);
                        self::getAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl, $id_project, $zip);
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryChildChecked($id_industry, $module->id_module_structure) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            $newRow = Session::get('rows_attachments') + 1;
                            Session::put('rows_attachments', $newRow);
                            foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                                $attachs = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                                $project = Project::getFirstById($id_project);
                                $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->fk_industry);
                                $newFolder = 1;
                                if($attachs->first() || $attachments_industry->first()) {
                                    foreach($attachs as $attach) {
                                        if($zip == 'check') {
                                            Session::put('check_for_attachments', 1);
                                        } else {
                                            if($newFolder == 1) {
                                                if(file_exists(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment)) {
                                                    if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                                        $newFolder = 0;
                                                        $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                    }
                                                }
                                            } else {
                                                if(file_exists(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment)) {
                                                    $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                }
                                            }
                                        }
                                    }

                                    foreach($attachments_industry as $attach) {
                                        if($zip == 'check') {
                                            Session::put('check_for_attachments', 1);
                                        } else {
                                            if($newFolder == 1) {
                                                if(file_exists(public_path().'/images/attach_software/'.$attach->folder_attachment.'/'.$attach->name_attachment)) {
                                                    if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                                        $newFolder = 0;
                                                        $zip->addFile(public_path().'/images/attach_software/'.$attach->folder_attachment.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                    }
                                                } elseif (file_exists(public_path().'/images/attach_industry/'.$attach->folder_attachment.'/'.$attach->name_attachment)) {
                                                    if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                                        $newFolder = 0;
                                                        $zip->addFile(public_path().'/images/attach_industry/'.$attach->folder_attachment.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                    }
                                                }
                                            } else {
                                                if(file_exists(public_path().'/images/attach_software/'.$attach->folder_attachment.'/'.$attach->name_attachment)) {
                                                    $zip->addFile(public_path().'/images/attach_software/'.$attach->folder_attachment.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                } elseif (file_exists(public_path().'/images/attach_industry/'.$attach->folder_attachment.'/'.$attach->name_attachment)) {
                                                    $zip->addFile(public_path().'/images/attach_industry/'.$attach->folder_attachment.'/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getSoftwareAttachmentsZip($module_structure, $fk_software, $lvl = 1, $zip, $id_industry = null, $id_project = null)
    {
        $lvl++;
        foreach ($module_structure as $module) {
          if($module->is_default == 1) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
                $newRow = Session::get('rows_attachments') + 1;
                Session::put('rows_attachments', $newRow);
                self::getSoftwareAttachmentsZip($module_structure, $fk_software, $lvl, $zip);
            } else {
              if ($id_project) {
                  $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
              } else {
                  $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
              }

              if (count($pr_module_tasks) > 0) {
                  $newRow = Session::get('rows_attachments') + 1;
                  Session::put('rows_attachments', $newRow);
                  foreach ($pr_module_tasks as $pr_module_task) {
                      if($pr_module_task->ms_tasks_default == 1) {
                        if ($id_industry) {
                            $industry_ms_tasks = Industry_ms::getFirstByKey($pr_module_task->id_ms_task, $id_industry);
                            $attachs = Industry_attach::where('fk_industry_ms', '=', $industry_ms_tasks->id_industry_ms)->get();
                        } else {
                            $attachs = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                        }

                        $newFolder = 1;

                        foreach($attachs as $attach) {
                            if($zip == 'check') {
                                Session::put('check_for_attachments', 1);
                            } else {
                                if($newFolder == 1) {
                                    if(file_exists(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment)) {
                                        if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                            $newFolder = 0;
                                            $zip->addFile(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                        }
                                    }
                                } else {
                                  if(file_exists(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment)) {
                                      $zip->addFile(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                  }
                                }
                             }
                          }
                       }
                     }
                  }
               }
            }
        }
    }

    public static function getIndustryAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl = 1, $zip)
    {
        $lvl++;
        foreach ($module_structure as $module) {
          if($module->is_default == 1) {
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                // $module_structure = Module_structure::getModuleByLvl($fk_software, $lvl, $module->id_module_structure, 0);
                $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, 0);
                $newRow = Session::get('rows_attachments') + 1;
                Session::put('rows_attachments', $newRow);
                self::getIndustryAttachmentsZip($module_structure, $fk_software, $id_industry, $lvl, $zip);
            } else {
              $pr_module_tasks = Ms_task::getTasksByStructure($module->id_module_structure, 0, 0, '', '', 0, '', null, 0, 0);
              if (count($pr_module_tasks) > 0) {
                  $newRow = Session::get('rows_attachments') + 1;
                  Session::put('rows_attachments', $newRow);
                  foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                      if($pr_module_task->is_default == 1) {
                        $pr_module_task_key++;
                        // $attachs = Software_attach::getAttachmentByMS_task($pr_module_task->id_ms_task);
                        $industry_attachs = Industry_attach::getAttachmentByMS_task($pr_module_task->id_ms_task, $id_industry);
                        $id_project_row = Projects_rows::where('fk_ms_task', '=', $pr_module_task->id_ms_task)
                                          ->where('fk_project', '=', $pr_module_task->fk_project)->first();
                        $project_attachments = Projects_attach::where('fk_project_row', '=', $id_project_row->id_project_row)->get();
                        // echo "<pre>"; var_dump($project_attachments);
                        $id_project = $id_project_row->fk_project;
                        $newFolder = 1;

                        if($project_attachments->first() || $industry_attachs->first()) {
                            foreach($industry_attachs as $attach) {
                              if($zip == 'check') {
                                  Session::put('check_for_attachments', 1);
                              } else {
                                  if($newFolder == 1) {
                                      if(file_exists(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment)) {
                                          if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                              $newFolder = 0;
                                              $zip->addFile(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                          }
                                      }
                                  } else {
                                    if(file_exists(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment)) {
                                        $zip->addFile(public_path().'/images/attach_software/' . $attach->folder_attachment . '/'.$attach->name_attachment, $pr_module_task->name_task.' Files/'.$attach->name_attachment);
                                    }
                                }
                             }
                          }
                          foreach($project_attachments as $project_attach) {
                              if($zip == 'check') {
                                  Session::put('check_for_attachments', 1);
                              } else {
                                  if($newFolder == 1) {
                                      if(file_exists(public_path().'/images/attach_task/'.$id_project.'/'.$project_attach->name_attachment)) {
                                          if($zip->addEmptyDir($pr_module_task->name_task.' Files')) {
                                              $newFolder = 0;
                                              $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$project_attach->name_attachment, $pr_module_task->name_task.' Files/'.$project_attach->name_attachment);
                                          }
                                      }
                                  }
                                  else {
                                      if(file_exists(public_path().'/images/attach_task/'.$id_project.'/'.$project_attach->name_attachment)) {
                                          $zip->addFile(public_path().'/images/attach_task/'.$id_project.'/'.$project_attach->name_attachment, $pr_module_task->name_task.' Files/'.$project_attach->name_attachment);
                                      }
                                  }
                                }
                             }
                          }
                        }
                     }
                  }
               }
            }
        }
    }

    public static function printTreeExcelDetailsTaskCell($module_structure, $fk_software, $id_industry, $lvl = 1, $id_project, $sheet)
    {
        $lvl++;
        $current_project = Project::getFirstById($id_project);
        $current_project_created_at = strtotime($current_project->created_at);
        foreach ($module_structure as $module) {
            $module_locked_at = strtotime($module->locked_at);
            if (Module_structure::hasChild($module->id_module_structure) == 1) {
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $module_structure = Module_structure::getModuleByLvlIndustry($fk_software, $id_industry, $lvl, $module->id_module_structure, $id_project);
                        $mod = [];
                        for($i = 2; $i < $lvl+1; $i++) {
                            $mod[$i] = '';
                        }
                        $mod[] = $module->id_name;
                        $newRow = Session::get('excel_rows') + 1;
                        Session::put('excel_rows', $newRow);
                        self::printTreeExcelDetailsTaskCell($module_structure, $fk_software, $id_industry, $lvl, $id_project, $sheet);
                    }
                }
            } else {
                $module_locked_at = strtotime($module->locked_at);
                if($module->locked_at == null || $module->locked_at == '0000-00-00 00:00:00' || ($module_locked_at > $current_project_created_at)) {
                    if (Industry_ms::isIndustryModuleCheckedTasks($id_industry, $module->id_module_structure, $id_project) == true) {
                        $pr_module_tasks = Ms_task::getTasksByStructureChecked($module->id_module_structure, $id_project);
                        if (count($pr_module_tasks) > 0) {
                            $newRow = Session::get('excel_rows') + 1;
                            Session::put('excel_rows', $newRow);
                            $mod = [];
                            for($i = 2; $i < $lvl+1; $i++) {
                                $mod[$i] = '';
                            }
                            $mod = [];
                            for($i = 1; $i < $lvl; $i++) {
                                $mod[$i] = '';
                            }
                            foreach ($pr_module_tasks as $pr_module_task_key => $pr_module_task) {
                                if(Session::get('task_cell') < count($mod)+1) {
                                    Session::put('task_cell', count($mod)+1);
                                }
                                $mod2 = $mod;
                                $pr_module_task_key++;
                                $newRow = Session::get('excel_rows') + 1;
                                Session::put('excel_rows', $newRow);
                                $mod2[] = $pr_module_task->name_task;
                                if (strlen(trim($pr_module_task->task_description)) > 3) {
                                    $mod2[] = strip_tags($pr_module_task->task_description);
                                } else {
                                    $mod2[] = strip_tags($pr_module_task->description_task);
                                }
                                $mod2[] = $pr_module_task->task_priority;
                                $attachments = Projects_attach::getAttachmentByTask($pr_module_task->fk_ms_task, $id_project);
                                $project = Project::getFirstById($id_project);
                                $attachments_industry = Industry_attach::getAttachmentByTask($pr_module_task->id_ms_task, $project->fk_industry);
                                $attachs = count($attachments) + count($attachments_industry);
                                if(Session::get('excel_files') < $attachs) {
                                    Session::put('excel_files', $attachs);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getModuleByIndustryParent($id_industry, $fk_software, $fk_project = 0)
    {
        $modules = Module_structure::join('pr_industry_ms', function ($join) use ($id_industry) {
            $join->on('id_module_structure', '=', 'fk_module_structure');
            $join->where('pr_industry_ms.fk_industry', '=', $id_industry);
        })
            ->select('fk_module_parent')
            ->where('pr_module_structure.fk_software', '=', $fk_software)
            ->groupBy('fk_module_parent')
            ->get();

        $array = array();
        foreach ($modules as $module) {
            $array[] = $module->fk_module_parent;
        }
        if ($fk_project == 0) {
            $list = Module_structure::where('pr_module_structure.fk_software', '=', $fk_software)
                ->selectRaw("pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->whereIn('fk_id', $array)
                ->where('level', '=', 1)
                ->orderBy('ms_ind_order')
                ->orderBy('ms_order')
                ->get();
        } else {
            $list = Module_structure::where('pr_module_structure.fk_software', '=', $fk_software)
                ->selectRaw("pr_module_structure.* ,coalesce(`pr_modules`.`name_module`,`pr_submodules`.`name_submodule`) AS `id_name`")
                ->leftJoin('pr_modules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_modules.id_module')
                        ->where('pr_module_structure.id_type', '=', '1');
                })
                ->leftJoin('pr_submodules', function ($join) {
                    $join->on('pr_module_structure.fk_id', '=', 'pr_submodules.id_submodule')
                        ->where('pr_module_structure.id_type', '=', '2');
                })
                ->leftJoin('pr_industry_ms_order', function ($join) use ($id_industry) {
                    $join->on('pr_industry_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_industry_ms_order.fk_industry', '=', $id_industry);
                })
                ->leftJoin('pr_projects_ms_order', function ($join) use ($fk_project) {
                    $join->on('pr_projects_ms_order.fk_module_structure', '=', 'pr_module_structure.id_module_structure')
                        ->where('pr_projects_ms_order.fk_project', '=', $fk_project);
                })
                ->whereIn('fk_id', $array)
                ->where('level', '=', 1)
                ->orderBy('ms_proj_order', 'asc')
                ->orderBy('ms_ind_order')
                ->orderBy('ms_order')
                ->get();
        }
        return $list;
    }

    public static function hasChildIndustry($id_module_structure, $id_industry)
    {
        $list = Module_structure::join('pr_industry_ms', 'id_module_structure', '=', 'fk_module_structure')
            ->whereFk_parent($id_module_structure)
            ->where('fk_industry', '=', $id_industry)
            ->first();
        if (count($list) == 0) {
            $count = 0;
        } else {
            $count = 1;
        }
        return $count;
    }

    public static function getModuleByIndustry($id_industry, $fk_software, $lvl = 0)
    {
        $list = Module_structure::join('pr_industry_ms', 'id_module_structure', '=', 'fk_module_structure')
            ->where('pr_module_structure.fk_software', '=', $fk_software)
            ->where('pr_industry_ms.fk_industry', '=', $id_industry)
            ->get();
        return $list;
    }

    public static function getSoftwareByIndustry($id_industry)
    {
        $list = Software::join('pr_module_structure', 'id_software', '=', 'fk_software')
            ->join('pr_industry_ms', 'id_module_structure', '=', 'fk_module_structure')
            ->where('fk_industry', '=', $id_industry)
            ->select('id_software', 'name_software', 'name_software_en')
            ->groupBy('id_software')
            ->get();
        return $list;
    }

    public static function getSoftwareByMs($id_software)
    {
        $list = Software::join('pr_module_structure', 'id_software', '=', 'fk_software')
            ->where('fk_software', '=', $id_software)
            ->select('id_software', 'name_software', 'name_software_en')
            ->groupBy('id_software')
            ->get();
        return $list;
    }

    public static function getProjectByIndustry($id_industry)
    {
        $list = Project::join('pr_module_structure', 'id_project', '=', 'fk_project')
            ->where('fk_industry', '=', $id_industry)
            ->select('id_project', 'name_project')
            ->groupBy('id_project')
            ->get();
        return $list;
    }
}
