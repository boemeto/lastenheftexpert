@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('footer_scripts')
  <script>
      $("#modalAddPost").on('hidden.bs.modal', function () {
          $("#modalAddPost").removeData('bs.modal');
      });
      $("#modalAddPost").on('shown.bs.modal', function () {
      });
  </script>
@stop
@section('content')

    <div class="panel widget light-widget panel-bd-top  ">
        <div class="panel-heading ">
            <h3 class="mgtp-10"> {{trans('messages.nav_packages')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-responsive data_table">
              <thead>
                <tr class="packages_header">
                    <th>{{trans('messages.label_name')}}</th>
                    <th>{{trans('messages.label_details')}}</th>
                    <th>{{trans('messages.label_price')}}</th>
                    <th>{{trans('messages.label_color')}}</th>
                    <th>
                      {{-- {{trans('messages.label_color_preview')}} --}}
                    </th>
                    <th>{{trans('messages.label_period')}}</th>
                    <th>{{trans('messages.label_period_type')}}</th>
                    <th>{{trans('messages.label_project_type')}}</th>
                    <th>{{trans('messages.label_is_active')}}</th>
                    <th>{{trans('messages.label_action')}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($list as $pack)
                    <tr>
                        <td>{{$pack->name_package}}  </td>
                        <td>{{$pack->description}}</td>
                        <td>
                          <b class="pull-right pdr30">{{ formatNumbers( $pack->price)}} €</b>
                        </td>
                        <td>
                            {{$pack->color}}
                        </td>
                        <td>
                          <div class="color_code text-center" style="background: {{$pack->color}}"></div>
                        </td>
                        <td class="text-center">{{$pack->period}}</td>
                        <td>{{ getDictionaryName($pack->period_type,7) }}</td>
                        <td>
                          {{$pack->fk_type_project==1?trans('messages.with_tree'):trans('messages.without_tree')}}
                        </td>
                        <td>{{ getDictionaryName($pack->is_active,1)  }}   </td>
                        <td class="packages_edit_buttons">
                            <a href="{{ URL::route('packs.show',array($pack->id_package)) }}"
                               class="btn  ls-dark-blue-btn     btn-round btn-l">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ URL::route('packs.edit',array($pack->id_package)) }}"
                               data-toggle="modal" data-target="#modalAddPost"
                               class="btn  ls-orange-btn    btn-round btn-l">
                                <i class="fa fa-edit"></i>
                            </a>
                            {{ Form::open(array('route' => array('packs.destroy', $pack->id_package), 'method' => 'delete')) }}
                            <button type="submit" class="btn btn-round btn-l ls-red-btn "
                                    onclick="return confirm('{{trans('validation.confirmation_delete')}}')">
                                <i class="fa fa-trash"></i>
                            </button>
                            {{ Form::close()}}
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modalAddPost" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <a href="{{ URL::route('packs.create') }}"
       id="a_form_headquarter_0" data-toggle="modal" data-target="#modalAddPost"
       class="btn static_add_button ls-light-blue-btn a_form_headquarter  btn-round btn-xxl">
        <i class="fa fa-plus"></i>
    </a>
    <div class="title_static_add_button" id="div_a_form_headquarter_0">
        {{trans('messages.nav_package') }}  {{trans('messages.act_create') }}
    </div>
@stop
