<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Feb-16
 * Time: 12:16
 */
class Industry_consultant extends Eloquent
{


    public static $messages = [];
    public $primaryKey = 'id_industry_consultant';
    protected $table = 'pr_industry_consultant';
    protected $guarded = ['id_industry_consultant'];
    protected $fillable = ['fk_industry', 'fk_consultant'];


    public static function getFirstById($id)
    {
        $list = Industry_consultant::where('id_industry_consultant', '=', $id)->first();
        return $list;
    }

    public static function getFirstByKey($fk_industry, $fk_consultant)
    {
        $list = Industry_consultant::where('fk_industry', '=', $fk_industry)
            ->where('fk_consultant', '=', $fk_consultant)
            ->first();
        return $list;

    }

 

}