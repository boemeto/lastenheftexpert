<?php

/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 26-Oct-15
 * Time: 11:29
 */
class Adm_task extends Eloquent
{

    public static $messages = [];
    public static $rules = [
        'name_task' => 'required',
    ];
    public $primaryKey = 'id_task';
    protected $table = 'adm_tasks';
    protected $guarded = ['id_task'];
    protected $fillable = ['name_task', 'notes_task', 'fk_creator', 'fk_developer', 'fk_sprint', 'fk_status', 'fk_priority'];

    public static function isValid($data)
    {
        $validation = Validator::make($data, static::$rules);
        if ($validation->passes()) {
            return true;
        }
        static::$messages = $validation->messages();
        return FALSE;
    }

    public static function getFirstById($id)
    {
        $list = Adm_task::where('id_task', '=', $id)->first();
        return $list;
    }


    public static function getTaskBySprint($id_sprint)
    {
        $list = Adm_task::select('*', 'adm_tasks.created_at')
            ->leftJoin('adm_priorities', 'id_priority', '=', 'fk_priority')
            ->leftJoin('adm_statuses', 'id_status', '=', 'fk_status')
            ->where('fk_sprint', '=', $id_sprint)
            ->orderBy('adm_tasks.due_date', 'asc')
            ->orderBy('name_priority', 'asc')
            ->orderBy('fk_status', 'asc')
            ->get();
        return $list;
    }

    public static function createNewItem($name_task, $notes_task, $id_user, $fk_sprint, $fk_priority, $cost_task, $fk_status, $due_date)
    {
        $task = new Adm_task();
        $task->name_task = $name_task;
        $task->notes_task = $notes_task;
        $task->fk_creator = $id_user;
        $task->fk_sprint = $fk_sprint;
        $task->fk_status = $fk_status;
        $task->fk_priority = $fk_priority;
        $task->cost_task = $cost_task;
        $task->due_date = $due_date;
        $task->save();
        $id_task = $task->id_task;
        return $id_task;
    }


}