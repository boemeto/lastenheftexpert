<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('assets/images/ios/fickle-logo-72.png')}}"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('assets/images/ios/fickle-logo-72.png')}}"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('assets/images/ios/fickle-logo-114.png')}}"/>

    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

    <title>{{trans('messages.title_long')}}</title>


    <!-- Plugin Css Put Here -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Plugin Css End -->
    <!-- Custom styles Style -->
    <link href="{{ URL::asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/theme.min.css')}}" rel="stylesheet">
    <!-- Custom styles Style End-->

    <!-- Responsive Style For-->
    <link href="{{ URL::asset('assets/css/responsive.css')}}" rel="stylesheet">
    <!-- Responsive Style For-->

    <!-- Custom styles for this template -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="single-page   responsive login-layout" style="background: #F0F0F0">

<div class="panel-body  ">
    <div class="login-icon"><i class="fa fa-cog"></i></div>
    <h1 class="font-semibold text-center" style="font-size:52px">404 ERROR</h1>
 
    <div class="form-group">
        <div class="col-md-12">
            <h4 class="text-center mgbt-xs-20">Your requested page could not be found or it is currently unavailable.</h4>
            <p class="text-center"> Please <a href="{{URL::to('/')}}">click here</a> to go back to our home page.</p>

        </div>
    </div>

</div>

</body>
</html>