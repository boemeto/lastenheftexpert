<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('assets/images/ios/fickle-logo-72.png') }}"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{{ URL::asset('assets/images/ios/fickle-logo-72.png') }}"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{{ URL::asset('assets/images/ios/fickle-logo-114.png') }}"/>

    <!-- TODO: Add a favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">

    <title>{{trans('messages.title_long')}}</title>

    <!--Page loading plugin Start -->
    <link href="{{ URL::asset('assets/css/plugins/pace.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('assets/js/pace.min.js') }}"></script>
    <!--Page loading plugin End   -->

    <!-- Plugin Css Put Here -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/bootstrap-switch.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/ladda-themeless.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/custom_smart_wizard.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('assets/css/plugins/humane_themes/bigbox.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/humane_themes/libnotify.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/humane_themes/jackedup.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/icheck/skins/all.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/fileinput.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/plugins/smoothness.jquery-ui.css') }}" rel="stylesheet">
    <!-- Confirmation Messages -->
    <link href="{{ URL::asset('assets/css/plugins/amaranjs/amaran.min.css')}}" rel="stylesheet">


    <!-- Plugin Css End -->
    <!-- Custom styles Style -->
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- Custom styles Style End-->

    <!-- Responsive Style For-->
    <link href="{{ URL::asset('assets/css/responsive.css') }}" rel="stylesheet">
    <!-- Responsive Style For-->

    <!-- Custom styles for this template -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- Google maps -  location API --}}
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    {{-- insert necessary scripts in header--}}
    @yield('header_scripts')


</head>
<body class="login-screen light-green-color">


<div class="login-box">
    @yield('content')
</div>


</body>


<!--Layout Script start -->
<script src="{{ URL::asset('assets/js/pages/login.js') }}"></script>
<script src="{{ URL::asset('assets/js/lib/jquery-2.1.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/lib/jqueryui.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-switch.min.js') }}"></script>
<!--Layout Script end -->
<!--Form Wizard JS Start-->
<script src="{{ URL::asset('assets/js/jquery.smartWizard.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/formWizard.js') }}"></script>
<!--Form Wizard JS End-->
<!--Form Script start -->
<script src="{{ URL::asset('assets/js/icheck.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/checkboxRadio.js') }}"></script>
<!--File input Script start -->
<script src="{{ URL::asset('assets/js/fileinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.autosize.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/sampleForm.js')}}"></script>
<!--Form Script end -->

<script src="{{ URL::asset('assets/js/lib/jquery.easing.js') }}"></script>


<!--Script for notification start-->

<script src="{{ URL::asset('assets/js/loader/ladda.js') }}"></script>
<script src="{{ URL::asset('assets/js/humane.min.js') }}"></script>


<!--ConFirmation Messages Script-->
<script src="{{ URL::asset('assets/js/jquery.amaran.min.js') }}"></script>
<!--ConFirmation Messages Script End-->

{{-- insert necessary scripts in footer--}}
@yield('footer_scripts')
<script type="text/javascript">

    function show_msg_amaran() {
        if ($('.amaran_confirm').length > 0) {
            var confirm = $('.amaran_confirm').html();
            $.amaran({
                'theme': 'colorful',
                'content': {
                    message: confirm,
                    bgcolor: '#324e59',
                    color: '#fff'
                }
            });
        }

        if ($('.amaran_error').length > 0) {
            var error = $('.amaran_error').html();
            $.amaran({
                'theme': 'colorful',
                'content': {

                    message: error,
                    bgcolor: '#D9534F',
                    color: '#fff'
                }

            });
        }
    }

    $(document).ready(function () {

        show_msg_amaran();

    });
</script>

</html>