<script type="text/javascript">
    (function ($) {
        $.fn.EasyTree = function (options) {
            var defaults = {
                selectable: true,
                deletable: true,
                editable: true,
                addable: true,
                i18n: {
                    deleteNull: '<?php print trans('messages.label_deleteNull') ?>',
                    deleteConfirmation: '<?php print trans('messages.label_deleteConfirmation') ?>',
                    confirmButtonLabel: '<?php print trans('messages.act_confirm') ?>',
                    editMultiple: '<?php print trans('messages.act_edit') ?>',
                    addMultiple: '<?php print trans('messages.act_add') ?>',
                    collapseTip: '<?php print trans('messages.act_collapse') ?>',
                    expandTip: '<?php print trans('messages.act_expand') ?>',
                    selectTip: '<?php print trans('messages.act_select') ?>',
                    unselectTip: '<?php print trans('messages.act_deselect') ?>',
                    editTip: '<?php print trans('messages.act_edit') ?>',
                    saveTip: '<?php print trans('messages.act_save') ?>',
                    addTip: '<?php print trans('messages.act_add') ?>',
                    deleteTip: '<?php print trans('messages.act_delete') ?>',
                    cancelButtonLabel: '<?php print trans('messages.act_cancel') ?>'
                }
            };


            var warningAlert = $('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var dangerAlert = $('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong></strong><span class="alert-content"></span> </div> ');
            var createInput = $('<div class="input-group">' +
                    '<input type="text" class="tree-control">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm">Save</button>' +
                    '<button type="button" class="btn ls-red-btn cancel">Cancel</button>' +
                    ' </span>' +
                    ' </div> ');
            var editInput = $('<div class="input-group">' +
                    '<input type="text" class="easy-tree-editor">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" class="btn ls-light-blue-btn confirm_edit">Save</button>' +
                    '<button type="button" class="btn ls-red-btn cancel_edit">Cancel</button>' +
                    ' </span>' +
                    ' </div> ');
            var createButton = $('<div class="create"><button class="btn ls-light-blue-btn btn-round"><i ' +
                    'class="fa fa-plus"></i></button></div> ');
            var editButton = $('<div class="edit"><button  class="btn ls-orange-btn btn-round"><i ' +
                    'class="fa fa-edit"></i></button></div>');
            var deleteButton = $('<div class="remove"><button class="btn  ls-red-btn btn-round"><i ' +
                    'class="fa fa-remove"></i></button></div>');
            var optionBox = '<div class="row">' +
                    '<div class="col-xs-2"></div>' +
                    '<div class="col-xs-10 inside-with-border">' +
                    '<a href="javascript:void(0)" class="btn ls-light-blue-btn addSubmoduleTask" onClick="modal_dialog_add_task_new(0)"> ' +
                    '<i class="fa fa-plus"></i> </a>' +
                    '</div>' +
                    '</div>';

            options = $.extend(defaults, options);

            this.each(function () {


                var easyTree = $(this);

                $(easyTree).find('li:has(ul)').addClass('parent_li').find('span.menu_title').attr('title', options.i18n.collapseTip);

                // add easy tree toolbar dom
                if (options.deletable || options.editable || options.addable) {

                    $(easyTree).find('li > span.menu_title').after('<div class="easy-tree-toolbar"  style="display:none;"></div>');
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('li > span.menu_title').find('.easy-tree-toolbar').append(deleteButton);
//                    $(easyTree).prepend('<div class="easy-tree-toolbar" style="display:block;"></div> ');

                }


                // addable
                if (options.addable) {

                    $(easyTree).find('.easy-tree-toolbar').append(createButton);
                    $(easyTree).find('.create > button').click(function () {

                        var selected = getAddableItems();
                        if (selected.length <= 0) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editNull);
                        }
                        else if (selected.length > 1) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editMultiple);
                        }
                        else {
                            var value = $.trim($(selected).find(' > span.menu_title > a').text());

                            var id = $(selected).find('.menu_title').attr('id');
                            if (id.indexOf("company") >= 0) {
                                var id_nr = id.replace('company', "");
                                modal_dialog_add_headquarter(id_nr);
                            }
                            if (id.indexOf("headquarter") >= 0) {
                                var id_nr = id.replace('headquarter', "");
                                modal_dialog_add_user(id_nr);
                            }
                            if (id.indexOf("user") >= 0) {
                                var id_nr = id.replace('user', "");
//                                modal_dialog_add_user(id_nr);

                            }
                        }

                    });
                }

                // editable
                if (options.editable) {
//           
                    $(easyTree).find('.easy-tree-toolbar').append(editButton);
                    $(easyTree).find('.edit > button').attr('title', options.i18n.editTip).click(function () {


//                        $(easyTree).find('input.easy-tree-editor').remove();
                        $(easyTree).find('li > span.menu_title > a:hidden').show();
                        var selected = getEditableItems();
                        if (selected.length <= 0) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editNull);
                        }
                        else if (selected.length > 1) {
                            $(easyTree).prepend(warningAlert);
                            $(easyTree).find('.alert .alert-content').html(options.i18n.editMultiple);
                        }
                        else {
                            var value = $.trim($(selected).find(' > span.menu_title > a').text());

                            var id = $(selected).find('.menu_title').attr('id');
                            if (id.indexOf("company") >= 0) {
                                var id_nr = id.replace('company', "");
                                modal_dialog_edit_company(id_nr);
                            }
                            if (id.indexOf("headquarter") >= 0) {
                                var id_nr = id.replace('headquarter', "");
                                modal_dialog_edit_headquarter(id_nr);
                            }
                            if (id.indexOf("user") >= 0) {
                                var id_nr = id.replace('user', "");
                                modal_dialog_edit_user(id_nr);

                            }

                        }
                    });
                }

                // deletable
                if (options.deletable) {

                    $(easyTree).find('.easy-tree-toolbar').append(deleteButton);
                    $(easyTree).find('.remove > button').click(function () {
                        var selected = getEditableItems();
                        var id = $(selected).find('.menu_title').attr('id');
                        if (id.indexOf("company") >= 0) {
                            var id_nr = id.replace('company', "");
                            modal_dialog_delete_company(id_nr);
                        }
                        if (id.indexOf("headquarter") >= 0) {
                            var id_nr = id.replace('headquarter', "");
                            modal_dialog_delete_headquarter(id_nr);
                        }
                        if (id.indexOf("user") >= 0) {
                            var id_nr = id.replace('user', "");
                            modal_dialog_delete_user(id_nr);
                        }

                    });
                }

                // collapse or expand
                $(easyTree).delegate(' li.parent_li > span.glyphicon', 'click', function (e) {
                    var div_children = $(this).parent('li.parent_li').find(' div > ul > li');
                    var all_children = $(this).parent('li.parent_li').find('  ul > li');
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(':visible')) {
//                        children.hide('fast');
                        $(this).attr('title', options.i18n.expandTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-plus-sign')
                                .removeClass('glyphicon-minus-sign');

                        all_children.children('span').find(
                                ' > span.glyphicon').addClass(
                                'glyphicon-plus-sign').removeClass(
                                'glyphicon-minus-sign');
                        $(this).parent('li.parent_li').find('.border_row').hide('fast');
                        all_children.hide('fast');

                    } else {

                        if ($(this).find('tasks_title')) {
                            children.show('fast');
                            div_children.show('fast');
                        }
                        $(this).attr('title', options.i18n.collapseTip)
                                .find(' > span.glyphicon').not('.close_tasks')
                                .addClass('glyphicon-minus-sign')
                                .removeClass('glyphicon-plus-sign');
                    }
                    e.stopPropagation();
                });

                // selectable, only single select
                if (options.selectable) {

//                    alert('selectable');

                    $(easyTree).find('li > span > a').attr('title', options.i18n.selectTip);
                    $(easyTree).find('li > span > a').click(function (e) {
                        var li = $(this).parent().parent();

                        $('.easy-tree-toolbar').not($(li).children('.easy-tree-toolbar')).hide();


                        if (li.hasClass('li_selected')) {
                            $(this).attr('title', options.i18n.selectTip);
                            $(li).removeClass('li_selected');
                            $('.easy-tree-toolbar').hide();
                        }
                        else {
                            $(easyTree).find('li.li_selected').removeClass('li_selected');
                            $(this).attr('title', options.i18n.unselectTip);
                            $(li).addClass('li_selected');

                            if (!$(li).hasClass('not_selectable') || $(li).hasClass('li_editable')) {
                                $(this).parent().next('.easy-tree-toolbar').show();
                            }
                        }

                        if (options.deletable || options.editable || options.addable) {
                            var selected = getAddableItems();
                            var li_parents = $(this).parents("li").length;
                            hide_modal_box();
                            if (options.addable) {

                                if ((selected.length <= 0 || selected.length > 1) || li_parents > 4) {
//                                    alert('trues');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').addClass('disabled');
                                }
                                else {
//                                    alert('falses');
                                    $(easyTree).find('.easy-tree-toolbar .create > button').removeClass('disabled');
                                }
                            }
                            var selected = getEditableItems();
                            if (options.editable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .edit > button').removeClass('disabled');
                            }
                            var selected = getDeletableItems();
                            if (options.deletable) {
                                if (selected.length <= 0 || selected.length > 1)
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').addClass('disabled');
                                else
                                    $(easyTree).find('.easy-tree-toolbar .remove > button').removeClass('disabled');
                            }

                        }

                        e.stopPropagation();

                    });
                }

                var getAddableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_selectable');
                };

                // Get selected items
                var getSelectedItems = function () {
                    return $(easyTree).find('li.li_selected');
                };

                var getEditableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable');
                };

                var getDeletableItems = function () {
                    return $(easyTree).find('li.li_selected').not('li.li_not_editable').not('li.li_not_deletable');
                };

            });
        };

    })(jQuery);

</script>