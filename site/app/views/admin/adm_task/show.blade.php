@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop
@section('header_scripts')
    <link href="{{URL::asset('assets/js/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet"/>

@stop
@section('footer_scripts')

    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>



    <script type="text/javascript">


        $(".file-3").fileinput({
            showCaption: false,
            browseClass: "btn ls-light-blue-btn",
            browseLabel: 'Upload ...',
            fileType: "any",
            showUpload: false,
            maxFileCount: 5,
        });

        $("#modalEditTask").on('hidden.bs.modal', function () {
            $("#modalEditTask").removeData('bs.modal');
        });
        $("#modalAddTask").on('hidden.bs.modal', function () {
            $("#modalAddTask").removeData('bs.modal');
        });
        $("#modalEditSprint").on('hidden.bs.modal', function () {
            $("#modalEditSprint").removeData('bs.modal');
        });
        $("#modalAddSprint").on('hidden.bs.modal', function () {
            $("#modalAddSprint").removeData('bs.modal');
        });
        $("#modalEditSprint").on('shown.bs.modal', function () {
            jQuery(".switchCheckBox").bootstrapSwitch();
        });


        $("#modalAddTask").on('shown.bs.modal', function () {

            $('#sandbox-container').datepicker({
                weekStart: 1,
                language: "de",
                daysOfWeekDisabled: "0,6",
                autoclose: true,
                todayHighlight: true,
                format: "dd.mm.yyyy",
            });

            $(document).on('change', 'input[type="file"]', function () {
                var input = $(this);
                var numFiles = input.get(0).files ? input.get(0).files.length : 1;
                var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $('input[type="file"]').on('fileselect', function (event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;
                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }
            });


            // call sumernote text editor
            jQuery('.summernote_task').summernote({});
            jQuery(".switchCheckBox").bootstrapSwitch();
            var rowCount = 1;

            $(document).on("click", "#addMoreRows", function () {

                rowCount++;
                var recRow = '<div id="rowCount' + rowCount + '">' +
                        '<div class="input-group">' +
                        '<select class="form-control" name="fk_responsible[' + rowCount + ']">' +
                        <?php
                                foreach ($users as $id_user => $user) {
                                    print '\'<option value="' . $id_user . '">' . $user . '</option>\' +';
                                }
                                ?>
                                '</select>' +
                        '<span class="input-group-btn">' +
                        ' <a href="#"  class="removeRow btn btn-danger"   id="removeRow_' + rowCount + '" title="Remove row"><i class="fa fa-trash-o"></i></a>' +
                        '</span>';
                '</div></div>';
                jQuery('#addedRows').append(recRow);
            });

            $(document).on("click", ".removeRow", function () {

                var id = $(this).attr('id');
                var removeNum = id.replace('removeRow_', "");
                jQuery('#rowCount' + removeNum).remove();
            });

        });

        $("#modalEditTask").on('shown.bs.modal', function () {


            $('#sandbox-container').datepicker({
                weekStart: 1,
                language: "de",
                daysOfWeekDisabled: "0,6",
                autoclose: true,
                todayHighlight: true,
                format: "dd.mm.yyyy",
            });

            // call sumernote text editor
            $(document).on('change', 'input[type="file"]', function () {
                var input = $(this);
                var numFiles = input.get(0).files ? input.get(0).files.length : 1;
                var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $('input[type="file"]').on('fileselect', function (event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;
                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }
            });

            jQuery(".switchCheckBox").bootstrapSwitch();
            jQuery('.summernote').summernote({});
        });

        $('.show_attach').click(function () {
            var id = $(this).attr('id');
            var id_nr = id.replace('show_attach_', "");
            $('#div_add_attach_' + id_nr).toggle();
        });
    </script>
@stop
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"> Releases </h3>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body">

                    @foreach($releases as $release)
                        <div class="hex {{($release->is_current)?'email':'twitter'}} {{($release->id_release == $id)?'active':''}}">
                            <a href="{{URL::to('adm_task/'.$release->id_release)}}">
                                <div class="overlay"></div>
                                {{$release->id_release}}
                            </a>
                        </div>
                    @endforeach

                </div>
            </div>

        </div>
        <!-- Main Content Element  Start-->
        <div class="row">
            <div class="col-md-12">
                <div class="ls-timeline">
                    <ul class="ls_tmtimeline">
                        <li class="month add_sprint">
                            <a href="{{ URL::to('adm_tasks_dialog_add_sprint/' .$id) }}"
                               data-toggle="modal" data-target="#modalAddSprint" title="Neuer Sprint">
                                <div class="ls_tmicon_ls ls">
                                    <h4><i class="fa fa-plus" style="font-size: 1.4em"></i> <br>Sprint</h4>
                                </div>
                            </a>
                        </li>

                        @foreach($sprints as $sprint)
                            <li class="month {{($sprint->is_closed == 1)?'closed_sprint':''}} ">
                                <a href="{{ URL::to('adm_tasks_dialog_edit_sprint/' .$sprint->id_sprint) }}"
                                   data-toggle="modal" data-target="#modalEditSprint" title="Sprint bearbeiten">
                                    <div class="ls_tmicon_ls ls">
                                        <h4>{{$sprint->name_sprint}}</h4>
                                    </div>
                                </a>

                                <div class="ls_tmicon_desc">
                                    {{nl2br($sprint->comment)}}
                                </div>
                            </li>


                            @if($sprint->is_closed == 0)
                                <li class="add_task">
                                    <a href="{{ URL::to('adm_tasks_dialog_add_task/' .$id.'/'.$sprint->id_sprint) }}"
                                       class="pull-left" data-toggle="modal" data-target="#modalAddTask" title="Neue Aufgabe">
                                        <div class="ls_tmicon">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            <?php $tasks = Adm_task::getTaskBySprint($sprint->id_sprint); ?>
                            @foreach($tasks as $task)
                                <li>
                                    <?php $creator = User::getFirstById($task->fk_creator) ?>
                                    <?php $image_user = User::getLogoUser($creator->id); ?>
                                    <div class="ls-timeline-user">
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object img-circle" src="{{ URL::asset($image_user)}}" title="{{$creator->first_name.' '.$creator->last_name}}">
                                            </a>

                                            <div class="media-body ">

                                                <h4 class="media-heading">
                                                    Status: <span data-type="select" id="editable_status{{$task->id_task}}">{{$task->name_status}}</span><br>

                                                </h4>

                                                <time class="ls_tmtime" datetime="{{$task->due_date}}">
                                                    <span>  Fälligkeit: {{date('d.m.Y', strtotime($task->due_date))}}</span>
                                                    <span>
                                                        @if($task->cost_task !='')
                                                            Kosten: {{$task->cost_task }}  <i class="fa fa-eur"></i>
                                                        @endif
                                                    </span>
                                                </time>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ URL::to('adm_tasks_dialog_edit_task/' .$task->id_task) }}"
                                       data-toggle="modal" data-target="#modalEditTask" title="Aufgabe bearbeiten">
                                        <div class="ls_tmicon" style="box-shadow: 0 0 0 3px  {{$task->color_priority}};">
                                            {{$task->name_priority}}
                                        </div>
                                    </a>

                                    <div class="ls_tmlabel ">
                                        <?php $responsible = Adm_responsibility::getResponsibilityByTask($task->id_task); ?>
                                        <h2>
                                            <span class="left adm_task_title">{{$task->name_task}}</span>
                                            <span class="right">
                                                               @foreach($responsible as $user)
                                                    <?php    $image_user = User::getLogoUser($user->id); ?>
                                                    <a href="{{URL::to('delete_resp_task/'.$task->id_task.'/'.$user->id)}}" onclick="return confirm('{{trans('validation.confirmation_question')}}')">
                                                        <img class="img-circle small"
                                                             src="{{ URL::asset($image_user)}}"
                                                             onmouseover="this.src='{{ URL::asset('assets/images/delete-icon.png')}}'"
                                                             onmouseout="this.src='{{ URL::asset($image_user)}}'"
                                                             title="{{$user->first_name.' '.$user->last_name}}">
                                                    </a>
                                                @endforeach
                                                {{ Form::open(array('route' => array('adm_task.destroy', $task->id_task), 'method' => 'delete','style'=>"display: inline-block;")) }}
                                                <button type="submit" class="btn btn-danger btn-flat " onclick="return confirm('{{trans('validation.confirmation_question')}}')" style="display: inline-block;">
                                                    <i class=" fa fa-trash  "></i>

                                                </button>
                                                {{ Form::close()}}
                                                
                                            </span>

                                            <div class="clear"></div>
                                        </h2>

                                        {{$task->notes_task}}

                                        {{--<a href="javascript:void(0)" class="show_attach btn btn-default" id="show_attach_{{$task->id_task}}">--}}
                                        {{--<i class="fa fa-paperclip"></i>--}}
                                        {{--</a>--}}


                                        <div class="show_attachments">
                                            <div class="row">


                                                <?php $attachments = Adm_attachment::getTaskAttachments($task->id_task); ?>
                                                @foreach($attachments as $attach)
                                                    <div class="col-xs-2" style="position: relative; text-align: right;">
                                                        <?php $img_attach = Adm_attachment::getTaskAttachImages($attach->id_attachment); ?>

                                                        <a href="{{ URL::to('/')}}/images/attach_adm_task/{{$attach->name_file}}" target="_blank">
                                                            <img src="{{$img_attach}}" style="max-height: 60px; max-width: 99%;">
                                                        </a>
                                                        <a href="{{URL::to('/adm_delete_attached_file/'.$attach->id_attachment)}}">    <span class="badge badge-red"> 
                                                           <i class="fa fa-trash"></i> 
                                                        </span></a>


                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>
                                        <br>

                                        <div class="task_comment_section">

                                            <div class="form-group">
                                                <?php $comments = Adm_comment::getCommentsByTask($task->id_task); ?>
                                                <table class="table table-comments">
                                                    @foreach($comments as $comm)
                                                        <tr>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-1">
                                                                        <?php    $image_user = User::getLogoUser($comm->id); ?>
                                                                        <img class="img-circle small" src="{{ URL::asset($image_user)}}" title="{{$comm->first_name.' '.$comm->last_name}}">

                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {{nl2br($comm->comment)}}
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        {{--<a href="" class="btn ls-orange-btn"><i class="fa fa-edit"></i> </a>--}}
                                                                        {{ Form::open(array('route' => array('adm_comments.destroy', $comm->id_comment), 'method' => 'delete','style'=>"display: inline-block;")) }}
                                                                        <button type="submit" class="btn btn-danger btn-flat "
                                                                                onclick="return confirm('{{trans('validation.confirmation_question')}}')" style="display: inline-block;">
                                                                            <i class=" fa fa-trash  "></i>
                                                                        </button>
                                                                        {{ Form::close()}}
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                                {{ Form::open(['action'=>'AdmCommentsController@store']) }}
                                                <textarea class="form-control" name="comment" placeholder="Kommentieren" style="height: 70px"></textarea>
                                                {{Form::hidden('id_task',$task->id_task)}}
                                                <br>
                                                {{Form::submit('Speichern',['class'=>'btn btn-default'])}}
                                                {{Form::close()}}
                                            </div>

                                        </div>

                                    </div>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- Main Content Element  End-->


    </div>

    <!-- Modal Edit  task -->
    <div class="modal fade" id="modalEditTask" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- Modal Add new task-->
    <div class="modal fade" id="modalAddTask" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- Modal Add new sprint-->
    <div class="modal fade" id="modalAddSprint" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- Modal Edit Sprint-->
    <div class="modal fade" id="modalEditSprint" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@stop