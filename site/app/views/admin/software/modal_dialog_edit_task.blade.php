<?php
    $projects_rows = Projects_rows::where('fk_ms_task', '=', $pr_module_task->id_ms_task)->first();
?>

<div class="row">
    <a href="javascript:void(0)" onclick="hide_modal_box();">
        <div class="hide_controller">
            <i class="fa fa-times"></i>
        </div>
    </a>
    <div class="col-xs-12 text-right">
      <div class="row">
        <div class="col-md-2 pull-right" style="{{count($projects_rows)==0?'width:13%;':''}}">
            {{Form::checkbox('is_default',1,$pr_module_task->is_default, ['class'=>'switchCheckBox inline is_default_task',
              'id'=>'is_default_task_'.$pr_module_task->id_ms_task,'data-size'=>"mini"])}}
            @if (count($projects_rows)!=0)
                <a href="{{URL::to('delete_task_from_structure/'.$id_ms_task)}}"
                   class="button_delete_task btn inline btn-round btn-l btn-block btn-danger"
                   data-form="{{$id_ms_task}}"
                   data-toggle="modal" data-target="#confirmDelete"
                   data-title="Delete Task"
                   data-message="{{trans('validation.confirmation_delete_task')}}">
                    <i class="fa fa-trash"></i>
                </a>
            @endif
        </div>
        <div class="col-md-2 pull-right" style="margin-top: -1px">
          <i class="fa fa-info-circle" title="{{trans('messages.label_default_task_standard')}}"></i>
        </div>
      </div>
    </div>
    <div class="col-xs-12 mgtp-10 ">
        <div class="title_line"> {{($pr_module_task->is_default == 0)?'<i class="fa fa-bell-o" style="color: #D9534F"></i>':''}}  {{ $task->name_task  }} </div>
    </div>
</div>
{{ Form::model($pr_module_task,['route'=>['task.update',$pr_module_task->fk_task], 'class'=>'dz-clickable remove-border', 'id'=>'dropzoneFormEdit', 'method'=>'PATCH'])}}

<div class="row">
    <div class="col-md-12">
        <section class="ac-container description-section">
            <div id="task_description">
                <input id="ac-1" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-1" class="description-label">{{trans('messages.label_details')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-4 border-pd5">
                                {{Form::radio('edit_task',2,true,['class'=>'icheck-green'])}}
                                <span class="edit-task-label">{{ trans('messages.l_edit_current_task')}}<span>
                            </div>
                            <div class="col-xs-4 col-xs-offset-2 border-pd5">
                                {{Form::radio('edit_task',1,false,['class'=>'icheck-green'])}}
                                <span class="edit-task-label">{{ trans('messages.l_edit_all_tasks')}}<span>
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <!--start copy-->
                        <ul class="nav nav-tabs icon-tab">
                            <li class="active"><a href="#deutsch" data-toggle="tab"> <span>DE</span></a></li>
                            <li><a href="#engl" data-toggle="tab"> <span>EN</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tab-border">
                            <div class="tab-pane fade in active" id="deutsch">
                                <div class="form-group">
                                    {{ Form::text('name_task', $task->getAttribute('name_task','de'), [
                                      'class'=>"form-control",
                                      "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                      "required",
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <b>Character Counter:</b> {{ strlen($task->getAttribute('description_task','de')) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task" class="summernote" placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','de')}}</textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="engl">
                                <div class="form-group">
                                    {{ Form::text('name_task_en', $task->getAttribute('name_task','en'), [
                                      'class'=>"form-control",
                                      "placeholder"=> trans('messages.nav_task').' ' .trans('messages.l_name'),
                                      'oninvalid'=>"this.setCustomValidity('" . trans('validation.validate_empty_field') . "')",
                                      "oninput"=>"this.setCustomValidity('')"
                                      ]) }}
                                </div>
                                <div class="form-group">
                                    <b>Character Counter:</b> {{ strlen($task->getAttribute('description_task','en')) }}
                                </div>
                                <div class="form-group">
                                    <textarea name="description_task_en" class="summernote " placeholder="{{trans('messages.l_description')}}">{{ $task->getAttribute('description_task','en')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <section class="ac-container attachment-section">
                                <div id="task-attachments">
                                    <input id="ac-2" name="accordion-1" type="checkbox" checked="checked">
                                    <label for="ac-2" class="attachment-label">{{trans('messages.label_sidebar_attachments')}}</label>
                                    <article class="ac-any">
                                        <div class="dropzone article_content border-tb" id="uploadFiles">
                                            {{Form::hidden('id_ms_task',$pr_module_task->id_ms_task,["id"=>"id_ms_task"])}}
                                            <div class="dz-message row">
                                                <i class="fa fa-paperclip"></i> Ziehen/Ablegen oder hier klicken um eine Datei anzuhängen
                                            </div>
                                        </div>
                                    </article>
                                    <article class="ac-any attachment-article">
                                        <div class="article_content">
                                            @if(count($attachments)>0)
                                                <div class="modal-attachments">
                                                    <div class="row">
                                                        @foreach($attachments as $attachment)
                                                            <?php    $icon = Software_attach::getIconByAttachment($attachment->extension) ?>
                                                            <div class="col-xs-3 col-attachments"
                                                                 id="col-attachments{{$attachment->id_software_attachment}}">
                                                                <div style="display: block; width: 100%">
                                                                    <a href="{{ Url::to('/')}}/images/attach_software/{{$attachment->folder_attachment}}/{{$attachment->name_attachment}}"
                                                                       download class="button_download_file" target="_blank">
                                                                        <div class="file_attachments" style="background: {{$icon}} "
                                                                             id="div_file_{{$attachment->id_software_attachment}}"
                                                                             title="{{$attachment->name_attachment }}">
                                                                        </div>
                                                                    </a>
                                                                    <div class="attachments_edit_buttons" id="attachments_edit_buttons{{$attachment->id_software_attachment}}">
                                                                        <a href="javascript:void(0)"
                                                                           id="button_delete_attachment{{$attachment->id_software_attachment}}"
                                                                           class="button_delete_attachment">
                                                                            <i class="fa fa-trash-o"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                                {{--<div style="display: block; width: 100%">--}}
                                                                {{--<div class="attachments_name_file" id="attachments_name_file{{$attachment->id_projects_attach}}">--}}
                                                                {{--{{$attachment->name_attachment }}--}}
                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </article>
                                </div>
                            </section>
                        </div>
                        <div class="form-group mgtp-10">
                            {{ Form::text('code_task',  $task->code_task, ['class'=>"form-control","placeholder"=> trans('messages.nav_task_code') ]) }}
                        </div>
                        <div class="row" style="margin: 10px 0">
                            <div class="col-xs-3 border-pd5">
                                {{Form::checkbox('is_checked' ,1, $pr_module_task->is_checked,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}}
                                {{trans('messages.label_suggested')}} {{trans('messages.nav_task')}}
                            </div>
                            <div class="col-xs-3 col-xs-offset-3 border-pd5">
                                {{Form::checkbox('is_mandatory' ,1, $pr_module_task->is_mandatory,['class'=>'icheck-green','data-size'=>"mini", 'style'=>'display:inline-block'])}}
                                {{trans('messages.label_mandatory')}} {{trans('messages.nav_task')}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="lvl_priority" id="task_pair" class="task-priority-pair">
                                    <option value="">{{trans('messages.label_priority')}}</option>
                                    @foreach($priorities as $priority)
                                        <option value="{{$priority->lvl_priority}}" {{($pr_module_task->lvl_priority == $priority->lvl_priority)?'selected':''}}>{{$priority->lvl_priority}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                    $selected_pair = Task_pair::getFirstById($pr_module_task->fk_pair);
                                    if (count($selected_pair) == 1) {
                                        $selected_color = $selected_pair->color_pair;
                                    } else {
                                        $selected_color = '#fff';
                                }
                                ?>
                                <select name="fk_pair" id="task_pair" class="task-priority-pair" style="background: {{$selected_color}}" >
                                    <option value="0" style=" background: #fff">{{trans('messages.label_pair')}} {{trans('messages.nav_task')}}</option>
                                    @foreach($pairs as $pair)
                                        <option value="{{$pair->id_pair}}" style=" background: {{$pair->color_pair}}" {{($pr_module_task->fk_pair == $pair->id_pair)?'selected':''}}>{{$pair->name_pair}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <!--end copy-->
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-3" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-3">{{trans('messages.label_select_industry')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <ul class="nav nav-tabs icon-tab">
                            @foreach($industries_types as $industry_type)
                                @if($industry_type->id_industry_type == 1)
                                    <li class="active">
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#ind{{$industry_type->id_industry_type}}" data-toggle="tab">
                                            <span><i class="fa {{$industry_type->font_icon}}"></i> </span></a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="check-all">
                          <button type="button" class="check-all-icon" title="Check all" data-message="{{trans('messages.check_all_industries')}}"
                                  data-task-id="{{$id_ms_task}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}">
                              <i class="fa fa-check" aria-hidden="true" ></i>
                          </button>
                          <button type="button" class="check-all-icon-inactive" title="Check all">
                              <i class="fa fa-check" aria-hidden="true" ></i>
                          </button>
                          <button type="button" class="uncheck-all-icon" title="Uncheck all" data-message="{{trans('messages.uncheck_all_industries')}}"
                                  data-task-id="{{$id_ms_task}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}">
                              <i class="fa fa-remove"></i>
                          </button>
                          <button type="button" class="uncheck-all-icon-inactive" title="Uncheck all">
                              <i class="fa fa-remove"></i>
                          </button>
                        </div>
                        <div class="tab-content tab-border select_industry">
                            @foreach($industries_types as $industry_type)
                                <?php
                                  $industries = Industry::getIndustryByType($industry_type->id_industry_type);
                                ?>
                                <div class="tab-pane fade {{($industry_type->id_industry_type == 1)?' in active':''}}  " id="ind{{$industry_type->id_industry_type}}">
                                    <div style="display: block">
                                        {{$industry_type->name_industry_type}}
                                    </div>
                                    <div class="check-all-type">
                                        @if (count($industries)>0)
                                            <button type="button" class="add-check-all-type" title="Check all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.check_all_categ_industries')}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}"
                                                    data-task-id="{{$id_ms_task}}">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-check-all-type-inactive" title="Check all">
                                                <i class="fa fa-check" aria-hidden="true" ></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type" title="Uncheck all" data-category-id="{{$industry_type->id_industry_type}}"
                                                    data-message="{{trans('messages.uncheck_all_categ_industries')}}" data-url="{{action('TaskController@check_admin_all_industry_task')}}"
                                                    data-task-id="{{$id_ms_task}}" style="display: none">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            <button type="button" class="add-uncheck-all-type-inactive" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @elseif(count($industries) == 0)
                                            <button type="button" class="inactive-btn" title="Uncheck all" style="display: inline-block">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button type="button" class="inactive-btn" title="Check all">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        @endif()
                                    </div>
                                    <hr>
                                    @foreach($industries as $industry)
                                        <div class="industry-container" style="display: block">
                                            <input type="checkbox" class='icheck-green task_check' id="task_check_{{$industry->id_industry}}_{{$id_ms_task}}"
                                               {{(Industry_ms::isIndustryChecked($industry->id_industry, $id_ms_task) == true)?'checked':''}}
                                               name="fk_industry[]" value="{{$industry->id_industry}}" data-url="{{action('TaskController@check_admin_industry_task')}}"
                                               style="display: inline-block"> {{$industry->name_industry}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-4" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-4">{{trans('messages.label_synonyms')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="meta_tags" value="{{$task->getAttribute('meta_tags')}}"/>
                    </div>
                </article>
            </div>
            <div>
                <input id="ac-5" name="accordion-1" type="checkbox" checked="checked">
                <label for="ac-5">{{trans('messages.label_address_code')}}</label>
                <article class="ac-any">
                    <div class="article_content">
                        <input type="text" class="input-tags" name="address_code" value="{{$task->getAttribute('address_code')}}"/>
                    </div>
                </article>
            </div>
        </section>
        {{Form::hidden('id_ms_task' ,$id_ms_task)}}
        {{-- <div class="row"> --}}
          {{-- <div class="col-md-6"> --}}
            <button type="submit" class="btn ls-light-blue-btn btn-block">
                {{trans('messages.act_save')}}
            </button>
          {{-- </div> --}}
          {{-- <div class="col-md-6">
            <button type="button" class="btn ls-orange-btn btn-block"
                    onclick="window.task_id={{$task->id_task}}">
                {{trans('messages.duplicate')}}
            </button>
          </div> --}}
        {{-- <div> --}}
          {{-- <br> --}}
        <button type="button" class="btn btn-danger btn-block" onclick="hide_modal_box()">
            {{trans('messages.act_close')}}
        </button>
    </div>
</div>
<br>
{{Form::close()}}

<script type="text/javascript">
  //   function duplicate(task_id) {
  //     $.ajax(
  //       {
  //          url:'/duplicate_software_task/'+task_id,
  //          method: 'get',
  //          success: function(data)
  //          {
  //            console.log(data);
  //          }
  //   });
  // }
  $(document).ready(function() {
      var all_checked_inputs = $(".select_industry input:checked").length;
      $('.select_industry .tab-pane').each(function(){
          var category_checked_inputs = $(this).find('input:checked').length;
          var category_input_containers= $(this).find('.industry-container').length;

          if (category_checked_inputs == 0) {
              $(this).find('.add-check-all-type').show();
              $(this).find('.add-check-all-type-inactive').hide();
              $(this).find('.add-uncheck-all-type').hide();
              $(this).find('.add-uncheck-all-type-inactive').show();
          }

          if (category_checked_inputs > 0 && category_checked_inputs < category_input_containers) {
              $(this).find('.add-check-all-type').show();
              $(this).find('.add-check-all-type-inactive').hide();
              $(this).find('.add-uncheck-all-type').show();
              $(this).find('.add-uncheck-all-type-inactive').hide();
          }

          if(category_checked_inputs == category_input_containers) {
              $(this).find('.add-check-all-type').hide();
              $(this).find('.add-check-all-type-inactive').show();
              $(this).find('.add-uncheck-all-type').show();
              $(this).find('.add-uncheck-all-type-inactive').hide();
          }
      });
  });

  $(document).ready(function() {
      if($(".select_industry input:checked").length == $(".industry-container").length) {
          $('.check-all-icon').hide();
          $('.check-all-icon-inactive').show();
      }

      if ($(".select_industry input:checked").length == 0) {
          $('.uncheck-all-icon').hide();
          $('.uncheck-all-icon-inactive').show();
      }
  });

  $('#dropzoneFormEdit').on('click', function() {
    setTimeout(function() {
       $(document).one('DOMNodeInserted', '.dz-preview', function(e) {
            $(e.target.nodeName).find('.dz-preview').each(function() {
               $(this).insertAfter('.dz-message');
            });
          });
      });
  });
</script>
