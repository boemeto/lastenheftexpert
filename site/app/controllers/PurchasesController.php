<?php

use DB;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Session;
use Stripe;

class PurchasesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	public function getCheckout() {
      //dd(Input::all());
      $projectId = Input::all()['project_id'];
      $project = Project::getFirstById($projectId);

      return View::make('front.projects.project_expired', compact('project'));
	}

	public function postCheckout($id) {
      //Sending charge to STRIPE
      $subpackage_id = Input::all()['subpackage_id'];
      $subpackage = Subpackages::getFirstByPackageId($subpackage_id);
      //price with TVA
      $price = $subpackage->subpackage_price*1.19;
      $apiKey = Config::get('stripe.stripe.secret');
      $stripe = Stripe::make($apiKey);

      $token = Input::all()['stripeToken'];
      $customer = $stripe->customers()->create([
                      'email' => Input::all()['stripeEmail'],
                      'source'  => $token
      ]);

      $charge = $stripe->charges()->create([
          'customer' => $customer['id'],
          'currency' => 'EUR',
          'amount'   => $price,
      ]);

      //DB update for project, package and invoice
      $date = date('d.m.Y');
      $project = Project::getFirstById($id);
      $package = Package::getFirstById($subpackage->package_id);
			$invoice = new Invoice();

      $dateInvoice = date('Y.m.d');

      $invoice->inv_serial = 'LE';
      $invoice->inv_number = Invoice::getInvoiceNumber();
      $invoice->inv_date = $dateInvoice;
      $invoice->fk_company = $project->fk_company;
      $invoice->fk_project = $project->id_project;
      $invoice->fk_user = $project->fk_user;
      $invoice->unit_price = $subpackage->subpackage_price;
      $invoice->payment_term = $package->payment_interval;
      $invoice->sum_paid = $price;
			$invoice->fk_status = 3;
			$invoice->invoice_period = $subpackage->subpackage_period;
			$invoice->payment_method = 'card';

      if(strtotime($project->due_date) > strtotime($date)) {
          $nextDate = date('Y-m-d', strtotime("+$subpackage->subpackage_period months", strtotime($project->due_date)));
					$nextDate = date('Y-m-d', strtotime("+1 day", strtotime($nextDate)));
					$invoice->initial_date = date('Y-m-d', strtotime("+1 day", strtotime($project->due_date)));
      } else {
          $nextDate = date('Y-m-d', strtotime("+$subpackage->subpackage_period months", strtotime($date)));
					$invoice->initial_date = $dateInvoice;
      }

			$invoice->end_date = $nextDate;
      $project->due_date = $nextDate;
      $project->status = 1;
      $project->update();
      $invoice->save();

      $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
      return Redirect::back()->withErrors($errors);
	}

	public function getPaymentPaypalStatus()
	{
			// echo "<pre>"; dd(Input::get('id'));
			return Redirect::action('ProjectsController@show', ['id' => Input::get('id')]);
	}

	public function postCheckoutPaypal($id) {
			// dd(Input::get('data')['project_id']);
      $subpackage_id = Input::get('data')['subpackage_id'];
      $subpackage = Subpackages::getFirstByPackageId($subpackage_id);
      //price with TVA
      $price = $subpackage->subpackage_price*1.19;
      //DB update for project, package and invoice
      $date = date('d.m.Y');
      $project = Project::getFirstById(Input::get('data')['project_id']);
      $package = Package::getFirstById($subpackage->package_id);
			$invoice = new Invoice();

      $dateInvoice = date('Y.m.d');

      $invoice->inv_serial = 'LE';
      $invoice->inv_number = Invoice::getInvoiceNumber();
      $invoice->inv_date = $dateInvoice;
      $invoice->fk_company = $project->fk_company;
      $invoice->fk_project = $project->id_project;
      $invoice->fk_user = $project->fk_user;
      $invoice->unit_price = $subpackage->subpackage_price;
      $invoice->payment_term = $package->payment_interval;
      $invoice->sum_paid = $price;
			$invoice->fk_status = 3;
			$invoice->invoice_period = $subpackage->subpackage_period;
			$invoice->payment_method = 'paypal';

      if(strtotime($project->due_date) > strtotime($date)) {
          $nextDate = date('Y-m-d', strtotime("+$subpackage->subpackage_period months", strtotime($project->due_date)));
					$nextDate = date('Y-m-d', strtotime("+1 day", strtotime($nextDate)));
					$invoice->initial_date = date('Y-m-d', strtotime("+1 day", strtotime($project->due_date)));
      } else {
          $nextDate = date('Y-m-d', strtotime("+$subpackage->subpackage_period months", strtotime($date)));
					$invoice->initial_date = $dateInvoice;
      }

			$invoice->end_date = $nextDate;
      $project->due_date = $nextDate;
      $project->status = 1;
      $project->update();
      $invoice->save();

      $errors = new MessageBag(['confirm_' => [Lang::get('validation.edit_success')]]);
      return Redirect::action('ProjectsController@show', ['id' => Input::get('data')['project_id']])->withErrors($errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
