@extends('admin.layouts.default')
@section('dropdown-menu')
    @include('admin.layouts.dropdown-menu')
@stop
@section('sidebar-menu')
    @include('admin.layouts.sidebar-menu')
@stop

@section('header_scripts')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins/selectize.bootstrap3.css')}}">
@stop

@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/software/dropzone.js')}}"></script>
    <!-- Module menu tree style -->
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
    @include('admin/software/tree_script')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
    {{--<script src="{{ URL::asset('assets/js/jquery.sticky.js')}}"></script>--}}
    @include('admin/scripts/ajax_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
            $(".dropzone").dropzone({uploadMultiple: false});

            hide_empty_modules();
        });
        function hide_empty_modules() {
            $(".parent_li").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }

            });
            $(".li_not_selectable").not('.software').each(function () {
                if ($(this).find(".tasks_title").length == 0) {
                    $(this).hide();
                }
            });
        }

        $(document).on('ifChecked', ".task_check", function (event) {

            var id = jQuery(this).attr('id');
            var id_nr = id.replace('task_check_', "");
            var arr = id_nr.split('_');
            var data = {
                'id_industry': arr[0],
                'id_ms_task': arr[1],
                'is_checked': 1
            };
            // save to database
            jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
                  .done(function (msg) {
                      $.amaran({
                          'theme': 'colorful',
                          'content': {
                              message: msg,
                              bgcolor: '#324e59',
                              color: '#fff'
                          }
                      });
                  })
                  .fail(function (xhr, textStatus, errorThrown) {
                      alert(xhr.responseText);
                  });
        });

        // edit task - remove task from an industry (on check with ajax)
        $(document).on("ifUnchecked", ".task_check", function () {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('task_check_', "");
            var arr = id_nr.split('_');
            var data = {
                'id_industry': arr[0],
                'id_ms_task': arr[1],
                'is_checked': 0
            };
            // save to database
            jQuery.post('{{  route("ajax.check_admin_industry_task") }}', data)
                    .done(function (msg) {
                        $.amaran({
                            'theme': 'colorful',
                            'content': {
                                message: msg,
                                bgcolor: '#324e59',
                                color: '#fff'
                            }
                        });
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        alert(xhr.responseText);
                    });
        });

        $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
            var id = jQuery(this).attr('id');
            var ms_task_id = $(this).attr('data-ms-task-id');
            var id_nr = id.replace('is_default_task_', '');
            var data = {
                'id_ms_task': id_nr,
                'is_default': state
            };
            jQuery.post('{{  route("ajax.set_task_default") }}', data)
                  .done(function (msg) {
                      if(msg == 1) {
                          $('#'+ms_task_id).find('.fa-bell-o').remove();
                      } else {
                          $('#'+ms_task_id).append('<i class="fa fa-bell-o" style="font-size: 13px"></i>');
                      }
                  })
                  .fail(function (xhr, textStatus, errorThrown) {
                      alert(xhr.responseText);
                  });
        });

         // edit an existing task
        function modal_dialog_edit_task_admin(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_edit_task_admin/" + id;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                // call dropzone - drag and drop files
                                $(".dropzone").dropzone({uploadMultiple: false});
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
    //                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };

                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                      .done(function (msg) {
  //                                                alert(msg)
                                      })
                                      .fail(function (xhr, textStatus, errorThrown) {
                                          alert(xhr.responseText);
                                      });
                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
            setTimeout( function() {
                var attachmentHeight = $('.attachment-section').height();
                $('.attachment-section').css('position', 'absolute');
                $('.attachment-section').css('top', $('#task_description').outerHeight());
                $('.attachment-section').css('width', '100%');
                $('#task_description').css('margin-bottom', attachmentHeight+'px');
            }, 1000);
        }

        // open add new task for an existing submodule
        function modal_dialog_add_task(id) {
            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task/" + id;
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                $(".dropzone").dropzone({uploadMultiple: false});
                                // delete attachment
                                jQuery('.button_delete_file').click(function (event) {
                                    if (confirm("Are you sure?")) {

                                        var id = jQuery(this).attr('id');
                                        var id_nr = id.replace('button_delete_file', '');
                                        var data = {'id_software_attachment': id_nr};
                                        jQuery.post('{{  route("ajax.remove_file_software") }}', data)
                                                .done(function (msg) {
                                                    if (parseInt(msg) > 0) {
                                                        var attached_files, new_val, id_ms_task;
                                                        id_ms_task = parseInt(msg);
                                                        attached_files = $('#attachment_counter' + id_ms_task).html();
                                                        new_val = parseInt(attached_files) - 1;
                                                        $('#attachment_counter' + id_ms_task).html(new_val);
                                                    }
                                                })
                                                .fail(function (xhr, textStatus, errorThrown) {
                                                    alert(xhr.responseText);
                                                });
                                        jQuery('#col-attachments' + id_nr).hide();
                                        return true;
                                    }
                                    return false;
                                });
                                // cal edit buttons on an attachment file
                                $('.col-attachments').hover(function () {
                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('col-attachments', "");
                                    $('#attachments_edit_buttons' + id_nr).toggle();
//                                $('#attachments_name_file' + id_nr).toggle();
                                });

                                $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {

                                    var id = jQuery(this).attr('id');
                                    var id_nr = id.replace('is_default_task_', '');
                                    var data = {
                                        'id_ms_task': id_nr,
                                        'is_default': state
                                    };
                                    jQuery.post('{{  route("ajax.set_task_default") }}', data)
                                          .done(function (msg) {
//                                                alert(msg)
                                          })
                                          .fail(function (xhr, textStatus, errorThrown) {
                                              alert(xhr.responseText);
                                          });

                                });

                                // description on hover
                                $('.file_attachments').hover(function () {
                                    var tooltip = '<div class="tooltip"></div>';
                                    // Hover over code
                                    var title = $.trim($(this).attr('title'));

                                    if (title.length > 0) {
                                        $(this).data('tipText', title).removeAttr('title');
                                        $('body').append(tooltip);
                                        $('.tooltip').html(title);
                                        $('.tooltip').fadeIn('slow');
                                    } else {
                                        $('body').append(tooltip);
                                    }

                                }, function () {
                                    // Hover out code
                                    $(this).attr('title', $(this).data('tipText'));
                                    $('.tooltip').remove();
                                }).mousemove(function (e) {
                                    var mousex = e.pageX + 20; //Get X coordinates
                                    var mousey = e.pageY + 10; //Get Y coordinates
                                    $('.tooltip').css({top: mousey, left: mousex})
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);
                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // open add new task for a newly created module/submodule
        function modal_dialog_add_task_new(id) {

            if (id == 0) {
                document.getElementById('task_page_details').innerHTML = ' Please select desired structure. ';
            }
            else {
                var strURL = "{{URL::to('/')}}/modal_dialog_add_task_new/" + id + "/<?php print $software->id_software; ?>";
                var req = getXMLHTTP();
                if (req) {
                    req.onreadystatechange = function () {
                        if (req.readyState == 4) {
                            // only if "OK"
                            if (req.status == 200) {
                                jQuery('#task_page_details').html(req.responseText);
                                input_tags_call();
                                jQuery(".switchCheckBox").bootstrapSwitch();
                                $('.summernote').summernote({});
                                $('input.icheck-green').iCheck({
                                    checkboxClass: 'icheckbox_minimal-green',
                                    radioClass: 'iradio_minimal-green',
                                    increaseArea: '10%' // optional
                                });
                                show_modal_box();
                            } else {
                                jQuery('#task_page_details').html("There was a problem while using XMLHTTP:\n" + req.statusText);

                            }
                        }
                    };
                    req.open("GET", strURL, true);
                    req.send(null);
                }
            }
        }

        // drag and drop order tasks/ module
        $(function () {
            $('.sortable_task').sortable({
                axis: 'y',
                opacity: 0.7,
                group: '.sortable_task',
                tolerance: 'pointer',
                update: function (event, ui) {
                    var list_sortable = $(this).sortable('toArray').toString();
//                    alert(list_sortable)
                    // change order in the database using Ajax
                    var data = {
                        'list_order': list_sortable,
                    };
                    $.post('{{  route("ajax.set_order_adm_task") }}', data)
                            .done(function (msg) {
//                            alert('ok')
                            })
                            .fail(function (xhr, textStatus, errorThrown) {
                                alert(xhr.responseText);
                            });
                }
            });
            // do this for every level
            for (var level = 0; level <= 5; level++) {
                $('.sortable_modules_' + level).sortable({
                    axis: 'y',
                    opacity: 0.7,
                    handle: 'span',
                    update: function (event, ui) {
                        var list_sortable = $(this).sortable('toArray').toString();
                        // change order in the database using Ajax
                        var data = {
                            'list_order': list_sortable
                        };

                        $.post('{{  route("ajax.set_order_adm_modules") }}', data)
                                .done(function (msg) {
                                    // success
                                })
                                .fail(function (xhr, textStatus, errorThrown) {
                                    //  alert(xhr.responseText);
                                });
                    }
                });
            }
        });

    </script>
      <script type="text/javascript">
         $(document).on("click", ".delete_task", function (e) {
             e.preventDefault();
             $('.confirm_delete_keyword').removeClass('disabled');
             var data_id = $(this).attr('id');
             var msg = $(this).attr('data-msg');
              $('#formConfirm')
                 .find('#frm_body').html(msg)
                 .end().modal('show');
              $('#frm_submit').addClass('confirm_delete_keyword').attr('data_id', data_id);
              $('#formConfirm').on('click', '.confirm_delete_keyword', function(ev) {
                 var id = $(this).attr('data_id');
                 var form = $("[id=" + id + "]").parent();
                 $(form).submit();
             });
          });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#scrollTo').length != 0) {
                var id_tasks = [];

                $('.task_id').each (function() {
                    var task_id = $(this).text().replace(' ', '');
                    id_tasks.push(task_id);
                });

                if(jQuery.inArray($('#scrollTo').val(), id_tasks) !== -1) {
                    $('html, body').animate({
                        scrollTop: $('#'+$('#scrollTo').val()).offset().top - 50
                    }, 'slow');
                }
            }
            hide_empty_modules();
        });
    </script>
    <script>

    </script>
@stop

@section('footer_scripts')
    <script src="{{ URL::asset('assets/js/software/dropzone.js')}}"></script>
    <!-- Module menu tree style -->
    <script src="{{ URL::asset('assets/js/pages/demo.treeView.js')}}"></script>
    @include('admin/software/tree_script')
    <script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/drag_order/jquery-ui-1.10.4.custom.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/selectize.min.js')}}"></script>
    {{--<script src="{{ URL::asset('assets/js/jquery.sticky.js')}}"></script>--}}
    @include('admin/scripts/ajax_scripts')
    <script type="text/javascript">
        $('.is_default_task').on('switchChange.bootstrapSwitch', function (event, state) {
            var id = jQuery(this).attr('id');
            var id_nr = id.replace('is_default_task_', '');
            var data = {
                'id_ms_task': id_nr,
                'is_default': state
            };
            jQuery.post('{{  route("ajax.set_task_default") }}', data)
              .done(function (msg) {
//                                                alert(msg)
              })
              .fail(function (xhr, textStatus, errorThrown) {
                  alert(xhr.responseText);
              });
        });
    </script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $('th').click(function(){
            var table = $(this).parents('#TaskTable').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc){rows = rows.reverse()}
            for (var i = 0; i < rows.length; i++){table.append(rows[i])}
        })
        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index), valB = getCellValue(b, index)
                return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
            }
        }
        function getCellValue(row, index){
            return $(row).children('td').eq(index).html()
        }
    </script>

@stop

@section('content')
    {{-- <a href="{{ action('TaskController@approve_tasks') }}"
        <div class="row">
            send test email
        </div>
    </a> --}}
    <div class="panel widget light-widget panel-bd-top">
      <div class="panel-heading ">
          <h3 class="mgtp-10">Tasks</h3>
      </div>
      <div class="panel-body table_invoice">
          <!-- /.box-header -->
          {{-- <div class="box-body"> --}}
              @if(count($task) > 0)
                  <table id="TaskTable" class="table table-responsive table-condensed data_table">
                      <thead>
                        <tr class="row">
                          <th> {{trans('messages.label_id')}} </th>
                          <th id="TaskName">{{trans('messages.l_name')}}</th>
                          <th id="TaskIndustry">{{trans('messages.label_industry')}}</th>
                          <th id="TaskDate" class="text-center">{{trans('messages.label_date')}}</th>
                          <th id="TaskCompany">{{trans('messages.label_company')}}</th>
                          <th id="TaskStatus" class="text-center">{{trans('messages.label_status')}}</th>
                          <th id="TaskAction" class="text-center">{{trans('messages.label_action')}}</th>
                        </tr>
                      </thead>
                        <?php $industries = Industry::all(); ?>
                      <tbody>
                          @foreach($task as $row)
                            {{-- {{'<pre>', dd($row)}} --}}
                              <tr class="row" id="{{$row->id_ms_task}}">
                                  <td class="task_id">{{$row->id_ms_task}}</td>
                                  <td class="task_name" style="width: 500px;"> {{ $row->name_task  }} </td>
                                  <td class="task_industry" >
                                      @foreach($industries as $industry)
                                          <div style="display: block">
                                               {{(Industry_ms::isIndustryChecked($industry->id_industry, $row->id_ms_task) == true)? $industry->name_industry:''}}
                                          </div>
                                      @endforeach
                                  </td>
                                  <td class="task_date">{{ date("d/m/Y", strtotime($row->created_at)) }} </td>
                                  <?php
                                    $company_name = DB::table('companies')->whereId_company($row->fk_company)->pluck('name_company');
                                   ?>
                                  <td class="task_Company"> {{($company_name) ? $company_name : "Task from admin."}} </td>
                                  <td class="task_status text-center">
                                      {{Form::checkbox('is_default', 1, $row->is_default, ['class'=>'switchCheckBox inline is_default_task','id'=>'is_default_task_'.$row->id_ms_task,'data-size'=>"mini"])}}
                                  </td>
                                  <td class="task_buttons text-center">
                                      <a
                                         href="javascript:void(0)"
                                         onclick="modal_dialog_edit_task_admin(<?php print $row->id_task ?>)"
                                         class="btn btn-app btn-round ls-orange-btn">
                                          <i class="fa fa-edit"></i>
                                      </a>
                                      {{ Form::open(array('route' => array('task.destroy', $row->id_task), 'method' => 'delete')) }}
                                          <button type="submit"
                                                  class="btn ls-red-btn btn-round delete_task"
                                                  id="{{$row->id_task}}"
                                                  data-toggle="modal" data-target="#confirmDelete"
                                                  data-title="Delete Task"
                                                  data-msg="{{trans('validation.confirmation_delete_task')}}">
                                              <i class="fa fa-trash"></i>
                                          </button>
                                      {{ Form::close()}}
                                  </td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
                  {{-- Confirmation Messages--}}
                  @if( Session::has('error_message'))
                      <div class="form-group has-error">
                          <label class="control-label" for="inputError">
                              <i class="fa fa-times-circle-o"></i> {{ Session::get('error_message') }}</label>
                      </div>
                  @endif
                  @if( Session::has('success_message'))
                      <div class="form-group has-success">
                          <label class="control-label" for="inputSuccess">
                              <i class="fa fa-check"></i> {{ Session::get('success_message') }}</label>
                      </div>
                  @endif
                  <div class="row text-center">{{$pagination}}</div>
              @endif
              <br>
              <div id="task_page_details_parent">
                  <div class="task_page_details" id="task_page_details"></div>
              </div>
              @if(Session::has('scrollTo'))
                  <input type="hidden" id="scrollTo" value="{{Session::get('scrollTo')}}">
              @endif
          {{-- </div> --}}
          <!-- /.box-body -->
      </div>
      @include('admin.alert_box.delete_confirm')
    </div>
@stop
